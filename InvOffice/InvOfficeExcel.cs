﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Inv.Support;
using CX = ClosedXML.Excel;

namespace Inv.Office
{
  public static class ExcelFoundation
  {
    public static bool IsInstalled()
    {
#if NET5_0_OR_GREATER
      if (!OperatingSystem.IsWindows())
        return false;
#endif

      return Type.GetTypeFromProgID("Excel.Application") != null;
    }
  }

  public static class ExcelWorksheetLimits
  {
    public static int MinRowNumber => CX.XLHelper.MinRowNumber;
    public static int MaxRowNumber => CX.XLHelper.MaxRowNumber;
    public static int MinColumnNumber => CX.XLHelper.MinColumnNumber;
    public static int MaxColumnNumber => CX.XLHelper.MaxColumnNumber;

    /// <summary>
    /// Earlier dates will be represented as strings and cannot be used in calculations.
    /// </summary>
    public static DateTime EarliestDateForCalculation = new DateTime(1900, 01, 01);
    public static DateTime LeapYearBugUntilDate = new DateTime(1900, 02, 28);
  }

  public enum ExcelBorderStyle
  {
    DashDot = CX.XLBorderStyleValues.DashDot,
    DashDotDot = CX.XLBorderStyleValues.DashDotDot,
    Dashed = CX.XLBorderStyleValues.Dashed,
    Dotted = CX.XLBorderStyleValues.Dotted,
    Double = CX.XLBorderStyleValues.Double,
    Hair = CX.XLBorderStyleValues.Hair,
    Medium = CX.XLBorderStyleValues.Medium,
    MediumDashDot = CX.XLBorderStyleValues.MediumDashDot,
    MediumDashDotDot = CX.XLBorderStyleValues.MediumDashDotDot,
    MediumDashed = CX.XLBorderStyleValues.MediumDashed,
    None = CX.XLBorderStyleValues.None,
    SlantDashDot = CX.XLBorderStyleValues.SlantDashDot,
    Thick = CX.XLBorderStyleValues.Thick,
    Thin = CX.XLBorderStyleValues.Thin
  }

  internal static class ExcelUtilities
  {
    /// <summary>
    /// Implemented to overcome failure to set row height when a cell has WrapText = true.
    /// </summary>
    /// <param name="Cell"></param>
    public static void AdjustHeightToContents(ExcelCell Cell)
    {
      AdjustHeightToContents(Cell.Row, Cell.ColumnNumber, Cell.ColumnNumber);
    }
    public static void AdjustHeightToContents(ExcelRange Range)
    {
      var RowList = Range.RowList();
      var FirstRow = RowList.First();
      var FirstColumnNumber = FirstRow.Base.FirstCell().WorksheetColumn().ColumnNumber();
      var LastColumnNumber = FirstRow.Base.LastCellUsed().WorksheetColumn().ColumnNumber();

      RowList.ForEach(R => AdjustHeightToContents(R, FirstColumnNumber, LastColumnNumber));
    }
    public static void AdjustHeightToContents(List<ExcelRow> RowList)
    {
      RowList.ForEach(R => AdjustHeightToContents(R));
    }
    public static void AdjustHeightToContents(ExcelRow Row)
    {
      var FirstColumnNumber = Row.Base.FirstCellUsed().WorksheetColumn().ColumnNumber();
      var LastColumnNumber = Row.Base.LastCellUsed().WorksheetColumn().ColumnNumber();

      AdjustHeightToContents(Row, FirstColumnNumber, LastColumnNumber);
    }
    public static void AdjustHeightToContents(ExcelRow Row, int FromColumnNumber, int UntilColumnNumber)
    {
      Debug.Assert(FromColumnNumber <= UntilColumnNumber, "FromColumnNumber must not be greater than UntilColumnNumber.");

      var BaseRow = Row.Base;

      var CellWrapTextList = BaseRow.CellsUsed(C =>
      {
        var ColumnNumber = C.WorksheetColumn().ColumnNumber();
        return ColumnNumber >= FromColumnNumber && ColumnNumber <= UntilColumnNumber && C.Style.Alignment.WrapText && C.DataType == CX.XLDataType.Text;
      }
      ).ToList();

      var MaxTextHeight = 0.0;

      foreach (var Cell in CellWrapTextList)
      {
        var Text = Cell.GetString();
        var Font = Cell.Style.Font;
        //var Width = Cell.IsMerged() ? 9999 : Cell.WorksheetColumn().Width;  // Excel does not consider merged columns. Defaults to single line height.
        var Width = Cell.IsMerged() ? RangeWidth(Cell.MergedRange()) : Cell.WorksheetColumn().Width;   // Consider merged columns.
        var Height = MeasureTextHeight(Text, Font, Width);
        MaxTextHeight = Math.Max(MaxTextHeight, Height);
      }

      if (MaxTextHeight > BaseRow.Height)
        BaseRow.Height = MaxTextHeight;
      else
        BaseRow.AdjustToContents();     // Does not handle WrapText or Merged
    }

    private static double RangeWidth(CX.IXLRange BaseRange)
    {
      return BaseRange.Columns().Select(C => C.WorksheetColumn().Width).Sum();
    }
    private static double MeasureTextHeight(string Text, CX.IXLFont Font, double Width)
    {
      if (string.IsNullOrEmpty(Text))
        return 0.0;

#if NET48
      using (var Bitmap = new System.Drawing.Bitmap(1, 1))
      using (var Graphics = System.Drawing.Graphics.FromImage(Bitmap))
      using (var DrawingFont = new System.Drawing.Font(Font.FontName, (float)Font.FontSize))
      {
        var PixelWidth = Convert.ToInt32(Width * 7.5);  //7.5 pixels per excel column width
        var TextSize = Graphics.MeasureString(Text, DrawingFont, PixelWidth);

        // 72 DPI and 96 points per inch.  Excel height in points with max of 409 per Excel requirements.
        return Math.Min(Convert.ToDouble(TextSize.Height) * 72.0 / 96.0, 409.0);
      }
#else
      return Math.Min(Font.FontSize * 72.0 / 96.0, 409.0); // TODO: is this close enough?
#endif
    }
  }

  public sealed class ExcelFile : IDisposable
  {
    public static ExcelFile Create(Inv.File File, bool TrackEvents)
    {
      return Create(File.GetPlatformPath(), TrackEvents);
    }
    public static ExcelFile Create(string FilePath, bool TrackEvents)
    {
      var Result = new ExcelFile(FilePath, TrackEvents);
      Result.Create();
      return Result;
    }
    public static ExcelFile Open(Inv.File File, bool TrackEvents)
    {
      return Open(File.GetPlatformPath(), TrackEvents);
    }
    public static ExcelFile Open(string FilePath, bool TrackEvents)
    {
      var Result = new ExcelFile(FilePath, TrackEvents);
      Result.Open();
      return Result;
    }

    public ExcelProperties Properties
    {
      get
      {
        if (PropertiesField == null)
          PropertiesField = new ExcelProperties(Base.Properties);
        return PropertiesField;
      }
    }
    public ExcelPageSetup PageSetup
    {
      get
      {
        if (PageSetupField == null)
          PageSetupField = new ExcelPageSetup(Base.PageOptions);
        return PageSetupField;
      }
    }
    public ExcelStyle Style
    {
      get
      {
        if (StyleField == null)
          StyleField = new ExcelStyle(Base.Style);
        return StyleField;
      }
    }

    public ExcelSheet AddSheet(string Name)
    {
      return new ExcelSheet(Base.Worksheets.Add(Name));
    }
    public void Save()
    {
      if (IsOpened)
        Base.Save();
      else
        Base.SaveAs(FilePath);
    }

    void IDisposable.Dispose()
    {
      Base?.Dispose();
    }

    private ExcelFile(string FilePath, bool TrackEvents)
    {
      this.FilePath = FilePath;
      this.TrackEvents = TrackEvents;
    }
    private void Create()
    {
      this.Base = new CX.XLWorkbook(TrackEvents ? CX.XLEventTracking.Enabled : CX.XLEventTracking.Disabled);

      // Configure page defaults to apply to all worksheets.
      PageSetup.A4();
      PageSetup.OrientPortrait();
    }
    private void Open()
    {
      this.Base = new CX.XLWorkbook(FilePath, TrackEvents ? CX.XLEventTracking.Enabled : CX.XLEventTracking.Disabled);
      this.IsOpened = true;
    }

    private readonly string FilePath;
    private readonly bool TrackEvents;
    private CX.XLWorkbook Base;
    private ExcelProperties PropertiesField;
    private ExcelPageSetup PageSetupField;
    private ExcelStyle StyleField;
    private bool IsOpened;
  }

  public sealed class ExcelProperties
  {
    internal ExcelProperties(CX.XLWorkbookProperties Properties)
    {
      this.Base = Properties;
    }

    public string Author
    {
      get => Base.Author;
      set => Base.Author = value;
    }
    public string Title
    {
      get => Base.Title;
      set => Base.Title = value;
    }
    public string Subject
    {
      get => Base.Subject;
      set => Base.Subject = value;
    }
    public string Category
    {
      get => Base.Category;
      set => Base.Category = value;
    }
    public string Keywords
    {
      get => Base.Keywords;
      set => Base.Keywords = value;
    }
    public string Comments
    {
      get => Base.Comments;
      set => Base.Comments = value;
    }
    public string Status
    {
      get => Base.Status;
      set => Base.Status = value;
    }
    public string LastModifiedBy
    {
      get => Base.LastModifiedBy;
      set => Base.LastModifiedBy = value;
    }
    public string Company
    {
      get => Base.Company;
      set => Base.Company = value;
    }
    public string Manager
    {
      get => Base.Manager;
      set => Base.Manager = value;
    }

    private readonly CX.XLWorkbookProperties Base;
  }

  public sealed class ExcelSheet : IDisposable
  {
    internal ExcelSheet(CX.IXLWorksheet Worksheet)
    {
      this.Base = Worksheet;
      this.PageSetup = new ExcelPageSetup(Base.PageSetup);
    }

    public string Name
    {
      get => Base.Name;
      set => Base.Name = value;
    }
    public ExcelPageSetup PageSetup { get; }
    /// <summary>
    /// Position of worksheet in workbook.
    /// Setting position will shuffle other sheets' positions
    /// </summary>
    public int Position
    {
      get => Base.Position;
      set => Base.Position = value;
    }

    public void Dispose()
    {
      Base.Dispose();
    }
    public ExcelColumn GetColumn(int ColumnNumber)
    {
      Debug.Assert(ColumnNumber >= ExcelWorksheetLimits.MinColumnNumber && ColumnNumber <= ExcelWorksheetLimits.MaxColumnNumber, "");

      return new ExcelColumn(Base.Column(ColumnNumber));
    }
    public ExcelRow GetRow(int RowNumber)
    {
      Debug.Assert(RowNumber >= ExcelWorksheetLimits.MinRowNumber && RowNumber <= ExcelWorksheetLimits.MaxRowNumber, "");

      return new ExcelRow(Base.Row(RowNumber));
    }
    public ExcelColumnManager ColumnManager()
    {
      return new ExcelColumnManager(Base.Columns());
    }
    public ExcelColumnManager ColumnManager(int ColumnNumber)
    {
      return new ExcelColumnManager(Base.Columns(ColumnNumber, ColumnNumber));
    }
    public ExcelColumnManager ColumnManager(string ColumnAddress)
    {
      return new ExcelColumnManager(Base.Columns(ColumnAddress));
    }
    public ExcelColumnManager ColumnManager(int FirstColumnNumber, int LastColumnNumber)
    {
      return new ExcelColumnManager(Base.Columns(FirstColumnNumber, LastColumnNumber));
    }
    public ExcelColumnManager ColumnManager(string FirstColumnAddress, string LastColumnAddress)
    {
      return new ExcelColumnManager(Base.Columns(FirstColumnAddress, LastColumnAddress));
    }
    public ExcelRowManager RowManager()
    {
      return new ExcelRowManager(Base.Rows());
    }
    public ExcelRowManager RowManager(string RowAddress)
    {
      return new ExcelRowManager(Base.Rows(RowAddress));
    }
    public ExcelRowManager RowManager(int FirstRowNumber, int LastRowNumber)
    {
      return new ExcelRowManager(Base.Rows(FirstRowNumber, LastRowNumber));
    }
    public ExcelCell GetCell(int RowNumber, int ColumnNumber)
    {
      return new ExcelCell(Base.Cell(RowNumber, ColumnNumber));
    }
    public ExcelCell GetCell(string Address)
    {
      return new ExcelCell(Base.Cell(Address));
    }
    public ExcelRange GetRange(int FromRowNumber, int FromColumnNumber, int UntilRowNumber, int UntilColumnNumber)
    {
      return new ExcelRange(Base.Range(FromRowNumber, FromColumnNumber, UntilRowNumber, UntilColumnNumber));
    }
    public ExcelRange GetRange(string FirstCellAddress, string LastCellAddress)
    {
      return new ExcelRange(Base.Range(FirstCellAddress, LastCellAddress));
    }
    public void FreezeToRow(int UntilRow)
    {
      Base.SheetView.FreezeRows(UntilRow);
    }
    public void FreezeToColumn(int UntilColumn)
    {
      Base.SheetView.FreezeColumns(UntilColumn);
    }
    public void SetTabActive(bool IsActive = true)
    {
      Base.SetTabActive(IsActive);
    }

    private readonly CX.IXLWorksheet Base;
  }

  public sealed class ExcelColumnManager
  {
    internal ExcelColumnManager(CX.IXLColumns Columns)
    {
      this.Base = Columns;
      this.Style = new ExcelStyle(Base.Style);
    }

    public ExcelStyle Style { get; }
    public double Width
    {
      set => Base.Width = value;
    }

    public void Clear()
    {
      Base.Clear();
    }
    public void AdjustWidthToContents()
    {
      Base.AdjustToContents();
    }
    public void AdjustWidthToContents(int StartRow)
    {
      Base.AdjustToContents(StartRow);
    }
    public void AdjustWidthToContents(double MinWidth, double MaxWidth)
    {
      Base.AdjustToContents(MinWidth, MaxWidth);
    }
    public void AdjustWidthToContents(int StartRow, int EndRow)
    {
      Base.AdjustToContents(StartRow, EndRow);
    }
    public void AdjustWidthToContents(int StartRow, double MinWidth, double MaxWidth)
    {
      Base.AdjustToContents(StartRow, MinWidth, MaxWidth);
    }
    public void AdjustWidthToContents(int StartRow, int EndRow, double MinWidth, double MaxWidth)
    {
      Base.AdjustToContents(StartRow, EndRow, MinWidth, MaxWidth);
    }

    private readonly CX.IXLColumns Base;
  }

  public sealed class ExcelRowManager
  {
    internal ExcelRowManager(CX.IXLRows Rows)
    {
      this.Base = Rows;
      this.Style = new ExcelStyle(Base.Style);
    }

    public ExcelStyle Style { get; }
    public double Height
    {
      set => Base.Height = value;
    }

    public void Clear()
    {
      Base.Clear();
    }
    public void AdjustHeightToContents()
    {
      Base.AdjustToContents();
    }
    public void AdjustToContents(int StartRow)
    {
      Base.AdjustToContents(StartRow);
    }
    public void AdjustHeightToContents(double MinWidth, double MaxWidth)
    {
      Base.AdjustToContents(MinWidth, MaxWidth);
    }
    public void AdjustHeightToContents(int StartRow, int EndRow)
    {
      Base.AdjustToContents(StartRow, EndRow);
    }
    public void AdjustHeightToContents(int StartRow, double MinWidth, double MaxWidth)
    {
      Base.AdjustToContents(StartRow, MinWidth, MaxWidth);
    }
    public void AdjustHeightToContents(int StartRow, int EndRow, double MinWidth, double MaxWidth)
    {
      Base.AdjustToContents(StartRow, EndRow, MinWidth, MaxWidth);
    }

    private readonly CX.IXLRows Base;
  }

  public sealed class ExcelCell
  {
    internal ExcelCell(CX.IXLCell Cell)
    {
      this.Base = Cell;
    }

    public ExcelStyle Style
    {
      get
      {
        if (_Style == null)
          _Style = new ExcelStyle(Base.Style);

        return _Style;
      }
    }
    public bool ValueAsBoolean
    {
      get
      {
        Debug.Assert(bool.TryParse(Base.GetString(), out _), "Boolean type expected.");

        return Base.GetBoolean();
      }
      set
      {
        Base.SetValue<bool>(value);
      }
    }
    public Inv.Date ValueAsDate
    {
      get
      {
        Debug.Assert(Inv.Date.TryParse(Base.GetString(), out _), "Inv.Date type expected.");
        return Inv.Date.Parse(Base.GetString());
      }
      set
      {
        this.ValueAsDateTime = value.ToDateTime();
      }
    }
    public Inv.Time ValueAsTime
    {
      get
      {
        Debug.Assert(Inv.Time.TryParse(Base.GetString(), out _), "Inv.Time type expected.");
        return Inv.Time.Parse(Base.GetString());
      }
      set
      {
        var OADate = value.ToDateTime().ToOADate();
        this.ValueAsDouble = OADate - Math.Truncate(OADate);
        this.Style.NumberFormatId = 18;
      }
    }
    /// <summary>
    /// Note: Dates earlier than 01/01/1900 will be set as strings
    /// </summary>
    public DateTime ValueAsDateTime
    {
      get
      {
        Debug.Assert(DateTime.TryParse(Base.GetString(), out _), "DateTime type expected.");
        return Base.GetDateTime();
      }
      set
      {
        if (value < ExcelWorksheetLimits.EarliestDateForCalculation)
        {
          Base.SetValue<string>(value.ToString("dd/MM/yyyy"));
        }
        else
        {
          if (value > ExcelWorksheetLimits.LeapYearBugUntilDate)
            Base.SetValue(value);
          else
            Base.SetValue(value.Subtract(TimeSpan.FromDays(1)));
        }
      }
    }
    public char ValueAsCharacter
    {
      get
      {
        Debug.Assert(char.TryParse(Base.GetString(), out _), "Character type expected.");
        return char.Parse(Base.GetString());
      }
      set => Base.SetValue<char>(value);
    }
    public string ValueAsString
    {
      get => Base.GetString();
      set
      {
        Debug.Assert(value == null || value.GetType() == typeof(string), "String type expected.");
        Base.SetValue<string>(value);
      }
    }
    public decimal ValueAsDecimal
    {
      get
      {
        Debug.Assert(Decimal.TryParse(Base.GetString(), out _), "Decimal type expected.");
        return Base.GetValue<decimal>();
      }
      set => Base.SetValue<decimal>(value);
    }
    public double ValueAsDouble
    {
      get
      {
        Debug.Assert(Double.TryParse(Base.GetString(), out _), "Double type expected.");
        return Base.GetDouble();
      }
      set => Base.SetValue<double>(value);
    }
    public int ValueAsInteger32
    {
      get
      {
        Debug.Assert(int.TryParse(Base.GetString(), out _), "int type expected.");
        return Base.GetValue<int>();
      }
      set => Base.SetValue<int>(value);
    }
    public long ValueAsInteger64
    {
      get
      {
        Debug.Assert(long.TryParse(Base.GetString(), out _), "long type expected.");
        return Base.GetValue<long>();
      }
      set => Base.SetValue<long>(value);
    }
    public float ValueAsFloat
    {
      get
      {
        Debug.Assert(float.TryParse(Base.GetString(), out _), "float type expected.");

        return Base.GetValue<float>();
      }
      set
      {
        Base.SetValue<float>(value);
      }
    }
    public decimal ValueAsCurrency
    {
      get
      {
        Debug.Assert(decimal.TryParse(Base.GetString(), out _), "decimal type expected.");

        return Base.GetValue<decimal>();
      }
      set
      {
        Base.SetValue<decimal>(value);
        this.Style.NumberFormat.CurrencyFormat();
      }
    }
    public Inv.Money ValueAsMoney
    {
      get
      {
        Debug.Assert(Inv.Money.TryParse(Base.GetString(), out _), "Inv.Money type expected.");

        return Base.GetValue<Inv.Money>();
      }
      set
      {
        Base.SetValue<decimal>(value.GetAmount());
        this.Style.NumberFormat.CurrencyFormat();
      }
    }
    public TimeSpan ValueAsTimeSpan
    {
      get
      {
        Debug.Assert(TimeSpan.TryParse(Base.GetString(), out _), "TimeSpan type expected.");

        return Base.GetTimeSpan();
      }
      set
      {
        Base.SetValue<TimeSpan>(value);
      }
    }
    /// <summary>
    /// Formula string based on row and column addressing notation (eg: "R1C2+RC[-2]")
    /// </summary>
    public string ValueAsFormulaR1C1Format
    {
      get
      {
        Debug.Assert(Base.HasFormula, "Cell expected to contain formula.");
        return Base.FormulaR1C1;
      }
      set
      {
        Debug.Assert(value == null || value.GetType() == typeof(string), "String type expected for a formula cell.");
        Base.SetFormulaR1C1(value);
      }
    }
    public ExcelColumn Column
    {
      get => new ExcelColumn(Base.WorksheetColumn());
    }
    public ExcelRow Row
    {
      get => new ExcelRow(Base.WorksheetRow());
    }
    public string AddressString
    {
      get => Base.Address.ToString();
    }
    public int RowNumber
    {
      get => Base.Address.RowNumber;
    }
    public int ColumnNumber
    {
      get => Base.Address.ColumnNumber;
    }

    public ExcelRange AsRange()
    {
      return new ExcelRange(Base.AsRange());
    }
    public string GetFormattedString()
    {
      return Base.GetFormattedString();
    }
    public void Clear()
    {
      Base.Clear();
    }
    public void Select()
    {
      Base.Select();
    }
    public ExcelCell CellRight(int Step = 1)
    {
      return new ExcelCell(Base.CellRight(Step));
    }
    public ExcelCell CellLeft(int Step = 1)
    {
      return new ExcelCell(Base.CellLeft(Step));
    }
    public ExcelCell CellAbove(int Step = 1)
    {
      return new ExcelCell(Base.CellAbove(Step));
    }
    public ExcelCell CellBelow(int Step = 1)
    {
      return new ExcelCell(Base.CellBelow(Step));
    }
    public ExcelSheet GetWorksheet()
    {
      return new ExcelSheet(Base.Worksheet);
    }

    private readonly CX.IXLCell Base;
    private ExcelStyle _Style;
  }

  public sealed class ExcelRange
  {
    internal ExcelRange(CX.IXLRange Range)
    {
      this.Base = Range;
      this.Style = new ExcelStyle(Base.Style);
    }

    /// <summary>
    /// Formula string based on Excel addressing notation (eg: "A2+$B$2")
    /// </summary>
    public string ValueAsFormulaA1Format
    {
      set => Base.FormulaA1 = value;
    }
    /// <summary>
    /// Formula string based on row and column addressing notation (eg: "R1C2+RC[-2]")
    /// </summary>
    public string ValueAsFormulaR1C1Format
    {
      set => Base.FormulaR1C1 = value;
    }
    public ExcelStyle Style { get; }

    public Inv.DistinctList<ExcelCell> CellList()
    {
      return new Inv.DistinctList<ExcelCell>(Base.Cells().Select(C => new ExcelCell(C)));
    }
    public void FormatAsBoolean()
    {
      Base.DataType = CX.XLDataType.Boolean;
    }
    public void FormatAsDateTime()
    {
      Base.DataType = CX.XLDataType.DateTime;
    }
    public void FormatAsText()
    {
      Base.DataType = CX.XLDataType.Text;
    }
    public void FormatAsNumber()
    {
      Base.DataType = CX.XLDataType.Number;
    }
    public void FormatAsTimeSpan()
    {
      Base.DataType = CX.XLDataType.TimeSpan;
    }
    public void Clear()
    {
      Base.Clear();
    }
    public ExcelCell FirstCell()
    {
      return new ExcelCell(Base.FirstCell());
    }
    public ExcelCell LastCell()
    {
      return new ExcelCell(Base.LastCell());
    }
    public ExcelCell GetCell(int RelativeRowNumber, int RelativeColumnNumber)
    {
      return new ExcelCell(Base.Cell(RelativeRowNumber, RelativeColumnNumber));
    }
    public ExcelRangeRow FirstRow()
    {
      return new ExcelRangeRow(Base.FirstRow());
    }
    public ExcelRangeRow LastRow()
    {
      return new ExcelRangeRow(Base.LastRow());
    }
    public ExcelRangeRow Row(int RelativeRowNumber)
    {
      return new ExcelRangeRow(Base.Row(RelativeRowNumber));
    }
    public Inv.DistinctList<ExcelRow> RowList()
    {
      return new Inv.DistinctList<ExcelRow>(Base.Rows().Select(R => new ExcelRow(R.WorksheetRow())));
    }
    public ExcelRangeColumn FirstColumn()
    {
      return new ExcelRangeColumn(Base.FirstColumn());
    }
    public ExcelRangeColumn LastColumn()
    {
      return new ExcelRangeColumn(Base.LastColumn());
    }
    public ExcelRangeColumn Column(int RelativeColumnNumber)
    {
      return new ExcelRangeColumn(Base.Column(RelativeColumnNumber));
    }
    public Inv.DistinctList<ExcelColumn> ColumnList()
    {
      return new Inv.DistinctList<ExcelColumn>(Base.Columns().Select(C => new ExcelColumn(C.WorksheetColumn())));
    }
    public ExcelSheet GetWorksheet()
    {
      return new ExcelSheet(Base.Worksheet);
    }
    public void AdjustColumnWidthToContents()
    {
      var FirstRow = Base.FirstColumn().ColumnNumber();
      var LastRow = Base.LastColumn().ColumnNumber();
      Base.Columns().Select(C => C.WorksheetColumn()).ForEach(C => C.AdjustToContents(FirstRow, LastRow));
    }
    public void AdjustRowHeightToContents()
    {
      ExcelUtilities.AdjustHeightToContents(this);
    }
    public void SetRowHeight(int Height)
    {
      Base.Rows().Select(R => R.WorksheetRow()).ForEach(R => R.Height = Height);
    }
    public void SetColumnWidth(int Width)
    {
      Base.Columns().Select(C => C.WorksheetColumn()).ForEach(C => C.Width = Width);
    }
    /// <summary>
    /// Contents and style of merged cells based on the first cell.
    /// </summary>
    public void Merge()
    {
      Base.Merge();
    }
    public void Select()
    {
      Base.Select();
    }

    private readonly CX.IXLRange Base;
  }

  public sealed class ExcelStyle
  {
    internal ExcelStyle(CX.IXLStyle Style)
    {
      this.Base = Style;
      this.Font = new ExcelFont(Base.Font);
      this.NumberFormat = new ExcelNumberFormat(Base.NumberFormat);
      this.Alignment = new ExcelAlignment(Base.Alignment);
      this.Fill = new ExcelFill(Base.Fill);
      this.Border = new Border(Base.Border);
    }

    public ExcelFont Font { get; }
    public ExcelNumberFormat NumberFormat { get; }
    public ExcelAlignment Alignment { get; }
    public ExcelFill Fill { get; }
    public Border Border { get; }
    public int NumberFormatId
    {
      get => Base.NumberFormat.NumberFormatId;
      set => Base.NumberFormat.NumberFormatId = value;
    }
    public string DateFormat
    {
      get => Base.DateFormat.Format;
      set => Base.DateFormat.Format = value;
    }

    private readonly CX.IXLStyle Base;
  }

  public sealed class Border
  {
    internal Border(CX.IXLBorder Border)
    {
      this.Base = Border;
    }

    public void SetAllBorders(ExcelBorderStyle BorderStyle = ExcelBorderStyle.Thin)
    {
      SetOutsideBorder(BorderStyle);
      SetInsideBorder(BorderStyle);
    }
    public void SetAllBordersColour(Inv.Colour Colour)
    {
      var XLColour = CX.XLColor.FromArgb(Colour.GetArgb());
      Base.OutsideBorderColor = XLColour;
      Base.InsideBorderColor = XLColour;
    }
    public void SetOutsideBorder(ExcelBorderStyle BorderStyle = ExcelBorderStyle.Thin)
    {
      Base.OutsideBorder = (CX.XLBorderStyleValues)BorderStyle;
    }
    public void SetOutsideBorderColour(Inv.Colour Colour)
    {
      Base.OutsideBorderColor = CX.XLColor.FromArgb(Colour.GetArgb());
    }
    public void SetInsideBorder(ExcelBorderStyle BorderStyle = ExcelBorderStyle.Thin)
    {
      Base.InsideBorder = (CX.XLBorderStyleValues)BorderStyle;
    }
    public void SetInsideBorderColour(Inv.Colour Colour)
    {
      Base.InsideBorderColor = CX.XLColor.FromArgb(Colour.GetArgb());
    }
    public void SetTopBorder(ExcelBorderStyle BorderStyle = ExcelBorderStyle.Thin)
    {
      Base.TopBorder = (CX.XLBorderStyleValues)BorderStyle;
    }
    public void SetTopBorderColour(Inv.Colour Colour)
    {
      Base.TopBorderColor = CX.XLColor.FromArgb(Colour.GetArgb());
    }
    public void SetBottomBorder(ExcelBorderStyle BorderStyle = ExcelBorderStyle.Thin)
    {
      Base.BottomBorder = (CX.XLBorderStyleValues)BorderStyle;
    }
    public void SetBottomBorderColour(Inv.Colour Colour)
    {
      Base.BottomBorderColor = CX.XLColor.FromArgb(Colour.GetArgb());
    }
    public void SetLeftBorder(ExcelBorderStyle BorderStyle = ExcelBorderStyle.Thin)
    {
      Base.LeftBorder = (CX.XLBorderStyleValues)BorderStyle;
    }
    public void SetLeftBorderColour(Inv.Colour Colour)
    {
      Base.LeftBorderColor = CX.XLColor.FromArgb(Colour.GetArgb());
    }
    public void SetRightBorder(ExcelBorderStyle BorderStyle = ExcelBorderStyle.Thin)
    {
      Base.RightBorder = (CX.XLBorderStyleValues)BorderStyle;
    }
    public void SetRightBorderColour(Inv.Colour Colour)
    {
      Base.RightBorderColor = CX.XLColor.FromArgb(Colour.GetArgb());
    }
    public void SetDiagonalBorder(ExcelBorderStyle BorderStyle = ExcelBorderStyle.Thin)
    {
      Base.DiagonalBorder = (CX.XLBorderStyleValues)BorderStyle;
    }
    public void SetDiagonalBorderColour(Inv.Colour Colour)
    {
      Base.DiagonalBorderColor = CX.XLColor.FromArgb(Colour.GetArgb());
    }

    private readonly CX.IXLBorder Base;
  }

  public sealed class ExcelFill
  {
    internal ExcelFill(CX.IXLFill Fill)
    {
      this.Base = Fill;
    }

    public Inv.Colour BackgroundColour
    {
      get => Inv.Colour.FromArgb(Base.BackgroundColor.Color.ToArgb());
      set => Base.BackgroundColor = CX.XLColor.FromArgb(value.GetArgb());
    }

    private readonly CX.IXLFill Base;
  }

  public sealed class ExcelAlignment
  {
    internal ExcelAlignment(CX.IXLAlignment Alignment)
    {
      this.Base = Alignment;
    }

    public bool ShrinkToFit
    {
      get => Base.ShrinkToFit;
      set => Base.ShrinkToFit = value;
    }
    public int TextRotation
    {
      get
      {
        return Base.TextRotation;
      }
      set
      {
        // TODO: figure best approach here....setting -ve values weird and set > 90 appears as negative value in Excel.....

        // NOTE: ClosedXml states -90 to 180 but Excel appears to limit to -90 to 90 (defaulting to Excel limit)
        // (Has been some issues with Excel text rotation in the past for certain values)
        //Debug.Assert(value >= -90 && value <= 90, string.Format("TextRotation == {0} Must be between -90 and 90 degrees.", value.ToString()));

        Base.TextRotation = value;
      }
    }
    public bool WrapText
    {
      get => Base.WrapText;
      set => Base.WrapText = value;
    }

    public void AlignCentre()
    {
      Base.Horizontal = CX.XLAlignmentHorizontalValues.Center;
    }
    public void AlignLeft()
    {
      Base.Horizontal = CX.XLAlignmentHorizontalValues.Left;
    }
    public void AlignRight()
    {
      Base.Horizontal = CX.XLAlignmentHorizontalValues.Right;
    }
    public void AlignCentreContinuous()
    {
      Base.Horizontal = CX.XLAlignmentHorizontalValues.CenterContinuous;
    }
    public void AlignDistributed()
    {
      Base.Horizontal = CX.XLAlignmentHorizontalValues.Distributed;
    }
    public void AlignFill()
    {
      Base.Horizontal = CX.XLAlignmentHorizontalValues.Fill;
    }
    public void AlignGeneral()
    {
      Base.Horizontal = CX.XLAlignmentHorizontalValues.General;
    }
    public void AlignJustify()
    {
      Base.Horizontal = CX.XLAlignmentHorizontalValues.Justify;
    }
    public void VerticalTop()
    {
      Base.Vertical = CX.XLAlignmentVerticalValues.Top;
    }
    public void VerticalBottom()
    {
      Base.Vertical = CX.XLAlignmentVerticalValues.Bottom;
    }
    public void VerticalCentre()
    {
      Base.Vertical = CX.XLAlignmentVerticalValues.Center;
    }
    public void VerticalJustify()
    {
      Base.Vertical = CX.XLAlignmentVerticalValues.Justify;
    }
    public void VerticalDistributed()
    {
      Base.Vertical = CX.XLAlignmentVerticalValues.Distributed;
    }

    private readonly CX.IXLAlignment Base;
  }

  public sealed class ExcelNumberFormat
  {
    internal ExcelNumberFormat(CX.IXLNumberFormat NumberFormat)
    {
      this.Base = NumberFormat;
    }

    public string Format
    {
      get => Base.Format;
      set => Base.Format = value;
    }
    /// <summary>
    /// Format = "_($* #,##0.00_)"
    /// </summary>
    public void CurrencyFormat()
    {
      this.Format = "_($* #,##0.00_)";
    }
    /// <summary>
    /// Percentage as a whole number eg: "20%"
    /// </summary>
    public void PercentFormat()
    {
      Base.NumberFormatId = 9;
    }
    /// <summary>
    /// Percentage as a fractional number eg: "20.00%"
    /// </summary>
    public void PercentFractionalFormat()
    {
      Base.NumberFormatId = 10;
    }
    /// <summary>
    /// Format = "hh:mm AM/PM"
    /// </summary>
    public void TimeAmPmFormat()
    {
      Base.Format = "hh:mm AM/PM";
    }

    private readonly CX.IXLNumberFormat Base;
  }

  public sealed class ExcelFont
  {
    internal ExcelFont(CX.IXLFont Font)
    {
      this.Base = Font;
    }

    public string Name
    {
      get => Base.FontName;
      set => Base.FontName = value;
    }
    public bool Bold
    {
      get => Base.Bold;
      set => Base.Bold = value;
    }
    public bool Italic
    {
      get => Base.Italic;
      set => Base.Italic = value;
    }
    public double Size
    {
      get => Base.FontSize;
      set => Base.FontSize = value;
    }
    public Inv.Colour Colour
    {
      get => Inv.Colour.FromArgb(Base.FontColor.Color.ToArgb());
      set => Base.FontColor = CX.XLColor.FromArgb(value.GetArgb());
    }

    public void UnderlineDouble()
    {
      Base.Underline = CX.XLFontUnderlineValues.Double;
    }
    public void UnderlineDoubleAccounting()
    {
      Base.Underline = CX.XLFontUnderlineValues.DoubleAccounting;
    }
    public void UnderlineNone()
    {
      Base.Underline = CX.XLFontUnderlineValues.None;
    }
    public void UnderlineSingle()
    {
      Base.Underline = CX.XLFontUnderlineValues.Single;
    }
    public void UnderlineSingleAccounting()
    {
      Base.Underline = CX.XLFontUnderlineValues.SingleAccounting;
    }

    private readonly CX.IXLFont Base;
  }

  public sealed class ExcelColumn
  {
    internal ExcelColumn(CX.IXLColumn Column)
    {
      this.Base = Column;
      this.Style = new ExcelStyle(Base.Style);
    }

    public double Width
    {
      get => Base.Width;
      set => Base.Width = value;
    }
    public ExcelStyle Style { get; }

    public ExcelRange AsRange()
    {
      return new ExcelRange(Base.AsRange());
    }
    public void AdjustWidthToContents()
    {
      Base.AdjustToContents();
    }

    private readonly CX.IXLColumn Base;
  }

  public sealed class ExcelRangeColumn
  {
    internal ExcelRangeColumn(CX.IXLRangeColumn RangeColumn)
    {
      this.Base = RangeColumn;
      this.Style = new ExcelStyle(Base.Style);
    }

    public ExcelStyle Style { get; }

    public ExcelRange AsRange()
    {
      return new ExcelRange(Base.AsRange());
    }
    public ExcelCell FirstCell()
    {
      return new ExcelCell(Base.FirstCell());
    }
    public ExcelCell LastCell()
    {
      return new ExcelCell(Base.LastCell());
    }
    public Inv.DistinctList<ExcelCell> CellList()
    {
      return new Inv.DistinctList<ExcelCell>(Base.Cells().Select(C => new ExcelCell(C)));
    }

    private readonly CX.IXLRangeColumn Base;
  }

  public sealed class ExcelRow
  {
    internal ExcelRow(CX.IXLRow Row)
    {
      this.Base = Row;
      this.Style = new ExcelStyle(Base.Style);
    }

    public double Height
    {
      get => Base.Height;
      set => Base.Height = value;
    }
    public ExcelStyle Style { get; }

    public ExcelRange AsRange()
    {
      return new ExcelRange(Base.AsRange());
    }
    public void AdjustHeightToContents()
    {
      ExcelUtilities.AdjustHeightToContents(this);
    }
    public ExcelCell FirstCell()
    {
      return new ExcelCell(Base.FirstCell());
    }
    public ExcelCell LastCell()
    {
      return new ExcelCell(Base.LastCell());
    }
    /// <summary>
    /// Contents and style of merged cells based on the first cell.
    /// </summary>
    public void Merge()
    {
      Base.Merge();
    }

    internal readonly CX.IXLRow Base;
  }

  public sealed class ExcelRangeRow
  {
    internal ExcelRangeRow(CX.IXLRangeRow RangeRow)
    {
      this.Base = RangeRow;
      this.Style = new ExcelStyle(Base.Style);
    }

    public ExcelStyle Style { get; }

    public ExcelRange AsRange()
    {
      return new ExcelRange(Base.AsRange());
    }
    public ExcelCell FirstCell()
    {
      return new ExcelCell(Base.FirstCell());
    }
    public ExcelCell LastCell()
    {
      return new ExcelCell(Base.LastCell());
    }
    public Inv.DistinctList<ExcelCell> CellList()
    {
      return new Inv.DistinctList<ExcelCell>(Base.Cells().Select(C => new ExcelCell(C)));
    }

    private readonly CX.IXLRangeRow Base;
  }

  public sealed class ExcelPageSetup
  {
    internal ExcelPageSetup(CX.IXLPageSetup PrinterSettings)
    {
      this.Base = PrinterSettings;
      this.Header = new ExcelHeaderFooter(Base.Header);
      this.Footer = new ExcelHeaderFooter(Base.Footer);
    }

    public ExcelHeaderFooter Header { get; }
    public ExcelHeaderFooter Footer { get; }

    public void OrientLandscape()
    {
      Base.PageOrientation = CX.XLPageOrientation.Landscape;
    }
    public void OrientPortrait()
    {
      Base.PageOrientation = CX.XLPageOrientation.Portrait;
    }
    public void A2()
    {
      Base.PaperSize = CX.XLPaperSize.A2Paper;
    }
    public void A3()
    {
      Base.PaperSize = CX.XLPaperSize.A3Paper;
    }
    public void A4()
    {
      Base.PaperSize = CX.XLPaperSize.A4Paper;
    }
    public void A5()
    {
      Base.PaperSize = CX.XLPaperSize.A5Paper;
    }
    /// <summary>
    /// Gets or sets the number of pages wide (horizontal) the worksheet will be printed on.
    /// <para>If you don't specify the PagesTall, Excel will adjust that value</para>
    /// <para>based on the contents of the worksheet and the PagesWide number.</para>
    /// <para>Setting this value will override the Scale value.</para>
    /// </summary>
    public int PagesWide
    {
      get => Base.PagesWide;
      set => Base.PagesWide = value;
    }
    /// <summary>
    /// Gets or sets the number of pages tall (vertical) the worksheet will be printed on.
    /// <para>If you don't specify the PagesWide, Excel will adjust that value</para>
    /// <para>based on the contents of the worksheet and the PagesTall number.</para>
    /// <para>Setting this value will override the Scale value.</para>
    /// </summary>
    public int PagesTall
    {
      get => Base.PagesTall;
      set => Base.PagesTall = value;
    }

    private readonly CX.IXLPageSetup Base;
  }

  public sealed class ExcelHeaderFooter
  {
    internal ExcelHeaderFooter(CX.IXLHeaderFooter HeaderFooter)
    {
      this.Base = HeaderFooter;
    }

    public string LeftText
    {
      set => Base.Left.AddText(value);
    }
    public string RightText
    {
      set => Base.Right.AddText(value);
    }
    public string CentreText
    {
      set => Base.Center.AddText(value);
    }

    private readonly CX.IXLHeaderFooter Base;
  }
}