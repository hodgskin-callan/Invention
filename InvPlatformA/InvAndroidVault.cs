using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Inv
{
  internal class AndroidVault
  {
    public AndroidVault(Android.Content.Context Context)
    {
      this.Context = Context;
      this.ServiceId = Context.PackageName ?? "Invention";
      this.PasswordProtection = new Java.Security.KeyStore.PasswordProtection(FilePassword);
    }

    public IEnumerable<Secret> LoadSecrets(Inv.Vault Vault)
    {
      lock (FileLock)
      {
        Load();

        var Result = new Inv.DistinctList<Secret>();

        var Postfix = "-" + ServiceId;

        var KeyAliasEnumerator = KeyStore.Aliases();
        while (KeyAliasEnumerator.HasMoreElements)
        {
          var KeyAlias = KeyAliasEnumerator.NextElement().ToString();
          if (KeyAlias.EndsWith(Postfix))
          {
            var KeyEntry = KeyStore.GetEntry(KeyAlias, PasswordProtection) as Java.Security.KeyStore.SecretKeyEntry;
            if (KeyEntry != null)
            {
              var KeyName = KeyAlias.Split('-')[0];

              var Bytes = KeyEntry.SecretKey.GetEncoded();
              var Serialized = System.Text.Encoding.UTF8.GetString(Bytes);
              var Secret = new Secret(Vault, KeyName);
              Secret.Deserialize(Serialized);
              Result.Add(Secret);
            }
          }
        }

        Result.Sort((a, b) => a.Name.CompareTo(b.Name));

        return Result;
      }
    }
    public void Load(Secret Secret)
    {
      lock (FileLock)
      {
        Load();

        var KeyEntry = KeyStore.GetEntry(MakeAlias(Secret), PasswordProtection) as Java.Security.KeyStore.SecretKeyEntry;

        if (KeyEntry != null)
        {
          var Bytes = KeyEntry.SecretKey.GetEncoded();
          var Serialized = System.Text.Encoding.UTF8.GetString(Bytes);
          Secret.Deserialize(Serialized);
        }
        else
        {
          Secret.Properties.Clear();
        }
      }
    }
    public void Save(Secret Secret)
    {
      lock (FileLock)
      {
        Load();

        var SecretKey = new SecretKey(Secret);
        var Entry = new Java.Security.KeyStore.SecretKeyEntry(SecretKey);
        KeyStore.SetEntry(MakeAlias(Secret), Entry, PasswordProtection);

        Save();
      }
    }
    public void Delete(Secret Secret)
    {
      lock (FileLock)
      {
        Load();
        
        KeyStore.DeleteEntry(MakeAlias(Secret));

        Save();
      }
    }

    [DebuggerNonUserCode]
    private void Load()
    {
      Debug.Assert(System.Threading.Monitor.IsEntered(FileLock));
      
      if (KeyStore == null)
      {
        this.KeyStore = Java.Security.KeyStore.GetInstance(Java.Security.KeyStore.DefaultType);
        try
        {
          using (var s = Context.OpenFileInput(FileName))
            KeyStore.Load(s, FilePassword);
        }
        catch (Java.IO.FileNotFoundException)
        {
          LoadEmptyKeyStore(FilePassword);
        }
      }
    }
    private void Save()
    {
      Debug.Assert(System.Threading.Monitor.IsEntered(FileLock));

      using (var Stream = Context.OpenFileOutput(FileName, Android.Content.FileCreationMode.Private))
        KeyStore.Store(Stream, FilePassword);
    }
    private string MakeAlias(Secret Secret)
    {
      return Secret.Name + "-" + ServiceId;
    }
    private void LoadEmptyKeyStore(char[] password)
    {
      if (id_load_Ljava_io_InputStream_arrayC == IntPtr.Zero)
        id_load_Ljava_io_InputStream_arrayC = Android.Runtime.JNIEnv.GetMethodID(KeyStore.Class.Handle, "load", "(Ljava/io/InputStream;[C)V");

      var intPtr = IntPtr.Zero;
      var intPtr2 = Android.Runtime.JNIEnv.NewArray(password);
      Android.Runtime.JNIEnv.CallVoidMethod(KeyStore.Handle, id_load_Ljava_io_InputStream_arrayC, new Android.Runtime.JValue[]
			{
				new Android.Runtime.JValue (intPtr),
				new Android.Runtime.JValue (intPtr2)
			});
      Android.Runtime.JNIEnv.DeleteLocalRef(intPtr);
      if (password != null)
      {
        Android.Runtime.JNIEnv.CopyArray(intPtr2, password);
        Android.Runtime.JNIEnv.DeleteLocalRef(intPtr2);
      }
    }

    private readonly Android.Content.Context Context;
    private readonly string ServiceId;
    private readonly Java.Security.KeyStore.PasswordProtection PasswordProtection;
    private Java.Security.KeyStore KeyStore;

    private const string FileName = "Inv.Vault";
    private static readonly object FileLock = new object();
    private static readonly char[] FilePassword = "3295043EA18CA264B2C40E0B72051DEF2D07AD2B4593F43DDDE1515A7EC32617".ToCharArray();
    private static IntPtr id_load_Ljava_io_InputStream_arrayC;

    private sealed class SecretKey : Java.Lang.Object, Javax.Crypto.ISecretKey
    {
      public SecretKey(Secret Secret)
      {
        this.bytes = System.Text.Encoding.UTF8.GetBytes(Secret.Serialize());
      }
      public byte[] GetEncoded() => bytes;
      public string Algorithm => "RAW";
      public string Format => "RAW";

      private readonly byte[] bytes;
    }
  }
}
