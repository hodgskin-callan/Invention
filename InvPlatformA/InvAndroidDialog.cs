using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  internal sealed class AndroidDateTimePickerFragment : Android.App.DialogFragment
  {
    public AndroidDateTimePickerFragment()
    {
      var Now = DateTime.Now;

      this.SelectedDate = new DateTime(Now.Year, Now.Month, Now.Day, Now.Hour, Now.Minute, 0);
    }

    public static readonly string TAG = "X:" + typeof(AndroidDateTimePickerFragment).Name.ToUpper();

    public DateTime SelectedDate { get; set; }
    public event Action SelectEvent;
    public event Action CancelEvent;
    public event Action FinishEvent;
    public event Action<Exception> FaultEvent;

    public override Android.App.Dialog OnCreateDialog(Android.OS.Bundle savedInstanceState)
    {
#if DEBUG
      //throw new Exception("Fatal crash testing in OnCreateDialog.");
#endif

      try
      {
        this.DismissEventInvoked = false;

        this.DatePicker = new Android.Widget.DatePicker(Context);
        DatePicker.Init(SelectedDate.Year, SelectedDate.Month - 1, SelectedDate.Day, null);

        this.TimePicker = new Android.Widget.TimePicker(Context);
        TimePicker.Hour = SelectedDate.Hour;
        TimePicker.Minute = SelectedDate.Minute;

        var LinearLayout = new Android.Widget.LinearLayout(Activity);
        LinearLayout.Orientation = Android.Widget.Orientation.Vertical;
        LinearLayout.AddView(DatePicker);
        LinearLayout.AddView(TimePicker);

        using (var Builder = new Android.App.AlertDialog.Builder(Activity))
        {
          Builder.SetView(LinearLayout);
          Builder.SetNegativeButton("Cancel", delegate { CancelInvoke(); });
          Builder.SetPositiveButton("OK", delegate { SelectInvoke(); });

          return Builder.Create();
        }
      }
      catch (Exception Exception)
      {
        if (System.Diagnostics.Debugger.IsAttached)
          System.Diagnostics.Debugger.Break();

        FaultEvent?.Invoke(Exception.Preserve());

        return null;
      }
    }
    public override void OnDismiss(Android.Content.IDialogInterface Dialog)
    {
      base.OnDismiss(Dialog);
      
      CancelInvoke();

      FinishEvent?.Invoke();
    }

    private void SelectInvoke()
    {
      this.SelectedDate = new DateTime(DatePicker.Year, DatePicker.Month + 1, DatePicker.DayOfMonth, TimePicker.Hour, TimePicker.Minute, 0);

      if (!DismissEventInvoked)
      {
        SelectEvent?.Invoke();

        DismissEventInvoked = true;
      }
    }
    private void CancelInvoke()
    {
      if (!DismissEventInvoked)
      {
        CancelEvent?.Invoke();

        DismissEventInvoked = true;
      }
    }

    private bool DismissEventInvoked;
    private Android.Widget.DatePicker DatePicker;
    private Android.Widget.TimePicker TimePicker;
  }

  // SOURCE: http://developer.xamarin.com/guides/android/user_interface/date_picker/
  internal sealed class AndroidDatePickerFragment : Android.App.DialogFragment, AndroidDatePickerDialog.IOnDateSetListener
  {
    public AndroidDatePickerFragment()
    {
      this.SelectedDate = DateTime.Now.Date;
    }

    // TAG can be any string of your choice.
    public static readonly string TAG = "X:" + typeof(AndroidDatePickerFragment).Name.ToUpper();

    // Initialize this value to prevent NullReferenceExceptions.
    public DateTime SelectedDate { get; set; }
    public event Action SelectEvent;
    public event Action CancelEvent;
    public event Action FinishEvent;
    public event Action<Exception> FaultEvent;

    public override Android.App.Dialog OnCreateDialog(Android.OS.Bundle savedInstanceState)
    {
      try
      {
        // NOTE: monthOfYear is a value between 0 and 11.
        var Result = new AndroidDatePickerDialog(Activity, this, SelectedDate.Year, SelectedDate.Month - 1, SelectedDate.Day);
        Result.CancelEvent += () => CancelInvoke();
        Result.DismissEvent += () => CancelInvoke();
        return Result;
      }
      catch (Exception Exception)
      {
        if (System.Diagnostics.Debugger.IsAttached)
          System.Diagnostics.Debugger.Break();

        FaultEvent?.Invoke(Exception.Preserve());

        return null;
      }
    }
    public override void OnDismiss(Android.Content.IDialogInterface dialog)
    {
      base.OnDismiss(dialog);

      FinishEvent?.Invoke();
    }

    private void CancelInvoke()
    {
      CancelEvent?.Invoke();
    }

    void Android.App.DatePickerDialog.IOnDateSetListener.OnDateSet(Android.Widget.DatePicker view, int year, int monthOfYear, int dayOfMonth)
    {
      // NOTE: monthOfYear is a value between 0 and 11.
      this.SelectedDate = new DateTime(year, monthOfYear + 1, dayOfMonth);

      SelectEvent?.Invoke();
    }
  }

  internal sealed class AndroidDatePickerDialog : Android.App.DatePickerDialog
  {
    public AndroidDatePickerDialog(Android.Content.Context context, AndroidDatePickerDialog.IOnDateSetListener listener, int year, int monthOfYear, int dayOfMonth)
      : base(context, listener, year, monthOfYear, dayOfMonth)
    {
    }

    public new event Action CancelEvent;
    public new event Action DismissEvent;

    public override void Cancel()
    {
      base.Cancel();

      CancelEvent?.Invoke();
    }
    public override void Dismiss()
    {
      base.Dismiss();

      DismissEvent?.Invoke();
    }
  }

  internal sealed class AndroidTimePickerFragment : Android.App.DialogFragment, AndroidTimePickerDialog.IOnTimeSetListener
  {
    public AndroidTimePickerFragment()
    {
      this.SelectedTime = DateTime.Now.Date;
    }

    // TAG can be any string of your choice.
    public static readonly string TAG = "X:" + typeof(AndroidTimePickerFragment).Name.ToUpper();

    // Initialize this value to prevent NullReferenceExceptions.
    public DateTime SelectedTime { get; set; }
    public event Action SelectEvent;
    public event Action CancelEvent;
    public event Action FinishEvent;
    public event Action<Exception> FaultEvent;

    public override Android.App.Dialog OnCreateDialog(Android.OS.Bundle savedInstanceState)
    {
      try
      {
        // NOTE: monthOfYear is a value between 0 and 11.
        var Result = new AndroidTimePickerDialog(Activity, this, SelectedTime.Hour, SelectedTime.Minute);
        Result.CancelEvent += () => CancelInvoke();
        Result.DismissEvent += () => CancelInvoke();
        return Result;
      }
      catch (Exception Exception)
      {
        if (System.Diagnostics.Debugger.IsAttached)
          System.Diagnostics.Debugger.Break();

        FaultEvent?.Invoke(Exception.Preserve());

        return null;
      }
    }
    public override void OnDismiss(Android.Content.IDialogInterface dialog)
    {
      base.OnDismiss(dialog);

      FinishEvent?.Invoke();
    }

    private void CancelInvoke()
    {
      CancelEvent?.Invoke();
    }

    void Android.App.TimePickerDialog.IOnTimeSetListener.OnTimeSet(Android.Widget.TimePicker view, int hourOfDay, int minute)
    {
      // NOTE: monthOfYear is a value between 0 and 11.
      this.SelectedTime = DateTime.MinValue + new TimeSpan(hourOfDay, minute, 0);

      SelectEvent?.Invoke();
    }
  }

  internal sealed class AndroidTimePickerDialog : Android.App.TimePickerDialog
  {
    public AndroidTimePickerDialog(Android.Content.Context context, AndroidTimePickerDialog.IOnTimeSetListener listener, int hourOfDay, int minuteOfHour)
      : base(context, listener, hourOfDay, minuteOfHour, true)
    {
    }
    public new event Action CancelEvent;
    public new event Action DismissEvent;

    public override void Cancel()
    {
      base.Cancel();

      CancelEvent?.Invoke();
    }
    public override void Dismiss()
    {
      base.Dismiss();

      DismissEvent?.Invoke();
    }
  }
}