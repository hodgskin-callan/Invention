using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Subclass to include a simple splash screen of an image in your app.
  /// </summary>
  public abstract class AndroidSplashActivity : Android.App.Activity
  {
    public Inv.Colour BackgroundColour
    {
      set => Window.DecorView.SetBackgroundColor(AndroidEngine.TranslateGraphicsColor(value ?? Inv.Colour.Black));
    }
    /// <summary>
    /// Set this to an Id in your resources.
    /// </summary>
    public int ImageResourceId { get; protected set; }
    /// <summary>
    /// Set this to the type of the main app activity.
    /// </summary>
    public Type LaunchActivity { get; protected set; }

    /// <summary>
    /// Implement this method to set <see cref="ImageResourceId"/> for the splash image, <see cref="LaunchActivity"/> for the main activity.
    /// </summary>
    protected abstract void Install();

    /// <summary>
    /// Called when the activity is starting.
    /// </summary>
    /// <param name="bundle"></param>
    protected override void OnCreate(Android.OS.Bundle bundle)
    {
      if (Inv.Assert.IsEnabled)
      {
        var AndroidActivityAttribute = GetType().CustomAttributes.Find(A => A.AttributeType == typeof(Android.App.ActivityAttribute));
        var MainLauncherParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "MainLauncher");

        Inv.Assert.Check(
          //MainLauncherParameter != null && 
          MainLauncherParameter.TypedValue.Value != null, "[Activity] attribute must include MainLauncher parameter.");
      }

      RequestWindowFeature(Android.Views.WindowFeatures.NoTitle);
      base.OnCreate(bundle);

      Install();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(ImageResourceId != 0, "ImageResourceId must be specified in the Install method.");
        Inv.Assert.Check(LaunchActivity != null, "LaunchActivity must be specified in the Install method.");
      }

      if (AndroidFoundation.SdkVersion >= 31) // Android 12+
      {
        // NOTE: Starting in Android 12, the system applies the Android system default splash screen on cold and warm starts for all apps.
        // By default, this system splash screen is constructed using your app's launcher icon element and the windowBackground of your theme, if it's a single color.

        // launch the main activity.
        StartActivity(LaunchActivity);

        // terminate the splash activity without displaying anything.
        Finish();
      }
      else
      {
        var RelativeLayout = new Android.Widget.RelativeLayout(this);
        SetContentView(RelativeLayout);
        RelativeLayout.LayoutParameters = new Android.Widget.FrameLayout.LayoutParams(Android.Widget.FrameLayout.LayoutParams.MatchParent, Android.Widget.FrameLayout.LayoutParams.MatchParent)
        {
          Gravity = Android.Views.GravityFlags.Fill
        };
        var ImageView = new Android.Widget.ImageView(this);
        RelativeLayout.AddView(ImageView);
        ImageView.SetImageResource(ImageResourceId);
        var ImageLP = new Android.Widget.RelativeLayout.LayoutParams(Android.Widget.RelativeLayout.LayoutParams.WrapContent, Android.Widget.RelativeLayout.LayoutParams.WrapContent);
        ImageView.LayoutParameters = ImageLP;
        ImageLP.AddRule(Android.Widget.LayoutRules.CenterInParent);

        var FadeIn = new Android.Views.Animations.AlphaAnimation(0, 1);
        FadeIn.Interpolator = new Android.Views.Animations.DecelerateInterpolator();
        FadeIn.Duration = 1000;

        var FadeOut = new Android.Views.Animations.AlphaAnimation(1, 0);
        FadeOut.Interpolator = new Android.Views.Animations.AccelerateInterpolator();
        FadeOut.StartOffset = 1000;
        FadeOut.Duration = 1000;

        var Animation = new Android.Views.Animations.AnimationSet(false);
        Animation.AddAnimation(FadeIn);
        Animation.AddAnimation(FadeOut);
        Animation.AnimationEnd += (Sender, Event) => RelativeLayout.Visibility = Android.Views.ViewStates.Invisible;

        RelativeLayout.Animation = Animation;

        System.Threading.Tasks.Task.Run(() =>
        {
          // give time to show the splash animation.
          System.Threading.Thread.Sleep(1000);

          // launch the main activity.
          StartActivity(LaunchActivity);

          // terminate the splash activity.
          Finish();
        });
      }
    }
  }
}