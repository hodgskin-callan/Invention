﻿namespace Inv.Database.Test
{
  [Android.App.Activity(Label = "InvDatabaseTest", Theme = "@android:style/Theme.NoTitleBar", MainLauncher = true, NoHistory = true)]
  public sealed class SplashActivity : Inv.AndroidSplashActivity
  {
    protected override void Install()
    {
      this.ImageResourceId = InvDatabaseTestA.Resource.Drawable.Icon;
      this.LaunchActivity = typeof(MainActivity);
    }
  }

  [Android.App.Activity(Label = "InvDatabaseTest", MainLauncher = false, Icon = "@drawable/icon", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
  public sealed class MainActivity : Inv.AndroidActivity
  {
    protected override void Install(Inv.Application Application)
    {
      this.ShowStatusBar = true;
      this.ShowNavigationBar = true;

      var Folder = Application.Directory.Root.NewFolder(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal));
      
      var PlatformList = new Inv.DistinctList<Inv.Database.Platform>();
      PlatformList.Add(new SQLite.Platform(Folder.NewFile("InvDatabaseTest.db")));

      Shell.Install(Application, PlatformList);
    }
  }
}