﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inv
{
  public static class ServiceModelExceptionHandler
  {
    static ServiceModelExceptionHandler()
    {
      if (System.ServiceModel.Dispatcher.ExceptionHandler.AsynchronousThreadExceptionHandler != null)
        throw new Exception("AsynchronousThreadExceptionHandler has already been set.");

      System.ServiceModel.Dispatcher.ExceptionHandler.AsynchronousThreadExceptionHandler = new AsyncExceptionHandler();
    }

    public static event Action<Exception> FaultEvent;

    private sealed class AsyncExceptionHandler : System.ServiceModel.Dispatcher.ExceptionHandler
    {
      internal AsyncExceptionHandler()
        : base()
      {
      }

      public override bool HandleException(Exception exception)
      {
        try
        {
          var LocalDelegate = FaultEvent;

          if (LocalDelegate != null)
            LocalDelegate(exception);
          else
            return false;
        }
        catch
        {
          return false;
        }

        return true; // TRUE means this exception was handled and don't terminate the process.
      }
    }
  }
}