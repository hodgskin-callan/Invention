﻿#if DEBUG
//#define DYNAMIC_LIBRARY_TRACE
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Reflection.Emit;
using System.Reflection;
using System.Threading;
using Inv.Support;

namespace Inv
{
  internal sealed class DynamicLibraryEntry
  {
    internal DynamicLibraryEntry()
    {
      this.ImportDelegateDictionary = new Dictionary<long, object>();
      this.ExportDelegateDictionary = new Dictionary<int, Delegate>();
      this.ExportWrapperDelegateDictionary = new Dictionary<int, Delegate>();
    }

    internal void Clear()
    {
      this.ImportDelegateDictionary.Clear();
      this.ExportDelegateDictionary.Clear();
      this.ExportWrapperDelegateDictionary.Clear();
    }

    public Dictionary<Int64, object> ImportDelegateDictionary;
    public Dictionary<int, Delegate> ExportDelegateDictionary;
    public Dictionary<int, Delegate> ExportWrapperDelegateDictionary;

    public Delegate GetExportDelegate(int Value)
    {
      var Result = ExportDelegateDictionary.GetValueOrDefault(Value);

      if (Result == null)
        throw new Exception($"Unable to find exported delegate ({Value})");

      return Result;
    }
    public object GetImportDelegate(Int64 Value)
    {
      var Result = ImportDelegateDictionary.GetValueOrDefault(Value);

      if (Result == null)
        throw new Exception($"Unable to find imported delegate ({Value})");

      return Result;
    }

    static DynamicLibraryEntry()
    {
      GetExportDelegateMethod = typeof(DynamicLibraryEntry).GetMethod(nameof(DynamicLibraryEntry.GetExportDelegate), BindingFlags.Public | BindingFlags.Instance, null, new Type[] { typeof(int) }, null);
      GetImportDelegateMethod = typeof(DynamicLibraryEntry).GetMethod(nameof(DynamicLibraryEntry.GetImportDelegate), BindingFlags.Public | BindingFlags.Instance, null, new Type[] { typeof(Int64) }, null);
    }

    internal static readonly MethodInfo GetExportDelegateMethod;
    internal static readonly MethodInfo GetImportDelegateMethod;
  }

  public sealed class DynamicLibrary
  {
    public DynamicLibrary()
    {
      this.Handle = IntPtr.Zero;
      this.GetHRForExceptionMethod = typeof(Inv.DynamicLibrary).GetMethod(nameof(GetHRForException), BindingFlags.Static | BindingFlags.NonPublic, null, new Type[] { typeof(Exception) }, null);
      this.CheckExceptionForHResultMethod = typeof(Inv.DynamicLibrary).GetMethod(nameof(CheckExceptionForHResult), BindingFlags.Static | BindingFlags.NonPublic, null, new Type[] { typeof(Int32) }, null);
    }

    public IntPtr Handle { get; private set; }
    public bool IsActive => Handle != IntPtr.Zero;

    public void Open(string Name)
    {
      if (!TryOpen(Name))
        throw new System.ComponentModel.Win32Exception(Marshal.GetLastWin32Error(), string.Format("Could not load library '{0}'.", Name)); 
    }
    public bool TryOpen(string Name)
    {
      Debug.Assert(!IsActive, "Library must not be active.");

      this.Name = Name;
      this.Handle = Win32.Kernel32.LoadLibrary(Name);

      var Result = IsActive;

      if (Result)
      {
        this.LibraryReferenceKey = this.Name + this.Handle.ToString("X") + this.GetHashCode().ToString("X");

        using (CriticalSection.Lock())
          LibraryEntryDictionary.Add(this.LibraryReferenceKey, new DynamicLibraryEntry());

        WriteTraceLine($"DYNAMIC LIBRARY OPENED [{LibraryReferenceKey}]");
      }

      return Result;
    }
    public void Close()
    {
      Debug.Assert(IsActive, "Library must be active.");

      // KJV 2021-05-06
      // Note: there is a known issue that calling Win32.Kernel32.FreeLibrary hangs, even in trivial cases.
      // Research is required to determine why this is occurring and to resolve so that DLLs can be unloaded cleanly.
      
      // if (!Win32.Kernel32.FreeLibrary(Handle))
      //   throw new System.ComponentModel.Win32Exception(Marshal.GetLastWin32Error(), string.Format("Could not free library '{0}'.", Name));

      WriteTraceLine($"DYNAMIC LIBRARY CLOSED [{LibraryReferenceKey}]");
      
      this.Handle = IntPtr.Zero;
      this.Name = null;

      using (CriticalSection.Lock())
      {
        if (LibraryEntryDictionary.ContainsKey(this.LibraryReferenceKey))
        {
          var Entry = LibraryEntryDictionary.RemoveValueOrDefault(this.LibraryReferenceKey);

          Debug.Assert(Entry != null, "Entry must be found for this key: " + LibraryReferenceKey);

          if (Entry != null)
            Entry.Clear();
        }
      }
    }
    public TDelegate ImportDelegate<TDelegate>(string Name, bool UseMarshal = false, bool UseLogging = true)
      where TDelegate : class
    {
      Debug.Assert(IsActive, "Library must be active.");

      CheckMarshalling(Name, typeof(TDelegate).GetMethod("Invoke"));

      var Address = Win32.Kernel32.GetProcAddress(Handle, Name);
      if (Address == IntPtr.Zero)
        throw new ApplicationException(string.Format("Could not load delegate '{0}' in library '{1}'.", Name, this.Name));

      if (UseMarshal)
        return (TDelegate)(object)Marshal.GetDelegateForFunctionPointer(Address, typeof(TDelegate));

      return (TDelegate)(object)GenerateWrappedDelegateForFunctionPointer<TDelegate>(Name, Address, UseLogging);
    }
    public IntPtr ExportDelegate(string Name, Delegate Delegate, bool UseLogging = true)
    {
      if (Delegate == null)
        return IntPtr.Zero;

      var LibraryEntry = GetLibraryEntry();
      var UniqueToken = Interlocked.Increment(ref UniqueTokenSeed);
      LibraryEntry.ExportDelegateDictionary[UniqueToken] = Delegate;

      var Identifier = $"({LibraryReferenceKey}.{Name}) [{UniqueToken}]";

      WriteTraceLine($"DYNAMIC LIBRARY EXPORT REGISTERED {Identifier}");

      var WrapperDelegateType = WrapDelegate(Delegate.GetType());

      var DelegateType = Delegate.GetType();
      var DelegateMethod = DelegateType.GetMethod("Invoke");
      var DelegateParameters = DelegateMethod.GetParameters();

      CheckMarshalling(Name, DelegateMethod);

      var HasReturnType = DelegateMethod.ReturnType != typeof(void);

      Type[] WrapperParameterTypeArray;
      if (HasReturnType)
      {
        WrapperParameterTypeArray = new Type[DelegateParameters.Count() + 1];
        Array.Copy(DelegateParameters.Select(x => x.ParameterType).ToArray(), WrapperParameterTypeArray, DelegateParameters.Count());
        WrapperParameterTypeArray[DelegateParameters.Count()] = DelegateMethod.ReturnType.MakeByRefType();
      }
      else
      {
        WrapperParameterTypeArray = DelegateParameters.Select(x => x.ParameterType).ToArray();
      }

      var WrapperDelegate = new DynamicMethod("OutboundWrapper" + Delegate.GetType().FullName.Replace('.', '_') + UniqueToken, typeof(UInt32), WrapperParameterTypeArray, typeof(Inv.DynamicLibrary));

      // Make sure that our parameters are defined equivalently to the user-visible delegate definition
      WrapperDelegate.DefineParameter(0, ParameterAttributes.Retval, "hr");
      for (var ParameterIndex = 0; ParameterIndex < DelegateParameters.Count(); ParameterIndex++)
      {
        var ParameterInfo = DelegateParameters[ParameterIndex];
        WrapperDelegate.DefineParameter(ParameterIndex + 1, ParameterInfo.Attributes, ParameterInfo.Name);
      }

      if (HasReturnType)
        WrapperDelegate.DefineParameter(DelegateParameters.Count() + 1, ParameterAttributes.Out, "retval");

      // Build the method body
      var ILGenerator = WrapperDelegate.GetILGenerator();
      var ResultLocal = ILGenerator.DeclareLocal(typeof(Int32));
      var ReturnLocal = HasReturnType ? ILGenerator.DeclareLocal(DelegateMethod.ReturnType) : null;

      if (IsTracing() && UseLogging)
      {
        ILGenerator.PushString($"DYNAMIC LIBRARY EXPORT CALLED {Identifier}");
        ILGenerator.CallMethod(DebugWriteLineMethod);
      }

      var TryBlock = ILGenerator.BeginExceptionBlock();

      ILGenerator.PushStaticField(LibraryEntryDictionaryField);
      ILGenerator.PushString(this.LibraryReferenceKey);
      ILGenerator.CallMethod(LibraryEntryDictionaryGetItemMethod);
      ILGenerator.PushInt32(UniqueToken);
      ILGenerator.CallMethod(DynamicLibraryEntry.GetExportDelegateMethod);
      ILGenerator.CastToType(DelegateType);

      var OutParameterList = new List<Tuple<int, LocalBuilder>>();
      for (var ParameterIndex = 0; ParameterIndex < DelegateParameters.Count(); ParameterIndex++)
      {
        var DelegateParameter = DelegateParameters[ParameterIndex];

        // Out parameters are passed by reference.
        if (DelegateParameter.IsOut)
        {
          var OutParameter = new Tuple<int, LocalBuilder>(ParameterIndex, ILGenerator.DeclareLocal(DelegateParameter.ParameterType.GetElementType()));
          OutParameterList.Add(OutParameter);

          ILGenerator.PushLocalVariableAddress(OutParameter.Item2);
        }
        else
        {
          ILGenerator.PushArgument(ParameterIndex);
        }
      }

      // Call the native method
      ILGenerator.CallLateBoundMethod(DelegateMethod);

      if (HasReturnType)
      {
        ILGenerator.PopLocalVariable(ReturnLocal);

        ILGenerator.PushArgument(DelegateParameters.Count());
        ILGenerator.PushLocalVariable(ReturnLocal);

        ILGenerator.PopToAddress(DelegateMethod.ReturnType);
      }

      // Resolve all the 'out' variables into their correct location
      foreach (var OutParameter in OutParameterList)
      {
        var OutParameterType = OutParameter.Item2.LocalType;

        ILGenerator.PushArgument(OutParameter.Item1);
        ILGenerator.PushLocalVariable(OutParameter.Item2);

        ILGenerator.PopToAddress(OutParameterType);
      }

      ILGenerator.PushInt32(0);
      ILGenerator.PopLocalVariable(ResultLocal);

      ILGenerator.Leave(TryBlock);
      ILGenerator.BeginCatchBlock(ExceptionType);

      ILGenerator.CallMethod(GetHRForExceptionMethod);

      ILGenerator.PopLocalVariable(ResultLocal);
      ILGenerator.Leave(TryBlock);

      ILGenerator.EndExceptionBlock();

      ILGenerator.PushLocalVariable(ResultLocal);
      ILGenerator.Return();

      var GeneratedDelegate = WrapperDelegate.CreateDelegate(WrapperDelegateType);

      // Keep a reference around that should remain valid so long as this library is loaded.
      LibraryEntry.ExportWrapperDelegateDictionary.Add(UniqueToken, GeneratedDelegate);

      return Marshal.GetFunctionPointerForDelegate(GeneratedDelegate);
    }

    private DynamicLibraryEntry GetLibraryEntry()
    {
      using (CriticalSection.Lock())
      {
        if (!LibraryEntryDictionary.TryGetValue(this.LibraryReferenceKey, out var Result))
        {
          Result = new DynamicLibraryEntry();
          LibraryEntryDictionary.Add(this.LibraryReferenceKey, Result);
        }

        return Result;
      }
    }
    // Mangle the method signature of the user-supplied delegate in a manner similar to P/Invoke with PreserveSig=false
    // or COM interop. Creates a delegate with the specified signature for a given unmanaged function pointer by creating an 'inner'
    // delegate with a mangled signature, and an 'outer' delegate which calls it.
    private TDelegate GenerateWrappedDelegateForFunctionPointer<TDelegate>(string DelegateName, IntPtr FunctionPointer, bool UseLogging)
      where TDelegate : class
    {
      var LibraryEntry = GetLibraryEntry();

      var Identifier = $"({LibraryReferenceKey}.{DelegateName}) [{FunctionPointer.ToInt64()}]";

      var FunctionPointerWrapperType = WrapDelegate(typeof(TDelegate));
      LibraryEntry.ImportDelegateDictionary[FunctionPointer.ToInt64()] = (object)Marshal.GetDelegateForFunctionPointer(FunctionPointer, FunctionPointerWrapperType);

      WriteTraceLine($"DYNAMIC LIBRARY IMPORT REGISTERED {Identifier}");

      var DelegateType = typeof(TDelegate);
      var DelegateMethod = DelegateType.GetMethod("Invoke");
      var DelegateParameters = DelegateMethod.GetParameters();
      var DelegateParameterTypeArray = DelegateParameters.Select(x => x.ParameterType).ToArray();
      
      var WrapperDelegate = new DynamicMethod("Wrapper" + DelegateType.FullName.Replace('.', '_') + "_" + FunctionPointer.ToString("X"), DelegateMethod.ReturnType, DelegateParameterTypeArray, this.GetType());
      for (var ParameterIndex = 0; ParameterIndex < DelegateParameters.Count(); ParameterIndex++)
      {
        var Parameter = DelegateParameters[ParameterIndex];
        WrapperDelegate.DefineParameter(ParameterIndex, Parameter.Attributes, Parameter.Name);
      }

      // Get the collection of function pointer delegates, and fetch the one we're interested in. Cast it to our previously-generated delegate type.
      // This whole chunk here is basically (with 'libraryname' and 0x12345678 being set at runtime, but before the delegate itself is created)
      // var theDelegate = (TDelegate)Inv.DynamicLibrary.LibraryReferenceDictionary["libraryname"].ImportDelegateDictionary[0x12345678];
      var ILGenerator = WrapperDelegate.GetILGenerator();

      if (IsTracing() && UseLogging)
      {
        ILGenerator.PushString($"DYNAMIC LIBRARY IMPORT CALLED {Identifier}");
        ILGenerator.CallMethod(DebugWriteLineMethod);
      }

      ILGenerator.PushStaticField(LibraryEntryDictionaryField);
      ILGenerator.PushString(this.LibraryReferenceKey);
      ILGenerator.CallMethod(LibraryEntryDictionaryGetItemMethod);
      ILGenerator.PushInt64(FunctionPointer.ToInt64());
      ILGenerator.CallMethod(DynamicLibraryEntry.GetImportDelegateMethod);
      ILGenerator.CastToType(FunctionPointerWrapperType);

      var OutParameterList = new List<Tuple<int, LocalBuilder>>();
      for (var ParameterIndex = 0; ParameterIndex < DelegateParameters.Count(); ParameterIndex++)
      {
        var DelegateParameter = DelegateParameters[ParameterIndex];

        if (DelegateParameter.IsOut)
        {
          var OutParameter = new Tuple<int, LocalBuilder>(ParameterIndex, ILGenerator.DeclareLocal(DelegateParameter.ParameterType.GetElementType()));
          OutParameterList.Add(OutParameter);

          ILGenerator.PushLocalVariableAddress(OutParameter.Item2);
        }
        else
        {
          ILGenerator.PushArgument(ParameterIndex);
        }
      }

      // And the return value, if there is one.
      var ReturnLocal = DelegateMethod.ReturnType != typeof(void) ? ILGenerator.DeclareLocal(DelegateMethod.ReturnType) : null;
      if (ReturnLocal != null)
        ILGenerator.PushLocalVariableAddress(ReturnLocal);

      // Call the native method
      ILGenerator.CallLateBoundMethod(FunctionPointerWrapperType.GetMethod("Invoke"));

      // Check the HRESULT and throw an exception if necessary
      ILGenerator.CallMethod(CheckExceptionForHResultMethod);

      foreach (var OutParameter in OutParameterList)
      {
        var ParameterType = OutParameter.Item2.LocalType;

        ILGenerator.PushArgument(OutParameter.Item1);
        ILGenerator.PushLocalVariable(OutParameter.Item2);

        ILGenerator.PopToAddress(ParameterType);
      }

      // If there's a return value, push it to the stack and return it.
      if (DelegateMethod.ReturnType != typeof(void))
        ILGenerator.PushLocalVariable(ReturnLocal);

      ILGenerator.Return();

      return (TDelegate)(object)WrapperDelegate.CreateDelegate(typeof(TDelegate));
    }

    private bool IsTracing()
    {
#if DYNAMIC_LIBRARY_TRACE
      return true;
#else
      return false;
#endif
    }
    [Conditional("DYNAMIC_LIBRARY_TRACE")]
    private void WriteTraceLine(string Message)
    {
      Debug.WriteLine(Message);
    }
    [Conditional("DEBUG")]
    private void CheckMarshalling(string Name, MethodInfo MethodInfo)
    {
      var StringBuilder = new StringBuilder();

      void CheckStruct(string Path, Type StructType)
      {
        if (StructType.IsValueType && !StructType.IsPrimitive && !StructType.IsEnum)
        {
          var StructLayoutAttribute = StructType.StructLayoutAttribute;
          if (StructLayoutAttribute == null || StructLayoutAttribute.Value != LayoutKind.Sequential || StructLayoutAttribute.CharSet != CharSet.Ansi || StructLayoutAttribute.Pack != 1)
            StringBuilder.AppendLine($"Delegate '{Name}' has struct parameter '{Path}' missing [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]");

          foreach (var Field in StructType.GetFields())
            CheckStruct($"{Path}.{Field.Name}", Field.FieldType);
        }
      }

      void CheckParameter(string ParameterName, ParameterInfo ParameterInfo)
      {
        if (ParameterInfo.ParameterType.FullName.ExcludeAfter("&").Equals(typeof(bool).FullName, StringComparison.InvariantCultureIgnoreCase))
        {
          var IsMarshalled = false;
          foreach (MarshalAsAttribute MarshalAsAttribute in ParameterInfo.GetCustomAttributes(typeof(MarshalAsAttribute)))
          {
            if (MarshalAsAttribute.Value == UnmanagedType.U1)
            {
              IsMarshalled = true;
              break;
            }
          }
          if (!IsMarshalled)
            StringBuilder.AppendLine($"Delegate '{Name}' has bool parameter '{ParameterName}' missing [MarshalAs(UnmanagedType.U1)] attribute");
        }

        CheckStruct(ParameterName, ParameterInfo.ParameterType);
      }

      foreach (var Parameter in MethodInfo.GetParameters())
        CheckParameter(Parameter.Name, Parameter);

      if (MethodInfo.ReturnParameter != null && MethodInfo.ReturnParameter.ParameterType != typeof(void))
        CheckParameter("Return", MethodInfo.ReturnParameter);

      if (StringBuilder.Length > 0)
        throw new Exception(StringBuilder.ToString());
    }

    private string Name;
    private string LibraryReferenceKey;
    // These can't be static as they cannot be reflected during static construction.
    private readonly MethodInfo GetHRForExceptionMethod;
    private readonly MethodInfo CheckExceptionForHResultMethod;

    static DynamicLibrary()
    {
      CriticalSection = new Inv.ExclusiveCriticalSection("Inv.DynamicLibrary");
      LibraryEntryDictionary = new Dictionary<string, DynamicLibraryEntry>();
      LibraryEntryDictionaryField = typeof(Inv.DynamicLibrary).GetField(nameof(LibraryEntryDictionary), BindingFlags.NonPublic | BindingFlags.Static);
      LibraryEntryDictionaryGetItemMethod = LibraryEntryDictionary.GetType().GetMethod("get_Item", new Type[] { typeof(string) });
      GeneratedWrapDelegateDictionary = new Dictionary<Type, Type>();
      ExceptionType = typeof(Exception);
      DebugWriteLineMethod = typeof(Debug).GetMethod("WriteLine", new Type[] { typeof(string) });
    }

    /// <summary>
    /// Create a new delegate type that wraps the specified delegate with a HRESULT, correctly
    /// rewriting return values into out parameters
    ///
    /// i.e.
    /// delegate int SomeDelegate(bool a, out string b)
    /// ->
    /// delegate HRESULT SomeDelegateWrapper(bool a, out string b, out int retval);
    ///
    /// </summary>
    /// <param name="delegateType">The delegate type you wish to wrap</param>
    /// <returns>A Type corresponding to the wrapper delegate.</returns>
    private static Type WrapDelegate(Type delegateType)
    {
      if (GeneratedWrapDelegateDictionary.ContainsKey(delegateType))
        return GeneratedWrapDelegateDictionary[delegateType];

      var DelegateMethod = delegateType.GetMethod("Invoke");
      var DelegateParameters = DelegateMethod.GetParameters();
      var ReturnType = DelegateMethod.ReturnType;

      if (GeneratedAssembly == null)
      {
        var AssemblyName = new AssemblyName("InvDynamicLibraryWrapper");
        AssemblyName.Version = new System.Version(1, 0, 0, 0);
        GeneratedAssembly = AppDomain.CurrentDomain.DefineDynamicAssembly(AssemblyName, AssemblyBuilderAccess.RunAndSave);
        GeneratedModule = GeneratedAssembly.DefineDynamicModule("GeneratedWrappers", true);
      }

      var TypeBuilder = GeneratedModule.DefineType(
        "Wrapper" + delegateType.FullName.Replace('.', '_'), 
        TypeAttributes.Class | TypeAttributes.Public | TypeAttributes.Sealed | TypeAttributes.AnsiClass | TypeAttributes.AutoClass,
        typeof(System.MulticastDelegate)
      );

      var ConstructorBuilder = TypeBuilder.DefineConstructor(MethodAttributes.RTSpecialName | MethodAttributes.HideBySig | MethodAttributes.Public, CallingConventions.Standard, Array.Empty<Type>());
      ConstructorBuilder.SetImplementationFlags(MethodImplAttributes.Runtime | MethodImplAttributes.Managed);

      // Reserve an extra parameter if we need to pass an [out] return value.
      var ParameterTypeArray = new Type[DelegateParameters.Length + (ReturnType != typeof(void) ? 1 : 0)];

      for (var ParameterIndex = 0; ParameterIndex < DelegateParameters.Length; ParameterIndex++)
      {
        var ParameterType = DelegateParameters[ParameterIndex].ParameterType;
        ParameterTypeArray[ParameterIndex] = ParameterType;
      }

      // If the original delegate has a return value, that gets pushed to the end.
      if (ReturnType != typeof(void))
        ParameterTypeArray[ParameterTypeArray.Length - 1] = ReturnType.MakeByRefType();

      // Define the Invoke method for the delegate
      var InvokeMethodBuilder = TypeBuilder.DefineMethod(
        "Invoke", 
        MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.NewSlot | MethodAttributes.Virtual,
        typeof(UInt32),
        ParameterTypeArray
      );

      InvokeMethodBuilder.DefineParameter(0, ParameterAttributes.Retval, "hr");

      for (var ParameterIndex = 0; ParameterIndex < DelegateParameters.Length; ParameterIndex++)
      {
        var DelegateParameter = DelegateParameters[ParameterIndex];

        var ParameterBuilder = InvokeMethodBuilder.DefineParameter(ParameterIndex + 1, DelegateParameter.Attributes, DelegateParameter.Name);
        foreach (var Attribute in DelegateParameter.GetCustomAttributes(false).Where(x => x is MarshalAsAttribute).Select(x => x as MarshalAsAttribute))
        {
          var CloneAttribute = CloneMarshalAsAttribute(Attribute);
          ParameterBuilder.SetCustomAttribute(CloneAttribute);
        }
      }

      // Propagate MarshalAs attributes on the return type to the generated wrapper type
      if (ReturnType != typeof(void))
      {
        var ParameterBuilder = InvokeMethodBuilder.DefineParameter(DelegateParameters.Length + 1, ParameterAttributes.Out, "retval");
        foreach (var Attribute in DelegateMethod.ReturnParameter.GetCustomAttributes(false).Where(x => x is MarshalAsAttribute).Select(x => x as MarshalAsAttribute))
        {
          var CloneAttribute = CloneMarshalAsAttribute(Attribute);
          ParameterBuilder.SetCustomAttribute(CloneAttribute);
        }
      }

      // We don't provide an implementation, since it's effectively used as a template.
      InvokeMethodBuilder.SetImplementationFlags(MethodImplAttributes.Runtime | MethodImplAttributes.Managed);

      var AsType = TypeBuilder.CreateType();

      GeneratedWrapDelegateDictionary.Add(delegateType, AsType);

      return AsType;
    }
    private static CustomAttributeBuilder CloneMarshalAsAttribute(MarshalAsAttribute attr)
    {
      var attrType = typeof(MarshalAsAttribute);
      var constructor = attrType.GetConstructor(new Type[] { typeof(UnmanagedType) });
      var fieldNames = attrType.GetFields().ToArray();
      var fieldValues = fieldNames.Select(x => x.GetValue(attr)).ToArray();
      var newAttr = new CustomAttributeBuilder(constructor, new object[] { attr.Value }, fieldNames, fieldValues);
      return newAttr;
    }
    
    // Called from runtime-generated code.
    [DebuggerNonUserCode]
    private static void CheckExceptionForHResult(Int32 HRESULT)
    {
      // TODO: Fetch Delphi error messages and stack traces and such.
      var Exception = Marshal.GetExceptionForHR(HRESULT);

      if (Exception != null)
        throw Exception;

      if ((HRESULT & 0x8000000) == 0x8000000)
        throw new ApplicationException("Non-success value returned from Delphi code: " + HRESULT.ToString("X"));
    }
    [DebuggerNonUserCode]
    private static Int32 GetHRForException(Exception Exception)
    {
      if (Exception is ApplicationException)
      {
        var WrapException = new ApplicationException(Exception.Message + "\nStacktrace:\n" + Exception.StackTrace);
        Marshal.GetHRForException(WrapException);
        
        unchecked 
        { 
          return (Int32)0x80004005; 
        }
      }

      return Marshal.GetHRForException(Exception);
    }

    private static readonly Inv.ExclusiveCriticalSection CriticalSection;
    private static readonly Dictionary<string, DynamicLibraryEntry> LibraryEntryDictionary;
    private static readonly FieldInfo LibraryEntryDictionaryField;
    private static readonly MethodInfo LibraryEntryDictionaryGetItemMethod;
    private static readonly Type ExceptionType;
    private static readonly MethodInfo DebugWriteLineMethod;
    private static Dictionary<Type, Type> GeneratedWrapDelegateDictionary;
    private static AssemblyBuilder GeneratedAssembly;
    private static ModuleBuilder GeneratedModule;
    private static int UniqueTokenSeed;
  }

  [DebuggerNonUserCode]
  internal static class ILGeneratorHelper
  {
    internal static void PushLocalVariable(this ILGenerator ilGen, LocalBuilder local) => ilGen.Emit(OpCodes.Ldloc, local);
    internal static void PushLocalVariableAddress(this ILGenerator ilGen, LocalBuilder local) => ilGen.Emit(OpCodes.Ldloca, local);
    internal static void PushField(this ILGenerator ilGen, FieldInfo field) => ilGen.Emit(OpCodes.Ldfld, field);
    internal static void PushInt32(this ILGenerator ilGen, Int32 value) => ilGen.Emit(OpCodes.Ldc_I4, value);
    internal static void PushInt64(this ILGenerator ilGen, long value) => ilGen.Emit(OpCodes.Ldc_I8, value);
    internal static void PushString(this ILGenerator ilGen, string value) => ilGen.Emit(OpCodes.Ldstr, value);
    internal static void PushArgument(this ILGenerator ilGen, int arg)
    {
      switch (arg)
      {
        case 0:
          ilGen.Emit(OpCodes.Ldarg_0);
          break;

        case 1:
          ilGen.Emit(OpCodes.Ldarg_1);
          break;

        case 2:
          ilGen.Emit(OpCodes.Ldarg_2);
          break;

        case 3:
          ilGen.Emit(OpCodes.Ldarg_3);
          break;

        default:
          ilGen.Emit(OpCodes.Ldarg, arg);
          break;
      }
    }
    internal static void PushStaticField(this ILGenerator ilGen, FieldInfo field) => ilGen.Emit(OpCodes.Ldsfld, field);

    internal static void CallMethod(this ILGenerator ilGen, MethodInfo method) => ilGen.Emit(OpCodes.Call, method);
    internal static void CallLateBoundMethod(this ILGenerator ilGen, MethodInfo method) => ilGen.Emit(OpCodes.Callvirt, method);

    internal static void Return(this ILGenerator ilGen) => ilGen.Emit(OpCodes.Ret);

    internal static void PopLocalVariable(this ILGenerator ilGen, LocalBuilder local) => ilGen.Emit(OpCodes.Stloc, local);
    internal static void PopToAddress(this ILGenerator ilGen, Type variableType)
    {
      if (variableType == typeof(bool))
        ilGen.Emit(OpCodes.Stind_I1);
      else if (variableType == typeof(Int16) || variableType == typeof(UInt16))
        ilGen.Emit(OpCodes.Stind_I2);
      else if (variableType == typeof(Int32) || variableType == typeof(UInt32))
        ilGen.Emit(OpCodes.Stind_I4);
      else if (variableType == typeof(Int64) || variableType == typeof(UInt64))
        ilGen.Emit(OpCodes.Stind_I8);
      else if (variableType == typeof(Single) || variableType == typeof(float))
        ilGen.Emit(OpCodes.Stind_R4);
      else if (variableType == typeof(Double))
        ilGen.Emit(OpCodes.Stind_R8);
      else if (variableType.IsValueType || variableType.IsByRef)
        ilGen.Emit(OpCodes.Stobj, variableType);
      else
        ilGen.Emit(OpCodes.Stind_Ref);
    }

    internal static void Leave(this ILGenerator ilgen, System.Reflection.Emit.Label label) => ilgen.Emit(OpCodes.Leave, label);

    internal static void CastToType(this ILGenerator ilGen, Type type) => ilGen.Emit(OpCodes.Castclass, type);
  }
}
