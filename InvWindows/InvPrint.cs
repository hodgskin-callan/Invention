﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Drawing.Printing;
using System.Printing;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Represents a printer attached to the system.
  /// </summary>
  public sealed class Printer
  {
    /// <summary>
    /// Initialise a new <see cref="Printer"/> object with the given print settings.
    /// </summary>
    /// <param name="Name" />
    public Printer(string Name)
    {
      this.PrintSettings = new PrinterSettings()
      {
        PrinterName = Name
      };
    }

    /// <summary>
    /// Gets the full name of this <see cref="Printer"/>.
    /// </summary>
    public string FullName => PrintSettings.PrinterName;
    /// <summary>
    /// Gets a value indicating whether this is the system default printer.
    /// </summary>
    public bool IsDefault => PrintSettings.IsDefaultPrinter;
    /// <summary>
    /// Gets a collection of paper sources supported by this printer.
    /// </summary>
    public PrinterSettings.PaperSourceCollection PaperSources => PrintSettings.PaperSources;
    /// <summary>
    /// Gets the bounds of the printable area of the page for the printer.
    /// </summary>
    /// <returns>A System.Drawing.RectangleF representing the length and width, in hundredths of an inch, of the area the printer is capable of printing in.</returns>
    public System.Drawing.RectangleF PrintableArea => PrintSettings.DefaultPageSettings.PrintableArea;
    /// <summary>
    /// Gets the maximum number of copies that the printer enables the user to print at a time.
    /// </summary>
    public int MaximumCopies => PrintSettings.MaximumCopies;
    /// <summary>
    ///  The various number of pages that a user can choose to print on a single side of a sheet of paper.
    /// </summary>
    public int[] PagesPerSheetCapability => AccessQueue().GetPrintCapabilities().PagesPerSheetCapability.ToArray();
    public System.Printing.PrintQueue AccessQueue()
    {
      var Result = FindQueue();

      Result.Refresh();

      return Result;
    }
    /// <summary>
    /// Get an icon for this printer.
    /// </summary>
    /// <returns>A bitmap image representation of this <see cref="Printer"/>.</returns>
    public Inv.Image GetIcon()
    {
      var printerName = PrintSettings.PrinterName;
      if (printerName.StartsWith("\\", StringComparison.Ordinal))
      {
        var parts = printerName.Split('\\');
        if (parts.Length == 4)
          printerName = parts[3] + " on " + parts[2];
      }

      Win32.Shell32.SHGetDesktopFolder(out var pDesktop);
      var desktop = (Win32.Shell32.IShellFolder)Marshal.GetTypedObjectForIUnknown(pDesktop, typeof(Win32.Shell32.IShellFolder));

      Win32.Shell32.SHGetSpecialFolderLocation(IntPtr.Zero, Win32.Shell32.CSIDL.PRINTERS, out var pPrinters);
      desktop.BindToObject(pPrinters, IntPtr.Zero, ref Win32.Shell32.IID_IShellFolder, out var pPrinterInterfaces);
      var printers = (Win32.Shell32.IShellFolder)Marshal.GetTypedObjectForIUnknown(pPrinterInterfaces, typeof(Win32.Shell32.IShellFolder));

      printers.EnumObjects(IntPtr.Zero, Win32.Shell32.SHCONTF.NONFOLDERS, out var pEnumId);
      if (pEnumId == null)
        return null;

      var enumId = (Win32.Shell32.IEnumIDList)Marshal.GetTypedObjectForIUnknown(pEnumId, typeof(Win32.Shell32.IEnumIDList));

      var flag = Win32.Shell32.SHGFI.USEFILEATTRIBUTES | Win32.Shell32.SHGFI.SYSICONINDEX;
      var shfi = new Win32.Shell32.SHFILEINFO();
      var imageList = Win32.Shell32.SHGetFileInfo(".txt", Win32.Shell32.FILE_ATTRIBUTE.NORMAL, ref shfi, Win32.Shell32.cbFileInfo, flag);

      while (enumId.Next(1, out var pidlPrinter, out var fetched) == Win32.Shell32.S_OK && fetched == 1)
      {
        var pidlFull = new Win32.Shell32.PIDL(pidlPrinter, true);
        pidlFull.Insert(pPrinters);

        Win32.Shell32.FILE_ATTRIBUTE dwAttr = 0;
        shfi = new Win32.Shell32.SHFILEINFO();
        var dwflag = /* Shell32.SHGFI.SYSICONINDEX | */ Win32.Shell32.SHGFI.PIDL | Win32.Shell32.SHGFI.ICON | Win32.Shell32.SHGFI.TYPENAME | Win32.Shell32.SHGFI.DISPLAYNAME;
        Win32.Shell32.SHGetFileInfo(pidlFull.Ptr, dwAttr, ref shfi, Win32.Shell32.cbFileInfo, dwflag);
        Marshal.FreeCoTaskMem(pidlFull.Ptr);

        // right, now we've done all that com stuff, we have an icon
        if (shfi.szDisplayName.Equals(printerName))
        {
          var i = Win32.Shell32.ImageList_ReplaceIcon(imageList, -1, shfi.hIcon);
          var iconPtr = Win32.Shell32.ImageList_GetIcon(imageList, i, Win32.Shell32.ILD.NORMAL);
          if (iconPtr != IntPtr.Zero)
          {
            var icon = System.Drawing.Icon.FromHandle(iconPtr);
            var retVal = (System.Drawing.Icon)icon.Clone();
            Win32.Shell32.DestroyIcon(iconPtr);

            if (retVal == null)
            {
              return null;
            }
            else
            {
              var hBitmap = retVal.ToBitmap().GetHbitmap();
              try
              {
                return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap, System.IntPtr.Zero, System.Windows.Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions()).ConvertToInvImagePng();
              }
              finally
              {
                Win32.Gdi32.DeleteObject(hBitmap);
              }
            }
          }
        }
      }
      return null;
    }

    private System.Printing.PrintQueue FindQueue()
    {
      PrintQueue Result = null;

      var enumerationFlags = new EnumeratedPrintQueueTypes[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections };

      using (var ps = new LocalPrintServer())
      {
        foreach (var p in ps.GetPrintQueues(enumerationFlags))
        {
          if (p.FullName.Equals(PrintSettings.PrinterName))
            Result = p;
        }
      }

      if (Result == null)
        throw new Exception("unable to find queue for " + PrintSettings.PrinterName);

      return Result;
    }

    private readonly PrinterSettings PrintSettings;
  }

  /// <summary>
  /// Manages the list of system printers.
  /// </summary>
  public static class PrinterManager
  {
    static PrinterManager()
    {
      InstalledPrinterList = new Inv.DistinctList<Printer>();

      DefaultPrinter = null;

      try
      {
        foreach (var PrinterName in PrinterSettings.InstalledPrinters.Convert<string>())
        {
          var Printer = new Printer(PrinterName);

          InstalledPrinterList.Add(Printer);

          if (Printer.IsDefault)
            DefaultPrinter = Printer;
        }
      }
      catch (Exception Exception)
      {
        Debug.WriteLine(Exception.Describe());

        if (Debugger.IsAttached)
          Debugger.Break();
      }
    }

    /// <summary>
    /// The default printer for the system.
    /// </summary>
    public static Inv.Printer DefaultPrinter { get; private set; }
    /// <summary>
    /// The list of printers installed on the system.
    /// </summary>
    public static IReadOnlyList<Printer> PrinterList => InstalledPrinterList;

    public static Inv.Printer GetPrinter(string PrinterName)
    {
      return PrinterName != null ? PrinterList.FirstOrDefault(P => string.Equals(P.FullName, PrinterName, StringComparison.InvariantCultureIgnoreCase)) : null;
    }
    public static IEnumerable<Inv.Printer> GetPrinters(params string[] PrinterNameArray)
    {
      var Result = new Inv.DistinctList<Inv.Printer>();

      foreach (var PrinterName in PrinterNameArray)
      {
        var Printer = GetPrinter(PrinterName);

        if (Printer != null)
          Result.Add(Printer);
      }

      return Result;
    }

    private static Inv.DistinctList<Inv.Printer> InstalledPrinterList;
  }
}