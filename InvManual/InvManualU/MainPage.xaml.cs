﻿namespace InvManualU
{
  public sealed partial class MainPage : Windows.UI.Xaml.Controls.Page
  {
    public MainPage()
    {
      this.InitializeComponent();

      Inv.UwaShell.XboxLive.AutoSignIn = false;
      Inv.UwaShell.Attach(this, Inv.Manual.Shell.Install);
    }
  }
}