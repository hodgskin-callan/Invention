﻿namespace Inv.Manual
{
  public class Program
  {
    [System.STAThread]
    static void Main(string[] args)
    {
      Inv.WpfShell.CheckRequirements(() =>
      {
        Inv.WpfShell.Options.DeviceEmulation = Inv.WpfDeviceEmulation.iPhoneX;
        Inv.WpfShell.Options.DeviceEmulationRotated = false;
        Inv.WpfShell.Options.DeviceEmulationFramed = true;
        Inv.WpfShell.Options.FullScreenMode = false;
        Inv.WpfShell.Options.DefaultWindowWidth = 1920;
        Inv.WpfShell.Options.DefaultWindowHeight = 1080;

        Inv.WpfShell.RunBridge(B =>
        {
          Inv.Manual.Shell.Install(B.Application);

          B.SystemTray.Tooltip = "Invention Manual";

          B.Application.SuspendEvent += () =>
          {
            B.SystemTray.Show();
            B.SystemTray.ToastInformation("Invention Manual", "The application is suspended.");
          };
          B.Application.ResumeEvent += () =>
          {
            B.SystemTray.Show();
            B.SystemTray.ToastInformation("Invention Manual", "The application has been resumed.");
          };
        });
      });
    }
  }
}