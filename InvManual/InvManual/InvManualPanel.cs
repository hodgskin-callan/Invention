﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Inv.Support;

namespace Inv.Manual
{
  internal sealed class DialogPanel : Inv.Panel<Inv.Overlay>
  {
    public DialogPanel(Inv.Surface Surface)
    {
      this.Surface = Surface;
      this.Base = Surface.NewOverlay();
    }

    public void AddLayer(Inv.Panel Panel)
    {
      Base.AddPanel(Panel);
    }
    public void RemoveLayer(Inv.Panel Panel)
    {
      Base.RemovePanel(Panel);
    }
    public FlyoutPanel NewFlyoutPanel()
    {
      return new FlyoutPanel(this, Surface);
    }

    private readonly Inv.Surface Surface;
  }

  internal sealed class FlyoutPanel : Inv.Panel<Inv.Button>
  {
    public FlyoutPanel(DialogPanel Dialog, Inv.Surface Surface)
    {
      this.Dialog = Dialog;

      this.Base = Surface.NewFlatButton();
      Base.Background.In(Inv.Colour.Black.Opacity(0.50F));
      Base.SingleTapEvent += () => Hide();
    }

    public Inv.Panel Content
    {
      get => Base.Content;
      set => Base.Content = value;
    }

    public void Show()
    {
      Dialog.AddLayer(this);
    }
    public void Hide()
    {
      Dialog.RemoveLayer(this);
    }

    private readonly DialogPanel Dialog;
  }

  internal sealed class SearchPanel : Inv.Panel<TapButton>
  {
    public SearchPanel(Inv.Surface Surface)
    {
      this.Base = Surface.NewSearchButton();
      Base.Background.In(Inv.Colour.DimGray);
      Base.Corner.Set(Theme.BookCorner);
      Base.Size.SetWidth(Theme.BookSize);
      Base.Padding.Set(Theme.BookPadding);
      Base.Margin.Set(Theme.BookGap);

      var Dock = Surface.NewHorizontalDock();
      Base.Content = Dock;

      var Graphic = Surface.NewGraphic();
      Dock.AddFooter(Graphic);
      Graphic.Image = Resources.Images.SearchWhite;
      Graphic.Size.Set(40, 40);

      var Label = Surface.NewLabel();
      Dock.AddClient(Label);
      Label.Justify.Center();
      Label.Padding.Set(4, 0, 4, 0);
      Label.Font.Custom(Theme.BookFontSize).In(Inv.Colour.White).Weight = Theme.DocumentFontWeight;
      Label.Text = "Topics";
    }

    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public event Action SingleTapEvent
    {
      add => Base.SingleTapEvent += value;
      remove => Base.SingleTapEvent -= value;
    }
  }

  internal sealed class SubjectPanel : Inv.Panel<Inv.Overlay>
  {
    public SubjectPanel(Inv.Surface Surface)
    {
      this.Base = Surface.NewOverlay();

      this.TextLabel = Surface.NewLabel();
      Base.AddPanel(TextLabel);
      TextLabel.Margin.Set(20);
      TextLabel.Font.Custom(40).In(Theme.SubjectColour).Weight = Theme.DocumentFontWeight;

      this.BadgeLabel = Surface.NewLabel();
      Base.AddPanel(BadgeLabel);
      BadgeLabel.Margin.Set(20, 80, 10, 10);
      BadgeLabel.Alignment.BottomRight();
      BadgeLabel.Font.Custom(16).In(Inv.Colour.DarkGray);
    }

    public string Text
    {
      get => TextLabel.Text;
      set => TextLabel.Text = value;
    }
    public Badge Badge
    {
      set 
      {
        BadgeLabel.Font.In(value.IsUpdated ? Inv.Colour.Yellow : Inv.Colour.DarkGray);

        if (value.IsUnread)
          BadgeLabel.Text = "";
        else
          BadgeLabel.Text = value.ToString(); 
      }
    }

    private readonly Inv.Label TextLabel;
    private readonly Inv.Label BadgeLabel;
  }

  internal sealed class DocumentPanel : Inv.Panel<Inv.Stack>
  {
    public DocumentPanel(Inv.Surface Surface)
    {
      this.Surface = Surface;
      this.Base = Surface.NewVerticalStack();
      Base.Margin.Set(0, 0, 0, Theme.DocumentGap);
    }

    public void Break()
    {
      var Break = Surface.NewFrame();
      Base.AddPanel(Break);
      Break.Margin.Set(Theme.DocumentGap, Theme.DocumentGap, Theme.DocumentGap, 0);
    }
    public void Paragraph(string Text)
    {
      var Paragraph = new Paragraph(Surface);
      Base.AddPanel(Paragraph);
      Paragraph.SetText(Text);
    }
    public void Paragraph(Action<Paragraph> Action)
    {
      var Paragraph = new Paragraph(Surface);
      Base.AddPanel(Paragraph);
      Action(Paragraph);
    }
    public BulletList BulletList(params string[] ItemArray)
    {
      var BulletList = new BulletList(Surface);
      Base.AddPanel(BulletList);

      foreach (var Item in ItemArray.ExceptNull())
        BulletList.AddItem(Item);

      return BulletList;
    }
    public NumberedList NumberedList(params string[] ItemArray)
    {
      var NumberedList = new NumberedList(Surface);
      Base.AddPanel(NumberedList);

      foreach (var Item in ItemArray.ExceptNull())
        NumberedList.AddItem(Item);

      return NumberedList; 
    }
    public void Code(params string[] TextArray)
    {
      var Code = new CodePanel(Surface);
      Base.AddPanel(Code);
      Code.Margin.Set(Theme.DocumentGap, 0, Theme.DocumentGap, 0);
      Code.Text = TextArray.ExceptNull().AsSeparatedText("\r\n"); // don't use Environment.NewLine as iOS is '\n'
    }
    public void LogoTile(Inv.Image Image, string Title, Uri Uri)
    {
      var LogoTile = new LogoTile(Surface);
      LogoTile.Margin.Set(Theme.DocumentGap, 0, Theme.DocumentGap, 0);
      LogoTile.LogoImage = Image;
      LogoTile.TitleText = Title;
      LogoTile.ActionText = Uri.WithoutScheme();
      LogoTile.SingleTapEvent += () => Surface.Window.Application.Web.Launch(Uri);

      Base.AddPanel(LogoTile);
    }
    public void Hyperlink(Uri Uri)
    {
      var Hyperlink = Surface.NewLaunchButton();
      Base.AddPanel(Hyperlink);
      Hyperlink.Margin.Set(Theme.DocumentGap, 0, Theme.DocumentGap, 0);
      Hyperlink.SingleTapEvent += () => Surface.Window.Application.Web.Launch(Uri);
      Hyperlink.Padding.Set(10);
      Hyperlink.Background.In(Theme.SheetColour);
      Hyperlink.Alignment.TopLeft();

      var Code = Surface.NewLabel();
      Hyperlink.Content = Code;
      Code.Font.Name = Theme.HyperlinkFontName;
      Code.Font.Custom(Theme.HyperlinkFontSize).In(Theme.HyperlinkFontColour);
      Code.Border.Set(0, 0, 0, 1).In(Theme.HyperlinkFontColour);
      Code.Text = Uri.AbsoluteUri;
    }
    public PreviewPanel Preview()
    {
      var Result = new PreviewPanel(Surface);
      Base.AddPanel(Result);

      return Result;
    }

    private readonly Inv.Surface Surface;
  }

  internal sealed class LogoTile : Inv.Panel<TapButton>
  {
    public LogoTile(Inv.Surface Surface)
    {
      this.Base = Surface.NewLaunchButton();
      Base.Background.In(Theme.LogoTileColour);
      Base.Padding.Set(4);

      var Dock = Surface.NewHorizontalDock();
      Base.Content = Dock;

      var Frame = Surface.NewFrame();
      Dock.AddHeader(Frame);
      Frame.Corner.Set(32);
      Frame.Size.Set(64);
      Frame.Background.In(Theme.LogoTitleColour);
      Frame.Alignment.TopLeft();

      this.Graphic = Surface.NewGraphic();
      Frame.Content = Graphic;
      Graphic.Padding.Set(10);

      var Stack = Surface.NewVerticalStack();
      Dock.AddClient(Stack);
      Stack.Margin.Set(8, 0, 0, 0);
      Stack.Alignment.CenterStretch();

      this.TitleLabel = Surface.NewLabel();
      Stack.AddPanel(TitleLabel);
      TitleLabel.Font.Custom(22).Light().In(Theme.LogoTitleColour);

      this.ActionLabel = Surface.NewLabel();
      Stack.AddPanel(ActionLabel);
      ActionLabel.Font.Custom(14).Regular().In(Theme.LogoActionColour);
      ActionLabel.LineWrapping = false;

      var DirectGraphic = Surface.NewGraphic();
      Dock.AddFooter(DirectGraphic);
      DirectGraphic.Alignment.Center();
      DirectGraphic.Size.Set(32);
      DirectGraphic.Image = Theme.LogoAdvanceImage;
    }

    public Inv.Margin Margin => Base.Margin;
    public Inv.Colour Colour
    {
      set => Base.Background.Colour = value;
    }
    public Inv.Image LogoImage
    {
      set => Graphic.Image = value;
    }
    public string TitleText
    {
      set => TitleLabel.Text = value;
    }
    public string ActionText
    {
      set => ActionLabel.Text = value;
    }
    public event Action SingleTapEvent
    {
      add => Base.SingleTapEvent += value;
      remove => Base.SingleTapEvent -= value;
    }

    private readonly Inv.Graphic Graphic;
    private readonly Inv.Label TitleLabel;
    private readonly Inv.Label ActionLabel;
  }

  internal sealed class Paragraph : Inv.Panel<Inv.Label>
  {
    internal Paragraph(Inv.Surface Surface)
    {
      this.Base = Surface.NewLabel();
      Base.Margin.Set(Theme.DocumentGap, 0, Theme.DocumentGap, 0);
      Base.Font.Custom(Theme.DocumentFontSize).In(Theme.DocumentFontColour).Weight = Theme.DocumentFontWeight;
    }

    internal void SetText(string Text)
    {
      Base.Text = Text;
    }
    public void Sentence(string Text)
    {
      Debug.Assert(!Text.Trim().EndsWith("."), "Sentence must not end with a full stop as it is inserted for you.");

      Base.Text = Base.Text.Append(Text + ".", " ");
    }
  }

  internal sealed class NumberedList : Inv.Panel<Inv.Table>
  {
    internal NumberedList(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = Surface.NewTable();

      this.NumberColumn = Base.AddAutoColumn();
      this.TextColumn = Base.AddStarColumn();
    }

    public void AddItem(string Text)
    {
      var Row = Base.AddAutoRow();

      var NumberLabel = Surface.NewLabel();
      Base.GetCell(NumberColumn, Row).Content = NumberLabel;
      NumberLabel.Alignment.TopLeft();
      NumberLabel.Margin.Set(Theme.DocumentGap, 0, 0, 0);
      NumberLabel.Padding.Set(0, 0, 10, 0);
      NumberLabel.Font.Custom(Theme.DocumentFontSize).In(Theme.DocumentFontColour).Weight = Theme.DocumentFontWeight;
      NumberLabel.Text = (Row.Index + 1) + ".";

      var TextLabel = Surface.NewLabel();
      Base.GetCell(TextColumn, Row).Content = TextLabel;
      TextLabel.Margin.Set(0, 0, Theme.DocumentGap, 0);
      TextLabel.Font.Custom(Theme.DocumentFontSize).In(Theme.DocumentFontColour).Weight = Theme.DocumentFontWeight;
      TextLabel.LineWrapping = true;
      TextLabel.Text = Text;
    }

    private readonly Inv.Surface Surface;
    private readonly TableColumn NumberColumn;
    private readonly TableColumn TextColumn;
  }

  internal sealed class BulletList : Inv.Panel<Inv.Table>
  {
    internal BulletList(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = Surface.NewTable();

      this.NumberColumn = Base.AddAutoColumn();
      this.TextColumn = Base.AddStarColumn();
    }

    public void AddItem(string Text)
    {
      var Row = Base.AddAutoRow();

      var NumberLabel = Surface.NewLabel();
      Base.GetCell(NumberColumn, Row).Content = NumberLabel;
      NumberLabel.Alignment.TopLeft();
      NumberLabel.Margin.Set(Theme.DocumentGap, 0, 0, 0);
      NumberLabel.Padding.Set(0, 0, 10, 0);
      NumberLabel.Font.Custom(Theme.DocumentFontSize).In(Theme.DocumentFontColour).Weight = Theme.DocumentFontWeight;
      NumberLabel.Text = "• ";

      var TextLabel = Surface.NewLabel();
      Base.GetCell(TextColumn, Row).Content = TextLabel;
      TextLabel.Margin.Set(0, 0, Theme.DocumentGap, 0);
      TextLabel.Font.Custom(Theme.DocumentFontSize).In(Theme.DocumentFontColour).Weight = Theme.DocumentFontWeight;
      TextLabel.LineWrapping = true;
      TextLabel.Text = Text;
    }

    private readonly Inv.Surface Surface;
    private readonly TableColumn NumberColumn;
    private readonly TableColumn TextColumn;
  }

  internal sealed class NavigatePanel : Inv.Panel<Inv.Dock>
  {
    public NavigatePanel(Inv.Surface Surface)
    {
      this.Base = Surface.NewHorizontalDock();
      Base.Padding.Set(0, Theme.NavigateGutterSize, 0, 0);
      Base.Background.In(Theme.NavigateGutterColour);

      this.BackButton = Surface.NewBackButton();
      Base.AddHeader(BackButton);
      BackButton.Background.In(Inv.Colour.LightGray);
      BackButton.Padding.Set(10);
      BackButton.Size.SetWidth(Theme.NavigateWidth);
      BackButton.Corner.Set(0, 2, 0, 0);
      BackButton.Margin.Set(0, 0, Theme.NavigateGutterSize, 0);

      this.BackLabel = Surface.NewLabel();
      BackButton.Content = BackLabel;
      BackLabel.Font.Custom(16).In(Inv.Colour.Black).Weight = Theme.DocumentFontWeight;
      BackLabel.LineWrapping = false;

      this.NextButton = Surface.NewNextButton();
      Base.AddFooter(NextButton);
      NextButton.Background.In(Theme.KeyColour);
      NextButton.Padding.Set(10);
      NextButton.Size.SetWidth(Theme.NavigateWidth);
      NextButton.Corner.Set(2, 0, 0, 0);
      NextButton.Margin.Set(Theme.NavigateGutterSize, 0, 0, 0);

      this.NextLabel = Surface.NewLabel();
      NextButton.Content = NextLabel;
      NextLabel.Justify.Right();
      NextLabel.Font.Custom(16).In(Inv.Colour.White).Weight = Theme.DocumentFontWeight;
      NextLabel.LineWrapping = false;

      this.TitleButton = Surface.NewEscapeButton();
      Base.AddClient(TitleButton);
      TitleButton.Corner.Set(2, 2, 0, 0);
      TitleButton.Background.In(Inv.Colour.LightGray.Darken(0.10F));
      //TitleButton.Padding.Set(10);

      this.TitleLabel = Surface.NewLabel();
      TitleButton.Content = TitleLabel;
      TitleLabel.Font.Custom(20).In(Inv.Colour.Black).Weight = Theme.DocumentFontWeight;
      TitleLabel.Justify.Center();
      TitleLabel.LineWrapping = false;
    }

    public string TitleText
    {
      get => TitleLabel.Text;
      set => TitleLabel.Text = value;
    }
    public bool TitleIsEnabled
    {
      get => TitleButton.IsEnabled;
      set => TitleButton.IsEnabled = value;
    }
    public event Action TitleEvent
    {
      add => TitleButton.SingleTapEvent += value;
      remove => TitleButton.SingleTapEvent -= value;
    }
    public string BackText
    {
      get => BackLabel.Text;
      set => BackLabel.Text = value;
    }
    public bool BackIsEnabled
    {
      get => BackButton.IsEnabled;
      set => BackButton.IsEnabled = value;
    }
    public event Action BackEvent
    {
      add => BackButton.SingleTapEvent += value;
      remove => BackButton.SingleTapEvent -= value;
    }
    public string NextText
    {
      get => NextLabel.Text;
      set => NextLabel.Text = value;
    }
    public bool NextIsEnabled
    {
      get => NextButton.IsEnabled;
      set => NextButton.IsEnabled = value;
    }
    public event Action NextEvent
    {
      add => NextButton.SingleTapEvent += value;
      remove => NextButton.SingleTapEvent -= value;
    }

    internal void Back()
    {
      BackButton.SingleTap();
    }
    internal void Next()
    {
      NextButton.SingleTap();
    }

    private readonly TapButton BackButton;
    private readonly Inv.Label BackLabel;
    private readonly TapButton NextButton;
    private readonly Inv.Label NextLabel;
    private readonly TapButton TitleButton;
    private readonly Inv.Label TitleLabel;
  }

  internal sealed class PreviewPanel : Inv.Panel<Inv.Frame>
  {
    public PreviewPanel(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = Surface.NewFrame();
      Base.Elevation.Set(5);
      Base.Size.Set(256, 256);
      Base.Background.In(Inv.Colour.WhiteSmoke);
    }

    public Inv.Size Size => Base.Size;

    public Inv.Control GetExampleControl()
    {
      return ExamplePanel.Control;
    }
    public void SetExamplePanel(Inv.Panel Panel)
    {
      this.ExamplePanel = Panel;
      Base.Content = ExamplePanel;
    }

    public Inv.Label SetExampleLabel()
    {
      var Result = Surface.NewLabel();
      Result.Background.In(Inv.Colour.DodgerBlue);
      Result.Justify.Center();
      Result.Font.Custom(20).In(Inv.Colour.White);
      Result.Text = "Example Label";

      SetExamplePanel(Result);

      return Result;
    }

    private readonly Inv.Surface Surface;
    private Inv.Panel ExamplePanel;
  }

  internal sealed class CodePanel : Inv.Panel<Inv.Memo>
  {
    public CodePanel(Inv.Surface Surface)
    {
      this.Base = Surface.NewMemo();
      Base.IsReadOnly = true;
#if DEBUG
      //Base.Background.In(Inv.Colour.DarkGreen);
      //Base.IsReadOnly = false;
#endif
      Base.Font.Name = Theme.CodeFontName;
      Base.Font.Custom(Theme.CodeFontSize).In(Theme.CodeFontColour);
    }

    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public string Text
    {
      get => Base.Text;
      set
      {
        Base.Text = value;

        Base.RemoveMarkups();

        if (value != null)
          SyntaxHighlighting(value);
      }
    }

    private void SyntaxHighlighting(string Text)
    {
      var KeywordMarkup = Base.AddMarkup();
      KeywordMarkup.Font.In(Colour.CornflowerBlue);
      foreach (Match m in KeywordRegex.Matches(Text))
        KeywordMarkup.AddRange(m.Index, m.Length);

      var NumberMarkup = Base.AddMarkup();
      NumberMarkup.Font.In(Colour.Violet);
      foreach (Match m in NumberRegex.Matches(Text))
        NumberMarkup.AddRange(m.Index, m.Length);

      var TypeMarkup = Base.AddMarkup();
      TypeMarkup.Font.In(Colour.Turquoise);
      foreach (Match m in TypeRegex.Matches(Text))
        TypeMarkup.AddRange(m.Index, m.Length - 1);  // don't include the trailing . ( [

      var StringMarkup = Base.AddMarkup();
      StringMarkup.Font.In(Colour.BurlyWood);
      foreach (Match m in LiteralCharacterRegex.Matches(Text))
        StringMarkup.AddRange(m.Index, m.Length);
      foreach (Match m in LiteralStringRegex.Matches(Text))
        StringMarkup.AddRange(m.Index, m.Length);
      foreach (Match m in VerbatimStringRegex.Matches(Text))
        StringMarkup.AddRange(m.Index, m.Length);

      var CommentMarkup = Base.AddMarkup();
      CommentMarkup.Font.In(Colour.LightGreen);
      foreach (Match m in BlockCommentRegex.Matches(Text))
      {
        if (!StringMarkup.InRange(m.Index))
          CommentMarkup.AddRange(m.Index, m.Length);
      }
      foreach (Match m in LineCommentRegex.Matches(Text))
      {
        if (!StringMarkup.InRange(m.Index))
          CommentMarkup.AddRange(m.Index, m.Length - 2); // don't include the '\r\n'
      }
    }

    private readonly Regex KeywordRegex = new Regex(@"\b(public|private|partial|sealed|abstract|virtual|override|static|namespace|new|class|using|void|foreach|this|in|const|var|while|do|true|false|string|int|long|ulong|uint|short|byte)\b");
    private readonly Regex NumberRegex = new Regex(@"\b([0-9]+|[0-9]+([x\.][0-9,A-F]+))F?\b");
    private readonly Regex TypeRegex = new Regex(@"\b(Colour|Rect|Point|Activity|Brushes|FontWeights|DateTime|Date|Time|Uri|UwaShell|WpfShell|iOSShell|AndroidShell|Shell|AndroidActivity|Resources|Coordinate|STAThread)[\(\.\]]\b");
    private readonly Regex BlockCommentRegex = new Regex(@"/\*(.*?)\*/", RegexOptions.Multiline);
    private readonly Regex LineCommentRegex = new Regex(@"//(.*?)\r?\n", RegexOptions.Singleline);
    private readonly Regex LiteralCharacterRegex = new Regex("'[^']'");
    private readonly Regex LiteralStringRegex = new Regex(@"""((\\[^\n]|[^""\n])*)""");
    private readonly Regex VerbatimStringRegex = new Regex(@"@(""[^""]*"")+");
  }

  internal sealed class PalettePanel : Inv.Panel<Inv.Table>
  {
    public PalettePanel(Inv.Surface Surface)
    {
      this.Base = Surface.NewTable();

      this.ColourDictionary = new Dictionary<Inv.Colour, TapButton>(Inv.Colour.All.Length);

      foreach (var Colour in Inv.Colour.All.OrderBy(C => C.IsTransparent).ThenBy(C => C.GetARGBRecord().GetHSLRecord().H))
      {
        var ColourButton = Surface.NewNextButton();
        ColourDictionary.Add(Colour, ColourButton);
        ColourButton.Border.Set(1).In(Colour == Inv.Colour.Black ? Inv.Colour.DimGray : Inv.Colour.Black);
        ColourButton.Background.In(Colour);
        ColourButton.SingleTapEvent += () => SelectEvent?.Invoke(Colour);

        if (Colour == Inv.Colour.Transparent)
        {
          var ColourLabel = Surface.NewLabel();
          ColourButton.Content = ColourLabel;
          ColourLabel.Alignment.Center();
          ColourLabel.Justify.Center();
          ColourLabel.Font.Custom(20).In(Inv.Colour.White).Weight = Theme.DocumentFontWeight;
          ColourLabel.LineWrapping = false;
          ColourLabel.Text = "T";
        }
      }
    }

    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public event Action<Inv.Colour> SelectEvent;

    public void Arrange(int PaletteWidth)
    {
      if (this.LastWidth != PaletteWidth)
      {
        this.LastWidth = PaletteWidth;

        var FrameGap = 4;
        var FrameSize = 46;
        var ColumnWidth = Math.Max(1, PaletteWidth / FrameSize);

        var TableRow = (Inv.TableRow)null;

        Base.RemovePanels();

        var TableColumnArray = new Inv.TableColumn[ColumnWidth];
        for (var TableColumnIndex = 0; TableColumnIndex < ColumnWidth; TableColumnIndex++)
          TableColumnArray[TableColumnIndex] = Base.AddAutoColumn();

        var ColumnIndex = 0;

        foreach (var ColourEntry in ColourDictionary)
        {
          if (TableRow == null || ColumnIndex >= ColumnWidth)
          {
            TableRow = Base.AddAutoRow();
            ColumnIndex = 0;
          }

          var TableColumn = TableColumnArray[ColumnIndex++];

          var ColourLabel = ColourEntry.Value;
          ColourLabel.Margin.Set(FrameGap);
          ColourLabel.Size.Set(FrameSize - FrameGap - FrameGap, FrameSize - FrameGap - FrameGap);

          Base.GetCell(TableColumn, TableRow).Content = ColourLabel;
        }
      }
    }

    private readonly Dictionary<Inv.Colour, TapButton> ColourDictionary;
    private int LastWidth;
  }

  internal sealed class ControlPanel : Inv.Panel<Inv.Stack>
  {
    public ControlPanel(Inv.Surface Surface, string Title)
    {
      this.Surface = Surface;
      this.Base = Surface.NewVerticalStack();
      this.Title = Title;
      this.StackList = new Inv.DistinctList<Inv.Stack>();
      this.ButtonList = new Inv.DistinctList<ControlButton>();
    }

    public string Title { get; private set; }
    public Inv.Margin Margin => Base.Margin;

    public ControlButton AddButton(string Text)
    {
      var Result = new ControlButton(this, Surface);
      Result.Text = Text;
      ButtonList.Add(Result);

      return Result;
    }

    internal event Action SelectEvent;

    internal void Arrange(int AvailableWidth)
    {
      if (this.LastWidth != AvailableWidth)
      {
        this.LastWidth = AvailableWidth;

        foreach (var Stack in StackList)
          Stack.RemovePanels();
        StackList.Clear();

        Base.RemovePanels();

        var ControlCount = AvailableWidth / (Theme.ControlWidth + (Theme.ControlGap * 2));

        Inv.Stack ControlStack = null;

        foreach (var ControlButton in ButtonList)
        {
          if (ControlStack == null || ControlStack.Panels.Count >= ControlCount)
          {
            ControlStack = Surface.NewHorizontalStack();
            StackList.Add(ControlStack);
            Base.AddPanel(ControlStack);
            ControlStack.Alignment.Center();
          }

          ControlStack.AddPanel(ControlButton);
        }
      }
    }
    internal void Refresh()
    {
      foreach (var Button in ButtonList)
        Button.Refresh();
    }
    internal void Autoplay()
    {
      var ControlButtonIndex = 0;

      this.ControlTimer = Surface.Window.NewTimer();
      ControlTimer.IntervalTime = TimeSpan.FromSeconds(1);
      ControlTimer.IntervalEvent += () =>
      {
        if (ControlButtonIndex < ButtonList.Count)
        {
          var ControlButton = ButtonList[ControlButtonIndex++];
          ControlButton.Select();
        }
        else
        {
          if (ButtonList.Count > 0)
          {
            var ControlButton = ButtonList[0];
            ControlButton.Select();
          }

          ControlTimer.Stop();
          this.ControlTimer = null;
        }
      };
      ControlTimer.Start();
    }
    internal void StopAutoplay()
    {
      if (ControlTimer != null)
      {
        ControlTimer.Stop();
        this.ControlTimer = null;
      }
    }
    internal void SelectInvoke()
    {
      if (SelectEvent != null)
        SelectEvent();
    }

    private readonly Inv.Surface Surface;
    private readonly Inv.DistinctList<Inv.Stack> StackList;
    private readonly Inv.DistinctList<ControlButton> ButtonList;
    private Inv.WindowTimer ControlTimer;
    private int LastWidth;
  }

  internal enum TapType
  {
    Back,
    Next,
    Escape,
    Launch
  }

  internal sealed class TapButton : Inv.Panel<Inv.Button>
  {
    internal TapButton(Inv.Surface Surface, TapType TapType)
    {
      var Sound = TapSoundArray[TapType];

      this.Base = Surface.NewFlatButton();
      Base.SingleTapEvent += () =>
      {
        // play tap sfx with random pitch shifting.
        Surface.Window.Application.Audio.Play(Sound, Volume: 0.50F, Rate: 1.0F + Random.Next(-6, +6) / 100.0F);
      };
      Base.PushAnimated();
    }

    public Inv.Background Background => Base.Background;
    public Inv.Border Border => Base.Border;
    public Inv.Margin Margin => Base.Margin;
    public Inv.Padding Padding => Base.Padding;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Size Size => Base.Size;
    public Inv.Corner Corner => Base.Corner;
    public Inv.Codepoint Codepoint => Base.Codepoint;
    public Inv.Panel Content
    {
      get => Base.Content;
      set => Base.Content = value;
    }
    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set => Base.IsEnabled = value;
    }
    public event Action SingleTapEvent
    {
      add => Base.SingleTapEvent += value;
      remove => Base.SingleTapEvent -= value;
    }

    public void SingleTap() => Base.SingleTap();

    private static readonly Random Random = new Random();
    private static readonly Inv.EnumArray<TapType, Inv.Sound> TapSoundArray = new EnumArray<TapType, Sound>()
    {
      { TapType.Back, Resources.Sounds.BackTap },
      { TapType.Next, Resources.Sounds.NextTap },
      { TapType.Escape, Resources.Sounds.EscapeTap },
      { TapType.Launch, Resources.Sounds.LaunchTap },
    };
  }

  internal sealed class BookButton : Inv.Panel<TapButton>
  {
    internal BookButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewNextButton();
      Base.Codepoint.Caller();
      Base.Background.In(Theme.ControlSelectColour);
      Base.Size.SetWidth(Theme.BookSize);
      Base.Corner.Set(Theme.BookCorner);
      Base.Margin.Set(Theme.BookGap);
      Base.SingleTapEvent += () => Select();

      var Overlay = Surface.NewOverlay();
      Base.Content = Overlay;

      this.TextLabel = Surface.NewLabel();
      Overlay.AddPanel(TextLabel);
      TextLabel.Padding.Set(Theme.BookPadding);
      TextLabel.Justify.Center();
      TextLabel.Font.Custom(Theme.BookFontSize).In(Inv.Colour.White).Weight = Theme.DocumentFontWeight;

      this.StatusLabel = Surface.NewLabel();
      Overlay.AddPanel(StatusLabel);
      StatusLabel.Padding.Set(4);
      StatusLabel.Alignment.BottomRight();
      StatusLabel.Font.Custom(16).In(Inv.Colour.WhiteSmoke).Weight = Theme.DocumentFontWeight;
    }

    public string Text
    {
      get => TextLabel.Text;
      set => TextLabel.Text = value;
    }
    public Progress Progress
    {
      set 
      {
        Base.Background.In(value.IsComplete() ? Inv.Colour.DimGray : Theme.ControlSelectColour);
        StatusLabel.Text = value.ToString(); 
      }
    }
    public event Action SelectEvent;

    internal void Select()
    {
      if (SelectEvent != null)
        SelectEvent();
    }

    private readonly Inv.Label TextLabel;
    private readonly Inv.Label StatusLabel;
  }

  internal sealed class TopicButton : Inv.Panel<TapButton>
  {
    internal TopicButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewNextButton();
      Base.Background.In(Inv.Colour.DimGray);
      Base.Size.SetWidth(Theme.TopicSize);
      Base.Padding.Set(Theme.TopicPadding);
      Base.Corner.Set(Theme.TopicCorner);
      Base.Margin.Set(Theme.TopicGap);
      Base.SingleTapEvent += () => Select();

      this.Label = Surface.NewLabel();
      Base.Content = Label;
      Label.Justify.Left();
      Label.Font.Custom(16).In(Inv.Colour.White).Weight = Theme.DocumentFontWeight;
    }

    public string Text
    {
      get => Label.Text;
      set => Label.Text = value;
    }
    public Badge Badge
    {
      set => Base.Background.In(value.IsRead ? Inv.Colour.Black : Inv.Colour.DimGray);
    }
    public event Action SelectEvent;

    internal void Select()
    {
      if (SelectEvent != null)
        SelectEvent();
    }

    private readonly Inv.Label Label;
  }

  internal sealed class ControlButton : Inv.Panel<Inv.Button>
  {
    internal ControlButton(ControlPanel Panel, Inv.Surface Surface)
    {
      this.Panel = Panel;

      this.Base = Surface.NewFlatButton();
      Base.Background.In(Inv.Colour.DimGray);
      Base.Padding.Set(10);
      Base.Corner.Set(2);
      Base.Size.SetWidth(Theme.ControlWidth);
      Base.Margin.Set(Theme.ControlGap);
      Base.SingleTapEvent += () => Select();
      Base.PushAnimated();

      this.Label = Surface.NewLabel();
      Base.Content = Label;
      Label.Justify.Center();
      Label.Font.Custom(16).In(Inv.Colour.White).Weight = Theme.DocumentFontWeight;
    }

    public string Text
    {
      get => Label.Text;
      set => Label.Text = value;
    }
    public event Action SelectEvent;
    public event Func<bool> SelectQuery;

    internal void Refresh()
    {
      Base.Background.In(SelectQuery != null && SelectQuery() ? Theme.ControlSelectColour : Theme.ControlNormalColour);
    }
    internal void Select()
    {
      Panel.StopAutoplay();

      if (SelectEvent != null)
        SelectEvent();

      Panel.SelectInvoke();
    }

    private readonly ControlPanel Panel;
    private readonly Inv.Label Label;
  }
}