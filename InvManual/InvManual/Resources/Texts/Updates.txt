2017-09-26
- Native Bridge topic.
- Build Automation topic.
- Server app tutorial.
- Integrated Medium articles.
- Expanded About page with more links.
- Animated buttons.

2017-01-17
- More NuGet packages supported.
- Tutorials for creating portable apps.
- Topics search for looking up by subject.

2017-01-16
- Added Audio topic.

2017-01-15
- first published to iOS (internal beta).

2017-01-14
- first published to Google Play (open beta).
- reloads from the last page when app is started.