#pragma warning disable 0649
namespace Inv.Manual
{
  public static class Resources
  {
    static Resources()
    {
      global::Inv.Resource.Foundation.Import(typeof(Resources), "Resources.InvManual.InvResourcePackage.rs");
    }

    public static readonly ResourcesImages Images;
    public static readonly ResourcesSounds Sounds;
    public static readonly ResourcesTexts Texts;
  }

  public sealed class ResourcesImages
  {
    public ResourcesImages() { }

    ///<Summary>(.png) 128 x 128 (3.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Advance;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ClipboardWhite;
    ///<Summary>(.png) 256 x 256 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Email;
    ///<Summary>(.png) 1600 x 1600 (52.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Gitlab;
    ///<Summary>(.png) 120 x 122 (5.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Kestral;
    ///<Summary>(.png) 512 x 512 (38.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Logo;
    ///<Summary>(.png) 256 x 256 (6.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Medium;
    ///<Summary>(.png) 128 x 128 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NuGet;
    ///<Summary>(.png) 128 x 128 (4.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Pathos;
    ///<Summary>(.png) 128 x 128 (3.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Phoenix;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SearchBlack;
    ///<Summary>(.png) 192 x 192 (2.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SearchWhite;
    ///<Summary>(.png) 128 x 128 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Spellunk;
    ///<Summary>(.png) 256 x 256 (3.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Twitter;
    ///<Summary>(.png) 128 x 128 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VisualStudio;
  }

  public sealed class ResourcesSounds
  {
    public ResourcesSounds() { }

    ///<Summary>(.mp3) ~ (4.0KB)</Summary>
    public readonly global::Inv.Resource.SoundReference BackTap;
    ///<Summary>(.mp3) ~ (6.2KB)</Summary>
    public readonly global::Inv.Resource.SoundReference ErrorTap;
    ///<Summary>(.mp3) ~ (3.3KB)</Summary>
    public readonly global::Inv.Resource.SoundReference EscapeTap;
    ///<Summary>(.mp3) ~ (1.9KB)</Summary>
    public readonly global::Inv.Resource.SoundReference LaunchTap;
    ///<Summary>(.mp3) ~ (4.9KB)</Summary>
    public readonly global::Inv.Resource.SoundReference NextTap;
  }

  public sealed class ResourcesTexts
  {
    public ResourcesTexts() { }

    ///<Summary>(.txt) 2017-09-26 (0.5KB)</Summary>
    public readonly global::Inv.Resource.TextReference Updates;
    ///<Summary>(.txt) a2021.0407.1821 (0.0KB)</Summary>
    public readonly global::Inv.Resource.TextReference Version;
  }
}