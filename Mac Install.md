# Installing a MacBook Pro

As required for use with Invention iOS projects.

> NOTE: the latest version of Visual Studio is recommended when using the latest version of Xcode.

1. Follow MacOS installation wizards
1. Go to System Preferences > Sharing
   * Turn on Remote Login for your user
   * Change Computer Name to something easier to type
1. Upgrade MacOS
   * Pull down Apple Logo > About this Mac
   * Click Software Update...
   * Upgrade to latest available OS version (so that you can upgrade to the latest Xcode version)
1. Install Xcode from App Store
   * Run Xcode, accept license agreement
   * Pull down Xcode > Preferences > Account
   * Add new account 'AppleID'
1. Open Visual Studio
   * Go to Tools > iOS > Pair to Mac
   * Add your Mac by name
   * You will be prompted to install Mono and iOS packages (will take a while)
1. Install Chrome, set as default browser
1. On the Mac, run KeyChain Access
   * Pull down Keychain Access > Certificate Assistant > Request a Certificate From a Certificate Authority
   * Check Saved to disk
   * Click Continue
   * Save CertificateSigningRequest to your desktop
1. In Chrome, launch developer.apple.com.
   * Click Account
   * Click Certificates
   * Click Add button
   * Select iOS Distribution
   * Choose the CertificateSigningRequest you saved to your desktop
   * Download the generated certificate
   * Open the generated certificate and install to your User
   * Click Profiles
   * Add/Edit a profile and select the newly created certificate
   * Save the Profile to your desktop
   * Open the Profile to install it, locally
1. Set your Invention Build project as startup
   * In the code, make sure to change the computer name to point to your new Mac
   * Build for iOS
   * This can take quite a while (10+ minutes)
   * **When your Mac prompts to access your keychain, enter the password and click ALWAYS ALLOW THIS**
1. Access your PC via SSH
   * Go to Finder
   * Pull down Go > Connect to Server
   * Type `smb://<PC computer name>` (eg. `smb://callanh`)
   * Click Connect
   * Type your password and check Remember this password in my keychain
   * Select your Deployment folder (you may need to share this on your PC, if you haven't already)
1. Install Transporter from App Store
   * Run Transporter
   * Accept license agreement
   * Sign in with your AppleID
   * Click Add App
   * Locate your PC computer name and select the Deployment share
   * Switch file view to List mode
   * Sort by Date Modified column in reverse order, so the latest .ipa file is at the top
   * Select your .ipa file
   * Wait for it to be copied and prepared and then click DELIVER
