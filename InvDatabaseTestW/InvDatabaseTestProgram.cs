﻿using System;

namespace Inv.Database.Test
{
  public class Program
  {
    [STAThread]
    static void Main(string[] args)
    {
      Inv.WpfShell.CheckRequirements(() =>
      {
        Inv.WpfShell.Options.DeviceEmulation = null;// Inv.WpfDeviceEmulation.iPhone5;
        Inv.WpfShell.Options.DeviceEmulationRotated = false;
        Inv.WpfShell.Options.DeviceEmulationFramed = true;
        Inv.WpfShell.Options.FullScreenMode = false;
        Inv.WpfShell.Options.DefaultWindowWidth = 420;
        Inv.WpfShell.Options.DefaultWindowHeight = 650;

        Inv.WpfShell.Run(A =>
        {
          var PlatformList = new Inv.DistinctList<Inv.Database.Platform>();
          //PlatformList.Add(new MySQL.Platform("localhost", "root", "u0p031RNYc7tnqJL7NeBWUHboX8ToB", "InvDatabaseTest")); // Kyle home
          //PlatformList.Add(new MySQL.Platform("localhost", "root", "JOQtFieRujZgCmufS6ly", "InvDatabaseTest")); // Kyle work
          //PlatformList.Add(new MSSQL.Platform("localhost", "InvDatabaseTest"));
          PlatformList.Add(new SQLite.Platform(A.Directory.Root.NewFile("InvDatabaseTest.db")));

          Shell.Install(A, PlatformList);
        });
      });
    }
  }
}