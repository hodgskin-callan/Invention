﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Inv.Support;

namespace InvMeasurement
{
  [TestClass]
  public class RectTests
  {
    [TestMethod, Timeout(60000)]
    public void Subtract()
    {
      IEnumerable<Inv.Rect> OrderByIdentify(IEnumerable<Inv.Rect> Rects)
      {
        return Rects.OrderBy(E => E.Left).ThenBy(E => E.Top).ThenBy(E => E.Right).ThenBy(E => E.Bottom);
      }

      string Format(Inv.Rect[] RectArray)
      {
        if (RectArray == null)
          return "not intersecting";

        if (RectArray.Length == 0)
          return "empty";

        return OrderByIdentify(RectArray).Select(R => $"({R})").AsSeparatedText(" ");
      }

      void Test(Inv.Rect A, Inv.Rect B, Inv.Rect[] ExpectedArray)
      {
        var DetectedArray = A.Subtract(B);

        if (DetectedArray != ExpectedArray && !OrderByIdentify(ExpectedArray).SequenceEqual(OrderByIdentify(DetectedArray)))
          Assert.Fail($"{A}.Subtract({B}): Expected {Format(ExpectedArray)} but found {Format(DetectedArray)}");
      }

      // Corners.
      Test(new Inv.Rect(0, 0, 10, 10), new Inv.Rect(0, 0, 1, 1), new[] { new Inv.Rect(0, 1, 10, 9), new Inv.Rect(1, 0, 9, 1) });
      Test(new Inv.Rect(0, 0, 10, 10), new Inv.Rect(9, 9, 1, 1), new[] { new Inv.Rect(0, 0, 10, 9), new Inv.Rect(0, 9, 9, 1) });
      Test(new Inv.Rect(0, 0, 10, 10), new Inv.Rect(9, 0, 1, 1), new[] { new Inv.Rect(0, 0, 9, 1), new Inv.Rect(0, 1, 10, 9) });
      Test(new Inv.Rect(0, 0, 10, 10), new Inv.Rect(0, 9, 1, 1), new[] { new Inv.Rect(0, 0, 10, 9), new Inv.Rect(1, 9, 9, 1) });

      // Center square.
      Test(new Inv.Rect(0, 0, 10, 10), new Inv.Rect(5, 5, 1, 1), new[] { new Inv.Rect(0, 0, 10, 5), new Inv.Rect(0, 5, 5, 1), new Inv.Rect(6, 5, 4, 1), new Inv.Rect(0, 6, 10, 4) });

      // Subtraction is partially outside.
      Test(new Inv.Rect(0, 0, 10, 10), new Inv.Rect(5, -5, 1, 10), new[] { new Inv.Rect(0, 0, 5, 5), new Inv.Rect(6, 0, 4, 5), new Inv.Rect(0, 5, 10, 5) });
      Test(new Inv.Rect(0, 0, 10, 10), new Inv.Rect(-5, 5, 10, 1), new[] { new Inv.Rect(0, 0, 10, 5), new Inv.Rect(5, 5, 5, 1), new Inv.Rect(0, 6, 10, 4) });
      Test(new Inv.Rect(0, 0, 10, 10), new Inv.Rect(5, 5, 10, 1), new[] { new Inv.Rect(0, 0, 10, 5), new Inv.Rect(0, 5, 5, 1), new Inv.Rect(0, 6, 10, 4) });
      Test(new Inv.Rect(0, 0, 10, 10), new Inv.Rect(5, 5, 1, 10), new[] { new Inv.Rect(0, 0, 10, 5), new Inv.Rect(0, 5, 5, 5), new Inv.Rect(6, 5, 4, 5) });

      // Non-intersecting rects.
      Test(new Inv.Rect(0, 0, 10, 10), new Inv.Rect(-10, -10, 5, 5), null);

      // Subtracting a superset.
      Test(new Inv.Rect(0, 0, 10, 10), new Inv.Rect(-1, -1, 15, 15), new Inv.Rect[0]);
    }
  }

  [TestClass]
  public class TriangleTests
  {
    [TestMethod]
    public void Points()
    {
      void TestTriangle(Inv.Point A, Inv.Point B, Inv.Point C, Action<Inv.Triangle> ExecuteAction)
      {
        ExecuteAction(new Inv.Triangle(A, B, C));
        ExecuteAction(new Inv.Triangle(A, C, B));
        ExecuteAction(new Inv.Triangle(B, A, C));
        ExecuteAction(new Inv.Triangle(B, C, A));
        ExecuteAction(new Inv.Triangle(C, A, B));
        ExecuteAction(new Inv.Triangle(C, B, A));
      }

      TestTriangle(new Inv.Point(0, 0), new Inv.Point(0, 31), new Inv.Point(31, 31), T =>
      {
        var PointArray = T.GetAreaPoints().ToArray();

        for (var X = 0; X <= 31; X++)
        {
          for (var Y = 0; Y <= 31; Y++)
          {
            var Point = new Inv.Point(X, Y);

            if (X > Y)
              Assert.IsFalse(PointArray.Contains(Point), $"Triangle {T} contains unexpected point {Point}");
            else
              Assert.IsTrue(PointArray.Contains(Point), $"Triangle {T} does not contain expected point {Point}");      
          }
        }
      });
    }
  }
}