﻿using System;
using System.Threading;

namespace InvMoney
{
  [TestClass]
  public sealed class TruncateTests
  {
    [TestMethod, Timeout(60000)]
    public void ToZero1()
    {
      var RawValue = 0.000001M;
      Assert.AreEqual(0M, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void ToZero2()
    {
      var RawValue = -0.000001M;
      Assert.AreEqual(0M, new Inv.Money(RawValue).GetAmount());
    }

    [TestMethod, Timeout(60000)]
    public void Overflow1()
    {
      var RawValue = 9999999999999999999999999999M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Overflow2()
    {
      var RawValue = -9999999999999999999999999999M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }

    [TestMethod, Timeout(60000)]
    public void To4DP1()
    {
      var RawValue = 10.03492M;
      Assert.AreEqual(10.0349M, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void To4DP2()
    {
      var RawValue = -10.03492M;
      Assert.AreEqual(-10.0349M, new Inv.Money(RawValue).GetAmount());
    }

    [TestMethod, Timeout(60000)]
    public void Unchanged1()
    {
      var RawValue = 10.034M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged2()
    {
      var RawValue = -10.034M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged3()
    {
      var RawValue = 10.0341M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged4()
    {
      var RawValue = 10.0342M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged5()
    {
      var RawValue = 10.0343M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged6()
    {
      var RawValue = 10.0344M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged7()
    {
      var RawValue = 10.0345M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged8()
    {
      var RawValue = 10.0346M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged9()
    {
      var RawValue = 10.0347M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged10()
    {
      var RawValue = 10.0348M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged11()
    {
      var RawValue = 10.0349M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged12()
    {
      var RawValue = 10.0340M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged13()
    {
      var RawValue = -10.0341M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged14()
    {
      var RawValue = -10.0342M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged15()
    {
      var RawValue = -10.0343M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged16()
    {
      var RawValue = -10.0344M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged17()
    {
      var RawValue = -10.0345M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged18()
    {
      var RawValue = -10.0346M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged19()
    {
      var RawValue = -10.0347M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged20()
    {
      var RawValue = -10.0348M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged21()
    {
      var RawValue = -10.0349M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod, Timeout(60000)]
    public void Unchanged22()
    {
      var RawValue = -10.0340M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }

    [TestMethod, Timeout(60000)]
    public void Round0()
    {
      var RawValue = new Inv.Money(4M);
      Assert.AreEqual(RawValue.GetAmount().ToString(), "4");
    }
    [TestMethod, Timeout(60000)]
    public void Round1()
    {
      var RawValue = new Inv.Money(4.00M);
      Assert.AreEqual(RawValue.GetAmount().ToString(), "4");
    }
  }

  [TestClass]
  public sealed class ToStringTests
  {
    public ToStringTests()
    {
      AustralianCulture = new System.Globalization.CultureInfo("en-AU");
      ItalianCulture = new System.Globalization.CultureInfo("it-IT");
      JapaneseCulture = new System.Globalization.CultureInfo("ja-JP");
    }

    [TestMethod, Timeout(60000)]
    public void InterpolatedStringUsesDefaultFormatInCurrentCultureAustralia()
    {
      using (new CultureContext(AustralianCulture))
      {
        var Amount = new Inv.Money(12345.555m);

        var Actual = $"{Amount}";

        Assert.AreEqual("$12,345.56", Actual);
      }
    }
    [TestMethod, Timeout(60000)]
    public void InterpolatedStringUsesDefaultFormatInCurrentCultureItaly()
    {
      using (new CultureContext(ItalianCulture))
      {
        var Amount = new Inv.Money(12345.555m);

        var Actual = $"{Amount}";

        Assert.AreEqual("12.345,56 €", Actual);
      }
    }
    [TestMethod, Timeout(60000)]
    public void InterpolatedStringUsesDefaultFormatInCurrentCultureJapan()
    {
      using (new CultureContext(JapaneseCulture))
      {
        var Amount = new Inv.Money(12345.555m);

        var Actual = $"{Amount}";

        Assert.AreEqual("¥12,346", Actual);
      }
    }

    [TestMethod, Timeout(60000)]
    public void InterpolatedStringWithFormatUsesFormatInCurrentCultureAustralia()
    {
      using (new CultureContext(AustralianCulture))
      {
        var Amount = new Inv.Money(12345.555m);

        var Actual = $"{Amount:N0}";

        Assert.AreEqual("12,346", Actual);
      }
    }
    [TestMethod, Timeout(60000)]
    public void InterpolatedStringWithFormatUsesFormatInCurrentCultureItaly()
    {
      using (new CultureContext(ItalianCulture))
      {
        var Amount = new Inv.Money(12345.555m);

        var Actual = $"{Amount:N0}";

        Assert.AreEqual("12.346", Actual);
      }
    }
    [TestMethod, Timeout(60000)]
    public void InterpolatedStringWithFormatUsesFormatInCurrentCultureJapan()
    {
      using (new CultureContext(JapaneseCulture))
      {
        var Amount = new Inv.Money(12345.555m);

        var Actual = $"{Amount:N0}";

        Assert.AreEqual("12,346", Actual);
      }
    }

    private readonly System.Globalization.CultureInfo AustralianCulture;
    private readonly System.Globalization.CultureInfo ItalianCulture;
    private readonly System.Globalization.CultureInfo JapaneseCulture;

    private sealed class CultureContext : IDisposable
    {
      public CultureContext(System.Globalization.CultureInfo NewCulture)
      {
        this.InitialCulture = Thread.CurrentThread.CurrentCulture;
        Thread.CurrentThread.CurrentCulture = NewCulture;
      }
      ~CultureContext()
      {
        Dispose(false);
      }

      public void Dispose()
      {
        Dispose(true);
        GC.SuppressFinalize(this);
      }

      private void Dispose(bool IsDisposing)
      {
        if (IsDisposed)
          return;

        if (IsDisposing)
        {
          Thread.CurrentThread.CurrentCulture = InitialCulture;
        }

        IsDisposed = true;
      }

      private readonly System.Globalization.CultureInfo InitialCulture;
      private bool IsDisposed;
    }
  }
}