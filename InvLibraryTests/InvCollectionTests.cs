﻿using System;

namespace InvCollection
{
  [TestClass]
  public class DistinctListTests
  {
    [TestMethod, Timeout(60000)]
    public void ModifyWhileEnumerating()
    {
      var FrameworkArray = new string[] { "Hello", "world", "what", "a", "lovely", "day" };

      var DistinctList = new Inv.DistinctList<string>(FrameworkArray);

      var ListIndex = 0;
      foreach (var Item in DistinctList)
      {
        if (Item != FrameworkArray[ListIndex])
          Assert.Fail("DistinctList failed to enumerate each item.");

        DistinctList.Remove(Item);

        ListIndex++;
      }

      Assert.IsTrue(DistinctList.Count == 0, "DistinctList enumeration failed");
    }

    [TestMethod, Timeout(60000)]
    public void RemoveAll_ReturnsZeroForAnEmptyList()
    {
      var List = new Inv.DistinctList<int>(0);

      var Actual = List.RemoveAll(_ => true);

      Assert.AreEqual(0, Actual);
      CollectionAssert.AreEqual(Array.Empty<int>(), List);
    }

    [TestMethod, Timeout(60000)]
    public void RemoveAll_ReturnsOneWhenRemovingFirstElement()
    {
      var List = new Inv.DistinctList<int>() { 4, 5, 6 };

      var Actual = List.RemoveAll(X => X == 4);

      Assert.AreEqual(1, Actual);
      CollectionAssert.AreEqual(new[] { 5, 6 }, List);
    }

    [TestMethod, Timeout(60000)]
    public void RemoveAll_ReturnsOneWhenRemovingSingleElement()
    {
      var List = new Inv.DistinctList<int>() { 4, 5, 6 };

      var Actual = List.RemoveAll(X => X == 5);

      Assert.AreEqual(1, Actual);
      CollectionAssert.AreEqual(new[] { 4, 6 }, List);
    }

    [TestMethod, Timeout(60000)]
    public void RemoveAll_ReturnsOneWhenRemovingLastElement()
    {
      var List = new Inv.DistinctList<int>() { 4, 5, 6 };

      var Actual = List.RemoveAll(X => X == 6);

      Assert.AreEqual(1, Actual);
      CollectionAssert.AreEqual(new[] { 4, 5 }, List);
    }

    [TestMethod, Timeout(60000)]
    public void RemoveAll_ReturnsRemovedCountWhenRemovingMultipleElements()
    {
      var List = new Inv.DistinctList<int>() { 4, 5, 6, 7, 8 };

      var Actual = List.RemoveAll(X => X % 2 == 0);

      Assert.AreEqual(3, Actual);
      CollectionAssert.AreEqual(new[] { 5, 7 }, List);
    }

    [TestMethod, Timeout(60000)]
    public void RemoveAll_ReturnsRemovedCountWhenRemovingAllElements()
    {
      var List = new Inv.DistinctList<int>() { 4, 5, 6 };

      var Actual = List.RemoveAll(_ => true);

      Assert.AreEqual(3, Actual);
      CollectionAssert.AreEqual(Array.Empty<int>(), List);
    }
  }

  [TestClass]
  public class GridTests
  {
    [TestMethod, Timeout(60000)]
    public void RotateGrid()
    {
      var Grid = new Inv.Grid<char>(2, 2);
      Grid[0, 0] = 'a';
      Grid[1, 0] = 'b';
      Grid[0, 1] = 'c';
      Grid[1, 1] = 'd';

      // ab
      // cd

      Grid.Rotate90Degrees();
      Assert.IsTrue(Grid[0, 0] == 'b');
      Assert.IsTrue(Grid[1, 0] == 'd');
      Assert.IsTrue(Grid[0, 1] == 'a');
      Assert.IsTrue(Grid[1, 1] == 'c');

      // bd
      // ac

      Grid.Rotate90Degrees();
      Assert.IsTrue(Grid[0, 0] == 'd');
      Assert.IsTrue(Grid[1, 0] == 'c');
      Assert.IsTrue(Grid[0, 1] == 'b');
      Assert.IsTrue(Grid[1, 1] == 'a');

      // dc
      // ba
    }
  }
}