﻿using System;
using System.Runtime.Serialization;
using Inv.Support;

namespace InvDateTime
{
  [TestClass]
  public class DayOfWeekTests
  {
    [TestMethod, Timeout(60000)]
    public void StartOfWeekDate()
    {
      var Date = new Inv.Date(2019, 03, 28);

      Assert.AreEqual(new Inv.Date(2019, 03, 25), Date.StartOfWeek(DayOfWeek.Monday));
      Assert.AreEqual(new Inv.Date(2019, 03, 24), Date.StartOfWeek(DayOfWeek.Sunday));
    }

    [TestMethod, Timeout(60000)]
    public void StartOfWeekDateTime()
    {
      var Date = new DateTime(2019, 03, 28);

      Assert.AreEqual(new DateTime(2019, 03, 25), Date.StartOfWeek(DayOfWeek.Monday));
      Assert.AreEqual(new DateTime(2019, 03, 24), Date.StartOfWeek(DayOfWeek.Sunday));
    }

    [TestMethod, Timeout(60000)]
    public void EndOfWeekDate()
    {
      var Date = new Inv.Date(2019, 03, 28);

      Assert.AreEqual(new Inv.Date(2019, 04, 01), Date.EndOfWeek(DayOfWeek.Monday));
      Assert.AreEqual(new Inv.Date(2019, 03, 31), Date.EndOfWeek(DayOfWeek.Sunday));
    }

    [TestMethod, Timeout(60000)]
    public void EndOfWeekDateTime()
    {
      var Date = new DateTime(2019, 03, 28);

      Assert.AreEqual(new DateTime(2019, 04, 01), Date.EndOfWeek(DayOfWeek.Monday));
      Assert.AreEqual(new DateTime(2019, 03, 31), Date.EndOfWeek(DayOfWeek.Sunday));
    }
  }

  [TestClass]
  public class DateTests
  {
    public static IEnumerable<object[]> InClosedIntervalAllowingNullsData =>
      Enumerable.Concat(CommonIntervalData, new[]
      {
        new object[] { new Inv.Date(2000, 1, 1), new Inv.Date(2000, 1, 1), null, false },
        new object[] { new Inv.Date(2000, 1, 1), null, new Inv.Date(2000, 1, 1), false },
        new object[] { new Inv.Date(2000, 1, 1), new Inv.Date(2000, 1, 1), new Inv.Date(2000, 1, 1), false }
      });

    public static IEnumerable<object[]> InLeftOpenIntervalAllowingNullsData =>
      Enumerable.Concat(CommonIntervalData, new[]
      {
        new object[] { new Inv.Date(2000, 1, 1), new Inv.Date(2000, 1, 1), null, true },
        new object[] { new Inv.Date(2000, 1, 1), null, new Inv.Date(2000, 1, 1), false },
        new object[] { new Inv.Date(2000, 1, 1), new Inv.Date(2000, 1, 1), new Inv.Date(2000, 1, 1), false }
      });

    public static IEnumerable<object[]> InRightOpenIntervalAllowingNullsData =>
      Enumerable.Concat(CommonIntervalData, new[]
      {
        new object[] { new Inv.Date(2000, 1, 1), new Inv.Date(2000, 1, 1), null, false },
        new object[] { new Inv.Date(2000, 1, 1), null, new Inv.Date(2000, 1, 1), true },
        new object[] { new Inv.Date(2000, 1, 1), new Inv.Date(2000, 1, 1), new Inv.Date(2000, 1, 1), false }
      });

    public static IEnumerable<object[]> BetweenInclusiveAllowingNullsData =>
      Enumerable.Concat(CommonIntervalData, new[]
      {
        new object[] { new Inv.Date(2000, 1, 1), new Inv.Date(2000, 1, 1), null, true },
        new object[] { new Inv.Date(2000, 1, 1), null, new Inv.Date(2000, 1, 1), true },
        new object[] { new Inv.Date(2000, 1, 1), new Inv.Date(2000, 1, 1), new Inv.Date(2000, 1, 1), true }
      });

    [TestMethod, Timeout(60000)]
    public void NextDayOfWeek()
    {
      var StartDate = new Inv.Date(2013, 9, 3);
      var FindDayOfWeek = DayOfWeek.Wednesday;

      var ExpectedResult = new Inv.Date(2013, 9, 4);
      var Result = StartDate.NextDayOfWeek(FindDayOfWeek);
      if (Result != ExpectedResult)
        Assert.Fail("NextDayOfWeek fail.");

      StartDate = new Inv.Date(2013, 9, 6);
      FindDayOfWeek = DayOfWeek.Monday;
      ExpectedResult = new Inv.Date(2013, 9, 9);
      Result = StartDate.NextDayOfWeek(FindDayOfWeek);
      if (Result != ExpectedResult)
        Assert.Fail("NextDayOfWeek fail.");

      StartDate = new Inv.Date(2013, 9, 6);
      FindDayOfWeek = DayOfWeek.Friday;
      ExpectedResult = new Inv.Date(2013, 9, 13);
      Result = StartDate.NextDayOfWeek(FindDayOfWeek);
      if (Result != ExpectedResult)
        Assert.Fail("NextDayOfWeek fail.");
    }

    [TestMethod, Timeout(60000)]
    public void PreviousDayOfWeek()
    {
      var StartDate = new Inv.Date(2014, 9, 8);
      var FindDayOfWeek = DayOfWeek.Monday;
      var ExpectedResult = new Inv.Date(2014, 9, 1);
      var Result = StartDate.PreviousDayOfWeek(FindDayOfWeek);
      if (Result != ExpectedResult)
        Assert.Fail("PreviousDayOfWeek fail.");

      StartDate = new Inv.Date(2014, 9, 5);
      FindDayOfWeek = DayOfWeek.Monday;
      ExpectedResult = new Inv.Date(2014, 9, 1);
      Result = StartDate.PreviousDayOfWeek(FindDayOfWeek);
      if (Result != ExpectedResult)
        Assert.Fail("PreviousDayOfWeek fail.");

      StartDate = new Inv.Date(2014, 9, 7);
      FindDayOfWeek = DayOfWeek.Monday;
      ExpectedResult = new Inv.Date(2014, 9, 1);
      Result = StartDate.PreviousDayOfWeek(FindDayOfWeek);
      if (Result != ExpectedResult)
        Assert.Fail("PreviousDayOfWeek fail.");
    }

    [TestMethod, Timeout(60000)]
    public void String()
    {
      var AustralianCulture = new System.Globalization.CultureInfo("en-AU");
      var UnitedStatesCulture = new System.Globalization.CultureInfo("en-US");
      var CurrentCulture = System.Globalization.CultureInfo.CurrentCulture;
      var InvariantCulture = System.Globalization.CultureInfo.InvariantCulture;

      var TestDate = new Inv.Date(2000, 11, 22);
      var TestDateTime = TestDate.ToDateTime();

      Assert.IsTrue(
        string.Equals(TestDate.ToString(AustralianCulture), TestDateTime.ToString(AustralianCulture.DateTimeFormat.ShortDatePattern, AustralianCulture)),
        "Australian date format");

      Assert.IsTrue(
        string.Equals(TestDate.ToString(UnitedStatesCulture), TestDateTime.ToString(UnitedStatesCulture.DateTimeFormat.ShortDatePattern, UnitedStatesCulture)),
        "United States date format");

      Assert.IsTrue(
        string.Equals(TestDate.ToString(), TestDateTime.ToString(CurrentCulture.DateTimeFormat.ShortDatePattern, CurrentCulture)),
        ".ToString() should equal the short date pattern for the current culture");

      Assert.IsTrue(
        string.Equals(TestDate.ToString("yyyyMMdd"), TestDateTime.ToString("yyyyMMdd", CurrentCulture)),
        ".ToString(string) should equal that pattern for the current culture");

      Assert.IsTrue(
        string.Equals(TestDate.ToString("yyyyMMdd", CurrentCulture), TestDateTime.ToString("yyyyMMdd", CurrentCulture)),
        ".ToString(string, IFormatProvider) should be equal to the same method call on DateTime");

      Assert.IsTrue(
        string.Equals(TestDate.ToString(null, CurrentCulture), TestDateTime.ToString(CurrentCulture.DateTimeFormat.ShortDatePattern, CurrentCulture)),
        ".ToString(null, IFormatProvider) should be equal to the same method call on DateTime with a short date format");

      Assert.IsTrue(
        string.Equals(TestDate.ToString(null, InvariantCulture), TestDateTime.ToString(InvariantCulture.DateTimeFormat.ShortDatePattern, InvariantCulture)),
        ".ToString(null, IFormatProvider) should be equal to the same method call on DateTime with a short date format");
    }

    [TestMethod, Timeout(60000)]
    public void AgeInYears()
    {
      var LeapDayBirthDate = new Inv.Date(1992, 02, 29);
      var LeapYearBirthDate = new Inv.Date(1992, 06, 15);
      var NonLeapYearBirthDate = new Inv.Date(1993, 06, 15);

      // on birthday
      Assert.AreEqual(30, LeapYearBirthDate.AgeInYears(new Inv.Date(2022, 06, 15)), "on birthday - leap birth year");
      Assert.AreEqual(29, NonLeapYearBirthDate.AgeInYears(new Inv.Date(2022, 06, 15)), "on birthday - non leap birth year");
      Assert.AreEqual(30, LeapDayBirthDate.AgeInYears(new Inv.Date(2022, 03, 01)), "on birthday - leap day birth date");

      // in birthday month, but before birthday
      Assert.AreEqual(29, LeapYearBirthDate.AgeInYears(new Inv.Date(2022, 06, 10)), "in birthday month, before birthday - leap birth year");
      Assert.AreEqual(28, NonLeapYearBirthDate.AgeInYears(new Inv.Date(2022, 06, 10)), "in birthday month, before birthday - non leap birth year");
      // Note: in C#, a year after 29/02 is 28/02, so 28/02 is considered the birthday anniversary for someone born on a leap day.
      Assert.AreEqual(30, LeapDayBirthDate.AgeInYears(new Inv.Date(2022, 02, 28)), "in birthday month, before birthday - leap day birth date");

      // in birthday month, but after birthday
      Assert.AreEqual(30, LeapYearBirthDate.AgeInYears(new Inv.Date(2022, 06, 20)), "in birthday month, after birthday - leap birth year");
      Assert.AreEqual(29, NonLeapYearBirthDate.AgeInYears(new Inv.Date(2022, 06, 20)), "in birthday month, after birthday - non leap birth year");
      // no days in February after the leap day
      //Assert.AreEqual(29, LeapDayBirthDate.AgeInYears(new Inv.Date(2022, 02, 30)), "in birthday month, after birthday - leap day birth date");

      // in earlier month than birthday
      Assert.AreEqual(29, LeapYearBirthDate.AgeInYears(new Inv.Date(2022, 05, 01)), "before birthday month - leap birth year");
      Assert.AreEqual(28, NonLeapYearBirthDate.AgeInYears(new Inv.Date(2022, 05, 01)), "before birthday month - non leap birth year");
      Assert.AreEqual(29, LeapDayBirthDate.AgeInYears(new Inv.Date(2022, 01, 01)), "before birthday month - leap day birth date");

      // in later month than birthday
      Assert.AreEqual(30, LeapYearBirthDate.AgeInYears(new Inv.Date(2022, 08, 01)), "after birthday month - leap birth year");
      Assert.AreEqual(29, NonLeapYearBirthDate.AgeInYears(new Inv.Date(2022, 08, 01)), "after birthday month - non leap birth year");
      Assert.AreEqual(30, LeapDayBirthDate.AgeInYears(new Inv.Date(2022, 04, 01)), "after birthday month - leap day birth date");
    }

    [TestMethod, Timeout(60000)]
    public void DateTimeDifference()
    {
      Assert.AreEqual(Inv.TimePeriod.FromYears(4), new Inv.Date(2000, 01, 01).ToTimePeriodDifference(new Inv.Date(1996, 01, 01)));
      Assert.AreEqual(Inv.TimePeriod.FromYears(4).Years, new Inv.Date(2000, 02, 01).ToTimePeriodDifference(new Inv.Date(1996, 01, 01)).Years);
      Assert.AreEqual(Inv.TimePeriod.FromYears(3).Years, new Inv.Date(2000, 01, 01).ToTimePeriodDifference(new Inv.Date(1996, 02, 01)).Years);

      Assert.AreEqual(Inv.TimePeriod.FromYears(3), new Inv.Date(1999, 02, 28).ToTimePeriodDifference(new Inv.Date(1996, 02, 29)));
      Assert.AreEqual(Inv.TimePeriod.FromYears(-3).Years, new Inv.Date(1996, 02, 29).ToTimePeriodDifference(new Inv.Date(1999, 02, 28)).Years);
      Assert.AreEqual(Inv.TimePeriod.FromYears(4).Years, new Inv.Date(2000, 02, 29).ToTimePeriodDifference(new Inv.Date(1996, 02, 29)).Years);

      Assert.AreEqual(Inv.TimePeriod.FromMonths(1), new Inv.Date(2000, 02, 29).ToTimePeriodDifference(new Inv.Date(2000, 01, 31)));
      Assert.AreEqual(Inv.TimePeriod.FromMonths(1), new Inv.Date(2000, 02, 29).ToTimePeriodDifference(new Inv.Date(2000, 01, 30)));
      Assert.AreEqual(Inv.TimePeriod.FromMonths(1), new Inv.Date(2000, 02, 29).ToTimePeriodDifference(new Inv.Date(2000, 01, 29)));
      Assert.AreEqual(Inv.TimePeriod.FromMonths(1), new Inv.Date(2000, 02, 28).ToTimePeriodDifference(new Inv.Date(2000, 01, 28)));

      Assert.AreEqual(Inv.TimePeriod.FromMonths(1), new Inv.Date(1999, 02, 28).ToTimePeriodDifference(new Inv.Date(1999, 01, 31)));
      Assert.AreEqual(Inv.TimePeriod.FromMonths(1), new Inv.Date(1999, 02, 28).ToTimePeriodDifference(new Inv.Date(1999, 01, 30)));
      Assert.AreEqual(Inv.TimePeriod.FromMonths(1), new Inv.Date(1999, 02, 28).ToTimePeriodDifference(new Inv.Date(1999, 01, 29)));
      Assert.AreEqual(Inv.TimePeriod.FromMonths(1), new Inv.Date(1999, 02, 28).ToTimePeriodDifference(new Inv.Date(1999, 01, 28)));

      Assert.AreEqual(Inv.TimePeriod.FromMonths(2), new Inv.Date(2000, 03, 31).ToTimePeriodDifference(new Inv.Date(2000, 01, 31)));
    }

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(InClosedIntervalAllowingNullsData))]
    public void InClosedIntervalAllowingNulls(Inv.Date Value, Inv.Date? Left, Inv.Date? Right, bool Expected)
    {
      var Actual = Value.InClosedIntervalAllowingNulls(Left, Right);

      Assert.AreEqual(Expected, Actual);
    }

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(InLeftOpenIntervalAllowingNullsData))]
    public void InLeftOpenIntervalAllowingNulls(Inv.Date Value, Inv.Date? Left, Inv.Date? Right, bool Expected)
    {
      var Actual = Value.InLeftOpenIntervalAllowingNulls(Left, Right);

      Assert.AreEqual(Expected, Actual);
    }

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(InRightOpenIntervalAllowingNullsData))]
    public void InRightOpenIntervalAllowingNulls(Inv.Date Value, Inv.Date? Left, Inv.Date? Right, bool Expected)
    {
      var Actual = Value.InRightOpenIntervalAllowingNulls(Left, Right);

      Assert.AreEqual(Expected, Actual);
    }

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(BetweenInclusiveAllowingNullsData))]
    public void BetweenInclusiveAllowingNulls(Inv.Date Value, Inv.Date? Left, Inv.Date? Right, bool Expected)
    {
      var Actual = Value.BetweenInclusiveAllowingNulls(Left, Right);

      Assert.AreEqual(Expected, Actual);
    }

    private static IEnumerable<object[]> CommonIntervalData =>
      new[]
      {
        new object[] { new Inv.Date(2000, 1, 1), null, null, true },
        new object[] { new Inv.Date(2000, 1, 1), new Inv.Date(1999, 12, 31), null, true },
        new object[] { new Inv.Date(2000, 1, 1), null, new Inv.Date(2000, 1, 2), true },
        new object[] { new Inv.Date(2000, 1, 1), new Inv.Date(1999, 12, 31), new Inv.Date(2000, 1, 2), true },

        new object[] { new Inv.Date(2000, 1, 1), new Inv.Date(2000, 1, 2), null, false },
        new object[] { new Inv.Date(2000, 1, 1), null, new Inv.Date(1999, 12, 31), false },

        new object[] { new Inv.Date(2000, 1, 1), new Inv.Date(2000, 1, 2), new Inv.Date(1999, 12, 31), false }
      };
  }

  [TestClass]
  public class DateTimeOffsetTests
  {
    public static IEnumerable<object[]> StartOfDay_ProducesMidnightInSameZoneForDaysWithoutOffsetChangeData =>
      new[]
      {
        new object[] { WesternAustraliaStandardTime, new DateTimeOffset(2023, 12, 20, 11, 0, 0, TimeSpan.FromHours(8)), new DateTimeOffset(2023, 12, 20, 0, 0, 0, TimeSpan.FromHours(8)) },
        new object[] { AustralianEasternStandardTime, new DateTimeOffset(2023, 12, 20, 15, 0, 0, TimeSpan.FromHours(11)), new DateTimeOffset(2023, 12, 20, 0, 0, 0, TimeSpan.FromHours(11)) }
      };

    public static IEnumerable<object[]> EndOfDay_ProducesMidnightInSameZoneForDaysWithoutOffsetChangeData =>
      new[]
      {
        new object[] { WesternAustraliaStandardTime, new DateTimeOffset(2023, 12, 20, 11, 0, 0, TimeSpan.FromHours(8)), new DateTimeOffset(2023, 12, 21, 0, 0, 0, TimeSpan.FromHours(8)).AddTicks(-1) },
        new object[] { AustralianEasternStandardTime, new DateTimeOffset(2023, 12, 20, 15, 0, 0, TimeSpan.FromHours(11)), new DateTimeOffset(2023, 12, 21, 0, 0, 0, TimeSpan.FromHours(11)).AddTicks(-1) }
      };

    public static IEnumerable<object[]> StartOfDay_ProducesMidnightInSameZoneForTimesBeforeOffsetChangeData =>
      new[]
      {
        new object[] { AustralianEasternStandardTime, new DateTimeOffset(2022, 10, 2, 1, 0, 0, TimeSpan.FromHours(10)), new DateTimeOffset(2022, 10, 2, 0, 0, 0, TimeSpan.FromHours(10)) },
        new object[] { AustralianEasternStandardTime, new DateTimeOffset(2023, 4, 2, 2, 0, 0, TimeSpan.FromHours(11)), new DateTimeOffset(2023, 4, 2, 0, 0, 0, TimeSpan.FromHours(11)) }
      };

    public static IEnumerable<object[]> EndOfDay_ProducesMidnightInSameZoneForTimesAfterOffsetChangeData =>
      new[]
      {
        new object[] { AustralianEasternStandardTime, new DateTimeOffset(2022, 10, 2, 3, 0, 0, TimeSpan.FromHours(11)), new DateTimeOffset(2022, 10, 3, 0, 0, 0, TimeSpan.FromHours(11)).AddTicks(-1) },
        new object[] { AustralianEasternStandardTime, new DateTimeOffset(2023, 4, 2, 2, 0, 0, TimeSpan.FromHours(10)), new DateTimeOffset(2023, 4, 3, 0, 0, 0, TimeSpan.FromHours(10)).AddTicks(-1) }
      };

    public static IEnumerable<object[]> StartOfDay_ProducesMidnightInSameZoneButDifferentOffsetForTimesAfterOffsetChangeData =>
      new[]
      {
        new object[] { AustralianEasternStandardTime, new DateTimeOffset(2022, 10, 2, 3, 0, 0, TimeSpan.FromHours(11)), new DateTimeOffset(2022, 10, 2, 0, 0, 0, TimeSpan.FromHours(10)) },
        new object[] { AustralianEasternStandardTime, new DateTimeOffset(2023, 4, 2, 2, 0, 0, TimeSpan.FromHours(10)), new DateTimeOffset(2023, 4, 2, 0, 0, 0, TimeSpan.FromHours(11)) }
      };

    public static IEnumerable<object[]> EndOfDay_ProducesMidnightInSameZoneButDifferentOffsetForTimesBeforeOffsetChangeData =>
      new[]
      {
        new object[] { AustralianEasternStandardTime, new DateTimeOffset(2022, 10, 2, 1, 0, 0, TimeSpan.FromHours(10)), new DateTimeOffset(2022, 10, 3, 0, 0, 0, TimeSpan.FromHours(11)).AddTicks(-1) },
        new object[] { AustralianEasternStandardTime, new DateTimeOffset(2023, 4, 2, 2, 0, 0, TimeSpan.FromHours(11)), new DateTimeOffset(2023, 4, 3, 0, 0, 0, TimeSpan.FromHours(10)).AddTicks(-1) }
      };

    public static IEnumerable<object[]> StartOfDay_ProducesCorrectResultWhenOffsetChangesAtMidnightData =>
      new[]
      {
        new object[] { CubaStandardTime, new DateTimeOffset(2023, 3, 12, 1, 0, 0, TimeSpan.FromHours(-4)), new DateTimeOffset(2023, 3, 12, 1, 0, 0, TimeSpan.FromHours(-4)) },
        new object[] { CubaStandardTime, new DateTimeOffset(2023, 11, 5, 0, 30, 0, TimeSpan.FromHours(-4)), new DateTimeOffset(2023, 11, 5, 0, 0, 0, TimeSpan.FromHours(-4)) },
        new object[] { CubaStandardTime, new DateTimeOffset(2023, 11, 5, 1, 30, 0, TimeSpan.FromHours(-5)), new DateTimeOffset(2023, 11, 5, 0, 0, 0, TimeSpan.FromHours(-4)) }
      };

    public static IEnumerable<object[]> EndOfDay_ProducesCorrectResultWhenOffsetChangesAtMidnightData =>
      new[]
      {
        new object[] { CubaStandardTime, new DateTimeOffset(2023, 3, 12, 1, 0, 0, TimeSpan.FromHours(-4)), new DateTimeOffset(2023, 3, 13, 0, 0, 0, TimeSpan.FromHours(-4)).AddTicks(-1) },
        new object[] { CubaStandardTime, new DateTimeOffset(2023, 11, 5, 0, 30, 0, TimeSpan.FromHours(-4)), new DateTimeOffset(2023, 11, 6, 0, 0, 0, TimeSpan.FromHours(-5)).AddTicks(-1) },
        new object[] { CubaStandardTime, new DateTimeOffset(2023, 11, 5, 1, 30, 0, TimeSpan.FromHours(-5)), new DateTimeOffset(2023, 11, 6, 0, 0, 0, TimeSpan.FromHours(-5)).AddTicks(-1) }
      };

    public static IEnumerable<object[]> StartOfDay_ProducesCorrectResultWhenOffsetChangesAt11PMData =>
      new[]
      {
        new object[] { BangladeshStandardTime, new DateTimeOffset(2009, 6, 19, 22, 0, 0, TimeSpan.FromHours(6)), new DateTimeOffset(2009, 6, 19, 0, 0, 0, TimeSpan.FromHours(6)) },
        new object[] { BangladeshStandardTime, new DateTimeOffset(2009, 6, 19, 23, 0, 0, TimeSpan.FromHours(6)), new DateTimeOffset(2009, 6, 20, 0, 0, 0, TimeSpan.FromHours(7)) },
        new object[] { BangladeshStandardTime, new DateTimeOffset(2009, 12, 31, 23, 59, 59, 998, TimeSpan.FromHours(7)), new DateTimeOffset(2009, 12, 31, 0, 0, 0, TimeSpan.FromHours(7)) },
        new object[] { BangladeshStandardTime, new DateTimeOffset(2009, 12, 31, 23, 59, 59, 999, TimeSpan.FromHours(7)), new DateTimeOffset(2009, 12, 31, 0, 0, 0, TimeSpan.FromHours(7)) }
      };

    public static IEnumerable<object[]> EndOfDay_ProducesCorrectResultWhenOffsetChangesAt11PMData =>
      new[]
      {
        new object[] { BangladeshStandardTime, new DateTimeOffset(2009, 6, 19, 22, 0, 0, TimeSpan.FromHours(6)), new DateTimeOffset(2009, 6, 19, 23, 0, 0, TimeSpan.FromHours(6)).AddTicks(-1) },
        new object[] { BangladeshStandardTime, new DateTimeOffset(2009, 6, 19, 23, 0, 0, TimeSpan.FromHours(6)), new DateTimeOffset(2009, 6, 21, 0, 0, 0, TimeSpan.FromHours(7)).AddTicks(-1) },
        new object[] { BangladeshStandardTime, new DateTimeOffset(2009, 12, 31, 0, 0, 0, TimeSpan.FromHours(7)), new DateTimeOffset(2010, 1, 1, 0, 0, 0, TimeSpan.FromHours(6)).AddTicks(-1) }
      };

    public static IEnumerable<object[]> StartOfDay_ProducesCorrectResultWhenOffsetChangesOneMillisecondBeforeMidnightData =>
      new[]
      {
        new object[] { PacificSouthAmericaStandardTime, new DateTimeOffset(2022, 9, 10, 23, 59, 59, 998, TimeSpan.FromHours(-4)), new DateTimeOffset(2022, 9, 10, 0, 0, 0, TimeSpan.FromHours(-4)) },
        new object[] { PacificSouthAmericaStandardTime, new DateTimeOffset(2022, 9, 10, 23, 59, 59, 999, TimeSpan.FromHours(-4)), new DateTimeOffset(2022, 9, 11, 0, 59, 59, 999, TimeSpan.FromHours(-3)) },
        new object[] { PacificSouthAmericaStandardTime, new DateTimeOffset(2022, 9, 11, 1, 0, 0, TimeSpan.FromHours(-3)), new DateTimeOffset(2022, 9, 11, 0, 59, 59, 999, TimeSpan.FromHours(-3)) }
      };

    public static IEnumerable<object[]> EndOfDay_ProducesCorrectResultWhenOffsetChangesOneMillisecondBeforeMidnightData =>
      new[]
      {
        new object[] { PacificSouthAmericaStandardTime, new DateTimeOffset(2022, 9, 10, 0, 0, 0, TimeSpan.FromHours(-4)), new DateTimeOffset(2022, 9, 10, 23, 59, 59, 999, TimeSpan.FromHours(-4)).AddTicks(-1) },
        new object[] { PacificSouthAmericaStandardTime, new DateTimeOffset(2023, 4, 1, 0, 0, 0, TimeSpan.FromHours(-3)), new DateTimeOffset(2023, 4, 2, 0, 0, 0, TimeSpan.FromHours(-4)).AddTicks(-1) }
      };

    public static IEnumerable<object[]> StartOfDay_ProducesCorrectResultWhenBaseOffsetChangesBy24HoursData =>
      new[]
      {
        new object[] { SamoaStandardTime, new DateTimeOffset(2011, 12, 30, 23, 50, 0, TimeSpan.FromHours(-10)), new DateTimeOffset(2011, 12, 30, 0, 0, 0, TimeSpan.FromHours(-10)) },
        new object[] { SamoaStandardTime, new DateTimeOffset(2011, 12, 31, 0, 30, 0, TimeSpan.FromHours(-10)), new DateTimeOffset(2011, 12, 31, 0, 0, 0, TimeSpan.FromHours(-10)) },
        //new object[] { SamoaStandardTime, new DateTimeOffset(2012, 1, 1, 12, 0, 0, TimeSpan.FromHours(14)), new DateTimeOffset(2012, 1, 1, 1, 0, 0, TimeSpan.FromHours(14)) }
      };

    public static IEnumerable<object[]> EndOfDay_ProducesCorrectResultWhenBaseOffsetChangesBy24HoursData =>
      new[]
      {
        new object[] { SamoaStandardTime, new DateTimeOffset(2011, 12, 30, 23, 50, 0, TimeSpan.FromHours(-10)), new DateTimeOffset(2011, 12, 31, 0, 0, 0, TimeSpan.FromHours(-10)).AddTicks(-1) },
        //new object[] { SamoaStandardTime, new DateTimeOffset(2011, 12, 31, 0, 30, 0, TimeSpan.FromHours(-10)), new DateTimeOffset(2011, 12, 31, 1, 0, 0, TimeSpan.FromHours(-10)).AddTicks(-1) },
        new object[] { SamoaStandardTime, new DateTimeOffset(2012, 1, 1, 12, 0, 0, TimeSpan.FromHours(14)), new DateTimeOffset(2012, 1, 2, 0, 0, 0, TimeSpan.FromHours(14)).AddTicks(-1) }
      };

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(StartOfDay_ProducesMidnightInSameZoneForDaysWithoutOffsetChangeData))]
    public void StartOfDay_ProducesMidnightInSameZoneForDaysWithoutOffsetChange(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected) => TestStartOfDay(Zone, Source, Expected);

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(EndOfDay_ProducesMidnightInSameZoneForDaysWithoutOffsetChangeData))]
    public void EndOfDay_ProducesMidnightInSameZoneForDaysWithoutOffsetChange(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected) => TestEndOfDay(Zone, Source, Expected);

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(StartOfDay_ProducesMidnightInSameZoneForTimesBeforeOffsetChangeData))]
    public void StartOfDay_ProducesMidnightInSameZoneForTimesBeforeOffsetChange(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected) => TestStartOfDay(Zone, Source, Expected);

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(EndOfDay_ProducesMidnightInSameZoneForTimesAfterOffsetChangeData))]
    public void EndOfDay_ProducesMidnightInSameZoneForTimesAfterOffsetChange(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected) => TestEndOfDay(Zone, Source, Expected);

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(StartOfDay_ProducesMidnightInSameZoneButDifferentOffsetForTimesAfterOffsetChangeData))]
    public void StartOfDay_ProducesMidnightInSameZoneButDifferentOffsetForTimesAfterOffsetChange(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected) => TestStartOfDay(Zone, Source, Expected);

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(EndOfDay_ProducesMidnightInSameZoneButDifferentOffsetForTimesBeforeOffsetChangeData))]
    public void EndOfDay_ProducesMidnightInSameZoneButDifferentOffsetForTimesBeforeOffsetChange(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected) => TestEndOfDay(Zone, Source, Expected);

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(StartOfDay_ProducesCorrectResultWhenOffsetChangesAtMidnightData))]
    public void StartOfDay_ProducesCorrectResultWhenOffsetChangesAtMidnight(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected) => TestStartOfDay(Zone, Source, Expected);

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(EndOfDay_ProducesCorrectResultWhenOffsetChangesAtMidnightData))]
    public void EndOfDay_ProducesCorrectResultWhenOffsetChangesAtMidnight(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected) => TestEndOfDay(Zone, Source, Expected);

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(StartOfDay_ProducesCorrectResultWhenOffsetChangesAt11PMData))]
    public void StartOfDay_ProducesCorrectResultWhenOffsetChangesAt11PM(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected) => TestStartOfDay(Zone, Source, Expected);

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(EndOfDay_ProducesCorrectResultWhenOffsetChangesAt11PMData))]
    public void EndOfDay_ProducesCorrectResultWhenOffsetChangesAt11PM(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected) => TestEndOfDay(Zone, Source, Expected);

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(StartOfDay_ProducesCorrectResultWhenOffsetChangesOneMillisecondBeforeMidnightData))]
    public void StartOfDay_ProducesCorrectResultWhenOffsetChangesOneMillisecondBeforeMidnight(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected) => TestStartOfDay(Zone, Source, Expected);

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(EndOfDay_ProducesCorrectResultWhenOffsetChangesOneMillisecondBeforeMidnightData))]
    public void EndOfDay_ProducesCorrectResultWhenOffsetChangesOneMillisecondBeforeMidnight(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected) => TestEndOfDay(Zone, Source, Expected);

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(StartOfDay_ProducesCorrectResultWhenBaseOffsetChangesBy24HoursData))]
    public void StartOfDay_ProducesCorrectResultWhenBaseOffsetChangesBy24Hours(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected) => TestStartOfDay(Zone, Source, Expected);

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(EndOfDay_ProducesCorrectResultWhenBaseOffsetChangesBy24HoursData))]
    public void EndOfDay_ProducesCorrectResultWhenBaseOffsetChangesBy24Hours(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected) => TestEndOfDay(Zone, Source, Expected);

    [TestMethod, Timeout(60000)]
    public void StartOfDay_ProducesResultInRequestedTimeZone()
    {
      // MSTest throws an exception internally when this data is used as part of a DataTestMethod, so it's inlined here instead
      TestStartOfDay
      (
        Zone: LineIslandsStandardTime,
        Source: new DateTimeOffset(2023, 1, 1, 23, 0, 0, TimeSpan.FromHours(-12)),
        Expected: new DateTimeOffset(2023, 1, 3, 0, 0, 0, TimeSpan.FromHours(14))
      );

      TestStartOfDay
      (
        Zone: DatelineStandardTime,
        Source: new DateTimeOffset(2023, 1, 3, 1, 0, 0, TimeSpan.FromHours(14)),
        Expected: new DateTimeOffset(2023, 1, 1, 0, 0, 0, TimeSpan.FromHours(-12))
      );
    }

    [TestMethod, Timeout(60000)]
    public void EndOfDay_ProducesResultInRequestedTimeZone()
    {
      // MSTest throws an exception internally when this data is used as part of a DataTestMethod, so it's inlined here instead
      TestEndOfDay
      (
        Zone: LineIslandsStandardTime,
        Source: new DateTimeOffset(2023, 1, 1, 23, 0, 0, TimeSpan.FromHours(-12)),
        Expected: new DateTimeOffset(2023, 1, 4, 0, 0, 0, TimeSpan.FromHours(14)).AddTicks(-1)
      );

      TestEndOfDay
      (
        Zone: DatelineStandardTime,
        Source: new DateTimeOffset(2023, 1, 3, 1, 0, 0, TimeSpan.FromHours(14)),
        Expected: new DateTimeOffset(2023, 1, 2, 0, 0, 0, TimeSpan.FromHours(-12)).AddTicks(-1)
      );
    }

    private void TestStartOfDay(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected)
    {
      var Actual = Source.StartOfDay(Zone);

      Assert.AreEqual(Expected, Actual);
      Assert.AreEqual(Expected.Offset, Actual.Offset);
    }

    private void TestEndOfDay(TimeZoneInfo Zone, DateTimeOffset Source, DateTimeOffset Expected)
    {
      var Actual = Source.EndOfDay(Zone);

      Assert.AreEqual(Expected, Actual);
      Assert.AreEqual(Expected.Offset, Actual.Offset);
    }

    /// <summary>
    /// (UTC-12:00) International Date Line West
    /// </summary>
    private static TimeZoneInfo DatelineStandardTime { get; } = TimeZoneInfo.FindSystemTimeZoneById("Dateline Standard Time");
    /// <summary>
    /// (UTC-05:00) Havana
    /// </summary>
    /// <remarks>+1 from 2023-03-12 12am until 2023-11-05 1am</remarks>
    private static TimeZoneInfo CubaStandardTime { get; } = TimeZoneInfo.FindSystemTimeZoneById("Cuba Standard Time");
    /// <summary>
    /// (UTC-04:00) Santiago
    /// </summary>
    /// <remarks>+1 from 2022-09-10 11:59:59.999pm until 2023-04-01 11:59:59.999pm</remarks>
    private static TimeZoneInfo PacificSouthAmericaStandardTime { get; } = TimeZoneInfo.FindSystemTimeZoneById("Pacific SA Standard Time");
    /// <summary>
    /// (UTC+06:00) Dhaka
    /// </summary>
    /// <remarks>+1 from 2009-06-19 11pm until 2009-12-31 11:59:59.999pm</remarks>
    private static TimeZoneInfo BangladeshStandardTime { get; } = TimeZoneInfo.FindSystemTimeZoneById("Bangladesh Standard Time");
    /// <summary>
    /// (UTC+08:00) Perth
    /// </summary>
    /// <remarks>+1 from 2008-10-26 2am until 2009-03-29 3am</remarks>
    private static TimeZoneInfo WesternAustraliaStandardTime { get; } = TimeZoneInfo.FindSystemTimeZoneById("W. Australia Standard Time");
    /// <summary>
    /// (UTC+10:00) Canberra, Melbourne, Sydney
    /// </summary>
    /// <remarks>+1 from 2022-10-02 2am until 2023-04-02 3am</remarks>
    private static TimeZoneInfo AustralianEasternStandardTime { get; } = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
    /// <summary>
    /// (UTC+13:00) Samoa
    /// </summary>
    /// <remarks>Had a 24 hour jump from UTC-10 to UTC+14 on 2012-01-01 1am when their base offset was changed from -11 to +13</remarks>
    private static TimeZoneInfo SamoaStandardTime { get; } = TimeZoneInfo.FindSystemTimeZoneById("Samoa Standard Time");
    /// <summary>
    /// (UTC+14:00) Kiritimati Island
    /// </summary>
    private static TimeZoneInfo LineIslandsStandardTime { get; } = TimeZoneInfo.FindSystemTimeZoneById("Line Islands Standard Time");
  }

  [TestClass]
  public class DateRangeTests
  {
    [TestMethod, Timeout(60000)]
    public void Compare()
    {
      if (!Inv.DateRangeSet.Empty.IsEqualTo(Inv.DateRangeSet.Empty))
        Assert.Fail("Empty date range comparison fail.");

      if (!Inv.DateRangeSet.Universal.IsEqualTo(Inv.DateRangeSet.Universal))
        Assert.Fail("Universal date range comparison fail.");

      var LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 15));
      var RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 15));

      if (!LeftDateRangeSet.IsEqualTo(RightDateRangeSet))
        Assert.Fail("Single date range comparison fail.");

      LeftDateRangeSet = Inv.DateRangeSet.FromDateRangeArray(new[] { new Inv.DateRange(new Inv.Date(2013, 09, 08), new Inv.Date(2013, 09, 15)), new Inv.DateRange(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 15)) });
      RightDateRangeSet = Inv.DateRangeSet.FromDateRangeArray(new[] { new Inv.DateRange(new Inv.Date(2013, 09, 08), new Inv.Date(2013, 09, 15)), new Inv.DateRange(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 15)) });

      if (!LeftDateRangeSet.IsEqualTo(RightDateRangeSet))
        Assert.Fail("Multiple date range comparison fail.");
    }

    [TestMethod, Timeout(60000)]
    public void Union()
    {
      var LeftDateRangeSet = new Inv.DateRangeSet();
      var RightDateRangeSet = new Inv.DateRangeSet();
      var ResultDateRangeSet = new Inv.DateRangeSet();
      if (!LeftDateRangeSet.Union(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #1 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2014, 01, 24), new Inv.Date(2014, 01, 26));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2014, 01, 27), null);
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2014, 01, 24), null);
      if (!LeftDateRangeSet.Union(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #2 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      if (!LeftDateRangeSet.Union(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #3 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 10), new Inv.Date(2013, 08, 15));
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 15));
      if (!LeftDateRangeSet.Union(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #4 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 11), new Inv.Date(2013, 08, 15));
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 15));
      if (!LeftDateRangeSet.Union(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #5 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(null, new Inv.Date(2013, 08, 09));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      ResultDateRangeSet = new Inv.DateRangeSet(null, new Inv.Date(2013, 08, 10));
      if (!LeftDateRangeSet.Union(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #6 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(null, new Inv.Date(2013, 08, 09));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2013, 08, 11), new Inv.Date(2013, 08, 18)));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 06), new Inv.Date(2013, 08, 13));
      ResultDateRangeSet = new Inv.DateRangeSet(null, new Inv.Date(2013, 08, 18));
      if (!LeftDateRangeSet.Union(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #7 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2011, 10, 22), new Inv.Date(2013, 07, 22));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2011, 10, 22), new Inv.Date(2012, 10, 22)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2012, 10, 18), new Inv.Date(2013, 01, 18)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2012, 12, 29), new Inv.Date(2013, 03, 29)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2013, 03, 15), new Inv.Date(2013, 06, 15)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2013, 07, 20), new Inv.Date(2013, 10, 20)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2013, 10, 21), new Inv.Date(2014, 01, 21)));
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2011, 10, 22), new Inv.Date(2014, 01, 21));
      if (!LeftDateRangeSet.IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #8 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2004, 12, 27), new Inv.Date(2005, 12, 27));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2006, 04, 18), new Inv.Date(2007, 04, 18)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2008, 06, 27), new Inv.Date(2009, 06, 27)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2009, 06, 29), new Inv.Date(2010, 06, 29)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2010, 06, 29), new Inv.Date(2011, 06, 29)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2011, 07, 06), new Inv.Date(2012, 07, 06)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2012, 07, 06), new Inv.Date(2013, 07, 06)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2013, 07, 06), new Inv.Date(2014, 07, 06)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2003, 11, 04), new Inv.Date(2014, 07, 05)));
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2003, 11, 04), new Inv.Date(2014, 07, 06));
      if (!LeftDateRangeSet.IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #9 fail.");
    }

    [TestMethod, Timeout(60000)]
    public void Intersect()
    {
      var LeftDateRangeSet = new Inv.DateRangeSet();
      var RightDateRangeSet = new Inv.DateRangeSet();
      var ResultDateRangeSet = new Inv.DateRangeSet();

      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #1 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #2 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 10), new Inv.Date(2013, 08, 15));
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 10), new Inv.Date(2013, 08, 10));
      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #3 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 11), new Inv.Date(2013, 08, 15));
      ResultDateRangeSet = Inv.DateRangeSet.Empty;
      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #4 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(null, new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 11), null);
      ResultDateRangeSet = Inv.DateRangeSet.Empty;
      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #5 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(null, new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 01), null);
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 01), new Inv.Date(2013, 08, 10));
      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #6 fail.");

      LeftDateRangeSet = Inv.DateRangeSet.FromDateRangeArray(new[] { new Inv.DateRange(null, new Inv.Date(2013, 08, 01)), new Inv.DateRange(new Inv.Date(2013, 08, 15), null) });
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 13));
      ResultDateRangeSet = Inv.DateRangeSet.Empty;
      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #7 fail.");

      LeftDateRangeSet = Inv.DateRangeSet.FromDateRangeArray(new[] { new Inv.DateRange(null, new Inv.Date(2013, 08, 01)), new Inv.DateRange(new Inv.Date(2013, 08, 15), null) });
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 15), null);
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 15), null);
      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #8 fail.");
    }

    [TestMethod]
    public void Subtract()
    {
      var LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 12), new Inv.Date(2013, 09, 12));
      var RightDateRangeSet = Inv.DateRangeSet.FromDateRangeArray(new[] { new Inv.DateRange(new Inv.Date(2013, 08, 01), new Inv.Date(2013, 08, 22)), new Inv.DateRange(new Inv.Date(2013, 08, 26), new Inv.Date(2013, 11, 29)) });
      var ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 23), new Inv.Date(2013, 08, 25));
      if (!LeftDateRangeSet.Subtract(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range subtract fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2014, 01, 24), new Inv.Date(2014, 02, 24));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2014, 01, 24), new Inv.Date(2014, 01, 26));
      RightDateRangeSet = RightDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2014, 01, 27), null));
      ResultDateRangeSet = Inv.DateRangeSet.Empty;
      if (!LeftDateRangeSet.Subtract(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range subtract fail.");
    }

    [TestMethod, Timeout(60000)]
    public void Invert()
    {
      var NothingSet = new Inv.DateRangeSet();
      var EverythingSet = NothingSet.Invert();
      if (!(EverythingSet.Count == 1 && EverythingSet.FirstRange.From == null && EverythingSet.FirstRange.Until == null))
        Assert.Fail("Nothing -> Everything Invert set fail.");
      NothingSet = EverythingSet.Invert();
      if (!(NothingSet.Count == 0))
        Assert.Fail("Everything -> Nothing Invert set fail.");

      var SingleSet = new Inv.DateRangeSet(new Inv.Date(2013, 10, 10), new Inv.Date(2013, 10, 15));
      var DoubleSet = SingleSet.Invert();
      if (!(DoubleSet.Count == 2 && DoubleSet.FirstRange.From == null && DoubleSet.FirstRange.Until == new Inv.Date(2013, 10, 09) && DoubleSet.LastRange.From == new Inv.Date(2013, 10, 16) && DoubleSet.LastRange.Until == null))
        Assert.Fail("Single -> Double Invert set fail.");

      var TwoSet = Inv.DateRangeSet.FromDateRangeArray(new[] { new Inv.DateRange(null, new Inv.Date(2013, 10, 10)), new Inv.DateRange(new Inv.Date(2013, 10, 15), null) });
      var OneSet = TwoSet.Invert();
      if (!(OneSet.Count == 1 && OneSet.FirstRange.From == new Inv.Date(2013, 10, 11) && OneSet.FirstRange.Until == new Inv.Date(2013, 10, 14)))
        Assert.Fail("Two -> One Invert set fail.");
    }

    [TestMethod, Timeout(60000)]
    public void Intersects()
    {
      var DateRangeSet = new Inv.DateRangeSet(new Inv.Date(2014, 01, 01), new Inv.Date(2014, 01, 05));
      var Range = new Inv.DateRange(new Inv.Date(2014, 01, 05), new Inv.Date(2014, 01, 10));

      if (!DateRangeSet.Intersects(Range))
        Assert.Fail("Expecting the ranges to intersect as they are inclusive.");
    }

    [TestMethod, Timeout(60000)]
    public void AsWordedApproximation()
    {
      const int DaysInAMonth = 30;
      // the constant above is duplicated from DateRangeHelper.ToApproximation()

      var StartDate = new Inv.Date(2014, 01, 01);

      if (new Inv.DateRange(StartDate, StartDate).ToApproximation() != "Today")
        Assert.Fail("Failed to correctly approximate a today range");

      if (new Inv.DateRange(StartDate, StartDate.AddDays(1)).ToApproximation() != "Tomorrow")
        Assert.Fail("Failed to correctly approximate a tomorrow range");

      if (new Inv.DateRange(StartDate, StartDate.AddDays(2)).ToApproximation() != "2 days")
        Assert.Fail("Failed to correctly approximate a 2 day range");

      if (new Inv.DateRange(StartDate, StartDate.AddDays(7)).ToApproximation() != "7 days")
        Assert.Fail("Failed to correctly approximate a 7 day range");

      if (new Inv.DateRange(StartDate, StartDate.AddDays(13)).ToApproximation() != "13 days")
        Assert.Fail("Failed to correctly approximate a 13 day range");

      if (new Inv.DateRange(StartDate, StartDate.AddWeeks(2)).ToApproximation() != "2 weeks")
        Assert.Fail("Failed to correctly approximate a 2 week range");

      if (new Inv.DateRange(StartDate, StartDate.AddWeeks(2).AddDays(1)).ToApproximation() != "2 weeks")
        Assert.Fail("Failed to correctly approximate a 2 week 1 day range");

      if (new Inv.DateRange(StartDate, StartDate.AddWeeks(2).AddDays(4)).ToApproximation() != "2 weeks")
        Assert.Fail("Failed to correctly approximate a 2 week 4 day range");

      if (new Inv.DateRange(StartDate, StartDate.AddWeeks(2).AddDays(5)).ToApproximation() != "3 weeks")
        Assert.Fail("Failed to correctly approximate a 2 week 5 day range");

      var WeekStartDate = StartDate.AddWeeks(4);

      if (new Inv.DateRange(StartDate, WeekStartDate.AddDays(-1)).ToApproximation() != "4 weeks")
        Assert.Fail("Failed to correctly approximate a 4 week - 1 day range");

      if (new Inv.DateRange(StartDate, WeekStartDate).ToApproximation() != "4 weeks")
        Assert.Fail("Failed to correctly approximate a 4 week - 1 day range");

      if (new Inv.DateRange(StartDate, WeekStartDate.AddDays(1)).ToApproximation() != "4 weeks")
        Assert.Fail("Failed to correctly approximate a 4 week 1 day range");

      if (new Inv.DateRange(StartDate, WeekStartDate.AddDays(2)).ToApproximation() != "1 month")
        Assert.Fail("Failed to correctly approximate a 4 week 2 day range");

      var MonthStartDate = StartDate.AddDays(DaysInAMonth);

      if (new Inv.DateRange(StartDate, MonthStartDate.AddDays(14)).ToApproximation() != "1 month")
        Assert.Fail("Failed to correctly approximate a 1 month and 14 day range");

      if (new Inv.DateRange(StartDate, MonthStartDate.AddDays(15)).ToApproximation() != "1 month")
        Assert.Fail("Failed to correctly approximate a 1 month and 15 day range");

      if (new Inv.DateRange(StartDate, MonthStartDate.AddDays(16)).ToApproximation() != "2 months")
        Assert.Fail("Failed to correctly approximate a 1 month and 16 day range");

      if (new Inv.DateRange(StartDate, StartDate.AddDays(DaysInAMonth * 2)).ToApproximation() != "2 months")
        Assert.Fail("Failed to correctly approximate a 2 month range");
    }

    [TestMethod, Timeout(60000)]
    public void HUMEMembershipSuspensionUnionMerge()
    {
      var LeftDateRangeSet = Inv.DateRangeSet.Empty;
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2017, 09, 12), new Inv.Date(2017, 09, 24));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 01, 13), new Inv.Date(2018, 01, 29));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 04, 03), new Inv.Date(2018, 04, 17));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 05, 07), new Inv.Date(2018, 05, 28));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 05, 01), new Inv.Date(2018, 05, 06));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 05, 29), new Inv.Date(2018, 06, 03));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 06, 15), new Inv.Date(2018, 07, 23));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 07, 24), new Inv.Date(2018, 09, 03));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 06, 04), new Inv.Date(2018, 06, 14));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 09, 17), new Inv.Date(2018, 10, 30));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 12, 24), new Inv.Date(2019, 01, 13));

      var ResultDateRangeSet = Inv.DateRangeSet.Empty;
      ResultDateRangeSet = ResultDateRangeSet.Union(new Inv.Date(2017, 09, 12), new Inv.Date(2017, 09, 24));
      ResultDateRangeSet = ResultDateRangeSet.Union(new Inv.Date(2018, 01, 13), new Inv.Date(2018, 01, 29));
      ResultDateRangeSet = ResultDateRangeSet.Union(new Inv.Date(2018, 04, 03), new Inv.Date(2018, 04, 17));
      ResultDateRangeSet = ResultDateRangeSet.Union(new Inv.Date(2018, 05, 01), new Inv.Date(2018, 09, 03));
      ResultDateRangeSet = ResultDateRangeSet.Union(new Inv.Date(2018, 09, 17), new Inv.Date(2018, 10, 30));
      ResultDateRangeSet = ResultDateRangeSet.Union(new Inv.Date(2018, 12, 24), new Inv.Date(2019, 01, 13));

      if (!ResultDateRangeSet.IsEqualTo(LeftDateRangeSet))
        Assert.Fail("Failed to perform union");
    }
  }

  [TestClass]
  public class TimeTests
  {
    [TestMethod, Timeout(60000)]
    public void String()
    {
      var AustralianCulture = new System.Globalization.CultureInfo("en-AU");
      var UnitedStatesCulture = new System.Globalization.CultureInfo("en-US");
      var CurrentCulture = System.Globalization.CultureInfo.CurrentCulture;
      var InvariantCulture = System.Globalization.CultureInfo.InvariantCulture;

      var TestTime = new Inv.Time(13, 58);
      var TestDateTime = TestTime.ToDateTime();

      Assert.IsTrue(
        string.Equals(TestTime.ToString(null, AustralianCulture), TestDateTime.ToString(AustralianCulture.DateTimeFormat.ShortTimePattern, AustralianCulture)),
        "Australian time format");

      Assert.IsTrue(
        string.Equals(TestTime.ToString(null, UnitedStatesCulture), TestDateTime.ToString(UnitedStatesCulture.DateTimeFormat.ShortTimePattern, UnitedStatesCulture)),
        "United States time format");

      Assert.IsTrue(
        string.Equals(TestTime.ToString(), TestDateTime.ToString(CurrentCulture.DateTimeFormat.ShortTimePattern, CurrentCulture)),
        ".ToString() should equal the short time pattern for the current culture");

      Assert.IsTrue(
        string.Equals(TestTime.ToString("HHmm"), TestDateTime.ToString("HHmm", CurrentCulture)),
        ".ToString(string) should equal that pattern for the current culture");

      Assert.IsTrue(
        string.Equals(TestTime.ToString("HHmm", CurrentCulture), TestDateTime.ToString("HHmm", CurrentCulture)),
        ".ToString(string, IFormatProvider) should be equal to the same method call on DateTime");

      Assert.IsTrue(
        string.Equals(TestTime.ToString(null, CurrentCulture), TestDateTime.ToString(CurrentCulture.DateTimeFormat.ShortTimePattern, CurrentCulture)),
        ".ToString(null, IFormatProvider) should be equal to the same method call on DateTime with a short time format");

      Assert.IsTrue(
        string.Equals(TestTime.ToString(null, InvariantCulture), TestDateTime.ToString(InvariantCulture.DateTimeFormat.ShortTimePattern, InvariantCulture)),
        ".ToString(null, IFormatProvider) should be equal to the same method call on DateTime with a short time format");
    }
  }

  [TestClass]
  public class TimeRangeTests
  {
    [TestMethod, Timeout(60000)]
    public void Compare()
    {
      if (!Inv.TimeRangeSet.Empty.IsEqualTo(Inv.TimeRangeSet.Empty))
        Assert.Fail("Empty Time range comparison fail.");

      if (!Inv.TimeRangeSet.Universal.IsEqualTo(Inv.TimeRangeSet.Universal))
        Assert.Fail("Universal Time range comparison fail.");

      var LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(11, 00)));
      var RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(11, 00)));

      if (!LeftTimeRangeSet.IsEqualTo(RightTimeRangeSet))
        Assert.Fail("Single Time range comparison fail.");

      LeftTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(11, 00)), new Inv.TimeRange(new Inv.Time(07, 00), new Inv.Time(08, 00)) });
      RightTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(11, 00)), new Inv.TimeRange(new Inv.Time(07, 00), new Inv.Time(08, 00)) });

      if (!LeftTimeRangeSet.IsEqualTo(RightTimeRangeSet))
        Assert.Fail("Multiple Time range comparison fail.");
    }

    [TestMethod, Timeout(60000)]
    public void Union()
    {
      var LeftTimeRangeSet = new Inv.TimeRangeSet();
      var RightTimeRangeSet = new Inv.TimeRangeSet();
      var ResultTimeRangeSet = new Inv.TimeRangeSet();

      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #1 fail.");

      LeftTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(09, 00), new Inv.Time(10, 00)), new Inv.TimeRange(new Inv.Time(12, 00), new Inv.Time(14, 30)) });
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(12, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(09, 00), new Inv.Time(14, 30)));

      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #2 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(09, 00), new Inv.Time(11, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      ResultTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(09, 00), new Inv.Time(11, 00)), new Inv.TimeRange(new Inv.Time(12, 00), null) });
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #3 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(09, 00), new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(09, 00), null));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #3 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #4 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(15, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(15, 00)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #5 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(11, 00), new Inv.Time(15, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)), new Inv.TimeRange(new Inv.Time(11, 00), new Inv.Time(15, 00)) });
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #6 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(09, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(10, 00)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #7 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(09, 00)));
      LeftTimeRangeSet = LeftTimeRangeSet.Union(new Inv.TimeRange(new Inv.Time(11, 00), new Inv.Time(18, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(06, 00), new Inv.Time(13, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(18, 00)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #8 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(18, 00), new Inv.Time(23, 59)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(17, 00), new Inv.Time(17, 59)));
      ResultTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(17, 00), new Inv.Time(17, 59)), new Inv.TimeRange(new Inv.Time(18, 00), new Inv.Time(23, 59)) });
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #9 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(17, 00), new Inv.Time(17, 59)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(18, 00), new Inv.Time(23, 59)));
      ResultTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(17, 00), new Inv.Time(17, 59)), new Inv.TimeRange(new Inv.Time(18, 00), new Inv.Time(23, 59)) });
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #10 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(03, 00), new Inv.Time(23, 59)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(12, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(03, 00), new Inv.Time(23, 59)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #11 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(03, 00), new Inv.Time(23, 59)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(03, 00), new Inv.Time(23, 59)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #12 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(23, 59)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(23, 59)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #13 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(23, 59)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(12, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(23, 59)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #14 fail.");
    }

    [TestMethod, Timeout(60000)]
    public void Intersect()
    {
      var LeftTimeRangeSet = new Inv.TimeRangeSet();
      var RightTimeRangeSet = new Inv.TimeRangeSet();
      var ResultTimeRangeSet = new Inv.TimeRangeSet();

      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #1 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #2 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), null));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #3 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(10, 01)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), null));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(10, 01)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #4 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), null));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(10, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #5 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), null));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(10, 01)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(10, 01)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #6 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(11, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #7 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #8 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(12, 30)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), new Inv.Time(12, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #9 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 30), new Inv.Time(14, 30)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 30), new Inv.Time(14, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #10 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(11, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #11 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(12, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #12 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(12, 30)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), new Inv.Time(12, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #13 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 30), new Inv.Time(14, 30)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 30), new Inv.Time(14, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #14 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 30), new Inv.Time(14, 30)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #15 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(11, 30), new Inv.Time(14, 30)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(11, 30), new Inv.Time(12, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #16 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), new Inv.Time(14, 30)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #17 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 30), new Inv.Time(14, 30)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(12, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #18 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(11, 30), new Inv.Time(14, 30)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(12, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(11, 30), new Inv.Time(12, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #19 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), new Inv.Time(14, 30)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(12, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #20 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(15, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #21 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(11, 00), new Inv.Time(15, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #22 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(11, 00), null));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #23 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), null));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(10, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #24 fail.");

      LeftTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(null, new Inv.Time(01, 00)), new Inv.TimeRange(new Inv.Time(15, 00), null) });
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(13, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #25 fail.");

      LeftTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(null, new Inv.Time(01, 00)), new Inv.TimeRange(new Inv.Time(15, 00), null) });
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(15, 00), null));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(15, 00), null));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #26 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #27 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(01, 30)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(01, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #28 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 30), new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 30), new Inv.Time(02, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #29 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 15), new Inv.Time(01, 45)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 15), new Inv.Time(01, 45)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #30 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #31 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(01, 30)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(01, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #32 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 30), new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 30), new Inv.Time(02, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #33 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 15), new Inv.Time(01, 45)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 15), new Inv.Time(01, 45)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #34 fail.");
    }

    [TestMethod, Timeout(60000)]
    public void Subtract()
    {
      var LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 12), new Inv.Time(09, 12)));
      var RightTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(08, 01), new Inv.Time(08, 22)), new Inv.TimeRange(new Inv.Time(08, 26), new Inv.Time(11, 29)) });
      var ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 22), new Inv.Time(08, 26)));
      if (!LeftTimeRangeSet.Subtract(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range subtract #1 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(06, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(03, 00)));
      RightTimeRangeSet = RightTimeRangeSet.Union(new Inv.TimeRange(new Inv.Time(04, 00), null));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(03, 00), new Inv.Time(04, 00)));
      if (!LeftTimeRangeSet.Subtract(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range subtract #2 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 24), new Inv.Time(02, 24)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 24), new Inv.Time(01, 26)));
      RightTimeRangeSet = RightTimeRangeSet.Union(new Inv.TimeRange(new Inv.Time(01, 26), null));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Subtract(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range subtract #3 fail.");

    }

    [TestMethod, Timeout(60000)]
    public void Invert()
    {
      var NothingSet = new Inv.TimeRangeSet();
      var EverythingSet = NothingSet.Invert();
      if (!(EverythingSet.Count == 1 && EverythingSet.FirstRange.From == null && EverythingSet.FirstRange.Until == null))
        Assert.Fail("Nothing -> Everything Invert set fail.");

      NothingSet = EverythingSet.Invert();
      if (!(NothingSet.Count == 0))
        Assert.Fail("Everything -> Nothing Invert set fail.");

      var SingleSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 10), new Inv.Time(10, 15)));
      var DoubleSet = SingleSet.Invert();
      if (!(DoubleSet.Count == 2 && DoubleSet.FirstRange.From == null && DoubleSet.FirstRange.Until == new Inv.Time(10, 10) && DoubleSet.LastRange.From == new Inv.Time(10, 15) && DoubleSet.LastRange.Until == null))
        Assert.Fail("Single -> Double Invert set fail.");

      var TwoSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(null, new Inv.Time(10, 10)), new Inv.TimeRange(new Inv.Time(10, 15), null) });
      var OneSet = TwoSet.Invert();
      if (!(OneSet.Count == 1 && OneSet.FirstRange.From == new Inv.Time(10, 10) && OneSet.FirstRange.Until == new Inv.Time(10, 15)))
        Assert.Fail("Two -> One Invert set fail.");
    }

    [TestMethod, Timeout(60000)]
    public void Intersects()
    {
      var TimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 01), new Inv.Time(01, 05)));
      var Range = new Inv.TimeRange(new Inv.Time(01, 05), new Inv.Time(01, 10));

      if (TimeRangeSet.Intersects(Range))
        Assert.Fail("Expecting the ranges to NOT intersect as they are exclusive.");

      TimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 01), new Inv.Time(01, 06)));
      Range = new Inv.TimeRange(new Inv.Time(01, 05), new Inv.Time(01, 10));

      if (!TimeRangeSet.Intersects(Range))
        Assert.Fail("Expecting the ranges to intersect as they are exclusive.");
    }
  }

  [TestClass]
  public class DateTimeRangeTests
  {
    [TestMethod, Timeout(60000)]
    public void Compare()
    {
      if (!Inv.DateTimeRangeSet.Empty.IsEqualTo(Inv.DateTimeRangeSet.Empty))
        Assert.Fail("Empty Time range comparison fail.");

      if (!Inv.DateTimeRangeSet.Universal.IsEqualTo(Inv.DateTimeRangeSet.Universal))
        Assert.Fail("Universal Time range comparison fail.");

      var LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), Inv.Date.Today + new Inv.Time(11, 00)));
      var RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), Inv.Date.Today + new Inv.Time(11, 00)));

      if (!LeftTimeRangeSet.IsEqualTo(RightTimeRangeSet))
        Assert.Fail("Single Time range comparison fail.");

      LeftTimeRangeSet = Inv.DateTimeRangeSet.FromDateTimeRangeArray(new[] { new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), Inv.Date.Today + new Inv.Time(11, 00)), new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(07, 00), Inv.Date.Today + new Inv.Time(08, 00)) });
      RightTimeRangeSet = Inv.DateTimeRangeSet.FromDateTimeRangeArray(new[] { new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), Inv.Date.Today + new Inv.Time(11, 00)), new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(07, 00), Inv.Date.Today + new Inv.Time(08, 00)) });

      if (!LeftTimeRangeSet.IsEqualTo(RightTimeRangeSet))
        Assert.Fail("Multiple Time range comparison fail.");
    }

    [TestMethod, Timeout(60000)]
    public void Union()
    {
      var LeftTimeRangeSet = new Inv.DateTimeRangeSet();
      var RightTimeRangeSet = new Inv.DateTimeRangeSet();
      var ResultTimeRangeSet = new Inv.DateTimeRangeSet();

      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #1 fail.");

      LeftTimeRangeSet = Inv.DateTimeRangeSet.FromDateTimeRangeArray(new[] { new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(09, 00), Inv.Date.Today + new Inv.Time(10, 00)), new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), Inv.Date.Today + new Inv.Time(14, 30)) });
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), Inv.Date.Today + new Inv.Time(12, 00)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(09, 00), Inv.Date.Today + new Inv.Time(14, 30)));

      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #2 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(09, 00), Inv.Date.Today + new Inv.Time(11, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), null));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.FromDateTimeRangeArray(new[] { new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(09, 00), Inv.Date.Today + new Inv.Time(11, 00)), new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), null) });
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #3 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(09, 00), Inv.Date.Today + new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), null));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(09, 00), null));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #3 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(10, 00)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(10, 00)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #4 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), Inv.Date.Today + new Inv.Time(15, 00)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(15, 00)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #5 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(11, 00), Inv.Date.Today + new Inv.Time(15, 00)));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.FromDateTimeRangeArray(new[] { new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(10, 00)), new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(11, 00), Inv.Date.Today + new Inv.Time(15, 00)) });
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #6 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(09, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(10, 00)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(10, 00)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #7 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(09, 00)));
      LeftTimeRangeSet = LeftTimeRangeSet.Union(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(11, 00), Inv.Date.Today + new Inv.Time(18, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(06, 00), Inv.Date.Today + new Inv.Time(13, 00)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(18, 00)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #8 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(18, 00), Inv.Date.Today + new Inv.Time(23, 59)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(17, 00), Inv.Date.Today + new Inv.Time(17, 59)));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.FromDateTimeRangeArray(new[] { new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(17, 00), Inv.Date.Today + new Inv.Time(17, 59)), new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(18, 00), Inv.Date.Today + new Inv.Time(23, 59)) });
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #9 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(17, 00), Inv.Date.Today + new Inv.Time(17, 59)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(18, 00), Inv.Date.Today + new Inv.Time(23, 59)));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.FromDateTimeRangeArray(new[] { new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(17, 00), Inv.Date.Today + new Inv.Time(17, 59)), new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(18, 00), Inv.Date.Today + new Inv.Time(23, 59)) });
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #10 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(03, 00), Inv.Date.Today + new Inv.Time(23, 59)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(12, 00)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(03, 00), Inv.Date.Today + new Inv.Time(23, 59)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #11 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(03, 00), Inv.Date.Today + new Inv.Time(23, 59)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(03, 00), Inv.Date.Today + new Inv.Time(23, 59)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #12 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(23, 59)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(23, 59)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #13 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(23, 59)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(12, 00)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(23, 59)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #14 fail.");
    }

    [TestMethod, Timeout(60000)]
    public void Intersect()
    {
      var LeftTimeRangeSet = new Inv.DateTimeRangeSet();
      var RightTimeRangeSet = new Inv.DateTimeRangeSet();
      var ResultTimeRangeSet = new Inv.DateTimeRangeSet();

      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #1 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(10, 00)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(10, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #2 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), null));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #3 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(10, 01)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), null));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), Inv.Date.Today + new Inv.Time(10, 01)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #4 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), null));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(10, 00)));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #5 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), null));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(10, 01)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), Inv.Date.Today + new Inv.Time(10, 01)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #6 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), Inv.Date.Today + new Inv.Time(11, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), null));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #7 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), Inv.Date.Today + new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), null));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #8 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), Inv.Date.Today + new Inv.Time(12, 30)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), null));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), Inv.Date.Today + new Inv.Time(12, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #9 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 30), Inv.Date.Today + new Inv.Time(14, 30)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), null));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 30), Inv.Date.Today + new Inv.Time(14, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #10 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), null));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), Inv.Date.Today + new Inv.Time(11, 00)));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #11 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), null));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), Inv.Date.Today + new Inv.Time(12, 00)));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #12 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), null));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), Inv.Date.Today + new Inv.Time(12, 30)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), Inv.Date.Today + new Inv.Time(12, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #13 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), null));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 30), Inv.Date.Today + new Inv.Time(14, 30)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 30), Inv.Date.Today + new Inv.Time(14, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #14 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 30), Inv.Date.Today + new Inv.Time(14, 30)));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #15 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(11, 30), Inv.Date.Today + new Inv.Time(14, 30)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(11, 30), Inv.Date.Today + new Inv.Time(12, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #16 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), Inv.Date.Today + new Inv.Time(14, 30)));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #17 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 30), Inv.Date.Today + new Inv.Time(14, 30)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(12, 00)));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #18 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(11, 30), Inv.Date.Today + new Inv.Time(14, 30)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(12, 00)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(11, 30), Inv.Date.Today + new Inv.Time(12, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #19 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(12, 00), Inv.Date.Today + new Inv.Time(14, 30)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(12, 00)));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #20 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 00), Inv.Date.Today + new Inv.Time(15, 00)));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #21 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(11, 00), Inv.Date.Today + new Inv.Time(15, 00)));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #22 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(11, 00), null));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #23 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), null));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(10, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #24 fail.");

      LeftTimeRangeSet = Inv.DateTimeRangeSet.FromDateTimeRangeArray(new[] { new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(01, 00)), new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(15, 00), null) });
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 00), Inv.Date.Today + new Inv.Time(13, 00)));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #25 fail.");

      LeftTimeRangeSet = Inv.DateTimeRangeSet.FromDateTimeRangeArray(new[] { new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(01, 00)), new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(15, 00), null) });
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(15, 00), null));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(15, 00), null));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #26 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(02, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #27 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(01, 30)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(01, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #28 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 30), Inv.Date.Today + new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 30), Inv.Date.Today + new Inv.Time(02, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #29 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 15), Inv.Date.Today + new Inv.Time(01, 45)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 15), Inv.Date.Today + new Inv.Time(01, 45)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #30 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(02, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #31 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(01, 30)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(01, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #32 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 30), Inv.Date.Today + new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 30), Inv.Date.Today + new Inv.Time(02, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #33 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 15), Inv.Date.Today + new Inv.Time(01, 45)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 15), Inv.Date.Today + new Inv.Time(01, 45)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #34 fail.");
    }

    [TestMethod, Timeout(60000)]
    public void Subtract()
    {
      var LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 12), Inv.Date.Today + new Inv.Time(09, 12)));
      var RightTimeRangeSet = Inv.DateTimeRangeSet.FromDateTimeRangeArray(new[] { new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 01), Inv.Date.Today + new Inv.Time(08, 22)), new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 26), Inv.Date.Today + new Inv.Time(11, 29)) });
      var ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(08, 22), Inv.Date.Today + new Inv.Time(08, 26)));
      if (!LeftTimeRangeSet.Subtract(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range subtract #1 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(06, 00)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 00), Inv.Date.Today + new Inv.Time(03, 00)));
      RightTimeRangeSet = RightTimeRangeSet.Union(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(04, 00), null));
      ResultTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(03, 00), Inv.Date.Today + new Inv.Time(04, 00)));
      if (!LeftTimeRangeSet.Subtract(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range subtract #2 fail.");

      LeftTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 24), Inv.Date.Today + new Inv.Time(02, 24)));
      RightTimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 24), Inv.Date.Today + new Inv.Time(01, 26)));
      RightTimeRangeSet = RightTimeRangeSet.Union(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 26), null));
      ResultTimeRangeSet = Inv.DateTimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Subtract(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range subtract #3 fail.");

    }

    [TestMethod, Timeout(60000)]
    public void Invert()
    {
      var NothingSet = new Inv.DateTimeRangeSet();
      var EverythingSet = NothingSet.Invert();
      if (!(EverythingSet.Count == 1 && EverythingSet.FirstRange.From == null && EverythingSet.FirstRange.Until == null))
        Assert.Fail("Nothing -> Everything Invert set fail.");

      NothingSet = EverythingSet.Invert();
      if (!(NothingSet.Count == 0))
        Assert.Fail("Everything -> Nothing Invert set fail.");

      var SingleSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 10), Inv.Date.Today + new Inv.Time(10, 15)));
      var DoubleSet = SingleSet.Invert();
      if (!(DoubleSet.Count == 2 && DoubleSet.FirstRange.From == null && DoubleSet.FirstRange.Until == Inv.Date.Today + new Inv.Time(10, 10) && DoubleSet.LastRange.From == Inv.Date.Today + new Inv.Time(10, 15) && DoubleSet.LastRange.Until == null))
        Assert.Fail("Single -> Double Invert set fail.");

      var TwoSet = Inv.DateTimeRangeSet.FromDateTimeRangeArray(new[] { new Inv.DateTimeRange(null, Inv.Date.Today + new Inv.Time(10, 10)), new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(10, 15), null) });
      var OneSet = TwoSet.Invert();
      if (!(OneSet.Count == 1 && OneSet.FirstRange.From == Inv.Date.Today + new Inv.Time(10, 10) && OneSet.FirstRange.Until == Inv.Date.Today + new Inv.Time(10, 15)))
        Assert.Fail("Two -> One Invert set fail.");
    }

    [TestMethod, Timeout(60000)]
    public void Intersects()
    {
      var TimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 01), Inv.Date.Today + new Inv.Time(01, 05)));
      var Range = new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 05), Inv.Date.Today + new Inv.Time(01, 10));

      if (TimeRangeSet.Intersects(Range))
        Assert.Fail("Expecting the ranges to NOT intersect as they are exclusive.");

      TimeRangeSet = new Inv.DateTimeRangeSet(new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 01), Inv.Date.Today + new Inv.Time(01, 06)));
      Range = new Inv.DateTimeRange(Inv.Date.Today + new Inv.Time(01, 05), Inv.Date.Today + new Inv.Time(01, 10));

      if (!TimeRangeSet.Intersects(Range))
        Assert.Fail("Expecting the ranges to intersect as they are exclusive.");
    }
  }

  [TestClass]
  public class XmlSerializationTests
  {
    [TestMethod, Timeout(60000)]
    public void RoundTrip()
    {
      using (var MemoryStream = new System.IO.MemoryStream())
      {
        var FirstPackage = new XmlSerializationPackage()
        {
          Date = new Inv.Date(2013, 9, 3),
          Time = new Inv.Time(04, 01, 02, 03),
          TimePeriod = new Inv.TimePeriod(Years: 1, Months: 2, Weeks: 0, Days: 4, Hours: 5, Minutes: 6, Seconds: 7, Milliseconds: 0)
        }; // weeks and milliseconds are not supported in Xml.

        var BinaryFormatter = new DataContractSerializer(typeof(XmlSerializationPackage));
        BinaryFormatter.WriteObject(MemoryStream, FirstPackage);

        MemoryStream.Flush();

        MemoryStream.Position = 0;

        var SecondPackage = (XmlSerializationPackage)BinaryFormatter.ReadObject(MemoryStream);

        if (FirstPackage.Date != SecondPackage.Date)
          Assert.Fail("Xml Serialization Date fail.");

        if (FirstPackage.Time != SecondPackage.Time)
          Assert.Fail("Xml Serialization Time fail.");

        if (FirstPackage.TimePeriod != SecondPackage.TimePeriod)
          Assert.Fail("Xml Serialization TimePeriod fail.");
      }
    }

    [DataContract]
    private sealed class XmlSerializationPackage
    {
      [DataMember]
      public Inv.Date Date { get; set; }
      [DataMember]
      public Inv.Time Time { get; set; }
      [DataMember]
      public Inv.TimePeriod TimePeriod { get; set; }
    }
  }
}