﻿using System;
using System.Text;
using System.Security.Cryptography;
using Inv.Support;

namespace InvSupport
{
  [TestClass]
  public class IEnumerableHelperTests
  {
    public static IEnumerable<object[]> FirstOrNullData =>
      new[]
      {
        new object[] { Array.Empty<int>(), default(int?) },
        new object[] { new[] { 1 }, 1 },
        new object[] { new[] { 2, 3 }, 2 }
      };

    public static IEnumerable<object[]> LastOrNullData =>
      new[]
      {
        new object[] { Array.Empty<int>(), default(int?) },
        new object[] { new[] { 1 }, 1 },
        new object[] { new[] { 2, 3 }, 3 }
      };

    public static IEnumerable<object[]> ContainsAllData =>
      new[]
      {
        new object[] { Enumerable.Empty<int>(), Enumerable.Empty<int>(), true },
        new object[] { Enumerable.Range(1, 5), Enumerable.Empty<int>(), true },
        new object[] { Enumerable.Empty<int>(), Enumerable.Range(1, 5), false },
        new object[] { new[] { 1, 2, 3 }, new[] { 4, 5, 6 }, false },
        new object[] { new[] { 1, 2, 3 }, new[] { 2, 3, 4 }, false },
        new object[] { new[] { 1, 2, 3 }, new[] { 3, 2, 1 }, true },
        new object[] { new[] { 1, 2, 3 }, new[] { 3, 1 }, true },
        new object[] { new[] { 1, 2, 2, 3 }, new[] { 1, 2, 3 }, true },
        new object[] { new[] { 1, 2, 3 }, new[] { 1, 2, 2, 3 }, false }
      };

    [TestMethod, Timeout(60000)]
    public void OrderByWithNulls()
    {
      var List = new int?[] { null, 2, 1, 5 };

      var OrderList = List.OrderByWithNullsFirst(C => C).ToList();

      CollectionAssert.AreEqual(new int?[] { null, 1, 2, 5 }, OrderList);

      OrderList = List.OrderByWithNullsLast(C => C).ToList();

      CollectionAssert.AreEqual(new int?[] { 1, 2, 5, null }, OrderList);
    }

    [TestMethod, Timeout(60000)]
    public void NextOrLast()
    {
      var Source = new[] { 1, 2, 3, 2, 4 };

      Assert.AreEqual(1, Source.NextOrLast(10));
      Assert.AreEqual(2, Source.NextOrLast(1));
      Assert.AreEqual(3, Source.NextOrLast(2));
      Assert.AreEqual(4, Source.NextOrLast(4));

      Assert.AreEqual(1, Source.NextOrLast(10, _ => true));
      Assert.AreEqual(2, Source.NextOrLast(1, _ => true));
      Assert.AreEqual(3, Source.NextOrLast(2, _ => true));
      Assert.AreEqual(4, Source.NextOrLast(4, _ => true));

      Assert.AreEqual(4, Source.NextOrLast(10, _ => false));
      Assert.AreEqual(4, Source.NextOrLast(1, _ => false));
      Assert.AreEqual(4, Source.NextOrLast(2, _ => false));
      Assert.AreEqual(4, Source.NextOrLast(4, _ => false));

      Assert.AreEqual(2, Source.NextOrLast(10, X => X != 1));
      Assert.AreEqual(3, Source.NextOrLast(1, X => X != 2));
      Assert.AreEqual(2, Source.NextOrLast(2, X => X != 3));
      Assert.AreEqual(4, Source.NextOrLast(4, X => X != 4));

      Source = Array.Empty<int>();

      Assert.ThrowsException<ArgumentOutOfRangeException>(() => Source.NextOrLast(10));

      Assert.ThrowsException<ArgumentOutOfRangeException>(() => Source.NextOrLast(10, _ => false));
    }

    [TestMethod, Timeout(60000)]
    public void PreviousOrFirst()
    {
      var Source = new[] { 1, 2, 3, 2, 4 };

      Assert.AreEqual(1, Source.PreviousOrFirst(10));
      Assert.AreEqual(1, Source.PreviousOrFirst(1));
      Assert.AreEqual(1, Source.PreviousOrFirst(2));
      Assert.AreEqual(2, Source.PreviousOrFirst(4));

      Assert.AreEqual(1, Source.PreviousOrFirst(10, _ => true));
      Assert.AreEqual(1, Source.PreviousOrFirst(1, _ => true));
      Assert.AreEqual(1, Source.PreviousOrFirst(2, _ => true));
      Assert.AreEqual(2, Source.PreviousOrFirst(4, _ => true));

      Assert.AreEqual(1, Source.PreviousOrFirst(10, _ => false));
      Assert.AreEqual(1, Source.PreviousOrFirst(1, _ => false));
      Assert.AreEqual(1, Source.PreviousOrFirst(2, _ => false));
      Assert.AreEqual(1, Source.PreviousOrFirst(4, _ => false));

      Assert.AreEqual(1, Source.PreviousOrFirst(10, X => X != 1));
      Assert.AreEqual(1, Source.PreviousOrFirst(1, X => X != 2));
      Assert.AreEqual(1, Source.PreviousOrFirst(2, X => X != 3));
      Assert.AreEqual(3, Source.PreviousOrFirst(4, X => X != 2));

      Source = Array.Empty<int>();

      Assert.ThrowsException<ArgumentOutOfRangeException>(() => Source.PreviousOrFirst(10));

      Assert.ThrowsException<ArgumentOutOfRangeException>(() => Source.PreviousOrFirst(10, _ => false));
    }

    [TestMethod, Timeout(60000)]
    public void DistinctPartitionOnOrderedSource()
    {
      var Source = new[] { 1, 1, 1, 2, 2, 3 };
      var Expected = new[]
      {
        new[] { 1 },
        new[] { 1 },
        new[] { 1, 2 },
        new[] { 2, 3 }
      };

      var Actual = Source.DistinctPartition(X => X);

      AssertPartitionsEqual(Expected, Actual);
    }

    [TestMethod, Timeout(60000)]
    public void DistinctPartitionOnOptimalSource()
    {
      var Source = new[] { 1, 2, 3, 1, 2, 1 };
      var Expected = new[]
      {
        new[] { 1, 2, 3 },
        new[] { 1, 2 },
        new[] { 1 }
      };

      var Actual = Source.DistinctPartition(X => X);

      AssertPartitionsEqual(Expected, Actual);
    }

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(FirstOrNullData))]
    public void FirstOrNull(IEnumerable<int> Source, int? Expected)
    {
      var Actual = Source.FirstOrNull();

      Assert.AreEqual(Expected, Actual);
    }

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(LastOrNullData))]
    public void LastOrNull(IEnumerable<int> Source, int? Expected)
    {
      var Actual = Source.LastOrNull();

      Assert.AreEqual(Expected, Actual);
    }

    [TestMethod, Timeout(60000)]
    [DataTestMethod]
    [DynamicData(nameof(ContainsAllData))]
    public void ContainsAll(IEnumerable<int> Source, IEnumerable<int> Items, bool Expected)
    {
      var Actual = Source.ContainsAll(Items);

      Assert.AreEqual(Expected, Actual);
    }

    private void AssertPartitionsEqual<T>(IEnumerable<IEnumerable<T>> Expected, IEnumerable<IEnumerable<T>> Actual)
    {
      using (var ExpectedEnumerator = Expected.GetEnumerator())
      using (var ActualEnumerator = Actual.GetEnumerator())
      {
        bool ExpectedHasNext;
        bool ActualHasNext;

        do
        {
          ExpectedHasNext = ExpectedEnumerator.MoveNext();
          ActualHasNext = ActualEnumerator.MoveNext();

          Assert.AreEqual(ExpectedHasNext, ActualHasNext);

          if (ExpectedHasNext && ActualHasNext)
            CollectionAssert.AreEqual(ExpectedEnumerator.Current.ToArray(), ActualEnumerator.Current.ToArray());
        }
        while (ExpectedHasNext && ActualHasNext);
      }
    }
  }

  [TestClass]
  public class DoubleHelperTests
  {
    [TestMethod, Timeout(60000)]
    public void PrecisionComparison()
    {
      void Test(double A, double B, double Precision, int ExpectedResult)
      {
        Assert.AreEqual(ExpectedResult, A.PrecisionCompareTo(B, Precision), $"Comparing {A} to {B} with precision {Precision}");
        Assert.AreEqual(ExpectedResult * -1, B.PrecisionCompareTo(A, Precision), $"Comparing {B} to {A} with precision {Precision}");
        Assert.AreEqual(ExpectedResult == 0, A.PrecisionEquals(B, Precision), $"Testing equality for {A} and {B} with precision {Precision}");
        Assert.AreEqual(ExpectedResult == 0, B.PrecisionEquals(A, Precision), $"Testing equality for {B} and {A} with precision {Precision}");
      }

      Test(double.NaN, double.NaN, 0, 0);
      Test(double.NaN, double.PositiveInfinity, 0, -1);
      Test(double.NaN, double.NegativeInfinity, 0, -1);
      Test(double.PositiveInfinity, double.PositiveInfinity, 0, 0);
      Test(double.PositiveInfinity, double.NegativeInfinity, 0, 1);
      Test(double.NegativeInfinity, double.NegativeInfinity, 0, 0);

      Test(1.0, 1.0, 0, 0);
      Test(1.000000005, 1.0, 0.000000005, 0);
      Test(1.0000000051, 1.0, 0.000000005, 1);
      Test(1.0000000049, 1.0, 0.000000005, 0);
      Test(0.999999995, 1.0, 0.000000005, 0);
      Test(0.9999999949, 1.0, 0.000000005, -1);
    }
  }

  [TestClass]
  public class DateTimeHelperTests
  {
    [TestMethod, Timeout(60000)]
    public void IsContiguousRange()
    {
      var Today = Inv.Date.Today;

      DateTime New(int Hour, int Minute)
      {
        return Today + new Inv.Time(Hour, Minute);
      }

      // Exact match.
      Assert.IsTrue(Inv.Support.DateTimeHelper.IsContiguousRange(New(09, 00), New(10, 00), New(09, 00), New(10, 00)));

      // Contiguous on end of first period.
      Assert.IsTrue(Inv.Support.DateTimeHelper.IsContiguousRange(New(09, 00), New(10, 00), New(10, 00), New(11, 00)));

      // Contiguous on end of second period.
      Assert.IsTrue(Inv.Support.DateTimeHelper.IsContiguousRange(New(09, 00), New(10, 00), New(08, 00), New(09, 00)));

      // First period contains second period.
      Assert.IsTrue(Inv.Support.DateTimeHelper.IsContiguousRange(New(09, 00), New(10, 00), New(09, 15), New(09, 45)));

      // Second period contains first period.
      Assert.IsTrue(Inv.Support.DateTimeHelper.IsContiguousRange(New(09, 00), New(10, 00), New(08, 00), New(11, 00)));

      // True intersection 1.
      Assert.IsTrue(Inv.Support.DateTimeHelper.IsContiguousRange(New(09, 00), New(10, 00), New(09, 30), New(11, 00)));

      // True intersection 2.
      Assert.IsTrue(Inv.Support.DateTimeHelper.IsContiguousRange(New(09, 00), New(10, 00), New(08, 30), New(09, 30)));
    }

    [TestMethod, Timeout(60000)]
    public void AsDateTimeOffset()
    {
      var AestZone = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");

      var RegularTime = new DateTime(2024, 6, 1, 0, 0, 0);
      var RegularTimeExpected = new DateTimeOffset(2024, 6, 1, 0, 0, 0, TimeSpan.FromHours(10));
      var RegularTimeActual = RegularTime.AsDateTimeOffset(AestZone);
      Assert.AreEqual(RegularTimeExpected, RegularTimeActual);

      var AdjustedTime = new DateTime(2024, 1, 1, 0, 0, 0);
      var AdjustedTimeExpected = new DateTimeOffset(2024, 1, 1, 0, 0, 0, TimeSpan.FromHours(11));
      var AdjustedTimeActual = AdjustedTime.AsDateTimeOffset(AestZone);
      Assert.AreEqual(AdjustedTimeExpected, AdjustedTimeActual);

      var InvalidTime = new DateTime(2023, 10, 1, 2, 0, 0);
      var InvalidTimeExpected = new DateTimeOffset(2023, 10, 1, 2, 0, 0, TimeSpan.FromHours(10));
      var InvalidTimeActual = InvalidTime.AsDateTimeOffset(AestZone);
      Assert.AreEqual(InvalidTimeExpected, InvalidTimeActual);

      var AmbiguousTime = new DateTime(2024, 4, 7, 2, 0, 0);
      var AmbiguousTimeExpected = new DateTimeOffset(2024, 4, 7, 2, 0, 0, TimeSpan.FromHours(10));
      var AmbiguousTimeActual = AmbiguousTime.AsDateTimeOffset(AestZone);
      Assert.AreEqual(AmbiguousTimeExpected, AmbiguousTimeActual);

      var GreenlandZone = TimeZoneInfo.FindSystemTimeZoneById("Greenland Standard Time");

      var NegativeAdjustedTime = new DateTime(2023, 2, 1, 0, 0, 0);
      var NegativeAdjustedTimeExpected = new DateTimeOffset(2023, 2, 1, 0, 0, 0, TimeSpan.FromHours(-3));
      var NegativeAdjustedTimeActual = NegativeAdjustedTime.AsDateTimeOffset(GreenlandZone);
      Assert.AreEqual(NegativeAdjustedTimeExpected, NegativeAdjustedTimeActual);

      var NegativeAdjustmentInvalidTime = new DateTime(2023, 3, 25, 22, 0, 0);
      var NegativeAdjustmentInvalidTimeExpected = new DateTimeOffset(2023, 3, 25, 22, 0, 0, TimeSpan.FromHours(-2));
      var NegativeAdjustmentInvalidTimeActual = NegativeAdjustmentInvalidTime.AsDateTimeOffset(GreenlandZone);
      Assert.AreEqual(NegativeAdjustmentInvalidTimeExpected, NegativeAdjustmentInvalidTimeActual);

      var LocalTime = DateTime.Now;
      Assert.ThrowsException<ArgumentException>(() =>
      {
        LocalTime.AsDateTimeOffset(AestZone);
        LocalTime.AsDateTimeOffset(GreenlandZone);
      });

      var UtcTime = DateTime.UtcNow;
      Assert.ThrowsException<ArgumentException>(() =>
      {
        UtcTime.AsDateTimeOffset(AestZone);
        UtcTime.AsDateTimeOffset(GreenlandZone);
      });
    }
  }

  [TestClass]
  public sealed class NumberHelperTests
  {
    public static IEnumerable<object[]> ClampData =>
      new[]
      {
        new object[] { -5, 4, 9, 4 },
        new object[] { 4, 4, 9, 4 },
        new object[] { 6, 4, 9, 6 },
        new object[] { 9, 4, 9, 9 },
        new object[] { 15, 4, 9, 9 },
        new object[] { -3, 2, 2, 2 },
        new object[] { 2, 2, 2, 2 },
        new object[] { 8, 2, 2, 2 }
      };

    public static IEnumerable<object[]> ClampDoubleData =>
      new[]
      {
        new object[] { double.NaN, 4d, 9d, double.NaN },
        new object[] { 4d, double.NaN, 9d, 4d },
        new object[] { 15d, double.NaN, 9d, 9d },
        new object[] { 5d, 4d, double.NaN, 5d },
        new object[] { 3d, 4d, double.NaN, 4d },
        new object[] { 5d, double.NaN, double.NaN, 5d }
      };

    [DataTestMethod]
    [DynamicData(nameof(ClampData))]
    [Timeout(60000)]
    public void Clamp(int Value, int Min, int Max, int Expected)
    {
      var Actual = Value.Clamp(Min, Max);

      Assert.AreEqual(Expected, Actual);
    }

    [TestMethod]
    [Timeout(60000)]
    public void Clamp_FailsInvAssertIfMinIsGreaterThanMax()
    {
      if (!Inv.Assert.IsEnabled)
        return;

      Assert.ThrowsException<Exception>(() =>
      {
        5.Clamp(8, 3);
      });
    }

    [DataTestMethod]
    [DynamicData(nameof(ClampData))]
    [Timeout(60000)]
    public void Clamp_Double(double Value, double Min, double Max, double Expected)
    {
      var Actual = Value.Clamp(Min, Max);

      Assert.AreEqual(Expected, Actual);
    }
  }

  [TestClass]
  public class RandomNumberGeneratorHelperTests
  {
    public static IEnumerable<object[]> GetInt_ProducesValueInclusiveOfMinAndExclusiveOfMaxData =>
      new[]
      {
        new object[] { 0, 2 },
        new object[] { int.MinValue, int.MinValue + 2 },
        new object[] { int.MaxValue - 2, int.MaxValue },
      };

    [DataTestMethod]
    [DynamicData(nameof(GetInt_ProducesValueInclusiveOfMinAndExclusiveOfMaxData))]
    [Timeout(60000)]
    public void GetInt_ProducesValueInclusiveOfMinAndExclusiveOfMax(int MinValue, int MaxValue)
    {
      var Actual = Generator.GetInt(MinValue, MaxValue);

      Assert.IsTrue(MinValue <= Actual && Actual < MaxValue, "Actual ({1}) must be greater than or equal to MinValue ({0}) and less than MaxValue ({2})", MinValue, Actual, MaxValue);
    }

    [TestMethod, Timeout(60000)]
    public void GetInt_ReturnsMinWhenMaxEqualsMin()
    {
      var MinValue = 5;
      var MaxValue = MinValue;

      var Actual = Generator.GetInt(MinValue, MaxValue);

      Assert.AreEqual(MinValue, Actual);
    }

    [TestMethod, Timeout(60000)]
    public void GetInt_ThrowsIfMinIsGreaterThanMax()
    {
      var MinValue = 2;
      var MaxValue = 1;

      Assert.ThrowsException<ArgumentOutOfRangeException>(() =>
      {
        Generator.GetInt(MinValue, MaxValue);
      });
    }

    [ClassInitialize]
    public static void TestFixtureSetup(TestContext Context)
    {
      Generator = RandomNumberGenerator.Create();
    }

    [ClassCleanup]
    public static void TestFixtureTearDown()
    {
      Generator?.Dispose();
      Generator = null;
    }

    private static RandomNumberGenerator Generator;
  }

  [TestClass]
  public class StringHelperTests
  {
    [TestMethod, Timeout(60000)]
    public void AsSeparatedText()
    {
      var TextList = new[]
      {
        new SeparatedTextClass("1"),
        new SeparatedTextClass("2"),
        new SeparatedTextClass("3"),
        new SeparatedTextClass("4"),
        new SeparatedTextClass("5"),
        new SeparatedTextClass(null)
      };

      Assert.AreEqual("1 and 5 others", Inv.Support.StringHelper.AsShortSeparatedText(TextList, T => T.Format(), MaximumDisplayCount: 1));
      Assert.AreEqual("1, 2 and 4 others", Inv.Support.StringHelper.AsShortSeparatedText(TextList, T => T.Format(), MaximumDisplayCount: 2));
      Assert.AreEqual("1, 2, 3 and 3 others", Inv.Support.StringHelper.AsShortSeparatedText(TextList, T => T.Format(), MaximumDisplayCount: 3));
      Assert.AreEqual("1, 2, 3, 4 and 2 others", Inv.Support.StringHelper.AsShortSeparatedText(TextList, T => T.Format(), MaximumDisplayCount: 4));
      Assert.AreEqual("1, 2, 3, 4, 5 and 1 other", Inv.Support.StringHelper.AsShortSeparatedText(TextList, T => T.Format(), MaximumDisplayCount: 5));
      Assert.ThrowsException<SeparatedTextClassException>(() => Inv.Support.StringHelper.AsShortSeparatedText(TextList, T => T.Format(), MaximumDisplayCount: 6));
    }

    private sealed class SeparatedTextClass
    {
      public SeparatedTextClass(string Value)
      {
        this.Value = Value;
      }

      public string Format()
      {
        if (Value == null)
          throw new SeparatedTextClassException($"This {nameof(SeparatedTextClass)} should not have Format() called.");

        return Value;
      }

      private readonly string Value;
    }

    private sealed class SeparatedTextClassException : Exception
    {
      public SeparatedTextClassException(string Message) : base(Message)
      {
      }
    }
  }

  [TestClass]
  public class TimeHelperTests
  {
    [TestMethod, Timeout(60000)]
    public void OverlapsMinutes()
    {
      void Check(bool Expected, Inv.Time FromA, Inv.Time UntilA, Inv.Time FromB, Inv.Time UntilB)
      {
        Assert.AreEqual(Expected, TimeHelper.OverlapsMinutes(FromA, UntilA, FromB, UntilB));
        Assert.AreEqual(Expected, TimeHelper.OverlapsMinutes(FromB, UntilB, FromA, UntilA));
      }

      Check(false, new Inv.Time(08, 00), new Inv.Time(09, 00), new Inv.Time(06, 00), new Inv.Time(08, 00));
      Check(true, new Inv.Time(08, 00), new Inv.Time(09, 00), new Inv.Time(06, 00), new Inv.Time(08, 30));
      Check(true, new Inv.Time(08, 00), new Inv.Time(09, 00), new Inv.Time(06, 00), new Inv.Time(09, 00));
      Check(true, new Inv.Time(08, 00), new Inv.Time(09, 00), new Inv.Time(06, 00), new Inv.Time(10, 00));

      Check(false, new Inv.Time(08, 00), new Inv.Time(09, 00), new Inv.Time(07, 00), new Inv.Time(08, 00));
      Check(true, new Inv.Time(08, 00), new Inv.Time(09, 00), new Inv.Time(07, 00), new Inv.Time(08, 01));
      Check(true, new Inv.Time(08, 00), new Inv.Time(09, 00), new Inv.Time(07, 00), new Inv.Time(09, 00));
      Check(true, new Inv.Time(08, 00), new Inv.Time(09, 00), new Inv.Time(07, 00), new Inv.Time(10, 00));

      Check(true, new Inv.Time(08, 00), new Inv.Time(09, 00), new Inv.Time(08, 00), new Inv.Time(08, 30));
      Check(true, new Inv.Time(08, 00), new Inv.Time(09, 00), new Inv.Time(08, 00), new Inv.Time(09, 00));
      Check(true, new Inv.Time(08, 00), new Inv.Time(09, 00), new Inv.Time(08, 00), new Inv.Time(10, 00));

      Check(true, Inv.Time.Midnight, new Inv.Time(04, 00), Inv.Time.Midnight, new Inv.Time(02, 00));
      Check(true, Inv.Time.Midnight, new Inv.Time(04, 00), Inv.Time.Midnight, new Inv.Time(04, 00));
      Check(true, Inv.Time.Midnight, new Inv.Time(04, 00), Inv.Time.Midnight, new Inv.Time(06, 00));
      Check(true, Inv.Time.Midnight, new Inv.Time(04, 00), new Inv.Time(02, 00), new Inv.Time(06, 00));
      Check(true, Inv.Time.Midnight, new Inv.Time(04, 00), new Inv.Time(02, 00), new Inv.Time(04, 00));
      Check(false, Inv.Time.Midnight, new Inv.Time(04, 00), new Inv.Time(04, 00), new Inv.Time(06, 00));

      Check(false, new Inv.Time(22, 00), Inv.Time.Midnight, new Inv.Time(20, 00), new Inv.Time(22, 00));
      Check(true, new Inv.Time(22, 00), Inv.Time.Midnight, new Inv.Time(20, 00), new Inv.Time(23, 00));
      Check(true, new Inv.Time(22, 00), Inv.Time.Midnight, new Inv.Time(20, 00), Inv.Time.Midnight);
      Check(true, new Inv.Time(22, 00), Inv.Time.Midnight, new Inv.Time(22, 00), Inv.Time.Midnight);
      Check(true, new Inv.Time(22, 00), Inv.Time.Midnight, new Inv.Time(23, 00), Inv.Time.Midnight);
    }
  }

  [TestClass]
  public class TimeSpanHelperTests
  {
    [TestMethod, Timeout(60000)]
    public void FormatTimeSpanShortYears()
    {
      string Format(TimeSpan Value) => Value.FormatTimeSpanShort();

      Assert.AreEqual("6 years", Format(TimeSpan.FromDays(365 * 6)));
      Assert.AreEqual("5 years", Format(TimeSpan.FromDays(365 * 5)));
      Assert.AreEqual("4.7 years", Format(TimeSpan.FromDays(365 * 4.7)));
      Assert.AreEqual("4.5 years", Format(TimeSpan.FromDays(365 * 4.5)));
      Assert.AreEqual("4.2 years", Format(TimeSpan.FromDays(365 * 4.2)));
      Assert.AreEqual("4 years", Format(TimeSpan.FromDays(365 * 4)));
      Assert.AreEqual("3.7 years", Format(TimeSpan.FromDays(365 * 3.7)));
      Assert.AreEqual("3.5 years", Format(TimeSpan.FromDays(365 * 3.5)));
      Assert.AreEqual("3.2 years", Format(TimeSpan.FromDays(365 * 3.2)));
      Assert.AreEqual("3 years", Format(TimeSpan.FromDays(365 * 3)));
      Assert.AreEqual("2.7 years", Format(TimeSpan.FromDays(365 * 2.7)));
      Assert.AreEqual("2.5 years", Format(TimeSpan.FromDays(365 * 2.5)));
      Assert.AreEqual("2.2 years", Format(TimeSpan.FromDays(365 * 2.2)));
      Assert.AreEqual("2 years", Format(TimeSpan.FromDays(365 * 2)));
      Assert.AreEqual("1.7 years", Format(TimeSpan.FromDays(365 * 1.7)));
      Assert.AreEqual("1.5 years", Format(TimeSpan.FromDays(365 * 1.5)));
      Assert.AreEqual("1.2 years", Format(TimeSpan.FromDays(365 * 1.2)));
      Assert.AreEqual("1 year", Format(TimeSpan.FromDays(365 * 1)));
    }
  }

  [TestClass]
  public class PhoneHelperTests
  {
    [TestMethod, Timeout(60000)]
    public void TextFormatNumberForAustralia()
    {
      void Check(string Expected, string Test) => Assert.AreEqual(Expected, PhoneHelper.FormatPhoneNumberForAustralia(Test));

      // Formatting criteria sourced from:
      // https://en.wikipedia.org/wiki/National_conventions_for_writing_telephone_numbers#Australia
      // https://en.wikipedia.org/wiki/Telephone_numbers_in_Australia

      // Containing non-numeric content
      Check("test", "test");
      Check("9202 5555", "92025555 mum");
      Check("9450 2222", "9450 2222 work");

      // Local Numbers
      Check("9202 5555", "92025555");
      Check("9450 2222", "9450 2222");

      // Geographic Numbers
      Check("08 9202 5555", "0892025555");
      Check("03 9450 2222", "03 9450 2222");

      // Nongeographic Numbers
      Check("1800 920 255", "1800920255");
      Check("1300 450 222", "1300450222");
      Check("180 4502", "1804502");
      Check("13 24 56", "13 24 56");
      Check("13 24 56", "132456");

      // Mobile Numbers
      Check("0401 747 452", "0401747452");
      Check("0401 747 452", "0401 747 452");

      // International Mobile Numbers
      Check("+61 401 747 452", "+61401747452");
      Check("+61 401 747 452", "61401747452");

      // International Geographic Numbers
      Check("+61 3 9450 2222", "+61394502222");
      Check("+61 3 9450 2222", "+61 3 9450 2222");
      Check("+61 3 9450 2222", "+61 03 9450 2222");
      Check("+61 3 9450 2222", "61394502222");
      Check("+61 3 9450 2222", "610394502222");

      // Not formattable so returned unaltered
      Check("1234", "1234");
      Check("12341800920255", "12341800920255");
      Check("12341300450222", "12341300450222");
      Check("+123461394502222", "+123461394502222");
      Check("3 29450 32222", "3 29450 32222");
      Check("13 24 563", "13 24 563");
    }
    [TestMethod, Timeout(60000)]
    public void TextNumberFormatForNewZealand()
    {
      void Check(string Expected, string Test) => Assert.AreEqual(Expected, PhoneHelper.FormatPhoneNumberForNewZealand(Test));

      // Formatting criteria sourced from:
      // https://en.wikipedia.org/wiki/National_conventions_for_writing_telephone_numbers#New_Zealand
      // https://en.wikipedia.org/wiki/Telephone_numbers_in_New_Zealand

      // containing non-numeric content
      Check("test", "test");
      Check("431 5555", "4315555 mum");
      Check("754 2222", "754 2222 work");

      // local numbers
      Check("431 5555", "4315555");
      Check("754 2222", "754 2222");

      // geographic numbers
      Check("09 431 5555", "094315555");
      Check("06 754 2222", "06 754 2222");

      // non-geographic numbers

      Check("0800 345 678", "0800345678");
      Check("0508 123 987", "0508 123 987");

      // mobile numbers
      Check("021 588 577", "021588577");
      Check("021 567 3111", "021 567 3111");
      Check("021 3874 6555", "02138746555");

      // international mobile numbers
      Check("+64 21 588 577", "+6421588577");
      Check("+64 21 567 3111", "64215673111");
      Check("+64 21 3874 6555", "+642138746555");

      // international geographic numbers
      Check("+64 9 431 5555", "+6494315555");
      Check("+64 9 431 5555", "+64 9 431 5555");
      Check("+64 9 431 5555", "+64 09 431 5555");
      Check("+64 9 431 5555", "6494315555");
      Check("+64 9 431 5555", "64094315555");

      // not formattable so return unaltered
      Check("1234", "1234");
      Check("12341800920255", "12341800920255");
      Check("12341300450222", "12341300450222");
      Check("+123461394502222", "+123461394502222");
      Check("3 29450 32222", "3 29450 32222");
      Check("13 24 56", "13 24 56");
    }
  }
}
