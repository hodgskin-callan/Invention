﻿using System;

namespace InvJson
{
  [TestClass]
  public class SerializerTests
  {
    [TestMethod, Timeout(60000)]
    public void Converter()
    {
      var A = new Test()
      {
        Colour = Inv.Colour.Red,
        ColourNull = null,
        Date = Inv.Date.Now,
        DateNull = null,
        Time = Inv.Time.Now,
        TimeNull = null,
        TimePeriod = Inv.TimePeriod.FromMinutes(5),
        TimePeriodNull = null,
        Money = new Inv.Money(25.25m),
        MoneyNull = null
      };

      var Value = Newtonsoft.Json.JsonConvert.SerializeObject(A, Inv.JsonHelper.NewSerializerSettings());

      var B = Newtonsoft.Json.JsonConvert.DeserializeObject<Test>(Value, Inv.JsonHelper.NewSerializerSettings());

      Assert.AreEqual(A.Colour, B.Colour);
      Assert.AreEqual(A.ColourNull, B.ColourNull);
      Assert.AreEqual(A.Time, B.Time);
      Assert.AreEqual(A.TimeNull, B.TimeNull);
      Assert.AreEqual(A.Date, B.Date);
      Assert.AreEqual(A.DateNull, B.DateNull);
      Assert.AreEqual(A.TimePeriod, B.TimePeriod);
      Assert.AreEqual(A.TimePeriodNull, B.TimePeriodNull);
      Assert.AreEqual(A.Money, B.Money);
      Assert.AreEqual(A.MoneyNull, B.MoneyNull);
    }

    [TestMethod, Timeout(60000)]
    public void LongList()
    {
      var A = new ListTest()
      {
        LongList = new List<long> { 1, 2 }
      };

      var Value = Newtonsoft.Json.JsonConvert.SerializeObject(A);

      var B = Newtonsoft.Json.JsonConvert.DeserializeObject<ListTest>(Value);

      Assert.AreEqual(A.LongList[0], B.LongList[0]);
      Assert.AreEqual(A.LongList[1], B.LongList[1]);
    }

    private sealed class ListTest
    {
      public List<long> LongList;
    }

    private sealed class Test
    {
      public Inv.Date Date;
      public Inv.Time Time;
      public Inv.Date? DateNull;
      public Inv.Time? TimeNull;
      public Inv.Colour Colour;
      public Inv.Colour ColourNull;
      public Inv.TimePeriod TimePeriod;
      public Inv.TimePeriod? TimePeriodNull;
      public Inv.Money Money;
      public Inv.Money? MoneyNull;
    }
  }
}