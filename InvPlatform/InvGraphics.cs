﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Graphics"/>.
  /// </summary>
  public sealed class Graphics
  {
    internal Graphics(Application Application)
    {
      this.Application = Application;
    }

    /// <summary>
    /// Get the dimensions (width, height) of the image.
    /// </summary>
    /// <param name="Image"></param>
    /// <returns></returns>
    public Inv.Dimension GetDimension(Inv.Image Image)
    {
      //Application.RequireThreadAffinity();

      return Application.Platform.GraphicsGetDimension(Image);
    }
    /// <summary>
    /// Convert the image into a grayscale version.
    /// </summary>
    /// <param name="Image"></param>
    /// <returns></returns>
    public Inv.Image Grayscale(Inv.Image Image)
    {
      //Application.RequireThreadAffinity();

      if (Image == null)
        return null;
      
      return Application.Platform.GraphicsGrayscale(Image);
    }
    /// <summary>
    /// Tint the input image with the provided colour.
    /// </summary>
    /// <param name="Image"></param>
    /// <param name="Colour"></param>
    /// <returns></returns>
    public Inv.Image Tint(Inv.Image Image, Inv.Colour Colour)
    {
      //Application.RequireThreadAffinity();

      if (Image == null)
        return null;

      return Application.Platform.GraphicsTint(Image, Colour);
    }
    /// <summary>
    /// Resize the input image to the specified dimension.
    /// </summary>
    /// <param name="Image"></param>
    /// <param name="Dimension"></param>
    /// <returns></returns>
    public Inv.Image Resize(Inv.Image Image, Inv.Dimension Dimension)
    {
      //Application.RequireThreadAffinity();

      if (Image == null)
        return null;

      return Application.Platform.GraphicsResize(Image, Dimension);
    }
    /// <summary>
    /// Resize the input image to the specified dimension.
    /// </summary>
    /// <param name="Image"></param>
    /// <param name="Dimension"></param>
    /// <returns></returns>
    public Inv.Image Crop(Inv.Image Image, Inv.Rect Rect)
    {
      //Application.RequireThreadAffinity();

      if (Image == null)
        return null;

      if (CropImageDelegate != null)
        return CropImageDelegate(Image, Rect);

      return WritePixels(ReadPixels(Image).Crop(Rect));
    }
    /// <summary>
    /// Extract a grid of colour pixels from the source image.
    /// </summary>
    /// <param name="Image"></param>
    /// <returns></returns>
    public Inv.Pixels ReadPixels(Inv.Image Image)
    {
      //Application.RequireThreadAffinity();

      if (Image == null)
        return null;

      return Application.Platform.GraphicsReadPixels(Image);
    }
    /// <summary>
    /// Compose an image from a grid of colour pixels.
    /// </summary>
    /// <param name="Pixels"></param>
    /// <returns></returns>
    public Inv.Image WritePixels(Inv.Pixels Pixels)
    {
      //Application.RequireThreadAffinity();

      if (Pixels == null)
        return null;

      return Application.Platform.GraphicsWritePixels(Pixels);
    }
    /// <summary>
    /// Free up the platform-specific resource that represents the images.
    /// </summary>
    /// <param name="ImageList"></param>
    public void Reclaim(IReadOnlyList<Inv.Image> ImageList)
    {
      //Application.RequireThreadAffinity();

      if (ImageList.Count > 0)
        Application.Platform.GraphicsReclaim(ImageList);
    }
    /// <summary>
    /// Draw directly onto an in-memory image.
    /// </summary>
    /// <param name="DrawWidth"></param>
    /// <param name="DrawHeight"></param>
    /// <param name="DrawAction"></param>
    /// <returns></returns>
    public Inv.Image Draw(Inv.Dimension Dimension, Action<Inv.DrawContract> DrawAction)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Dimension.Width > 0, "DrawWidth must be greater than zero.");
        Inv.Assert.Check(Dimension.Height > 0, "DrawHeight must be greater than zero.");
        Inv.Assert.Check(this.DrawImageDelegate != null, "DrawAction is not supported on this platform.");
      }

      //Application.RequireThreadAffinity();

      return this.DrawImageDelegate?.Invoke(Dimension, DrawAction);
    }
    /// <summary>
    /// Calculate the width and height of the text fragment in this font.
    /// </summary>
    /// <param name="TextFragment"></param>
    /// <param name="TextFont"></param>
    /// <returns></returns>
    public Inv.Dimension CalculateText(Inv.DrawFont DrawFont, string TextFragment, int MaximumTextWidth = 0, int MaximumTextHeight = 0)
    {
      //Application.RequireThreadAffinity();

      return Application.Platform.CanvasCalculateText(TextFragment, DrawFont, MaximumTextWidth, MaximumTextHeight);
    }
    /// <summary>
    /// Render the panel at the specified dimension into an image.
    /// </summary>
    /// <param name="Panel"></param>
    /// <param name="ReturnAction"></param>
    /// <returns></returns>
    public void Render(Inv.Panel Panel, Action<Inv.Image> ReturnAction)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.CheckNotNull(Panel, nameof(Panel));
        Inv.Assert.CheckNotNull(ReturnAction, nameof(ReturnAction));
      }

      Application.RequireThreadAffinity();

      Application.Platform.GraphicsRender(Panel, ReturnAction);
    }
    internal Application Application { get; private set; }

    internal DrawImageDelegate DrawImageDelegate { get; set; }
    internal CropImageDelegate CropImageDelegate { get; set; }
    internal CanvasDrawDelegate CanvasDrawDelegate { get; set; }
  }

  internal delegate Inv.Image DrawImageDelegate(Inv.Dimension Dimension, Action<Inv.DrawContract> DrawAction);
  internal delegate Inv.Image CropImageDelegate(Inv.Image Image, Inv.Rect Rect);
  internal delegate void CanvasDrawDelegate(Inv.Canvas Canvas, Action<Inv.DrawContract> DrawAction);
}