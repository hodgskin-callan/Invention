﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public static class WebPolicy
  {
    public static bool DangerousAcceptAnyServerCertificate { get; private set; }

    public static void SetDangerousAcceptAnyServerCertificate()
    {
      if (!DangerousAcceptAnyServerCertificate)
      {
        Debug.Assert(!WebBrokerAlreadyInstantiated, "WebBroker has already been instantiated so this mode will have no effect.");

        DangerousAcceptAnyServerCertificate = true;

        // NOTE: ServicePointManager no longer seems required due to the new HttpClientHandler.
        //System.Net.ServicePointManager.Expect100Continue = true;
        //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Ssl3;
        //System.Net.ServicePointManager.ServerCertificateValidationCallback = (Sender, Cert, Chain, Error) => return true;
      }
    }

    internal static bool WebBrokerAlreadyInstantiated = false;
  }

  /// <summary>
  /// Broker for interop with web sites and web services.
  /// </summary>
  public sealed class WebBroker
  {
    public WebBroker(Uri BaseUri = null)
    {
      this.BaseUri = BaseUri;
      this.Headers = new WebHeaders();

      //System.Net.WebRequest.DefaultWebProxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
    }

    /// <summary>
    /// Base Uri for all requests.
    /// </summary>
    public Uri BaseUri { get; }
    /// <summary>
    /// Additional headers to apply to the request.
    /// </summary>
    public WebHeaders Headers { get; }
    public Newtonsoft.Json.JsonSerializerSettings JsonSerializerSettings { get; set; }
    public WebCompletionOption? RequestCompletionOption { get; set; }
    public TimeSpan? Timeout { get; set; }

    public WebRequest GET(string Path)
    {
      return NewRequest(WebMethod.GET, Path);
    }
    public WebRequest POST(string Path)
    {
      return NewRequest(WebMethod.POST, Path);
    }
    public WebRequest PUT(string Path)
    {
      return NewRequest(WebMethod.PUT, Path);
    }
    public WebRequest DELETE(string Path)
    {
      return NewRequest(WebMethod.DELETE, Path);
    }
    public WebRequest HEAD(string Path)
    {
      return NewRequest(WebMethod.HEAD, Path);
    }
    public WebRequest OPTIONS(string Path)
    {
      return NewRequest(WebMethod.OPTIONS, Path);
    }
    public WebRequest PATCH(string Path)
    {
      return NewRequest(WebMethod.PATCH, Path);
    }
    public WebRequest TRACE(string Path)
    {
      return NewRequest(WebMethod.TRACE, Path);
    }
    public WebRequest NewRequest(WebMethod WebMethod, string RelativePath)
    {
      var Uri = BaseUri != null ? new Uri(BaseUri, RelativePath) : new Uri(RelativePath);

      var Request = new WebRequest(HttpClient, WebMethod, Uri);
      Request.Timeout = Timeout;
      Request.Headers.Add(Headers);

      Request.CompletionOption = RequestCompletionOption;

      if (Headers.UserAgent.ProductName != null)
        Request.UserAgent.Add(new System.Net.Http.Headers.ProductInfoHeaderValue(Headers.UserAgent.ProductName, Headers.UserAgent.ProductVersion));

      foreach (var ProductComment in Headers.UserAgent.ProductComments)
        Request.UserAgent.Add(new System.Net.Http.Headers.ProductInfoHeaderValue(ProductComment));

      return Request;
    }

    static WebBroker()
    {
      // TODO: is it important to have a static HttpClient for effective connection pooling?

      var HttpClientHandler = new System.Net.Http.HttpClientHandler()
      {
#if !NET5_0_OR_GREATER
        CookieContainer = new System.Net.CookieContainer() // cookies not supported in Blazor.
#endif
      };

      if (WebPolicy.DangerousAcceptAnyServerCertificate)
        HttpClientHandler.ServerCertificateCustomValidationCallback = (Sender, Cert, Chain, Error) => true;

      HttpClient = new System.Net.Http.HttpClient(HttpClientHandler);

      // Remove the client's default timeout, as this may interfere with the request's timeout.
      // Each request falls back to the standard HttpClient timeout of 100s anyway.
      HttpClient.Timeout = System.Threading.Timeout.InfiniteTimeSpan;

      WebPolicy.WebBrokerAlreadyInstantiated = true;
    }

    private static readonly System.Net.Http.HttpClient HttpClient;
  }

  public sealed class WebContentType
  {
    internal WebContentType(string Name)
    {
      this.Name = Name;
    }

    public string Name { get; }

    public static WebContentType New(string Name)
    {
      return new WebContentType(Name);
    }
  }

  public static class WebContentTypes
  {
    public static WebContentType MultiPartFormData = WebContentType.New("multipart/form-data");
    public static WebContentType TextPlain = WebContentType.New("text/plain");
    public static WebContentType TextHtml = WebContentType.New("text/html");
    public static WebContentType TextCsv = WebContentType.New("text/csv");
    public static WebContentType ApplicationJson = WebContentType.New("application/json");
    public static WebContentType ApplicationXml = WebContentType.New("application/xml");
    public static WebContentType ApplicationXWwwFormUrlEncoded = WebContentType.New("application/x-www-form-urlencoded");
    public static WebContentType ApplicationOctetStream = WebContentType.New("application/octet-stream");
  }

  public enum WebMethod
  {
    DELETE,
    GET,
    HEAD,
    OPTIONS,
    PATCH,
    POST,
    PUT,
    TRACE
  }

  public static class WebMethodHelper
  {
    public static System.Net.Http.HttpMethod ToHttpMethod(this WebMethod Method)
    {
      switch (Method)
      {
        case WebMethod.DELETE:
          return System.Net.Http.HttpMethod.Delete;
        case WebMethod.GET:
          return System.Net.Http.HttpMethod.Get;
        case WebMethod.HEAD:
          return System.Net.Http.HttpMethod.Head;
        case WebMethod.OPTIONS:
          return System.Net.Http.HttpMethod.Options;
        case WebMethod.PATCH:
          return new System.Net.Http.HttpMethod("PATCH");
        case WebMethod.POST:
          return System.Net.Http.HttpMethod.Post;
        case WebMethod.PUT:
          return System.Net.Http.HttpMethod.Put;
        case WebMethod.TRACE:
          return System.Net.Http.HttpMethod.Trace;
        default:
          throw new Exception("Unhandled WebMethod: " + Method);
      }
    }
  }

  public enum WebCompletionOption
  {
    ResponseContentRead,
    ResponseHeadersRead
  }

  public sealed class WebHeaders : IEnumerable<KeyValuePair<string, string>>
  {
    internal WebHeaders()
    {
      this.Dictionary = new Dictionary<string, string>();
      this.UserAgent = new WebUserAgent();
    }

    public WebUserAgent UserAgent { get; }

    public void Clear()
    {
      Dictionary.Clear();
    }
    public void Add(string Key, string Value)
    {
      Dictionary[Key] = Value;
    }
    public void Add(WebHeaders Headers)
    {
      foreach (var Entry in Headers)
        Dictionary[Entry.Key] = Entry.Value;
    }
    public string GetValueOrDefault(string Key)
    {
      return Dictionary.GetValueOrNull(Key);
    }
    public void Remove(string Key)
    {
      Dictionary.Remove(Key);
    }

    public IEnumerator<KeyValuePair<string, string>> GetEnumerator() => Dictionary.GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator() => Dictionary.GetEnumerator();

    private readonly Dictionary<string, string> Dictionary;
  }

  public sealed class WebUserAgent
  {
    internal WebUserAgent()
    {
      this.ProductCommentList = new List<string>();
    }

    public string ProductName { get; set; }
    public string ProductVersion { get; set; }
    public IReadOnlyList<string> ProductComments => ProductCommentList;

    public void ClearProductComments() => ProductCommentList.Clear();
    public void AddProductComment(string Comment)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!string.IsNullOrWhiteSpace(Comment), "Must be a valid comment.");

      ProductCommentList.Add(Comment);
    }

    private readonly List<string> ProductCommentList;
  }

  public sealed class WebRequest : IDisposable
  {
    internal WebRequest(System.Net.Http.HttpClient Client, WebMethod Method, Uri Uri)
    {
      this.Client = Client;
      this.Method = Method;
      this.Uri = Uri;
      this.Headers = new WebHeaders();
      this.RequestMessage = new System.Net.Http.HttpRequestMessage();
    }
    public void Dispose()
    {
      RequestMessage?.Dispose();
    }

    public Uri Uri { get; }
    public TimeSpan? Timeout { get; set; }
    public WebCompletionOption? CompletionOption { get; set; }
    public string AuthorizationScheme { get; set; }
    public string AuthorizationValue { get; set; }
    public WebMethod Method { get; }
    public WebHeaders Headers { get; }
    public WebContentType ContentType { get; private set; }
    public Encoding ContentEncoding { get; set; }

    public void AsPlainTextLine(string Line)
    {
      this.ContentEncoding = Encoding.UTF8;

      AsStream(WebContentTypes.TextPlain, S =>
      {
        using (var RequestWriter = CreateOpenStreamWriter(S))
          RequestWriter.WriteLine(Line);
      });
    }
    public void AsCsvTextLine(string[] Text)
    {
      this.ContentEncoding = Encoding.UTF8;

      AsStream(WebContentTypes.TextCsv, S =>
      {
        using (var RequestWriter = CreateOpenStreamWriter(S))
        using (var CsvWriter = new Inv.CsvWriter(RequestWriter))
          CsvWriter.WriteRecord(Text);
      });
    }
    public void AsText(string Value, Encoding Encoding, WebContentType ContentType)
    {
      this.ContentType = ContentType;
      this.ContentEncoding = Encoding;
      this.RequestMessage.Content = new System.Net.Http.StringContent(Value, ContentEncoding, ContentType.Name);
    }
    public void AsJson(object Content, JsonSerializer Serializer = null)
    {
      this.ContentEncoding = Encoding.UTF8;

      var Serialiser = Serializer ?? new JsonSerializer();

      AsStream(WebContentTypes.ApplicationJson, S =>
      {
        Serialiser.SaveToStream(Content, S, true);
      });
    }
    public void AsStream(WebContentType ContentType, Stream Stream)
    {
      this.ContentType = ContentType;
      this.RequestMessage.Content = new System.Net.Http.StreamContent(Stream);
    }
    public void AsStream(WebContentType ContentType, Action<Stream> StreamAction)
    {
      this.ContentType = ContentType;
      this.RequestMessage.Content = new StreamActionContent(StreamAction);
    }
    public WebResponse Send()
    {
      // NOTE: the wrapper task is to avoid deadlocks for synchronous calls in the foreground thread.
      var Result = Task.Run(() => SendAsync()).Result;

      if (Result.Exception != null)
        throw Result.Exception.Preserve();

      return Result;
    }
    public async Task<WebResponse> SendAsync()
    {
      static System.Net.Http.HttpCompletionOption WebToHttpCompletionOption(Inv.WebCompletionOption Option)
      {
        return Option switch
        {
          WebCompletionOption.ResponseContentRead => System.Net.Http.HttpCompletionOption.ResponseContentRead,
          WebCompletionOption.ResponseHeadersRead => System.Net.Http.HttpCompletionOption.ResponseHeadersRead,
          _ => throw EnumHelper.UnexpectedValueException(Option)
        };
      }

      RequestMessage.Method = Method.ToHttpMethod();
      RequestMessage.RequestUri = Uri;

      foreach (var Header in Headers)
        RequestMessage.Headers.Add(Header.Key, Header.Value);

      if (!string.IsNullOrWhiteSpace(AuthorizationScheme) && !string.IsNullOrWhiteSpace(AuthorizationValue))
        RequestMessage.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(AuthorizationScheme, AuthorizationValue);

      if (RequestMessage.Content != null && ContentType != null)
        RequestMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(ContentType.Name);

      if (RequestMessage.Content != null && ContentEncoding != null)
        RequestMessage.Content.Headers.ContentEncoding.Add(ContentEncoding.WebName);

      Exception RequestException = null;
      System.Net.Http.HttpResponseMessage ResponseMessage = null;
      try
      {
        var SendTimeout = this.Timeout ?? TimeSpan.FromSeconds(100);
        var CancellationToken = new System.Threading.CancellationTokenSource(SendTimeout).Token;
        var HttpCompletionOption = WebToHttpCompletionOption(this.CompletionOption ?? WebCompletionOption.ResponseHeadersRead);

        try
        {
          ResponseMessage = await Client.SendAsync(RequestMessage, HttpCompletionOption, CancellationToken);
        }
        catch (Exception Ex)
        {
          var AggregateException = Ex as AggregateException;
          if (AggregateException != null && AggregateException.InnerExceptions.Count == 1)
          {
            var TaskCanceledException = AggregateException.InnerException as TaskCanceledException;
            if (TaskCanceledException != null)
              throw new TimeoutException($"Request failed to complete within timeout period ({SendTimeout.TotalMilliseconds}ms)");
          }

          throw;
        }
      }
      catch (Exception Ex)
      {
        RequestException = Ex;
      }

      var WebResponse = new Inv.WebResponse(ResponseMessage, Uri)
      {
        ContentLength = ResponseMessage?.Content?.Headers?.ContentLength,
        ContentType = ResponseMessage?.Content?.Headers?.ContentType?.MediaType,
        StatusCode = ResponseMessage?.StatusCode,
        Exception = RequestException
      };

      return WebResponse;
    }

    internal System.Net.Http.Headers.HttpHeaderValueCollection<System.Net.Http.Headers.ProductInfoHeaderValue> UserAgent => RequestMessage.Headers.UserAgent;

    private StreamWriter CreateOpenStreamWriter(Stream InputStream)
    {
      return new System.IO.StreamWriter(InputStream, ContentEncoding ?? Encoding.UTF8, 1024, true);
    }

    private readonly System.Net.Http.HttpClient Client;
    private readonly System.Net.Http.HttpRequestMessage RequestMessage;

    public class StreamActionContent : System.Net.Http.HttpContent
    {
      public StreamActionContent(Action<Stream> StreamAction)
      {
        this.StreamAction = StreamAction;
      }

      protected override Task SerializeToStreamAsync(Stream stream, System.Net.TransportContext context)
      {
        StreamAction?.Invoke(stream);

        return Task.FromResult(true);
      }
      protected override bool TryComputeLength(out long length)
      {
        length = -1;
        return false;
      }

      private readonly Action<Stream> StreamAction;
    }
  }

  public sealed class WebResponse : IDisposable
  {
    internal WebResponse(System.Net.Http.HttpResponseMessage ResponseMessage, Uri Uri)
    {
      this.ResponseMessage = ResponseMessage;
      this.Uri = Uri;

      this.Headers = new WebHeaders();

      if (ResponseMessage?.Headers != null)
      {
        foreach (var Header in ResponseMessage.Headers)
        {
          if (Header.Value.Count() == 1)
            Headers.Add(Header.Key, Header.Value.First());
        }
      }
    }
    public void Dispose()
    {
      if (ResponseMessage != null)
      {
        ResponseMessage.Dispose();
        this.ResponseMessage = null;
      }
    }

    public Uri Uri { get; }
    public WebHeaders Headers { get; }
    public System.Net.HttpStatusCode? StatusCode { get; internal set; }
    public string ContentType { get; internal set; }
    public long? ContentLength { get; internal set; }
    public Exception Exception { get; internal set; }

    public System.IO.Stream AsStream()
    {
      return Task.Run(() => AsStreamAsync()).Result;
    }
    public async Task<System.IO.Stream> AsStreamAsync()
    {
      ResponseAssert();

      return await ResponseMessage?.Content?.ReadAsStreamAsync();
    }
    public string AsPlainText()
    {
      return Task.Run(() => AsPlainTextAsync()).Result;
    }
    public async Task<string> AsPlainTextAsync()
    {
      ResponseAssert();

      if (ResponseMessage == null)
        return null;

      try
      {
        using (var ResponseStream = await AsStreamAsync())
        {
          if (ResponseStream == null)
            return null;

          using (var StreamReader = new System.IO.StreamReader(ResponseStream))
            return StreamReader.ReadToEnd().Trim();
        }
      }
      finally
      {
        Dispose();
      }
    }
    public string[] AsPlainTextLines()
    {
      return Task.Run(() => AsPlainTextLinesAsync()).Result;
    }
    public async Task<string[]> AsPlainTextLinesAsync()
    {
      ResponseAssert();

      if (ResponseMessage == null)
        return Array.Empty<string>();

      try
      {
        using (var ResponseStream = await AsStreamAsync())
        {
          if (ResponseStream == null)
            return Array.Empty<string>();

          IEnumerable<string> GetLines()
          {
            using (var ResponseReader = new System.IO.StreamReader(ResponseStream))
            {
              while (!ResponseReader.EndOfStream)
                yield return ResponseReader.ReadLine();
            }
          }

          return GetLines().ToArray();
        }
      }
      finally
      {
        Dispose();
      }
    }
    public T AsJson<T>(JsonSerializer Serializer = null)
    {
      return Task.Run(() => AsJsonAsync<T>(Serializer)).Result;
    }
    public async Task<T> AsJsonAsync<T>(JsonSerializer Serializer = null)
    {
      ResponseAssert();

      if (ResponseMessage == null)
        return default;

      try
      {
        var Serialiser = Serializer ?? new JsonSerializer();
        try
        {
          var ResultStream = await AsStreamAsync();

          if (ResultStream == null)
            return default;

          return Serialiser.LoadFromStream<T>(ResultStream);
        }
        catch (Exception Exception)
        {
          Debug.WriteLine(await AsPlainTextAsync());

          throw new AggregateException("Failed to deserialize Json", Exception);
        }
      }
      finally
      {
        Dispose();
      }
    }
    public Inv.Image AsImage()
    {
      return Task.Run(() => AsImageAsync()).Result;
    }
    public async Task<Inv.Image> AsImageAsync()
    {
      ResponseAssert();

      if (ResponseMessage == null)
        return null;

      var ServerContentLength = ContentLength ?? 0;

      using (var ResponseStream = await AsStreamAsync())
      {
        if (ResponseStream == null)
          return null;

        using (var MemoryStream = new MemoryStream(ServerContentLength > 0 ? (int)ServerContentLength : 0))
        {
          await ResponseStream.CopyToAsync(MemoryStream);

          MemoryStream.Flush();

          return new Inv.Image(MemoryStream.ToArray(), System.IO.Path.GetExtension(Uri.AbsolutePath));
        }
      }
    }

    private void ResponseAssert()
    {
      Debug.Assert(ResponseMessage != null, "Attempted to access the response without checking if it was successful.");
    }

    private System.Net.Http.HttpResponseMessage ResponseMessage;
  }
}