﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
#if WINDOWS_UWP
using System.IO;
using Windows.Networking;
using Windows.Networking.Sockets;
#else
using System.Net.Sockets;
#endif
using Inv.Support;

namespace Inv.Tcp
{
#if WINDOWS_UWP
  internal sealed class Client
  {
    public Client(string Host, int Port)
    {
      this.Host = Host;
      this.Port = Port;
    }

    public bool IsActive { get; private set; }
    public System.IO.Stream Stream => DualStream;

    public void Connect()
    {
      if (!IsActive)
      {
        this.IsActive = true;
        this.StreamSocket = new StreamSocket();

        try
        {
          StreamSocket.ConnectAsync(new HostName(Host), Port.ToString(), SocketProtectionLevel.PlainSocket).AsTask().Wait();

          // zero disables buffering.
          this.DualStream = new DualStream(StreamSocket.InputStream.AsStreamForRead(0), StreamSocket.OutputStream.AsStreamForWrite(0));
        }
        catch
        {
          StreamSocket.Dispose();
          this.StreamSocket = null;

          throw;
        }
      }
    }
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;

        if (DualStream != null)
        {
          DualStream.Dispose();
          this.DualStream = null;
        }

        if (StreamSocket != null)
        {
          StreamSocket.Dispose();
          this.StreamSocket = null;
        }
      }
    }

    private readonly string Host;
    private readonly int Port;
    private StreamSocket StreamSocket;
    private DualStream DualStream;
  }

  internal sealed class DualStream : System.IO.Stream
  {
    public DualStream(System.IO.Stream ReadStream, System.IO.Stream WriteStream)
    {
      this.ReadStream = ReadStream;
      this.WriteStream = WriteStream;
    }

    public override bool CanRead => ReadStream.CanRead;
    public override bool CanSeek => false;
    public override bool CanWrite => WriteStream.CanWrite;
    public override long Length => throw new NotImplementedException();
    public override long Position
    {
      get => throw new NotImplementedException();
      set => throw new NotImplementedException();
    }
    public override int Read(byte[] buffer, int offset, int count) => ReadStream.Read(buffer, offset, count);
    public override long Seek(long offset, System.IO.SeekOrigin origin) => throw new NotImplementedException();
    public override void SetLength(long value) => throw new NotImplementedException();
    public override void Write(byte[] buffer, int offset, int count) => WriteStream.Write(buffer, offset, count);
    public override void Flush() => throw new NotImplementedException();

    private readonly System.IO.Stream ReadStream;
    private readonly System.IO.Stream WriteStream;
  }

  internal sealed class Server
  {
    public Server(string Host, int Port)
    {
      this.Host = Host;
      this.Port = Port;
      this.ChannelList = new DistinctList<Channel>();
    }

    public event Action<Channel> AcceptEvent;
    public event Action<Channel> RejectEvent;

    public void Connect()
    {
      if (TcpServer == null)
      {
        this.TcpServer = new StreamSocketListener();
        TcpServer.ConnectionReceived += (Sender, Event) =>
        {
          var Channel = new Channel(this, Event.Socket);

          lock (ChannelList)
            ChannelList.Add(Channel);

          AcceptEvent?.Invoke(Channel);
        };
        TcpServer.BindServiceNameAsync(Port.ToString(), SocketProtectionLevel.PlainSocket).AsTask().Wait();
      }
    }
    public void Disconnect()
    {
      if (TcpServer != null)
      {
        TcpServer.Dispose();
        TcpServer = null;
      }

      foreach (var Channel in ChannelList)
      {
        try
        {
          Channel.Drop();
        }
        catch
        {
        }
      }

      ChannelList.Clear();
    }

    internal void CloseChannel(Channel Channel)
    {
      try
      {
        Channel.ClosedInvoke();

        RejectEvent?.Invoke(Channel);
      }
      finally
      {
        Channel.Drop();

        lock (ChannelList)
          ChannelList.Remove(Channel);
      }
    }

    private readonly string Host;
    private readonly int Port;
    private readonly Inv.DistinctList<Channel> ChannelList;
    private StreamSocketListener TcpServer;
  }

  internal sealed class Channel
  {
    internal Channel(Server Server, StreamSocket StreamSocket)
    {
      this.Server = Server;
      this.StreamSocket = StreamSocket;
      this.DualStream = new DualStream(StreamSocket.InputStream.AsStreamForRead(0), StreamSocket.OutputStream.AsStreamForWrite(0)); // zero disables buffering.
      this.IsActive = true;
    }

    public Server Server { get; private set; }
    public bool IsActive { get; private set; }
    public System.IO.Stream Stream => DualStream;
    public event Action ClosedEvent;

    public void Drop()
    {
      this.IsActive = false;

      if (DualStream != null)
      {
        DualStream.Dispose();
        this.DualStream = null;
      }

      if (StreamSocket != null)
      {
        this.StreamSocket.Dispose();
        this.StreamSocket = null;
      }
    }

    internal void ClosedInvoke() => ClosedEvent?.Invoke();

    private StreamSocket StreamSocket;
    private DualStream DualStream;
  }

  internal sealed class FlushWriteStream : System.IO.Stream
  {
    public FlushWriteStream(System.IO.Stream Base)
    {
      this.Base = Base;
    }

    public override bool CanRead => Base.CanRead;
    public override bool CanSeek => Base.CanSeek;
    public override bool CanWrite => Base.CanWrite;
    public override long Length => Base.Length;
    public override long Position
    {
      get => Base.Position;
      set => Base.Position = value;
    }
    public override int Read(byte[] buffer, int offset, int count) => Base.Read(buffer, offset, count);
    public override long Seek(long offset, System.IO.SeekOrigin origin) => Base.Seek(offset, origin);
    public override void SetLength(long value) => Base.SetLength(value);
    public override void Write(byte[] buffer, int offset, int count)
    {
      Base.Write(buffer, offset, count);
      Base.Flush();
    }
    public override void Flush() => Base.Flush();

    private readonly System.IO.Stream Base;
  }
#else
  internal sealed class Client
  {
    public Client(string Host, int Port)
    {
      this.Host = Host;
      this.Port = Port;
      this.TcpClient = new System.Net.Sockets.TcpClient();
      TcpClient.SendBufferSize = TransportFoundation.SendBufferSize;
      TcpClient.ReceiveBufferSize = TransportFoundation.ReceiveBufferSize;
      TcpClient.NoDelay = true;
    }

    public bool IsActive { get; private set; }
    public System.IO.Stream Stream => NetworkStream;

    public void Connect()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        try
        {
          var Result = TcpClient.ConnectAsync(Host, Port);
          if (!Result.Wait(TimeSpan.FromSeconds(5)))
          {
            TcpClient.Close();
            throw new TimeoutException($"Timed out while trying to connect to {Host}:{Port}.");
          }

          this.NetworkStream = TcpClient.GetStream();
        }
        catch (Exception Exception)
        {
          TcpClient.Close();

          throw new Exception(Exception.Message + " (" + Host + ":" + Port + ")", Exception.Preserve());
        }
      }
    }
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;

        if (NetworkStream != null)
        {
          try
          {
            NetworkStream.Dispose();
          }
          catch
          {
            // TODO: is this allowed?
          }

          this.NetworkStream = null;
        }

        try
        {
          TcpClient.Close();
        }
        catch
        {
          // TODO: is this allowed?
        }
      }
    }

    private readonly string Host;
    private readonly int Port;
    private readonly System.Net.Sockets.TcpClient TcpClient;
    private System.Net.Sockets.NetworkStream NetworkStream;
  }

  internal sealed class Server
  {
    public Server(string Host, int Port)
    {
      this.Host = Host;
      this.Port = Port;
      this.ChannelList = new Inv.DistinctList<Channel>();
    }

    public bool IsActive { get; private set; }
    public string Host { get; }
    public int Port { get; }
    public event Action StartEvent;
    public event Action StopEvent;
    public event Action<Exception> FaultEvent;
    public event Action<Channel> AcceptEvent;
    public event Action<Channel> RejectEvent;

    public void Connect()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Port != 0, "Port must be specified.");

      if (!IsActive)
      {
        this.TcpServer = new System.Net.Sockets.TcpListener(System.Net.IPAddress.Any, Port);
        TcpServer.Start();
        TcpServer.BeginAcceptTcpClient(AcceptTcpClient, null);

        this.IsActive = true;

        StartEvent?.Invoke();
      }
    }
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;

        StopEvent?.Invoke();

        if (TcpServer != null)
        {
          TcpServer.Stop();
          this.TcpServer = null;
        }

        foreach (var Channel in ChannelList)
        {
          try
          {
            Channel.Drop();
          }
          catch
          {
          }
        }

        ChannelList.Clear();
      }
    }

    internal void CloseChannel(Channel Channel)
    {
      try
      {
        Channel.ClosedInvoke();

        RejectEvent?.Invoke(Channel);
      }
      finally
      {
        Channel.Drop();

        lock (ChannelList)
          ChannelList.Remove(Channel);
      }
    }

    private void HandleFault(Exception Exception)
    {
      try
      {
        FaultEvent?.Invoke(Exception);
      }
      catch
      {
        if (Debugger.IsAttached)
          Debugger.Break();
      }
    }
    private void AcceptTcpClient(IAsyncResult Result)
    {
      try
      {
        // immediately wait for the next client.
        if (IsActive)
        {
          TcpServer.BeginAcceptTcpClient(AcceptTcpClient, null);

          var TcpClient = EndAcceptTcpClient(Result);

          if (TcpClient != null)
          {
            TcpClient.SendBufferSize = Inv.TransportFoundation.SendBufferSize;
            TcpClient.ReceiveBufferSize = Inv.TransportFoundation.ReceiveBufferSize;
            TcpClient.NoDelay = true;

            var Channel = new Channel(this, TcpClient);

            lock (ChannelList)
              ChannelList.Add(Channel);

            AcceptEvent?.Invoke(Channel);
          }
        }
      }
      catch (Exception Exception)
      {
        HandleFault(Exception);
      }
    }
    [DebuggerNonUserCode]
    private TcpClient EndAcceptTcpClient(IAsyncResult Result)
    {
      try
      {
        return TcpServer.EndAcceptTcpClient(Result);
      }
      catch (System.Net.Sockets.SocketException Exception)
      {
        // A blocking operation was interrupted by a call to WSACancelBlockingCall"
        if (Exception.SocketErrorCode == SocketError.Interrupted && Exception.ErrorCode == 10004)
          return null;
        else
          throw Exception.Preserve();
      }
    }

    private System.Net.Sockets.TcpListener TcpServer;
    private readonly Inv.DistinctList<Channel> ChannelList;
  }

  internal sealed class Channel
  {
    internal Channel(Server Server, System.Net.Sockets.TcpClient TcpClient)
    {
      this.Server = Server;
      this.TcpClient = TcpClient;
      this.NetworkStream = TcpClient.GetStream();
      this.IsActive = true;
    }

    public Server Server { get; }
    public bool IsActive { get; private set; }
    public System.Net.IPAddress RemoteIPAddress => ((System.Net.IPEndPoint)TcpClient.Client.RemoteEndPoint).Address;
    public System.IO.Stream Stream => NetworkStream;
    public event Action ClosedEvent;

    public void Drop()
    {
      this.IsActive = false;

      if (NetworkStream != null)
      {
        NetworkStream.Dispose();
        this.NetworkStream = null;
      }

      this.TcpClient.Close();
    }

    internal void ClosedInvoke()
    {
      ClosedEvent?.Invoke();
    }

    private readonly System.Net.Sockets.TcpClient TcpClient;
    private System.Net.Sockets.NetworkStream NetworkStream;
  }
#endif
}