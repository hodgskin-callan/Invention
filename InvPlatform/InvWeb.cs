﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.IO;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Web"/>.
  /// </summary>
  public sealed class Web
  {
    internal Web(Application Application)
    {
      this.Application = Application;
    }

    /// <summary>
    /// Install the app package located at the <paramref name="Uri"/>.
    /// </summary>
    /// <param name="Uri"></param>
    public void Install(Uri Uri)
    {
      Application.RequireThreadAffinity();

      Application.Platform.WebInstallUri(Uri);
    }
    /// <summary>
    /// Launch the <paramref name="Uri"/> in the default browser app.
    /// </summary>
    /// <param name="Uri"></param>
    public void Launch(Uri Uri)
    {
      Application.RequireThreadAffinity();

      Application.Platform.WebLaunchUri(Uri);
    }
    /// <summary>
    /// Create a new socket client.
    /// </summary>
    /// <param name="Host">Host name of the remote server.</param>
    /// <param name="Port">The listening port of the remote server.</param>
    /// <returns></returns>
    public WebClient NewClient(string Host, int Port)
    {
      return new WebClient(this, Host, Port);
    }
    /// <summary>
    /// Create a new socket server.
    /// </summary>
    /// <param name="Host">Host name of this server.</param>
    /// <param name="Port">Listening port of this server.</param>
    /// <returns></returns>
    public WebServer NewServer(string Host, int Port)
    {
      return new WebServer(this, Host, Port);
    }
    public WebBroadcast NewBroadcast()
    {
      return new WebBroadcast(this);
    }
    /// <summary>
    /// Create a web broker for interop with web sites and web services.
    /// </summary>
    /// <returns></returns>
    public WebBroker NewBroker(Uri BaseUri = null)
    {
      return new WebBroker(BaseUri);
    }

    internal Application Application { get; }
  }

  /// <summary>
  /// Raw-socket client for implementing custom TCP protocols.
  /// </summary>
  public sealed class WebClient
  {
    internal WebClient(Web Web, string Host, int Port)
    {
      this.Web = Web;
      this.Host = Host;
      this.Port = Port;
    }

    /// <summary>
    /// Connect to the server on <see cref="Host"/> and <see cref="Port"/>.
    /// </summary>
    public void Connect()
    {
      if (!IsActive)
      {
        Web.Application.Platform.WebClientConnect(this);
        this.IsActive = true;
      }
    }
    /// <summary>
    /// Disconnect from the server.
    /// </summary>
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;
        Web.Application.Platform.WebClientDisconnect(this);
      }
    }

    /// <summary>
    /// Host name.
    /// </summary>
    public string Host { get; }
    /// <summary>
    /// Listening port.
    /// </summary>
    public int Port { get; }
    /// <summary>
    /// Transport stream for sending and receiving data.
    /// </summary>
    public System.IO.Stream TransportStream { get; private set; }

    // NOTE: set by each platform implementation.
    internal object Node { get; set; }

    internal void SetTransportStream(System.IO.Stream TransportStream)
    {
      this.TransportStream = TransportStream;
    }

    private readonly Web Web;
    private bool IsActive;
  }

  /// <summary>
  /// Raw-socket server for implementing custom TCP protocols.
  /// </summary>
  public sealed class WebServer
  {
    internal WebServer(Web Web, string Host, int Port)
    {
      this.Web = Web;
      this.Host = Host;
      this.Port = Port;
      this.ChannelDictionary = new Dictionary<object, WebChannel>();
    }

    /// <summary>
    /// This is fired when a new client connects to the server.
    /// </summary>
    public event Action<WebChannel> AcceptEvent;
    /// <summary>
    /// This is fired when a client disconnects from the server.
    /// </summary>
    public event Action<WebChannel> RejectEvent;

    /// <summary>
    /// Start listening for clients on <see cref="Host"/> and <see cref="Port"/>.
    /// </summary>
    public void Connect()
    {
      if (!IsActive)
      {
        Web.Application.Platform.WebServerConnect(this);
        this.IsActive = true;
      }
    }
    /// <summary>
    /// Stop listening for clients.
    /// </summary>
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;
        Web.Application.Platform.WebServerDisconnect(this);
      }
    }

    /// <summary>
    /// Host name.
    /// </summary>
    public string Host { get; }
    /// <summary>
    /// Listening port.
    /// </summary>
    public int Port { get; }

    // NOTE: set by each platform implementation.
    internal object Node { get; set; }
    internal Action<object> DropDelegate { get; set; }

    internal void AcceptChannel(object Node, System.IO.Stream TransportStream)
    {
      var Result = new WebChannel(this);

      lock (ChannelDictionary)
      {
        Result.Node = Node;
        Result.SetTransportStream(TransportStream);

        ChannelDictionary.Add(Node, Result);
      }

      AcceptInvoke(Result);
    }
    internal void RejectChannel(object Node)
    {
      WebChannel Result;

      lock (ChannelDictionary)
      {
        Result = ChannelDictionary.GetValueOrNull(Node);

        if (Result != null)
        {
          ChannelDictionary.Remove(Node);

          Result.SetTransportStream(null);
        }
      }

      if (Result != null)
        RejectInvoke(Result);
    }
    internal void DropChannel(WebChannel WebChannel)
    {
      DropDelegate?.Invoke(WebChannel.Node);
    }

    private void AcceptInvoke(WebChannel WebChannel)
    {
      AcceptEvent?.Invoke(WebChannel);
    }
    private void RejectInvoke(WebChannel WebChannel)
    {
      RejectEvent?.Invoke(WebChannel);
    }

    private readonly Web Web;
    private readonly Dictionary<object, WebChannel> ChannelDictionary;
    private bool IsActive;
  }

  /// <summary>
  /// Represents a connected client on the server.
  /// </summary>
  public sealed class WebChannel
  {
    internal WebChannel(WebServer WebServer)
    {
      this.WebServer = WebServer;
    }

    /// <summary>
    /// Transport stream for sending and receiving data.
    /// </summary>
    public System.IO.Stream TransportStream { get; private set; }

    /// <summary>
    /// Disconnect this client.
    /// </summary>
    public void Drop()
    {
      WebServer.DropChannel(this);
    }

    // NOTE: set by each platform implementation.
    internal object Node { get; set; }

    internal void SetTransportStream(System.IO.Stream TransportStream)
    {
      this.TransportStream = TransportStream;
    }

    private readonly WebServer WebServer;
  }

  public interface IWebBroadcast
  {
    event Action<WebDatagram, WebEndpoint> ReceiveEvent;

    void Close();
    void Bind(WebEndpoint LocalEndpoint);
    void SendTo(WebDatagram Datagram, WebEndpoint RemoteEndpoint);
    void JoinMulticastGroup(string MulticastIpAddress);
    void LeaveMulticastGroup(string MulticastIpAddress);
    void StartReceive();
  }

  public sealed class WebBroadcast
  {
    internal WebBroadcast(Web Web)
    {
      this.Web = Web;
    }

    public event Action<WebDatagram, WebEndpoint> ReceiveEvent;

    public void Close()
    {
      Broadcast.Close();
    }
    public void Bind(WebEndpoint LocalEndpoint)
    {
      Broadcast.Bind(LocalEndpoint);
    }
    public void SendTo(WebDatagram Datagram, WebEndpoint RemoteEndpoint)
    {
      Broadcast.SendTo(Datagram, RemoteEndpoint);
    }
    public void JoinMulticastGroup(string MulticastIpAddress)
    {
      Broadcast.JoinMulticastGroup(MulticastIpAddress);
    }
    public void LeaveMulticastGroup(string MulticastIpAddress)
    {
      Broadcast.LeaveMulticastGroup(MulticastIpAddress);
    }
    public void StartReceive()
    {
      Broadcast.StartReceive();
    }

    internal object Node { get; set; }

    internal void SetBroadcast(IWebBroadcast InputBroadcast)
    {
      if (Broadcast != null)
        this.Broadcast.ReceiveEvent -= HandleBroadcastReceive;

      this.Broadcast = InputBroadcast;

      if (this.Broadcast != null)
        this.Broadcast.ReceiveEvent += HandleBroadcastReceive;
    }

    private void HandleBroadcastReceive(WebDatagram D, WebEndpoint E)
    {
      ReceiveEvent?.Invoke(D, E);
    }

    private readonly Web Web;
    private IWebBroadcast Broadcast;
  }

  public sealed class WebDatagram
  {
    public WebDatagram(byte[] Buffer)
    {
      this.Buffer = Buffer;
      this.Length = Buffer.Length;
    }

    public byte[] Buffer { get; }
    public int Length { get; }
  }

  public sealed class WebEndpoint
  {
    public WebEndpoint(string Address, int Port)
    {
      this.Address = Address;
      this.Port = Port;
    }

    public string Address { get; }
    public int Port { get; }
  }
}