﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  internal interface Platform
  {
    int ThreadAffinity();
    string CalendarTimeZoneName();
    void CalendarShowPicker(Inv.CalendarPicker CalendarPicker);
    bool EmailSendMessage(Inv.EmailMessage EmailMessage);
    bool PhoneIsSupported { get; }
    void PhoneDial(string PhoneNumber);
    void PhoneSMS(string PhoneNumber);
    long DirectoryGetLengthFile(Inv.File File);
    DateTime DirectoryGetLastWriteTimeUtcFile(Inv.File File);
    void DirectorySetLastWriteTimeUtcFile(Inv.File File, DateTime Timestamp);
    System.IO.Stream DirectoryCreateFile(Inv.File File);
    System.IO.Stream DirectoryAppendFile(Inv.File File);
    System.IO.Stream DirectoryOpenFile(Inv.File File);
    bool DirectoryExistsFile(Inv.File File);
    void DirectoryDeleteFile(Inv.File File);
    void DirectoryCopyFile(Inv.File SourceFile, Inv.File TargetFile);
    void DirectoryMoveFile(Inv.File SourceFile, Inv.File TargetFile);
    void DirectoryReplaceFile(Inv.File SourceFile, Inv.File TargetFile);
    IEnumerable<Inv.Folder> DirectoryGetFolderSubfolders(Inv.Folder Folder, string FolderMask);
    IEnumerable<Inv.File> DirectoryGetFolderFiles(Inv.Folder Folder, string FileMask);
    IEnumerable<Inv.Asset> DirectoryGetAssets(string AssetMask);
    long DirectoryGetLengthAsset(Inv.Asset Asset);
    DateTime DirectoryGetLastWriteTimeUtcAsset(Inv.Asset Asset);
    System.IO.Stream DirectoryOpenAsset(Inv.Asset Asset);
    bool DirectoryExistsAsset(Inv.Asset Asset);
    void DirectoryShowFilePicker(Inv.DirectoryFilePicker FilePicker);
    string DirectoryGetFolderPath(Inv.Folder Folder);
    string DirectoryGetFilePath(Inv.File File);
    bool LocationIsSupported { get; }
    void LocationLookup(Inv.LocationResult LocationLookup);
    void LocationShowMap(string Location);
    void AudioPlaySound(Inv.Sound Sound, float Volume, float Rate, float Pan);
    TimeSpan AudioGetSoundLength(Inv.Sound Sound);
    void AudioReclaim(IReadOnlyList<Inv.Sound> SoundList);
    void AudioPlayClip(Inv.AudioClip Clip);
    void AudioPauseClip(Inv.AudioClip Clip);
    void AudioResumeClip(Inv.AudioClip Clip);
    void AudioStopClip(Inv.AudioClip Clip);
    void AudioModulateClip(Inv.AudioClip Clip);
    void AudioStartSpeechRecognition(Inv.SpeechRecognition SpeechRecognition);
    void AudioStopSpeechRecognition(Inv.SpeechRecognition SpeechRecognition);
    bool AudioSpeechRecognitionIsSupported { get; }
    void WindowStartAnimation(Inv.Animation Animation);
    void WindowStopAnimation(Inv.Animation Animation);
    void WindowShowPopup(Inv.Popup Popup);
    void WindowHidePopup(Inv.Popup Popup);
    void WindowBrowse(Inv.File File);
    void WindowShare(Inv.File File);
    void WindowPost(Action Action);
    void WindowCall(Action Action);
    Inv.Dimension WindowGetDimension(Inv.Panel Panel);
    long ProcessMemoryUsedBytes();
    string ProcessAncillaryInformation();
    void WebClientConnect(Inv.WebClient Client);
    void WebClientDisconnect(Inv.WebClient Client);
    void WebServerConnect(Inv.WebServer Server);
    void WebServerDisconnect(Inv.WebServer Server);
    void WebBroadcastConnect(Inv.WebBroadcast Broadcast);
    void WebBroadcastDisconnect(Inv.WebBroadcast Broadcast);
    void WebInstallUri(Uri Uri);
    void WebLaunchUri(Uri Uri);
    void MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID);
    void VaultLoadSecret(Inv.Secret Secret);
    void VaultSaveSecret(Inv.Secret Secret);
    void VaultDeleteSecret(Inv.Secret Secret);
    bool ClipboardIsTextSupported { get; }
    string ClipboardGetText();
    void ClipboardSetText(string Text);
    bool ClipboardIsImageSupported { get; }
    Inv.Image ClipboardGetImage();
    void ClipboardSetImage(Inv.Image Image);
    void HapticFeedback(HapticFeedback Feedback);
    Inv.Dimension GraphicsGetDimension(Inv.Image Image);
    Inv.Image GraphicsGrayscale(Inv.Image Image);
    Inv.Image GraphicsTint(Inv.Image Image, Inv.Colour Colour);
    Inv.Image GraphicsResize(Inv.Image Image, Inv.Dimension Dimension);
    Inv.Pixels GraphicsReadPixels(Inv.Image Image);
    Inv.Image GraphicsWritePixels(Inv.Pixels Pixels);
    void GraphicsRender(Inv.Panel Panel, Action<Inv.Image> ReturnAction);
    void GraphicsReclaim(IReadOnlyList<Inv.Image> ImageList);
    Inv.Dimension CanvasCalculateText(string TextFragment, Inv.DrawFont TextFont, int MaximumTextWidth, int MaximumTextHeight);
  }
}
