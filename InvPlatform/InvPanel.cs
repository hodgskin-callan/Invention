﻿#if DEBUG
//#define TRACE_PANEL_CHANGE
//#define TRACE_PANEL_RENDER
#endif
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// The Panel is an interface contract which publishes a base control.
  /// This interface is necessary so subclasses can be used for custom controls.
  /// </summary>
  public interface Panel
  {
    /// <summary>
    /// The base control of the panel.
    /// </summary>
    Inv.Control Control { get; }
  }

  /// <summary>
  /// The abstract panel is the base class of the layout panels.
  /// </summary>
  public abstract class Control : Inv.Panel
  {
    internal Control(Inv.Surface Surface)
      : this(Surface?.Window ?? Inv.Application.Access().Window)
    {
    }
    private Control(Inv.Window Window)
    {
      this.Window = Window;
      this.Opacity = new Opacity(this);
      this.Background = new Background(this);
      this.Corner = new Corner(this);
      this.Border = new Border(this);
      this.Alignment = new Alignment(this);
      this.Visibility = new Visibility(this);
      this.Size = new Size(this);
      this.Margin = new Margin(this);
      this.Padding = new Padding(this);
      this.Elevation = new Elevation(this);
      this.Parent = null;
      this.ChildSet = null;
      //this.IsChanged = true; // NOTE: this assumes all platforms give correct defaults.
    }

    /// <summary>
    /// The controlling <see cref="Inv.Window"/> for this panel.
    /// </summary>
    public Inv.Window Window { get; }
    /// <summary>
    /// The percentage transparency of the panel and its children between 0 and 1.
    /// </summary>
    public Inv.Opacity Opacity { get; }
    /// <summary>
    /// Each panel has a background colour.
    /// Colour opacity is supported to allow the full ARGB colour range.
    /// </summary>
    public Inv.Background Background { get; }
    /// <summary>
    /// Each corner of the panel can be rounded.
    /// This is used for style reasons such as circular panels.
    /// </summary>
    public Inv.Corner Corner { get; }
    /// <summary>
    /// The outer edge of the panel can have a solid and coloured line.
    /// The border is drawn on inside of the margin and the outside of the padding.
    /// </summary>
    public Inv.Border Border { get; }
    /// <summary>
    /// The relative placement of a panel within its parent container.
    /// It is a key technique for laying out panels.
    /// </summary>
    public Inv.Alignment Alignment { get; }
    /// <summary>
    /// A panel can be collapsed which removes them from the layout.
    /// Hidden panels that reserve their space are not supported.
    /// </summary>
    public Inv.Visibility Visibility { get; }
    /// <summary>
    /// The width and height of panel can be explicitly set.
    /// Minimum and maximum constraints can also be set.
    /// </summary>
    public Inv.Size Size { get; }
    /// <summary>
    /// The outside spacing for a panel on all four sides.
    /// This is for gaps between adjacent panels.
    /// </summary>
    public Inv.Margin Margin { get; }
    /// <summary>
    /// The inside spacing for a panel on all four sides.
    /// This is used to indent the content inside a panel.
    /// </summary>
    public Inv.Padding Padding { get; }
    /// <summary>
    /// This is the relative depth between panels along the z-axis.
    /// It is used to indicate distances by the depth of the shadow.
    /// </summary>
    public Inv.Elevation Elevation { get; }
    /// <summary>
    /// Handle this event to know when the dimension of the panel have changed.
    /// </summary>
    public event Action AdjustEvent
    {
      add
      {
        RequireThreadAffinity();
        AdjustDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        AdjustDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Returns true if the AdjustEvent is handled on this panel.
    /// </summary>
    public bool HasAdjust => AdjustDelegate != null;

    /// <summary>
    /// Programmatically invoke an adjust (fires <see cref="AdjustEvent"/>).
    /// </summary>
    public void Readjust()
    {
      RequireThreadAffinity();

      AdjustInvoke();
    }
    /// <summary>
    /// Request the current dimension of the panel once it is in the visual tree, otherwise Inv.Dimension.Zero is returned.
    /// </summary>
    /// <returns></returns>
    public Inv.Dimension GetDimension()
    {
      return Window.Application.Platform.WindowGetDimension(this);
    }
    /// <summary>
    /// Returns true if this panel has child panels.
    /// </summary>
    /// <returns></returns>
    public bool HasChilds()
    {
      return ChildSet != null && ChildSet.Count > 0;
    }
    /// <summary>
    /// Enumerates any child panels of this panel.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<Inv.Panel> GetChilds()
    {
      if (ChildSet != null)
        return ChildSet;
      else
        return [];
    }

    public override string ToString()
    {
      return DisplayType;
    }

    internal Inv.Surface Surface { get; private set; }
    internal Inv.Panel Parent { get; private set; }
    internal object Node { get; set; }
    internal bool IsChanged { get; set; }
    internal abstract Inv.ControlType ControlType { get; }
    internal virtual string DisplayType => ControlType.ToString().ToLower();

    internal void Change()
    {
      RequireThreadAffinity();

      if (!IsChanged)
      {
        this.IsChanged = true;
        Window.ChangePanel(this);

#if TRACE_PANEL_CHANGE
        if (GetType() != typeof(Render))
          Debug.WriteLine("CHANGE " + DisplayType);
#endif
      }
    }
    internal bool Render()
    {
      RequireThreadAffinity();

      if (IsChanged)
      {
        this.IsChanged = false;
        Window.RenderPanel(this);

#if TRACE_PANEL_RENDER
        if (GetType() != typeof(Render))
          Debug.WriteLine("RENDER " + DisplayType);
#endif

        return true;
      }

      return false;
    }
    internal void AddChild(Inv.Panel Child)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Child, nameof(Child));

      var Control = Child.Control;

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.CheckNotNull(Control, nameof(Control));

        if (Control.Window != Window)
          Inv.Assert.Fail($"This {IdentityDisplay(this)} cannot add {IdentityDisplay(Child)} because it does not have an owning window.");

        if (Control == this)
          Inv.Assert.Fail($"This {IdentityDisplay(this)} cannot add itself.");

        if (Control.Parent == this)
          Inv.Assert.Fail($"This {IdentityDisplay(this)} cannot add the same {IdentityDisplay(Child)} multiple times.");

        if (Control.Parent != null)
          Inv.Assert.Fail($"This {IdentityDisplay(this)} cannot add {IdentityDisplay(Child)} because it already belongs to parent {IdentityDisplay(Control.Parent)}.");

        if (Control.Surface != null)
          Inv.Assert.Fail($"This {IdentityDisplay(this)} cannot add {IdentityDisplay(Child)} because it already belongs to a surface.");
      }

      if (Surface != null)
        Surface.Attach(this, Child);

      if (ChildSet == null)
        this.ChildSet = [];

      if (!ChildSet.Add(Child))
        Debug.Fail("Child was already added to the set.");

      Control.Parent = this;
    }
    internal void RemoveChild(Inv.Panel Child)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Child, nameof(Child));

      var Control = Child.Control;

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.CheckNotNull(Control, nameof(Control));

        if (Control.Window == null)
          Inv.Assert.Fail($"This {IdentityDisplay(this)} cannot remove {IdentityDisplay(Child)} because it does not have an owning window.");

        if (Control.Window != Window)
          Inv.Assert.Fail($"This {IdentityDisplay(this)} cannot remove {IdentityDisplay(Child)} because it does not belong to the correct owning window.");

        if (Control == this)
          Inv.Assert.Fail($"This {IdentityDisplay(this)} cannot remove itself.");

        if (Control.Parent == null)
          Inv.Assert.Fail($"This {IdentityDisplay(this)} cannot remove {IdentityDisplay(Child)} because it does not belong to any parent.");

        if (Control.Parent != this)
          Inv.Assert.Fail($"This {IdentityDisplay(this)} cannot remove {IdentityDisplay(Child)} because it belongs to a different parent {IdentityDisplay(Control.Parent)}.");
      }

      Control.Parent = null;

      if (ChildSet != null)
      {
        if (!ChildSet.Remove(Child))
          Debug.Fail("Child was not found in the set.");
      }

      if (Surface != null)
        Surface.Detach(this, Child);
    }
    internal void Unparent()
    {
      Debug.Assert(Parent != null);

      if (Parent != null)
      {
        var Control = Parent.Control;

        switch (Control.ControlType)
        {
          case Inv.ControlType.Button:
            var Button = (Button)Control;
            Debug.Assert(Button.Content == this, "Button content must match expected parent.");
            Button.Content = null;
            break;

          case Inv.ControlType.Board:
            var Board = (Board)Control;
            Board.RemovePin(this);
            break;

          case Inv.ControlType.Dock:
            var Dock = (Dock)Control;
            Dock.RemovePanel(this);
            break;

          case Inv.ControlType.Flow:
            var Flow = (Flow)Control;
            Flow.RemovePanel(this);
            break;

          case Inv.ControlType.Frame:
            var Frame = (Frame)Control;
            Debug.Assert(Frame.Content == this, "Frame content must match expected parent.");
            Frame.Content = null;
            break;

          case Inv.ControlType.Overlay:
            var Overlay = (Overlay)Control;
            Overlay.RemovePanel(this);
            break;

          case Inv.ControlType.Scroll:
            var Scroll = (Scroll)Control;
            Debug.Assert(Scroll.Content == this, "Scroll content must match expected parent.");
            Scroll.Content = null;
            break;

          case Inv.ControlType.Table:
            var Table = (Table)Control;
            Table.RemovePanel(this);
            break;

          case Inv.ControlType.Stack:
            var Stack = (Stack)Control;
            Stack.RemovePanel(this);
            break;

          case Inv.ControlType.Wrap:
            var Wrap = (Wrap)Control;
            Wrap.RemovePanel(this);
            break;

          default:
            throw Inv.Support.EnumHelper.UnexpectedValueException(Control.ControlType);
        }
      }

      Debug.Assert(Parent == null);
    }
    internal void AdjustInvoke()
    {
      AdjustDelegate?.Invoke();
    }
    internal void SetAdjust(Action Delegate)
    {
      this.AdjustDelegate = Delegate;
    }
    internal void SetSurface(Inv.Surface Surface)
    {
      Debug.Assert(Surface == null || Surface.Window == Window, "Surface must belong to this window.");
      this.Surface = Surface;
    }
    internal void RequireThreadAffinity()
    {
      if (Inv.Assert.IsEnabled)
        Window.RequireThreadAffinity();
    }

    private string IdentityDisplay(Inv.Panel Panel)
    {
      var Control = Panel?.Control;

      var Result = Control?.DisplayType ?? "null";

      if (Panel != null && Panel != Control)
        Result = Panel.GetType().Name + " : " + Result;

      return "[" + Result + "]";
    }

    Inv.Control Inv.Panel.Control => this;

    private HashSet<Inv.Panel> ChildSet;
    private Action AdjustDelegate;
  }

  internal enum ControlType
  {
    Block,
    Board,
    Browser,
    Button,
    Canvas,
    Dock,
    Edit,
    Flow,
    Frame,
    Graphic,
    Label,
    Memo,
    Native,
    Overlay,
    Scroll,
    Shape,
    Stack,
    Switch,
    Table,
    Wrap,
    Video
  }

  /// <summary>
  /// Horizontal or vertical orientation.
  /// </summary>
  public enum Orientation
  {
    /// <summary>
    /// Horizontal orientation means left-to-right
    /// </summary>
    Horizontal,
    /// <summary>
    /// Vertical orientations means top-to-bottom
    /// </summary>
    Vertical
  }

  /// <summary>
  /// This layout is Z-order stack where the panels are placed on top of each other.
  /// Alignment can be used to layout the panels inside the overlay.
  /// </summary>
  public sealed class Overlay : Inv.Control
  {
    /// <summary>
    /// Create a new overlay.
    /// </summary>
    public Overlay()
      : this(null)
    {
    }
    internal Overlay(Inv.Surface Surface)
      : base(Surface)
    {
      this.PanelCollection = new Inv.Collection<Inv.Panel>(this);
    }

    /// <summary>
    /// Create a new overlay.
    /// </summary>
    /// <returns></returns>
    public static Inv.Overlay New() => new Inv.Overlay();

    /// <summary>
    /// List the panels in the overlay (in z-order).
    /// </summary>
    public IReadOnlyList<Inv.Panel> Panels
    {
      get
      {
        RequireThreadAffinity();
        return PanelCollection;
      }
    }

    /// <summary>
    /// Add a panel on top of the overlaid panels.
    /// </summary>
    /// <param name="Panel"></param>
    public void AddPanel(Inv.Panel Panel)
    {
      RequireThreadAffinity();

      AddChild(Panel);
      PanelCollection.Add(Panel);
    }
    /// <summary>
    /// Insert a panel at a specified position in the overlaid panels.
    /// </summary>
    /// <param name="Index"></param>
    /// <param name="Panel"></param>
    public void InsertPanel(int Index, Inv.Panel Panel)
    {
      RequireThreadAffinity();

      AddChild(Panel);
      PanelCollection.Insert(Index, Panel);
    }
    /// <summary>
    /// Insert a panel before another panel in the overlay.
    /// </summary>
    /// <param name="Before"></param>
    /// <param name="Panel"></param>
    public void InsertPanelBefore(Inv.Panel Before, Inv.Panel Panel)
    {
      RequireThreadAffinity();

      var PanelIndex = PanelCollection.IndexOf(Before);
      if (PanelIndex < 0)
        PanelIndex = 0;

      InsertPanel(PanelIndex, Panel);
    }
    /// <summary>
    /// Insert a panel after another panel in the overlay.
    /// </summary>
    /// <param name="After"></param>
    /// <param name="Panel"></param>
    public void InsertPanelAfter(Inv.Panel After, Inv.Panel Panel)
    {
      RequireThreadAffinity();

      var PanelIndex = PanelCollection.IndexOf(After) + 1;
      if (PanelIndex <= 0)
        PanelIndex = PanelCollection.Count;

      InsertPanel(PanelIndex, Panel);
    }
    /// <summary>
    /// Remove a specific panel from the overlay.
    /// </summary>
    /// <param name="Panel"></param>
    public void RemovePanel(Inv.Panel Panel)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Panel, nameof(Panel));

      if (Panel != null)
      {
        RemoveChild(Panel);
        PanelCollection.Remove(Panel);
      }
    }
    /// <summary>
    /// Remove all panels from the overlay.
    /// </summary>
    public void RemovePanels()
    {
      RequireThreadAffinity();

      if (PanelCollection.Count > 0)
      {
        foreach (var Panel in PanelCollection)
          RemoveChild(Panel);
        PanelCollection.Clear();
      }
    }
    /// <summary>
    /// Replace all the panels with the provided panel array.
    /// </summary>
    /// <param name="PanelArray"></param>
    public void ComposePanels(params Inv.Panel[] PanelArray)
    {
      RequireThreadAffinity();

      RemovePanels();
      foreach (var Panel in PanelArray)
        AddPanel(Panel);
    }

    internal override ControlType ControlType => Inv.ControlType.Overlay;
    internal Collection<Inv.Panel> PanelCollection { get; private set; }
  }

  /// <summary>
  /// Stacks are for horizontal and vertical linear layout of panels.
  /// Stacked panels are arranged in the order they were added.
  /// </summary>
  public sealed class Stack : Inv.Control
  {
    /// <summary>
    /// Create a new stack.
    /// </summary>
    /// <param name="Orientation"></param>
    public Stack(Orientation Orientation)
      : this(null, Orientation)
    {
    }
    internal Stack(Surface Surface, Orientation Orientation)
      : base(Surface)
    {
      this.Orientation = Orientation;
      this.PanelCollection = new Inv.Collection<Panel>(this);
    }

    /// <summary>
    /// Create a new stack.
    /// </summary>
    /// <param name="Orientation"></param>
    /// <returns></returns>
    public static Inv.Stack New(Orientation Orientation) => new Inv.Stack(Orientation);
    /// <summary>
    /// Create a new vertical stack.
    /// </summary>
    /// <returns></returns>
    public static Inv.Stack NewVertical() => new Inv.Stack(Orientation.Vertical);
    /// <summary>
    /// Create a new horizontal stack.
    /// </summary>
    /// <returns></returns>
    public static Inv.Stack NewHorizontal() => new Inv.Stack(Orientation.Horizontal);

    /// <summary>
    /// Orientation of the stacked panels.
    /// </summary>
    public Orientation Orientation { get; private set; }
    /// <summary>
    /// Ask if the panels are stacked horizontally.
    /// </summary>
    public bool IsHorizontal
    {
      get => Orientation == Orientation.Horizontal;
    }
    /// <summary>
    /// Ask if the panels are stacked vertically.
    /// </summary>
    public bool IsVertical
    {
      get => Orientation == Orientation.Vertical;
    }
    /// <summary>
    /// List the panels in the stack.
    /// </summary>
    public IReadOnlyList<Inv.Panel> Panels
    {
      get
      {
        RequireThreadAffinity();
        return PanelCollection;
      }
    }

    /// <summary>
    /// Change the stacked panels to a horizontal orientation.
    /// </summary>
    public void SetHorizontal()
    {
      SetOrientation(Orientation.Horizontal);
    }
    /// <summary>
    /// Change the stacked panels to a vertical orientation.
    /// </summary>
    public void SetVertical()
    {
      SetOrientation(Orientation.Vertical);
    }
    /// <summary>
    /// Set the orientation of the stacked panels.
    /// </summary>
    /// <param name="Orientation"></param>
    public void SetOrientation(Orientation Orientation)
    {
      if (this.Orientation != Orientation)
      {
        this.Orientation = Orientation;
        Change();
      }
    }
    /// <summary>
    /// Add a panel to the end of the stack.
    /// </summary>
    /// <param name="Panel"></param>
    public void AddPanel(Panel Panel)
    {
      RequireThreadAffinity();

      AddChild(Panel);
      PanelCollection.Add(Panel);
    }
    /// <summary>
    /// Insert a panel at a specified location in the stack.
    /// </summary>
    /// <param name="Index"></param>
    /// <param name="Panel"></param>
    public void InsertPanel(int Index, Inv.Panel Panel)
    {
      RequireThreadAffinity();

      AddChild(Panel);
      PanelCollection.Insert(Index, Panel);
    }
    /// <summary>
    /// Remove all the panels from the stack.
    /// </summary>
    public void RemovePanels()
    {
      RequireThreadAffinity();

      if (PanelCollection.Count > 0)
      {
        foreach (var Panel in PanelCollection)
          RemoveChild(Panel);
        PanelCollection.Clear();
      }
    }
    /// <summary>
    /// Remove a specific panel from the stack.
    /// </summary>
    /// <param name="Panel"></param>
    public void RemovePanel(Panel Panel)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Panel, nameof(Panel));

      if (Panel != null)
      {
        RemoveChild(Panel);
        PanelCollection.Remove(Panel);
      }
    }
    /// <summary>
    /// Insert a panel before another panel in the stack.
    /// </summary>
    /// <param name="Before"></param>
    /// <param name="Panel"></param>
    public void InsertPanelBefore(Inv.Panel Before, Inv.Panel Panel)
    {
      RequireThreadAffinity();

      var PanelIndex = PanelCollection.IndexOf(Before);
      if (PanelIndex < 0)
        PanelIndex = 0;

      InsertPanel(PanelIndex, Panel);
    }
    /// <summary>
    /// Insert a panel after another panel in the stack.
    /// </summary>
    /// <param name="After"></param>
    /// <param name="Panel"></param>
    public void InsertPanelAfter(Inv.Panel After, Inv.Panel Panel)
    {
      RequireThreadAffinity();

      var PanelIndex = PanelCollection.IndexOf(After) + 1;
      if (PanelIndex <= 0)
        PanelIndex = PanelCollection.Count;

      InsertPanel(PanelIndex, Panel);
    }
    /// <summary>
    /// Replace all the panels with the provided panel array.
    /// </summary>
    /// <param name="PanelArray"></param>
    public void ComposePanels(params Panel[] PanelArray)
    {
      RequireThreadAffinity();

      RemovePanels();
      foreach (var Panel in PanelArray)
        AddPanel(Panel);
    }

    internal override ControlType ControlType => Inv.ControlType.Stack;
    internal override string DisplayType => Orientation == Orientation.Vertical ? "v-stack" : "h-stack";
    internal Inv.Collection<Panel> PanelCollection { get; private set; }
  }

  /// <summary>
  /// Docks are for horizontal and vertical linear layout of panels.
  /// Docked panels are arranged in the order they were added.
  /// </summary>
  public sealed class Dock : Inv.Control
  {
    /// <summary>
    /// Create a new dock.
    /// </summary>
    /// <param name="Orientation"></param>
    public Dock(Orientation Orientation)
      : this(null, Orientation)
    {
    }
    internal Dock(Inv.Surface Surface, Orientation Orientation)
      : base(Surface)
    {
      this.Orientation = Orientation;
      this.HeaderCollection = new Inv.Collection<Panel>(this);
      this.ClientCollection = new Inv.Collection<Panel>(this);
      this.FooterCollection = new Inv.Collection<Panel>(this);
    }

    /// <summary>
    /// Create a new dock.
    /// </summary>
    /// <param name="Orientation"></param>
    /// <returns></returns>
    public static Inv.Dock New(Orientation Orientation) => new Inv.Dock(Orientation);
    /// <summary>
    /// Create a new vertical dock.
    /// </summary>
    /// <returns></returns>
    public static Inv.Dock NewVertical() => new Inv.Dock(Orientation.Vertical);
    /// <summary>
    /// Create a new horizontal dock.
    /// </summary>
    /// <returns></returns>
    public static Inv.Dock NewHorizontal() => new Inv.Dock(Orientation.Horizontal);

    /// <summary>
    /// Orientation of the docked panels.
    /// </summary>
    public Orientation Orientation { get; private set; }
    /// <summary>
    /// Ask if the panels are docked horizontally.
    /// </summary>
    public bool IsHorizontal
    {
      get => Orientation == Orientation.Horizontal;
    }
    /// <summary>
    /// Ask if the panels are docked vertically.
    /// </summary>
    public bool IsVertical
    {
      get => Orientation == Orientation.Vertical;
    }
    /// <summary>
    /// Number of panels in the dock.
    /// </summary>
    public int PanelCount
    {
      get => HeaderCollection.Count + ClientCollection.Count + FooterCollection.Count;
    }
    /// <summary>
    /// List the header panels in the dock.
    /// </summary>
    public IReadOnlyList<Inv.Panel> Headers
    {
      get
      {
        RequireThreadAffinity();
        return HeaderCollection;
      }
    }
    /// <summary>
    /// List the client panels in the dock.
    /// </summary>
    public IReadOnlyList<Inv.Panel> Clients
    {
      get
      {
        RequireThreadAffinity();
        return ClientCollection;
      }
    }
    /// <summary>
    /// List the footer panels in the dock.
    /// </summary>
    public IReadOnlyList<Inv.Panel> Footers
    {
      get
      {
        RequireThreadAffinity();
        return FooterCollection;
      }
    }

    /// <summary>
    /// Change the docked panels to a horizontal orientation.
    /// </summary>
    public void SetHorizontal()
    {
      SetOrientation(Orientation.Horizontal);
    }
    /// <summary>
    /// Change the docked panels to a vertical orientation.
    /// </summary>
    public void SetVertical()
    {
      SetOrientation(Orientation.Vertical);
    }
    /// <summary>
    /// Set the orientation of the docked panels.
    /// </summary>
    /// <param name="Orientation"></param>
    public void SetOrientation(Orientation Orientation)
    {
      if (this.Orientation != Orientation)
      {
        this.Orientation = Orientation;
        Change();
      }
    }
    /// <summary>
    /// Remove all panels from the dock.
    /// </summary>
    public void RemovePanels()
    {
      RequireThreadAffinity();

      RemoveHeaders();
      RemoveClients();
      RemoveFooters();
    }
    /// <summary>
    /// Remove a specific panel from the dock.
    /// </summary>
    /// <param name="Panel"></param>
    public void RemovePanel(Panel Panel)
    {
      RequireThreadAffinity();

      RemoveChild(Panel);
      HeaderCollection.Remove(Panel);
      ClientCollection.Remove(Panel);
      FooterCollection.Remove(Panel);
    }
    /// <summary>
    /// Add a header panel to the dock.
    /// </summary>
    /// <param name="Panel"></param>
    public void AddHeader(Panel Panel)
    {
      RequireThreadAffinity();

      AddChild(Panel);
      HeaderCollection.Add(Panel);
    }
    /// <summary>
    /// Insert a header panel in the dock.
    /// </summary>
    /// <param name="Index"></param>
    /// <param name="Panel"></param>
    public void InsertHeader(int Index, Panel Panel)
    {
      RequireThreadAffinity();

      AddChild(Panel);
      HeaderCollection.Insert(Index, Panel);
    }
    /// <summary>
    /// Remove a header panel from the dock.
    /// </summary>
    /// <param name="Panel"></param>
    public void RemoveHeader(Panel Panel)
    {
      RequireThreadAffinity();

      RemoveChild(Panel);
      HeaderCollection.Remove(Panel);
    }
    /// <summary>
    /// Remove all headers from the dock.
    /// </summary>
    public void RemoveHeaders()
    {
      RequireThreadAffinity();

      if (HeaderCollection.Count > 0)
      {
        foreach (var Header in HeaderCollection)
          RemoveChild(Header);
        HeaderCollection.Clear();
      }
    }
    /// <summary>
    /// Add a client panel to the dock.
    /// </summary>
    /// <param name="Panel"></param>
    public void AddClient(Panel Panel)
    {
      RequireThreadAffinity();

      AddChild(Panel);
      ClientCollection.Add(Panel);
    }
    /// <summary>
    /// Insert a client panel in the dock.
    /// </summary>
    /// <param name="Index"></param>
    /// <param name="Panel"></param>
    public void InsertClient(int Index, Panel Panel)
    {
      RequireThreadAffinity();

      AddChild(Panel);
      ClientCollection.Insert(Index, Panel);
    }
    /// <summary>
    /// Remove a client panel from the dock.
    /// </summary>
    /// <param name="Panel"></param>
    public void RemoveClient(Panel Panel)
    {
      RequireThreadAffinity();

      RemoveChild(Panel);
      ClientCollection.Remove(Panel);
    }
    /// <summary>
    /// Remove the client panels from the dock.
    /// </summary>
    public void RemoveClients()
    {
      RequireThreadAffinity();

      if (ClientCollection.Count > 0)
      {
        foreach (var Client in ClientCollection)
          RemoveChild(Client);
        ClientCollection.Clear();
      }
    }
    /// <summary>
    /// Add a footer panel to the dock.
    /// </summary>
    /// <param name="Panel"></param>
    public void AddFooter(Panel Panel)
    {
      RequireThreadAffinity();

      AddChild(Panel);
      FooterCollection.Add(Panel);
    }
    /// <summary>
    /// Insert a footer panel in the dock.
    /// </summary>
    /// <param name="Index"></param>
    /// <param name="Panel"></param>
    public void InsertFooter(int Index, Panel Panel)
    {
      RequireThreadAffinity();

      AddChild(Panel);
      FooterCollection.Insert(Index, Panel);
    }
    /// <summary>
    /// Remove a footer panel from the dock.
    /// </summary>
    /// <param name="Panel"></param>
    public void RemoveFooter(Panel Panel)
    {
      RequireThreadAffinity();

      RemoveChild(Panel);
      FooterCollection.Remove(Panel);
    }
    /// <summary>
    /// Remove all footer panels from the dock.
    /// </summary>
    public void RemoveFooters()
    {
      RequireThreadAffinity();

      if (FooterCollection.Count > 0)
      {
        foreach (var Footer in FooterCollection)
          RemoveChild(Footer);
        FooterCollection.Clear();
      }
    }
    /// <summary>
    /// Replace all the panels with the provided arrays.
    /// </summary>
    /// <param name="HeaderArray"></param>
    /// <param name="ClientArray"></param>
    /// <param name="FooterArray"></param>
    public void ComposePanels(Panel[] HeaderArray, Panel[] ClientArray, Panel[] FooterArray)
    {
      RequireThreadAffinity();

      RemovePanels();
      foreach (var Header in HeaderArray)
        AddHeader(Header);

      foreach (var Client in ClientArray)
        AddClient(Client);

      foreach (var Footer in FooterArray)
        AddFooter(Footer);
    }

    internal override ControlType ControlType => Inv.ControlType.Dock;
    internal override string DisplayType => Orientation == Orientation.Vertical ? "v-dock" : "h-dock";
    internal Collection<Panel> HeaderCollection { get; private set; }
    internal Collection<Panel> ClientCollection { get; private set; }
    internal Collection<Panel> FooterCollection { get; private set; }

    internal bool CollectionRender()
    {
      RequireThreadAffinity();

      var Result = false;

      if (HeaderCollection.Render())
        Result = true;

      if (ClientCollection.Render())
        Result = true;

      if (FooterCollection.Render())
        Result = true;

      return Result;
    }
  }

  /// <summary>
  /// Wraps are for horizontal and vertical cascading layout of panels.
  /// Wrapped panels are arranged in the order they were added.
  /// </summary>
  public sealed class Wrap : Inv.Control
  {
    /// <summary>
    /// Create a new wrap.
    /// </summary>
    /// <param name="Orientation"></param>
    public Wrap(Orientation Orientation)
      : this(null, Orientation)
    {
    }
    internal Wrap(Inv.Surface Surface, Orientation Orientation)
      : base(Surface)
    {
      this.Orientation = Orientation;
      this.PanelCollection = new Inv.Collection<Panel>(this);
    }

    /// <summary>
    /// Create a new wrap.
    /// </summary>
    /// <param name="Orientation"></param>
    /// <returns></returns>
    public static Inv.Wrap New(Orientation Orientation) => new Inv.Wrap(Orientation);
    /// <summary>
    /// Create a new vertical wrap.
    /// </summary>
    /// <returns></returns>
    public static Inv.Wrap NewVertical() => new Inv.Wrap(Orientation.Vertical);
    /// <summary>
    /// Create a new horizontal wrap.
    /// </summary>
    /// <returns></returns>
    public static Inv.Wrap NewHorizontal() => new Inv.Wrap(Orientation.Horizontal);

    /// <summary>
    /// Orientation of the wrapped panels.
    /// </summary>
    public Orientation Orientation { get; private set; }
    /// <summary>
    /// Ask if the panels are wrapped horizontally.
    /// </summary>
    public bool IsHorizontal
    {
      get => Orientation == Orientation.Horizontal;
    }
    /// <summary>
    /// Ask if the panels are wrapped vertically.
    /// </summary>
    public bool IsVertical
    {
      get => Orientation == Orientation.Vertical;
    }
    /// <summary>
    /// List the panels in the wrap.
    /// </summary>
    public IReadOnlyList<Inv.Panel> Panels
    {
      get
      {
        RequireThreadAffinity();
        return PanelCollection;
      }
    }

    /// <summary>
    /// Change the wrapped panels to a horizontal orientation.
    /// </summary>
    public void SetHorizontal()
    {
      SetOrientation(Orientation.Horizontal);
    }
    /// <summary>
    /// Change the wrapped panels to a vertical orientation.
    /// </summary>
    public void SetVertical()
    {
      SetOrientation(Orientation.Vertical);
    }
    /// <summary>
    /// Set the orientation of the wrapped panels.
    /// </summary>
    /// <param name="Orientation"></param>
    public void SetOrientation(Orientation Orientation)
    {
      if (this.Orientation != Orientation)
      {
        this.Orientation = Orientation;
        Change();
      }
    }
    /// <summary>
    /// Add a panel to the end of the Wrap.
    /// </summary>
    /// <param name="Panel"></param>
    public void AddPanel(Panel Panel)
    {
      RequireThreadAffinity();

      AddChild(Panel);
      PanelCollection.Add(Panel);
    }
    /// <summary>
    /// Insert a panel at a specified location in the Wrap.
    /// </summary>
    /// <param name="Index"></param>
    /// <param name="Panel"></param>
    public void InsertPanel(int Index, Inv.Panel Panel)
    {
      RequireThreadAffinity();

      AddChild(Panel);
      PanelCollection.Insert(Index, Panel);
    }
    /// <summary>
    /// Remove all the panels from the Wrap.
    /// </summary>
    public void RemovePanels()
    {
      RequireThreadAffinity();

      if (PanelCollection.Count > 0)
      {
        foreach (var Panel in PanelCollection)
          RemoveChild(Panel);
        PanelCollection.Clear();
      }
    }
    /// <summary>
    /// Remove a specific panel from the Wrap.
    /// </summary>
    /// <param name="Panel"></param>
    public void RemovePanel(Panel Panel)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Panel, nameof(Panel));

      if (Panel != null)
      {
        RemoveChild(Panel);
        PanelCollection.Remove(Panel);
      }
    }
    /// <summary>
    /// Insert a panel before another panel in the Wrap.
    /// </summary>
    /// <param name="Before"></param>
    /// <param name="Panel"></param>
    public void InsertPanelBefore(Inv.Panel Before, Inv.Panel Panel)
    {
      RequireThreadAffinity();

      var PanelIndex = PanelCollection.IndexOf(Before);
      if (PanelIndex < 0)
        PanelIndex = 0;

      InsertPanel(PanelIndex, Panel);
    }
    /// <summary>
    /// Insert a panel after another panel in the Wrap.
    /// </summary>
    /// <param name="After"></param>
    /// <param name="Panel"></param>
    public void InsertPanelAfter(Inv.Panel After, Inv.Panel Panel)
    {
      RequireThreadAffinity();

      var PanelIndex = PanelCollection.IndexOf(After) + 1;
      if (PanelIndex <= 0)
        PanelIndex = PanelCollection.Count;

      InsertPanel(PanelIndex, Panel);
    }
    /// <summary>
    /// Replace all the panels with the provided panel array.
    /// </summary>
    /// <param name="PanelArray"></param>
    public void ComposePanels(params Panel[] PanelArray)
    {
      RequireThreadAffinity();

      RemovePanels();
      foreach (var Panel in PanelArray)
        AddPanel(Panel);
    }

    internal override ControlType ControlType => Inv.ControlType.Wrap;
    internal override string DisplayType => Orientation == Orientation.Vertical ? "v-wrap" : "h-wrap";
    internal Inv.Collection<Panel> PanelCollection { get; private set; }
  }

  /// <summary>
  /// Scrolls are vertical or horizontal scrolling regions when the panels exceed the layout space.
  /// The native scrolling control is used for each platform and gives the expected bounce and feel.
  /// </summary>
  public sealed class Scroll : Inv.Control
  {
    /// <summary>
    /// Create a new scroll.
    /// </summary>
    /// <param name="Orientation"></param>
    public Scroll(Orientation Orientation)
      : this(null, Orientation)
    {
    }
    internal Scroll(Inv.Surface Surface, Orientation Orientation)
      : base(Surface)
    {
      this.Orientation = Orientation;
      this.ContentSingleton = new Singleton<Inv.Panel>(this);
    }

    /// <summary>
    /// Create a new scroll.
    /// </summary>
    /// <param name="Orientation"></param>
    /// <returns></returns>
    public static Inv.Scroll New(Orientation Orientation) => new Inv.Scroll(Orientation);
    /// <summary>
    /// Create a new vertical scroll.
    /// </summary>
    /// <returns></returns>
    public static Inv.Scroll NewVertical() => new Inv.Scroll(Orientation.Vertical);
    /// <summary>
    /// Create a new horizontal scroll.
    /// </summary>
    /// <returns></returns>
    public static Inv.Scroll NewHorizontal() => new Inv.Scroll(Orientation.Horizontal);

    /// <summary>
    /// Orientation of the scrolled content.
    /// </summary>
    public Orientation Orientation { get; private set; }
    /// <summary>
    /// Ask if the content is scrolled horizontally.
    /// </summary>
    public bool IsHorizontal
    {
      get => Orientation == Orientation.Horizontal;
    }
    /// <summary>
    /// Ask if the content is scrolled vertically.
    /// </summary>
    public bool IsVertical
    {
      get => Orientation == Orientation.Vertical;
    }
    /// <summary>
    /// The content that can be scrolled.
    /// </summary>
    public Inv.Panel Content
    {
      get => ContentSingleton.Data;
      set
      {
        RequireThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
          {
            RemoveChild(ContentSingleton.Data);
            ContentSingleton.Data = null;
          }

          if (value != null)
          {
            AddChild(value);
            ContentSingleton.Data = value;
          }
        }
      }
    }
    /// <summary>
    /// Change the scrolled content to a horizontal orientation.
    /// </summary>
    public void SetHorizontal()
    {
      SetOrientation(Orientation.Horizontal);
    }
    /// <summary>
    /// Change the scrolled content to a vertical orientation.
    /// </summary>
    public void SetVertical()
    {
      SetOrientation(Orientation.Vertical);
    }
    /// <summary>
    /// Set the orientation of the scrolled content.
    /// </summary>
    /// <param name="Orientation"></param>
    public void SetOrientation(Orientation Orientation)
    {
      if (this.Orientation != Orientation)
      {
        this.Orientation = Orientation;
        Change();
      }
    }
    /// <summary>
    /// Request to view the start of the scrolled content.
    /// </summary>
    public void GoToStart()
    {
      GoTo(ScrollRequest.Start);
    }
    /// <summary>
    /// Request to view the end of the scrolled content.
    /// </summary>
    public void GoToEnd()
    {
      GoTo(ScrollRequest.End);
    }

    internal override ControlType ControlType => Inv.ControlType.Scroll;
    internal override string DisplayType => Orientation == Orientation.Vertical ? "v-scroll" : "h-scroll";
    internal Singleton<Inv.Panel> ContentSingleton { get; private set; }

    internal void GoTo(ScrollRequest ScrollRequest)
    {
      if (Request != ScrollRequest)
      {
        this.Request = ScrollRequest;
        Change();
      }
    }
    internal ScrollRequest? HandleRequest()
    {
      var Result = this.Request;
      this.Request = null;
      return Result;
    }

    private ScrollRequest? Request;
  }

  internal enum ScrollRequest
  {
    Start,
    End
  }

  /// <summary>
  /// The board is for pinning panels in any location and permits overlapping panels.
  /// Z-order is determined by the order the panels are added to the board.
  /// </summary>
  public sealed class Board : Inv.Control
  {
    /// <summary>
    /// Create a new board.
    /// </summary>
    public Board()
      : this(null)
    {
    }
    internal Board(Inv.Surface Surface)
      : base(Surface)
    {
      this.PinCollection = new Collection<BoardPin>(this);
    }

    /// <summary>
    /// Create a new board.
    /// </summary>
    /// <returns></returns>
    public static Inv.Board New() => new Inv.Board();

    /// <summary>
    /// List the panels that are pinned to this board.
    /// </summary>
    /// <returns></returns>
    public IReadOnlyList<BoardPin> Pins
    {
      get
      {
        RequireThreadAffinity();
        return PinCollection;
      }
    }

    /// <summary>
    /// Pin the panel at a rectangular position on the board.
    /// </summary>
    /// <param name="Panel"></param>
    /// <param name="Rect"></param>
    public void AddPin(Panel Panel, Rect Rect)
    {
      RequireThreadAffinity();

      AddChild(Panel);
      PinCollection.Add(new BoardPin(Rect, Panel));
    }
    /// <summary>
    /// Move a pinned panel to a new rectangular position on the board.
    /// </summary>
    /// <param name="Panel"></param>
    /// <param name="Rect"></param>
    public void MovePin(Panel Panel, Rect Rect)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Panel, nameof(Panel));

      if (Panel != null)
      {
        RemoveChild(Panel);
        PinCollection.RemoveWhere(E => E.Panel == Panel);

        PinCollection.Add(new BoardPin(Rect, Panel));
      }
    }
    /// <summary>
    /// Remove a pinned panel from the board.
    /// </summary>
    /// <param name="Panel"></param>
    public void RemovePin(Panel Panel)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Panel, nameof(Panel));

      if (Panel != null)
      {
        RemoveChild(Panel);
        PinCollection.RemoveWhere(E => E.Panel == Panel);
      }
    }
    /// <summary>
    /// Remove all pins from the board.
    /// </summary>
    public void RemovePins()
    {
      RequireThreadAffinity();

      if (PinCollection.Count > 0)
      {
        foreach (var Pin in PinCollection)
          RemoveChild(Pin.Panel);
        PinCollection.Clear();
      }
    }

    internal override ControlType ControlType => Inv.ControlType.Board;
    internal Collection<BoardPin> PinCollection { get; private set; }
  }

  /// <summary>
  /// Panels pinned to a board.
  /// </summary>
  public struct BoardPin
  {
    public BoardPin(Rect Rect, Panel Panel)
    {
      this.Rect = Rect;
      this.Panel = Panel;
    }

    /// <summary>
    /// Where the panel is pinned.
    /// </summary>
    public readonly Rect Rect;
    /// <summary>
    /// The panel which is pinned.
    /// </summary>
    public readonly Panel Panel;
  }

  /// <summary>
  /// The browser embeds the native web browser for each platform.
  /// This is used for integrating with webapps and loading html text.
  /// </summary>
  public sealed class Browser : Inv.Control
  {
    /// <summary>
    /// Create new browser.
    /// </summary>
    public Browser()
      : this(null)
    {
    }
    internal Browser(Inv.Surface Surface)
      : base(Surface)
    {
      this.UriSingleton = new Singleton<Uri>(this);
      this.HtmlSingleton = new Singleton<string>(this);
    }

    /// <summary>
    /// Create new browser.
    /// </summary>
    /// <returns></returns>
    public static Inv.Browser New() => new Inv.Browser();

    /// <summary>
    /// The Uri of the currently loaded web page.
    /// </summary>
    public Uri Uri => UriSingleton.Data;
    /// <summary>
    /// The Html of the currently loaded web document.
    /// </summary>
    public string Html => HtmlSingleton.Data;
    /// <summary>
    /// Fired before any Uri is fetched from the web server. Can be used to cancel the fetch.
    /// </summary>
    public event Action<BrowserFetch> FetchEvent;
    /// <summary>
    /// Fired after each Uri is completely loaded and ready for the end user.
    /// </summary>
    public event Action<BrowserReady> ReadyEvent;

    /// <summary>
    /// Load the web page at the provided <paramref name="Uri"/>.
    /// </summary>
    /// <param name="Uri"></param>
    public void LoadUri(Uri Uri)
    {
      RequireThreadAffinity();

      HtmlSingleton.Data = null;

      UriSingleton.Data = Uri;
      UriSingleton.Change(); // always change on load methods.
    }
    /// <summary>
    /// Load a web page from the provided <paramref name="Binary"/> document.
    /// </summary>
    /// <param name="Binary"></param>
    public void LoadBinary(Inv.Binary Binary)
    {
      using (var MemoryStream = new System.IO.MemoryStream(Binary.GetBuffer()))
      using (var StreamReader = new System.IO.StreamReader(MemoryStream))
        LoadHtml(StreamReader.ReadToEnd());
    }
    /// <summary>
    /// Load a web page from the provided <paramref name="Html"/> document.
    /// </summary>
    /// <param name="Html"></param>
    public void LoadHtml(string Html)
    {
      RequireThreadAffinity();

      UriSingleton.Data = null;

      HtmlSingleton.Data = Html;
      HtmlSingleton.Change(); // always change on load methods.
    }

    internal override ControlType ControlType => Inv.ControlType.Browser;
    internal Singleton<Uri> UriSingleton { get; private set; }
    internal Singleton<string> HtmlSingleton { get; private set; }

    internal void FetchInvoke(BrowserFetch Fetch)
    {
      FetchEvent?.Invoke(Fetch);
    }
    internal void ReadyInvoke(BrowserReady Ready)
    {
      ReadyEvent?.Invoke(Ready);
    }
  }

  /// <summary>
  /// The browser is proposing to fetch the given uri.
  /// You can optionally cancel the fetch.
  /// </summary>
  public sealed class BrowserFetch
  {
    internal BrowserFetch(Uri Uri)
    {
      this.Uri = Uri;
    }

    /// <summary>
    /// Uri that will be fetched.
    /// </summary>
    public Uri Uri { get; }

    /// <summary>
    /// Prevent the fetch.
    /// </summary>
    public void Cancel()
    {
      this.IsCancelled = true;
    }

    internal bool IsCancelled { get; private set; }
  }

  /// <summary>
  /// The browser has loaded the given uri.
  /// </summary>
  public sealed class BrowserReady
  {
    internal BrowserReady(Uri Uri)
    {
      this.Uri = Uri;
    }

    /// <summary>
    /// Uri that has been fetched.
    /// </summary>
    public Uri Uri { get; }
  }
  /// <summary>
  /// Pinch-or-zoom gesture.
  /// </summary>
  public struct Zoom
  {
    internal Zoom(Inv.Point Point, int Delta)
      : this()
    {
      this.Point = Point;
      this.Delta = Delta;
    }

    /// <summary>
    /// Center point of the zoom gesture.
    /// </summary>
    public Inv.Point Point { get; private set; }
    /// <summary>
    /// -1 for reduce and +1 for expand
    /// </summary>
    public int Delta { get; private set; }
  }

  /// <summary>
  /// Canvas is for custom drawing using primitives (lines, rectangles, ellipses, text and images).
  /// </summary>
  public sealed class Canvas : Inv.Control
  {
    /// <summary>
    /// Create a new canvas.
    /// </summary>
    public Canvas()
      : this(null)
    {
    }
    internal Canvas(Inv.Surface Surface)
      : base(Surface)
    {
      //this.Redrawing = false; // NOTE: C# defaults to false.
    }

    /// <summary>
    /// Create a new canvas.
    /// </summary>
    /// <returns></returns>
    public static Inv.Canvas New() => new Inv.Canvas();

    /// <summary>
    /// Handle this event to specify the measured dimensions of the canvas.
    /// </summary>
    public event Action<Inv.CanvasMeasure> MeasureEvent
    {
      add
      {
        RequireThreadAffinity();
        MeasureDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        MeasureDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Ask if the <see cref="MeasureEvent"/> is handled.
    /// </summary>
    public bool HasMeasure => MeasureDelegate != null;
    /// <summary>
    /// Handle this event to draw the entire canvas from scratch.
    /// </summary>
    public event Action<Inv.DrawContract> DrawEvent;
    /// <summary>
    /// Fired when the user completes a single tap on the canvas.
    /// </summary>
    public event Action<Inv.Point> SingleTapEvent;
    /// <summary>
    /// Fired when the user completes a double tap on the canvas.
    /// </summary>
    public event Action<Inv.Point> DoubleTapEvent;
    /// <summary>
    /// Fired when the user completes a context tap (right mouse click or long press gesture) on the canvas.
    /// </summary>
    public event Action<Inv.Point> ContextTapEvent;
    /// <summary>
    /// Fired when the user starts a touch on the canvas.
    /// </summary>
    public event Action<CanvasCommand> PressEvent;
    /// <summary>
    /// Fired when the user releases their touch on the canvas.
    /// </summary>
    public event Action<CanvasCommand> ReleaseEvent;
    /// <summary>
    /// Fired when the user moves their input while maintaining a touch on the canvas.
    /// </summary>
    public event Action<CanvasCommand> MoveEvent;
    /// <summary>
    /// Fired when the user gestures to pinch-or-zoom (includes touch gestures and mouse wheel).
    /// </summary>
    public event Action<Inv.Zoom> ZoomEvent;
    /// <summary>
    /// Handle this event to publish custom regions as accessibility elements (for VoiceOver, etc).
    /// </summary>
    public event Action<Inv.CanvasQuery> QueryEvent;

    /// <summary>
    /// Request the canvas to draw (which fires the <see cref="DrawEvent"/> in the rendering loop)
    /// </summary>
    public void InvalidateDraw()
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(DrawEvent != null, "Invalidate drawing requires the DrawEvent to be handled.");

      this.IsInvalidated = true;
      Change();
    }
    /// <summary>
    /// Immediately draw onto the canvas (without invoking the DrawEvent).
    /// </summary>
    public void ImmediateDraw(Action<Inv.DrawContract> DrawAction)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.CheckNotNull(DrawAction, nameof(DrawAction));
        Inv.Assert.Check(Window.Application.Graphics.CanvasDrawDelegate != null, "Immediate drawing requires the CanvasDrawDelegate to be handled by the platform.");
      }

      Window.Application.Graphics.CanvasDrawDelegate?.Invoke(this, DrawAction);
    }
    /// <summary>
    /// Programmatically invoke a single tap.
    /// </summary>
    /// <param name="Point"></param>
    public void SingleTap(Inv.Point Point)
    {
      SingleTapInvoke(Point);
    }
    /// <summary>
    /// Programmatically invoke a double tap.
    /// </summary>
    /// <param name="Point"></param>
    public void DoubleTap(Inv.Point Point)
    {
      DoubleTapInvoke(Point);
    }
    /// <summary>
    /// Programmatically invoke a context tap.
    /// </summary>
    /// <param name="Point"></param>
    public void ContextTap(Inv.Point Point)
    {
      ContextTapInvoke(Point);
    }
    /// <summary>
    /// Programmatically invoke a press.
    /// </summary>
    /// <param name="Point"></param>
    public void Press(CanvasCommand Action)
    {
      PressInvoke(Action);
    }
    /// <summary>
    /// Programmatically invoke a release.
    /// </summary>
    /// <param name="Point"></param>
    public void Release(CanvasCommand Action)
    {
      ReleaseInvoke(Action);
    }
    /// <summary>
    /// Programmatically invoke a move.
    /// </summary>
    /// <param name="Point"></param>
    public void Move(CanvasCommand Action)
    {
      MoveInvoke(Action);
    }
    /// <summary>
    /// Programmatically invoke a zoom.
    /// </summary>
    /// <param name="Zoom"></param>
    public void Zoom(Inv.Zoom Zoom)
    {
      ZoomInvoke(Zoom);
    }
    /// <summary>
    /// Capture move events until it is released.
    /// </summary>
    public void Capture()
    {
      this.IsCaptured = true;
    }
    /// <summary>
    /// No longer capture move events.
    /// </summary>
    public void Release()
    {
      this.IsCaptured = false;
    }

    internal override ControlType ControlType => Inv.ControlType.Canvas;
    internal bool IsInvalidated { get; private set; }
    internal bool IsCaptured { get; private set; }

    internal void PressInvoke(CanvasCommand Command)
    {
      RequireThreadAffinity();

      PressEvent?.Invoke(Command);
    }
    internal void ReleaseInvoke(CanvasCommand Command)
    {
      RequireThreadAffinity();

      ReleaseEvent?.Invoke(Command);
    }
    internal void MoveInvoke(CanvasCommand Command)
    {
      RequireThreadAffinity();

      MoveEvent?.Invoke(Command);
    }
    internal void SingleTapInvoke(Point Point)
    {
      RequireThreadAffinity();

      SingleTapEvent?.Invoke(Point);
    }
    internal void DoubleTapInvoke(Point Point)
    {
      RequireThreadAffinity();

      DoubleTapEvent?.Invoke(Point);
    }
    internal void ContextTapInvoke(Point Point)
    {
      RequireThreadAffinity();

      ContextTapEvent?.Invoke(Point);
    }
    internal void ZoomInvoke(Zoom Zoom)
    {
      RequireThreadAffinity();

      ZoomEvent?.Invoke(Zoom);
    }
    internal void DrawInvoke(DrawContract DrawContract)
    {
      this.IsInvalidated = false;

      DrawEvent?.Invoke(DrawContract);
    }
    internal void QueryInvoke(CanvasQuery Query)
    {
      QueryEvent?.Invoke(Query);
    }
    internal void MeasureInvoke(CanvasMeasure Measure)
    {
      MeasureDelegate?.Invoke(Measure);
    }
    internal void SetMeasure(Action<CanvasMeasure> Delegate)
    {
      this.MeasureDelegate = Delegate;
    }

    private Action<CanvasMeasure> MeasureDelegate;
  }

  public sealed class CanvasCommand
  {
    internal CanvasCommand(Inv.Point Point, bool LeftMouseButton, bool RightMouseButton)
    {
      this.Point = Point;
      this.LeftMouseButton = LeftMouseButton;
      this.RightMouseButton = RightMouseButton;
    }

    public Inv.Point Point { get; }
    public bool LeftMouseButton { get; }
    public bool RightMouseButton { get; }
  }

  /// <summary>
  /// Used set the measured dimension in the Canvas MeasureEvent.
  /// Refer to the constraint which is the maximum dimension available.
  /// </summary>
  public sealed class CanvasMeasure
  {
    internal CanvasMeasure(Inv.Dimension Constraint)
    {
      this.Constraint = Constraint;
    }

    /// <summary>
    /// Constraint is the maximum dimension available.
    /// </summary>
    public Inv.Dimension Constraint { get; }

    /// <summary>
    /// Set the measured dimension of the canvas.
    /// </summary>
    /// <param name="Width"></param>
    /// <param name="Height"></param>
    public void Set(int Width, int Height)
    {
      this.Dimension = new Inv.Dimension(Width, Height);
    }
    /// <summary>
    /// Set the measured dimension of the canvas.
    /// </summary>
    /// <param name="Dimension"></param>
    public void Set(Inv.Dimension Dimension)
    {
      this.Dimension = Dimension;
    }

    internal Inv.Dimension? Dimension { get; set; }
  }

  /// <summary>
  /// Contains rectangular regions with hints that are published as accessibility elements.
  /// </summary>
  public sealed class CanvasQuery
  {
    internal CanvasQuery()
    {
      this.RegionList = [];
    }

    /// <summary>
    /// List the regions.
    /// </summary>
    /// <returns></returns>
    public IReadOnlyList<CanvasRegion> Regions
    {
      get
      {
        // TODO: RequireThreadAffinity();
        return RegionList;
      }
    }

    /// <summary>
    /// Add a hint to a rectangular region.
    /// </summary>
    /// <param name="Rect"></param>
    /// <param name="Hint"></param>
    public void AddRegion(Inv.Rect Rect, string Hint)
    {
      RegionList.Add(new CanvasRegion(Rect, Hint));
    }

    private readonly Inv.DistinctList<CanvasRegion> RegionList;
  }

  /// <summary>
  /// The region is rectangular and has a text hint.
  /// </summary>
  public sealed class CanvasRegion
  {
    internal CanvasRegion(Inv.Rect Rect, string Hint)
    {
      this.Rect = Rect;
      this.Hint = Hint;
    }

    /// <summary>
    /// Rectangular location within the canvas.
    /// </summary>
    public Inv.Rect Rect { get; }
    /// <summary>
    /// The text hint to be published for accessibility.
    /// </summary>
    public string Hint { get; }
  }

  /// <summary>
  /// Vertical positioning for drawing text on the canvas.
  /// </summary>
  public enum VerticalPosition
  {
    /// <summary>
    /// The text is draw using the point as the top edge.
    /// </summary>
    Top,
    /// <summary>
    /// The text is drawn vertically centered around the point.
    /// </summary>
    Center,
    /// <summary>
    /// The text is draw using the point as the bottom edge.
    /// </summary>
    Bottom
  }

  /// <summary>
  /// Horizontal positioning for drawing text on the canvas.
  /// </summary>
  public enum HorizontalPosition
  {
    /// <summary>
    /// The text is drawn using the point as the left edge.
    /// </summary>
    Left,
    /// <summary>
    /// The text is drawn horizontally centered around the point.
    /// </summary>
    Center,
    /// <summary>
    /// The text is drawn using the point as the right edge.
    /// </summary>
    Right
  }

  /// <summary>
  /// Vertical or horizontal flipping of drawn images on the canvas.
  /// </summary>
  public enum Mirror
  {
    /// <summary>
    /// Flip the image on the Y-Axis
    /// </summary>
    Vertical,
    /// <summary>
    /// Flip the image on the X-Axis.
    /// </summary>
    Horizontal,
    /// <summary>
    /// Flip the image on the X-Axis and Y-Axis.
    /// </summary>
    HorizontalAndVertical
  }

  /// <summary>
  /// Pivot point for rotations.
  /// </summary>
  public enum Pivot
  {
    Center,
    CenterLeft,
    CenterRight,
    TopCenter,
    TopLeft,
    TopRight,
    BottomCenter,
    BottomLeft,
    BottomRight,
    Custom
  }

  /// <summary>
  /// Rotation method for drawing images on the canvas.
  /// </summary>
  public readonly struct Rotation
  {
    /// <summary>
    /// Rotation of zero degrees (no rotation specified).
    /// </summary>
    public static readonly Inv.Rotation Zero = Center(0);

    /// <summary>
    /// Custom rotation around a relative pivot point.
    /// </summary>
    /// <param name="Angle">Angle in degrees.</param>
    /// <param name="PivotPoint">This is a relative point to the drawn image rect, this is not absolute point to the canvas.</param>
    /// <returns></returns>
    public static Rotation Custom(float Angle, Inv.Point PivotPoint) => new Rotation(Angle, Pivot.Custom, PivotPoint);
    /// <summary>
    /// Rotate around the center of the drawn image.
    /// </summary>
    /// <param name="Angle">Angle in degrees.</param>
    /// <returns></returns>
    public static Rotation Center(float Angle) => new Rotation(Angle, Pivot.Center, CustomPoint: null);
    /// <summary>
    /// Rotate around the center-left of the drawn image.
    /// </summary>
    /// <param name="Angle">Angle in degrees.</param>
    /// <returns></returns>
    public static Rotation CenterLeft(float Angle) => new Rotation(Angle, Pivot.CenterLeft, CustomPoint: null);
    /// <summary>
    /// Rotate around the center-right of the drawn image.
    /// </summary>
    /// <param name="Angle">Angle in degrees.</param>
    /// <returns></returns>
    public static Rotation CenterRight(float Angle) => new Rotation(Angle, Pivot.CenterRight, CustomPoint: null);
    /// <summary>
    /// Rotate around the top-center of the drawn image.
    /// </summary>
    /// <param name="Angle">Angle in degrees.</param>
    /// <returns></returns>
    public static Rotation TopCenter(float Angle) => new Rotation(Angle, Pivot.TopCenter, CustomPoint: null);
    /// <summary>
    /// Rotate around the top left corner of the drawn image.
    /// </summary>
    /// <param name="Angle">Angle in degrees.</param>
    /// <returns></returns>
    public static Rotation TopLeft(float Angle) => new Rotation(Angle, Pivot.TopLeft, CustomPoint: null);
    /// <summary>
    /// Rotate around the top right corner of the drawn image.
    /// </summary>
    /// <param name="Angle">Angle in degrees.</param>
    /// <returns></returns>
    public static Rotation TopRight(float Angle) => new Rotation(Angle, Pivot.TopRight, CustomPoint: null);
    /// <summary>
    /// Rotate around the bottom-center of the drawn image.
    /// </summary>
    /// <param name="Angle">Angle in degrees.</param>
    /// <returns></returns>
    public static Rotation BottomCenter(float Angle) => new Rotation(Angle, Pivot.BottomCenter, CustomPoint: null);
    /// <summary>
    /// Rotate around the bottom left corner of the drawn image.
    /// </summary>
    /// <param name="Angle">Angle in degrees.</param>
    /// <returns></returns>
    public static Rotation BottomLeft(float Angle) => new Rotation(Angle, Pivot.BottomLeft, CustomPoint: null);
    /// <summary>
    /// Rotate around the bottom right corner of the drawn image.
    /// </summary>
    /// <param name="Angle">Angle in degrees.</param>
    /// <returns></returns>
    public static Rotation BottomRight(float Angle) => new Rotation(Angle, Pivot.BottomRight, CustomPoint: null);

    internal Rotation(float Angle, Pivot Pivot, Inv.Point? CustomPoint)
    {
      this.Angle = Angle;
      this.Pivot = Pivot;
      this.CustomPoint = CustomPoint;
    }

    /// <summary>
    /// Rotation angle in degrees (eg. 90.0F is 90 degrees).
    /// </summary>
    public float Angle { get; }
    /// <summary>
    /// Way to find rotation pivot point.
    /// </summary>
    public Pivot Pivot { get; }
    /// <summary>
    /// Only specified for custom pivots, is the relative pivot point to the drawn image rect.
    /// </summary>
    public Inv.Point? CustomPoint { get; }

    /// <summary>
    /// Locate the absolute pivot point to the actual drawn image rect.
    /// </summary>
    /// <param name="Rect"></param>
    /// <returns></returns>
    public Inv.Point LocatePoint(Inv.Rect Rect)
    {
      return Pivot switch
      {
        Pivot.Custom => Rect.TopLeft() + CustomPoint.Value,
        Pivot.TopLeft => Rect.TopLeft(),
        Pivot.TopCenter => Rect.TopCenter(),
        Pivot.TopRight => Rect.TopRight(),
        Pivot.CenterLeft => Rect.CenterLeft(),
        Pivot.Center => Rect.Center(),
        Pivot.CenterRight => Rect.CenterRight(),
        Pivot.BottomLeft => Rect.BottomLeft(),
        Pivot.BottomCenter => Rect.BottomCenter(),
        Pivot.BottomRight => Rect.BottomRight(),
        _ => Rect.Center() // if the enum is out of valid bounds.
      };
    }

    public override string ToString() => $"{Pivot} {Angle}°{(CustomPoint == null ? "" : " (" + CustomPoint.Value + ")")}";
  }

  /// <summary>
  /// Line joining when drawing polygons on the canvas.
  /// </summary>
  public enum LineJoin
  {
    /// <summary>
    /// Regular angular vertices.
    /// </summary>
    Miter = 0,
    /// <summary>
    /// Beveled vertices.
    /// </summary>
    Bevel = 1,
    /// <summary>
    /// Rounded vertices.
    /// </summary>
    Round = 2
  }

  /// <summary>
  /// Line capping when drawing polygons on the canvas.
  /// </summary>
  public enum LineCap
  {
    /// <summary>
    ///  A cap that does not extend past the last point of the line.
    /// </summary>
    Butt = 0,
    /// <summary>
    /// A semicircle that has a diameter equal to the line thickness.
    /// </summary>
    Round = 1,
    /// <summary>
    /// A rectangle that has a height equal to the line thickness and a length equal to half the line thickness.
    /// </summary>
    Square = 2/*,
    /// <summary>
    /// An isosceles right triangle whose base length is equal to the thickness of the line.
    /// </summary>
    Triangle*/ // NOTE: only supported by WPF/UWP?
  }

  public sealed class DrawFont
  {
    private DrawFont(string Name, int Size, Inv.FontWeight Weight, Inv.Colour Colour)
    {
      this.Name = Name;
      this.Size = Size;
      this.Weight = Weight;
      this.Colour = Colour;
    }

    public static DrawFont New(string Name = null, int Size = 12, Inv.FontWeight Weight = FontWeight.Regular, Inv.Colour Colour = null)
    {
      return new DrawFont(Name, Size, Weight, Colour ?? Inv.Colour.Black);
    }

    public string Name { get; }
    public int Size { get; }
    public Inv.FontWeight Weight { get; }
    public Inv.Colour Colour { get; }
    // TODO: Italics, Underline, SmallCaps, Strikethrough.

    public override string ToString() => $"{Name}|{Size}|{Weight}|{Colour}";

    public DrawFont Replace(string Name = null, int? Size = null, Inv.FontWeight? Weight = null, Inv.Colour Colour = null)
    {
      return new DrawFont(Name ?? this.Name, Size ?? this.Size, Weight ?? this.Weight, Colour ?? this.Colour);
    }
  }

  /// <summary>
  /// The canvas contract for custom drawing that is supported on all platforms.
  /// </summary>
  public interface DrawContract
  {
    /// <summary>
    /// Apply an opacity mask to the subsequent calls to draw, until Unmask is invoked.
    /// </summary>
    /// <param name="ViewportRect"></param>
    /// <param name="MaskAction"></param>
    void Mask(Inv.Rect ViewportRect, Action<Inv.DrawMask> MaskAction);
    /// <summary>
    /// Revert the opacity mask from the previous call to Mask.
    /// </summary>
    void Unmask();
    /// <summary>
    /// Draw text on the canvas.
    /// </summary>
    /// <param name="TextFragment">The text to draw</param>
    /// <param name="TextFont">Font definition</param>
    /// <param name="TextPoint">Position of the text relative to the positioning</param>
    /// <param name="TextHorizontal">Horizontal positioning of the text</param>
    /// <param name="TextVertical">Vertical positioning of the text</param>
    void DrawText(string TextFragment, Inv.DrawFont TextFont, Inv.Point TextPoint, Inv.HorizontalPosition TextHorizontal, Inv.VerticalPosition TextVertical, int MaximumTextWidth = 0, int MaximumTextHeight = 0);
    /// <summary>
    /// Draw one or more connected line segments.
    /// </summary>
    /// <param name="LineStrokeColour">The colour of the drawn lines</param>
    /// <param name="LineStrokeThickness">Thickness of the drawn lines</param>
    /// <param name="LineJoin"></param>
    /// <param name="LineCap"></param>
    /// <param name="LineSourcePoint">Starting point of the first line</param>
    /// <param name="LineTargetPoint">Ending point of the first line</param>
    /// <param name="LineExtraPointArray">The extra point joins from the end of the first line and so on</param>
    void DrawLine(Inv.Colour LineStrokeColour, int LineStrokeThickness, Inv.LineJoin LineJoin, Inv.LineCap LineCap, Inv.Point LineSourcePoint, Inv.Point LineTargetPoint, params Inv.Point[] LineExtraPointArray);
    /// <summary>
    /// Draw a rectangle on the canvas.
    /// </summary>
    /// <param name="RectangleFillColour">Fill colour</param>
    /// <param name="RectangleStrokeColour">Stroke colour</param>
    /// <param name="RectangleStrokeThickness">Thickness of the stroke</param>
    /// <param name="RectangleRect">Position of the rectangle</param>
    void DrawRectangle(Inv.Colour RectangleFillColour, Inv.Colour RectangleStrokeColour, int RectangleStrokeThickness, Inv.Rect RectangleRect);
    /// <summary>
    /// Draw a wedge of an ellipse.
    /// </summary>
    /// <param name="ArcFillColour"></param>
    /// <param name="ArcStrokeColour"></param>
    /// <param name="ArcStrokeThickness"></param>
    /// <param name="ArcCenter"></param>
    /// <param name="ArcRadius"></param>
    /// <param name="ArcStartAngle">Starting angle between 0 and 360</param>
    /// <param name="ArcSweepAngle">Ending angle between 0 and 360</param>
    void DrawArc(Inv.Colour ArcFillColour, Inv.Colour ArcStrokeColour, int ArcStrokeThickness, Inv.Point ArcCenter, Inv.Point ArcRadius, float ArcStartAngle, float ArcSweepAngle);
    /// <summary>
    /// Draw an ellipse on the canvas.
    /// </summary>
    /// <param name="EllipseFillColour"></param>
    /// <param name="EllipseStrokeColour"></param>
    /// <param name="EllipseStrokeThickness"></param>
    /// <param name="EllipseCenter"></param>
    /// <param name="EllipseRadius"></param>
    void DrawEllipse(Inv.Colour EllipseFillColour, Inv.Colour EllipseStrokeColour, int EllipseStrokeThickness, Inv.Point EllipseCenter, Inv.Point EllipseRadius);
    /// <summary>
    /// Draw an image on the canvas.
    /// </summary>
    /// <param name="ImageSource"></param>
    /// <param name="ImageRect"></param>
    /// <param name="ImageOpacity"></param>
    /// <param name="ImageTint"></param>
    /// <param name="ImageMirror"></param>
    /// <param name="ImageRotation"></param>
    void DrawImage(Inv.Image ImageSource, Inv.Rect ImageRect, float ImageOpacity = 1.0F, Inv.Colour ImageTint = null, Inv.Mirror? ImageMirror = null, Inv.Rotation? ImageRotation = null);
    /// <summary>
    /// Draw a polygon on the canvas.
    /// </summary>
    /// <param name="FillColour"></param>
    /// <param name="StrokeColour"></param>
    /// <param name="StrokeThickness"></param>
    /// <param name="LineJoin"></param>
    /// <param name="StartPoint"></param>
    /// <param name="PointArray"></param>
    void DrawPolygon(Inv.Colour FillColour, Inv.Colour StrokeColour, int StrokeThickness, Inv.LineJoin LineJoin, bool IsClosed, Inv.Point StartPoint, params Inv.Point[] PointArray);
  }

  public interface DrawMask
  {
    void DrawRectangle(Inv.Rect Rect, Inv.Colour FillColour, int StrokeThickness, Inv.Colour StrokeColour);
    void DrawImage(Inv.Rect Rect, Inv.Image Image, float Opacity = 1.0F, Inv.Mirror? Mirror = null);
  }

  /// <summary>
  /// The flow is a virtualised list of panels.
  /// It can be thought of as a combination of a vertical scroll and stack.
  /// </summary>
  public sealed class Flow : Inv.Control
  {
    /// <summary>
    /// Create a new flow.
    /// </summary>
    public Flow()
      : this(null)
    {
    }
    internal Flow(Inv.Surface Surface)
      : base(Surface)
    {
      this.SectionList = [];
    }

    /// <summary>
    /// Create a new flow.
    /// </summary>
    /// <returns></returns>
    public static Inv.Flow New() => new Inv.Flow();

    /// <summary>
    /// Number of sections in the flow.
    /// </summary>
    public int SectionCount
    {
      get => SectionList.Count;
    }
    /// <summary>
    /// The fixture panel above the flow.
    /// The fixture remains visible when you scroll vertically.
    /// The fixture scrolls horizontally to stay synchronised with the content.
    /// </summary>
    public Inv.Panel Fixture { get; private set; }
    /// <summary>
    /// Handle to be notified when the user requested a refresh with a gesture.
    /// </summary>
    public event Action<Inv.FlowRefresh> RefreshEvent
    {
      add
      {
        RequireThreadAffinity();
        RefreshDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        RefreshDelegate -= value;
        Change();
      }
    }

    /// <summary>
    /// Set the header panel for the flow.
    /// </summary>
    /// <param name="FixturePanel"></param>
    public void SetFixture(Inv.Panel FixturePanel)
    {
      RequireThreadAffinity();

      if (this.Fixture != FixturePanel)
      {
        if (this.Fixture != null)
          RemoveChild(this.Fixture);

        this.Fixture = FixturePanel;

        if (this.Fixture != null)
          AddChild(Fixture);

        Change();
      }
    }
    /// <summary>
    /// Add a new section to the flow.
    /// </summary>
    /// <returns></returns>
    public Inv.FlowSection AddSection()
    {
      RequireThreadAffinity();

      var Section = new FlowSection(this);
      SectionList.Add(Section);
      Reload();
      return Section;
    }
    /// <summary>
    /// Add a paged section to the flow.
    /// </summary>
    /// <typeparam name="TItem"></typeparam>
    /// <returns></returns>
    public Inv.PagedSection<TItem> AddPagedSection<TItem>(int RequestedBatchSize = 20)
    {
      RequireThreadAffinity();

      var Section = new PagedSection<TItem>(AddSection(), RequestedBatchSize);
      Reload();
      return Section;
    }
    /// <summary>
    /// Add a cached section to the flow.
    /// </summary>
    /// <typeparam name="TItem"></typeparam>
    /// <returns></returns>
    public Inv.CachedSection<TItem> AddCachedSection<TItem>()
    {
      RequireThreadAffinity();

      var Section = new CachedSection<TItem>(AddSection());
      Reload();
      return Section;
    }
    /// <summary>
    /// Remove a section from the flow.
    /// </summary>
    /// <param name="Section"></param>
    public void RemoveSection(Inv.FlowSection Section)
    {
      RequireThreadAffinity();

      if (SectionList.Remove(Section))
        Reload();
    }
    /// <summary>
    /// Remove all sections from the flow.
    /// </summary>
    public void RemoveSections()
    {
      RequireThreadAffinity();

      if (SectionList.Count > 0)
      {
        SectionList.Clear();
        Reload();
      }
    }
    /// <summary>
    /// Request a non-user reload of the flow (no refresh animation).
    /// </summary>
    public void Reload()
    {
      RequireThreadAffinity();

      this.IsReload = true;
      Change();
    }
    /// <summary>
    /// Programmatically invoke the user refresh animation and fire the <see cref="RefreshEvent"/>.
    /// </summary>
    public void Refresh()
    {
      RequireThreadAffinity();

      this.IsRefresh = true;
      Change();
    }

    internal override Inv.ControlType ControlType => Inv.ControlType.Flow;
    internal bool IsReload { get; set; }
    internal bool IsRefresh { get; set; }
    internal int? ScrollSection { get; set; }
    internal int? ScrollIndex { get; set; }
    internal bool IsRefreshable
    {
      get
      {
        RequireThreadAffinity();

        return RefreshDelegate != null;
      }
    }
    /// <summary>
    /// List the sections in the flow.
    /// </summary>
    /// <returns></returns>
    internal IReadOnlyList<Inv.FlowSection> Sections => SectionList;

    internal void RefreshInvoke(Inv.FlowRefresh TileRefresh)
    {
      RequireThreadAffinity();

      RefreshDelegate?.Invoke(TileRefresh);
    }
    internal void ScrollTo(Inv.FlowSection Section, int Index)
    {
      RequireThreadAffinity();

      var SectionIndex = SectionList.IndexOf(Section);
      if (SectionIndex != -1)
      {
        ScrollSection = SectionIndex;
        ScrollIndex = Index;
        Change();
      }
    }
    internal void RemovePanel(Inv.Panel Panel)
    {
      if (Panel != null)
      {
        foreach (var Section in SectionList)
        {
          if (Section.Header == Panel)
          {
            Section.SetHeader(null);
            return;
          }

          if (Section.Header == Panel)
          {
            Section.SetFooter(null);
            return;
          }
        }

        // not a section header or footer, must be an item panel.
        RemoveChild(Panel);
      }
    }
    /// <summary>
    /// Add a batched section to the flow.
    /// </summary>
    /// <typeparam name="TItem"></typeparam>
    /// <returns></returns>
    internal Inv.BatchedSection<TItem> AddBatchedSection<TItem>(int RequestedBatchSize = 20)
    {
      RequireThreadAffinity();

      var Section = new BatchedSection<TItem>(AddSection(), RequestedBatchSize);
      Reload();
      return Section;
    }

    private readonly Inv.DistinctList<Inv.FlowSection> SectionList;
    private Action<Inv.FlowRefresh> RefreshDelegate;

    internal struct IndexPath
    {
      public int Section;
      public int Index;
    }
  }

  /// <summary>
  /// Call <see cref="Complete"/> when you have finished loading data for the flow.
  /// Allows background loading of data.
  /// </summary>
  public sealed class FlowRefresh
  {
    internal FlowRefresh(Flow Flow, Action CompleteAction)
    {
      this.Flow = Flow;
      this.CompleteAction = CompleteAction;
    }

    /// <summary>
    /// Call this method when you have finished loading the data for the flow.
    /// </summary>
    public void Complete()
    {
      Flow.RequireThreadAffinity();

      CompleteAction();
    }

    private readonly Flow Flow;
    private readonly Action CompleteAction;
  }

  /// <summary>
  /// A section or group in the flow.
  /// </summary>
  public sealed class FlowSection
  {
    internal FlowSection(Inv.Flow Flow)
    {
      this.Flow = Flow;
    }

    /// <summary>
    /// Number of items in the section.
    /// </summary>
    public int ItemCount { get; private set; }
    /// <summary>
    /// The leading header panel of the section.
    /// </summary>
    public Inv.Panel Header { get; private set; }
    /// <summary>
    /// The trailing footer panel of the section.
    /// </summary>
    public Inv.Panel Footer { get; private set; }
    /// <summary>
    /// Handle to return the panel by index for this section.
    /// </summary>
    public event Func<int, Inv.Panel> ItemQuery;
    /// <summary>
    /// Handle to recycle the panel that was originally returned from ItemQuery.
    /// </summary>
    public event Action<int, Inv.Panel> RecycleEvent;

    /// <summary>
    /// Set the number of items in this section.
    /// </summary>
    /// <param name="ItemCount"></param>
    public void SetItemCount(int ItemCount)
    {
      RequireThreadAffinity();

      this.ItemCount = ItemCount;

      Flow.Reload();
    }
    /// <summary>
    /// Set the header panel for the section.
    /// </summary>
    /// <param name="HeaderPanel"></param>
    public void SetHeader(Inv.Panel HeaderPanel)
    {
      RequireThreadAffinity();

      if (this.Header != null)
        Flow.RemoveChild(this.Header);

      this.Header = HeaderPanel;

      if (this.Header != null)
        Flow.AddChild(Header);

      Flow.Reload();
    }
    /// <summary>
    /// Set the footer panel for the section.
    /// </summary>
    /// <param name="FooterPanel"></param>
    public void SetFooter(Inv.Panel FooterPanel)
    {
      RequireThreadAffinity();

      if (this.Footer != null)
        Flow.RemoveChild(this.Footer);

      this.Footer = FooterPanel;

      if (this.Footer != null)
        Flow.AddChild(Footer);

      Flow.Reload();
    }
    /// <summary>
    /// Reload this section.
    /// </summary>
    public void Reload()
    {
      Flow.Reload();
    }
    /// <summary>
    /// Scroll the item at the index into view.
    /// </summary>
    /// <param name="Index"></param>
    public void ScrollToItemAtIndex(int Index)
    {
      Flow.ScrollTo(this, Index);
    }

    internal Inv.Flow Flow { get; private set; }

    internal void RequireThreadAffinity()
    {
      if (Inv.Assert.IsEnabled)
        Flow.RequireThreadAffinity();
    }
    internal Inv.Panel ItemInvoke(int Item)
    {
      RequireThreadAffinity();

      if (ItemQuery != null)
      {
        var Result = ItemQuery(Item);

        if (Result != null && Result.Control.Parent == null)
          Flow.AddChild(Result);
        else
          Debug.Assert(Result == null || Result.Control.Parent == Flow, "Flow returned an item that is already parented to another panel.");

        return Result;
      }

      return null;
    }
    internal void RecycleInvoke(int Item, Inv.Panel Panel)
    {
      RequireThreadAffinity();

      if (Panel != null && Panel.Control.Parent == Flow)
        Flow.RemoveChild(Panel);
      else if (Panel != null)
        Debug.Assert(Panel.Control.Parent == null, "Flow recycled an item that is parented to another panel instead of this flow.");

      RecycleEvent?.Invoke(Item, Panel);
    }
  }

  /// <summary>
  /// Cached sections are for virtual lists where the entire list is known.
  /// The caching is for reusing the tiles displayed in the viewport.
  /// </summary>
  /// <typeparam name="TItem"></typeparam>
  public sealed class CachedSection<TItem>
  {
    internal CachedSection(Inv.FlowSection Base)
    {
      this.Base = Base;

      this.ActiveTileDictionary = []; // active tiles in the viewport.
      this.InactiveTileList = []; // cached tiles not in the viewport.

      Base.ItemQuery += (Item) =>
      {
        //System.Diagnostics.Debug.WriteLine("CACHING ITEM: " + Item);

        // TODO: remove from the start of the InactiveTileList so that the viewport reuses the same tiles in the same previous order (treat as a queue, not a stack).
        var Result = ActiveTileDictionary.GetOrAdd(Item, I => InactiveTileList.RemoveFirstOrNull() ?? NewFunction?.Invoke() ?? default);

        ComposeAction?.Invoke(Result, RecordArray != null ? RecordArray[Item] : default);

        return Result;
      };
      Base.RecycleEvent += (Item, Panel) =>
      {
        //System.Diagnostics.Debug.WriteLine("UNCACHING ITEM: " + Item);

        // recycle tiles that have left the viewport by putting them in an inactive list.
        var Tile = ActiveTileDictionary.RemoveValueOrNull(Item);
        if (Tile != null)
          InactiveTileList.Add(Tile);
      };
    }

    /// <summary>
    /// Specify how to produce a new tile and compose an item into a tile.
    /// </summary>
    /// <typeparam name="TTile"></typeparam>
    /// <param name="NewFunction"></param>
    /// <param name="ComposeAction"></param>
    public void Template<TTile>(Func<TTile> NewFunction, Action<TTile, TItem> ComposeAction)
      where TTile : Inv.Panel
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(this.NewFunction == null && this.ComposeAction == null, "Template must not be specified more than once.");

      this.NewFunction = () => NewFunction();
      this.ComposeAction = (T, R) => ComposeAction((TTile)T, R);
    }
    /// <summary>
    /// Clear all items from the cached section.
    /// </summary>
    public void Clear()
    {
      this.RecordArray = [];

      Base.SetItemCount(0);
    }
    /// <summary>
    /// Load the 
    /// </summary>
    /// <param name="Records"></param>
    public void Load(IEnumerable<TItem> Records)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(this.NewFunction != null && this.ComposeAction != null, "Template must be specified before Load.");

      this.RecordArray = Records.ToArray();

      Base.SetItemCount(RecordArray.Length);
    }
    /// <summary>
    /// Reload the cached section.
    /// </summary>
    public void Reload()
    {
      Base.Reload();
    }
    /// <summary>
    /// Set the header panel for the section.
    /// </summary>
    /// <param name="HeaderPanel"></param>
    public void SetHeader(Inv.Panel HeaderPanel) => Base.SetHeader(HeaderPanel);
    /// <summary>
    /// Set the footer panel for the section.
    /// </summary>
    /// <param name="FooterPanel"></param>
    public void SetFooter(Inv.Panel FooterPanel) => Base.SetFooter(FooterPanel);

    private readonly Inv.FlowSection Base;
    private readonly Dictionary<int, Inv.Panel> ActiveTileDictionary;
    private readonly Inv.DistinctList<Inv.Panel> InactiveTileList;
    private Func<Inv.Panel> NewFunction;
    private Action<Inv.Panel, TItem> ComposeAction;
    private TItem[] RecordArray;
  }

  /// <summary>
  /// This delegate is used for requesting a page window of items.
  /// </summary>
  /// <typeparam name="TItem"></typeparam>
  /// <param name="StartIndex"></param>
  /// <param name="EndIndex"></param>
  /// <param name="ReturnAction"></param>
  public delegate void PagedSectionRequestDelegate<TItem>(int StartIndex, int EndIndex, Action<IEnumerable<TItem>> ReturnAction);

  /// <summary>
  /// Paged sections are for virtual lists where the number of items is known, but not the individual items.
  /// 
  /// There is also caching to reuse the tiles displayed in the viewport.
  /// </summary>
  /// <typeparam name="TItem"></typeparam>
  public sealed class PagedSection<TItem>
  {
    internal PagedSection(Inv.FlowSection Base, int RequestedBatchSize)
    {
      this.RecordDictionary = [];

      this.Base = Base;

      this.ActiveTileDictionary = []; // active tiles in the viewport.
      this.InactiveTileList = []; // cached tiles not in the viewport.
      this.RequestedBatchSet = [];

      Base.ItemQuery += (Item) =>
      {
        //System.Diagnostics.Debug.WriteLine("PAGING ITEM: " + Item);

        var Result = ActiveTileDictionary.GetOrAdd(Item, I => InactiveTileList.RemoveLastOrNull() ?? NewFunction?.Invoke() ?? default);

        ComposeAction?.Invoke(Result, RecordDictionary.GetValueOrDefault(Item));

        var BatchIndex = Item / RequestedBatchSize;
        if (RequestedBatchSet.Add(BatchIndex))
        {
          // previous batch.
          var FirstBatch = BatchIndex;
          if (Item - RequestedBatchSize >= 0 && RequestedBatchSet.Add(FirstBatch - 1))
            FirstBatch--;

          // subsequent batch.
          var LastBatch = BatchIndex;
          if (Item + RequestedBatchSize < this.Base.ItemCount && RequestedBatchSet.Add(LastBatch + 1))
            LastBatch++;

          var StartIndex = (FirstBatch * RequestedBatchSize);
          var EndIndex = Math.Min(this.Base.ItemCount, (LastBatch * RequestedBatchSize) + RequestedBatchSize) - 1;

          if (StartIndex <= EndIndex)
          {
            RequestEvent?.Invoke(StartIndex, EndIndex, (Records) =>
            {
              Base.Flow.Window.Application.RequireThreadAffinity();

              var RecordEnumerator = Records.GetEnumerator();

              for (var TileIndex = StartIndex; TileIndex <= EndIndex; TileIndex++)
              {
                if (RecordEnumerator.MoveNext())
                {
                  // remember the loaded records.
                  var Record = RecordEnumerator.Current;
                  RecordDictionary[TileIndex] = Record;

                  // compose if the tile is currently in the viewport.
                  var Tile = ActiveTileDictionary.GetValueOrNull(TileIndex);
                  if (Tile != null)
                    ComposeAction?.Invoke(Tile, Record);
                  //else
                  //  Debug.WriteLine($"MISSED = {TileIndex}");
                }
              }
            });
          }
        }

        return Result;
      };
      Base.RecycleEvent += (Item, Panel) =>
      {
        Base.Flow.Window.Application.RequireThreadAffinity();

        //System.Diagnostics.Debug.WriteLine("UNPAGING ITEM: " + Item);

        // recycle tiles that have left the viewport by putting them in an inactive list.
        if (Panel != null)
        {
          var Result = ActiveTileDictionary.RemoveValueOrNull(Item);
          if (Result == Panel)
            InactiveTileList.Add(Panel);
          //else
          //  Debug.WriteLine($"CONFUSED = {Item}");
        }
      };
    }

    /// <summary>
    /// Handle this event to load the items for a given page window.
    /// </summary>
    public event PagedSectionRequestDelegate<TItem> RequestEvent;

    /// <summary>
    /// Specify how to produce a new tile and compose an item into a tile.
    /// </summary>
    /// <typeparam name="TTile"></typeparam>
    /// <param name="NewFunction"></param>
    /// <param name="ComposeAction"></param>
    public void Template<TTile>(Func<TTile> NewFunction, Action<TTile, TItem> ComposeAction)
      where TTile : Inv.Panel
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(this.NewFunction == null && this.ComposeAction == null, "Template must not be specified more than once.");

      this.NewFunction = () => NewFunction();
      this.ComposeAction = (T, R) => ComposeAction((TTile)T, R);
    }
    /// <summary>
    /// Load a paged section given a total item count.
    /// </summary>
    /// <param name="ItemCount"></param>
    public void Load(int ItemCount)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(this.NewFunction != null && this.ComposeAction != null, "Template must be specified before Load.");
        Inv.Assert.Check(RequestEvent != null, "RequestQuery must be specified before Load.");
      }

      InactiveTileList.AddRange(ActiveTileDictionary.Values);
      ActiveTileDictionary.Clear();
      RequestedBatchSet.Clear();
      RecordDictionary.Clear();

      Base.SetItemCount(ItemCount);
      Base.Reload();
    }

    private readonly Inv.FlowSection Base;
    private readonly HashSet<int> RequestedBatchSet;
    private readonly Dictionary<int, TItem> RecordDictionary;
    private readonly Dictionary<int, Inv.Panel> ActiveTileDictionary;
    private readonly Inv.DistinctList<Inv.Panel> InactiveTileList;
    private Func<Inv.Panel> NewFunction;
    private Action<Inv.Panel, TItem> ComposeAction;
  }

  /// <summary>
  /// Batched sections are for virtual lists where the data is fetched in batch from a remote server.
  /// This is important for web and database query results.
  /// </summary>
  /// <typeparam name="TRecord"></typeparam>
  internal sealed class BatchedSection<TRecord> : Inv.Mimic<Inv.FlowSection>
  {
    internal BatchedSection(Inv.FlowSection Base, int RequestedBatchSize)
    {
      this.Base = Base;
      this.CacheDictionary = [];
      this.RequestedBatchSet = [];
      this.CancellationSource = new System.Threading.CancellationTokenSource();
      this.RequestSize = RequestedBatchSize;

      Base.ItemQuery += (Item) =>
      {
        RequestBatchAtIndex(Item);

        if (CacheDictionary.TryGetValue(Item, out var Value))
          return ItemQuery?.Invoke(Value);
        else
          return null; // TODO: placeholder.
      };
    }

    /// <summary>
    /// Handle to return the data for a requested batch.
    /// </summary>
    public event Action<int, int, System.Threading.CancellationToken, Action<IEnumerable<TRecord>>> RequestEvent;
    /// <summary>
    /// Given a batched item, return the panel.
    /// </summary>
    public event Func<TRecord, Inv.Panel> ItemQuery;

    /// <summary>
    /// Set the number of items in the batched section.
    /// </summary>
    /// <param name="ItemCount"></param>
    public void SetItemCount(int ItemCount)
    {
      if (ItemCount != Base.ItemCount)
      {
        ClearCache();
        Base.SetItemCount(ItemCount);
      }
    }
    /// <summary>
    /// Set the header panel for the batched section.
    /// </summary>
    /// <param name="HeaderPanel"></param>
    public void SetHeader(Inv.Panel HeaderPanel)
    {
      Base.SetHeader(HeaderPanel);
    }
    /// <summary>
    /// Set the footer panel for the batched section.
    /// </summary>
    /// <param name="FooterPanel"></param>
    public void SetFooter(Inv.Panel FooterPanel)
    {
      Base.SetFooter(FooterPanel);
    }
    /// <summary>
    /// Reload the batched section.
    /// </summary>
    public void Reload()
    {
      ClearCache();
      Base.Reload();
    }

    private void ClearCache()
    {
      CancellationSource.Cancel();
      this.CancellationSource = new System.Threading.CancellationTokenSource();

      CacheDictionary.Clear();
      RequestedBatchSet.Clear();
    }
    private void RequestBatchAtIndex(int Index)
    {
      if (Index < 0 || Base.ItemCount <= Index)
        return;

      var IndexBatch = Index / RequestSize;

      if (RequestedBatchSet.Add(IndexBatch))
      {
        if (RequestEvent != null)
        {
          // previous batch.
          var FirstBatch = IndexBatch;
          if (Index - RequestSize >= 0 && RequestedBatchSet.Add(FirstBatch - 1))
            FirstBatch--;

          // subsequent batch.
          var LastBatch = IndexBatch;
          if (Index + RequestSize < Base.ItemCount && RequestedBatchSet.Add(LastBatch + 1))
            LastBatch++;

          var ItemIndex = (FirstBatch * RequestSize);
          var ItemCount = Math.Min(Base.ItemCount - ItemIndex, (LastBatch - FirstBatch + 1) * RequestSize);

          var Token = CancellationSource.Token;

          RequestEvent(ItemIndex, ItemCount, Token, (ResultItems) =>
          {
            Base.RequireThreadAffinity();

            if (Token.IsCancellationRequested)
              return;

            var CurrentIndex = ItemIndex;
            foreach (var ResultItem in ResultItems)
              CacheDictionary.Add(CurrentIndex++, ResultItem);

            if (Inv.Assert.IsEnabled)
              Inv.Assert.Check(CurrentIndex - ItemIndex == ItemCount, "An incorrect number of items was passed to the RequestBatch callback");

            Base.Reload();
          });
        }
      }
    }

    private readonly Dictionary<int, TRecord> CacheDictionary;
    private readonly HashSet<int> RequestedBatchSet;
    private readonly int RequestSize;
    private System.Threading.CancellationTokenSource CancellationSource;
  }

  /// <summary>
  /// The frame is simply a container for another panel and has no other layout behaviour.
  /// It is used to switch content inside another layout and can be animated using transitions.
  /// </summary>
  public sealed class Frame : Inv.Control
  {
    /// <summary>
    /// Create a new frame.
    /// </summary>
    public Frame()
      : this(null)
    {
    }
    internal Frame(Inv.Surface Surface)
      : base(Surface)
    {
      this.ContentSingleton = new Singleton<Inv.Panel>(this);
    }

    /// <summary>
    /// Create a new frame.
    /// </summary>
    /// <returns></returns>
    public static Inv.Frame New() => new Inv.Frame();

    /// <summary>
    /// Contained content of the frame.
    /// </summary>
    public Inv.Panel Content
    {
      get => ContentSingleton.Data;
      set
      {
        RequireThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
          {
            RemoveChild(ContentSingleton.Data);
            ContentSingleton.Data = null;
          }

          if (value != null)
          {
            AddChild(value);
            ContentSingleton.Data = value;
          }
        }
      }
    }

    /// <summary>
    /// Transition the content to another panel.
    /// </summary>
    /// <param name="ToPanel"></param>
    /// <returns></returns>
    public Inv.Transition Transition(Inv.Panel ToPanel)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(ToPanel == null || ToPanel.Control.Window == Window, "Panel must belong to this window.");

      if (ActiveTransition == null || Content != ToPanel)
      {
        this.FromPanel = Content;
        this.ActiveTransition = new Transition(Window);
        this.Content = ToPanel;

        Change(); // for the case where ActiveTransition == null and Content == ToPanel.
      }

      return ActiveTransition;
    }

    internal override Inv.ControlType ControlType => Inv.ControlType.Frame;
    internal Inv.Singleton<Inv.Panel> ContentSingleton { get; private set; }
    internal Inv.Transition ActiveTransition { get; set; }
    internal Inv.Panel FromPanel { get; set; }
  }

  /// <summary>
  /// This layout has rows and columns that can be sized as auto, star or fixed.
  /// Row and column spanning is not supported.
  /// </summary>
  public sealed class Table : Inv.Control
  {
    /// <summary>
    /// Create a new table.
    /// </summary>
    public Table()
      : this(null)
    {
    }
    internal Table(Inv.Surface Surface)
      : base(Surface)
    {
      this.RowCollection = new Collection<TableRow>(this);
      this.ColumnCollection = new Collection<TableColumn>(this);
      this.CellCollection = new Collection<TableCell>(this);
      this.CellGrid = new Grid<TableCell>();
    }

    /// <summary>
    /// Create a new table.
    /// </summary>
    /// <returns></returns>
    public static Inv.Table New() => new Inv.Table();

    /// <summary>
    /// List the rows in the table.
    /// </summary>
    public IReadOnlyList<Inv.TableRow> Rows
    {
      get
      {
        RequireThreadAffinity();
        return RowCollection;
      }
    }
    /// <summary>
    /// List the columns in the table.
    /// </summary>
    public IReadOnlyList<Inv.TableColumn> Columns
    {
      get
      {
        RequireThreadAffinity();
        return ColumnCollection;
      }
    }

    /// <summary>
    /// Remove a panel from the table rows, columns or cells.
    /// </summary>
    /// <param name="Panel"></param>
    public void RemovePanel(Inv.Panel Panel)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Panel, nameof(Panel));

      if (Panel != null)
      {
        foreach (var Row in RowCollection)
        {
          if (Row.Content == Panel)
            Row.Content = null;
        }

        foreach (var Column in ColumnCollection)
        {
          if (Column.Content == Panel)
            Column.Content = null;
        }

        foreach (var Cell in CellCollection)
        {
          if (Cell.Content == Panel)
            Cell.Content = null;
        }
      }
    }
    /// <summary>
    /// Remove all panels from the table columns, rows and cells.
    /// </summary>
    public void RemovePanels()
    {
      RequireThreadAffinity();

      RemoveRows();

      RemoveColumns();

      RemoveCells();
    }
    /// <summary>
    /// Add a new auto-sized row to the table.
    /// </summary>
    /// <returns></returns>
    public Inv.TableRow AddAutoRow()
    {
      RequireThreadAffinity();

      var Result = AddRow();
      Result.Auto();
      return Result;
    }
    /// <summary>
    /// Add a new star-sized row to the table.
    /// </summary>
    /// <returns></returns>
    public Inv.TableRow AddStarRow(int Units = 1)
    {
      RequireThreadAffinity();

      var Result = AddRow();
      Result.Star(Units);
      return Result;
    }
    /// <summary>
    /// Add a new fixed-sized row to the table.
    /// </summary>
    /// <returns></returns>
    public Inv.TableRow AddFixedRow(int Points)
    {
      RequireThreadAffinity();

      var Result = AddRow();
      Result.Fixed(Points);
      return Result;
    }
    /// <summary>
    /// Insert a row in the table.
    /// </summary>
    /// <param name="Index"></param>
    /// <returns></returns>
    public Inv.TableRow InsertRow(int Index)
    {
      RequireThreadAffinity();

      if (Index < 0)
        Index = 0;
      if (Index >= RowCollection.Count)
        return AddRow();

      foreach (var Row in RowCollection.Where(R => R.Index >= Index))
        Row.Index++;

      var Result = new TableRow(this, Index);
      RowCollection.Insert(Index, Result);

      CellGrid.Resize(CellGrid.Width, CellGrid.Height + 1);

      for (var RowIndex = RowCollection.Count - 2; RowIndex >= Index; RowIndex--)
        foreach (var Column in ColumnCollection)
          CellGrid[Column.Index, RowIndex + 1] = CellGrid[Column.Index, RowIndex];

      foreach (var Column in ColumnCollection)
        CellGrid[Column.Index, Index] = NewCell(Column, Result);

      return Result;
    }
    /// <summary>
    /// Remove a row from the table.
    /// </summary>
    /// <param name="Row"></param>
    public void RemoveRow(TableRow Row)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(ValidRow(Row), "Rows can only be removed from their own table");

      CellCollection.RemoveWhere(Cell => Cell.Row.Index == Row.Index);

      for (var RowIndex = Row.Index; RowIndex < RowCollection.Count - 1; RowIndex++)
      {
        foreach (var Column in ColumnCollection)
          CellGrid[Column.Index, RowIndex] = CellGrid[Column.Index, RowIndex + 1];
      }

      RowCollection.Remove(Row);
      CellGrid.Resize(CellGrid.Width, CellGrid.Height - 1);

      foreach (var MovedRow in RowCollection.Where(R => R.Index > Row.Index))
        MovedRow.Index--;

      Debug.Assert(CellGrid.Width * CellGrid.Height == CellCollection.Count);
    }
    /// <summary>
    /// Remove all rows from the table.
    /// </summary>
    public void RemoveRows()
    {
      RequireThreadAffinity();

      if (RowCollection.Count > 0)
      {
        foreach (var Row in RowCollection)
        {
          if (Row.Content != null)
            RemoveChild(Row.Content);
        }
        RowCollection.Clear();
      }

      RemoveCells();
    }
    /// <summary>
    /// Remove all columns from the table.
    /// </summary>
    public void RemoveColumns()
    {
      RequireThreadAffinity();

      if (ColumnCollection.Count > 0)
      {
        foreach (var Column in ColumnCollection)
        {
          if (Column.Content != null)
            RemoveChild(Column.Content);
        }
        ColumnCollection.Clear();
      }

      RemoveCells();
    }
    /// <summary>
    /// Remove all cells from the table.
    /// </summary>
    public void RemoveCells()
    {
      RequireThreadAffinity();

      if (CellCollection.Count > 0)
      {
        foreach (var Cell in CellCollection)
        {
          if (Cell.Content != null)
            RemoveChild(Cell.Content);
        }
        CellCollection.Clear();
      }

      CellGrid.Resize(ColumnCollection.Count, RowCollection.Count);
    }
    /// <summary>
    /// Add a new auto-sized column to the table.
    /// </summary>
    /// <returns></returns>
    public Inv.TableColumn AddAutoColumn()
    {
      RequireThreadAffinity();

      var Result = AddColumn();
      Result.Auto();
      return Result;
    }
    /// <summary>
    /// Add a new star-sized column to the table.
    /// </summary>
    /// <returns></returns>
    public Inv.TableColumn AddStarColumn(int Units = 1)
    {
      RequireThreadAffinity();

      var Result = AddColumn();
      Result.Star(Units);
      return Result;
    }
    /// <summary>
    /// Add a new fixed-sized column to the table.
    /// </summary>
    /// <returns></returns>
    public Inv.TableColumn AddFixedColumn(int Points)
    {
      RequireThreadAffinity();

      var Result = AddColumn();
      Result.Fixed(Points);
      return Result;
    }
    /// <summary>
    /// Insert a column in the table.
    /// </summary>
    /// <param name="Index"></param>
    /// <returns></returns>
    public Inv.TableColumn InsertColumn(int Index)
    {
      RequireThreadAffinity();

      if (Index < 0)
        Index = 0;
      if (Index >= ColumnCollection.Count)
        return AddColumn();

      foreach (var Column in ColumnCollection.Where(C => C.Index >= Index))
        Column.Index++;

      var Result = new TableColumn(this, Index);
      ColumnCollection.Insert(Index, Result);

      CellGrid.Resize(CellGrid.Width + 1, CellGrid.Height);

      for (var ColumnIndex = ColumnCollection.Count - 2; ColumnIndex >= Index; ColumnIndex--)
      {
        foreach (var Row in RowCollection)
          CellGrid[ColumnIndex + 1, Row.Index] = CellGrid[ColumnIndex, Row.Index];
      }

      foreach (var Row in RowCollection)
        CellGrid[Index, Row.Index] = NewCell(Result, Row);

      return Result;
    }
    /// <summary>
    /// Remove a column from the table.
    /// </summary>
    /// <param name="Column"></param>
    public void RemoveColumn(Inv.TableColumn Column)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(ValidColumn(Column), "Columns can only be removed from their own table");

      CellCollection.RemoveWhere(Cell => Cell.Column.Index == Column.Index);

      for (var ColumnIndex = Column.Index; ColumnIndex < ColumnCollection.Count - 1; ColumnIndex++)
      {
        foreach (var Row in RowCollection)
          CellGrid[ColumnIndex, Row.Index] = CellGrid[ColumnIndex + 1, Row.Index];
      }

      ColumnCollection.Remove(Column);
      CellGrid.Resize(CellGrid.Width - 1, CellGrid.Height);

      foreach (var MovedColumn in ColumnCollection.Where(C => C.Index > Column.Index))
        MovedColumn.Index--;

      Debug.Assert(CellGrid.Width * CellGrid.Height == CellCollection.Count);
    }
    /// <summary>
    /// Get a cell by the column and row index.
    /// </summary>
    /// <param name="ColumnIndex"></param>
    /// <param name="RowIndex"></param>
    /// <returns></returns>
    public TableCell GetCell(int ColumnIndex, int RowIndex)
    {
      RequireThreadAffinity();

      return GetCell(ColumnCollection[ColumnIndex], RowCollection[RowIndex]);
    }
    /// <summary>
    /// Get a cell by the column and row.
    /// </summary>
    /// <param name="Column"></param>
    /// <param name="Row"></param>
    /// <returns></returns>
    public TableCell GetCell(Inv.TableColumn Column, TableRow Row)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(ValidColumn(Column), "Column must belong to this table.");
        Inv.Assert.Check(ValidRow(Row), "Row must belong to this table.");
      }

      return CellGrid[Column.Index, Row.Index];
    }
    /// <summary>
    /// Enumerate all cells in the table (returned row-by-row).
    /// </summary>
    /// <returns></returns>
    public IEnumerable<Inv.TableCell> GetCells()
    {
      RequireThreadAffinity();

      return CellGrid;
    }
    /// <summary>
    /// Adjust the number of rows and columns in the table.
    /// </summary>
    /// <param name="ComposeColumns"></param>
    /// <param name="ComposeRows"></param>
    public void Compose(int ComposeColumns, int ComposeRows)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(ComposeColumns >= 0, "Columns must be zero or more.");
        Inv.Assert.Check(ComposeRows >= 0, "Rows must be zero or more.");
      }

      if (ComposeColumns != Columns.Count || ComposeRows != Rows.Count)
      {
        CellGrid.Resize(ComposeColumns, ComposeRows);

        while (ColumnCollection.Count > ComposeColumns)
        {
          var Column = ColumnCollection[ColumnCollection.Count - 1];
          CellCollection.RemoveWhere(R => R.Column == Column);
          ColumnCollection.RemoveAt(ColumnCollection.Count - 1);
        }

        while (ColumnCollection.Count < ComposeColumns)
          ColumnCollection.Add(new TableColumn(this, ColumnCollection.Count));

        while (RowCollection.Count > ComposeRows)
        {
          var Row = RowCollection[RowCollection.Count - 1];
          CellCollection.RemoveWhere(R => R.Row == Row);
          RowCollection.RemoveAt(RowCollection.Count - 1);
        }

        while (RowCollection.Count < ComposeRows)
          RowCollection.Add(new TableRow(this, RowCollection.Count));

        // TODO: this could be more efficient - only new rows and columns need cells.
        for (var Y = 0; Y < ComposeRows; Y++)
        {
          for (var X = 0; X < ComposeColumns; X++)
          {
            if (CellGrid[X, Y] == null)
              CellGrid[X, Y] = NewCell(ColumnCollection[X], RowCollection[Y]);
          }
        }
      }

      Debug.Assert(RowCollection.Count == ComposeRows);
      Debug.Assert(ColumnCollection.Count == ComposeColumns);
      Debug.Assert(CellGrid.Width * CellGrid.Height == CellCollection.Count);
    }
    /// <summary>
    /// Adjust the number of rows and columns in the table and apply the panels to the cells.
    /// </summary>
    /// <param name="PanelArray"></param>
    public void Compose(Inv.Panel[,] PanelArray)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(PanelArray.ExceptNull().IsDistinct(), "Table.Compose must be distinct panels without any spanning cells.");

      Compose(PanelArray.GetLength(1), PanelArray.GetLength(0));

      foreach (var Cell in CellCollection)
        Cell.Content = PanelArray[Cell.Row.Index, Cell.Column.Index];
    }

    internal override Inv.ControlType ControlType => Inv.ControlType.Table;
    internal Inv.Collection<Inv.TableRow> RowCollection { get; private set; }
    internal Inv.Collection<Inv.TableColumn> ColumnCollection { get; private set; }
    internal Inv.Collection<Inv.TableCell> CellCollection { get; private set; }

    internal bool CollectionRender()
    {
      RequireThreadAffinity();

      var Result = false;

      if (RowCollection.Render())
        Result = true;

      if (ColumnCollection.Render())
        Result = true;

      if (CellCollection.Render())
        Result = true;

      if (CellCollection.Any(C => C.IsChanged))
        Result = true;

      return Result;
    }
    internal Inv.TableColumn AddColumn()
    {
      var Result = new TableColumn(this, ColumnCollection.Count);
      ColumnCollection.Add(Result);

      CellGrid.Resize(CellGrid.Width + 1, CellGrid.Height);
      foreach (var Row in RowCollection)
        CellGrid[Result.Index, Row.Index] = NewCell(Result, Row);

      return Result;
    }
    internal Inv.TableRow AddRow()
    {
      var Result = new TableRow(this, RowCollection.Count);
      RowCollection.Add(Result);

      CellGrid.Resize(CellGrid.Width, CellGrid.Height + 1);
      foreach (var Column in ColumnCollection)
        CellGrid[Column.Index, Result.Index] = NewCell(Column, Result);

      return Result;
    }

    private bool ValidColumn(Inv.TableColumn Column) => Column.Table == this && ColumnCollection.Contains(Column);
    private bool ValidRow(Inv.TableRow Row) => Row.Table == this && RowCollection.Contains(Row);

    private Inv.TableCell NewCell(Inv.TableColumn Column, Inv.TableRow Row)
    {
      var Result = new TableCell(this, Column, Row);

      CellCollection.Add(Result);

      return Result;
    }

    private readonly Inv.Grid<TableCell> CellGrid;
  }

  /// <summary>
  /// This is a push-button which contains a content panel.
  /// </summary>
  public sealed class Button : Inv.Control
  {
    /// <summary>
    /// Create a new button.
    /// </summary>
    public Button(Inv.ButtonStyle Style)
      : this(null, Style)
    {
      Codepoint.Caller(); // set the codepoint to the parent caller.
    }
    private Button(Inv.Surface Surface, Inv.ButtonStyle Style)
      : base(Surface)
    {
      this.Style = Style;
      this.Codepoint = new Inv.Codepoint();
      this.Focus = new Inv.Focus(this);
      this.Tooltip = new Inv.Tooltip(this);
      this.ContentSingleton = new Inv.Singleton<Inv.Panel>(this);
      this.IsEnabledField = true;
      // this.IsFocusableField = false; // NOTE: this is the C# default.
    }
    internal static Inv.Button InlineNew(Inv.Surface Surface, Inv.ButtonStyle Style)
    {
      var Result = new Inv.Button(Surface, Style);
      Result.Codepoint.Set(2); // set the codepoint to the parent of the parent caller.
      return Result;
    }

    /// <summary>
    /// Create a new button.
    /// </summary>
    /// <returns></returns>
    public static Inv.Button New(Inv.ButtonStyle Style) => InlineNew(null, Style);
    /// <summary>
    /// Create a new stark button.
    /// </summary>
    /// <returns></returns>
    public static Inv.Button NewStark() => InlineNew(null, ButtonStyle.Stark);
    /// <summary>
    /// Create a new flat button.
    /// </summary>
    /// <returns></returns>
    public static Inv.Button NewFlat() => InlineNew(null, ButtonStyle.Flat);

    /// <summary>
    /// Marks the stack frame for the file:line of code that instantiated this button.
    /// </summary>
    public Inv.Codepoint Codepoint { get; private set; }
    /// <summary>
    /// The interaction style for the button.
    /// </summary>
    public Inv.ButtonStyle Style { get; private set; }
    /// <summary>
    /// The content is the layout panel inside the button.
    /// </summary>
    public Inv.Panel Content
    {
      get => ContentSingleton.Data;
      set
      {
        RequireThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
          {
            RemoveChild(ContentSingleton.Data);
            ContentSingleton.Data = null;
          }

          if (value != null)
          {
            AddChild(value);
            ContentSingleton.Data = value;
          }
        }
      }
    }
    /// <summary>
    /// When the button is enabled for user interaction.
    /// </summary>
    public bool IsEnabled
    {
      get
      {
        RequireThreadAffinity();

        return IsEnabledField;
      }
      set
      {
        RequireThreadAffinity();

        if (IsEnabledField != value)
        {
          this.IsEnabledField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// Button can be focused.
    /// </summary>
    public bool IsFocusable
    {
      get
      {
        RequireThreadAffinity();

        return IsFocusableField;
      }
      set
      {
        RequireThreadAffinity();

        if (IsFocusableField != value)
        {
          this.IsFocusableField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// Optional hint text as used in accessibility and tooltips.
    /// </summary>
    public string Hint
    {
      get
      {
        RequireThreadAffinity();

        return HintField;
      }
      set
      {
        RequireThreadAffinity();

        if (HintField != value)
        {
          this.HintField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// The focus manager for the button.
    /// </summary>
    public Inv.Focus Focus { get; private set; }
    /// <summary>
    /// The tooltip for the button.
    /// </summary>
    public Inv.Tooltip Tooltip { get; private set; }
    /// <summary>
    /// The user is over the button and has not yet moved away.
    /// </summary>
    public bool IsOver { get; private set; }
    /// <summary>
    /// The user pressed the button and has not yet released.
    /// </summary>
    public bool IsPressed { get; private set; }
    /// <summary>
    /// Over is fired when the user starts hovering over the button.
    /// </summary>
    public event Action OverEvent
    {
      add
      {
        RequireThreadAffinity();
        OverDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        OverDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Away is fired when the user moves the hover away from the button.
    /// </summary>
    public event Action AwayEvent
    {
      add
      {
        RequireThreadAffinity();
        AwayDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        AwayDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Press is fired when the user presses on the button.
    /// </summary>
    public event Action PressEvent
    {
      add
      {
        RequireThreadAffinity();
        PressDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        PressDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Release is fired when the user releases a pressed button.
    /// </summary>
    public event Action ReleaseEvent
    {
      add
      {
        RequireThreadAffinity();
        ReleaseDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        ReleaseDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Single tap is a short press on a touch screen or a left mouse click.
    /// </summary>
    public event Action SingleTapEvent;
    /// <summary>
    /// Context tap is a long press on a touch screen or a right mouse click.
    /// </summary>
    public event Action ContextTapEvent;

    /// <summary>
    /// Programmatically invoke the single tap.
    /// </summary>
    public void SingleTap()
    {
      SingleTapInvoke();
    }
    /// <summary>
    /// Ask if the single tap event has been handled.
    /// </summary>
    /// <returns></returns>
    public bool HasSingleTap()
    {
      return SingleTapEvent != null;
    }
    /// <summary>
    /// Remove the single tap event if it has been set.
    /// </summary>
    public void RemoveSingleTap()
    {
      this.SingleTapEvent = null;
    }
    /// <summary>
    /// Programmatically invoke the context tap.
    /// </summary>
    public void ContextTap()
    {
      ContextTapInvoke();
    }
    /// <summary>
    /// Ask if the context tap event has been handled.
    /// </summary>
    /// <returns></returns>
    public bool HasContextTap()
    {
      return ContextTapEvent != null;
    }
    /// <summary>
    /// Remove the single tap event if it has been set.
    /// </summary>
    public void RemoveContextTap()
    {
      this.ContextTapEvent = null;
    }
    /// <summary>
    /// Programmatically invoke the button press.
    /// </summary>
    public void Press()
    {
      PressInvoke();
    }
    /// <summary>
    /// Ask if the press event has been handled.
    /// </summary>
    /// <returns></returns>
    public bool HasPress()
    {
      return PressDelegate != null;
    }
    /// <summary>
    /// Programmatically invoke the button release.
    /// </summary>
    public void Release()
    {
      ReleaseInvoke();
    }
    /// <summary>
    /// Ask if the release event has been handled.
    /// </summary>
    /// <returns></returns>
    public bool HasRelease()
    {
      return ReleaseDelegate != null;
    }
    /// <summary>
    /// Programmatically invoke the button hover over.
    /// </summary>
    public void Over()
    {
      OverInvoke();
    }
    /// <summary>
    /// Ask if the Over event has been handled.
    /// </summary>
    /// <returns></returns>
    public bool HasOver()
    {
      return OverDelegate != null;
    }
    /// <summary>
    /// Programmatically invoke the button hover away.
    /// </summary>
    public void Away()
    {
      AwayInvoke();
    }
    /// <summary>
    /// Ask if the away event has been handled.
    /// </summary>
    /// <returns></returns>
    public bool HasAway()
    {
      return AwayDelegate != null;
    }

    internal override Inv.ControlType ControlType => Inv.ControlType.Button;
    internal Inv.Singleton<Inv.Panel> ContentSingleton { get; private set; }

    internal void SingleTapInvoke()
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      RequireThreadAffinity();

      SingleTapEvent?.Invoke();

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    internal void ContextTapInvoke()
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      RequireThreadAffinity();

      ContextTapEvent?.Invoke();

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    internal void PressInvoke()
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      RequireThreadAffinity();

      this.IsPressed = true;

      PressDelegate?.Invoke();

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    internal void ReleaseInvoke()
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      RequireThreadAffinity();

      this.IsPressed = false;

      ReleaseDelegate?.Invoke();

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    internal void SetPress(Action Delegate)
    {
      this.PressDelegate = Delegate;
    }
    internal void SetRelease(Action Delegate)
    {
      this.ReleaseDelegate = Delegate;
    }
    internal void OverInvoke()
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      RequireThreadAffinity();

      this.IsOver = true;

      OverDelegate?.Invoke();

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    internal void AwayInvoke()
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      RequireThreadAffinity();

      this.IsOver = false;

      AwayDelegate?.Invoke();

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    internal void SetOver(Action Delegate)
    {
      this.OverDelegate = Delegate;
    }
    internal void SetAway(Action Delegate)
    {
      this.AwayDelegate = Delegate;
    }

    private bool IsEnabledField;
    private bool IsFocusableField;
    private string HintField;
    private Action PressDelegate;
    private Action ReleaseDelegate;
    private Action OverDelegate;
    private Action AwayDelegate;

    private static readonly Inv.TraceClass TraceClass = Inv.Trace.NewClass<Inv.Button>(Inv.Trace.PlatformFeature);
  }

  /// <summary>
  /// The behaviour style for a button.
  /// </summary>
  public enum ButtonStyle
  {
    /// <summary>
    /// Stark buttons have no automatic styling when hovered, pushed or disabled.
    /// </summary>
    Stark,
    /// <summary>
    /// Flat buttons have automatic styling when hovered, pushed and disabled.
    /// When hovered, background colour is lightened by 25%.
    /// When pushed, background colour is darkened by 25%.
    /// When disabled, the opacity is halved (ie. 50%).
    /// </summary>
    Flat
  }

  /// <summary>
  /// Labels are plain-text blocks that can be styled with a font.
  /// </summary>
  public sealed class Label : Inv.Control
  {
    /// <summary>
    /// Create a new label.
    /// </summary>
    public Label()
      : this(null)
    {
    }
    internal Label(Inv.Surface Surface)
      : base(Surface)
    {
      this.Font = new Font(this);
      this.LineWrappingField = true;
      this.Justify = new Inv.Justify(this);
      this.Tooltip = new Tooltip(this);
    }

    /// <summary>
    /// Create a new label.
    /// </summary>
    /// <returns></returns>
    public static Inv.Label New() => new Inv.Label();

    /// <summary>
    /// The font used to draw the text.
    /// </summary>
    public Font Font { get; private set; }
    /// <summary>
    /// Text justification.
    /// </summary>
    public Justify Justify { get; private set; }
    /// <summary>
    /// The text to display in the label. Supports unicode and Environment.NewLine characters.
    /// </summary>
    public string Text
    {
      get
      {
        RequireThreadAffinity();

        return TextField;
      }
      set
      {
        RequireThreadAffinity();

        if (TextField != value)
        {
          this.TextField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// Whether the text will wrap to over new lines if there is not enough space, or truncate with an ellipsis.
    /// </summary>
    public bool LineWrapping
    {
      get
      {
        RequireThreadAffinity();

        return LineWrappingField;
      }
      set
      {
        RequireThreadAffinity();

        if (LineWrappingField != value)
        {
          this.LineWrappingField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// The tooltip for the label.
    /// </summary>
    public Inv.Tooltip Tooltip { get; private set; }

    internal override ControlType ControlType => Inv.ControlType.Label;
    internal override string DisplayType => "label:" + Text?.ConvertToCSharpString();

    private string TextField;
    private bool LineWrappingField;
  }

  /// <summary>
  /// Display an image such as PNG. The image is uniformly scaled to fit the layout.
  /// </summary>
  public sealed class Graphic : Inv.Control
  {
    /// <summary>
    /// Create a new graphic.
    /// </summary>
    public Graphic()
      : this(null)
    {
    }
    internal Graphic(Inv.Surface Surface)
      : base(Surface)
    {
      this.ImageSingleton = new Singleton<Inv.Image>(this);
      this.Fit = new Fit(this);
      this.Tooltip = new Tooltip(this);
    }

    /// <summary>
    /// Create a new graphic.
    /// </summary>
    /// <returns></returns>
    public static Inv.Graphic New() => new Inv.Graphic();

    /// <summary>
    /// The method of resizing the graphic to fit the available area.
    /// </summary>
    public Inv.Fit Fit { get; private set; }
    /// <summary>
    /// The tooltip for the graphic.
    /// </summary>
    public Inv.Tooltip Tooltip { get; private set; }
    /// <summary>
    /// The image to be displayed.
    /// </summary>
    public Inv.Image Image
    {
      get => ImageSingleton.Data;
      set => this.ImageSingleton.Data = value;
    }

    internal override ControlType ControlType => Inv.ControlType.Graphic;
    internal override string DisplayType => "graphic" + (Image != null ? ":" + Image.GetBuffer().Length : "");
    internal Singleton<Inv.Image> ImageSingleton { get; private set; }
  }

  /// <summary>
  /// The edit modes for the <see cref="Edit"/>
  /// </summary>
  public enum EditInput
  {
    /// <summary>
    /// Decimal number
    /// </summary>
    Decimal,
    /// <summary>
    /// Email address
    /// </summary>
    Email,
    /// <summary>
    /// Integer number
    /// </summary>
    Integer,
    /// <summary>
    /// Name of person, place or thing
    /// </summary>
    Name,
    /// <summary>
    /// General number
    /// </summary>
    Number,
    /// <summary>
    /// Password
    /// </summary>
    Password,
    /// <summary>
    /// Phone number
    /// </summary>
    Phone,
    /// <summary>
    /// Search terms
    /// </summary>
    Search,
    /// <summary>
    /// General text
    /// </summary>
    Text,
    /// <summary>
    /// Web Uri
    /// </summary>
    Uri,
    /// <summary>
    /// Username
    /// </summary>
    Username
  }

  /// <summary>
  /// Single-line text editor with several input modes.
  /// </summary>
  public sealed class Edit : Inv.Control
  {
    /// <summary>
    /// Create a new edit.
    /// </summary>
    /// <param name="Input"></param>
    public Edit(Inv.EditInput Input)
      : this(null, Input)
    {
    }
    internal Edit(Inv.Surface Surface, Inv.EditInput Input)
      : base(Surface)
    {
      this.Input = Input;
      this.Focus = new Inv.Focus(this);
      this.Font = new Inv.Font(this);
      this.Justify = new Inv.Justify(this);
    }

    /// <summary>
    /// Create a new edit.
    /// </summary>
    /// <param name="Input"></param>
    /// <returns></returns>
    public static Inv.Edit New(EditInput Input) => new Inv.Edit(Input);
    /// <summary>
    /// Create a new Text <see cref="Edit" />
    /// </summary>
    public static Inv.Edit NewText() => New(EditInput.Text);
    /// <summary>
    /// Create a new Number <see cref="Edit" />
    /// </summary>
    public static Inv.Edit NewNumber() => New(EditInput.Number);
    /// <summary>
    /// Create a new Integer <see cref="Edit" />
    /// </summary>
    public static Inv.Edit NewInteger() => New(EditInput.Integer);
    /// <summary>
    /// Create a new Decimal <see cref="Edit" />
    /// </summary>
    public static Inv.Edit NewDecimal() => New(EditInput.Decimal);
    /// <summary>
    /// Create a new Name <see cref="Edit" />
    /// </summary>
    public static Inv.Edit NewName() => New(EditInput.Name);
    /// <summary>
    /// Create a new Email <see cref="Edit" />
    /// </summary>
    public static Inv.Edit NewEmail() => New(EditInput.Email);
    /// <summary>
    /// Create a new Phone <see cref="Edit" />
    /// </summary>
    public static Inv.Edit NewPhone() => New(EditInput.Phone);
    /// <summary>
    /// Create a new Password <see cref="Edit" />
    /// </summary>
    public static Inv.Edit NewPassword() => New(EditInput.Password);
    /// <summary>
    /// Create a new Search <see cref="Edit" />
    /// </summary>
    public static Inv.Edit NewSearch() => New(EditInput.Search);
    /// <summary>
    /// Create a new Uri <see cref="Edit" />
    /// </summary>
    public static Inv.Edit NewUri() => New(EditInput.Uri);
    /// <summary>
    /// Create a new Username <see cref="Edit" />
    /// </summary>
    public static Inv.Edit NewUsername() => New(EditInput.Username);

    /// <summary>
    /// The input mode.
    /// </summary>
    public Inv.EditInput Input { get; private set; }
    /// <summary>
    /// The focus manager for the edit.
    /// </summary>
    public Inv.Focus Focus { get; private set; }
    /// <summary>
    /// The font used to display the text.
    /// </summary>
    public Inv.Font Font { get; private set; }
    /// <summary>
    /// Text justification.
    /// </summary>
    public Inv.Justify Justify { get; private set; }
    /// <summary>
    /// Optional hint text as used in accessibility and tooltips.
    /// </summary>
    public string Hint
    {
      get
      {
        RequireThreadAffinity();

        return HintField;
      }
      set
      {
        RequireThreadAffinity();

        if (HintField != value)
        {
          this.HintField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// When readonly, the edit cannot be changed by the user.
    /// </summary>
    public bool IsReadOnly
    {
      get
      {
        RequireThreadAffinity();

        return IsReadOnlyField;
      }
      set
      {
        RequireThreadAffinity();

        if (IsReadOnlyField != value)
        {
          this.IsReadOnlyField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// The text in the edit box.
    /// </summary>
    public string Text
    {
      get
      {
        RequireThreadAffinity();

        return TextField;
      }
      set
      {
        RequireThreadAffinity();

        if (TextField != value)
        {
          this.TextField = value;
          Change();
          ChangeInvoke();
        }
      }
    }
    /// <summary>
    /// Handle to be notified when the edit text is changed.
    /// </summary>
    public event Action ChangeEvent
    {
      add
      {
        RequireThreadAffinity();
        ChangeDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        ChangeDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Ask if the <see cref="ChangeEvent"/> is handled.
    /// </summary>
    public bool HasChange => ChangeDelegate != null;
    /// <summary>
    /// Handle to be notified when the edit text is accepted (enter key, or soft keyboard return).
    /// </summary>
    public event Action ReturnEvent
    {
      add
      {
        RequireThreadAffinity();
        ReturnDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        this.ReturnDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Ask if the <see cref="ReturnEvent"/> is handled.
    /// </summary>
    public bool HasReturn => ReturnDelegate != null;
    /// <summary>
    /// Handle to be notified when a key press occurs with the edit focused.
    /// </summary>
    public event Action<Inv.Keystroke> KeyPressEvent
    {
      add
      {
        RequireThreadAffinity();
        KeyPressDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        KeyPressDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Ask if the <see cref="KeyPressEvent"/> is handled.
    /// </summary>
    public bool HasKeyPress => KeyPressDelegate != null;

    /// <summary>
    /// Programmatically invoke the return.
    /// </summary>
    public void Return()
    {
      RequireThreadAffinity();

      ReturnDelegate?.Invoke();
    }

    internal override Inv.ControlType ControlType => Inv.ControlType.Edit;
    internal override string DisplayType => "edit:" + Text?.ConvertToCSharpString();

    internal void UpdateText(string Text)
    {
      // NOTE: called only by Invention Platform Server.

      // update the Text, mark as changed, but don't invoke ChangeDelegate.
      RequireThreadAffinity();

      if (TextField != Text)
      {
        this.TextField = Text;
        Change();
      }
    }
    internal void ChangeText(string Text)
    {
      // update the Text, but don't mark as changed, and invoke ChangeDelegate.
      RequireThreadAffinity();

      if (TextField != Text)
      {
        this.TextField = Text;

        ChangeInvoke();
      }
    }
    internal void PressKey(Inv.Keystroke Keystroke)
    {
      KeyPressDelegate?.Invoke(Keystroke);
    }
    internal void SetChange(Action Delegate)
    {
      this.ChangeDelegate = Delegate;
    }
    internal void SetReturn(Action Delegate)
    {
      this.ReturnDelegate = Delegate;
    }

    private void ChangeInvoke()
    {
      ChangeDelegate?.Invoke();
    }

    private string TextField;
    private string HintField;
    private bool IsReadOnlyField;
    private Action ChangeDelegate;
    private Action ReturnDelegate;
    private Action<Inv.Keystroke> KeyPressDelegate;
  }

  /// <summary>
  /// Multi-line text editor with optional markup.
  /// </summary>
  public sealed class Memo : Inv.Control
  {
    /// <summary>
    /// Create a new memo.
    /// </summary>
    public Memo()
      : this(null)
    {
    }
    internal Memo(Inv.Surface Surface)
      : base(Surface)
    {
      this.Focus = new Inv.Focus(this);
      this.Font = new Inv.Font(this);
      this.MarkupCollection = new Collection<Inv.MemoMarkup>(this);
    }

    /// <summary>
    /// Create a new memo.
    /// </summary>
    /// <returns></returns>
    public static Inv.Memo New() => new Inv.Memo();

    /// <summary>
    /// The focus manager for the memo.
    /// </summary>
    public Inv.Focus Focus { get; private set; }
    /// <summary>
    /// Font of the text in the memo.
    /// </summary>
    public Font Font { get; private set; }
    /// <summary>
    /// Text in the memo.
    /// </summary>
    public string Text
    {
      get
      {
        RequireThreadAffinity();

        return TextField;
      }
      set
      {
        RequireThreadAffinity();

        if (TextField != value)
        {
          this.TextField = value;
          Change();
          ChangeInvoke();
        }
      }
    }
    /// <summary>
    /// Whether the user can change the memo.
    /// </summary>
    public bool IsReadOnly
    {
      get
      {
        RequireThreadAffinity();

        return IsReadOnlyField;
      }
      set
      {
        RequireThreadAffinity();

        if (IsReadOnlyField != value)
        {
          this.IsReadOnlyField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// Handle to be notified when the memo text is changed.
    /// </summary>
    public event Action ChangeEvent
    {
      add
      {
        RequireThreadAffinity();
        this.ChangeDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        this.ChangeDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Ask if the memo handles the <see cref="ChangeEvent"/>.
    /// </summary>
    public bool HasChange
    {
      get => ChangeDelegate != null;
    }
    /// <summary>
    /// Remove all markups from the memo.
    /// </summary>
    public void RemoveMarkups()
    {
      RequireThreadAffinity();

      MarkupCollection.Clear();
    }
    /// <summary>
    /// Remove a markup from the memo.
    /// </summary>
    /// <param name="Markup"></param>
    public void RemoveMarkup(Inv.MemoMarkup Markup)
    {
      RequireThreadAffinity();

      MarkupCollection.Remove(Markup);
    }
    /// <summary>
    /// Add a new markup to the memo.
    /// </summary>
    /// <returns></returns>
    public Inv.MemoMarkup AddMarkup()
    {
      RequireThreadAffinity();

      var Result = new MemoMarkup(this);

      MarkupCollection.Add(Result);

      return Result;
    }

    internal override ControlType ControlType => Inv.ControlType.Memo;
    internal override string DisplayType => "memo:" + Text?.ConvertToCSharpString();
    internal Inv.Collection<MemoMarkup> MarkupCollection { get; private set; }

    internal void UpdateText(string Text)
    {
      // NOTE: called only by Invention Platform Server.

      // update the Text, mark as changed, but don't invoke ChangeDelegate.
      RequireThreadAffinity();

      if (TextField != Text)
      {
        this.TextField = Text;
        Change();
      }
    }
    internal void ChangeText(string Text)
    {
      // update the Text, but don't mark as changed, and invoke ChangeDelegate.
      RequireThreadAffinity();

      if (TextField != Text)
      {
        this.TextField = Text;
        ChangeInvoke();
      }
    }
    internal void SetChange(Action Delegate)
    {
      this.ChangeDelegate = Delegate;
    }

    private void ChangeInvoke()
    {
      ChangeDelegate?.Invoke();
    }

    private string TextField;
    private bool IsReadOnlyField;
    private Action ChangeDelegate;
  }

  /// <summary>
  /// Markup allows rich text selections in a memo.
  /// </summary>
  public sealed class MemoMarkup
  {
    internal MemoMarkup(Memo Memo)
    {
      this.Memo = Memo;
      this.Font = new Font(Memo);
      this.RangeList = [];
    }

    /// <summary>
    /// The font of this bit of markup.
    /// </summary>
    public Font Font { get; private set; }

    /// <summary>
    /// Ask if this markup covers a particular index in the memo text.
    /// </summary>
    /// <param name="Index"></param>
    /// <returns></returns>
    public bool InRange(int Index)
    {
      return RangeList.Any(R => Index >= R.Index && Index < R.Index + R.Count);
    }
    /// <summary>
    /// Apply this markup to a range in the memo text.
    /// </summary>
    /// <param name="Index"></param>
    /// <param name="Count"></param>
    public void AddRange(int Index, int Count)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Memo.Text != null, "Text must be specified.");
        Inv.Assert.Check(Index >= 0, "Index must be zero or more: {0}", Index);
        Inv.Assert.Check(Count > 0, "Count must be greater than zero: {0}", Count);
        Inv.Assert.Check(Index + Count <= Memo.Text.Length, "Range must not be more than the length of the text");
      }

      RangeList.Add(new MemoRange(Index, Count));

      Memo.Change();
    }

    internal Inv.DistinctList<MemoRange> RangeList { get; private set; }

    private readonly Memo Memo;
  }

  internal struct MemoRange
  {
    internal MemoRange(int IndexValue, int CountValue)
    {
      this.Index = IndexValue;
      this.Count = CountValue;
    }

    public readonly int Index;
    public readonly int Count;
  }

  /// <summary>
  /// Playback for video such as mp4. The video is uniformly scaled to fit the layout.
  /// </summary>
  public sealed class Video : Inv.Control
  {
    /// <summary>
    /// Create a new video.
    /// </summary>
    public Video()
      : this(null)
    {
    }
    internal Video(Inv.Surface Surface)
      : base(Surface)
    {
      this.SourceSingleton = new Singleton<VideoSource>(this);
      this.StateSingleton = new Singleton<VideoState>(this);
    }

    /// <summary>
    /// Create a new video.
    /// </summary>
    /// <returns></returns>
    public static Inv.Video New() => new Inv.Video();

    /// <summary>
    /// The Uri of the currently loaded video.
    /// </summary>
    public Uri Uri => SourceSingleton.Data?.Uri;
    /// <summary>
    /// The File of the currently loaded video.
    /// </summary>
    public Inv.File File => SourceSingleton.Data?.File;
    /// <summary>
    /// The Asset of the currently loaded video.
    /// </summary>
    public Inv.Asset Asset => SourceSingleton.Data?.Asset;
    /// <summary>
    /// Is the video playing.
    /// </summary>
    public bool IsPlaying
    {
      get => StateSingleton.Data == VideoState.Play;
    }
    /// <summary>
    /// Is the video paused.
    /// </summary>
    public bool IsPaused
    {
      get => StateSingleton.Data == VideoState.Pause;
    }
    /// <summary>
    /// Is the video stopped.
    /// </summary>
    public bool IsStopped
    {
      get => StateSingleton.Data == VideoState.Stop;
    }

    /// <summary>
    /// Set the video source to an asset.
    /// </summary>
    public void SetSourceAsset(Inv.Asset Asset)
    {
      this.SourceSingleton.Data = new VideoSource(Asset);
    }
    /// <summary>
    /// Set the video source to a file.
    /// </summary>
    public void SetSourceFile(Inv.File File)
    {
      this.SourceSingleton.Data = new VideoSource(File);
    }
    /// <summary>
    /// Set the video source to a uri.
    /// </summary>
    public void SetSourceUri(Uri Uri)
    {
      this.SourceSingleton.Data = new VideoSource(Uri);
    }

    /// <summary>
    /// Play the video.
    /// </summary>
    public void Play()
    {
      StateSingleton.Data = VideoState.Play;
    }
    /// <summary>
    /// Pause the playing video.
    /// </summary>
    public void Pause()
    {
      StateSingleton.Data = VideoState.Pause;
    }
    /// <summary>
    /// Stop the video.
    /// </summary>
    public void Stop()
    {
      StateSingleton.Data = VideoState.Stop;
    }
    /// <summary>
    /// Restart the video.
    /// </summary>
    public void Restart()
    {
      StateSingleton.Data = VideoState.Restart;
    }

    internal override ControlType ControlType => Inv.ControlType.Video;
    internal override string DisplayType => "video[" + SourceSingleton.Data + "]";
    internal Inv.Singleton<VideoState> StateSingleton { get; private set; }
    internal Inv.Singleton<VideoSource> SourceSingleton { get; private set; }
  }

  internal sealed class VideoSource
  {
    public VideoSource(Inv.Asset Asset)
    {
      this.Asset = Asset;
    }
    public VideoSource(Inv.File File)
    {
      this.File = File;
    }
    public VideoSource(Uri Uri)
    {
      this.Uri = Uri;
    }

    public Inv.Asset Asset { get; }
    public Inv.File File { get; }
    public Uri Uri { get; }

    public override string ToString()
    {
      if (Asset != null)
        return Asset.Name;
      else if (File != null)
        return File.GetRelativePath().AsSeparatedText("\\");
      else if (Uri != null)
        return Uri.AbsoluteUri;
      else
        return "N/A";
    }
  }

  internal enum VideoState
  {
    Stop,
    Pause,
    Play,
    Restart
  }

  /// <summary>
  /// Switch for boolean toggle state.
  /// </summary>
  public sealed class Switch : Inv.Control
  {
    /// <summary>
    /// Create a new switch.
    /// </summary>
    public Switch()
      : this(null)
    {
    }
    internal Switch(Inv.Surface Surface)
      : base(Surface)
    {
      this.IsEnabled = true;
    }

    /// <summary>
    /// Create a new switch.
    /// </summary>
    /// <returns></returns>
    public static Inv.Switch New() => new Inv.Switch();

    /// <summary>
    /// State of the switch.
    /// </summary>
    public bool IsOn
    {
      get
      {
        RequireThreadAffinity();

        return IsOnField;
      }
      set
      {
        RequireThreadAffinity();

        if (IsOnField != value)
        {
          this.IsOnField = value;
          Change();
          ChangeInvoke();
        }
      }
    }
    /// <summary>
    /// Primary colour of the switch.
    /// </summary>
    public Inv.Colour PrimaryColour
    {
      get
      {
        RequireThreadAffinity();

        return PrimaryColourField;
      }
      set
      {
        RequireThreadAffinity();

        if (PrimaryColourField != value)
        {
          this.PrimaryColourField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// Primary colour of the switch.
    /// </summary>
    public Inv.Colour SecondaryColour
    {
      get
      {
        RequireThreadAffinity();

        return SecondaryColourField;
      }
      set
      {
        RequireThreadAffinity();

        if (SecondaryColourField != value)
        {
          this.SecondaryColourField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// Whether the user can change the switch.
    /// </summary>
    public bool IsEnabled
    {
      get
      {
        RequireThreadAffinity();

        return IsEnabledField;
      }
      set
      {
        RequireThreadAffinity();

        if (IsEnabledField != value)
        {
          this.IsEnabledField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// Handle to be notified when the switch state is changed.
    /// </summary>
    public event Action ChangeEvent
    {
      add
      {
        RequireThreadAffinity();
        this.ChangeDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        this.ChangeDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Ask if the switch handles the <see cref="ChangeEvent"/>.
    /// </summary>
    public bool HasChange
    {
      get => ChangeDelegate != null;
    }

    internal override ControlType ControlType => Inv.ControlType.Switch;
    internal override string DisplayType => "switch:" + IsOnField.ToString();

    internal void UpdateChecked(bool IsChecked)
    {
      RequireThreadAffinity();

      if (IsOnField != IsChecked)
      {
        this.IsOnField = IsChecked;
        ChangeInvoke();
      }
    }
    internal void ChangeChecked(bool IsChecked)
    {
      RequireThreadAffinity();

      if (IsOnField != IsChecked)
      {
        this.IsOnField = IsChecked;
        Change();
        ChangeInvoke();
      }
    }
    internal void SetChange(Action Delegate)
    {
      this.ChangeDelegate = Delegate;
    }

    private void ChangeInvoke()
    {
      ChangeDelegate?.Invoke();
    }

    private bool IsOnField;
    private bool IsEnabledField;
    private Inv.Colour PrimaryColourField;
    private Inv.Colour SecondaryColourField;
    private Action ChangeDelegate;
  }

  /// <summary>
  /// Block of spans of formatted text.
  /// </summary>
  public sealed class Block : Inv.Control
  {
    /// <summary>
    /// Create a new block.
    /// </summary>
    public Block()
      : this(null)
    {
    }
    internal Block(Inv.Surface Surface)
      : base(Surface)
    {
      this.SpanCollection = new Collection<BlockSpan>(this);
      this.LineWrappingField = true;
      this.Font = new Font(this);
      this.Justify = new Justify(this);
      this.Tooltip = new Inv.Tooltip(this);
    }

    /// <summary>
    /// Create a new block.
    /// </summary>
    /// <returns></returns>
    public static Inv.Block New() => new Inv.Block();

    /// <summary>
    /// The font used to draw the text.
    /// </summary>
    public Inv.Font Font { get; private set; }
    /// <summary>
    /// Text justification.
    /// </summary>
    public Justify Justify { get; private set; }
    /// <summary>
    /// Whether the text will wrap to over new lines if there is not enough space, or truncate with an ellipsis.
    /// </summary>
    public bool LineWrapping
    {
      get
      {
        RequireThreadAffinity();

        return LineWrappingField;
      }
      set
      {
        RequireThreadAffinity();

        if (LineWrappingField != value)
        {
          this.LineWrappingField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// The tooltip for the block.
    /// </summary>
    public Inv.Tooltip Tooltip { get; private set; }
    /// <summary>
    /// List each span in the block.
    /// </summary>
    /// <returns></returns>
    public IReadOnlyList<BlockSpan> Spans
    {
      get
      {
        RequireThreadAffinity();
        return SpanCollection;
      }
    }

    /// <summary>
    /// Remove all spans and add a new run with the provided text.
    /// </summary>
    /// <param name="Text"></param>
    public void SetText(string Text)
    {
      RemoveSpans();
      if (!string.IsNullOrEmpty(Text))
        AddRun(Text);
    }
    /// <summary>
    /// Returns true if the block has no spans (ie. no runs and no breaks have been added).
    /// </summary>
    /// <returns></returns>
    public bool IsEmpty() => SpanCollection.Count == 0;
    /// <summary>
    /// Add a line break.
    /// </summary>
    /// <returns></returns>
    public BlockSpan AddBreak()
    {
      return AddSpan(BlockStyle.Break, Text: null);
    }
    /// <summary>
    /// Add a fragment of text.
    /// </summary>
    /// <param name="Text"></param>
    /// <param name="SpanAction"></param>
    /// <returns></returns>
    public BlockSpan AddRun(string Text)
    {
      return AddSpan(BlockStyle.Run, Text ?? string.Empty);
    }
    /// <summary>
    /// Remove a span.
    /// </summary>
    /// <param name="Span"></param>
    public void RemoveSpan(BlockSpan Span)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(SpanCollection.Contains(Span), "The span was not found in this block.");

      SpanCollection.Remove(Span);
    }
    /// <summary>
    /// Remove all spans.
    /// </summary>
    public void RemoveSpans()
    {
      SpanCollection.Clear();
    }

    /// <summary>
    /// Concatenate the spans text into a single string.
    /// </summary>
    /// <returns></returns>
    public string GetSpanText()
    {
      var Result = new StringBuilder();

      var PreviousRun = false;

      foreach (var Span in SpanCollection)
      {
        if (Span.Style == BlockStyle.Run)
        {
          if (PreviousRun)
            Result.Append(" ");

          Result.Append(Span.Text);
          PreviousRun = true;
        }
        else if (Span.Style == BlockStyle.Break)
        {
          Result.AppendLine();
          PreviousRun = false;
        }
      }

      return Result.ToString();
    }

    internal BlockSpan AddSpan(BlockStyle Style, string Text)
    {
      var Result = new BlockSpan(this, Style, Text);
      SpanCollection.Add(Result);
      return Result;
    }

    internal Collection<BlockSpan> SpanCollection { get; private set; }
    internal override Inv.ControlType ControlType => Inv.ControlType.Block;
    internal override string DisplayType => "block:" + GetSpanText().ConvertToCSharpString();

    private bool LineWrappingField;
  }

  /// <summary>
  /// BlockStyle is the type of span.
  /// </summary>
  public enum BlockStyle
  {
    /// <summary>
    /// Fragment of text.
    /// </summary>
    Run,
    /// <summary>
    /// Line break.
    /// </summary>
    Break
  }

  /// <summary>
  /// Span of text in a block.
  /// </summary>
  public sealed class BlockSpan
  {
    internal BlockSpan(Block Block, BlockStyle Style, string Text)
    {
      this.Block = Block;
      this.Style = Style;
      this.TextField = Text;
      this.Background = new Background(Change, Block.Window);
      this.Font = new Font(Change, Block.Window);
    }

    /// <summary>
    /// Run of text or a line break.
    /// </summary>
    public BlockStyle Style { get; }
    /// <summary>
    /// Background highlight of the span.
    /// </summary>
    public Inv.Background Background { get; }
    /// <summary>
    /// Font used for the text in the span.
    /// </summary>
    public Inv.Font Font { get; }
    /// <summary>
    /// Span text, will be null for a line break.
    /// </summary>
    public string Text
    {
      get
      {
        Block.RequireThreadAffinity();

        return TextField;
      }
      set
      {
        Block.RequireThreadAffinity();

        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(Style == BlockStyle.Run || value == null, "Text can only be assigned to a Run.");

        if (TextField != value)
        {
          this.TextField = value;
          Change();
        }
      }
    }

    public override string ToString()
    {
      return $"{Style.ToString().ToLowerInvariant()}:{Text?.ConvertToCSharpString()}";
    }

    internal object Node { get; set; }

    private void Change()
    {
      Block.SpanCollection.Change();
    }

    private readonly Block Block;
    private string TextField;
  }

  /// <summary>
  /// Contains a native control for the platform.
  /// </summary>
  public sealed class Native : Inv.Control
  {
    /// <summary>
    /// Create a new native.
    /// </summary>
    public Native(object Content = null)
      : this(null, Content)
    {
    }
    internal Native(Inv.Surface Surface, object Content)
      : base(Surface)
    {
      this.ContentSingleton = new Singleton<object>(this);

      if (Content != null)
        this.Content = Content;
    }

    /// <summary>
    /// Create a new native panel.
    /// </summary>
    /// <returns></returns>
    public static Inv.Native New(object Content = null) => new Inv.Native(Content);

    /// <summary>
    /// The native control.
    /// </summary>
    public object Content
    {
      get => ContentSingleton.Data;
      set
      {
        // check object against the expected native type registered with the Platform (eg. System.Windows.FrameworkElement).
        if (Inv.Assert.IsEnabled)
        {
          Inv.Assert.Check(Window.NativePanelType != null, "Native panels are not supported by this platform.");

          if (value != null && Window.NativePanelType != null)
            Inv.Assert.Check(value.GetType().GetReflectionInfo().IsSubclassOf(Window.NativePanelType), "Native panel content was {0} but must inherit from {1}.", value.GetType().FullName, Window.NativePanelType.FullName);
        }

        this.ContentSingleton.Data = value;
      }
    }

    internal override Inv.ControlType ControlType => Inv.ControlType.Native;
    internal override string DisplayType => "native" + (Content != null ? ":" + Content.ToString() : "");
    internal Inv.Singleton<object> ContentSingleton { get; private set; }
  }

  /// <summary>
  /// Shape.
  /// </summary>
  public sealed class Shape : Inv.Control
  {
    /// <summary>
    /// Create a new shape.
    /// </summary>
    public Shape()
      : this(null)
    {
    }
    internal Shape(Inv.Surface Surface)
      : base(Surface)
    {
      this.FigureCollection = new Collection<Inv.ShapeFigure>(this);
      this.Stroke = new ShapeStroke(this);
      this.Fill = new ShapeFill(this);
      this.Fit = new Fit(this);
    }

    /// <summary>
    /// Create a new shape panel.
    /// </summary>
    /// <returns></returns>
    public static Inv.Shape New() => new Inv.Shape();

    /// <summary>
    /// The outline of the shape.
    /// </summary>
    public Inv.ShapeStroke Stroke { get; private set; }
    /// <summary>
    /// The inner fill colour of the shape.
    /// </summary>
    public Inv.ShapeFill Fill { get; private set; }
    /// <summary>
    /// The method of resizing the shape to fit the available area.
    /// </summary>
    public Inv.Fit Fit { get; private set; }

    /// <summary>
    /// Polygon figure of the connected points.
    /// </summary>
    /// <param name="PathArray"></param>
    public ShapeFigure Polygon(params Inv.Point[] PathArray)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(PathArray.All(P => P.X >= 0 && P.Y >= 0), "Points must not have negative values for X or Y.");

      return AddFigure(new ShapeFigure(new ShapePolygon(PathArray)));
    }
    /// <summary>
    /// Triangle figure as defined by the three points.
    /// </summary>
    /// <param name="A"></param>
    /// <param name="B"></param>
    /// <param name="C"></param>
    public ShapeFigure Triangle(Inv.Point A, Inv.Point B, Inv.Point C)
    {
      return Polygon(A, B, C, A);
    }
    /// <summary>
    /// Rectangle shape as defined by the boundary.
    /// </summary>
    /// <param name="Rect"></param>
    public ShapeFigure Rectangle(Inv.Rect Rect)
    {
      return Polygon(Rect.TopLeft(), Rect.TopRight(), Rect.BottomRight(), Rect.BottomLeft(), Rect.TopLeft());
    }
    /// <summary>
    /// Line figure as defined by two points.
    /// </summary>
    /// <param name="Start"></param>
    /// <param name="End"></param>
    public ShapeFigure Line(Inv.Point Start, Inv.Point End)
    {
      return AddFigure(new ShapeFigure(new ShapeLine(Start, End)));
    }
    /// <summary>
    /// Ellipse figure as defined by two points.
    /// </summary>
    /// <param name="Center"></param>
    /// <param name="Radius"></param>
    public ShapeFigure Ellipse(Inv.Point Center, Inv.Point Radius)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Radius.X >= 0 && Radius.Y >= 0, "Radius must not have negative values for X or Y.");

      return AddFigure(new ShapeFigure(new ShapeEllipse(Center, Radius)));
    }
    /// <summary>
    /// Remove the specified figure from the shape.
    /// </summary>
    /// <param name="Figure"></param>
    public void RemoveFigure(ShapeFigure Figure)
    {
      FigureCollection.Remove(Figure);
    }
    /// <summary>
    /// Remove all figures from the shape.
    /// </summary>
    public void RemoveFigures()
    {
      FigureCollection.Clear();
    }

    internal override Inv.ControlType ControlType => Inv.ControlType.Shape;
    internal override string DisplayType => "shape";
    internal Collection<ShapeFigure> FigureCollection { get; private set; }

    internal Inv.Dimension GetNotionalDimension()
    {
      var NotionalWidth = 0;
      var NotionalHeight = 0;

      void CheckPoint(Inv.Point Point)
      {
        if (Point.X > NotionalWidth)
          NotionalWidth = Point.X;

        if (Point.Y > NotionalHeight)
          NotionalHeight = Point.Y;
      }

      foreach (var Figure in FigureCollection)
      {
        if (Figure.Polygon != null)
        {
          foreach (var Path in Figure.Polygon.PathArray)
            CheckPoint(Path);
        }

        if (Figure.Line != null)
        {
          CheckPoint(Figure.Line.Start);
          CheckPoint(Figure.Line.End);
        }

        if (Figure.Ellipse != null)
        {
          CheckPoint(Figure.Ellipse.Center - Figure.Ellipse.Radius);
          CheckPoint(Figure.Ellipse.Center + Figure.Ellipse.Radius);
        }
      }

      return new Inv.Dimension(NotionalWidth, NotionalHeight);
    }
    internal ShapeFigure AddFigure(ShapeFigure Figure)
    {
      FigureCollection.Add(Figure);
      return Figure;
    }
  }

  public sealed class ShapeFigure
  {
    internal ShapeFigure(ShapePolygon Polygon)
    {
      this.Polygon = Polygon;
    }
    internal ShapeFigure(ShapeEllipse Ellipse)
    {
      this.Ellipse = Ellipse;
    }
    internal ShapeFigure(ShapeLine Line)
    {
      this.Line = Line;
    }

    internal ShapePolygon Polygon { get; }
    internal ShapeEllipse Ellipse { get; }
    internal ShapeLine Line { get; }
  }

  internal sealed class ShapePolygon
  {
    internal ShapePolygon(Inv.Point[] PointArray)
    {
      this.PathArray = PointArray;
    }

    public Inv.Point[] PathArray { get; }
  }

  internal sealed class ShapeEllipse
  {
    internal ShapeEllipse(Inv.Point Center, Inv.Point Radius)
    {
      this.Center = Center;
      this.Radius = Radius;
    }

    public Inv.Point Center { get; }
    public Inv.Point Radius { get; }
  }

  internal sealed class ShapeLine
  {
    internal ShapeLine(Inv.Point Start, Inv.Point End)
    {
      this.Start = Start;
      this.End = End;
    }

    public Inv.Point Start { get; }
    public Inv.Point End { get; }
  }

  /// <summary>
  /// This template class should be subclassed to declare custom controls.
  /// The generic parameter is the type of the base panel in your custom control.
  /// It is a terse syntax and implements the Panel interface contract.
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class Panel<T> : Inv.Panel
    where T : class, Inv.Panel
  {
    public static explicit operator T(Panel<T> Self) => Self.Base;

    Control Panel.Control => Base.Control;

    /// <summary>
    /// In your constructor, set this Base field with the base panel of your custom control.
    /// </summary>
    protected T Base
    {
      get
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(BaseField != null, "Base has not been set.");

        return BaseField;
      }
      set
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(BaseField == null, "Base must only be set once.");

        this.BaseField = value;
      }
    }

    private T BaseField;
  }
}