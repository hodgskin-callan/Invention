﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using Inv.Support;

namespace Inv
{
  public sealed class Journal
  {
    internal Journal(Inv.Application Application)
    {
      this.Application = Application;
      this.RenderList = new Inv.DistinctList<JournalRender>();
    }

    public JournalEpisode NewEpisode(string Title)
    {
      return new JournalEpisode(this, Title);
    }

    internal Inv.Application Application { get; }

    internal void Render(JournalEpisode Episode, DateTimeOffset Timestamp)
    {
      bool Post;
      lock (RenderList)
      {
        Post = !RenderList.Any(R => R.Episode == Episode);

        if (Post)
        {
          FirstFrame = true;

          // conclude the episode after the next rendering pipeline has measured and arranged the layout.
          if (RenderList.Count == 0)
            Application.Window.ProcessEvent += PresentationProcess;

          RenderList.Add(new JournalRender(Episode, Timestamp));
        }
      }
    }

    private bool FirstFrame;

    private void PresentationProcess()
    {
      lock (RenderList)
      {
        if (FirstFrame)
        {
          FirstFrame = false;
        }
        else
        {
          if (RenderList.Count > 0)
          {
            var Rendered = DateTimeOffset.Now;

            foreach (var RenderEpisode in RenderList)
            {
              RenderEpisode.Episode.Write(JournalAction.Presentation, Rendered - RenderEpisode.Timestamp, Array.Empty<JournalTrack>());

              RenderEpisode.Episode.Conclude();
            }

            RenderList.Clear();

            // no longer need to be in the rendering pipeline.
            Application.Window.ProcessEvent -= PresentationProcess;
          }
        }
      }
    }

    private readonly Inv.DistinctList<JournalRender> RenderList;

    private struct JournalRender
    {
      public JournalRender(JournalEpisode Episode, DateTimeOffset Timestamp)
      {
        this.Episode = Episode;
        this.Timestamp = Timestamp;
      }

      public JournalEpisode Episode { get; }
      public DateTimeOffset Timestamp { get; }
    }
  }

  public enum JournalAction
  {
    Background,
    Foreground,
    Presentation
  }

  public sealed class JournalEpisode
  {
    internal JournalEpisode(Journal Journal, string Title)
    {
      this.Journal = Journal;
      this.Title = Title;
      this.StartTimestamp = DateTimeOffset.Now;
      this.EntryList = new Inv.DistinctList<JournalEntry>();
    }

    public Journal Journal { get; }
    public string Title { get; set; }
    public DateTimeOffset StartTimestamp { get; }
    public DateTimeOffset FinishTimestamp { get; private set; }
    public TimeSpan Duration => FinishTimestamp - StartTimestamp;
    public IReadOnlyList<JournalEntry> Entries => EntryList;
    public event Action ConcludeEvent;
    // TODO: unaccounted duration?
    // TODO: detect overlapping captures.

    /// <summary>
    /// Capture everything that happens in the current thread.
    /// </summary>
    /// <returns></returns>
    public JournalCapture Capture()
    {
      var CaptureTimestamp = DateTimeOffset.Now;

      return new JournalCapture(this, CaptureTimestamp);
    }
    public Inv.DataSize TotalSentSize()
    {
      return EntryList.Sum(E => E.Tracks.Sum(T => T.SentSize)); 
    }
    public Inv.DataSize TotalReceivedSize()
    {
      return EntryList.Sum(E => E.Tracks.Sum(T => T.ReceivedSize));
    }

    internal void Conclude()
    {
      this.FinishTimestamp = DateTimeOffset.Now;

      ConcludeEvent?.Invoke();
    }
    internal void Write(JournalAction Action, TimeSpan Duration, JournalTrack[] TrackArray)
    {
      EntryList.Add(new JournalEntry(Action, Duration, TrackArray));
    }

    private readonly Inv.DistinctList<JournalEntry> EntryList;
  }

  public sealed class JournalCapture : IDisposable
  {
    internal JournalCapture(JournalEpisode Episode, DateTimeOffset Timestamp)
    {
      this.Timestamp = Timestamp;
      this.Episode = Episode;
      this.TrackList = new Inv.DistinctList<JournalTrack>();
      this.IsForeground = Episode.Journal.Application.IsThreadAffinity();

      JournalGovernor.Recruit(this);
    }
    public void Dispose()
    {
      JournalGovernor.Dismiss(this);

      var Finished = DateTimeOffset.Now;

      if (IsForeground)
      {
        Episode.Write(JournalAction.Foreground, Finished - Timestamp, TrackList.ToArray());

        // we expect the presentation to run next.
        Episode.Journal.Render(Episode, Finished);
      }
      else
      {
        Episode.Write(JournalAction.Background, Finished - Timestamp, TrackList.ToArray());

        // we expect a foreground capture next.
      }
    }

    public readonly JournalEpisode Episode;

    public void Track(string Method, long ElapsedMS, long SentBytes, long ReceivedBytes)
    {
      TrackList.Add(new JournalTrack(Method, TimeSpan.FromMilliseconds(ElapsedMS), Inv.DataSize.FromBytes(SentBytes), Inv.DataSize.FromBytes(ReceivedBytes)));
    }

    internal readonly Inv.DistinctList<JournalTrack> TrackList;

    private readonly bool IsForeground;
    private readonly DateTimeOffset Timestamp;
  }

  public sealed class JournalTrack
  {
    internal JournalTrack(string Method, TimeSpan Duration, Inv.DataSize SentSize, Inv.DataSize ReceivedSize)
    {
      this.Method = Method;
      this.Duration = Duration;
      this.SentSize = SentSize;
      this.ReceivedSize = ReceivedSize;
    }

    public string Method { get; }
    public TimeSpan Duration { get; }
    public Inv.DataSize SentSize { get; }
    public Inv.DataSize ReceivedSize { get; }
  }

  public sealed class JournalEntry
  {
    internal JournalEntry(JournalAction Action, TimeSpan Duration, JournalTrack[] TrackArray)
    {
      this.Action = Action;
      this.Duration = Duration;
      this.TrackArray = TrackArray;
    }

    public JournalAction Action { get; }
    public TimeSpan Duration { get; }
    public IReadOnlyList<JournalTrack> Tracks => TrackArray;

    public Inv.DataSize TotalSentSize()
    {
      return TrackArray.Sum(T => T.SentSize);
    }
    public Inv.DataSize TotalReceivedSize()
    {
      return TrackArray.Sum(T => T.ReceivedSize);
    }

    private readonly JournalTrack[] TrackArray;
  }

  public sealed class JournalPanel : Inv.Panel<Inv.Scroll>
  {
    public JournalPanel()
    {
      this.Base = Inv.Scroll.NewVertical();

      this.Stack = Inv.Stack.NewVertical();
      Base.Content = Stack;
      Stack.Border.Set(2, 2, 2, 0).In(Inv.Colour.White);
      Stack.Corner.Set(10, 10, 0, 0);
      Stack.Background.In(Inv.Colour.Black);

      this.CacheTileList = new Inv.DistinctList<JournalTile>();
    }

    public event Action ExpireEvent;

    public void Refresh(JournalEpisode Episode)
    {
      var Tile = CacheTileList.RemoveLastOrNull() ?? new JournalTile();
      Stack.AddPanel(Tile);
      Tile.ExpireEvent += () =>
      {
        if (!CacheTileList.Contains(Tile))
        {
          CacheTileList.Add(Tile);
          Stack.RemovePanel(Tile);

          if (Stack.Panels.Count == 0)
            ExpireEvent?.Invoke();
        }
      };

      Tile.Refresh(Episode);
    }

    private readonly Inv.Stack Stack;
    private readonly Inv.DistinctList<JournalTile> CacheTileList;

    private static readonly Inv.Colour ErrorColour = Inv.Colour.Red.Lighten(0.20F);
    private static readonly Inv.Colour WarningColour = Inv.Colour.HotPink;
    private static readonly Inv.Colour InformationColour = Inv.Colour.Yellow;

    private sealed class JournalTile : Inv.Panel<Inv.Button>
    {
      public JournalTile()
      {
        this.Base = Inv.Button.NewFlat();
        Base.Background.In(Inv.Colour.Black);
        Base.Border.Set(0, 0, 0, 1).In(Inv.Colour.DimGray);

        var Stack = Inv.Stack.NewVertical();
        Base.Content = Stack;
        Stack.Padding.Set(10);

        this.Label = Inv.Label.New();
        Stack.AddPanel(Label);
        Label.Justify.Right();
        Label.Font.Monospaced().ExtraLarge().Bold();

        this.Block = Inv.Block.New();
        Stack.AddPanel(Block);
        Block.Visibility.Collapse();
        Block.Font.Monospaced().Large().In(Inv.Colour.White);

        this.Timer = Inv.WindowTimer.New();
        Timer.IntervalTime = TimeSpan.FromSeconds(5);
        Timer.IntervalEvent += () =>
        {
          Timer.Stop();
          ExpireEvent?.Invoke();
        };

        Base.SingleTapEvent += () =>
        {
          Block.Visibility.Toggle();
          Label.Visibility.Set(!Block.Visibility.Get());

          if (Block.Visibility.Get())
            Timer.Stop();
          else
            Timer.Restart();
        };
      }

      public event Action ExpireEvent;

      public void Refresh(JournalEpisode Episode)
      {
        var NonBackgroundTracks = Episode.Entries.Any(E => E.Action != JournalAction.Background && E.Tracks.Count > 0);
        var MultipleBackgroundTracks = Episode.Entries.Where(E => E.Action == JournalAction.Background).Sum(E => E.Tracks.Count) >= 2;
        var TitleColour = NonBackgroundTracks ? ErrorColour : MultipleBackgroundTracks ? WarningColour : InformationColour;

        var TotalSent = Episode.TotalSentSize();
        var TotalReceived = Episode.TotalReceivedSize();

        Label.Font.In(TitleColour);
        Label.Text = $"{Episode.Duration.TotalMilliseconds:N1} ms";

        if (TotalSent.TotalBytes > 0 || TotalReceived.TotalBytes > 0)
          Label.Text += $" ↑{TotalSent} ↓{TotalReceived}";

        Block.RemoveSpans();

        Block.AddRun($"{Episode.Duration.TotalMilliseconds,10:N1} ").Font.In(TitleColour).Bold();
        Block.AddRun($"{(Episode.Title ?? "<Unspecified Workflow>").ToUpper()}");

        foreach (var Entry in Episode.Entries)
        {
          var TotalMS = Entry.Duration.TotalMilliseconds;

          Block.AddBreak();
          Block.AddRun($"{TotalMS,10:N1} ").Font.Bold();
          Block.AddRun($"{Entry.Action.ToString().ToUpper()} ");

          var NonBackgroundTrack = Entry.Action != JournalAction.Background;
          var MultipleTracks = Entry.Tracks.Count >= 2;

          foreach (var Track in Entry.Tracks)
          {
            var Percent = TotalMS > 0 ? Track.Duration.TotalMilliseconds / TotalMS : 0;

            Block.AddBreak();
            Block.AddRun($"{Percent,14:P0} {Track.Method} ").Font.In(NonBackgroundTrack ? ErrorColour : MultipleTracks ? WarningColour : InformationColour);
            Block.AddRun($"↑{Track.SentSize} ↓{Track.ReceivedSize}").Font.In(Inv.Colour.LightGray);
          }
        }

        if (!Block.Visibility.Get())
          Timer.Restart();
      }

      private readonly Inv.Label Label;
      private readonly Inv.Block Block;
      private readonly Inv.WindowTimer Timer;
    }
  }

  public static class JournalGovernor
  {
    static JournalGovernor()
    {
      CaptureLocal = new System.Threading.ThreadLocal<Inv.DistinctList<JournalCapture>>(() => new Inv.DistinctList<JournalCapture>());
    }

    public static JournalCapture ActiveCapture() => CaptureLocal.Value?.LastOrDefault();

    internal static void Recruit(JournalCapture Capture)
    {
      Debug.Assert(!CaptureLocal.Value.Contains(Capture));

      CaptureLocal.Value.Add(Capture);
    }
    internal static void Dismiss(JournalCapture Capture)
    {
      Debug.Assert(CaptureLocal.Value.LastOrDefault() == Capture);

      CaptureLocal.Value.Remove(Capture);
    }

    private static readonly System.Threading.ThreadLocal<Inv.DistinctList<JournalCapture>> CaptureLocal;
  }
}
