﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Clipboard"/>
  /// </summary>
  public sealed class Clipboard
  {
    internal Clipboard(Application Application)
    {
      this.Application = Application;
    }

    /// <summary>
    /// All platforms support text on the clipboard.
    /// </summary>
    public bool IsTextSupported => Application.Platform.ClipboardIsTextSupported;
    /// <summary>
    /// Read and write unicode text from the shared clipboard.
    /// </summary>
    public string Text
    {
      get => Application.Platform.ClipboardGetText();
      set => Application.Platform.ClipboardSetText(value);
    }
    /// <summary>
    /// Android is the only platform that does not support images on the clipboard.
    /// </summary>
    public bool IsImageSupported => Application.Platform.ClipboardIsImageSupported;
    /// <summary>
    /// Read and write images from the shared clipboard.
    /// </summary>
    public Inv.Image Image
    {
      get => Application.Platform.ClipboardGetImage();
      set => Application.Platform.ClipboardSetImage(value);
    }

    private readonly Application Application;
  }
}
