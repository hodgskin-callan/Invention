﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Device"/>.
  /// </summary>
  public sealed class Device
  {
    internal Device(Application Application)
    {
      this.Application = Application;
      this.ControllerList = new Inv.DistinctList<Controller>();
    }

    /// <summary>
    /// Target platform: Android, iOS, Windows Desktop and Universal Windows.
    /// </summary>
    public DeviceTarget Target { get; internal set; }
    /// <summary>
    /// Is this a mobile device platform.
    /// </summary>
    public bool IsMobile => Target == DeviceTarget.IOS || Target == DeviceTarget.Android;
    /// <summary>
    /// Is this a Windows platform.
    /// </summary>
    public bool IsWindows => Target == DeviceTarget.UniversalWindows || Target == DeviceTarget.WindowsDesktop;
    /// <summary>
    /// Is this a Windows Desktop (PC) platform.
    /// </summary>
    public bool IsWindowsDesktop => Target == DeviceTarget.WindowsDesktop;
    /// <summary>
    /// Is this a Windows Desktop (PC) platform.
    /// </summary>
    public bool IsUniversalWindows => Target == DeviceTarget.UniversalWindows;
    /// <summary>
    /// Is this a Android platform.
    /// </summary>
    public bool IsAndroid => Target == DeviceTarget.Android;
    /// <summary>
    /// Is this an iOS platform.
    /// </summary>
    public bool IsIOS => Target == DeviceTarget.IOS;
    /// <summary>
    /// Is this a Blazor WebAssembly platform.
    /// </summary>
    public bool IsBlazorWebAssembly => Target == DeviceTarget.BlazorWebAssembly;
    /// <summary>
    /// Name of the device.
    /// </summary>
    public string Name { get; internal set; }
    /// <summary>
    /// Manufacturer of the device.
    /// </summary>
    public string Manufacturer { get; internal set; }
    /// <summary>
    /// Model of the device.
    /// </summary>
    public string Model { get; internal set; }
    /// <summary>
    /// Operating system running the device.
    /// </summary>
    public string System { get; internal set; }
    /// <summary>
    /// Does the device have a physical keyboard.
    /// </summary>
    public bool Keyboard { get; internal set; }
    /// <summary>
    /// Does the device have a mouse.
    /// </summary>
    public bool Mouse { get; internal set; }
    /// <summary>
    /// Does the device have a touch screen.
    /// </summary>
    public bool Touch { get; internal set; }
    /// <summary>
    /// The platform-specific default proportional font name.
    /// </summary>
    public string ProportionalFontName { get; internal set; }
    /// <summary>
    /// The platform-specific default monospaced font name.
    /// </summary>
    public string MonospacedFontName { get; internal set; }
    /// <summary>
    /// Pixel density of the device screen.
    /// eg. 2.0F means 2px to a 1pt
    /// </summary>
    public float PixelDensity { get; internal set; }
    /// <summary>
    /// Default theme for this device: Light or Dark.
    /// </summary>
    public DeviceTheme Theme { get; internal set; }
    /// <summary>
    /// Dark theme is currently active.
    /// </summary>
    public bool IsDarkTheme => Theme == DeviceTheme.Dark;
    /// <summary>
    /// Light theme is currently active.
    /// </summary>
    public bool IsLightTheme => Theme == DeviceTheme.Light;
    public event Action<Controller> AddControllerEvent;
    public event Action<Controller> RemoveControllerEvent;

    public IEnumerable<Controller> GetControllers()
    {
      Application.RequireThreadAffinity();

      return ControllerList;
    }
    public Controller GetController(int Index)
    {
      return Index >= 0 && Index < ControllerList.Count ? ControllerList[Index] : null;
    }

    internal Application Application { get; }

    internal Controller FindController(object Node)
    {
      Application.RequireThreadAffinity();

      return ControllerList.Find(C => C.Contract.Node == Node);
    }
    internal void AddController(Controller Controller)
    {
      Application.RequireThreadAffinity();

      ControllerList.Add(Controller);

      AddControllerEvent?.Invoke(Controller);
    }
    internal void RemoveController(Controller Controller)
    {
      Application.RequireThreadAffinity();

      if (!ControllerList.Remove(Controller))
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Fail("Controller does not exist in the managed list");
      }

      RemoveControllerEvent?.Invoke(Controller);
    }

    private readonly Inv.DistinctList<Controller> ControllerList;
  }

  /// <summary>
  /// The setting of the device's default theme colours (light or dark)
  /// </summary>
  public enum DeviceTheme
  {
    /// <summary>
    /// Light background and dark text colours.
    /// </summary>
    Light,
    /// <summary>
    /// Dark background and light text colours;
    /// </summary>
    Dark
  }

  /// <summary>
  /// Enumeration of device platforms.
  /// </summary>
  public enum DeviceTarget
  {
    /// <summary>
    /// Android
    /// </summary>
    Android,
    /// <summary>
    /// iOS
    /// </summary>
    IOS,
    /// <summary>
    /// Universal Windows
    /// </summary>
    UniversalWindows,
    /// <summary>
    /// Windows Desktop
    /// </summary>
    WindowsDesktop,
    /// <summary>
    /// Blazor WebAssembly
    /// </summary>
    BlazorWebAssembly
  }
}
