﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Audio"/>
  /// </summary>
  public sealed class Audio
  {
    internal Audio(Application Application)
    {
      this.Application = Application;
      this.IsRateControlled = true;
      this.IsVolumeControlled = true;
      this.IsPanControlled = true;
    }

    /// <summary>
    /// All sounds are muted through this API.
    /// </summary>
    public bool IsMuted { get; private set; }
    /// <summary>
    /// Volume variations are prevented through this API, always playback at full volume.
    /// </summary>
    public bool IsVolumeControlled { get; private set; }
    /// <summary>
    /// Rate variations are prevented through this API, always playback at regular rate.
    /// </summary>
    public bool IsRateControlled { get; private set; }
    /// <summary>
    /// Pan variations are prevented through this API, always playback with a balanced pan.
    /// </summary>
    public bool IsPanControlled { get; private set; }

    /// <summary>
    /// Minimum allowed playback volume (silent).
    /// </summary>
    public readonly float ZeroVolume = _ZeroVolume;
    /// <summary>
    /// Maximum allowed playback volume (loudest).
    /// </summary>
    public readonly float FullVolume = _FullVolume;
    /// <summary>
    /// Minimum allowed playback rate (slowest speed).
    /// </summary>
    public readonly float SlowestRate = _SlowestRate;
    /// <summary>
    /// Maximum allowed playback rate (fastest speed).
    /// </summary>
    public readonly float FastestRate = _FastestRate;
    /// <summary>
    /// Regular playback rate (normal speed).
    /// </summary>
    public readonly float RegularRate = _RegularRate;
    /// <summary>
    /// Minimum playback pan (left only).
    /// </summary>
    public readonly float LeftmostPan = _LeftmostPan;
    /// <summary>
    /// Maximum playback pan (right only).
    /// </summary>
    public readonly float RightmostPan = _RightmostPan;
    /// <summary>
    /// Balanced playback pan (equal on left and right).
    /// </summary>
    public readonly float BalancedPan = _BalancedPan;

    /// <summary>
    /// Global mute for sounds played through this API.
    /// </summary>
    /// <param name="IsMuted"></param>
    public void SetMuted(bool IsMuted)
    {
      this.IsMuted = IsMuted;
    }
    /// <summary>
    /// Global control to prevent volume variations for sounds played through this API.
    /// </summary>
    /// <param name="IsVolumeControlled"></param>
    public void SetVolumeControl(bool IsVolumeControlled)
    {
      this.IsVolumeControlled = IsVolumeControlled;
    }
    /// <summary>
    /// Global control to prevent rate variations for sounds played through this API.
    /// </summary>
    /// <param name="IsRateControlled"></param>
    public void SetRateControl(bool IsRateControlled)
    {
      this.IsRateControlled = IsRateControlled;
    }
    /// <summary>
    /// Global control to prevent pan variations for sounds played through this API.
    /// </summary>
    /// <param name="IsPanControlled"></param>
    public void SetPanControl(bool IsPanControlled)
    {
      this.IsPanControlled = IsPanControlled;
    }
    /// <summary>
    /// Play a mp3 track using the platform's audio subsystem.
    /// The sound will be played once and to the end of the track.
    /// </summary>
    /// <param name="Sound">The mp3 track held in memory</param>
    /// <param name="Volume">The volume level between 0.0F and 1.0F</param>
    /// <param name="Rate">The rate variation between 0.5F and 2.0F</param>
    /// <param name="Pan">The balance of left and right speakers between -1.0F and 1.0F</param>
    public void Play(Inv.Sound Sound, float Volume = _FullVolume, float Rate = _RegularRate, float Pan = _BalancedPan)
    {
      if (!IsMuted)
        Application.Platform.AudioPlaySound(Sound, ControlVolume(Volume), ControlRate(Rate), ControlPan(Pan));
    }
    /// <summary>
    /// Create a clip which can be played and optionally stopped before it is finished.
    /// </summary>
    /// <param name="Sound">The mp3 track held in memory</param>
    /// <param name="Volume">The volume adjustment between 0.0 and 1.0</param>
    /// <param name="Rate">The rate variation between 0.5 and 2.0</param>
    /// <param name="Pan">The balance of left and right speakers between -1.0 and 1.0</param>
    /// <param name="Looped">Loop the playing of this sound until it is explicitly stopped</param>
    /// <returns></returns>
    public AudioClip NewClip(Inv.Sound Sound, float Volume = _FullVolume, float Rate = _RegularRate, float Pan = _BalancedPan, bool Looped = false)
    {
      return new AudioClip(this, Sound, ControlVolume(Volume), ControlRate(Rate), ControlPan(Pan), Looped);
    }
    /// <summary>
    /// Get the play duration of the sound as a TimeSpan (to the millisecond accuracy).
    /// </summary>
    /// <param name="Sound"></param>
    /// <returns></returns>
    public TimeSpan GetLength(Inv.Sound Sound)
    {
      return Application.Platform.AudioGetSoundLength(Sound);
    }
    /// <summary>
    /// Free up the platform-specific resource that represents the sounds.
    /// </summary>
    /// <param name="SoundList"></param>
    public void Reclaim(IReadOnlyList<Inv.Sound> SoundList)
    {
      Application.RequireThreadAffinity();

      if (SoundList.Count > 0)
        Application.Platform.AudioReclaim(SoundList);
    }
    /// <summary>
    /// Use the device microphone to transcribe speech to text.
    /// </summary>
    /// <returns></returns>
    public SpeechRecognition NewSpeechRecognition()
    {
      return new SpeechRecognition(this);
    }

    internal Application Application { get; }

    internal float ControlVolume(float Volume)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Volume >= ZeroVolume && Volume <= FullVolume, $"Volume must be between {ZeroVolume} and {FullVolume}.");

      return IsVolumeControlled ? Volume : FullVolume;
    }
    internal float ControlRate(float Rate)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Rate >= SlowestRate && Rate <= FastestRate, $"Rate must be between {SlowestRate} and {FastestRate}.");

      return IsRateControlled ? Rate : RegularRate;
    }
    internal float ControlPan(float Pan)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Pan >= LeftmostPan && Pan <= RightmostPan, $"Pan must be between {LeftmostPan} and {RightmostPan}.");

      return IsPanControlled ? Pan : BalancedPan;
    }

    private const float _ZeroVolume = 0.0F;
    private const float _FullVolume = 1.0F;
    private const float _SlowestRate = 0.5F;
    private const float _FastestRate = 2.0F;
    private const float _RegularRate = 1.0F;
    private const float _LeftmostPan = -1.0F;
    private const float _RightmostPan = +1.0F;
    private const float _BalancedPan = 0.0F;
  }

  /// <summary>
  /// Control the playback of a sound track.
  /// </summary>
  public sealed class AudioClip
  {
    internal AudioClip(Audio Audio, Inv.Sound Sound, float Volume, float Rate, float Pan, bool Looped)
    {
      this.Audio = Audio;
      this.Sound = Sound;
      this.Volume = Volume;
      this.Rate = Rate;
      this.Pan = Pan;
      this.Looped = Looped;
    }

    /// <summary>
    /// The sound to be played.
    /// </summary>
    public Sound Sound { get; private set; }
    /// <summary>
    /// The volume level to play the sound between 0.0F and 1.0F.
    /// </summary>
    public float Volume { get; private set; }
    /// <summary>
    /// The rate to play the sound between 0.5F and 2.0F.
    /// </summary>
    public float Pan { get; private set; }
    /// <summary>
    /// The pan to play the sound between -1.0F and +1.0F.
    /// -1 is entirely the left speaker.
    /// +1 is entirely the right speaker.
    /// </summary>
    public float Rate { get; private set; }
    /// <summary>
    /// Whether to continuously loop the track until it is manually stopped.
    /// </summary>
    public bool Looped { get; private set; }
    /// <summary>
    /// Audio clip is actively playing.
    /// </summary>
    public bool IsPlaying { get; private set; }

    /// <summary>
    /// Start playing the clip.
    /// </summary>
    public void Play()
    {
      Audio.Application.RequireThreadAffinity();

      if (!Audio.IsMuted)
      {
        this.IsPlaying = true;

        Audio.Application.Platform.AudioPlayClip(this);
      }
    }
    /// <summary>
    /// Stop playing the clip.
    /// </summary>
    public void Stop()
    {
      Audio.Application.RequireThreadAffinity();

      this.IsPlaying = false;

      Audio.Application.Platform.AudioStopClip(this);
    }
    /// <summary>
    /// Interrupt the playback.
    /// </summary>
    public void Pause()
    {
      Audio.Application.RequireThreadAffinity();

      Audio.Application.Platform.AudioPauseClip(this);
    }
    /// <summary>
    /// Continue after pausing the playback.
    /// </summary>
    public void Resume()
    {
      Audio.Application.RequireThreadAffinity();

      Audio.Application.Platform.AudioResumeClip(this);
    }
    /// <summary>
    /// Get the play duration of the clip as a TimeSpan (to the millisecond accuracy).
    /// <returns></returns>
    /// </summary>
    public TimeSpan GetLength()
    {
      Audio.Application.RequireThreadAffinity();

      return Audio.Application.Platform.AudioGetSoundLength(Sound);
    }
    /// <summary>
    /// Adjust the volume of the clip, before, during or after playback.
    /// </summary>
    /// <param name="Volume"></param>
    public void SetVolume(float Volume)
    {
      Audio.Application.RequireThreadAffinity();

      var Adjustment = Audio.ControlVolume(Volume);
      if (this.Volume != Adjustment)
      {
        this.Volume = Adjustment;
        Audio.Application.Platform.AudioModulateClip(this);
      }
    }
    /// <summary>
    /// Adjust the rate of the clip, before, during or after playback.
    /// </summary>
    /// <param name="Rate"></param>
    public void SetRate(float Rate)
    {
      Audio.Application.RequireThreadAffinity();

      var Adjustment = Audio.ControlRate(Rate);
      if (this.Rate != Adjustment)
      {
        this.Rate = Adjustment;
        Audio.Application.Platform.AudioModulateClip(this);
      }
    }
    /// <summary>
    /// Adjust the pan of the clip, before, during or after playback.
    /// </summary>
    /// <param name="Pan"></param>
    public void SetPan(float Pan)
    {
      Audio.Application.RequireThreadAffinity();

      var Adjustment = Audio.ControlPan(Pan);
      if (this.Pan != Adjustment)
      {
        this.Pan = Adjustment;
        Audio.Application.Platform.AudioModulateClip(this);
      }
    }
    /// <summary>
    /// Adjust looping of the clip, before, during or after playback.
    /// </summary>
    /// <param name="Looped"></param>
    public void SetLooped(bool Looped)
    {
      Audio.Application.RequireThreadAffinity();

      if (this.Looped != Looped)
      {
        this.Looped = Looped;
        Audio.Application.Platform.AudioModulateClip(this);
      }
    }

    internal object Node { get; set; }

    internal void Set(float Volume, float Rate, float Pan, bool Looped)
    {
      var AdjustmentVolume = Audio.ControlVolume(Volume);
      var AdjustmentRate = Audio.ControlRate(Rate);
      var AdjustmentPan = Audio.ControlPan(Pan);
      if (this.Volume != AdjustmentVolume || this.Rate != AdjustmentRate || this.Pan != AdjustmentPan || Looped != this.Looped)
      {
        this.Volume = AdjustmentVolume;
        this.Rate = AdjustmentRate;
        this.Pan = AdjustmentPan;
        this.Looped = Looped;

        Audio.Application.Platform.AudioModulateClip(this);
      }
    }

    private readonly Audio Audio;
  }

  public sealed class SpeechRecognition
  {
    internal SpeechRecognition(Audio Audio)
    {
      this.Audio = Audio;
    }

    public bool IsActive { get; private set; }
    public bool IsSupported => Audio.Application.Platform.AudioSpeechRecognitionIsSupported;
    public event Action<SpeechTranscription> TranscriptionEvent;

    public void Start()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        Audio.Application.Platform.AudioStartSpeechRecognition(this);
      }
    }
    public void Stop()
    {
      if (IsActive)
      {
        this.IsActive = false;

        Audio.Application.Platform.AudioStopSpeechRecognition(this);
      }
    }

    internal object Node { get; set; }

    internal void TranscriptionInvoke(SpeechTranscription Transcription)
    {
      TranscriptionEvent?.Invoke(Transcription);
    }

    private readonly Audio Audio;
  }

  public sealed class SpeechTranscription
  {
    internal SpeechTranscription()
    {
    }

    public bool IsFinal { get; internal set; }
    public string Text { get; internal set; }
  }
}
