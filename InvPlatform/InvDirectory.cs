﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Directory"/>.
  /// </summary>
  public sealed class Directory
  {
    internal Directory(Application Application)
    {
      this.Application = Application;
      this.Root = new Folder(this, null, null);
      this.AssetFileManager = new Inv.ReadonlyFileManager
      (
        ListFunc: (Mask) => GetAssets(Mask).Select(A => A.Name),
        ExistsFunc: (Name) => new Asset(this, Name).Exists(),
        OpenFunc: (Name) => new Asset(this, Name).Open()
      );
    }

    /// <summary>
    /// Platform-specific installation location.
    /// </summary>
    public string Installation { get; internal set; }
    /// <summary>
    /// Root folder for the installation.
    /// </summary>
    public Folder Root { get; }
    public Inv.ReadonlyFileManager AssetFileManager { get; }

    /// <summary>
    /// Select a folder in the root folder.
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    [Obsolete("Use Root.NewFolder instead.", true)]
    public Folder NewFolder(string Name)
    {
      return Root.NewFolder(Name);
    }
    /// <summary>
    /// Enumerate the assets matching the <paramref name="Mask"/>.
    /// </summary>
    /// <param name="Mask"></param>
    /// <returns></returns>
    public IEnumerable<Asset> GetAssets(string Mask)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!string.IsNullOrWhiteSpace(Mask), "File mask must be specified.");

      return Application.Platform.DirectoryGetAssets(Mask);
    }
    /// <summary>
    /// Select an asset from the installation files.
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public Asset NewAsset(string Name)
    {
      return new Asset(this, Name);
    }
    /// <summary>
    /// Request the user to pick any file.
    /// </summary>
    /// <returns></returns>
    public DirectoryFilePicker NewAnyFilePicker()
    {
      return new DirectoryFilePicker(this, PickType.Any);
    }
    /// <summary>
    /// Request the user to pick an image file.
    /// </summary>
    /// <returns></returns>
    public DirectoryImagePicker NewImageFilePicker()
    {
      return new DirectoryImagePicker(this);
    }
    /// <summary>
    /// Request the user to pick a sound file.
    /// </summary>
    /// <returns></returns>
    public DirectorySoundPicker NewSoundFilePicker()
    {
      return new DirectorySoundPicker(this);
    }

    internal DirectoryFilePicker NewFilePicker(PickType PickType)
    {
      return new DirectoryFilePicker(this, PickType);
    }

    internal Application Application { get; }
  }

  internal enum PickType
  {
    Any,
    Image,
    Sound
  }

  /// <summary>
  /// File that has been picked by the user.
  /// </summary>
  public sealed class Pick
  {
    internal Pick(string Name, Func<System.IO.Stream> StreamFunc)
    {
      this.Name = Name;
      this.StreamFunc = StreamFunc;
    }

    /// <summary>
    /// Name of the picked file, including the extension.
    /// </summary>
    public string Name { get; }
    /// <summary>
    /// Name of the picked file, excluding the extension.
    /// </summary>
    public string Title => System.IO.Path.GetFileNameWithoutExtension(Name);
    /// <summary>
    /// Extension of the picked file.
    /// </summary>
    public string Extension => System.IO.Path.GetExtension(Name);

    /// <summary>
    /// Open a read stream.
    /// </summary>
    /// <returns></returns>
    public System.IO.Stream Open()
    {
      return StreamFunc();
    }
    /// <summary>
    /// Read the entire picked file into a binary.
    /// </summary>
    /// <returns></returns>
    public Inv.Binary ReadBinary()
    {
      using (var SelectStream = Open())
      using (var MemoryStream = new System.IO.MemoryStream())
      {
        if (SelectStream != null)
          SelectStream.CopyTo(MemoryStream);

        MemoryStream.Flush();

        return new Inv.Binary(MemoryStream.ToArray(), Extension);
      }
    }
    /// <summary>
    /// Treat the pick file as text.
    /// </summary>
    /// <returns></returns>
    public TextPick AsText()
    {
      return new TextPick(this);
    }
    /// <summary>
    /// Treat the pick file as csv.
    /// </summary>
    /// <returns></returns>
    public CsvPick AsCsv()
    {
      return new CsvPick(this);
    }
    /// <summary>
    /// Treat the pick file as ini.
    /// </summary>
    /// <returns></returns>
    public IniPick AsIni()
    {
      return new IniPick(this);
    }
    /// <summary>
    /// Treat the pick file as compact (custom structured binary).
    /// </summary>
    /// <returns></returns>
    public CompactPick AsCompact()
    {
      return new CompactPick(this);
    }
    /// <summary>
    /// Treat the pick file as syntax (custom structured text).
    /// </summary>
    /// <returns></returns>
    public SyntaxPick AsSyntax(Inv.Syntax.Grammar Grammar)
    {
      return new SyntaxPick(this, Grammar);
    }

    public override string ToString() => Name;

    private readonly Func<System.IO.Stream> StreamFunc;
  }

  /// <summary>
  /// Text API for a picked file.
  /// </summary>
  public sealed class TextPick
  {
    internal TextPick(Pick Base)
    {
      this.Base = Base;
    }

    /// <summary>
    /// Open the picked file with <see cref="System.IO.StreamReader"/>.
    /// </summary>
    /// <returns></returns>
    public System.IO.StreamReader Open()
    {
      return new System.IO.StreamReader(Base.Open());
    }
    /// <summary>
    /// Read all lines from the picked file.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<string> ReadLines()
    {
      using (var TextReader = Open())
      {
        var PickLine = TextReader.ReadLine();

        while (PickLine != null)
        {
          yield return PickLine;

          PickLine = TextReader.ReadLine();
        }
      }
    }
    /// <summary>
    /// Read the entire picked file into a string.
    /// </summary>
    /// <returns></returns>
    public string ReadAll()
    {
      using (var StreamReader = Open())
        return StreamReader.ReadToEnd();
    }

    public override string ToString() => Base.ToString();

    private readonly Pick Base;
  }

  /// <summary>
  /// Csv API for a picked file.
  /// </summary>
  public sealed class CsvPick
  {
    internal CsvPick(Pick Base)
    {
      this.Base = Base;
    }

    /// <summary>
    /// Open the picked file with <see cref="Inv.CsvReader"/>.
    /// </summary>
    /// <returns></returns>
    public Inv.CsvReader Open()
    {
      return new Inv.CsvReader(Base.Open());
    }

    public override string ToString() => Base.ToString();

    private readonly Pick Base;
  }

  /// <summary>
  /// Ini API for a picked file.
  /// </summary>
  public sealed class IniPick
  {
    internal IniPick(Pick Base)
    {
      this.Base = Base;
    }

    /// <summary>
    /// Open the picked file with <see cref="Inv.IniReader"/>.
    /// </summary>
    /// <returns></returns>
    public Inv.IniReader Open()
    {
      return new Inv.IniReader(Base.Open());
    }

    public override string ToString() => Base.ToString();

    private readonly Pick Base;
  }

  /// <summary>
  /// Compact API for a picked file.
  /// </summary>
  public sealed class CompactPick
  {
    internal CompactPick(Pick Base)
    {
      this.Base = Base;
    }

    /// <summary>
    /// Open the picked file with <see cref="Inv.CompactReader"/>.
    /// </summary>
    /// <returns></returns>
    public Inv.CompactReader Open()
    {
      return new Inv.CompactReader(Base.Open(), true);
    }

    public override string ToString() => Base.ToString();

    private readonly Pick Base;
  }

  /// <summary>
  /// Syntax API for a picked file.
  /// </summary>
  public sealed class SyntaxPick
  {
    internal SyntaxPick(Pick Base, Inv.Syntax.Grammar Grammar)
    {
      this.Base = Base;
      this.Grammar = Grammar;
    }

    /// <summary>
    /// Read the picked file as structured syntax.
    /// </summary>
    /// <param name="ExtractAction"></param>
    public void Read(Action<Inv.Syntax.Reader> ExtractAction)
    {
      using (var Stream = Base.Open())
        Inv.Syntax.Foundation.ReadTextStream(Grammar, Stream, ExtractAction);
    }

    public override string ToString() => Base.ToString();

    private readonly Pick Base;
    private readonly Inv.Syntax.Grammar Grammar;
  }

  /// <summary>
  /// Base file picker.
  /// </summary>
  public sealed class DirectoryFilePicker
  {
    internal DirectoryFilePicker(Directory Directory, PickType PickType)
    {
      this.Directory = Directory;
      this.PickType = PickType;
    }

    /// <summary>
    /// Title to display to the user.
    /// </summary>
    public string Title { get; set; }
    /// <summary>
    /// Fired when the user selects a pick.
    /// </summary>
    public event Action<Pick> SelectEvent;
    /// <summary>
    /// Fired when the user cancels the pick.
    /// </summary>
    public event Action CancelEvent;

    /// <summary>
    /// Show the file picker to the user.
    /// </summary>
    public void Show()
    {
      Directory.Application.Platform.DirectoryShowFilePicker(this);
    }

    internal PickType PickType { get; }
    internal object Node { get; set; }

    internal void SelectInvoke(Pick Pick)
    {
      SelectEvent?.Invoke(Pick);
    }
    internal void CancelInvoke()
    {
      CancelEvent?.Invoke();
    }

    private readonly Directory Directory;
  }

  /// <summary>
  /// Image file picker.
  /// </summary>
  public sealed class DirectoryImagePicker
  {
    internal DirectoryImagePicker(Directory Directory)
    {
      this.Base = new DirectoryFilePicker(Directory, PickType.Image);

      Base.SelectEvent += (Pick) => SelectEvent?.Invoke(new Inv.Image(Pick.ReadBinary()));
    }

    /// <summary>
    /// Title to display to the user.
    /// </summary>
    public string Title
    {
      get => Base.Title;
      set => Base.Title = value;
    }
    /// <summary>
    /// Fired when the user picks an image.
    /// </summary>
    public event Action<Inv.Image> SelectEvent;
    /// <summary>
    /// Fired when the user cancels the pick.
    /// </summary>
    public event Action CancelEvent
    {
      add => Base.CancelEvent += value;
      remove => Base.CancelEvent -= value;
    }

    /// <summary>
    /// Show the image picker to the user.
    /// </summary>
    public void Show()
    {
      Base.Show();
    }

    private readonly DirectoryFilePicker Base;
  }

  /// <summary>
  /// Sound file picker.
  /// </summary>
  public sealed class DirectorySoundPicker
  {
    internal DirectorySoundPicker(Directory Directory)
    {
      this.Base = new DirectoryFilePicker(Directory, PickType.Sound);

      Base.SelectEvent += (Pick) =>
      {
        SelectEvent?.Invoke(new Inv.Sound(Pick.ReadBinary()));
      };
    }

    /// <summary>
    /// Title to display to the user.
    /// </summary>
    public string Title
    {
      get => Base.Title;
      set => Base.Title = value;
    }
    /// <summary>
    /// Fired when the user picks a sound file.
    /// </summary>
    public event Action<Inv.Sound> SelectEvent;
    /// <summary>
    /// Fired when the user cancels the pick.
    /// </summary>
    public event Action CancelEvent
    {
      add => Base.CancelEvent += value;
      remove => Base.CancelEvent -= value;
    }

    /// <summary>
    /// Show the sound picker to the user.
    /// </summary>
    public void Show()
    {
      Base.Show();
    }

    private readonly DirectoryFilePicker Base;
  }

  /// <summary>
  /// Represents a folder inside the installation root folder.
  /// </summary>
  public sealed class Folder
  {
    internal Folder(Directory Directory, Folder Parent, string Name)
    {
      System.Diagnostics.Debug.Assert(Directory.Root == null || Parent != null, "Only the root folder may have no parent.");
      System.Diagnostics.Debug.Assert(Parent == null || Name != null, "Subfolders must have a name.");

      this.Directory = Directory;
      this.Parent = Parent;
      this.Name = Name;
    }

    /// <summary>
    /// The parent folder of this folder.
    /// </summary>
    public Folder Parent { get; }
    /// <summary>
    /// Name of the folder.
    /// </summary>
    public string Name { get; }

    /// <summary>
    /// Select a file in the folder.
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public File NewFile(string Name)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!string.IsNullOrWhiteSpace(Name), "File name must be specified.");

      return new File(this, Name);
    }
    /// <summary>
    /// Select a subfolder.
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public Folder NewFolder(string Name)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!string.IsNullOrWhiteSpace(Name), "Folder name must be specified.");

      return new Folder(Directory, this, Name);
    }
    /// <summary>
    /// Enumerate the files in the folder matching the <paramref name="Mask"/>.
    /// </summary>
    /// <param name="Mask"></param>
    /// <returns></returns>
    public IEnumerable<File> GetFiles(string Mask)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!string.IsNullOrWhiteSpace(Mask), "File mask must be specified.");

      return Directory.Application.Platform.DirectoryGetFolderFiles(this, Mask);
    }
    /// <summary>
    /// Enumerate the subfolders matching the <paramref name="Mask"/>.
    /// </summary>
    /// <param name="Mask"></param>
    /// <returns></returns>
    public IEnumerable<Folder> GetFolders(string Mask)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!string.IsNullOrWhiteSpace(Mask), "Folder mask must be specified.");

      return Directory.Application.Platform.DirectoryGetFolderSubfolders(this, Mask);
    }
    /// <summary>
    /// Relative path of the folder to the installation root.
    /// </summary>
    public string[] GetRelativePath()
    {
      var Result = new List<string>();

      Folder Current = this;
      while (Current != null && !Current.IsRoot)
      {
        Result.Insert(0, Current.Name);

        Current = Current.Parent;
      }

      return Result.ToArray();
    }
    /// <summary>
    /// Returns the fully qualified and platform-specific file path.
    /// Note that this is a leaky abstraction to the underlying platform implementation.
    /// </summary>
    /// <returns></returns>
    public string GetPlatformPath()
    {
      return Directory.Application.Platform.DirectoryGetFolderPath(this);
    }

    internal Directory Directory { get; }
    internal bool IsRoot => Parent == null;

    public override string ToString() => GetRelativePath().AsSeparatedText(System.IO.Path.DirectorySeparatorChar.ToString());
  }

  /// <summary>
  /// Assets are additional files in the application folder.
  /// These files are delivered by the app developer.
  /// </summary>
  public sealed class Asset
  {
    internal Asset(Directory Directory, string Name)
    {
      this.Directory = Directory;
      this.Name = Name;
    }

    /// <summary>
    /// Owning directory API.
    /// </summary>
    public Directory Directory { get; }
    /// <summary>
    /// Name of the asset, including the extension. eg. "Notes.txt".
    /// </summary>
    public string Name { get; }
    /// <summary>
    /// Name of the asset, exclusing the extension. eg. "Notes".
    /// </summary>
    public string Title => System.IO.Path.GetFileNameWithoutExtension(Name);
    /// <summary>
    /// Extension of the asset. eg. ".txt".
    /// </summary>
    public string Extension => System.IO.Path.GetExtension(Name);

    /// <summary>
    /// Get the length of the asset.
    /// </summary>
    /// <returns></returns>
    public Inv.DataSize GetSize()
    {
      return Inv.DataSize.FromBytes(Directory.Application.Platform.DirectoryGetLengthAsset(this));
    }
    /// <summary>
    /// Get the last write time (Utc) for the asset.
    /// </summary>
    /// <returns></returns>
    public DateTime GetLastWriteTimeUtc()
    {
      return Directory.Application.Platform.DirectoryGetLastWriteTimeUtcAsset(this);
    }
    /// <summary>
    /// Ask if the asset exists in the application folder.
    /// </summary>
    /// <returns></returns>
    public bool Exists()
    {
      return Directory.Application.Platform.DirectoryExistsAsset(this);
    }
    /// <summary>
    /// Open the asset as a stream.
    /// </summary>
    /// <returns></returns>
    public System.IO.Stream Open()
    {
      return Directory.Application.Platform.DirectoryOpenAsset(this);
    }
    /// <summary>
    /// Read the entire file into a byte array.
    /// </summary>
    /// <returns></returns>
    public byte[] ReadAllBytes()
    {
      using (var Stream = Open())
      {
        var Result = new byte[Stream.Length];

        var Actual = Stream.Read(Result, 0, Result.Length);

        if (Actual < Result.Length)
        {
          System.Diagnostics.Debug.Fail("This is not really an expected situation (excluding a timing hole where the file size changes between the two calls). Do we need a while loop to read the entire file into a single byte array?");

          Array.Resize(ref Result, Actual);
        }

        return Result;
      }
    }
    /// <summary>
    /// Treat this asset as Text.
    /// </summary>
    /// <returns></returns>
    public TextAsset AsText()
    {
      return new TextAsset(this);
    }
    /// <summary>
    /// Treat this asset as Csv.
    /// </summary>
    /// <returns></returns>
    public CsvAsset AsCsv()
    {
      return new CsvAsset(this);
    }
    /// <summary>
    /// Treat this asset as Ini.
    /// </summary>
    /// <returns></returns>
    public IniAsset AsIni()
    {
      return new IniAsset(this);
    }
    /// <summary>
    /// Treat this Asset as an image.
    /// </summary>
    /// <returns></returns>
    public ImageAsset AsImage()
    {
      return new ImageAsset(this);
    }
    /// <summary>
    /// Treat this asset as Compact (custom structured binary).
    /// </summary>
    /// <returns></returns>
    public CompactAsset AsCompact()
    {
      return new CompactAsset(this);
    }
    /// <summary>
    /// Treat this asset as Syntax (custom structured text).
    /// </summary>
    /// <returns></returns>
    public SyntaxAsset AsSyntax(Inv.Syntax.Grammar Grammar)
    {
      return new SyntaxAsset(this, Grammar);
    }
    /// <summary>
    /// Copy the asset to a file.
    /// </summary>
    /// <param name="File"></param>
    public void Copy(Inv.File File)
    {
      using (var AssetStream = Open())
      using (var FileStream = File.Create())
        AssetStream.CopyTo(FileStream, 65536);
    }

    public override string ToString() => Name;
  }

  /// <summary>
  /// Text Asset API.
  /// </summary>
  public sealed class TextAsset
  {
    internal TextAsset(Asset Base)
    {
      this.Base = Base;
    }

    /// <summary>
    /// Open the asset with <see cref="System.IO.StreamReader"/>.
    /// </summary>
    /// <returns></returns>
    public System.IO.StreamReader Open()
    {
      return new System.IO.StreamReader(Base.Open());
    }
    /// <summary>
    /// Read all the text lines from the asset.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<string> ReadLines()
    {
      using (var TextReader = Open())
      {
        var AssetLine = TextReader.ReadLine();

        while (AssetLine != null)
        {
          yield return AssetLine;

          AssetLine = TextReader.ReadLine();
        }
      }
    }
    /// <summary>
    /// Read the entire text asset into a string.
    /// </summary>
    /// <returns></returns>
    public string ReadAll()
    {
      if (Base.Exists())
      {
        using (var StreamReader = Open())
          return StreamReader.ReadToEnd();
      }
      else
      {
        return null;
      }
    }

    public override string ToString() => Base.ToString();

    private readonly Asset Base;
  }

  /// <summary>
  /// Csv Asset API.
  /// </summary>
  public sealed class CsvAsset
  {
    internal CsvAsset(Asset Base)
    {
      this.Base = Base;
    }

    /// <summary>
    /// Open the asset with <see cref="Inv.CsvReader"/>.
    /// </summary>
    /// <returns></returns>
    public Inv.CsvReader Open()
    {
      return new Inv.CsvReader(Base.Open());
    }

    public override string ToString() => Base.ToString();

    private readonly Asset Base;
  }

  /// <summary>
  /// Ini Asset API.
  /// </summary>
  public sealed class IniAsset
  {
    internal IniAsset(Asset Base)
    {
      this.Base = Base;
    }

    /// <summary>
    /// Open the asset with <see cref="Inv.IniReader"/>.
    /// </summary>
    /// <returns></returns>
    public Inv.IniReader Open()
    {
      return new Inv.IniReader(Base.Open());
    }

    public override string ToString() => Base.ToString();

    private readonly Asset Base;
  }

  /// <summary>
  /// Image Asset API.
  /// </summary>
  public sealed class ImageAsset
  {
    internal ImageAsset(Asset Base)
    {
      this.Base = Base;
    }

    /// <summary>
    /// Read the asset into an image.
    /// </summary>
    /// <returns></returns>
    public Inv.Image Read()
    {
      return new Inv.Image(Base.ReadAllBytes(), Base.Extension);
    }

    public override string ToString() => Base.ToString();

    private readonly Asset Base;
  }

  /// <summary>
  /// Compact Asset API.
  /// </summary>
  public sealed class CompactAsset
  {
    internal CompactAsset(Asset Base)
    {
      this.Base = Base;
    }

    /// <summary>
    /// Open the asset with <see cref="Inv.CompactReader"/>.
    /// </summary>
    /// <returns></returns>
    public Inv.CompactReader Open()
    {
      return new Inv.CompactReader(Base.Open(), true);
    }

    public override string ToString() => Base.ToString();

    private readonly Asset Base;
  }

  /// <summary>
  /// Syntax Asset API.
  /// </summary>
  public sealed class SyntaxAsset
  {
    internal SyntaxAsset(Asset Base, Inv.Syntax.Grammar Grammar)
    {
      this.Base = Base;
      this.Grammar = Grammar;
    }

    /// <summary>
    /// Read the asset as structured text syntax.
    /// </summary>
    /// <param name="ExtractAction"></param>
    public void Read(Action<Inv.Syntax.Reader> ExtractAction)
    {
      using (var Stream = Base.Open())
        Inv.Syntax.Foundation.ReadTextStream(Grammar, Stream, ExtractAction);
    }

    public override string ToString() => Base.ToString();

    private readonly Asset Base;
    private readonly Inv.Syntax.Grammar Grammar;
  }

  /// <summary>
  /// Represents a file in the application folder.
  /// </summary>
  public sealed class File
  {
    internal File(Folder Folder, string Name)
    {
      this.Folder = Folder;
      this.Name = Name;
    }

    /// <summary>
    /// Owning folder.
    /// </summary>
    public Folder Folder { get; }
    /// <summary>
    /// Name of the file, including the extension. eg. "Notes.txt"
    /// </summary>
    public string Name { get; }
    /// <summary>
    /// Name of the file, excluding the extension. eg. "Notes"
    /// </summary>
    public string Title => System.IO.Path.GetFileNameWithoutExtension(Name);
    /// <summary>
    /// Extension of the file. eg. ".txt"
    /// </summary>
    public string Extension => System.IO.Path.GetExtension(Name);

    /// <summary>
    /// Get the length of the file.
    /// </summary>
    /// <returns></returns>
    public Inv.DataSize GetSize()
    {
      return Inv.DataSize.FromBytes(Folder.Directory.Application.Platform.DirectoryGetLengthFile(this));
    }
    /// <summary>
    /// Get the last write time (Utc) for the file.
    /// </summary>
    /// <returns></returns>
    public DateTime GetLastWriteTimeUtc()
    {
      return Folder.Directory.Application.Platform.DirectoryGetLastWriteTimeUtcFile(this);
    }
    /// <summary>
    /// Set the last write time (Utc) for the file.
    /// </summary>
    /// <param name="Timestamp"></param>
    public void SetLastWriteTimeUtc(DateTime Timestamp)
    {
      Folder.Directory.Application.Platform.DirectorySetLastWriteTimeUtcFile(this, Timestamp);
    }
    /// <summary>
    /// Create new or replace an existing file and return the stream for writing.
    /// </summary>
    /// <returns></returns>
    public System.IO.Stream Create()
    {
      return Folder.Directory.Application.Platform.DirectoryCreateFile(this);
    }
    /// <summary>
    /// Open an existing file and return the stream for reading.
    /// </summary>
    /// <returns></returns>
    public System.IO.Stream Open()
    {
      return Folder.Directory.Application.Platform.DirectoryOpenFile(this);
    }
    /// <summary>
    /// Append an existing file and return the stream for writing, positioned at the end of the file.
    /// </summary>
    /// <returns></returns>
    public System.IO.Stream Append()
    {
      return Folder.Directory.Application.Platform.DirectoryAppendFile(this);
    }
    /// <summary>
    /// Ask if the file exists in the owning folder.
    /// </summary>
    /// <returns></returns>
    public bool Exists()
    {
      return Folder.Directory.Application.Platform.DirectoryExistsFile(this);
    }
    /// <summary>
    /// Delete the file from the owning folder.
    /// </summary>
    public void Delete()
    {
      Folder.Directory.Application.Platform.DirectoryDeleteFile(this);
    }
    /// <summary>
    /// Copy this file to the location specified by <paramref name="CopyFile"/> but fail if it already exists.
    /// </summary>
    /// <param name="CopyFile"></param>
    public void Copy(Inv.File CopyFile)
    {
      Folder.Directory.Application.Platform.DirectoryCopyFile(this, CopyFile);
    }
    /// <summary>
    /// Move this file to the location specified by <paramref name="MoveFile"/> but fail if it already exists.
    /// </summary>
    /// <param name="MoveFile"></param>
    public void Move(Inv.File MoveFile)
    {
      Folder.Directory.Application.Platform.DirectoryMoveFile(this, MoveFile);
    }
    /// <summary>
    /// Replace this file with the location specified by <paramref name="ReplaceFile"/>.
    /// </summary>
    /// <param name="MoveFile"></param>
    public void Replace(Inv.File ReplaceFile)
    {
      Folder.Directory.Application.Platform.DirectoryReplaceFile(this, ReplaceFile);
    }
    /// <summary>
    /// Read the entire file into a byte array.
    /// </summary>
    /// <returns></returns>
    public byte[] ReadAllBytes()
    {
      using (var Stream = Open())
      {
        var Result = new byte[Stream.Length];

        var Actual = Stream.Read(Result, 0, Result.Length);

        if (Actual < Result.Length)
        {
          System.Diagnostics.Debug.Fail("This is not really an expected situation (excluding a timing hole where the file size changes between the two calls). Do we need a while loop to read the entire file into a single byte array?");

          Array.Resize(ref Result, Actual);
        }

        return Result;
      }
    }
    /// <summary>
    /// Create or replace the entire file with the byte array.
    /// </summary>
    /// <param name="Buffer"></param>
    public void WriteAllBytes(byte[] Buffer)
    {
      using (var Stream = Create())
        Stream.Write(Buffer, 0, Buffer.Length);
    }
    /// <summary>
    /// Relative path of the file to the installation root.
    /// </summary>
    public string[] GetRelativePath()
    {
      var Result = Folder.GetRelativePath();

      Array.Resize(ref Result, Result.Length + 1);

      Result[Result.Length - 1] = Name;

      return Result;
    }
    /// <summary>
    /// Returns the fully qualified and platform-specific file path.
    /// Note that this is a leaky abstraction to the underlying platform implementation.
    /// </summary>
    /// <returns></returns>
    public string GetPlatformPath()
    {
      return Folder.Directory.Application.Platform.DirectoryGetFilePath(this);
    }

    /// <summary>
    /// Treat this file as an image.
    /// </summary>
    /// <returns></returns>
    public ImageFile AsImage()
    {
      return new ImageFile(this);
    }
    /// <summary>
    /// Treat this file as csv.
    /// </summary>
    /// <returns></returns>
    public CsvFile AsCsv()
    {
      return new CsvFile(this);
    }
    /// <summary>
    /// Treat this file as ini.
    /// </summary>
    /// <returns></returns>
    public IniFile AsIni()
    {
      return new IniFile(this);
    }
    /// <summary>
    /// Treat this file as text.
    /// </summary>
    /// <returns></returns>
    public TextFile AsText()
    {
      return new TextFile(this);
    }
    /// <summary>
    /// Treat this file as compact (custom binary).
    /// </summary>
    /// <returns></returns>
    public CompactFile AsCompact()
    {
      return new CompactFile(this);
    }
    /// <summary>
    /// Treat this file as syntax (custom structured text).
    /// </summary>
    /// <param name="Grammar"></param>
    /// <returns></returns>
    public SyntaxFile AsSyntax(Inv.Syntax.Grammar Grammar)
    {
      return new SyntaxFile(this, Grammar);
    }

    public override string ToString() => GetRelativePath().AsSeparatedText(System.IO.Path.DirectorySeparatorChar.ToString());
  }

  /// <summary>
  /// Compact file API.
  /// </summary>
  public sealed class CompactFile
  {
    internal CompactFile(File Base)
    {
      this.Base = Base;
    }

    /// <summary>
    /// Create new or replace existing file.
    /// </summary>
    /// <returns></returns>
    public Inv.CompactWriter Create()
    {
      return new Inv.CompactWriter(Base.Create(), true);
    }
    /// <summary>
    /// Open existing file for reading.
    /// </summary>
    /// <returns></returns>
    public Inv.CompactReader Open()
    {
      return new Inv.CompactReader(Base.Open(), true);
    }
    /// <summary>
    /// Append from the end of an existing file.
    /// </summary>
    /// <returns></returns>
    public Inv.CompactWriter Append()
    {
      return new Inv.CompactWriter(Base.Append(), true);
    }

    public override string ToString() => Base.ToString();

    private readonly File Base;
  }

  /// <summary>
  /// Image File API.
  /// </summary>
  public sealed class ImageFile
  {
    internal ImageFile(File Base)
    {
      this.Base = Base;
    }

    /// <summary>
    /// Create new or replace existing image file.
    /// </summary>
    /// <returns></returns>
    public void Write(Inv.Image Image)
    {
      Base.WriteAllBytes(Image.GetBuffer());
    }
    /// <summary>
    /// Read existing file into an image.
    /// </summary>
    /// <returns></returns>
    public Inv.Image Read()
    {
      return new Inv.Image(Base.ReadAllBytes(), Base.Extension);
    }

    public override string ToString() => Base.ToString();

    private readonly File Base;
  }

  /// <summary>
  /// Csv File API.
  /// </summary>
  public sealed class CsvFile
  {
    internal CsvFile(File Base)
    {
      this.Base = Base;
    }

    /// <summary>
    /// Create new or replace existing file.
    /// </summary>
    /// <returns></returns>
    public Inv.CsvWriter Create()
    {
      return new Inv.CsvWriter(Base.Create());
    }
    /// <summary>
    /// Open existing file for reading.
    /// </summary>
    /// <returns></returns>
    public Inv.CsvReader Open()
    {
      return new Inv.CsvReader(Base.Open());
    }
    /// <summary>
    /// Append from the end of an existing file.
    /// </summary>
    /// <returns></returns>
    public Inv.CsvWriter Append()
    {
      return new Inv.CsvWriter(Base.Append());
    }

    public override string ToString() => Base.ToString();

    private readonly File Base;
  }

  /// <summary>
  /// Ini File API.
  /// </summary>
  public sealed class IniFile
  {
    internal IniFile(File Base)
    {
      this.Base = Base;
    }

    /// <summary>
    /// Create new or replace existing file.
    /// </summary>
    /// <returns></returns>
    public Inv.IniWriter Create()
    {
      return new Inv.IniWriter(Base.Create());
    }
    /// <summary>
    /// Open existing file for reading.
    /// </summary>
    /// <returns></returns>
    public Inv.IniReader Open()
    {
      return new Inv.IniReader(Base.Open());
    }
    /// <summary>
    /// Append from the end of an existing file.
    /// </summary>
    /// <returns></returns>
    public Inv.IniWriter Append()
    {
      return new Inv.IniWriter(Base.Append());
    }

    public override string ToString() => Base.ToString();

    private readonly File Base;
  }

  /// <summary>
  /// Text File API.
  /// </summary>
  public sealed class TextFile
  {
    internal TextFile(File Base)
    {
      this.Base = Base;
    }

    /// <summary>
    /// Create new or replace existing file.
    /// </summary>
    /// <returns></returns>
    public System.IO.StreamWriter Create()
    {
      return new System.IO.StreamWriter(Base.Create());
    }
    /// <summary>
    /// Open existing file for reading.
    /// </summary>
    /// <returns></returns>
    public System.IO.StreamReader Open()
    {
      return new System.IO.StreamReader(Base.Open());
    }
    /// <summary>
    /// Append from the end of an existing file.
    /// </summary>
    /// <returns></returns>
    public System.IO.StreamWriter Append()
    {
      return new System.IO.StreamWriter(Base.Append());
    }
    /// <summary>
    /// Ask if the file exists in the owning folder.
    /// </summary>
    /// <returns></returns>
    public bool Exists()
    {
      return Base.Exists();
    }
    /// <summary>
    /// Read all lines from the text file.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<string> ReadLines()
    {
      using (var TextReader = Open())
      {
        var FileLine = TextReader.ReadLine();

        while (FileLine != null)
        {
          yield return FileLine;

          FileLine = TextReader.ReadLine();
        }
      }
    }
    /// <summary>
    /// Read the entire text file into a string.
    /// </summary>
    /// <returns></returns>
    public string ReadAll()
    {
      if (Base.Exists())
      {
        using (var StreamReader = Open())
          return StreamReader.ReadToEnd();
      }
      else
      {
        return null;
      }
    }
    /// <summary>
    /// Create new or replace existing file with a string.
    /// </summary>
    /// <param name="Text"></param>
    public void WriteAll(string Text)
    {
      using (var StreamWriter = Create())
        StreamWriter.Write(Text);
    }

    public override string ToString() => Base.ToString();

    private readonly File Base;
  }

  /// <summary>
  /// Syntax File API.
  /// </summary>
  public sealed class SyntaxFile
  {
    internal SyntaxFile(File Base, Inv.Syntax.Grammar Grammar)
    {
      this.Base = Base;
      this.Grammar = Grammar;
    }

    /// <summary>
    /// Write the text file as structured syntax.
    /// </summary>
    /// <param name="FormatAction"></param>
    public void Write(Action<Inv.Syntax.Writer> FormatAction)
    {
      using (var Stream = Base.Create())
        Inv.Syntax.Foundation.WriteTextStream(Grammar, Stream, FormatAction);
    }
    /// <summary>
    /// Read the text file as structured syntax.
    /// </summary>
    /// <param name="ExtractAction"></param>
    public void Read(Action<Inv.Syntax.Reader> ExtractAction)
    {
      using (var Stream = Base.Open())
        Inv.Syntax.Foundation.ReadTextStream(Grammar, Stream, ExtractAction);
    }

    public override string ToString() => Base.ToString();

    private readonly File Base;
    private readonly Inv.Syntax.Grammar Grammar;
  }
}