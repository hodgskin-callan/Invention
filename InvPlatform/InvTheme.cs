﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  public static class Theme
  {
    static Theme()
    {
      Set(DefaultPrimaryLightVariantColour, DefaultPrimaryDarkVariantColour, DefaultSecondaryLightVariantColour, DefaultSecondaryDarkVariantColour, IsDarkValue: false);
    }

    public static bool IsDark { get; private set; }
    public static bool IsLight => !IsDark;
    /// <summary>
    /// The colour displayed most frequently across an app's screens and components.
    /// </summary>
    public static Inv.Colour PrimaryColour => PrimaryLightVariantColour;
    public static Inv.Colour PrimaryLightVariantColour { get; private set; }
    public static Inv.Colour PrimaryDarkVariantColour { get; private set; }
    /// <summary>
    /// The colour used to render text and icons appearing 'on' a primary-coloured surface.
    /// </summary>
    public static Inv.Colour OnPrimaryColour => OnPrimaryLightVariantColour;
    public static Inv.Colour OnPrimaryLightVariantColour { get; private set; }
    public static Inv.Colour OnPrimaryDarkVariantColour { get; private set; }
    /// <summary>
    /// The accent colour displayed across an app's screens and components.
    /// </summary>
    public static Inv.Colour SecondaryColour => SecondaryLightVariantColour;
    public static Inv.Colour SecondaryLightVariantColour { get; private set; }
    public static Inv.Colour SecondaryDarkVariantColour { get; private set; }
    /// <summary>
    /// The colour used to render text and icons appearing 'on' a secondary-coloured surface.
    /// </summary>
    public static Inv.Colour OnSecondaryColour => OnSecondaryLightVariantColour;
    public static Inv.Colour OnSecondaryLightVariantColour { get; private set; }
    public static Inv.Colour OnSecondaryDarkVariantColour { get; private set; }
    /// <summary>
    /// BlackSmoke or White for the background colour of dark/light themes.
    /// </summary>
    public static Inv.Colour BackgroundColour { get; private set; }
    /// <summary>
    /// White or Black for text and icons appearing 'on' a background-coloured surface.
    /// </summary>
    public static Inv.Colour OnBackgroundColour { get; private set; }
    public static Inv.Colour ErrorColour { get; private set; }
    public static Inv.Colour OnErrorColour { get; private set; }
    public static Inv.Colour SurfaceColour => SmokeColour;
    public static Inv.Colour OnSurfaceColour => OnBackgroundColour;
    /// <summary>
    /// GraySmoke or WhiteSmoke as the primary background colour.
    /// </summary>
    public static Inv.Colour SmokeColour { get; private set; }
    /// <summary>
    /// DimGray or LightGray to give firm edges around smoke coloured backgrounds.
    /// </summary>
    public static Inv.Colour EdgeColour { get; private set; }
    /// <summary>
    /// DeepGray or DarkGray for soft edge colour;
    /// </summary>
    public static Inv.Colour SoftColour { get; private set; }
    /// <summary>
    /// LightGray or DimGray for subtle text colour;
    /// </summary>
    public static Inv.Colour SubtleColour { get; private set; }

    public static void Set(Inv.Colour PrimaryColour, Inv.Colour SecondaryColour, bool IsDarkValue)
    {
      Set(PrimaryColour, PrimaryColour, SecondaryColour, SecondaryColour, IsDarkValue);
    }
    public static void Set(Inv.Colour PrimaryLightVariantColourValue, Inv.Colour PrimaryDarkVariantColourValue, Inv.Colour SecondaryLightVariantColourValue, Inv.Colour SecondaryDarkVariantColourValue, bool IsDarkValue)
    {
      IsDark = IsDarkValue;

      PrimaryLightVariantColour = PrimaryLightVariantColourValue ?? DefaultPrimaryLightVariantColour;
      OnPrimaryLightVariantColour = PrimaryLightVariantColour.BackgroundToBlackWhiteForeground();
      PrimaryDarkVariantColour = PrimaryDarkVariantColourValue ?? DefaultPrimaryDarkVariantColour;
      OnPrimaryDarkVariantColour = PrimaryDarkVariantColour.BackgroundToBlackWhiteForeground();

      SecondaryLightVariantColour = SecondaryLightVariantColourValue ?? DefaultSecondaryLightVariantColour;
      OnSecondaryLightVariantColour = SecondaryLightVariantColour.BackgroundToBlackWhiteForeground();
      SecondaryDarkVariantColour = SecondaryDarkVariantColourValue ?? DefaultSecondaryDarkVariantColour;
      OnSecondaryDarkVariantColour = SecondaryDarkVariantColour.BackgroundToBlackWhiteForeground();

      ErrorColour = IsDark ? Inv.Colour.FromHexadecimalString("CF6679") : Inv.Colour.FromHexadecimalString("B00020");
      OnErrorColour = IsDark ? Inv.Colour.Black : Inv.Colour.White;

      SmokeColour = IsDark ? Inv.Colour.GraySmoke : Inv.Colour.WhiteSmoke;
      OnBackgroundColour = IsDark ? Inv.Colour.White : Inv.Colour.Black;
      SubtleColour = IsDark ? Inv.Colour.LightGray : Inv.Colour.DimGray;
      EdgeColour = IsDark ? Inv.Colour.DimGray : Inv.Colour.LightGray;
      SoftColour = IsDark ? Inv.Colour.DeepGray : Inv.Colour.DarkGray;
      BackgroundColour = IsDark ? Inv.Colour.BlackSmoke : Inv.Colour.White;
    }
    public static void SetDark()
    {
      Set(PrimaryLightVariantColour, PrimaryDarkVariantColour, SecondaryLightVariantColour, SecondaryDarkVariantColour, IsDarkValue: true);
    }
    public static void SetLight()
    {
      Set(PrimaryLightVariantColour, PrimaryDarkVariantColour, SecondaryLightVariantColour, SecondaryDarkVariantColour, IsDarkValue: false);
    }

    public static class Light
    {
      public static Inv.Colour White => Theme.BackgroundColour;
      public static Inv.Colour WhiteSmoke => Theme.SmokeColour;
      public static Inv.Colour LightGray => Theme.EdgeColour;
      public static Inv.Colour DimGray => Theme.SubtleColour;
      public static Inv.Colour DarkGray => Theme.SoftColour;
    }

    public static readonly Inv.Colour DefaultPrimaryLightVariantColour = Inv.Colour.FromHexadecimalString("106FCF");
    public static readonly Inv.Colour DefaultPrimaryDarkVariantColour = Inv.Colour.FromHexadecimalString("8DC7F7");
    //public static readonly Inv.Colour DefaultSecondaryLightVariantColour = Inv.Colour.FromHexadecimalString("CF7010"); // Complementary
    //public static readonly Inv.Colour DefaultSecondaryDarkVariantColour = Inv.Colour.FromHexadecimalString("F7BD8D"); // Complementary
    public static readonly Inv.Colour DefaultSecondaryLightVariantColour = Inv.Colour.FromHexadecimalString("10CFCF"); // Analogous
    public static readonly Inv.Colour DefaultSecondaryDarkVariantColour = Inv.Colour.FromHexadecimalString("8DF7F2"); // Analogous
  }
}
