﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  public static class Reproduction
  {
    public static string ReproduceShell(Inv.Surface Surface)
    {
      var Result =
@"using System;
using Inv.Support;

namespace Reproduction
{
  public static class Shell
  {
    public static void Install(Inv.Application Application)
    {
      var Surface = Application.Window.NewSurface();
      Application.Window.Transition(Surface);
      // TODO //
    }
  }
}";

      return Result.Replace("// TODO //", ReproduceSurface(Surface));
    }

    private static string ReproduceSurface(Inv.Surface Surface)
    {
      var StringList = new List<string>();

      var IncrementArray = new Inv.EnumArray<ControlType, int>();
      IncrementArray.Fill(1);

      void ReproduceControl(string Assignment, Inv.Control Control)
      {
        var Index = IncrementArray[Control.ControlType]++;

        var Variable = Control.ControlType.ToString() + Index;

        string Suffix;
        switch (Control.ControlType)
        {
          case ControlType.Dock:
            var Dock = (Dock)Control;
            Suffix = Dock.Orientation.ToString();
            break;

          case ControlType.Stack:
            var Stack = (Stack)Control;
            Suffix = Stack.Orientation.ToString();
            break;

          case ControlType.Wrap:
            var Wrap = (Wrap)Control;
            Suffix = Wrap.Orientation.ToString();
            break;

          case ControlType.Scroll:
            var Scroll = (Scroll)Control;
            Suffix = Scroll.Orientation.ToString();
            break;

          case ControlType.Button:
            var Button = (Button)Control;
            Suffix = Button.Style.ToString();
            break;

          case ControlType.Edit:
            var Edit = (Edit)Control;
            Suffix = Edit.Input.ToString();
            break;

          case ControlType.Overlay:
          case ControlType.Frame:
          case ControlType.Label:
          case ControlType.Graphic:
          case ControlType.Flow:
          case ControlType.Table:
          case ControlType.Board:
          case ControlType.Block:
          case ControlType.Canvas:
          case ControlType.Memo:
          case ControlType.Browser:
          case ControlType.Video:
          case ControlType.Switch:
            Suffix = "";
            break;

          default:
            throw new Exception("ControlType not handled: " + Control.ControlType);
        }

        StringList.Add("");
        StringList.Add($"var {Variable} = Inv.{Control.ControlType}.New{Suffix}();");
        StringList.Add(string.Format(Assignment, Variable));

        if (!Control.Visibility.Get())
          StringList.Add($"{Variable}.Visibility.Collapse();");

        var Placement = Control.Alignment.Get();
        if (Placement != Inv.Placement.Stretch)
          StringList.Add($"{Variable}.Alignment." + Placement.ToString() + "();");

        void ReproduceSize(string Qualifier, int? Width, int? Height)
        {
          if (Width != null || Height != null)
          {
            if (Width != null && Height == null)
              StringList.Add($"{Variable}.Size.Set{Qualifier}Width({Width.Value});");
            else if (Width == null && Height != null)
              StringList.Add($"{Variable}.Size.Set{Qualifier}Height({Height.Value});");
            else if (Width.Value == Height.Value)
              StringList.Add($"{Variable}.Size.Set{Qualifier}({Width.Value});");
            else
              StringList.Add($"{Variable}.Size.Set{Qualifier}({Width.Value}, {Height.Value});");
          }
        }

        ReproduceSize("", Control.Size.Width, Control.Size.Height);
        ReproduceSize("Minimum", Control.Size.MinimumWidth, Control.Size.MinimumHeight);
        ReproduceSize("Maximum", Control.Size.MaximumWidth, Control.Size.MaximumHeight);

        void ReproduceEdge(string Name, Inv.Edge Edge)
        {
          if (Edge.IsSet)
          {
            if (Edge.IsUniform)
              StringList.Add($"{Variable}.{Name}.Set(" + Edge.Left + ");");
            else if (Edge.IsVertical && Edge.IsHorizontal)
              StringList.Add($"{Variable}.{Name}.Set(" + Edge.Left + ", " + Edge.Top + ");");
            else
              StringList.Add($"{Variable}.{Name}.Set(" + Edge.Left + ", " + Edge.Top + ", " + Edge.Right + ", " + Edge.Bottom + ");");
          }
        }

        ReproduceEdge("Margin", Control.Margin);
        ReproduceEdge("Padding", Control.Padding);

        if (Control.Corner.IsSet)
        {
          if (Control.Corner.IsUniform)
            StringList.Add($"{Variable}.Corner.Set(" + Control.Corner.TopLeft + ");");
          else
            StringList.Add($"{Variable}.Corner.Set(" + Control.Corner.TopLeft + ", " + Control.Corner.TopRight + ", " + Control.Corner.BottomRight + ", " + Control.Corner.BottomLeft + ");");
        }

        var Percent = Control.Opacity.Get();
        if (Percent != 1.0F)
          StringList.Add($"{Variable}.Opacity.Set(" + Percent.ToString("F") + "F);");

        var Depth = Control.Elevation.Get();
        if (Depth != 0)
          StringList.Add($"{Variable}.Elevation.Set(" + Depth.ToString() + ");");

        if (Control.Background.Colour != null)
          StringList.Add($"{Variable}.Background.In({ReproduceColour(Control.Background.Colour)});");

        if (Control.Border.IsSet || Control.Border.Colour != null)
        {
          var ColourSuffix = Control.Border.Colour != null ? $".In({ReproduceColour(Control.Border.Colour)})" : "";

          if (Control.Border.IsUniform)
            StringList.Add($"{Variable}.Border.Set({Control.Border.Left}){ColourSuffix};");
          else if (Control.Border.IsVertical && Control.Border.IsHorizontal)
            StringList.Add($"{Variable}.Border.Set({Control.Border.Left}, {Control.Border.Top}){ColourSuffix};");
          else
            StringList.Add($"{Variable}.Border.Set({Control.Border.Left}, {Control.Border.Top}, {Control.Border.Right}, {Control.Border.Bottom}){ColourSuffix};");
        }

        switch (Control.ControlType)
        {
          case ControlType.Overlay:
            var Overlay = (Overlay)Control;

            foreach (var Child in Overlay.Panels)
              ReproducePanel(Variable + ".AddPanel({0});", Child);
            break;

          case ControlType.Block:
            var Block = (Block)Control;

            foreach (var Span in Block.Spans)
            {
              var FontList = new List<string>();
              ReproduceFont(Surface, FontList, "S", Span.Font);

              void IncludeFontList()
              {
                StringList.Add("{");

                foreach (var Font in FontList)
                  StringList.Add("  " + Font);

                StringList.Add("});");
              }

              switch (Span.Style)
              {
                case BlockStyle.Break:
                  if (FontList.Count > 0)
                  {
                    StringList.Add($"{Variable}.AddBreak(S =>");
                    IncludeFontList();
                  }
                  else
                  {
                    StringList.Add($"{Variable}.AddBreak();");
                  }
                  break;

                case BlockStyle.Run:
                  if (FontList.Count > 0)
                  {
                    StringList.Add($"{Variable}.AddRun({Span.Text?.ConvertToCSharpString()}, S =>");
                    IncludeFontList();
                  }
                  else
                  {
                    StringList.Add($"{Variable}.AddRun({Span.Text?.ConvertToCSharpString()});");
                  }
                  break;

                default:
                  throw new Exception("BlockStyle not handled: " + Span.Style);
              }
            }
            break;

          case ControlType.Board:
            var Board = (Board)Control;

            foreach (var Child in Board.Pins)
              ReproducePanel(Variable + $".AddPin({0}, new Inv.Rect({Child.Rect.Left}, {Child.Rect.Top}, {Child.Rect.Right}, {Child.Rect.Bottom}));", Child.Panel);
            break;

          case ControlType.Stack:
            var Stack = (Stack)Control;

            foreach (var Child in Stack.Panels)
              ReproducePanel(Variable + ".AddPanel({0});", Child);
            break;

          case ControlType.Wrap:
            var Wrap = (Wrap)Control;

            foreach (var Child in Wrap.Panels)
              ReproducePanel(Variable + ".AddPanel({0});", Child);
            break;

          case ControlType.Dock:
            var Dock = (Dock)Control;

            foreach (var Child in Dock.Headers)
              ReproducePanel(Variable + ".AddHeader({0});", Child);

            foreach (var Child in Dock.Clients)
              ReproducePanel(Variable + ".AddClient({0});", Child);

            foreach (var Child in Dock.Footers)
              ReproducePanel(Variable + ".AddFooter({0});", Child);
            break;

          case ControlType.Table:
            var Table = (Table)Control;

            foreach (var Column in Table.Columns)
            {
              var ColumnVariable = Variable + "Column" + Column.Index;

              StringList.Add($"var {ColumnVariable} = {Variable}.Add{Column.LengthType.ToString()}Column({ReproduceTableAxisValue(Column)});");

              if (Column.Content != null)
                ReproducePanel(ColumnVariable + ".Content = {0};", Column.Content);
            }

            foreach (var Row in Table.Rows)
            {
              var RowVariable = Variable + "Row" + Row.Index;

              StringList.Add($"var {RowVariable} = {Variable}.Add{Row.LengthType.ToString()}Row({ReproduceTableAxisValue(Row)});");

              if (Row.Content != null)
                ReproducePanel(RowVariable + ".Content = {0};", Row.Content);
            }

            foreach (var Cell in Table.GetCells())
            {
              if (Cell.Content != null)
                ReproducePanel($"{Variable}.GetCell({Variable}Column{Cell.Column.Index}, {Variable}Row{Cell.Row.Index}).Content = {{0}};", Cell.Content);
            }
            break;

          case ControlType.Frame:
            var Frame = (Frame)Control;
            if (Frame.Content != null)
              ReproducePanel(Variable + ".Content = {0};", Frame.Content);
            break;

          case ControlType.Button:
            var Button = (Button)Control;
            if (Button.Content != null)
              ReproducePanel(Variable + ".Content = {0};", Button.Content);
            break;

          case ControlType.Scroll:
            var Scroll = (Scroll)Control;
            if (Scroll.Content != null)
              ReproducePanel(Variable + ".Content = {0};", Scroll.Content);
            break;

          case ControlType.Label:
            var Label = (Label)Control;

            if (!Label.LineWrapping)
              StringList.Add($"{Variable}.LineWrapping = false;");

            if (Label.Justify.Get() != Inv.Justification.Left)
              StringList.Add($"{Variable}.Justify.{Label.Justify.Get()}();");

            ReproduceFont(Surface, StringList, Variable, Label.Font);

            if (Label.Text != null)
              StringList.Add($"{Variable}.Text = {Label.Text.ConvertToCSharpString()};");
            break;

          case ControlType.Edit:
            var Edit = (Edit)Control;

            if (Edit.IsReadOnly)
              StringList.Add($"{Variable}.IsReadOnly = true;");

            if (Edit.Justify.Get() != Inv.Justification.Left)
              StringList.Add($"{Variable}.Justify.{Edit.Justify.Get()}();");

            ReproduceFont(Surface, StringList, Variable, Edit.Font);

            if (Edit.Text != null)
              StringList.Add($"{Variable}.Text = {Edit.Text.ConvertToCSharpString()};");
            break;

          case ControlType.Memo:
            var Memo = (Memo)Control;

            if (Memo.IsReadOnly)
              StringList.Add(Variable + ".IsReadOnly = true;");

            ReproduceFont(Surface, StringList, Variable, Memo.Font);

            if (Memo.Text != null)
              StringList.Add($"{Variable}.Text = {Memo.Text.ConvertToCSharpString()};");

            // TODO: memo markup.
            break;

          case ControlType.Graphic:
            var Graphic = (Graphic)Control;

            if (Graphic.Image != null)
              StringList.Add(string.Format(Variable + ".Image = new Inv.Image(Convert.FromBase64String({0}), {1});", Convert.ToBase64String(Graphic.Image.GetBuffer()).ConvertToCSharpString(), Graphic.Image.GetFormat()?.ConvertToCSharpString()));
            break;

          case ControlType.Switch:
            //var Switch = (Switch)Control;

            // TODO: colours
            break;

          case ControlType.Flow:
            //var Flow = (Flow)Control;

            // TODO: current visual tree?
            break;

          case ControlType.Canvas:
            //var Canvas = (Canvas)Control;

            // TODO: execute Draw and code generate the current view?
            break;

          case ControlType.Browser:
            var Browser = (Browser)Control;

            if (Browser.Uri != null)
              StringList.Add($"{Variable}.LoadUri(new Uri({Browser.Uri.AbsoluteUri.ConvertToCSharpString()}));");
            else if (Browser.Html != null)
              StringList.Add($"{Variable}.LoadHtml({Browser.Html.ConvertToCSharpString()});");
            break;

          case ControlType.Video:
            var Video = (Video)Control;

            if (Video.Uri != null)
              StringList.Add($"{Variable}.LoadUri(new Uri({Video.Uri.AbsoluteUri.ConvertToCSharpString()}));");
            else if (Video.File != null)
              throw new Exception("Video.File not supported.");
            else if (Video.Asset != null)
              throw new Exception("Video.Asset not supported.");
            break;

          default:
            throw new Exception("ControlType not handled: " + Control.ControlType);
        }
      }

      void ReproducePanel(string Assignment, Inv.Panel Panel)
      {
        if (Panel == null)
          StringList.Add(string.Format(Assignment, "null"));
        else
          ReproduceControl(Assignment, Panel.Control);
      }

      ReproducePanel("Surface.Content = {0};", Surface.Content);

      return StringList.AsSeparatedText(Environment.NewLine + "      ") // each string on a separate line, with the appropriate whitespace indent.
        .Replace(Environment.NewLine + "      " + Environment.NewLine, Environment.NewLine + Environment.NewLine); // strip the whitespace indent from empty lines.
    }
    private static void ReproduceFont(Inv.Surface Surface, List<string> StringList, string Variable, Inv.Font Font)
    {
      var ChainList = new List<string>();

      if (Font.Name != null)
      {
        if (Font.Name == Surface.Window.Application.Device.ProportionalFontName)
          ChainList.Add($"Proportional()");
        else if (Font.Name == Surface.Window.Application.Device.MonospacedFontName)
          ChainList.Add($"Monospaced()");
        else
          ChainList.Add($"Family({Font.Name.ConvertToCSharpString()})");
      }

      if (Font.Size != null)
      {
        var Common = Inv.FontSize.CommonName(Font.Size.Value);

        if (Common != null)
          ChainList.Add($"{Common}()");
        else
          ChainList.Add($"Custom({Font.Size.Value})");
      }

      if (Font.Weight != null)
        ChainList.Add($"{Font.Weight}()");

      if (Font.IsStrikethrough != null)
        ChainList.Add($"Strikethrough({Font.IsStrikethrough.Value.ConvertToCSharpKeyword()})");

      if (Font.IsUnderlined != null)
        ChainList.Add($"Underline({Font.IsUnderlined.Value.ConvertToCSharpKeyword()})");

      if (Font.IsItalics != null)
        ChainList.Add($"Italics({Font.IsItalics.Value.ConvertToCSharpKeyword()})");

      if (Font.IsSmallCaps != null)
        ChainList.Add($"SmallCaps({Font.IsSmallCaps.Value.ConvertToCSharpKeyword()})");

      if (Font.Colour != null)
        ChainList.Add($"In({ReproduceColour(Font.Colour)})");

      if (ChainList.Count > 0)
        StringList.Add($"{Variable}.Font.{ChainList.AsSeparatedText(".")};");
    }
    private static string ReproduceColour(Inv.Colour Colour)
    {
      var KnownName = Colour.Name;

      if (KnownName == Colour.Hex)
        return $"Inv.Colour.FromArgb(0x{KnownName.StripWhitespace()})";
      else
        return $"Inv.Colour.{KnownName}";
    }
    private static string ReproduceTableAxisValue(Inv.TableAxis TableAxis)
    {
      switch (TableAxis.LengthType)
      {
        case TableAxisLength.Auto:
          return "";

        case TableAxisLength.Fixed:
          return TableAxis.LengthValue.ToString();

        case TableAxisLength.Star:
          return TableAxis.LengthValue != 1 ? TableAxis.LengthValue.ToString() : "";

        default:
          throw new Exception("TableAxisLength not handled: " + TableAxis.LengthType);
      }
    }
  }
}
