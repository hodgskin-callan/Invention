﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Calendar"/>
  /// </summary>
  public sealed class Calendar
  {
    internal Calendar(Application Application)
    {
      this.Application = Application;
    }

    /// <summary>
    /// Get the time zone name configured on this device.
    /// </summary>
    /// <returns></returns>
    public string GetTimeZoneName()
    {
      return Application.Platform.CalendarTimeZoneName();
    }
    /// <summary>
    /// Create a date and time picker using the device platform controls.
    /// </summary>
    /// <returns></returns>
    public CalendarPicker NewDateTimePicker()
    {
      return NewPicker(true, true);
    }
    /// <summary>
    /// Create a date picker using the device platform controls.
    /// </summary>
    /// <returns></returns>
    public CalendarPicker NewDatePicker()
    {
      return NewPicker(true, false);
    }
    /// <summary>
    /// Create a time picker using the device platform controls.
    /// </summary>
    /// <returns></returns>
    public CalendarPicker NewTimePicker()
    {
      return NewPicker(false, true);
    }

    internal Application Application { get; private set; }

    internal CalendarPicker NewPicker(bool SetDate, bool SetTime)
    {
      return new CalendarPicker(this, SetDate, SetTime);
    }
  }

  /// <summary>
  /// Request the user to pick a date and/or time.
  /// </summary>
  public sealed class CalendarPicker
  {
    internal CalendarPicker(Calendar Calendar, bool SetDate, bool SetTime)
    {
      this.Calendar = Calendar;
      this.SetDate = SetDate;
      this.SetTime = SetTime;
    }

    /// <summary>
    /// New date time picker.
    /// </summary>
    /// <returns></returns>
    public static CalendarPicker NewDateTime() => new CalendarPicker(Inv.Application.Access().Calendar, true, true);
    /// <summary>
    /// New date picker.
    /// </summary>
    /// <returns></returns>
    public static CalendarPicker NewDate() => new CalendarPicker(Inv.Application.Access().Calendar, true, false);
    /// <summary>
    /// New time picker.
    /// </summary>
    /// <returns></returns>
    public static CalendarPicker NewTime() => new CalendarPicker(Inv.Application.Access().Calendar, false, true);

    /// <summary>
    /// If the user will be asked for a date.
    /// </summary>
    public bool SetDate { get; private set; }
    /// <summary>
    /// If the user will be asked for a time.
    /// </summary>
    public bool SetTime { get; private set; }
    /// <summary>
    /// The initial value and the value that was picked.
    /// </summary>
    public DateTime Value { get; set; }
    /// <summary>
    /// Fired when the user picks a date and/or time.
    /// </summary>
    public event Action SelectEvent;
    /// <summary>
    /// Fired when the user cancels this picker.
    /// </summary>
    public event Action CancelEvent;

    /// <summary>
    /// Show the picker and ask the user to select a date and/or time.
    /// </summary>
    public void Show()
    {
      Calendar.Application.Platform.CalendarShowPicker(this);
    }

    internal object Node { get; set; }

    internal void SelectInvoke()
    {
      if (SelectEvent != null)
        SelectEvent();
    }
    internal void CancelInvoke()
    {
      if (CancelEvent != null)
        CancelEvent();
    }

    private readonly Calendar Calendar;
  }
}
