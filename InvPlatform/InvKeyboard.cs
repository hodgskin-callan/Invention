﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Represents the hardware keyboard or bluetooth keyboard for a mobile device.
  /// </summary>
  public sealed class Keyboard
  {
    internal Keyboard(Inv.Application Application)
    {
      this.Application = Application;
      this.KeyModifier = new KeyModifier();
      this.BindingList = new Inv.DistinctList<KeyboardBinding>();
    }

    /// <summary>
    /// Current state of the key modifiers shift, control and alt.
    /// </summary>
    public Inv.KeyModifier KeyModifier { get; }

    /// <summary>
    /// Create a new keyboard binding to capture keystrokes by the user.
    /// </summary>
    /// <returns></returns>
    public KeyboardBinding NewBinding()
    {
      Application.RequireThreadAffinity();

      return new KeyboardBinding(this);
    }
    /// <summary>
    /// Programmatically invoke a key press.
    /// </summary>
    public void KeyPress(Inv.Keystroke Keystroke)
    {
      Application.RequireThreadAffinity();

      KeyPressEvent?.Invoke(Keystroke);

      foreach (var Binding in BindingList)
        Binding.KeyPressInvoke(Keystroke);
    }
    /// <summary>
    /// Programmatically invoke a key release.
    /// </summary>
    public void KeyRelease(Inv.Keystroke Keystroke)
    {
      Application.RequireThreadAffinity();

      KeyReleaseEvent?.Invoke(Keystroke);

      foreach (var Binding in BindingList)
        Binding.KeyReleaseInvoke(Keystroke);
    }
    /// <summary>
    /// Set the logical focus to a specified panel.
    /// </summary>
    /// <param name="Panel"></param>
    public void SetFocus(Inv.Panel Panel)
    {
      this.Focus = new KeyboardFocus(Panel?.Control);
    }

    // internal events only, use the Bindings.
    internal KeyboardFocus Focus { get; set; }
    internal event Action KeyModifierEvent;
    internal event Action<Inv.Keystroke> KeyPressEvent;
    internal event Action<Inv.Keystroke> KeyReleaseEvent;

    internal void Capture(KeyboardBinding Binding)
    {
      Application.RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!BindingList.Contains(Binding), "Binding must be released before it can be captured again.");

      if (!BindingList.Contains(Binding))
        BindingList.Add(Binding);
    }
    internal void Release(KeyboardBinding Binding)
    {
      Application.RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(BindingList.Contains(Binding), "Binding must be captured before it can be released.");

      BindingList.Remove(Binding);
    }
    internal void CheckModifier(KeyModifier UpdateModifier)
    {
      Application.RequireThreadAffinity();

      if (!KeyModifier.IsEqual(UpdateModifier))
      {
        KeyModifier.SetFlags(UpdateModifier.GetFlags());

        KeyModifierEvent?.Invoke();

        foreach (var Binding in BindingList)
          Binding.KeyModifierInvoke();
      }
    }

    private readonly Inv.Application Application;
    private readonly Inv.DistinctList<KeyboardBinding> BindingList;
  }

  internal sealed class KeyboardFocus
  {
    public KeyboardFocus(Inv.Control Control)
    {
      this.Control = Control;
    }

    public Inv.Control Control { get; }
  }

  /// <summary>
  /// This is a binding for responding to keystrokes by the user.
  /// </summary>
  public sealed class KeyboardBinding
  {
    internal KeyboardBinding(Inv.Keyboard Keyboard)
    {
      this.Keyboard = Keyboard;
    }

    /// <summary>
    /// Produce a new keyboard binding with an optional delegate.
    /// </summary>
    /// <returns></returns>
    public static KeyboardBinding New() => Inv.Application.Access().Keyboard.NewBinding();

    /// <summary>
    /// This event is fired when a key modifier is changed (shift, control or alt).
    /// </summary>
    public event Action KeyModifierEvent;
    /// <summary>
    /// This event is fired when a key is pressed on a non-virtual keyboard including bluetooth keyboards attached to devices.
    /// </summary>
    public event Action<Inv.Keystroke> KeyPressEvent;
    /// <summary>
    /// This event is fired when a key is released on a non-virtual keyboard including bluetooth keyboards attached to devices.
    /// </summary>
    public event Action<Inv.Keystroke> KeyReleaseEvent;

    /// <summary>
    /// Bind an action to the Key without any modifier.
    /// </summary>
    /// <param name="Key"></param>
    /// <param name="Action"></param>
    public void Bind(Inv.Key Key, Action Action)
    {
      Bind(new Inv.Keystroke(Key), Action);
    }
    /// <summary>
    /// Bind an action to the Key with a Modifier.
    /// </summary>
    /// <param name="KeyModifier"></param>
    /// <param name="Key"></param>
    /// <param name="Action"></param>
    public void Bind(Inv.KeyModifier KeyModifier, Inv.Key Key, Action Action)
    {
      Bind(new Inv.Keystroke(Key, KeyModifier), Action);
    }
    /// <summary>
    /// Bind an action to the Keystroke.
    /// </summary>
    /// <param name="Keystroke"></param>
    /// <param name="Action"></param>
    public void Bind(Inv.Keystroke Keystroke, Action Action)
    {
      BindAction += (Input) =>
      {
        if (Input.IsEqual(Keystroke))
          Action?.Invoke();
      };
    }
    /// <summary>
    /// Bind an action to the Ctrl+Key.
    /// </summary>
    /// <param name="Key"></param>
    /// <param name="Action"></param>
    public void BindCtrl(Inv.Key Key, Action Action)
    {
      BindAction += (Input) =>
      {
        if (Input.Modifier.IsCtrl && Input.Key == Key)
          Action?.Invoke();
      };
    }
    /// <summary>
    /// Bind an action to the Alt+Key.
    /// </summary>
    /// <param name="Key"></param>
    /// <param name="Action"></param>
    public void BindAlt(Inv.Key Key, Action Action)
    {
      BindAction += (Input) =>
      {
        if (Input.Modifier.IsAlt && Input.Key == Key)
          Action?.Invoke();
      };
    }
    /// <summary>
    /// Bind an action to the Shift+Key.
    /// </summary>
    /// <param name="Key"></param>
    /// <param name="Action"></param>
    public void BindShift(Inv.Key Key, Action Action)
    {
      BindAction += (Input) =>
      {
        if (Input.Modifier.IsShift && Input.Key == Key)
          Action?.Invoke();
      };
    }
    /// <summary>
    /// Start receiving keystrokes for this binding.
    /// </summary>
    public void Capture()
    {
      Keyboard.Capture(this);
    }
    /// <summary>
    /// Stop receiving keystrokes for this binding.
    /// </summary>
    public void Release()
    {
      Keyboard.Release(this);
    }

    internal void KeyModifierInvoke()
    {
      KeyModifierEvent?.Invoke();
    }
    internal void KeyPressInvoke(Inv.Keystroke Keystroke)
    {
      KeyPressEvent?.Invoke(Keystroke);

      BindAction?.Invoke(Keystroke);
    }
    internal void KeyReleaseInvoke(Inv.Keystroke Keystroke)
    {
      KeyReleaseEvent?.Invoke(Keystroke);

      //BindAction?.Invoke(Keystroke);
    }

    private readonly Inv.Keyboard Keyboard;
    private Action<Keystroke> BindAction;
  }

  /// <summary>
  /// Cross-platform key enumeration.
  /// </summary>
  public enum Key
  {
    #region Keys.
    /// <summary>
    /// Letter A.
    /// </summary>
    A,
    /// <summary>
    /// Letter B.
    /// </summary>
    B,
    /// <summary>
    /// Letter C.
    /// </summary>
    C,
    /// <summary>
    /// Letter D.
    /// </summary>
    D,
    /// <summary>
    /// Letter E.
    /// </summary>
    E,
    /// <summary>
    /// Letter F.
    /// </summary>
    F,
    /// <summary>
    /// Letter G.
    /// </summary>
    G,
    /// <summary>
    /// Letter H.
    /// </summary>
    H,
    /// <summary>
    /// Letter I.
    /// </summary>
    I,
    /// <summary>
    /// Letter J.
    /// </summary>
    J,
    /// <summary>
    /// Letter K.
    /// </summary>
    K,
    /// <summary>
    /// Letter L.
    /// </summary>
    L,
    /// <summary>
    /// Letter M.
    /// </summary>
    M,
    /// <summary>
    /// Letter N.
    /// </summary>
    N,
    /// <summary>
    /// Letter O.
    /// </summary>
    O,
    /// <summary>
    /// Letter P.
    /// </summary>
    P,
    /// <summary>
    /// Letter Q.
    /// </summary>
    Q,
    /// <summary>
    /// Letter R.
    /// </summary>
    R,
    /// <summary>
    /// Letter S.
    /// </summary>
    S,
    /// <summary>
    /// Letter T.
    /// </summary>
    T,
    /// <summary>
    /// Letter U.
    /// </summary>
    U,
    /// <summary>
    /// Letter V.
    /// </summary>
    V,
    /// <summary>
    /// Letter W.
    /// </summary>
    W,
    /// <summary>
    /// Letter X.
    /// </summary>
    X,
    /// <summary>
    /// Letter Y.
    /// </summary>
    Y,
    /// <summary>
    /// Letter Z.
    /// </summary>
    Z,
    /// <summary>
    /// Number 0.
    /// </summary>
    n0,
    /// <summary>
    /// Number 1.
    /// </summary>
    n1,
    /// <summary>
    /// Number 2.
    /// </summary>
    n2,
    /// <summary>
    /// Number 3.
    /// </summary>
    n3,
    /// <summary>
    /// Number 4.
    /// </summary>
    n4,
    /// <summary>
    /// Number 5.
    /// </summary>
    n5,
    /// <summary>
    /// Number 6.
    /// </summary>
    n6,
    /// <summary>
    /// Number 7.
    /// </summary>
    n7,
    /// <summary>
    /// Number 8.
    /// </summary>
    n8,
    /// <summary>
    /// Number 9.
    /// </summary>
    n9,
    /// <summary>
    /// Function Key 1.
    /// </summary>
    F1,
    /// <summary>
    /// Function Key 2.
    /// </summary>
    F2,
    /// <summary>
    /// Function Key 3.
    /// </summary>
    F3,
    /// <summary>
    /// Function Key 4.
    /// </summary>
    F4,
    /// <summary>
    /// Function Key 5.
    /// </summary>
    F5,
    /// <summary>
    /// Function Key 6.
    /// </summary>
    F6,
    /// <summary>
    /// Function Key 7.
    /// </summary>
    F7,
    /// <summary>
    /// Function Key 8.
    /// </summary>
    F8,
    /// <summary>
    /// Function Key 9.
    /// </summary>
    F9,
    /// <summary>
    /// Function Key 10.
    /// </summary>
    F10,
    /// <summary>
    /// Function Key 11.
    /// </summary>
    F11,
    /// <summary>
    /// Function Key 12.
    /// </summary>
    F12,
    /// <summary>
    /// Period (.)
    /// </summary>
    Period,
    /// <summary>
    /// Quote (')
    /// </summary>
    Quote,
    /// <summary>
    /// Grave (`)
    /// </summary>
    Grave,
    /// <summary>
    /// Asterisk (*)
    /// </summary>
    Asterisk,
    /// <summary>
    /// Comma (,)
    /// </summary>
    Comma,
    /// <summary>
    /// Escape (Esc)
    /// </summary>
    Escape,
    /// <summary>
    /// Enter
    /// </summary>
    Enter,
    /// <summary>
    /// Tab
    /// </summary>
    Tab,
    /// <summary>
    /// Space
    /// </summary>
    Space,
    /// <summary>
    /// Insert (Ins)
    /// </summary>
    Insert,
    /// <summary>
    /// Delete (Del)
    /// </summary>
    Delete,
    /// <summary>
    /// Arrow Up
    /// </summary>
    Up,
    /// <summary>
    /// Arrow Down
    /// </summary>
    Down,
    /// <summary>
    /// Arrow Left
    /// </summary>
    Left,
    /// <summary>
    /// Arrow Right
    /// </summary>
    Right,
    /// <summary>
    /// Home
    /// </summary>
    Home,
    /// <summary>
    /// Page Up (PgUp)
    /// </summary>
    PageUp,
    /// <summary>
    /// End
    /// </summary>
    End,
    /// <summary>
    /// Page Down (PgDn)
    /// </summary>
    PageDown,
    /// <summary>
    /// Clear
    /// </summary>
    Clear,
    /// <summary>
    /// Slash (/)
    /// </summary>
    Slash,
    /// <summary>
    /// Backslash (\)
    /// </summary>
    Backslash,
    /// <summary>
    /// Plus (+)
    /// </summary>
    Plus,
    /// <summary>
    /// Minus (-)
    /// </summary>
    Minus,
    /// <summary>
    /// Colon (:)
    /// </summary>
    Colon,
    /// <summary>
    /// Backspace
    /// </summary>
    Backspace,
    /// <summary>
    /// Left shift
    /// </summary>
    LeftShift,
    /// <summary>
    /// Right shift
    /// </summary>
    RightShift,
    /// <summary>
    /// Left alt
    /// </summary>
    LeftAlt,
    /// <summary>
    /// Right alt
    /// </summary>
    RightAlt,
    /// <summary>
    /// Left ctrl
    /// </summary>
    LeftCtrl,
    /// <summary>
    /// Right ctrl
    /// </summary>
    RightCtrl,
    /// <summary>
    /// Open bracket
    /// </summary>
    OpenBracket,
    /// <summary>
    /// Close bracket
    /// </summary>
    CloseBracket
    #endregion
  }

  /// <summary>
  /// Extension methods for the Key.
  /// </summary>
  public static class KeyHelper
  {
    public static bool IsLetter(this Inv.Key Key) => Key >= Inv.Key.A && Key <= Inv.Key.Z;
  }

  internal enum KeyModifierFlags : byte
  {
    None = 0x00,
    LeftShift = 0x01,
    RightShift = 0x02,
    LeftAlt = 0x04,
    RightAlt = 0x08,
    LeftCtrl = 0x10,
    RightCtrl = 0x20
  }

  /// <summary>
  /// Cross-platform key modifier.
  /// </summary>
  public sealed class KeyModifier
  {
    internal KeyModifier(KeyModifierFlags Flags)
    {
      this.Flags = Flags;
    }
    internal KeyModifier()
    {
    }

    /// <summary>
    /// Is there no modifier keys pressed.
    /// </summary>
    public bool IsNone
    {
      get => Flags == KeyModifierFlags.None;
    }
    /// <summary>
    /// Is the left shift pressed.
    /// </summary>
    public bool IsLeftShift
    {
      get => (Flags & KeyModifierFlags.LeftShift) > 0;
      set
      {
        if (value)
          Flags |= KeyModifierFlags.LeftShift;
        else
          Flags &= ~KeyModifierFlags.LeftShift;
      }
    }
    /// <summary>
    /// Is the right shift pressed.
    /// </summary>
    public bool IsRightShift
    {
      get => (Flags & KeyModifierFlags.RightShift) > 0;
      set
      {
        if (value)
          Flags |= KeyModifierFlags.RightShift;
        else
          Flags &= ~KeyModifierFlags.RightShift;
      }
    }
    /// <summary>
    /// Is the shift pressed.
    /// </summary>
    public bool IsShift
    {
      get => (Flags & (KeyModifierFlags.LeftShift | KeyModifierFlags.RightShift)) > 0;
      set
      {
        if (value)
          Flags |= KeyModifierFlags.LeftShift | KeyModifierFlags.RightShift;
        else
          Flags &= ~KeyModifierFlags.LeftShift & ~KeyModifierFlags.RightShift;
      }
    }
    /// <summary>
    /// Is the left alt pressed.
    /// </summary>
    public bool IsLeftAlt
    {
      get => (Flags & KeyModifierFlags.LeftAlt) > 0;
      set
      {
        if (value)
          Flags |= KeyModifierFlags.LeftAlt;
        else
          Flags &= ~KeyModifierFlags.LeftAlt;
      }
    }
    /// <summary>
    /// Is the right alt pressed.
    /// </summary>
    public bool IsRightAlt
    {
      get => (Flags & KeyModifierFlags.RightAlt) > 0;
      set
      {
        if (value)
          Flags |= KeyModifierFlags.RightAlt;
        else
          Flags &= ~KeyModifierFlags.RightAlt;
      }
    }
    /// <summary>
    /// Is the alt pressed.
    /// </summary>
    public bool IsAlt
    {
      get => (Flags & (KeyModifierFlags.LeftAlt | KeyModifierFlags.RightAlt)) > 0;
      set
      {
        if (value)
          Flags |= KeyModifierFlags.LeftAlt | KeyModifierFlags.RightAlt;
        else
          Flags &= ~KeyModifierFlags.LeftAlt & ~KeyModifierFlags.RightAlt;
      }
    }
    /// <summary>
    /// Is the left ctrl pressed.
    /// </summary>
    public bool IsLeftCtrl
    {
      get => (Flags & KeyModifierFlags.LeftCtrl) > 0;
      set
      {
        if (value)
          Flags |= KeyModifierFlags.LeftCtrl;
        else
          Flags &= ~KeyModifierFlags.LeftCtrl;
      }
    }
    /// <summary>
    /// Is the right ctrl pressed.
    /// </summary>
    public bool IsRightCtrl
    {
      get => (Flags & KeyModifierFlags.RightCtrl) > 0;
      set
      {
        if (value)
          Flags |= KeyModifierFlags.RightCtrl;
        else
          Flags &= ~KeyModifierFlags.RightCtrl;
      }
    }
    /// <summary>
    /// Is the ctrl pressed.
    /// </summary>
    public bool IsCtrl
    {
      get => (Flags & (KeyModifierFlags.LeftCtrl | KeyModifierFlags.RightCtrl)) > 0;
      set
      {
        if (value)
          Flags |= KeyModifierFlags.LeftCtrl | KeyModifierFlags.RightCtrl;
        else
          Flags &= ~KeyModifierFlags.LeftCtrl & ~KeyModifierFlags.RightCtrl;
      }
    }

    /// <summary>
    /// Render the key modifier to a string.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      var Builder = new StringBuilder();

      if (IsLeftShift && IsRightShift)
        Builder.Append("shift + ");
      else if (IsLeftShift)
        Builder.Append("left shift + ");
      else if (IsRightShift)
        Builder.Append("right shift + ");

      if (IsLeftCtrl && IsRightCtrl)
        Builder.Append("ctrl + ");
      else if (IsLeftCtrl)
        Builder.Append("left ctrl + ");
      else if (IsRightCtrl)
        Builder.Append("right ctrl + ");

      if (IsLeftAlt && IsRightAlt)
        Builder.Append("alt + ");
      else if (IsLeftAlt)
        Builder.Append("left alt + ");
      else if (IsRightAlt)
        Builder.Append("right alt + ");

      if (Builder.Length == 0)
        return "";
      else
        return Builder.ToString().Substring(0, Builder.Length - 3);
    }

    /// <summary>
    /// Equality comparison for two key modifiers.
    /// </summary>
    /// <param name="KeyModifier"></param>
    /// <returns></returns>
    public bool IsEqual(KeyModifier KeyModifier)
    {
      return Flags == KeyModifier.Flags;
    }

    internal KeyModifier Clone() => new KeyModifier(GetFlags());
    internal KeyModifierFlags GetFlags()
    {
      return Flags;
    }
    internal void SetFlags(KeyModifierFlags Flags)
    {
      this.Flags = Flags;
    }

    private KeyModifierFlags Flags;
  }

  /// <summary>
  /// The keystroke is the <see cref="Key"/> that was pressed and the <see cref="Modifier"/>.
  /// </summary>
  public sealed class Keystroke
  {
    /// <summary>
    /// Create a new keystroke of a Key with no Modifier.
    /// </summary>
    /// <param name="Key"></param>
    public Keystroke(Inv.Key Key)
      : this(Key, new KeyModifier())
    {
    }
    /// <summary>
    /// Create a new keystroke with a Key and Modifier.
    /// </summary>
    /// <param name="Key"></param>
    /// <param name="Modifier"></param>
    public Keystroke(Inv.Key Key, Inv.KeyModifier Modifier)
    {
      this.Key = Key;
      this.Modifier = Modifier;
    }

    /// <summary>
    /// The key pressed.
    /// </summary>
    public Key Key { get; }
    /// <summary>
    /// Any active modifiers such as shift, ctrl or alt.
    /// </summary>
    public KeyModifier Modifier { get; }

    /// <summary>
    /// Indicates whether this keystroke is logically equivalent to another keystroke.
    /// </summary>
    /// <param name="OtherKeystroke">The keystroke to compare this keystroke to.</param>
    /// <returns></returns>
    public bool IsEqual(Inv.Keystroke OtherKeystroke)
    {
      if (OtherKeystroke == null)
        return false;

      return Key == OtherKeystroke.Key && Modifier.IsEqual(OtherKeystroke.Modifier);
    }

    /// <summary>
    /// Render the keystroke to a string.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      var Qualifier = Modifier.ToString();

      if (Qualifier.Length > 0)
        return Qualifier + " + " + Key.ToString();
      else
        return Key.ToString();
    }
  }
}
