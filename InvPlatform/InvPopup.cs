﻿using System;
using System.Collections.Generic;
using System.Text;
using Inv.Support;

namespace Inv
{
  public sealed class Popup
  {
    internal Popup(Inv.Window Window)
    {
      this.Window = Window;
      this.Position = new Inv.PopupPosition(this);
    }

    public static Popup New() => new Popup(Inv.Application.Access().Window);

    /// <summary>
    /// Position of the popup located relative to a target panel.
    /// </summary>
    public Inv.PopupPosition Position { get; }
    /// <summary>
    /// Handle to be notified when the popup is shown.
    /// </summary>
    public event Action ShowEvent
    {
      add
      {
        Window.RequireThreadAffinity();
        this.ShowDelegate += value;
        ChangeInvoke();
      }
      remove
      {
        Window.RequireThreadAffinity();
        this.ShowDelegate -= value;
        ChangeInvoke();
      }
    }
    /// <summary>
    /// Returns true if the ShowEvent is handled on this popup.
    /// </summary>
    public bool HasShow()
    {
      return ShowDelegate != null;
    }
    /// <summary>
    /// Handle to be notified when the popup is hidden.
    /// </summary>
    public event Action HideEvent
    {
      add
      {
        Window.RequireThreadAffinity();
        this.HideDelegate += value;
        ChangeInvoke();
      }
      remove
      {
        Window.RequireThreadAffinity();
        this.HideDelegate -= value;
        ChangeInvoke();
      }
    }
    /// <summary>
    /// Returns true if the HideEvent is handled on this popup.
    /// </summary>
    public bool HasHide()
    {
      return HideDelegate != null;
    }
    /// <summary>
    /// Returns true if the popup is currently shown.
    /// </summary>
    public bool IsActive
    {
      get
      {
        Window.RequireThreadAffinity();

        return ActiveField;
      }
    }
    /// <summary>
    /// Content of the popup.
    /// </summary>
    public Inv.Panel Content
    {
      get
      {
        Window.RequireThreadAffinity();

        return ContentField;
      }
      set
      {
        Window.RequireThreadAffinity();

        if (ContentField != value)
        {
          this.ContentField = value;
          ChangeInvoke();
        }
      }
    }

    public void Show()
    {
      Window.Application.Platform.WindowShowPopup(this);
    }
    public void Hide()
    {
      Window.Application.Platform.WindowHidePopup(this);
    }

    public override string ToString()
    {
      return Content?.ToString() ?? "null";
    }

    internal object Node { get; set; }

    internal Action ChangeAction;

    internal void ShowInvoke()
    {
      Window.RequireThreadAffinity();

      this.ActiveField = true;

      ShowDelegate?.Invoke();
    }
    internal void HideInvoke()
    {
      Window.RequireThreadAffinity();

      this.ActiveField = false;

      HideDelegate?.Invoke();
    }
    internal void ChangeInvoke()
    {
      ChangeAction?.Invoke();
    }

    private readonly Inv.Window Window;
    private Action ShowDelegate;
    private Action HideDelegate;
    private Inv.Panel ContentField;
    private bool ActiveField;
  }

  public enum PopupLocation
  {
    Default,
    Above,
    Below,
    LeftOf,
    RightOf,
    CenterOver,
    Pointer
  }

  public sealed class PopupPosition
  {
    internal PopupPosition(Inv.Popup Popup)
    {
      this.Popup = Popup;
    }

    public PopupLocation Location { get; private set; }
    public Inv.Panel Target { get; private set; }

    public void Above(Inv.Panel Target)
    {
      Set(PopupLocation.Above, Target);
    }
    public void Below(Inv.Panel Target)
    {
      Set(PopupLocation.Below, Target);
    }
    public void LeftOf(Inv.Panel Target)
    {
      Set(PopupLocation.LeftOf, Target);
    }
    public void RightOf(Inv.Panel Target)
    {
      Set(PopupLocation.RightOf, Target);
    }
    public void CenterOver(Inv.Panel Target)
    {
      Set(PopupLocation.CenterOver, Target);
    }
    public void Pointer(Inv.Panel Target)
    {
      Set(PopupLocation.Pointer, Target);
    }
    public void Set(Inv.PopupLocation Location, Inv.Panel Target)
    {
      this.Location = Location;
      this.Target = Target;

      Popup.ChangeInvoke();
    }

    private readonly Inv.Popup Popup;
  }
}