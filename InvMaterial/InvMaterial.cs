﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Inv.Support;

// Invention implementation of Material Design guidelines found at https://material.io/.

namespace Inv.Material
{
  public sealed class Space : Inv.Panel<Inv.Overlay>
  {
    public Space()
    {
      this.PlaneList = new Inv.DistinctList<Inv.Material.Plane>();

      this.Base = Inv.Overlay.New();
    }

    public Inv.Size Size => Base.Size;
    public Inv.Alignment Alignment => Base.Alignment;

    public Inv.Material.Sheet NewSheet()
    {
      return new Inv.Material.Sheet(this);
    }
    public AlertDialog NewAlertDialog()
    {
      var Result = new AlertDialog(this);
      Result.ShowEvent += () => AddPlane(Result);
      Result.HideEvent += () => RemovePlane(Result);
      return Result;
    }
    public SimpleDialog NewSimpleDialog()
    {
      var Result = new SimpleDialog(this);
      Result.ShowEvent += () => AddPlane(Result);
      Result.HideEvent += () => RemovePlane(Result);
      return Result;
    }
    public ConfirmationDialog NewConfirmationDialog()
    {
      var Result = new ConfirmationDialog(this);
      Result.ShowEvent += () => AddPlane(Result);
      Result.HideEvent += () => RemovePlane(Result);
      return Result;
    }
    public FullScreenDialog NewFullScreenDialog()
    {
      var Result = new FullScreenDialog(this);
      Result.ShowEvent += () => AddPlane(Result);
      Result.HideEvent += () => RemovePlane(Result);
      return Result;
    }
    public PropertySheet NewPropertySheet()
    {
      return new PropertySheet(this);
    }
    public RadioSelectDialog<T> NewSelectDialog<T>()
    {
      return new RadioSelectDialog<T>(this);
    }
    public FloatingActionContainer NewFloatingActionContainer() => new FloatingActionContainer(this);
    public bool HasActivePlane()
    {
      return PlaneList.Any();
    }
    public void HideActivePlane()
    {
      if (PlaneList.Any())
      {
        // If the entry successfully hides, then it will be removed from ModalEntryList.
        var Plane = PlaneList.LastOrDefault();

        if (Plane != null)
          Plane.Hide();
      }
    }
    public bool HasPanel(Inv.Panel Panel)
    {
      return Base.Panels.Contains(Panel);
    }
    public bool IsActivePanel(Inv.Panel Panel) => Base.Panels.LastOrDefault() == Panel;
    public void ShowPanel(Inv.Panel Panel)
    {
      Base.AddPanel(Panel);
    }
    public void HidePanel(Inv.Panel Panel)
    {
      Base.RemovePanel(Panel);
    }

    internal void AddPlane(Inv.Material.Plane Plane)
    {
      PlaneList.Add(Plane);
    }
    internal void RemovePlane(Inv.Material.Plane Plane)
    {
      var LastPlane = PlaneList.LastOrDefault();

      Inv.Assert.Check(LastPlane != null, "Attempted to remove plane but no plane is active.");
      Inv.Assert.Check(LastPlane != null && Plane == LastPlane, "Attempted to remove plane but a different plane was active");

      if (LastPlane != null)
        PlaneList.Remove(LastPlane);
    }
    internal bool IsActivePlane(Plane Plane)
    {
      var ActivePlane = PlaneList.LastOrDefault();
      return ActivePlane != null && ActivePlane == Plane;
    }

    private readonly Inv.DistinctList<Plane> PlaneList;
  }

  public sealed class Plane
  {
    internal void Hide()
    {
      HideEvent?.Invoke();
    }

    internal event Action HideEvent;
  }

  public sealed class Sheet
  {
    internal Sheet(Inv.Material.Space Space)
    {
      this.Space = Space;

      this.Frame = Inv.Frame.New();
      Frame.Alignment.Stretch();
    }

    public Inv.Panel Content
    {
      get => Frame.Content;
      set => Frame.Content = value;
    }
    public Inv.Background Background => Frame.Background;

    public void Show()
    {
      Space.ShowPanel(Frame);
    }
    public void Hide()
    {
      Space.HidePanel(Frame);
    }

    private readonly Inv.Material.Space Space;
    private readonly Inv.Frame Frame;
  }

  public sealed class Scaffold : Inv.Panel<Inv.Overlay>
  {
    public Scaffold()
    {
      this.Base = Inv.Overlay.New();

      this.Layout = new Inv.Material.AppLayout();
      Base.AddPanel(Layout);

      this.Scrim = new Inv.Material.Scrim();
      Base.AddPanel(Scrim);
      Scrim.Visibility.Collapse();
      Scrim.SingleTapEvent += () => Hide(true);

      this.BackgroundFrame = Inv.Frame.New();
      Base.AddPanel(BackgroundFrame);
      BackgroundFrame.Background.Colour = null;
      BackgroundFrame.Alignment.Stretch();
      BackgroundFrame.Margin.Set(0, 0, 56, 0); // Mobile only.

      this.Drawer = new Drawer();
      Drawer.Elevation.Set(4);
      Drawer.Alignment.StretchLeft();
      Drawer.HideEvent += () => Hide(true);
    }

    public AppBar AppBar => Layout.AppBar;
    public Drawer Drawer { get; }
    public Inv.Material.FloatingActionContainer FloatingActionContainer => Layout.FloatingActionContainer;

    public void Clear()
    {
      Layout.Clear();
    }
    public void ShowDrawer()
    {
      Scrim.Visibility.Show();
      BackgroundFrame.Transition(Drawer).CarouselPrevious();

      Layout.FloatingActionContainer.Visibility.Collapse();
    }
    public void HideDrawer()
    {
      Hide(true);
    }
    public bool IsDrawerShown() => Scrim.Visibility.Get();
    public Inv.Transition TransitionContent(Inv.Panel Content) => Layout.TransitionContent(Content);

    private void Hide(bool IsAnimated)
    {
      var Transition = BackgroundFrame.Transition(null);
      if (IsAnimated)
        Transition.CarouselNext();

      Scrim.Visibility.Collapse();

      Layout.FloatingActionContainer.Visibility.Show();
    }

    private readonly Inv.Material.AppLayout Layout;
    private readonly Inv.Material.Scrim Scrim;
    private readonly Inv.Frame BackgroundFrame;
  }

  public sealed class Drawer : Inv.Panel<Inv.Scroll>
  {
    public Drawer()
    {
      this.Base = Inv.Scroll.NewVertical();
      Base.Size.SetWidth(312);

      this.Stack = Inv.Stack.NewVertical();
      Base.Content = Stack;
      Stack.Background.In(Inv.Theme.BackgroundColour);
      Stack.Padding.Set(0, 8, 0, 0);

      this.HeaderOverlay = Inv.Overlay.New();
      Stack.AddPanel(HeaderOverlay);
      HeaderOverlay.Margin.Set(16, 0, 0, 0);
      HeaderOverlay.Visibility.Collapse();

      var TitleFrame = Inv.Frame.New();
      HeaderOverlay.AddPanel(TitleFrame);
      TitleFrame.Size.SetHeight(56);
      TitleFrame.Alignment.TopStretch();

      this.TitleLabel = Inv.Label.New();
      TitleFrame.Content = TitleLabel;
      TitleLabel.Margin.Set(0, 8, 0, 0);
      TitleLabel.Font.Custom(21).Medium().In(Inv.Theme.OnBackgroundColour);
      TitleLabel.Alignment.TopLeft();
      TitleLabel.LineWrapping = false;

      this.SubtextFrame = Inv.Frame.New();
      HeaderOverlay.AddPanel(SubtextFrame);
      SubtextFrame.Size.SetHeight(66);
      SubtextFrame.Visibility.Collapse();

      this.SubtextLabel = Inv.Label.New();
      SubtextFrame.Content = SubtextLabel;
      SubtextLabel.Margin.Set(0, 0, 0, 11);
      SubtextLabel.Font.Custom(14).Regular().In(Palette.Grey600);
      SubtextLabel.Alignment.BottomLeft();
      SubtextLabel.LineWrapping = false;

      this.GroupList = new Inv.DistinctList<DrawerGroup>();
      this.DividerList = new Inv.DistinctList<Divider>();
    }

    public string Title
    {
      get => TitleLabel.Text;
      set
      {
        TitleLabel.Text = value;
        HeaderOverlay.Visibility.Set(value != null);

        if (HeaderOverlay.Visibility.Get())
        {
          SubtextLabel.Text = Subtext;
          RefreshHeader();
        }
      }
    }
    public string Subtext
    {
      get => SubtextLabel.Text;
      set
      {
        SubtextLabel.Text = value;

        if (HeaderOverlay.Visibility.Get())
          RefreshHeader();
      }
    }
    public Inv.Alignment Alignment => Base.Alignment;
    public event Action HideEvent;

    public DrawerGroup AddGroup(string Subtitle = null)
    {
      if (GroupList.Any())
      {
        var Divider = new Divider(DividerOrientation.Vertical);
        Stack.AddPanel(Divider);
        Divider.Margin.Set(0, 0, 0, 8);
        DividerList.Add(Divider);
      }

      var Result = new DrawerGroup(this);
      GroupList.Add(Result);

      if (Subtitle != null)
        Result.Subtitle = Subtitle;

      Stack.AddPanel(Result);

      return Result;
    }
    public void RemoveGroups()
    {
      foreach (var ItemGroup in GroupList)
        Stack.RemovePanel(ItemGroup);

      foreach (var Divider in DividerList)
        Stack.RemovePanel(Divider);

      GroupList.Clear();
      DividerList.Clear();
    }
    public void ScrollToItem(DrawerItem Item)
    {
      // TODO: convert drawer to Flow to allow this.
    }

    internal Inv.Elevation Elevation => Base.Elevation;

    internal void RefreshHeader() => SubtextFrame.Visibility.Set(!string.IsNullOrWhiteSpace(Subtext));
    internal void Hide() => HideEvent?.Invoke();
    internal void Activate(DrawerItem Item)
    {
      foreach (var Group in GroupList)
      {
        foreach (var GroupItem in Group.GetItems())
        {
          if (GroupItem != Item)
            GroupItem.IsActive = false;
        }
      }
    }

    private readonly Inv.Stack Stack;
    private readonly Inv.Overlay HeaderOverlay;
    private readonly Inv.Label TitleLabel;
    private readonly Inv.Frame SubtextFrame;
    private Inv.Label SubtextLabel;
    private readonly Inv.DistinctList<Inv.Material.DrawerGroup> GroupList;
    private readonly Inv.DistinctList<Divider> DividerList;
  }

  public sealed class DrawerGroup : Inv.Panel<Inv.Frame>
  {
    internal DrawerGroup(Drawer Drawer)
    {
      this.Drawer = Drawer;

      this.Base = Inv.Frame.New();
      Base.Margin.Set(0, 0, 0, 8);

      this.Stack = Inv.Stack.NewVertical();
      Base.Content = Stack;

      this.SubtitleLabel = Inv.Label.New();
      Stack.AddPanel(SubtitleLabel);
      SubtitleLabel.Font.Custom(14).Regular().In(Palette.Grey600);
      SubtitleLabel.Margin.Set(16, 5, 16, 4);
      SubtitleLabel.Visibility.Collapse();

      this.ItemList = new Inv.DistinctList<DrawerItem>();
    }

    public string Subtitle
    {
      set
      {
        SubtitleLabel.Text = value;
        SubtitleLabel.Visibility.Set(!string.IsNullOrWhiteSpace(value));
      }
    }

    public DrawerItem AddItem(bool AutoClose = false)
    {
      var Result = new DrawerItem(Drawer);
      Result.AutoClose(AutoClose);

      ItemList.Add(Result);
      Stack.AddPanel(Result);

      Result.ActivatedEvent += () =>
      {
        Drawer.Activate(Result);
      };

      return Result;
    }

    internal IEnumerable<DrawerItem> GetItems() => ItemList;

    private readonly Drawer Drawer;
    private readonly Inv.Stack Stack;
    private readonly Inv.Label SubtitleLabel;
    private readonly Inv.DistinctList<DrawerItem> ItemList;
  }

  public sealed class DrawerItem : Inv.Panel<Inv.Frame>
  {
    internal DrawerItem(Drawer Drawer)
    {
      this.Base = Inv.Frame.New();
      Base.Size.SetHeight(44);
      Base.Margin.Set(8, 2);

      this.Button = Inv.Button.NewStark();
      Base.Content = Button;
      Button.Corner.Set(4);
      Button.OverEvent += () => RefreshButton();
      Button.AwayEvent += () => RefreshButton();
      Button.SingleTapEvent += () =>
      {
        if (SingleTapEvent != null)
        {
          if (IsAutoClose)
          {
            Drawer.Hide();
          }
          else
          {
            IsActive = true;

            ActivatedEvent?.Invoke();
          }

          SingleTapEvent.Invoke();
        }
      };

      var Dock = Inv.Dock.NewHorizontal();
      Button.Content = Dock;

      this.Graphic = Inv.Graphic.New();
      Dock.AddHeader(Graphic);
      Graphic.Margin.Set(8, 0, 24, 0);
      Graphic.Size.Set(24);
      Graphic.Alignment.CenterStretch();
      Graphic.Visibility.Collapse();

      this.CaptionLabel = Inv.Label.New();
      Dock.AddClient(CaptionLabel);
      CaptionLabel.Margin.Set(8, 0, 0, 0);
      CaptionLabel.Alignment.CenterStretch();
      CaptionLabel.LineWrapping = false;
      CaptionLabel.Font.Custom(14).Medium().In(Inv.Theme.OnBackgroundColour);
    }

    public string Caption
    {
      get => CaptionLabel.Text;
      set => CaptionLabel.Text = value;
    }
    public Inv.Image Image
    {
      get => ImageField;
      set
      {
        ImageField = value;

        Refresh();
      }
    }
    public bool IsActive
    {
      get => IsActiveField;
      set
      {
        IsActiveField = value;

        Refresh();
      }
    }
    public bool IsEnabled
    {
      get => Button.IsEnabled;
      set => Button.IsEnabled = value;
    }
    public Inv.Tooltip Tooltip => Button.Tooltip;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action SingleTapEvent;

    internal event Action ActivatedEvent;
    internal void AutoClose(bool Value = true) => this.IsAutoClose = Value;

    private void Refresh()
    {
      Graphic.Visibility.Set(ImageField != null);
      if (Graphic.Visibility.Get())
      {
        var ImageColour = IsActive ? Inv.Material.Theme.PrimaryColour : Palette.Grey600;
        Graphic.Image = Inv.Application.Access().Graphics.Tint(ImageField, ImageColour);
      }

      RefreshButton();
    }
    private void RefreshButton()
    {
      Button.Background.In(IsActive ? Inv.Material.Theme.PrimaryColour.AdjustAlpha(30) : Button.IsOver && SingleTapEvent != null ? Inv.Theme.SmokeColour : Inv.Theme.BackgroundColour);
      CaptionLabel.Font.In(IsActive ? Inv.Material.Theme.PrimaryColour : Inv.Theme.OnBackgroundColour);
    }

    private readonly Inv.Button Button;
    private readonly Inv.Graphic Graphic;
    private readonly Inv.Label CaptionLabel;
    private Inv.Image ImageField;
    private bool IsActiveField;
    private bool IsAutoClose;
  }

  public sealed class Scrim : Inv.Panel<Inv.Button>
  {
    public Scrim()
    {
      this.Base = Inv.Button.NewStark();
      Base.Background.In(Inv.Colour.Black.AdjustAlpha(56));
      Base.Visibility.Collapse();
      Base.SingleTapEvent += HandleSingleTap;
    }

    public Inv.Visibility Visibility => Base.Visibility;
    public event Action SingleTapEvent;

    public void Enable()
    {
      if (!Base.HasSingleTap())
        Base.SingleTapEvent += HandleSingleTap;
    }
    public void Disable()
    {
      if (Base.HasSingleTap())
        Base.SingleTapEvent -= HandleSingleTap;
    }

    private void HandleSingleTap()
    {
      SingleTapEvent?.Invoke();
    }
  }

  public sealed class AppLayout : Inv.Panel<Inv.Material.Space>
  {
    public AppLayout()
    {
      this.Base = new Inv.Material.Space();

      this.ContentFrame = Inv.Frame.New();
      Base.ShowPanel(ContentFrame);
      ContentFrame.Background.In(Inv.Theme.BackgroundColour);

      var AppBarFrame = Inv.Frame.New();
      Base.ShowPanel(AppBarFrame);
      AppBarFrame.Alignment.TopStretch();

      this.AppBar = new AppBar();
      AppBarFrame.Content = AppBar;
      AppBar.AdjustEvent += () => SetContentHeight();

      this.FloatingActionContainer = new FloatingActionContainer(Base);
      Base.ShowPanel(FloatingActionContainer);

      SetContentHeight();
    }

    public AppBar AppBar { get; }
    public Inv.Material.FloatingActionContainer FloatingActionContainer { get; }

    public Inv.Transition TransitionContent(Inv.Panel Content) => ContentFrame.Transition(Content);

    internal void Clear()
    {
      AppBar.Clear();

      FloatingActionContainer.Clear();
    }

    private void SetContentHeight()
    {
      var Dimension = AppBar.GetDimension();

      ContentFrame.Margin.Set(0, Dimension.Height != 0 ? Dimension.Height : 56, 0, 0);
    }

    private readonly Inv.Frame ContentFrame;
  }

  internal sealed class AppBarLayout : Inv.Panel<Inv.Dock>
  {
    public AppBarLayout()
    {
      this.Base = Inv.Dock.NewHorizontal();
      Base.Size.SetHeight(56);
      Base.Padding.Set(0, 4, 4, 4);

      this.LeadingButton = new IconButton();
      Base.AddHeader(LeadingButton);
      LeadingButton.Margin.Set(4, 0, 0, 0); // 16 left of leading icon.
      LeadingButton.Large();
      LeadingButton.Alignment.Center();
      LeadingButton.Visibility.Collapse();
      LeadingButton.ForegroundColour = Inv.Material.Theme.OnPrimaryColour;
      LeadingButton.SingleTapEvent += () => LeadingSingleTapAction?.Invoke();

      this.ContentFrame = Inv.Frame.New();
      Base.AddClient(ContentFrame);
      ContentFrame.Margin.Set(16, 0, 0, 0); // TODO: Should be 16 from baseline.

      this.TrailingIconButtonList = new Inv.DistinctList<IconButton>();
    }

    public int Height => Base.Size.Height.Value;
    public bool IsSubtle { get; set; }
    public Inv.Colour ButtonForegroundColour
    {
      set
      {
        this.ButtonForegroundColourField = value;
        LeadingButton.ForegroundColour = value;
      }
    }

    public void Clear()
    {
      ClearLeading();

      ContentFrame.Content = null;

      Base.RemoveFooters();
    }
    public void ClearLeading()
    {
      LeadingButton.Image = null;
      LeadingButton.Visibility.Collapse();

      LeadingSingleTapAction = null;
    }
    public void SetLeading(Inv.Image Image, Action SingleTapAction)
    {
      LeadingButton.Image = Image;
      LeadingButton.Visibility.Set(Image != null);

      this.LeadingSingleTapAction = SingleTapAction;
    }
    public IconButton AddTrailingIconButton()
    {
      var Button = new IconButton();
      Base.AddFooter(Button);
      TrailingIconButtonList.Add(Button);
      Button.Large();
      Button.Margin.Set(12, 0, 0, 0);

      Refresh();

      return Button;
    }

    internal Inv.Panel CustomContent
    {
      get => ContentFrame.Content;
      set => ContentFrame.Content = value;
    }

    internal void Refresh()
    {
      var ForegroundColour = ButtonForegroundColourField ?? (IsSubtle ? Inv.Theme.OnBackgroundColour : Inv.Theme.OnPrimaryLightVariantColour);

      LeadingButton.ForegroundColour = ForegroundColour;

      foreach (var Button in TrailingIconButtonList)
        Button.ForegroundColour = ForegroundColour;
    }

    private readonly IconButton LeadingButton;
    private readonly Inv.Frame ContentFrame;
    private readonly Inv.DistinctList<IconButton> TrailingIconButtonList;
    private Action LeadingSingleTapAction;
    private Inv.Colour ButtonForegroundColourField;
  }

  public sealed class AppBar : Inv.Panel<Inv.Stack>
  {
    public AppBar()
    {
      this.Base = Inv.Stack.NewVertical();
      Base.Alignment.TopStretch();
      Base.Elevation.Set(2);

      this.LayoutFrame = Inv.Frame.New();
      Base.AddPanel(LayoutFrame);

      this.PrimaryLayout = new AppBarLayout();
      LayoutFrame.Content = PrimaryLayout;

      this.FlexibleFrame = Inv.Frame.New();
      Base.AddPanel(FlexibleFrame);

      Refresh();
    }

    public int Height => PrimaryLayout.Height;
    public Inv.Panel FlexibleContent
    {
      set => FlexibleFrame.Content = value;
    }
    public Inv.Colour ButtonForegroundColour
    {
      set => PrimaryLayout.ButtonForegroundColour = value;
    }
    public Inv.Visibility Visibility => Base.Visibility;
    public Inv.Margin Margin => Base.Margin;

    public void SetLeading(Inv.Image Image, Action SingleTapEvent) => PrimaryLayout.SetLeading(Image, SingleTapEvent);
    public void ClearLeading() => PrimaryLayout.ClearLeading();
    public void ContentAsTitle(string Text)
    {
      this.TitleLabel = Inv.Label.New();
      PrimaryLayout.CustomContent = TitleLabel;
      TitleLabel.Text = Text;
      TitleLabel.Alignment.CenterLeft();
      TitleLabel.LineWrapping = false;
      TitleLabel.Font.Custom(21).Medium().In(GetForegroundColour());
    }
    public void ContentAsCustom(Inv.Panel Panel) => PrimaryLayout.CustomContent = Panel;
    public IconButton AddTrailingIconButton() => PrimaryLayout.AddTrailingIconButton();
    public void Clear()
    {
      PrimaryLayout.Clear();

      FlexibleContent = null;
    }
    public AppSearch EnableSearch()
    {
      if (AppSearch == null)
        this.AppSearch = new AppSearch(this);

      return AppSearch;
    }
    public void Subtle()
    {
      PrimaryLayout.IsSubtle = true;

      this.ForegroundColour = Inv.Theme.OnBackgroundColour;
      this.BackgroundColour = Inv.Theme.BackgroundColour;

      Refresh();

      Base.Elevation.Set(0);
    }
    public void SetColour(Inv.Colour BackgroundColour = null, Inv.Colour ForegroundColour = null)
    {
      if (ForegroundColour != null)
        this.ForegroundColour = ForegroundColour;
      else if (BackgroundColour != null)
        this.ForegroundColour = BackgroundColour.BackgroundToBlackWhiteForeground();

      if (BackgroundColour != null)
        this.BackgroundColour = BackgroundColour;

      Refresh();
    }

    internal Inv.Elevation Elevation => Base.Elevation;
    internal event Action AdjustEvent
    {
      add { Base.AdjustEvent += value; }
      remove { Base.AdjustEvent -= value; }
    }

    internal Inv.Dimension GetDimension() => Base.GetDimension();
    internal void ShowSearch(AppSearch Search) => LayoutFrame.Transition(Search.Layout);
    internal void HideSearch(AppSearch Search) => LayoutFrame.Transition(PrimaryLayout);

    private void Refresh()
    {
      Base.Background.In(GetBackgroundColour());

      if (TitleLabel != null)
        TitleLabel.Font.In(GetForegroundColour());

      PrimaryLayout.Refresh();
    }
    private Inv.Colour GetForegroundColour() => ForegroundColour ?? Inv.Theme.OnPrimaryLightVariantColour;
    private Inv.Colour GetBackgroundColour() => BackgroundColour ?? Inv.Theme.PrimaryLightVariantColour;

    private readonly Inv.Frame LayoutFrame;
    private readonly AppBarLayout PrimaryLayout;
    private readonly Inv.Frame FlexibleFrame;
    private AppSearch AppSearch;
    private Label TitleLabel;
    private Inv.Colour ForegroundColour;
    private Inv.Colour BackgroundColour;
  }

  public sealed class AppSearch
  {
    internal AppSearch(Inv.Material.AppBar AppBar)
    {
      this.AppBar = AppBar;

      this.Timer = Inv.Application.Access().Window.NewTimer();
      Timer.IntervalTime = TimeSpan.FromMilliseconds(500);
      Timer.IntervalEvent += () =>
      {
        Timer.Stop();

        var NewText = Edit.Text.EmptyOrWhitespaceAsNull();

        if (!string.Equals(Text, NewText, StringComparison.CurrentCultureIgnoreCase))
        {
          this.Text = NewText;

          ChangeEvent?.Invoke();
        }
      };

      this.Layout = new AppBarLayout();

      this.Edit = Inv.Edit.NewText();
      Layout.CustomContent = Edit;
      Edit.Alignment.CenterStretch();
      Edit.Font.Custom(18).In(Inv.Material.Theme.OnPrimaryColour);
      Edit.ChangeEvent += () =>
      {
        ClearButton.Visibility.Set(!string.IsNullOrWhiteSpace(Edit.Text));

        Timer.Restart();
      };

      this.ClearButton = Layout.AddTrailingIconButton();
      ClearButton.Image = Inv.Material.Resources.Images.Clear;
      ClearButton.Visibility.Collapse();
      ClearButton.SingleTapEvent += () => Clear();

      Layout.SetLeading(Inv.Material.Resources.Images.ArrowBack, () =>
      {
        Clear();
        Refresh(false);
      });

      // NOTE: this is added to the AppBar.
      this.SearchButton = AppBar.AddTrailingIconButton();
      SearchButton.Image = Inv.Material.Resources.Images.Search;
      SearchButton.SingleTapEvent += () =>
      {
        Refresh(true);

        Edit.Focus.Set();
      };
    }

    public string Text { get; private set; }
    public event Action ChangeEvent;
    public event Action EnterEvent;
    public event Action ExitEvent;

    internal Inv.Material.AppBarLayout Layout { get; }

    private void Refresh(bool IsSearching)
    {
      if (IsSearching)
      {
        SearchButton.Visibility.Collapse();

        AppBar.ShowSearch(this);

        EnterEvent?.Invoke();
      }
      else
      {
        SearchButton.Visibility.Show();

        AppBar.HideSearch(this);

        ExitEvent?.Invoke();
      }
    }
    private void Clear()
    {
      Edit.Text = "";
      ClearButton.Visibility.Set(false);
    }

    private readonly Inv.Material.AppBar AppBar;
    private readonly Inv.WindowTimer Timer;
    private readonly Inv.Edit Edit;
    private readonly Inv.Material.IconButton ClearButton;
    private readonly Inv.Material.IconButton SearchButton;
  }

  public sealed class Tab : Inv.Panel<Inv.Button>
  {
    internal Tab(Inv.Material.TabBar TabBar)
    {
      this.TabBar = TabBar;

      this.Base = Inv.Button.NewStark();
      Base.OverEvent += () => Refresh();
      Base.AwayEvent += () => Refresh();

      this.Overlay = Inv.Overlay.New();
      Base.Content = Overlay;

      this.IndicatorFrame = Inv.Frame.New();
      Overlay.AddPanel(IndicatorFrame);
      IndicatorFrame.Margin.Set(0, 0, 0, 1);
      IndicatorFrame.Size.SetHeight(1);
      IndicatorFrame.Alignment.BottomStretch();

      this.DividerFrame = Inv.Frame.New();
      Overlay.AddPanel(DividerFrame);
      DividerFrame.Size.SetHeight(1);
      DividerFrame.Alignment.BottomStretch();

      this.Label = Inv.Label.New();
      Overlay.AddPanel(Label);
      Label.Padding.Set(12, 0);
      Label.Alignment.Center();
      Label.Justify.Center();
      Label.Font.Custom(14).Medium().In(Inv.Theme.OnBackgroundColour);

      Refresh();
    }

    public string Title
    {
      get => Label.Text;
      set => Label.Text = value.ToUpper();
    }
    public bool IsActive => IsActiveField;
    public Inv.Visibility Visibility => Base.Visibility;
    public Inv.Tooltip Tooltip => Base.Tooltip;
    public event Action EnterEvent;
    public event Action LeaveEvent;

    internal bool IsEnabled
    {
      set => Base.IsEnabled = value;
    }
    internal event Action SingleTapEvent
    {
      add => Base.SingleTapEvent += value;
      remove => Base.SingleTapEvent -= value;
    }

    internal void SetActive(bool Value)
    {
      if (Value != IsActiveField)
      {
        this.IsActiveField = Value;

        Refresh();

        if (Value)
          EnterEvent?.Invoke();
        else
          LeaveEvent?.Invoke();
      }
    }
    internal void Refresh()
    {
      var IsOver = Base.IsOver;
      var BackgroundColour = IsActiveField ? Inv.Theme.PrimaryLightVariantColour.Opacity(Inv.Theme.IsLight ? 0.20F : 1.0F) : IsOver ? Inv.Theme.SmokeColour : Inv.Theme.BackgroundColour;
      var TextColour = IsActiveField ? (Inv.Theme.IsLight ? Inv.Theme.PrimaryLightVariantColour : Inv.Colour.White) : IsOver ? Inv.Theme.OnBackgroundColour : Inv.Theme.SubtleColour;
      var IndicatorColour = IsActiveField ? Inv.Theme.IsLight ? TextColour : Inv.Theme.OnBackgroundColour : Inv.Colour.Transparent;

      Base.Background.In(BackgroundColour);
      Label.Font.In(TextColour);
      IndicatorFrame.Background.In(IndicatorColour);
      DividerFrame.Background.In(IsActiveField ? IndicatorColour : Inv.Theme.EdgeColour);
    }

    private readonly Inv.Material.TabBar TabBar;
    private readonly Inv.Overlay Overlay;
    private readonly Inv.Frame IndicatorFrame;
    private readonly Inv.Frame DividerFrame;
    private readonly Inv.Label Label;
    private bool IsActiveField;
  }

  public sealed class TabBar : Inv.Panel<Inv.Dock>
  {
    public TabBar()
    {
      this.TabList = new Inv.DistinctList<Tab>();

      this.Base = Inv.Dock.NewHorizontal();
      Base.Size.SetHeight(48);
    }

    public Tab ActiveTab { get; private set; }
    public Inv.Visibility Visibility => Base.Visibility;
    public bool IsEnabled
    {
      set => TabList.ForEach(T => T.IsEnabled = value);
    }
    public event Action ChangeEvent;

    public Tab AddTab(string Title)
    {
      var Result = new Tab(this);
      Result.Title = Title;
      Result.SingleTapEvent += () =>
      {
        Activate(Result);
        ChangeEvent?.Invoke();
      };

      Base.AddClient(Result);

      TabList.Add(Result);

      return Result;
    }
    public void RemoveTabs()
    {
      Base.RemoveClients();
      TabList.Clear();
    }
    public void RemoveTabs(IEnumerable<Tab> Tabs)
    {
      foreach (var Tab in Tabs)
      {
        Base.RemoveClient(Tab);
        TabList.Remove(Tab);
      }
    }
    /// <summary>
    /// Activate a particular tab in the tab view.
    /// <para>Note that ChangeEvent only gets fired when a tab is tapped, and not when this method is called.</para>
    /// </summary>
    /// <param name="Tab"></param>
    public void Activate(Tab Tab) => InternalActivate(Tab);
    public IEnumerable<Tab> GetTabs() => TabList;

    internal void InternalActivate(Tab Tab)
    {
      this.ActiveTab = Tab;

      // Don't fire again if already active.
      if (Tab != null && Tab.IsActive)
        return;

      foreach (var EachTab in TabList)
        EachTab.SetActive(EachTab == Tab);
    }

    private readonly Inv.DistinctList<Tab> TabList;
  }

  public sealed class TabBarView
  {
    public TabBarView()
    {
      this.TabFrameList = new Inv.DistinctList<TabBarFrame>();

      this.TabBar = new TabBar();
      TabBar.ChangeEvent += () => TransitionToActive();

      this.ContentFrame = Inv.Frame.New();
    }

    public Inv.Material.TabBar TabBar { get; }
    public Inv.Frame ContentFrame { get; }
    public Inv.Material.Tab FirstTab => TabFrameList.FirstOrDefault()?.Tab;

    public Tab AddTab(string Title, Func<Inv.Panel> BuildContentQuery)
    {
      var Result = TabBar.AddTab(Title);

      var TabFrame = new TabBarFrame(TabFrameList.Count, Result, BuildContentQuery);

      TabFrameList.Add(TabFrame);

      return Result;
    }
    public Inv.Transition Activate(Tab Tab)
    {
      TabBar.InternalActivate(Tab);

      return TransitionToActive();
    }
    public void RemoveTabs()
    {
      TabBar.RemoveTabs();
      TabFrameList.Clear();
      ContentFrame.Content = null;
      this.ActiveFrame = null;
    }
    public void RefreshActive()
    {
      var Content = ActiveFrame?.BuildContent();

      ContentFrame.Transition(Content).Fade();
    }

    private Inv.Transition TransitionToActive()
    {
      var ActiveTab = TabBar.ActiveTab;
      var TabFrame = TabFrameList.Find(F => F.Tab == ActiveTab);
      var TabFrameContent = TabFrame?.BuildContent();

      var Transition = ContentFrame.Transition(TabFrameContent);

      if (ActiveFrame != null)
      {
        if (ActiveFrame.Index < TabFrame?.Index)
          Transition.CarouselNext();
        else
          Transition.CarouselPrevious();
      }
      else
      {
        // Don't animate on first load.
        if (ContentFrame.Content == null)
          Transition.None();
        else
          Transition.Fade();
      }

      this.ActiveFrame = TabFrame;

      return Transition;
    }

    private readonly Inv.DistinctList<TabBarFrame> TabFrameList;
    private TabBarFrame ActiveFrame;

    private sealed class TabBarFrame
    {
      public TabBarFrame(int Index, Tab Tab, Func<Inv.Panel> BuildContentQuery)
      {
        this.Index = Index;
        this.Tab = Tab;
        this.BuildContentQuery = BuildContentQuery;
      }

      public Tab Tab { get; }
      public int Index { get; }

      public Inv.Panel BuildContent() => BuildContentQuery?.Invoke();

      private readonly Func<Inv.Panel> BuildContentQuery;
    }
  }

  public sealed class Button : Inv.Panel<Inv.Button>
  {
    public Button()
    {
      this.Base = Inv.Button.NewStark();
      Base.Codepoint.Caller();
      Base.Corner.Set(2);
      Base.Size.SetHeight(36);
      Base.Size.SetMinimumWidth(64);
      Base.OverEvent += () => Refresh();
      Base.AwayEvent += () => Refresh();
      Base.PressEvent += () => Refresh();
      Base.ReleaseEvent+= () => Refresh();

      this.Stack = Inv.Stack.NewHorizontal();
      Base.Content = Stack;
      Stack.Alignment.Center();

      this.Label = Inv.Label.New();
      Stack.AddPanel(Label);
      Label.Alignment.Center();
      Label.Margin.Set(16, 0, 16, 0);
      Label.Font.Custom(14).Medium().In(Inv.Theme.OnBackgroundColour);

      AsText();
    }

    public Inv.Codepoint Codepoint => Base.Codepoint;
    public Inv.Image Image
    {
      get => Icon?.Image;
      set
      {
        if (value != Icon?.Image)
        {
          if (Icon == null)
          {
            Icon = new Inv.Material.Icon();
            Icon.Size.Set(20);
            Icon.Margin.Set(11, 0, 7, 0);
          }

          Icon.Image = value;

          if (value == null && Stack.Panels.Contains(Icon))
            Stack.RemovePanel(Icon);
          else if (value != null && !Stack.Panels.Contains(Icon))
            Stack.InsertPanel(0, Icon);

          Refresh();
        }
      }
    }
    public string Caption
    {
      get => Label.Text;
      set => Label.Text = value?.ToUpper();
    }
    public Inv.Focus Focus => Base.Focus;
    public Inv.Margin Margin => Base.Margin;
    public Inv.Visibility Visibility => Base.Visibility;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Tooltip Tooltip => Base.Tooltip;
    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set
      {
        Base.IsEnabled = value;
        Refresh();
      }
    }
    public bool IsFocusable
    {
      get => Base.IsFocusable;
      set => Base.IsFocusable = value;
    }
    public event Action SingleTapEvent
    {
      add => Base.SingleTapEvent += value;
      remove => Base.SingleTapEvent -= value;
    }

    /// <summary>
    /// Used for low emphasis actions.
    /// </summary>
    public void AsText()
    {
      this.StyleField = Style.Text;
      this.BackgroundColour = Inv.Colour.Transparent;
      this.HoverBackgroundColour = Inv.Theme.Light.WhiteSmoke;
      this.TextColour = Inv.Material.Theme.PrimaryColour;

      Refresh();
    }
    /// <summary>
    /// Used for moderate emphasis actions.
    /// </summary>
    public void AsOutlined()
    {
      this.StyleField = Style.Outlined;
      this.BackgroundColour = Inv.Colour.Transparent;
      this.HoverBackgroundColour = Inv.Theme.Light.WhiteSmoke;
      this.TextColour = Inv.Material.Theme.PrimaryColour;

      Refresh();
    }
    /// <summary>
    /// Used for high emphasis actions.
    /// </summary>
    public void AsContained(Inv.Colour Colour = null)
    {
      this.StyleField = Style.Contained;
      this.BackgroundColour = Colour ?? Inv.Material.Theme.PrimaryColour;
      this.HoverBackgroundColour = BackgroundColour.Lighten(0.10F);
      this.TextColour = BackgroundColour.BackgroundToBlackWhiteForeground();

      Refresh();
    }

    internal Inv.Font Font => Label.Font;
    internal Inv.Background Background => Base.Background;
    internal Inv.Border Border => Base.Border;
    internal Inv.Elevation Elevation => Base.Elevation;
    internal event Action OverEvent
    {
      add => Base.OverEvent += value;
      remove => Base.OverEvent -= value;
    }
    internal event Action AwayEvent
    {
      add => Base.AwayEvent += value;
      remove => Base.AwayEvent -= value;
    }

    private void Refresh()
    {
      var ForegroundColour = Base.IsEnabled ? TextColour : Inv.Material.Theme.DisabledPrimary;

      Font.In(ForegroundColour);
      Border.Set(StyleField == Style.Outlined ? 1 : 0).In(ForegroundColour);

      if (Base.IsEnabled)
        Base.Background.In(Base.IsPressed ? (StyleField == Style.Contained ? BackgroundColour.Darken(0.10F) : HoverBackgroundColour.Darken(0.10F)) : Base.IsOver ? HoverBackgroundColour : BackgroundColour);
      else
        Base.Background.In(StyleField == Style.Contained ? Inv.Material.Theme.DisabledSecondary : BackgroundColour);

      if (Image == null)
      {
        if (StyleField == Style.Text)
          Label.Margin.Set(8, 0, 8, 0);
        else
          Label.Margin.Set(16, 0, 16, 0);
      }
      else
      {
        if (Icon != null)
          Icon.Colour = ForegroundColour;

        Label.Margin.Set(0, 0, 16, 0);
      }
    }

    private readonly Inv.Stack Stack;
    private readonly Inv.Label Label;
    private Inv.Material.Icon Icon; // Created on demand.
    private Style StyleField;
    private Inv.Colour BackgroundColour;
    private Inv.Colour HoverBackgroundColour;
    private Inv.Colour TextColour;

    private enum Style
    {
      Text, Outlined, Contained
    }
  }

  public sealed class FloatingActionContainer : Inv.Panel<Inv.Frame>
  {
    internal FloatingActionContainer(Inv.Material.Space Space)
    {
      this.Space = Space;

      this.Base = Inv.Frame.New();
    }

    public Inv.Visibility Visibility => Base.Visibility;

    public FloatingActionButton AsButton()
    {
      if (Button == null)
      {
        Button = new FloatingActionButton();
      }
      else
      {
        Button.Clear();
        Button.RemoveSingleTap();
      }

      InitialiseButton();

      Base.Content = Button;

      return Button;
    }
    public FloatingActionSpeedDial AsSpeedDial()
    {
      if (SpeedDial == null)
        SpeedDial = new FloatingActionSpeedDial(Space);
      else
        SpeedDial.Clear();

      InitialiseSpeedDial();

      Base.Content = SpeedDial;

      return SpeedDial;
    }

    internal void Clear()
    {
      Base.Content = null;

      if (Button != null)
      {
        Button.Clear();

        InitialiseButton();
      }

      if (SpeedDial != null)
      {
        SpeedDial.Clear();

        InitialiseSpeedDial();
      }
    }

    private void InitialiseButton()
    {
      Button.Margin.Set(0, 0, 16, 16);
      Button.Alignment.BottomRight();
      Button.Visibility.Show();
    }
    private void InitialiseSpeedDial()
    {
      SpeedDial.Margin.Set(0, 0, 16, 16);
      SpeedDial.Alignment.BottomRight();
      SpeedDial.Visibility.Show();
    }

    private readonly Inv.Material.Space Space;
    private FloatingActionButton Button;
    private FloatingActionSpeedDial SpeedDial;
  }

  /// <summary>
  /// Floating Action Buttons (or FABs) are used for the primary action on a screen.
  /// </summary>
  public sealed class FloatingActionButton : Inv.Panel<Inv.Button>
  {
    public FloatingActionButton()
    {
      this.Base = Inv.Button.NewFlat();
      Base.Elevation.Set(3);
      Base.SingleTapEvent += () => SingleTapEvent?.Invoke();
      Base.Codepoint.Set(1);

      var Stack = Inv.Stack.NewHorizontal();
      Base.Content = Stack;
      Stack.Alignment.Center();

      this.Icon = new Inv.Material.Icon();
      Stack.AddPanel(Icon);
      Icon.Alignment.CenterStretch();
      Icon.Size.Set(24);

      this.Label = Inv.Label.New();
      Stack.AddPanel(Label);
      Label.Margin.Set(12, 0, 8, 1);
      Label.Font.Custom(13).Medium();

      Primary();
      Default();
    }

    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set => Base.IsEnabled = value;
    }
    /// <summary>
    /// The content of the included Text label.
    /// <para>Note: if the button is not being rendered as a <see cref="Style.Extended"/> FAB, the label will not be displayed.</para>
    /// </summary>
    public string Text
    {
      get => Label.Text;
      set
      {
        Label.Text = value?.ToUpper();
        Refresh();
      }
    }
    public Inv.Size Size => Base.Size;
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public Inv.Image Image
    {
      get => Icon.Image;
      set => Icon.Image = value;
    }
    public Inv.Tooltip Tooltip => Base.Tooltip;
    public event Action SingleTapEvent;

    public void Mini()
    {
      StyleField = Style.Mini;
      Refresh();
    }
    public void Default()
    {
      StyleField = Style.Default;
      Refresh();
    }
    /// <summary>
    /// Extended FABs also include a text label.
    /// <para>Note: if <see cref="Text"/> is not set, the button will be rendered as a <see cref="Style.Default"/> FAB.</para>
    /// </summary>
    public void Extended()
    {
      StyleField = Style.Extended;
      Refresh();
    }
    public void Primary()
    {
      Base.Background.In(Inv.Material.Theme.PrimaryColour);
      Icon.Colour = Inv.Material.Theme.OnPrimaryColour;
      Label.Font.In(Inv.Material.Theme.OnPrimaryColour);
    }
    public void Secondary()
    {
      Base.Background.In(Inv.Material.Theme.SecondaryColour);
      Icon.Colour = Inv.Material.Theme.OnSecondaryColour;
      Label.Font.In(Inv.Material.Theme.OnSecondaryColour);
    }
    public void Subtle()
    {
      Base.Background.In(Inv.Theme.BackgroundColour);
      Icon.Colour = Inv.Material.Palette.Grey600;
      Label.Font.In(Inv.Theme.OnBackgroundColour);
    }

    internal void Clear()
    {
      Label.Text = null;
      if (StyleField == Style.Extended)
        StyleField = Style.Default;
      Refresh();

      Image = null;
      IsEnabled = true;
    }
    internal void RemoveSingleTap() => SingleTapEvent = null;

    private void Refresh()
    {
      // 2022-12-01 KJV
      // Extended FABs by definition have Text. If the Text hasn't been provided we will render the button as a Default() FAB.

      int Size;

      switch (StyleField == Style.Extended && string.IsNullOrWhiteSpace(Text) ? Style.Default : StyleField)
      {
        case Style.Default:
          Size = 56;
          Base.Size.Set(Size);
          Base.Corner.Set(Size / 2);
          Base.Padding.Set(16);
          Label.Visibility.Collapse();
          break;

        case Style.Mini:
          Size = 40;
          Base.Size.Set(Size);
          Base.Corner.Set(Size / 2);
          Base.Padding.Set(8);
          Label.Visibility.Collapse();
          break;

        case Style.Extended:
          Size = 48;
          Base.Size.Set(null, Size);
          Base.Corner.Set(Size / 2);
          Base.Padding.Set(16, 0, 16, 0);
          Label.Visibility.Set(!string.IsNullOrWhiteSpace(Text));
          break;

        default:
          throw EnumHelper.UnexpectedValueException(StyleField);
      }
    }

    private readonly Inv.Material.Icon Icon;
    private readonly Inv.Label Label;
    private Style StyleField;

    private enum Style { Default, Mini, Extended }
  }

  public sealed class FloatingActionSpeedDial : Inv.Panel<Inv.Material.FloatingActionButton>
  {
    internal FloatingActionSpeedDial(Inv.Material.Space Space)
    {
      this.Space = Space;
      this.ButtonList = new Inv.DistinctList<FloatingActionSpeedDialButton>();

      this.Base = new FloatingActionButton();
      Base.SingleTapEvent += () =>
      {
        ButtonDock.Alignment.Set(Base.Alignment.Get());
        ButtonDock.Margin.Set(Base.Margin);

        CancelButton.Alignment.Set(Base.Alignment.Get());

        foreach (var Button in ButtonList)
        {
          switch (Base.Alignment.Get())
          {
            case Placement.TopLeft:
            case Placement.CenterLeft:
            case Placement.StretchLeft:
            case Placement.BottomLeft:
              Button.Alignment.CenterLeft();
              break;

            case Placement.TopCenter:
            case Placement.Center:
            case Placement.StretchCenter:
            case Placement.BottomCenter:
              Button.Alignment.Center();
              break;

            case Placement.TopRight:
            case Placement.CenterRight:
            case Placement.StretchRight:
            case Placement.BottomRight:
              Button.Alignment.CenterRight();
              break;

            case Placement.TopStretch:
            case Placement.CenterStretch:
            case Placement.Stretch:
            case Placement.BottomStretch:
              Button.Alignment.Stretch();
              break;

            default:
              break;
          }
        }

        Space.ShowPanel(Scrim);
        Space.ShowPanel(ButtonDock);

        Base.Visibility.Collapse();
      };

      this.Scrim = new Scrim();
      Scrim.SingleTapEvent += () => HideSpeedDial();
      Scrim.Visibility.Show();

      this.ButtonDock = Inv.Dock.NewVertical();

      this.CancelButton = new FloatingActionButton();
      ButtonDock.AddFooter(CancelButton);
      CancelButton.Image = Inv.Material.Resources.Images.Clear;
      CancelButton.SingleTapEvent += () => HideSpeedDial();
    }

    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public Inv.Image Image
    {
      get => Base.Image;
      set => Base.Image = value;
    }
    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set => Base.IsEnabled = value;
    }

    public FloatingActionSpeedDialButton AddButton()
    {
      var Result = new FloatingActionSpeedDialButton(() => HideSpeedDial());

      ButtonDock.AddHeader(Result);

      ButtonList.Add(Result);

      return Result;
    }

    internal void Clear()
    {
      Base.Clear();

      ButtonDock.RemovePanels();
      ButtonList.Clear();
    }

    private void HideSpeedDial()
    {
      Space.HidePanel(Scrim);
      Space.HidePanel(ButtonDock);

      Base.Visibility.Show();
    }

    private readonly Inv.Material.Space Space;
    private readonly Inv.Material.Scrim Scrim;
    private readonly Inv.Dock ButtonDock;
    private readonly Inv.Material.FloatingActionButton CancelButton;
    private readonly Inv.DistinctList<FloatingActionSpeedDialButton> ButtonList;
  }

  public sealed class FloatingActionSpeedDialButton : Inv.Panel<Inv.Material.FloatingActionButton>
  {
    internal FloatingActionSpeedDialButton(Action HideSpeedDialAction)
    {
      this.Base = new FloatingActionButton();
      Base.Alignment.Center();
      Base.Margin.Set(0, 0, 0, 8);
      Base.Subtle();
      Base.SingleTapEvent += () =>
      {
        HideSpeedDialAction?.Invoke();

        SingleTapEvent?.Invoke();
      };
    }

    public Inv.Image Image
    {
      get => Base.Image;
      set => Base.Image = value;
    }
    public string Text
    {
      get => Base.Text;
      set => Base.Text = value;
    }
    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set => Base.IsEnabled = value;
    }

    public Inv.Visibility Visibility => Base.Visibility;
    public event Action SingleTapEvent;

    public void Extended() => Base.Extended();

    internal Inv.Alignment Alignment => Base.Alignment;
  }

  public sealed class Icon : Inv.Panel<Inv.Graphic>
  {
    public Icon()
    {
      this.Base = Inv.Graphic.New();
      //Base.Size.Set(24);

      this.ColourField = Inv.Material.Theme.PrimaryColour;
    }

    public Inv.Size Size => Base.Size;
    public Inv.Margin Margin => Base.Margin;
    public Inv.Padding Padding => Base.Padding;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public Inv.Image Image
    {
      get => ImageField;
      set
      {
        if (ImageField != value)
        {
          ImageField = value;

          Refresh();
        }
      }
    }
    /// <summary>
    /// Sets the colour of the icon. Material icons default to being tinted by the theme primary colour.
    /// </summary>
    public Inv.Colour Colour
    {
      get => ColourField;
      set
      {
        if (ColourField != value)
        {
          ColourField = value ?? Inv.Material.Theme.PrimaryColour;

          Refresh();
        }
      }
    }
    public Inv.Background Background => Base.Background;
    public Inv.Corner Corner => Base.Corner;
    public Inv.Tooltip Tooltip => Base.Tooltip;

    private void Refresh()
    {
      Base.Image = Inv.Material.Icon.Tint(ImageField, ColourField);
    }

    private Inv.Colour ColourField;
    private Inv.Image ImageField;

    static Icon()
    {
      TintCacheDictionary = new Dictionary<Inv.Image, TintCache>();
    }

    private static Inv.Image Tint(Inv.Image Image, Inv.Colour Colour)
    {
      if (Image == null)
        return null;

      var TintCache = TintCacheDictionary.GetValueOrDefault(Image);
      if (TintCache == null)
      {
        TintCache = new TintCache();
        TintCacheDictionary.Add(Image, TintCache);
      }

      var Result = TintCache.ColourImageDictionary.GetValueOrDefault(Colour);

      if (Result == null)
      {
        Result = Inv.Application.Access().Graphics.Tint(Image, Colour);
        TintCache.ColourImageDictionary.Add(Colour, Result);
      }

      return Result;
    }

    private static readonly Dictionary<Inv.Image, TintCache> TintCacheDictionary;

    private sealed class TintCache
    {
      public TintCache()
      {
        this.ColourImageDictionary = new Dictionary<Inv.Colour, Inv.Image>();
      }

      public Dictionary<Inv.Colour, Inv.Image> ColourImageDictionary { get; }
    }
  }

  public sealed class IconButton : Inv.Panel<Inv.Button>
  {
    public IconButton()
    {
      this.Base = Inv.Button.NewStark();
      Base.Background.In(Inv.Colour.Transparent);
      Base.OverEvent += () =>
      {
        if (HasSingleTap())
          Base.Background.In(Inv.Theme.Light.WhiteSmoke);
      };
      Base.AwayEvent += () =>
      {
        if (HasSingleTap())
          Base.Background.In(Inv.Colour.Transparent);
      };
      Base.Codepoint.Set(1);

      this.Icon = new Inv.Material.Icon();
      Base.Content = Icon;
      Icon.Alignment.Stretch();
    }

    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set => Base.IsEnabled = value;
    }
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Margin Margin => Base.Margin;
    public Inv.Image Image
    {
      get => Icon.Image;
      set => Icon.Image = value;
    }
    public Inv.Colour ForegroundColour
    {
      get => Icon.Colour;
      set => Icon.Colour = value;
    }
    public Inv.Tooltip Tooltip => Base.Tooltip;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action SingleTapEvent
    {
      add => Base.SingleTapEvent += value;
      remove => Base.SingleTapEvent -= value;
    }

    public void Small()
    {
      Icon.Size.Set(18);
      SetSize(24);
    }
    public void Normal()
    {
      SetSize(32);
      Icon.Size.Set(24);
    }
    public void Large()
    {
      SetSize(48);
      Icon.Size.Set(24);
    }

    private void SetSize(int Size)
    {
      Base.Size.Set(Size);

      Base.Corner.Set(Size / 2);
    }
    private bool HasSingleTap() => Base.HasSingleTap();

    private readonly Inv.Material.Icon Icon;
  }

  public sealed class RadioSelectDialog<T>
  {
    internal RadioSelectDialog(Inv.Material.Space Space)
    {
      this.ConfirmationDialog = Space.NewConfirmationDialog();
      ConfirmationDialog.Alignment.Stretch();
      ConfirmationDialog.HideEvent += () => HideEvent?.Invoke();

      this.RadioFlow = new Inv.Material.ListRadioFlow<T>();
      ConfirmationDialog.Content = RadioFlow;
      RadioFlow.SingleTapEvent += () =>
      {
        if (IsAutomaticAccept)
          Accept();
      };

      var SearchDock = Inv.Dock.NewHorizontal();
      ConfirmationDialog.AddTitleFooter(SearchDock);
      SearchDock.Padding.Set(8);
      SearchDock.Margin.Set(12, 0, 12, 0);
      SearchDock.Alignment.Center();
      SearchDock.Corner.Set(3);
      SearchDock.Background.In(Inv.Theme.SmokeColour);

      var SearchImage = new Inv.Material.Icon();
      SearchDock.AddHeader(SearchImage);
      SearchImage.Margin.Set(0, 0, 8, 0);
      SearchImage.Image = Inv.Material.Resources.Images.Search;
      SearchImage.Size.Set(24, 24);

      this.SearchEdit = Inv.Edit.NewSearch();
      SearchDock.AddClient(SearchEdit);
      SearchEdit.Alignment.CenterStretch();
      SearchEdit.Font.Custom(16).Light().In(Inv.Theme.OnBackgroundColour);
      SearchEdit.Size.SetWidth(150);

      var Timer = Inv.Application.Access().Window.NewTimer();
      Timer.IntervalTime = TimeSpan.FromMilliseconds(500);
      Timer.IntervalEvent += () =>
      {
        Timer.Stop();

        RadioFlow.Search.Filter(SearchEdit.Text.EmptyOrWhitespaceAsNull());
      };

      SearchEdit.ChangeEvent += () => Timer.Restart();

      this.RightCancelAction = ConfirmationDialog.AddRightCancelAction(() => CancelEvent?.Invoke());

      this.LeftCancelAction = ConfirmationDialog.AddLeftCancelAction(() => CancelEvent?.Invoke());
      LeftCancelAction.Visibility.Collapse();

      this.AcceptAction = ConfirmationDialog.AddRightAction("OK", () => Accept());
      AcceptAction.AsOutlined();

      ConfirmationDialog.ShowEvent += () => AcceptAction.Visibility.Set(!IsAutomaticAccept);

      this.IsAutomaticAccept = true;
    }

    public string Title { get; set; }
    public string NullTitle
    {
      get => RadioFlow.NullTitle;
      set => RadioFlow.NullTitle = value;
    }
    public T Content
    {
      get => RadioFlow.SelectedContext;
      set => RadioFlow.SelectedContext = value;
    }
    public bool IsAutomaticAccept { get; set; }
    public Inv.Size Size => ConfirmationDialog.Size;
    public Inv.Margin Margin => ConfirmationDialog.Margin;
    public Inv.Alignment Alignment => ConfirmationDialog.Alignment;
    public IEqualityComparer<T> EqualityComparer
    {
      get => RadioFlow.EqualityComparer;
      set => RadioFlow.EqualityComparer = value;
    }
    public event Action<ListSelectItem<T>> ComposeEvent
    {
      add => RadioFlow.ComposeEvent += value;
      remove => RadioFlow.ComposeEvent -= value;
    }
    public event Action AcceptEvent;
    public event Action CancelEvent;
    public event Action HideEvent;

    public void OrderByTitle()
    {
      RadioFlow.OrderByTitle();
    }
    public void OrderByComposition()
    {
      RadioFlow.OrderByComposition();
    }
    public void OrderBy(Comparison<T> OrderByComparison)
    {
      RadioFlow.OrderBy(OrderByComparison);
    }
    public void GroupBy(Func<T, string> KeyQuery)
    {
      RadioFlow.GroupBy(KeyQuery);
    }
    public ListCollation<T, G> GroupBy<G>(Func<T, G> KeyQuery, Action<ListHeader<T, G>> HeaderAction)
    {
      return RadioFlow.GroupBy(KeyQuery, HeaderAction);
    }
    public void Show(IEnumerable<T> Values)
    {
      ConfirmationDialog.Title = Title;

      RadioFlow.Load(Values);

      // NOTE: for devices with software keyboards - do not focus the search by default, this is annoying for short lists.
      if (Inv.Application.Access().Device.Keyboard)
        ConfirmationDialog.SetFocus(SearchEdit.Focus);

      ConfirmationDialog.Show();
    }
    public void AlignCancelActionRight()
    {
      RightCancelAction.Visibility.Show();
      LeftCancelAction.Visibility.Collapse();
    }
    public void AlignCancelActionLeft()
    {
      LeftCancelAction.Visibility.Show();
      RightCancelAction.Visibility.Collapse();
    }
    public void AcceptActionAsOutlined() => AcceptAction.AsOutlined();
    public void AcceptActionAsContained() => AcceptAction.AsContained();

    internal bool IsEqualTo(T A, T B) => RadioFlow.IsEqualTo(A, B);
    internal string GetTitle(T Value, bool IncludeHeader) => RadioFlow.GetTitle(Value, IncludeHeader);

    private void Accept()
    {
      ConfirmationDialog.Hide();
      AcceptEvent?.Invoke();
    }

    private readonly ConfirmationDialog ConfirmationDialog;
    private readonly Inv.Edit SearchEdit;
    private readonly Inv.Material.ListRadioFlow<T> RadioFlow;
    private readonly Button RightCancelAction;
    private readonly Button LeftCancelAction;
    private readonly Button AcceptAction;
  }

  public sealed class CheckBox : Inv.Panel<Inv.Button>
  {
    public CheckBox()
    {
      this.Base = Inv.Button.NewStark();
      Base.Background.In(Inv.Colour.Transparent);
      Base.AwayEvent += () => Refresh();
      Base.OverEvent += () => Refresh();
      Base.SingleTapEvent += () =>
      {
        if (!IsEnabled)
          return;

        IsCheckedField = !IsCheckedField;
        Refresh();
      };

      var Stack = Inv.Stack.NewHorizontal();
      Base.Content = Stack;

      this.Icon = new Inv.Material.Icon();
      Stack.AddPanel(Icon);
      Icon.Margin.Set(16, 0, 0, 0);
      Icon.Padding.Set(4);
      Icon.Size.Set(32);
      Icon.Corner.Set(24);
      Icon.Alignment.Center();

      this.Label = Inv.Label.New();
      Stack.AddPanel(Label);
      Label.Margin.Set(8, 0, 0, 4);
      Label.Font.Custom(16);
      Label.Alignment.CenterLeft();
      Label.Visibility.Collapse();

      this.IsEnabledField = true;
      this.IsCheckedField = false;

      Refresh();
    }

    public string Text
    {
      get => Label.Text;
      set
      {
        Label.Text = value;
        Refresh();
      }
    }
    public bool IsChecked
    {
      get => IsCheckedField;
      set
      {
        if (IsCheckedField != value)
        {
          this.IsCheckedField = value;
          Refresh();
        }
      }
    }
    public bool IsEnabled
    {
      get => IsEnabledField;
      set
      {
        IsEnabledField = value;
        Refresh();
      }
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;

    public event Action CheckEvent
    {
      add => Base.SingleTapEvent += value;
      remove => Base.SingleTapEvent -= value;
    }

    private void Refresh()
    {
      Icon.Background.In(IsEnabledField && Base.IsOver ? Inv.Material.Theme.PrimaryColour.Opacity(0.25F) : Inv.Colour.Transparent);
      Icon.Image = IsCheckedField ? Resources.Images.CheckBox : Resources.Images.CheckBoxOutlineBlank;
      Icon.Colour = IsEnabledField ? null : Inv.Theme.Light.DarkGray;

      Label.Visibility.Set(!string.IsNullOrWhiteSpace(Label.Text));
      Label.Font.In(IsEnabledField ? Inv.Theme.OnBackgroundColour : Inv.Theme.Light.DarkGray);
    }

    private readonly Inv.Material.Icon Icon;
    private readonly Inv.Label Label;
    private bool IsCheckedField;
    private bool IsEnabledField;
  }

  public sealed class Switch : Inv.Panel<Inv.Switch>
  {
    public Switch()
    {
      this.Base = Inv.Switch.New();
      Base.PrimaryColour = Inv.Material.Theme.PrimaryColour;
      Base.SecondaryColour = Inv.Material.Theme.PrimaryColour.Lighten(0.50F);
    }

    public bool IsOn
    {
      get => Base.IsOn;
      set => Base.IsOn = value;
    }
    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set => Base.IsEnabled = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public event Action ChangeEvent
    {
      add { Base.ChangeEvent += value; }
      remove { Base.ChangeEvent -= value; }
    }
  }

  public sealed class FullScreenDialog
  {
    internal FullScreenDialog(Inv.Material.Space Space)
    {
      this.Space = Space;

      this.Plane = new Inv.Material.Plane();
      Plane.HideEvent += () => Hide();

      this.TransitionFrame = Inv.Frame.New();
      TransitionFrame.Alignment.Stretch();
      TransitionFrame.Background.In(Inv.Theme.BackgroundColour);

      this.ContentFrame = Inv.Frame.New();
    }

    public Inv.Background Background => ContentFrame.Background;
    public Inv.Panel Content
    {
      set { ContentFrame.Content = value; }
    }
    public event Action ShowEvent;
    public event Action HideEvent;

    public void SetFocus(Inv.Focus Focus)
    {
      if (Focus != null && IsShown)
        Focus.Set();
      else
        this.Focus = Focus;
    }
    public void Show()
    {
      Space.ShowPanel(TransitionFrame);

      TransitionFrame.Transition(ContentFrame);

      ShowEvent?.Invoke();

      if (Focus != null)
      {
        Focus.Set();
        this.Focus = null;
      }

      this.IsShown = true;
    }
    public void Hide()
    {
      this.IsShown = false;

      Space.HidePanel(TransitionFrame);

      HideEvent?.Invoke();
    }

    public static implicit operator Inv.Material.Plane(FullScreenDialog Self) => Self?.Plane;

    private readonly Inv.Material.Space Space;
    private readonly Inv.Material.Plane Plane;
    private readonly Inv.Frame TransitionFrame;
    private readonly Inv.Frame ContentFrame;
    private Inv.Focus Focus;
    private bool IsShown;
  }

  internal sealed class BaseDialog
  {
    internal BaseDialog(Inv.Material.Space Space)
    {
      this.Space = Space;
      this.Application = Inv.Application.Access();

      this.Plane = new Plane();
      Plane.HideEvent += () => Hide();

      this.Overlay = Inv.Overlay.New();
      Overlay.AdjustEvent += () => AdjustEvent?.Invoke();

      this.Scrim = new Inv.Material.Scrim();
      Overlay.AddPanel(Scrim);
      Scrim.Visibility.Show();
      Scrim.SingleTapEvent += () => Hide();

      this.ContentFrame = Inv.Frame.New();
      Overlay.AddPanel(ContentFrame);
      ContentFrame.Alignment.CenterStretch();
      ContentFrame.Background.In(Inv.Theme.BackgroundColour);
      ContentFrame.Elevation.Set(4);
      ContentFrame.Corner.Set(2);

      this.KeyboardBinding = Application.Keyboard.NewBinding();
      KeyboardBinding.Bind(Inv.Key.Escape, () =>
      {
        if (Space.IsActivePlane(this))
          Hide(EscapeActionList);
      });

      this.EscapeActionList = new Inv.DistinctList<Action>();
    }

    public Inv.Panel Content
    {
      set { ContentFrame.Content = value; }
    }
    public Inv.Size Size => ContentFrame.Size;
    public Inv.Margin Margin => ContentFrame.Margin;
    public Inv.Alignment Alignment => ContentFrame.Alignment;
    public event Action ShowEvent;
    public event Action HideEvent;
    public event Action AdjustEvent;

    public void Show()
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      Space.ShowPanel(Overlay);

      ShowEvent?.Invoke();

      KeyboardBinding.Capture();

      if (Focus != null)
      {
        Focus.Set();
        this.Focus = null;
      }

      this.IsShown = true;

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    public void Hide(Inv.DistinctList<Action> ActionList = null)
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      this.IsShown = false;

      Space.HidePanel(Overlay);

      KeyboardBinding.Release();

      ActionList?.ForEach(A => A?.Invoke());
      EscapeActionList.Clear();

      HideEvent?.Invoke();

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    public void SetFocus(Inv.Focus Focus)
    {
      if (Focus != null && IsShown)
        Focus.Set();
      else
        this.Focus = Focus;
    }
    public void DismissCasual()
    {
      Scrim.Enable();
      ContentFrame.Margin.Set(44);
    }
    public void DismissExplicit()
    {
      Scrim.Disable();
      ContentFrame.Margin.Set(8);
    }

    public static implicit operator Inv.Material.Plane(BaseDialog Self) => Self?.Plane;

    internal void RegisterEscapeAction(Action Action) => EscapeActionList.Add(Action);

    private readonly Inv.Material.Plane Plane;
    private readonly Inv.Material.Space Space;
    private readonly Inv.Application Application;
    private readonly Inv.Overlay Overlay;
    private readonly Inv.Material.Scrim Scrim;
    private readonly Inv.Frame ContentFrame;
    private readonly KeyboardBinding KeyboardBinding;
    private readonly Inv.DistinctList<Action> EscapeActionList;
    private Inv.Focus Focus;
    private bool IsShown;

    private static readonly Inv.TraceClass TraceClass = Inv.Trace.NewClass<Inv.Material.BaseDialog>(Inv.Trace.PlatformFeature);
  }

  public sealed class ConfirmationDialog
  {
    internal ConfirmationDialog(Inv.Material.Space Space)
    {
      this.Base = new BaseDialog(Space);
      Base.DismissExplicit();

      this.Dock = Inv.Dock.NewVertical();
      Base.Content = Dock;
      Dock.Corner.Set(0, 0, 2, 2);

      this.TitleDock = Inv.Dock.NewHorizontal();
      Dock.AddHeader(TitleDock);

      this.TitleLabel = Inv.Label.New();
      TitleDock.AddClient(TitleLabel);
      TitleLabel.Visibility.Collapse();
      TitleLabel.Margin.Set(24, 24, 24, 19);
      TitleLabel.Font.Custom(20).Medium().In(Inv.Theme.OnBackgroundColour);

      this.TitleDivider = Divider.NewVertical();
      Dock.AddHeader(TitleDivider);

      this.ActionDivider = Divider.NewVertical();
      Dock.AddFooter(ActionDivider);
      ActionDivider.Visibility.Collapse();

      this.ActionDock = Inv.Dock.NewHorizontal();
      Dock.AddFooter(ActionDock);
      ActionDock.Corner.Set(2);
      ActionDock.Alignment.CenterStretch();
      ActionDock.Margin.Set(8);
      ActionDock.Visibility.Collapse();
    }

    public string Title
    {
      get => TitleLabel.Text;
      set
      {
        TitleLabel.Text = value;
        TitleLabel.Visibility.Set(!string.IsNullOrWhiteSpace(value));

        if (string.IsNullOrWhiteSpace(value))
          TitleDivider.Visibility.Collapse();
      }
    }
    public Inv.Material.Divider TitleDivider { get; private set; }
    public Inv.Panel Content
    {
      get => ContentField;
      set
      {
        if (ContentField != value)
        {
          this.ContentField = value;

          Dock.RemoveClients();
          if (ContentField != null)
            Dock.AddClient(ContentField);
        }
      }
    }
    public Inv.Size Size => Base.Size;
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public event Action ShowEvent
    {
      add => Base.ShowEvent += value;
      remove => Base.ShowEvent -= value;
    }
    public event Action HideEvent
    {
      add => Base.HideEvent += value;
      remove => Base.HideEvent -= value;
    }
    public event Action AdjustEvent
    {
      add => Base.AdjustEvent += value;
      remove => Base.AdjustEvent -= value;
    }

    public void Show()
    {
      Base.Show();
    }
    public void Hide(Action Action = null)
    {
      Base.Hide(Action.SingleToDistinctList());
    }
    public void SetFocus(Inv.Focus Focus)
    {
      Base.SetFocus(Focus);
    }
    public void AddTitleFooter(Inv.Panel Panel)
    {
      TitleDock.AddFooter(Panel);
    }
    public Button AddRightAction(string Caption, Action Action)
    {
      var Button = CreateButton(Caption, Action);
      ActionDock.AddFooter(Button);
      Button.Margin.Set(8, 0, 0, 0);
      Button.Codepoint.Set(1);

      return Button;
    }
    public Button AddLeftAction(string Caption, Action Action)
    {
      var Button = CreateButton(Caption, Action);
      ActionDock.AddHeader(Button);
      Button.Margin.Set(0, 0, 8, 0);
      Button.Codepoint.Set(1);

      return Button;
    }
    public Button AddRightCancelAction(Action Action = null)
    {
      var Button = CreateCancelButton(Action);
      ActionDock.AddFooter(Button);
      Button.Margin.Set(8, 0, 0, 0);
      Button.Codepoint.Set(1);

      return Button;
    }
    public Button AddLeftCancelAction(Action Action = null)
    {
      var Button = CreateCancelButton(Action);
      ActionDock.AddHeader(Button);
      Button.Margin.Set(0, 0, 8, 0);
      Button.Codepoint.Set(1);

      return Button;
    }

    private Button CreateCancelButton(Action Action = null)
    {
      var Button = CreateButton("cancel", () => Hide(Action));
      Button.Codepoint.Set(1);

      if (Action != null)
        Base.RegisterEscapeAction(Action);

      return Button;
    }
    private Button CreateButton(string Caption, Action Action)
    {
      ActionDivider.Visibility.Show();
      ActionDock.Visibility.Show();

      var Button = new Button();
      Button.AsText();
      Button.Caption = Caption;
      Button.SingleTapEvent += () => Action?.Invoke();
      Button.Codepoint.Set(1);

      return Button;
    }

    public static implicit operator Inv.Material.Plane(ConfirmationDialog Self) => Self?.Base;

    private readonly Inv.Material.BaseDialog Base;
    private readonly Inv.Dock Dock;
    private readonly Inv.Dock TitleDock;
    private readonly Inv.Label TitleLabel;
    private readonly Inv.Material.Divider ActionDivider;
    private readonly Inv.Dock ActionDock;
    private Inv.Panel ContentField;
  }

  public sealed class SimpleDialog
  {
    internal SimpleDialog(Inv.Material.Space Space)
    {
      this.Base = new BaseDialog(Space);
      Base.DismissCasual();

      this.Dock = Inv.Dock.NewVertical();
      Base.Content = Dock;
      Dock.Padding.Set(0, 0, 0, 8);
      Dock.Corner.Set(0, 0, 2, 2);

      this.TitleLabel = Inv.Label.New();
      Dock.AddHeader(TitleLabel);
      TitleLabel.Visibility.Collapse();
      TitleLabel.Margin.Set(24, 24, 24, 19);
      TitleLabel.Font.Custom(20).Medium().In(Inv.Theme.OnBackgroundColour);
    }

    public string Title
    {
      get => TitleLabel.Text;
      set
      {
        TitleLabel.Text = value;
        TitleLabel.Visibility.Set(!string.IsNullOrWhiteSpace(value));
      }
    }
    public Inv.Size Size => Base.Size;
    public Inv.Panel Content
    {
      get => ContentField;
      set
      {
        if (ContentField != value)
        {
          this.ContentField = value;

          Dock.RemoveClients();
          if (ContentField != null)
            Dock.AddClient(ContentField);
        }
      }
    }
    public event Action ShowEvent
    {
      add => Base.ShowEvent += value;
      remove => Base.ShowEvent -= value;
    }
    public event Action HideEvent
    {
      add => Base.HideEvent += value;
      remove => Base.HideEvent -= value;
    }

    public void Show() => Base.Show();
    public void Hide() => Base.Hide();

    public static implicit operator Inv.Material.Plane(SimpleDialog Self) => Self?.Base;

    private readonly Inv.Material.BaseDialog Base;
    private readonly Inv.Dock Dock;
    private readonly Inv.Label TitleLabel;
    private Inv.Panel ContentField;
  }

  public sealed class AlertDialog
  {
    internal AlertDialog(Space Space)
    {
      this.Base = new BaseDialog(Space);
      Base.Alignment.Center();
      Base.DismissExplicit();

      var Dock = Inv.Dock.NewVertical();
      Base.Content = Dock;

      var ContentStack = Inv.Stack.NewVertical();
      Dock.AddClient(ContentStack);
      ContentStack.Corner.Set(2);
      ContentStack.Margin.Set(24);

      this.TitleLabel = Inv.Label.New();
      ContentStack.AddPanel(TitleLabel);
      TitleLabel.Visibility.Collapse();
      TitleLabel.Alignment.CenterLeft();
      TitleLabel.Margin.Set(0, 0, 0, 20);
      TitleLabel.LineWrapping = true;
      TitleLabel.Font.Custom(20).Medium().In(Inv.Theme.OnBackgroundColour);

      this.ContentLabel = Inv.Label.New();
      ContentStack.AddPanel(ContentLabel);
      ContentLabel.Alignment.CenterLeft();
      ContentLabel.LineWrapping = true;
      ContentLabel.Font.Custom(16).In(Inv.Theme.SubtleColour);

      this.ActionDock = Inv.Dock.NewHorizontal();
      Dock.AddFooter(ActionDock);
      ActionDock.Corner.Set(2);
      ActionDock.Alignment.CenterStretch();
      ActionDock.Margin.Set(8, 8, 8, 8);
      ActionDock.Visibility.Collapse();
    }

    public string Title
    {
      get => TitleLabel.Text;
      set
      {
        TitleLabel.Text = value;
        TitleLabel.Visibility.Set(!string.IsNullOrWhiteSpace(value));
      }
    }
    public string Message
    {
      set => ContentLabel.Text = value;
    }
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Size Size => Base.Size;
    public event Action ShowEvent
    {
      add => Base.ShowEvent += value;
      remove => Base.ShowEvent -= value;
    }
    public event Action HideEvent
    {
      add => Base.HideEvent += value;
      remove => Base.HideEvent -= value;
    }

    public void Show()
    {
      if (TraceClass.IsActive)
        TraceClass.Enter(Text: $"'{Title}'");

      Base.Show();

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    public void ShowStandardError(string Title, string Message)
    {
      this.Title = Title;
      this.Message = Message;
      AddRightCloseAction();
      Show();
    }
    public void Hide(Action Action = null)
    {
      if (TraceClass.IsActive)
        TraceClass.Enter(Text: $"'{Title}'");

      Base.Hide(Action.SingleToDistinctList());

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    public Button AddRightAction(string Caption, Action Action)
    {
      var Button = CreateButton(Caption, Action);
      ActionDock.AddFooter(Button);
      Button.Margin.Set(8, 0, 0, 0);

      return Button;
    }
    public Button AddLeftAction(string Caption, Action Action)
    {
      var Button = CreateButton(Caption, Action);
      ActionDock.AddHeader(Button);
      Button.Margin.Set(0, 0, 8, 0);

      return Button;
    }
    public void AddRightCloseAction(Action Action = null)
    {
      var Button = CreateRejectButton("close", Action);
      ActionDock.AddFooter(Button);
      Button.Margin.Set(8, 0, 0, 0);
    }
    public void AddLeftCloseAction(Action Action = null)
    {
      var Button = CreateRejectButton("close", Action);
      ActionDock.AddHeader(Button);
      Button.Margin.Set(0, 0, 8, 0);
    }
    public void AddRightCancelAction(Action Action = null)
    {
      var Button = CreateRejectButton("cancel", Action);
      ActionDock.AddFooter(Button);
      Button.Margin.Set(8, 0, 0, 0);
    }
    public void AddLeftCancelAction(Action Action = null)
    {
      var Button = CreateRejectButton("cancel", Action);
      ActionDock.AddHeader(Button);
      Button.Margin.Set(0, 0, 8, 0);
    }

    private Button CreateRejectButton(string Caption, Action Action = null)
    {
      var Button = CreateButton(Caption, () => Hide(Action));

      if (Action != null)
        Base.RegisterEscapeAction(Action);

      return Button;
    }
    private Button CreateButton(string Caption, Action Action)
    {
      ActionDock.Visibility.Show();

      var Button = new Button();
      Button.AsText();
      Button.Caption = Caption;
      Button.SingleTapEvent += () => Action?.Invoke();

      return Button;
    }

    public static implicit operator Inv.Material.Plane(AlertDialog Self) => Self?.Base;

    private readonly Inv.Material.BaseDialog Base;
    private readonly Inv.Label TitleLabel;
    private readonly Inv.Label ContentLabel;
    private readonly Inv.Dock ActionDock;

    private static readonly Inv.TraceClass TraceClass = Inv.Trace.NewClass<Inv.Material.AlertDialog>(Inv.Trace.PlatformFeature);
  }

  public sealed class Menu
  {
    public Menu()
    {
      this.Base = Inv.Popup.New();

      this.Stack = Inv.Stack.NewVertical();
      Base.Content = Stack;
      Stack.Background.In(Inv.Theme.SurfaceColour);
      Stack.Elevation.Set(2);
      Stack.Corner.Set(2);
      Stack.Margin.Set(4, 1, 4, 4);
      Stack.Padding.Set(0, 8);
      Stack.Size.SetMinimumWidth(112);
      Stack.Size.SetMaximumWidth(280);

      this.ItemList = new Inv.DistinctList<Inv.Material.MenuItem>();
    }

    public Inv.PopupPosition Position => Base.Position;
    public IReadOnlyList<MenuItem> Items => ItemList;

    public void Show()
    {
      Base.Show();
    }
    public void RemoveItems()
    {
      ItemList.Clear();
      Stack.RemovePanels();
    }
    public void AddHeader(string Header)
    {
      var Subheader = new Inv.Material.ListSubheader();
      Stack.AddPanel(Subheader);
      Subheader.Text = Header;
    }
    public MenuItem AddItem()
    {
      var Result = new MenuItem(this);
      Result.Codepoint.Caller();
      Stack.AddPanel(Result);
      ItemList.Add(Result);
      return Result;
    }
    public MenuDivider AddDivider()
    {
      var Result = new MenuDivider();
      Stack.AddPanel(Result);
      return Result;
    }

    internal void Hide() => Base.Hide();

    private readonly Inv.Popup Base;
    private readonly Inv.Stack Stack;
    private readonly Inv.DistinctList<MenuItem> ItemList;
  }

  public sealed class MenuItem : Inv.Panel<Inv.Button>
  {
    internal MenuItem(Menu Menu)
    {
      this.Base = Inv.Button.NewStark();
      Base.Background.In(Inv.Colour.Transparent);
      Base.Padding.Set(12, 0);
      Base.Size.SetHeight(48);
      Base.OverEvent += () => Base.Background.In(Inv.Theme.EdgeColour);
      Base.AwayEvent += () => Base.Background.In(Inv.Colour.Transparent);
      Base.SingleTapEvent += () =>
      {
        Menu.Hide();
        SingleTapEvent?.Invoke();
      };

      this.Dock = Inv.Dock.NewHorizontal();
      Base.Content = Dock;

      this.LeadingIcon = new Inv.Material.Icon();
      Dock.AddHeader(LeadingIcon);
      LeadingIcon.Size.Set(32);
      LeadingIcon.Alignment.Center();
      LeadingIcon.Padding.Set(0, 0, 12, 0);
      LeadingIcon.Colour = Inv.Theme.OnSurfaceColour;

      this.TextLabel = Inv.Label.New();
      Dock.AddClient(TextLabel);
      TextLabel.Alignment.CenterLeft();
      TextLabel.Font.Large().In(Inv.Theme.OnSurfaceColour);
      TextLabel.Text = Text;
    }

    public string Text
    {
      get => TextLabel.Text;
      set => TextLabel.Text = value;
    }
    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set
      {
        Base.IsEnabled = value;
        TextLabel.Font.In(value ? Inv.Theme.OnSurfaceColour : Inv.Theme.EdgeColour);
        LeadingIcon.Colour = value ? Inv.Theme.OnSurfaceColour : Inv.Theme.EdgeColour;
      }
    }
    public Inv.Codepoint Codepoint => Base.Codepoint;
    public Inv.Image LeadingImage
    {
      get => LeadingIcon.Image;
      set => LeadingIcon.Image = value;
    }
    public event Action SingleTapEvent;

    private readonly Inv.Dock Dock;
    private readonly Inv.Material.Icon LeadingIcon;
    private readonly Inv.Label TextLabel;
  }

  public sealed class MenuDivider : Inv.Panel<Inv.Material.Divider>
  {
    internal MenuDivider()
    {
      this.Base = Inv.Material.Divider.NewVertical();
      Base.Margin.Set(0, 8);
    }

    public Inv.Visibility Visibility => Base.Visibility;
  }

  public sealed class Chip : Inv.Panel<Inv.Button>
  {
    public Chip()
    {
      this.Base = Inv.Button.NewStark();
      Base.Corner.Set(16);
      Base.OverEvent += () => RefreshBackground();
      Base.AwayEvent += () => RefreshBackground();
      Base.Size.SetHeight(32);
      Base.Alignment.TopLeft();

      this.Dock = Inv.Dock.NewHorizontal();
      Base.Content = Dock;
      Dock.Alignment.Stretch();

      this.Icon = new Inv.Material.Icon();
      Dock.AddHeader(Icon);
      Icon.Size.Set(24);
      Icon.Margin.Set(4, 0, 6, 0);
      Icon.Alignment.CenterStretch();
      Icon.Visibility.Collapse();

      this.Label = Inv.Label.New();
      Dock.AddHeader(Label);
      Label.Margin.Set(0, 0, 0, 1);
      Label.Alignment.CenterStretch();
      Label.Font.Normal().Custom(14);
      Label.LineWrapping = false;

      this.ActionButton = Inv.Button.NewStark();
      Dock.AddFooter(ActionButton);
      ActionButton.Alignment.CenterStretch();
      ActionButton.Background.In(Inv.Colour.Transparent);
      ActionButton.Margin.Set(8, 0, 4, 0);
      ActionButton.Size.Set(24);
      ActionButton.Corner.Set(12);
      ActionButton.OverEvent += () =>
      {
        if (ActionButton.HasSingleTap())
          ActionButton.Background.In(ActionImageHoverColour);
      };
      ActionButton.AwayEvent += () =>
      {
        if (ActionButton.HasSingleTap())
          ActionButton.Background.In(Inv.Colour.Transparent);
      };

      this.ActionIcon = new Inv.Material.Icon();
      ActionButton.Content = ActionIcon;
      ActionIcon.Alignment.Stretch();
      ActionIcon.Size.Set(18);

      this.BackgroundColour = Inv.Theme.Light.LightGray;
      this.ForegroundColour = Inv.Theme.OnBackgroundColour;
      this.HoverBackgroundColour = Inv.Theme.Light.DimGray;
      this.ImageColour = Inv.Theme.Light.DarkGray;
      this.ActionImageColour = Inv.Theme.Light.DimGray;
      this.ActionImageHoverColour = Inv.Theme.Light.DimGray.Lighten(0.40F);

      Refresh();
      RefreshBackground();
    }

    public string Text
    {
      get => Label.Text;
      set => Label.Text = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Image Image
    {
      set
      {
        Icon.Image = value;
        Refresh();
      }
    }
    public Inv.Image ActionImage
    {
      set
      {
        ActionIcon.Image = value;
        Refresh();
      }
    }
    public Inv.Visibility Visibility => Base.Visibility;
    public Inv.Tooltip Tooltip => Base.Tooltip;
    public event Action SingleTapEvent
    {
      add
      {
        Base.SingleTapEvent += value;
        RefreshBackground();
      }
      remove
      {
        Base.SingleTapEvent -= value;
        RefreshBackground();
      }
    }
    public event Action ActionTapEvent
    {
      add
      {
        ActionButton.SingleTapEvent += value;
        RefreshBackground();
      }
      remove
      {
        ActionButton.SingleTapEvent -= value;
        RefreshBackground();
      }
    }

    public void Dye(Inv.Colour Colour)
    {
      this.IsOutlinedField = false;
      this.BackgroundColour = Colour;
      this.ForegroundColour = BackgroundColour.BackgroundToBlackWhiteForeground();
      this.HoverBackgroundColour = Colour.Darken(0.10F);
      this.HoverForegroundColour = HoverBackgroundColour.BackgroundToBlackWhiteForeground();

      if (Colour == Inv.Material.Theme.PrimaryColour)
        ImageColour = Inv.Material.Theme.OnPrimaryColour;
      else
        ImageColour = Inv.Theme.IsDark ? Colour.Lighten(0.30F) : Colour.Darken(0.30F);

      this.ActionImageColour = ImageColour;
      this.ActionImageHoverColour = ActionImageColour.Darken(0.10F);

      RefreshBackground();
    }
    public void Outlined()
    {
      this.IsOutlinedField = true;
      this.BackgroundColour = Inv.Theme.BackgroundColour;
      this.ForegroundColour = Inv.Theme.OnBackgroundColour;
      this.HoverBackgroundColour = Inv.Theme.Light.WhiteSmoke;
      this.HoverForegroundColour = Inv.Theme.OnBackgroundColour;
      this.ImageColour = Inv.Theme.Light.LightGray;
      this.ActionImageColour = Inv.Theme.Light.DarkGray;
      this.ActionImageHoverColour = Inv.Theme.Light.LightGray;
      RefreshBackground();
    }
    public bool HasSingleTap() => Base.HasSingleTap();

    private void Refresh()
    {
      int LabelLeftMargin;
      int LabelRightMargin;

      if (Icon.Image != null)
      {
        Icon.Visibility.Show();
        LabelLeftMargin = 0;
      }
      else
      {
        Icon.Visibility.Collapse();
        LabelLeftMargin = 12;
      }

      if (ActionIcon.Image != null)
      {
        ActionButton.Visibility.Show();
        LabelRightMargin = 0;
      }
      else
      {
        ActionButton.Visibility.Collapse();
        LabelRightMargin = 12;
      }

      Label.Margin.Set(LabelLeftMargin, 0, LabelRightMargin, 0);
    }
    private void RefreshBackground()
    {
      Base.Border.Set(IsOutlinedField ? 1 : 0).In(Inv.Theme.Light.LightGray);

      var IsBaseOver = Base.IsOver && Base.HasSingleTap() && IsOutlinedField;
      Base.Background.In(IsBaseOver ? HoverBackgroundColour : BackgroundColour);
      Label.Font.In(IsBaseOver ? HoverForegroundColour : ForegroundColour);

      Icon.Colour = ImageColour;

      ActionIcon.Colour = ActionImageColour;
    }

    private readonly Inv.Dock Dock;
    private readonly Inv.Material.Icon Icon;
    private readonly Inv.Label Label;
    private readonly Inv.Button ActionButton;
    private readonly Inv.Material.Icon ActionIcon;
    private Inv.Colour ImageColour;
    private bool IsOutlinedField;
    private Inv.Colour BackgroundColour;
    private Inv.Colour ForegroundColour;
    private Inv.Colour HoverBackgroundColour;
    private Inv.Colour HoverForegroundColour;
    private Inv.Colour ActionImageColour;
    private Inv.Colour ActionImageHoverColour;
  }

  public sealed class Card : Inv.Panel<Inv.Frame>
  {
    public Card()
    {
      // Constant width, variable height.

      // Max height is the maximum available height of the platform (i.e. screen height)

      // Cards don't scroll internally (they expand) on mobile, but can scroll on desktop.

      // https://material.io/guidelines/components/cards.html#cards-actions
      // Content
      // - Optional header
      // - Rich media
      // - Supporting text
      // - Supplemental actions (action area 4)

      this.Base = Inv.Frame.New();
      Base.Background.In(Inv.Theme.BackgroundColour);
      Base.Corner.Set(2);
      Base.Elevation.Set(1);
    }

    public Inv.Size Size => Base.Size;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Margin Margin => Base.Margin;
    public Inv.Padding Padding => Base.Padding;
    public Inv.Background Background => Base.Background;
    public Inv.Panel Content
    {
      set => Base.Content = value;
    }

    public void Outlined()
    {
      Base.Border.Set(1).In(Inv.Theme.Light.LightGray);
      Base.Elevation.Set(0);
    }
    public void Elevated()
    {
      Base.Border.Set(0);
      Base.Elevation.Set(1);
    }
  }

  public enum DividerOrientation { Horizontal, Vertical }

  public sealed class Divider : Inv.Panel<Inv.Frame>
  {
    public Divider(DividerOrientation Orientation)
    {
      this.Base = Inv.Frame.New();
      Base.Background.In((Inv.Theme.IsDark ? Inv.Colour.White : Inv.Colour.Black).AdjustAlpha(30));

      switch (Orientation)
      {
        case DividerOrientation.Horizontal:
          SetHorizontal();
          break;

        case DividerOrientation.Vertical:
          SetVertical();
          break;

        default:
          throw EnumHelper.UnexpectedValueException(Orientation);
      }
    }

    /// <summary>
    /// Create a new vertical divider.
    /// </summary>
    /// <returns></returns>
    public static Inv.Material.Divider NewVertical() => new Inv.Material.Divider(DividerOrientation.Vertical);
    /// <summary>
    /// Create a new horizontal divider.
    /// </summary>
    /// <returns></returns>
    public static Inv.Material.Divider NewHorizontal() => new Inv.Material.Divider(DividerOrientation.Horizontal);

    public Inv.Margin Margin => Base.Margin;
    public Inv.Visibility Visibility => Base.Visibility;
    public Inv.Alignment Alignment => Base.Alignment;

    public void SetHorizontal() => Base.Size.Set(1, null);
    public void SetVertical() => Base.Size.Set(null, 1);
  }

  public sealed class Calendar : Inv.Panel<Inv.Stack>
  {
    public Calendar()
    {
      this.Base = Inv.Stack.NewVertical();
      Base.Alignment.TopLeft();
      Base.Margin.Set(0, 0, 0, 8);
      Base.Background.In(Inv.Theme.BackgroundColour);

      var TitleDock = Inv.Dock.NewHorizontal();
      Base.AddPanel(TitleDock);
      TitleDock.Size.SetHeight(24);
      TitleDock.Margin.Set(24, 16, 8, 12);

      this.MonthLabel = Inv.Label.New();
      TitleDock.AddClient(MonthLabel);
      MonthLabel.Alignment.CenterLeft();
      MonthLabel.Font.In(Inv.Theme.Light.DimGray).Medium().Custom(14);

      var PreviousButton = Inv.Button.NewStark();
      TitleDock.AddFooter(PreviousButton);
      PreviousButton.Margin.Set(0, 0, 24, 0);
      PreviousButton.OverEvent += () => PreviousButton.Background.In(Inv.Theme.Light.WhiteSmoke);
      PreviousButton.AwayEvent += () => PreviousButton.Background.In(Inv.Colour.Transparent);
      PreviousButton.SingleTapEvent += () => Navigate(-1);

      var PreviousGraphic = Inv.Graphic.New();
      PreviousButton.Content = PreviousGraphic;
      PreviousGraphic.Alignment.Stretch();
      PreviousGraphic.Image = PreviousImage;

      var NextButton = Inv.Button.NewStark();
      TitleDock.AddFooter(NextButton);
      NextButton.OverEvent += () => NextButton.Background.In(Inv.Theme.Light.WhiteSmoke);
      NextButton.AwayEvent += () => NextButton.Background.In(Inv.Colour.Transparent);
      NextButton.SingleTapEvent += () => Navigate(+1);

      var NextGraphic = Inv.Graphic.New();
      NextButton.Content = NextGraphic;
      NextGraphic.Alignment.Stretch();
      NextGraphic.Image = NextImage;

      var DayOfWeekStack = Inv.Stack.NewHorizontal();
      Base.AddPanel(DayOfWeekStack);
      DayOfWeekStack.Margin.Set(16, 12);

      var DayOfWeekArray = DayOfWeekHelper.DayOfWeekSeries().ToArray();

      foreach (var DayOfWeek in DayOfWeekArray)
      {
        var DayOfWeekLabel = Inv.Label.New();
        DayOfWeekStack.AddPanel(DayOfWeekLabel);
        DayOfWeekLabel.Size.SetWidth(32);
        DayOfWeekLabel.Justify.Center();
        DayOfWeekLabel.Alignment.Center();
        DayOfWeekLabel.Text = DayOfWeek.ToString().Substring(0, 1);
        DayOfWeekLabel.Font.In(Inv.Theme.Light.DarkGray).Custom(12).Medium();
      }

      var WeekStack = Inv.Stack.NewVertical();
      Base.AddPanel(WeekStack);
      WeekStack.Margin.Set(16, 0, 16, 8);

      this.WeekRowList = new List<WeekRow>();

      for (var RowIndex = 0; RowIndex < 6; RowIndex++)
      {
        var WeekRow = new WeekRow(DayOfWeekArray);
        WeekRow.SelectEvent += (Date) =>
        {
          this.Content = Date;
          ChangeEvent?.Invoke();
        };

        WeekStack.AddPanel(WeekRow);
        WeekRowList.Add(WeekRow);
      }

      var DefaultDate = Inv.Date.Now;
      SetMonth(DefaultDate.Year, DefaultDate.Month);
    }

    public Inv.Date? Content
    {
      get => SelectedDateField;
      set
      {
        if (SelectedDateField != value)
        {
          this.SelectedDateField = value;

          if (SelectedDateField != null)
            Navigate(SelectedDateField.Value);

          RefreshSelected();
        }
      }
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public event Action ChangeEvent;

    public void Navigate(Inv.Date Date)
    {
      SetMonth(Date.Year, Date.Month);
    }
    public void NavigateToday()
    {
      var TodayDate = Inv.Date.Today;
      SetMonth(TodayDate.Year, TodayDate.Month);
    }

    private void Navigate(int Direction)
    {
      Navigate(new Inv.Date(Year, Month, 01).AddMonths(Direction));
    }
    private void SetMonth(int Year, int Month)
    {
      if (this.Year == Year && this.Month == Month)
        return;

      this.Year = Year;
      this.Month = Month;

      var TodayDate = Inv.Date.Now;
      var CurrentDate = new Inv.Date(Year, Month, 01);

      MonthLabel.Text = CurrentDate.ToString("MMMM yyyy");

      foreach (var WeekRow in WeekRowList)
      {
        foreach (var DayCell in WeekRow.GetDayCells())
        {
          if (CurrentDate.Day == 1 && CurrentDate.DayOfWeek != DayCell.DayOfWeek)
          {
            DayCell.Set(null, false, false);
          }
          else
          {
            DayCell.Set(CurrentDate, CurrentDate.Month == Month, CurrentDate == TodayDate);
            CurrentDate = CurrentDate.AddDays(1);
          }
        }
      }

      RefreshSelected();
    }
    private void RefreshSelected()
    {
      foreach (var WeekRow in WeekRowList)
      {
        foreach (var DayCell in WeekRow.GetDayCells())
          DayCell.IsSelected = DayCell.Date != null && DayCell.Date == SelectedDateField;
      }
    }

    private readonly Inv.Label MonthLabel;
    private readonly List<WeekRow> WeekRowList;
    private int Year;
    private int Month;
    private Inv.Date? SelectedDateField;

    private static readonly Inv.Image PreviousImage;
    private static readonly Inv.Image NextImage;

    static Calendar()
    {
      PreviousImage = Inv.Application.Access().Graphics.Tint(Inv.Material.Resources.Images.ChevronLeft, Inv.Theme.Light.DimGray);
      NextImage = Inv.Application.Access().Graphics.Tint(Inv.Material.Resources.Images.ChevronRight, Inv.Theme.Light.DimGray);
    }

    private sealed class WeekRow : Inv.Panel<Inv.Stack>
    {
      public WeekRow(DayOfWeek[] DayOfWeekArray)
      {
        this.Base = Inv.Stack.NewHorizontal();

        this.DayOfWeekCellDictionary = new Dictionary<DayOfWeek, DayCell>();

        foreach (var DayOfWeek in DayOfWeekArray)
        {
          var Cell = new DayCell(DayOfWeek);
          Cell.SingleTapEvent += () =>
          {
            if (Cell.Date != null)
              SelectEvent?.Invoke(Cell.Date.Value);
          };

          Base.AddPanel(Cell);
          DayOfWeekCellDictionary.Add(DayOfWeek, Cell);
        }
      }

      public event Action<Inv.Date> SelectEvent;

      public IEnumerable<DayCell> GetDayCells() => DayOfWeekCellDictionary.Values;

      private readonly Dictionary<DayOfWeek, DayCell> DayOfWeekCellDictionary;
    }

    private sealed class DayCell : Inv.Panel<Inv.Button>
    {
      public DayCell(DayOfWeek DayOfWeek)
      {
        this.DayOfWeek = DayOfWeek;

        this.Base = Inv.Button.NewStark();
        Base.Size.Set(32);
        Base.Corner.Set(16);
        Base.Border.In(Inv.Theme.OnBackgroundColour);
        Base.OverEvent += () => Refresh();
        Base.AwayEvent += () => Refresh();

        this.NumberLabel = Inv.Label.New();
        Base.Content = NumberLabel;
        NumberLabel.Alignment.Center();
        NumberLabel.Font.Custom(12).Medium();
      }

      public Inv.Date? Date { get; private set; }
      public DayOfWeek DayOfWeek { get; }
      public bool IsSelected
      {
        get => IsSelectedField;
        set
        {
          this.IsSelectedField = value;
          Refresh();
        }
      }
      public event Action SingleTapEvent
      {
        add => Base.SingleTapEvent += value;
        remove => Base.SingleTapEvent -= value;
      }

      public void Set(Inv.Date? Date, bool IsCurrent, bool IsToday)
      {
        this.Date = Date;
        this.IsCurrent = IsCurrent;
        this.IsToday = IsToday;

        Refresh();
      }

      private void Refresh()
      {
        Base.Background.In(IsSelectedField ? Inv.Material.Theme.PrimaryColour : Base.IsOver && Date != null ? Inv.Theme.Light.WhiteSmoke : Inv.Colour.Transparent);
        Base.Border.Set(IsToday ? 1 : 0);
        NumberLabel.Text = Date?.Day.ToString() ?? string.Empty;
        NumberLabel.Font.In(IsSelectedField ? Inv.Material.Theme.OnPrimaryColour : IsCurrent ? Inv.Theme.OnBackgroundColour : Inv.Theme.Light.DarkGray);
      }

      private readonly Inv.Label NumberLabel;
      private bool IsToday;
      private bool IsCurrent;
      private bool IsSelectedField;
    }
  }

  public sealed class Banner : Inv.Panel<Inv.Dock>
  {
    public Banner()
    {
      this.Base = Inv.Dock.NewHorizontal();
      Base.Background.In(Inv.Theme.BackgroundColour);
      Base.Padding.Set(8);
      Base.Border.Set(0, 0, 0, 1).In(Inv.Theme.Light.LightGray);
      Base.Alignment.TopStretch();

      var Stack = Inv.Stack.NewVertical();
      Base.AddClient(Stack);
      Stack.Alignment.CenterStretch();

      this.TitleLabel = Inv.Label.New();
      Stack.AddPanel(TitleLabel);
      TitleLabel.Font.Medium().Custom(16).In(Inv.Theme.OnBackgroundColour);

      this.TextLabel = Inv.Label.New();
      Stack.AddPanel(TextLabel);
      TextLabel.Margin.Set(0, 4, 0, 0);
      TextLabel.Font.Custom(13).In(Inv.Theme.OnBackgroundColour);

      this.ActionStack = Inv.Stack.NewHorizontal();
      Base.AddFooter(ActionStack);
      ActionStack.Alignment.BottomRight();
      ActionStack.Margin.Set(24, 0, 0, 0); // Material spec says 90px margin between text and actions but this seems excessive at times.
    }

    public string Title
    {
      get => TitleLabel.Text;
      set => TitleLabel.Text = value;
    }
    public string Text
    {
      get => TextLabel.Text;
      set => TextLabel.Text = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Visibility Visibility => Base.Visibility;

    public void Dye(Inv.Colour BackgroundColour)
    {
      Base.Background.In(BackgroundColour);

      var ForegroundColour = BackgroundColour.BackgroundToBlackWhiteForeground();
      TitleLabel.Font.In(ForegroundColour);
      TextLabel.Font.In(ForegroundColour);
    }

    public void AddActionButton(string Text, Action SingleTapEvent)
    {
      var Button = new Inv.Material.Button();
      ActionStack.AddPanel(Button);
      Button.Caption = Text;
      Button.AsText();
      Button.Margin.Set(8, 0, 0, 0);
      Button.SingleTapEvent += () =>
      {
        SingleTapEvent?.Invoke();
      };
    }
    public void AddActionPanel(Inv.Panel Panel)
    {
      ActionStack.AddPanel(Panel);
    }

    private readonly Inv.Label TitleLabel;
    private readonly Inv.Label TextLabel;
    private readonly Inv.Stack ActionStack;
  }

  public sealed class TooltipText : Inv.Panel<Inv.Label>
  {
    public TooltipText(string Text = null)
    {
      this.Base = Inv.Label.New();
      Base.Padding.Set(8, 4);
      Base.Background.In(Inv.Theme.BackgroundColour);
      Base.Font.Large();
      Base.Font.In(Inv.Theme.OnBackgroundColour);

      if (Text != null)
        Base.Text = Text;
    }

    public string Text
    {
      get => Base.Text;
      set => Base.Text = value;
    }
  }

  public sealed class SideSheet : Inv.Panel<Inv.Frame>
  {
    public SideSheet()
    {
      this.Base = Inv.Frame.New();
      Base.Size.SetWidth(320);
      Base.Border.Set(1, 0, 0, 0).In(Inv.Theme.Light.LightGray);
      Base.Background.In(Inv.Theme.BackgroundColour);
    }

    public Inv.Panel Content
    {
      get => Base.Content;
      set => Base.Content = value;
    }
  }

  public static class Theme
  {
    // Use PrimaryColour and OnPrimaryColour for most cases, buttons, text, icons, etc
    public static Inv.Colour PrimaryColour => Inv.Theme.IsDark ? Inv.Theme.PrimaryDarkVariantColour : Inv.Theme.PrimaryLightVariantColour;
    public static Inv.Colour OnPrimaryColour => Inv.Theme.IsDark ? Inv.Theme.OnPrimaryDarkVariantColour : Inv.Theme.OnPrimaryLightVariantColour;

    public static Inv.Colour SecondaryColour => Inv.Theme.IsDark ? Inv.Theme.SecondaryDarkVariantColour : Inv.Theme.SecondaryLightVariantColour;
    public static Inv.Colour OnSecondaryColour => Inv.Theme.IsDark ? Inv.Theme.OnSecondaryDarkVariantColour : Inv.Theme.OnSecondaryLightVariantColour;

    public static Inv.Colour DisabledPrimary => Inv.Theme.IsDark ? Inv.Colour.FromArgb(255, 125, 125, 125) : Inv.Theme.Light.DarkGray;
    public static Inv.Colour DisabledSecondary => Inv.Theme.IsDark ? Inv.Colour.FromArgb(255, 64, 64, 64) : Inv.Theme.Light.LightGray;
  }

  public static class Palette
  {
    public static readonly Inv.Colour White = Inv.Colour.White;
    public static readonly Inv.Colour Black = Inv.Colour.Black;
    public static readonly Inv.Colour Red50 = Inv.Colour.FromArgb(0xFFFFEBEE);
    public static readonly Inv.Colour Red100 = Inv.Colour.FromArgb(0xFFFFCDD2);
    public static readonly Inv.Colour Red200 = Inv.Colour.FromArgb(0xFFEF9A9A);
    public static readonly Inv.Colour Red300 = Inv.Colour.FromArgb(0xFFE57373);
    public static readonly Inv.Colour Red400 = Inv.Colour.FromArgb(0xFFEF5350);
    public static readonly Inv.Colour Red500 = Inv.Colour.FromArgb(0xFFF44336);
    public static readonly Inv.Colour Red600 = Inv.Colour.FromArgb(0xFFE53935);
    public static readonly Inv.Colour Red700 = Inv.Colour.FromArgb(0xFFD32F2F);
    public static readonly Inv.Colour Red800 = Inv.Colour.FromArgb(0xFFC62828);
    public static readonly Inv.Colour Red900 = Inv.Colour.FromArgb(0xFFB71C1C);
    public static readonly Inv.Colour Pink50 = Inv.Colour.FromArgb(0xFFFCE4EC);
    public static readonly Inv.Colour Pink100 = Inv.Colour.FromArgb(0xFFF8BBD0);
    public static readonly Inv.Colour Pink200 = Inv.Colour.FromArgb(0xFFF48FB1);
    public static readonly Inv.Colour Pink300 = Inv.Colour.FromArgb(0xFFF06292);
    public static readonly Inv.Colour Pink400 = Inv.Colour.FromArgb(0xFFEC407A);
    public static readonly Inv.Colour Pink500 = Inv.Colour.FromArgb(0xFFE91E63);
    public static readonly Inv.Colour Pink600 = Inv.Colour.FromArgb(0xFFD81B60);
    public static readonly Inv.Colour Pink700 = Inv.Colour.FromArgb(0xFFC2185B);
    public static readonly Inv.Colour Pink800 = Inv.Colour.FromArgb(0xFFAD1457);
    public static readonly Inv.Colour Pink900 = Inv.Colour.FromArgb(0xFF880E4F);
    public static readonly Inv.Colour Purple50 = Inv.Colour.FromArgb(0xFFF3E5F5);
    public static readonly Inv.Colour Purple100 = Inv.Colour.FromArgb(0xFFE1BEE7);
    public static readonly Inv.Colour Purple200 = Inv.Colour.FromArgb(0xFFCE93D8);
    public static readonly Inv.Colour Purple300 = Inv.Colour.FromArgb(0xFFBA68C8);
    public static readonly Inv.Colour Purple400 = Inv.Colour.FromArgb(0xFFAB47BC);
    public static readonly Inv.Colour Purple500 = Inv.Colour.FromArgb(0xFF9C27B0);
    public static readonly Inv.Colour Purple600 = Inv.Colour.FromArgb(0xFF8E24AA);
    public static readonly Inv.Colour Purple700 = Inv.Colour.FromArgb(0xFF7B1FA2);
    public static readonly Inv.Colour Purple800 = Inv.Colour.FromArgb(0xFF6A1B9A);
    public static readonly Inv.Colour Purple900 = Inv.Colour.FromArgb(0xFF4A148C);
    public static readonly Inv.Colour DeepPurple50 = Inv.Colour.FromArgb(0xFFEDE7F6);
    public static readonly Inv.Colour DeepPurple100 = Inv.Colour.FromArgb(0xFFD1C4E9);
    public static readonly Inv.Colour DeepPurple200 = Inv.Colour.FromArgb(0xFFB39DDB);
    public static readonly Inv.Colour DeepPurple300 = Inv.Colour.FromArgb(0xFF9575CD);
    public static readonly Inv.Colour DeepPurple400 = Inv.Colour.FromArgb(0xFF7E57C2);
    public static readonly Inv.Colour DeepPurple500 = Inv.Colour.FromArgb(0xFF673AB7);
    public static readonly Inv.Colour DeepPurple600 = Inv.Colour.FromArgb(0xFF5E35B1);
    public static readonly Inv.Colour DeepPurple700 = Inv.Colour.FromArgb(0xFF512DA8);
    public static readonly Inv.Colour DeepPurple800 = Inv.Colour.FromArgb(0xFF4527A0);
    public static readonly Inv.Colour DeepPurple900 = Inv.Colour.FromArgb(0xFF311B92);
    public static readonly Inv.Colour Indigo50 = Inv.Colour.FromArgb(0xFFE8EAF6);
    public static readonly Inv.Colour Indigo100 = Inv.Colour.FromArgb(0xFFC5CAE9);
    public static readonly Inv.Colour Indigo200 = Inv.Colour.FromArgb(0xFF9FA8DA);
    public static readonly Inv.Colour Indigo300 = Inv.Colour.FromArgb(0xFF7986CB);
    public static readonly Inv.Colour Indigo400 = Inv.Colour.FromArgb(0xFF5C6BC0);
    public static readonly Inv.Colour Indigo500 = Inv.Colour.FromArgb(0xFF3F51B5);
    public static readonly Inv.Colour Indigo600 = Inv.Colour.FromArgb(0xFF3949AB);
    public static readonly Inv.Colour Indigo700 = Inv.Colour.FromArgb(0xFF303F9F);
    public static readonly Inv.Colour Indigo800 = Inv.Colour.FromArgb(0xFF283593);
    public static readonly Inv.Colour Indigo900 = Inv.Colour.FromArgb(0xFF1A237E);
    public static readonly Inv.Colour Blue50 = Inv.Colour.FromArgb(0xFFE3F2FD);
    public static readonly Inv.Colour Blue100 = Inv.Colour.FromArgb(0xFFBBDEFB);
    public static readonly Inv.Colour Blue200 = Inv.Colour.FromArgb(0xFF90CAF9);
    public static readonly Inv.Colour Blue300 = Inv.Colour.FromArgb(0xFF64B5F6);
    public static readonly Inv.Colour Blue400 = Inv.Colour.FromArgb(0xFF42A5F5);
    public static readonly Inv.Colour Blue500 = Inv.Colour.FromArgb(0xFF2196F3);
    public static readonly Inv.Colour Blue600 = Inv.Colour.FromArgb(0xFF1E88E5);
    public static readonly Inv.Colour Blue700 = Inv.Colour.FromArgb(0xFF1976D2);
    public static readonly Inv.Colour Blue800 = Inv.Colour.FromArgb(0xFF1565C0);
    public static readonly Inv.Colour Blue900 = Inv.Colour.FromArgb(0xFF0D47A1);
    public static readonly Inv.Colour LightBlue50 = Inv.Colour.FromArgb(0xFFE1F5FE);
    public static readonly Inv.Colour LightBlue100 = Inv.Colour.FromArgb(0xFFB3E5FC);
    public static readonly Inv.Colour LightBlue200 = Inv.Colour.FromArgb(0xFF81D4FA);
    public static readonly Inv.Colour LightBlue300 = Inv.Colour.FromArgb(0xFF4FC3F7);
    public static readonly Inv.Colour LightBlue400 = Inv.Colour.FromArgb(0xFF29B6F6);
    public static readonly Inv.Colour LightBlue500 = Inv.Colour.FromArgb(0xFF03A9F4);
    public static readonly Inv.Colour LightBlue600 = Inv.Colour.FromArgb(0xFF039BE5);
    public static readonly Inv.Colour LightBlue700 = Inv.Colour.FromArgb(0xFF0288D1);
    public static readonly Inv.Colour LightBlue800 = Inv.Colour.FromArgb(0xFF0277BD);
    public static readonly Inv.Colour LightBlue900 = Inv.Colour.FromArgb(0xFF01579B);
    public static readonly Inv.Colour Cyan50 = Inv.Colour.FromArgb(0xFFE0F7FA);
    public static readonly Inv.Colour Cyan100 = Inv.Colour.FromArgb(0xFFB2EBF2);
    public static readonly Inv.Colour Cyan200 = Inv.Colour.FromArgb(0xFF80DEEA);
    public static readonly Inv.Colour Cyan300 = Inv.Colour.FromArgb(0xFF4DD0E1);
    public static readonly Inv.Colour Cyan400 = Inv.Colour.FromArgb(0xFF26C6DA);
    public static readonly Inv.Colour Cyan500 = Inv.Colour.FromArgb(0xFF00BCD4);
    public static readonly Inv.Colour Cyan600 = Inv.Colour.FromArgb(0xFF00ACC1);
    public static readonly Inv.Colour Cyan700 = Inv.Colour.FromArgb(0xFF0097A7);
    public static readonly Inv.Colour Cyan800 = Inv.Colour.FromArgb(0xFF00838F);
    public static readonly Inv.Colour Cyan900 = Inv.Colour.FromArgb(0xFF006064);
    public static readonly Inv.Colour Teal50 = Inv.Colour.FromArgb(0xFFE0F2F1);
    public static readonly Inv.Colour Teal100 = Inv.Colour.FromArgb(0xFFB2DFDB);
    public static readonly Inv.Colour Teal200 = Inv.Colour.FromArgb(0xFF80CBC4);
    public static readonly Inv.Colour Teal300 = Inv.Colour.FromArgb(0xFF4DB6AC);
    public static readonly Inv.Colour Teal400 = Inv.Colour.FromArgb(0xFF26A69A);
    public static readonly Inv.Colour Teal500 = Inv.Colour.FromArgb(0xFF009688);
    public static readonly Inv.Colour Teal600 = Inv.Colour.FromArgb(0xFF00897B);
    public static readonly Inv.Colour Teal700 = Inv.Colour.FromArgb(0xFF00796B);
    public static readonly Inv.Colour Teal800 = Inv.Colour.FromArgb(0xFF00695C);
    public static readonly Inv.Colour Teal900 = Inv.Colour.FromArgb(0xFF004D40);
    public static readonly Inv.Colour Green50 = Inv.Colour.FromArgb(0xFFE8F5E9);
    public static readonly Inv.Colour Green100 = Inv.Colour.FromArgb(0xFFC8E6C9);
    public static readonly Inv.Colour Green200 = Inv.Colour.FromArgb(0xFFA5D6A7);
    public static readonly Inv.Colour Green300 = Inv.Colour.FromArgb(0xFF81C784);
    public static readonly Inv.Colour Green400 = Inv.Colour.FromArgb(0xFF66BB6A);
    public static readonly Inv.Colour Green500 = Inv.Colour.FromArgb(0xFF4CAF50);
    public static readonly Inv.Colour Green600 = Inv.Colour.FromArgb(0xFF43A047);
    public static readonly Inv.Colour Green700 = Inv.Colour.FromArgb(0xFF388E3C);
    public static readonly Inv.Colour Green800 = Inv.Colour.FromArgb(0xFF2E7D32);
    public static readonly Inv.Colour Green900 = Inv.Colour.FromArgb(0xFF1B5E20);
    public static readonly Inv.Colour LightGreen50 = Inv.Colour.FromArgb(0xFFF1F8E9);
    public static readonly Inv.Colour LightGreen100 = Inv.Colour.FromArgb(0xFFDCEDC8);
    public static readonly Inv.Colour LightGreen200 = Inv.Colour.FromArgb(0xFFC5E1A5);
    public static readonly Inv.Colour LightGreen300 = Inv.Colour.FromArgb(0xFFAED581);
    public static readonly Inv.Colour LightGreen400 = Inv.Colour.FromArgb(0xFF9CCC65);
    public static readonly Inv.Colour LightGreen500 = Inv.Colour.FromArgb(0xFF8BC34A);
    public static readonly Inv.Colour LightGreen600 = Inv.Colour.FromArgb(0xFF7CB342);
    public static readonly Inv.Colour LightGreen700 = Inv.Colour.FromArgb(0xFF689F38);
    public static readonly Inv.Colour LightGreen800 = Inv.Colour.FromArgb(0xFF558B2F);
    public static readonly Inv.Colour LightGreen900 = Inv.Colour.FromArgb(0xFF33691E);
    public static readonly Inv.Colour Lime50 = Inv.Colour.FromArgb(0xFFF9FBE7);
    public static readonly Inv.Colour Lime100 = Inv.Colour.FromArgb(0xFFF0F4C3);
    public static readonly Inv.Colour Lime200 = Inv.Colour.FromArgb(0xFFE6EE9C);
    public static readonly Inv.Colour Lime300 = Inv.Colour.FromArgb(0xFFDCE775);
    public static readonly Inv.Colour Lime400 = Inv.Colour.FromArgb(0xFFD4E157);
    public static readonly Inv.Colour Lime500 = Inv.Colour.FromArgb(0xFFCDDC39);
    public static readonly Inv.Colour Lime600 = Inv.Colour.FromArgb(0xFFC0CA33);
    public static readonly Inv.Colour Lime700 = Inv.Colour.FromArgb(0xFFAFB42B);
    public static readonly Inv.Colour Lime800 = Inv.Colour.FromArgb(0xFF9E9D24);
    public static readonly Inv.Colour Lime900 = Inv.Colour.FromArgb(0xFF827717);
    public static readonly Inv.Colour Yellow50 = Inv.Colour.FromArgb(0xFFFFFDE7);
    public static readonly Inv.Colour Yellow100 = Inv.Colour.FromArgb(0xFFFFF9C4);
    public static readonly Inv.Colour Yellow200 = Inv.Colour.FromArgb(0xFFFFF59D);
    public static readonly Inv.Colour Yellow300 = Inv.Colour.FromArgb(0xFFFFF176);
    public static readonly Inv.Colour Yellow400 = Inv.Colour.FromArgb(0xFFFFEE58);
    public static readonly Inv.Colour Yellow500 = Inv.Colour.FromArgb(0xFFFFEB3B);
    public static readonly Inv.Colour Yellow600 = Inv.Colour.FromArgb(0xFFFDD835);
    public static readonly Inv.Colour Yellow700 = Inv.Colour.FromArgb(0xFFFBC02D);
    public static readonly Inv.Colour Yellow800 = Inv.Colour.FromArgb(0xFFF9A825);
    public static readonly Inv.Colour Yellow900 = Inv.Colour.FromArgb(0xFFF57F17);
    public static readonly Inv.Colour Amber50 = Inv.Colour.FromArgb(0xFFFFF8E1);
    public static readonly Inv.Colour Amber100 = Inv.Colour.FromArgb(0xFFFFECB3);
    public static readonly Inv.Colour Amber200 = Inv.Colour.FromArgb(0xFFFFE082);
    public static readonly Inv.Colour Amber300 = Inv.Colour.FromArgb(0xFFFFD54F);
    public static readonly Inv.Colour Amber400 = Inv.Colour.FromArgb(0xFFFFCA28);
    public static readonly Inv.Colour Amber500 = Inv.Colour.FromArgb(0xFFFFC107);
    public static readonly Inv.Colour Amber600 = Inv.Colour.FromArgb(0xFFFFB300);
    public static readonly Inv.Colour Amber700 = Inv.Colour.FromArgb(0xFFFFA000);
    public static readonly Inv.Colour Amber800 = Inv.Colour.FromArgb(0xFFFF8F00);
    public static readonly Inv.Colour Amber900 = Inv.Colour.FromArgb(0xFFFF6F00);
    public static readonly Inv.Colour Orange50 = Inv.Colour.FromArgb(0xFFFFF3E0);
    public static readonly Inv.Colour Orange100 = Inv.Colour.FromArgb(0xFFFFE0B2);
    public static readonly Inv.Colour Orange200 = Inv.Colour.FromArgb(0xFFFFCC80);
    public static readonly Inv.Colour Orange300 = Inv.Colour.FromArgb(0xFFFFB74D);
    public static readonly Inv.Colour Orange400 = Inv.Colour.FromArgb(0xFFFFA726);
    public static readonly Inv.Colour Orange500 = Inv.Colour.FromArgb(0xFFFF9800);
    public static readonly Inv.Colour Orange600 = Inv.Colour.FromArgb(0xFFFB8C00);
    public static readonly Inv.Colour Orange700 = Inv.Colour.FromArgb(0xFFF57C00);
    public static readonly Inv.Colour Orange800 = Inv.Colour.FromArgb(0xFFEF6C00);
    public static readonly Inv.Colour Orange900 = Inv.Colour.FromArgb(0xFFE65100);
    public static readonly Inv.Colour DeepOrange50 = Inv.Colour.FromArgb(0xFFFBE9E7);
    public static readonly Inv.Colour DeepOrange100 = Inv.Colour.FromArgb(0xFFFFCCBC);
    public static readonly Inv.Colour DeepOrange200 = Inv.Colour.FromArgb(0xFFFFAB91);
    public static readonly Inv.Colour DeepOrange300 = Inv.Colour.FromArgb(0xFFFF8A65);
    public static readonly Inv.Colour DeepOrange400 = Inv.Colour.FromArgb(0xFFFF7043);
    public static readonly Inv.Colour DeepOrange500 = Inv.Colour.FromArgb(0xFFFF5722);
    public static readonly Inv.Colour DeepOrange600 = Inv.Colour.FromArgb(0xFFF4511E);
    public static readonly Inv.Colour DeepOrange700 = Inv.Colour.FromArgb(0xFFE64A19);
    public static readonly Inv.Colour DeepOrange800 = Inv.Colour.FromArgb(0xFFD84315);
    public static readonly Inv.Colour DeepOrange900 = Inv.Colour.FromArgb(0xFFBF360C);
    public static readonly Inv.Colour Brown50 = Inv.Colour.FromArgb(0xFFEFEBE9);
    public static readonly Inv.Colour Brown100 = Inv.Colour.FromArgb(0xFFD7CCC8);
    public static readonly Inv.Colour Brown200 = Inv.Colour.FromArgb(0xFFBCAAA4);
    public static readonly Inv.Colour Brown300 = Inv.Colour.FromArgb(0xFFA1887F);
    public static readonly Inv.Colour Brown400 = Inv.Colour.FromArgb(0xFF8D6E63);
    public static readonly Inv.Colour Brown500 = Inv.Colour.FromArgb(0xFF795548);
    public static readonly Inv.Colour Brown600 = Inv.Colour.FromArgb(0xFF6D4C41);
    public static readonly Inv.Colour Brown700 = Inv.Colour.FromArgb(0xFF5D4037);
    public static readonly Inv.Colour Brown800 = Inv.Colour.FromArgb(0xFF4E342E);
    public static readonly Inv.Colour Brown900 = Inv.Colour.FromArgb(0xFF3E2723);
    public static readonly Inv.Colour Grey50 = Inv.Colour.FromArgb(0xFFFAFAFA);
    public static readonly Inv.Colour Grey100 = Inv.Colour.FromArgb(0xFFF5F5F5);
    public static readonly Inv.Colour Grey200 = Inv.Colour.FromArgb(0xFFEEEEEE);
    public static readonly Inv.Colour Grey300 = Inv.Colour.FromArgb(0xFFE0E0E0);
    public static readonly Inv.Colour Grey400 = Inv.Colour.FromArgb(0xFFBDBDBD);
    public static readonly Inv.Colour Grey500 = Inv.Colour.FromArgb(0xFF9E9E9E);
    public static readonly Inv.Colour Grey600 = Inv.Colour.FromArgb(0xFF757575);
    public static readonly Inv.Colour Grey700 = Inv.Colour.FromArgb(0xFF616161);
    public static readonly Inv.Colour Grey800 = Inv.Colour.FromArgb(0xFF424242);
    public static readonly Inv.Colour Grey900 = Inv.Colour.FromArgb(0xFF212121);
    public static readonly Inv.Colour BlueGrey50 = Inv.Colour.FromArgb(0xFFECEFF1);
    public static readonly Inv.Colour BlueGrey100 = Inv.Colour.FromArgb(0xFFCFD8DC);
    public static readonly Inv.Colour BlueGrey200 = Inv.Colour.FromArgb(0xFFB0BEC5);
    public static readonly Inv.Colour BlueGrey300 = Inv.Colour.FromArgb(0xFF90A4AE);
    public static readonly Inv.Colour BlueGrey400 = Inv.Colour.FromArgb(0xFF78909C);
    public static readonly Inv.Colour BlueGrey500 = Inv.Colour.FromArgb(0xFF607D8B);
    public static readonly Inv.Colour BlueGrey600 = Inv.Colour.FromArgb(0xFF546E7A);
    public static readonly Inv.Colour BlueGrey700 = Inv.Colour.FromArgb(0xFF455A64);
    public static readonly Inv.Colour BlueGrey800 = Inv.Colour.FromArgb(0xFF37474F);
    public static readonly Inv.Colour BlueGrey900 = Inv.Colour.FromArgb(0xFF26323);
  }
}