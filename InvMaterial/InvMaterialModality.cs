﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Inv.Support;

namespace Inv.Material
{
  public sealed class Modality : Inv.Panel<Inv.Overlay>
  {
    public Modality()
    {
      this.Window = Inv.Application.Access().Window;
      this.ActiveLayerCount = 0;
      this.LayerList = new Inv.DistinctList<ModalityLayer>();

      this.Base = Inv.Overlay.New();

      this.ContentFrame = Inv.Frame.New();
      Base.AddPanel(ContentFrame);
    }

    public Inv.Panel Content
    {
      get => ContentFrame.Content;
      set => ContentFrame.Content = value;
    }
    public bool IsActivated { get; private set; }
    public event Action ActivateEvent;
    public event Action DeactivateEvent;

    public void Post(Action Action)
    {
      Window.Post(Action);
    }
    public ModalityLayer NewLayer()
    {
      return new ModalityLayer(this);
    }
    public ModalityFlyout NewFlyout()
    {
      return new ModalityFlyout(this);
    }

    internal ModalityLayer LastExclusiveLayer()
    {
      return LayerList.Where(O => O.IsExclusive).LastOrDefault();
    }
    internal void AddLayer(ModalityLayer Layer)
    {
      Debug.Assert(!Layer.IsActive, "Layer must not already be active.");

      var PreviousLayer = LastExclusiveLayer();

      if (Layer.IsExclusive)
      {
        if (PreviousLayer == null)
          Window.PreventInput(ContentFrame);
        else
          Window.PreventInput(PreviousLayer.Frame);
      }

      LayerList.Add(Layer);

      Base.AddPanel(Layer.Frame);

      try
      {
        Layer.ShowInvoke();
      }
      finally
      {
        Layer.ExecuteFocus();

        if (Layer.IsExclusive)
        {
          if (ActiveLayerCount == 0)
          {
            this.IsActivated = true;

            ActivateEvent?.Invoke();
          }

          ActiveLayerCount++;
        }
      }
    }
    internal void RemoveLayer(ModalityLayer Layer)
    {
      Debug.Assert(Layer.IsActive, "Layer must already be active.");

      LayerList.Remove(Layer);

      var PreviousLayer = LastExclusiveLayer();

      if (Layer.IsExclusive)
      {
        if (PreviousLayer == null)
          Window.AllowInput(ContentFrame);
        else
          Window.AllowInput(PreviousLayer.Frame);
      }

      Base.RemovePanel(Layer.Frame);

      Layer.HideInvoke();

      if (Layer.IsExclusive)
      {
        ActiveLayerCount--;

        if (ActiveLayerCount == 0)
        {
          this.IsActivated = false;

          DeactivateEvent?.Invoke();
        }
      }
    }
    internal bool HasLayer(ModalityLayer Layer)
    {
      return LayerList.Contains(Layer);
    }

    private readonly Inv.Window Window;
    private readonly Inv.Frame ContentFrame;
    private readonly Inv.DistinctList<ModalityLayer> LayerList;
    private int ActiveLayerCount;
  }

  public sealed class ModalityLayer
  {
    internal ModalityLayer(Modality Canvas)
    {
      this.Canvas = Canvas;
      this.Frame = Inv.Frame.New();
    }

    public Inv.Panel Content
    {
      get => Frame.Content;
      set => Frame.Content = value;
    }
    public Inv.Margin Margin => Frame.Margin;
    public Inv.Padding Padding => Frame.Padding;
    public Inv.Size Size => Frame.Size;
    public Inv.Visibility Visibility => Frame.Visibility;
    public Inv.Alignment Alignment => Frame.Alignment;
    public Inv.Background Background => Frame.Background;
    public Inv.Focus PrimaryFocus { get; set; }
    public bool IsExclusive { get; set; }
    public bool IsActive { get; private set; }
    public event Action ShowEvent;
    public event Action HideEvent;
    public event Action AdjustEvent
    {
      add => Frame.AdjustEvent += value;
      remove => Frame.AdjustEvent -= value;
    }

    public void Show()
    {
      if (!IsActive)
      {
        try
        {
          try
          {
            Canvas.AddLayer(this);
          }
          finally
          {
            IsActive = true;
          }
        }
        catch
        {
          if (Canvas.HasLayer(this))
            Hide();

          throw;
        }
      }
    }
    public void Hide()
    {
      if (IsActive)
      {
        Canvas.RemoveLayer(this);

        IsActive = false;
      }
    }
    public Inv.Dimension GetDimension() => Frame.GetDimension();

    internal Inv.Frame Frame { get; private set; }
    internal bool HasExecuteFocus { get; private set; }

    internal void ShowInvoke()
    {
      ShowEvent?.Invoke();
    }
    internal void HideInvoke()
    {
      HideEvent?.Invoke();
    }
    internal void ExecuteFocus()
    {
      this.HasExecuteFocus = false;

      if (PrimaryFocus != null)
        PrimaryFocus.Set();

      this.HasExecuteFocus = true;
    }

    private readonly Modality Canvas;
  }

  public sealed class ModalityFlyout
  {
    internal ModalityFlyout(Modality Canvas)
    {
      this.Base = Canvas.NewLayer();
      Base.IsExclusive = true;
      Base.Background.Colour = Inv.Colour.Black.Opacity(0.20F);

      this.LayoutOverlay = Inv.Overlay.New();
      Base.Content = LayoutOverlay;

      this.LayoutButton = Inv.Button.NewFlat();
      LayoutOverlay.AddPanel(LayoutButton);
      LayoutButton.Alignment.Stretch();
      LayoutButton.Background.Colour = Inv.Colour.Transparent;
      LayoutButton.SingleTapEvent += () => Base.Hide();

      this.LayoutFrame = Inv.Frame.New();
      LayoutOverlay.AddPanel(LayoutFrame);
      LayoutFrame.Alignment.Center();
    }

    public Inv.Margin Margin => LayoutFrame.Margin;
    public Inv.Padding Padding => LayoutFrame.Padding;
    public Inv.Size Size => LayoutFrame.Size;
    public Inv.Visibility Visibility => LayoutFrame.Visibility;
    public Inv.Alignment Alignment => LayoutFrame.Alignment;
    public Inv.Background Background => LayoutFrame.Background;
    public Inv.Focus PrimaryFocus
    {
      set => Base.PrimaryFocus = value;
      get => Base.PrimaryFocus;
    }
    public Inv.Panel Content
    {
      get => LayoutFrame.Content;
      set => LayoutFrame.Content = value;
    }
    public event Action ShowEvent
    {
      add => Base.ShowEvent += value;
      remove => Base.ShowEvent -= value;
    }
    public event Action HideEvent
    {
      add => Base.HideEvent += value;
      remove => Base.HideEvent -= value;
    }

    public void Show() => Base.Show();
    public void Hide() => Base.Hide();
    public Inv.Dimension GetDimension() => LayoutFrame.GetDimension();

    private readonly ModalityLayer Base;
    private readonly Inv.Overlay LayoutOverlay;
    private readonly Inv.Frame LayoutFrame;
    private readonly Inv.Button LayoutButton;
  }
}