﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inv.Support;
using System.Diagnostics;

namespace Inv.Material
{
  public sealed class ListFlow : Inv.Panel<Inv.Flow>
  {
    public ListFlow()
    {
      this.Base = new Inv.Flow();
    }

    public Inv.Size Size => Base.Size;
    public Inv.Background Background => Base.Background;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;

    public ListFlowSection<T> AddSection<T>() => new ListFlowSection<T>(Base);
  }

  public sealed class ListFlow<T> : Inv.Panel<Inv.Flow>
  {
    public ListFlow()
    {
      this.Base = new Inv.Flow();
      this.Section = new ListFlowSection<T>(Base);
    }

    public Inv.Size Size => Base.Size;
    public Inv.Background Background => Base.Background;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Margin Margin => Base.Margin;
    public event Action<ListItem<T>> ComposeEvent
    {
      add => Section.ComposeEvent += value;
      remove => Section.ComposeEvent -= value;
    }

    public void Clear() => Section.Clear();
    public void Load(IEnumerable<T> Contexts) => Section.Load(Contexts);
    public void Reload() => Section.Reload();

    private readonly ListFlowSection<T> Section;
  }

  public sealed class ListFlowSection<T>
  {
    internal ListFlowSection(Inv.Flow Flow)
    {
      this.Flow = Flow;

      this.IsEnabledField = true;
      this.ItemList = new Inv.DistinctList<ListItem<T>>();

      this.CachedSection = Flow.AddCachedSection<T>();
      CachedSection.Template(() =>
      {
        var Item = new ListItem<T>();
        SetSize(Item.Size, Flow.GetDimension());
        ItemList.Add(Item);
        return Item;
      }, (I, R) =>
      {
        I.Context = R;
        I.IsEnabled = IsEnabledField;
        ComposeEvent?.Invoke(I);
      });

      Flow.AdjustEvent += () =>
      {
        var Dimension = Flow.GetDimension();

        foreach (var Item in ItemList)
          SetSize(Item.Size, Dimension);

        SetSize(HeaderPanel, Dimension);
        SetSize(FooterPanel, Dimension);
      };
    }

    public bool IsEnabled
    {
      get => IsEnabledField;
      set
      {
        if (IsEnabledField != value)
        {
          this.IsEnabledField = value;
          Reload();
        }
      }
    }
    public event Action<ListItem<T>> ComposeEvent;

    public void Clear() => CachedSection.Clear();
    public void Load(IEnumerable<T> Contexts) => CachedSection.Load(Contexts);
    public void Reload() => CachedSection.Reload();
    /// <summary>
    /// Set the header panel for the section.
    /// </summary>
    /// <param name="HeaderPanel"></param>
    public void SetHeader(Inv.Panel HeaderPanel)
    {
      this.HeaderPanel = HeaderPanel;
      CachedSection.SetHeader(HeaderPanel);

      SetSize(HeaderPanel);
    }
    /// <summary>
    /// Set the footer panel for the section.
    /// </summary>
    /// <param name="FooterPanel"></param>
    public void SetFooter(Inv.Panel FooterPanel)
    {
      this.FooterPanel = FooterPanel;
      CachedSection.SetFooter(FooterPanel);

      SetSize(FooterPanel);
    }

    private void SetSize(Inv.Panel Panel, Inv.Dimension? Dimension = null)
    {
      SetSize(Panel?.Control.Size, Dimension ?? Flow.GetDimension());
    }
    private void SetSize(Inv.Size Size, Inv.Dimension Dimension)
    {
      if (Size == null || Dimension == Inv.Dimension.Zero)
        return;

      Size.SetWidth(Dimension.Width);
    }

    private readonly Inv.Flow Flow;
    private readonly Inv.CachedSection<T> CachedSection;
    private readonly Inv.DistinctList<ListItem<T>> ItemList;
    private Inv.Panel HeaderPanel;
    private Inv.Panel FooterPanel;
    private bool IsEnabledField;
  }

  public sealed class ListItem<T> : Inv.Panel<Inv.Frame>
  {
    public ListItem()
    {
      this.Base = Inv.Frame.New();

      this.Subheader = new ListSubheader();

      this.IsEnabledField = true;

      void Highlight(bool IsOver, bool HasFocus)
      {
        Base.Background.In(IsEnabledField && (IsOver || HasFocus) ? Inv.Theme.Light.WhiteSmoke : Inv.Colour.Transparent);
      }

      this.Tile = new ListTile();
      Tile.Focus.GotEvent += () => Highlight(Tile.IsOver, true);
      Tile.Focus.LostEvent += () => Highlight(Tile.IsOver, false);
      Tile.OverEvent += () => Highlight(true, Tile.Focus.Has());
      Tile.AwayEvent += () => Highlight(false, Tile.Focus.Has());

      this.Content = new ListContent();

      this.Check = new ListCheck();
      Check.Focus.GotEvent += () => Highlight(Check.IsOver, true);
      Check.Focus.LostEvent += () => Highlight(Check.IsOver, false);
      Check.OverEvent += () => Highlight(true, Check.Focus.Has());
      Check.AwayEvent += () => Highlight(false, Check.Focus.Has());
    }

    public T Context { get; internal set; }

    public ListTile AsTile()
    {
      return SetContent(Tile);
    }
    public ListContent AsContent()
    {
      return SetContent(Content);
    }
    public ListSubheader AsSubheader()
    {
      return SetContent(Subheader);
    }
    public ListCheck AsCheck()
    {
      return SetContent(Check);
    }

    internal bool IsEnabled
    {
      get => IsEnabledField;
      set
      {
        if (IsEnabledField != value)
        {
          this.IsEnabledField = value;
          Tile.IsEnabled = IsEnabled;
          Check.IsEnabled = IsEnabled;
        }
      }
    }
    internal Inv.Size Size => Base.Size;

    private void Clear()
    {
      Tile.Clear();
      Check.Clear();
      Content.Clear();
      Subheader.Clear();
    }
    private TPanel SetContent<TPanel>(TPanel Panel)
      where TPanel : Inv.Panel
    {
      Clear();

      Base.Content = Panel;

      return Panel;
    }

    private readonly ListTile Tile;
    private readonly ListContent Content;
    private readonly ListSubheader Subheader;
    private readonly ListCheck Check;
    private bool IsEnabledField;
  }

  public sealed class ListTile : Inv.Panel<ListContent>
  {
    public ListTile()
    {
      this.Base = new ListContent();
      Base.IsFocusable = true;
    }

    public string Title
    {
      get => Base.Title;
      set => Base.Title = value;
    }
    public string Description
    {
      get => Base.Description;
      set => Base.Description = value;
    }
    public ListContentLayout Layout => Base.Layout;
    public Inv.Tooltip Tooltip => Base.Tooltip;
    public event Action SingleTapEvent
    {
      add => Base.SingleTapEvent += value;
      remove => Base.SingleTapEvent -= value;
    }
    public event Action ContextTapEvent
    {
      add => Base.ContextTapEvent += value;
      remove => Base.ContextTapEvent -= value;
    }

    public void Dye(Inv.Colour Colour) => Base.Dye(Colour);
    public void LeadNone()
    {
      Base.LeadNone();
    }
    public void LeadIcon(Inv.Image Image = null, Inv.Colour Colour = null)
    {
      Base.LeadIcon(Image, Colour);
    }
    public void LeadAvatar(Inv.Image Image) => Base.LeadAvatar(Image);
    public void TitleIn(Inv.Colour Colour) => Base.TitleIn(Colour);
    public void TrailNone()
    {
      Base.TrailNone();
    }
    public void TrailIcon(Inv.Image Image, Action SingleTapAction = null)
    {
      Base.TrailIcon(Image, SingleTapAction);
    }
    public void TrailMetadata(string Metadata, Inv.Colour Colour = null)
    {
      Base.TrailMetadata(Metadata, Colour);
    }

    internal bool IsEnabled
    {
      get => Base.IsEnabled;
      set => Base.IsEnabled = value;
    }
    internal bool IsOver => Base.IsOver;
    internal Inv.Focus Focus => Base.Focus;
    internal event Action OverEvent
    {
      add => Base.OverEvent += value;
      remove => Base.OverEvent -= value;
    }
    internal event Action AwayEvent
    {
      add => Base.AwayEvent += value;
      remove => Base.AwayEvent -= value;
    }

    internal void Clear()
    {
      Base.Clear();
    }
  }

  public sealed class ListContent : Inv.Panel<Inv.Button>
  {
    public ListContent()
    {
      this.IsEnabledField = true;

      this.DyeColour = Inv.Colour.Transparent;
      this.TitleColour = Inv.Theme.OnBackgroundColour;
      this.DescriptionColour = Inv.Theme.SubtleColour;

      this.Layout = new ListContentLayout();
      Layout.ChangeEvent += () => Refresh();

      this.Base = Inv.Button.NewStark();
      Base.Padding.Set(0, 0, 10, 0);
      Base.SingleTapEvent += () =>
      {
        if (IsEnabledField)
          SingleTapEvent?.Invoke();
      };

      this.Dock = Inv.Dock.NewHorizontal();
      Base.Content = Dock;

      this.LeadingFrame = Inv.Frame.New();
      Dock.AddHeader(LeadingFrame);
      LeadingFrame.Size.SetWidth(72);
      LeadingFrame.Alignment.StretchLeft();
      LeadingFrame.Visibility.Collapse();

      this.TextStack = Inv.Stack.NewVertical();
      Dock.AddClient(TextStack);
      TextStack.Alignment.CenterStretch();
      TextStack.Margin.Set(0, 0, 16, 0);

      this.TitleLabel = Inv.Label.New();
      TextStack.AddPanel(TitleLabel);
      TitleLabel.LineWrapping = false;

      this.DescriptionLabel = Inv.Label.New();
      TextStack.AddPanel(DescriptionLabel);
      DescriptionLabel.LineWrapping = true;
      DescriptionLabel.Margin.Set(0, 4, 0, 0);

      this.TrailingFrame = Inv.Frame.New();
      Dock.AddFooter(TrailingFrame);
      TrailingFrame.Margin.Set(0, 0, 0, 0);
      TrailingFrame.Visibility.Collapse();

      Clear();

      Refresh();
    }

    public string Title
    {
      get => TitleLabel.Text;
      set => TitleLabel.Text = value;
    }
    public string Description
    {
      get => DescriptionLabel.Text;
      set => DescriptionLabel.Text = value;
    }
    public bool IsEnabled
    {
      get => IsEnabledField;
      set
      {
        if (IsEnabledField != value)
        {
          this.IsEnabledField = value;
          Refresh();
        }
      }
    }
    public Inv.Codepoint Codepoint => Base.Codepoint;
    public Inv.Tooltip Tooltip => Base.Tooltip;
    public ListContentLayout Layout { get; }

    public void Dye(Inv.Colour Colour)
    {
      this.DyeColour = Colour ?? Inv.Colour.Transparent;
      this.TitleColour = Colour?.BackgroundToBlackWhiteForeground() ?? Inv.Theme.OnBackgroundColour;
      this.DescriptionColour = Colour?.Lighten(0.10F) ?? Inv.Theme.SubtleColour;

      Refresh();
    }
    public void LeadNone()
    {
      this.LeadingContentType = LeadingType.None;

      LeadingFrame.Content = null;
      LeadingFrame.Visibility.Collapse();

      this.LeadingIcon = null;
      this.LeadingAvatarGraphic = null;

      Refresh();
    }
    public void LeadIcon(Inv.Image Image = null, Inv.Colour Colour = null)
    {
      this.LeadingContentType = LeadingType.Icon;

      if (LeadingIcon == null)
      {
        this.LeadingIcon = new Inv.Material.Icon();
        LeadingIcon.Margin.Set(16, 0, 16, 0);
        LeadingIcon.Size.Set(24);
        LeadingIcon.Alignment.Center();
      }

      if (Image != null)
        LeadingIcon.Image = Image;

      this.LeadingIconColour = Colour ?? Inv.Material.Theme.PrimaryColour;

      LeadingFrame.Content = LeadingIcon;
      LeadingFrame.Visibility.Show();

      this.LeadingAvatarGraphic = null;

      Refresh();
    }
    public void LeadAvatar(Inv.Image Image)
    {
      this.LeadingContentType = LeadingType.Avatar;

      if (LeadingAvatarGraphic == null)
      {
        this.LeadingAvatarGraphic = Inv.Graphic.New();
        LeadingAvatarGraphic.Margin.Set(16, 0, 16, 0);
      }

      if (Image == null && EmptyAvatarImage == null)
        EmptyAvatarImage = Inv.Application.Access().Graphics.Tint(Inv.Material.Resources.Images.AccountCircle, Inv.Colour.DimGray);

      LeadingAvatarGraphic.Image = Image ?? EmptyAvatarImage;

      LeadingFrame.Content = LeadingAvatarGraphic;
      LeadingFrame.Visibility.Show();

      this.LeadingIcon = null;

      Refresh();
    }
    public void TitleIn(Inv.Colour Colour) => TitleLabel.Font.In(Colour);
    public void TrailNone()
    {
      TrailingFrame.Content = null;
      TrailingFrame.Visibility.Collapse();
    }
    public void TrailIcon(Inv.Image Image, Action SingleTapAction = null)
    {
      var TrailIconButton = new Inv.Material.IconButton();
      TrailIconButton.Alignment.Center();
      TrailIconButton.Large();
      TrailIconButton.Image = Image;

      if (SingleTapAction != null)
        TrailIconButton.SingleTapEvent += () => SingleTapAction?.Invoke();

      TrailingFrame.Background.In(null);
      TrailingFrame.Content = TrailIconButton;
      TrailingFrame.Visibility.Show();
    }
    public void TrailCustom(Inv.Panel Content)
    {
      TrailingFrame.Content = Content;
      TrailingFrame.Visibility.Set(Content != null);
    }
    public void TrailMetadata(string Metadata, Inv.Colour Colour = null)
    {
      var MetadataLabel = Inv.Label.New();
      MetadataLabel.Margin.Set(8, 16, 8, 0);
      MetadataLabel.Alignment.TopRight();
      MetadataLabel.Font.Custom(13).In(Colour ?? Inv.Theme.SubtleColour);
      MetadataLabel.Text = Metadata;

      TrailingFrame.Content = MetadataLabel;
      TrailingFrame.Visibility.Show();
    }

    internal bool IsOver => Base.IsOver;
    internal bool IsFocusable
    {
      get => Base.IsFocusable;
      set => Base.IsFocusable = value;
    }
    internal Inv.Focus Focus => Base.Focus;
    internal Inv.Size Size => Base.Size;
    internal event Action OverEvent
    {
      add => Base.OverEvent += value;
      remove => Base.OverEvent -= value;
    }
    internal event Action AwayEvent
    {
      add => Base.AwayEvent += value;
      remove => Base.AwayEvent -= value;
    }
    internal event Action SingleTapEvent;
    internal event Action ContextTapEvent
    {
      add => Base.ContextTapEvent += value;
      remove => Base.ContextTapEvent -= value;
    }

    internal void Clear()
    {
      Title = null;
      Description = null;

      LeadNone();
      TrailNone();

      this.SingleTapEvent = null;

      Base.RemoveContextTap();

      Layout.Clear();

      Refresh();
    }

    private void Refresh()
    {
      Base.Background.In(DyeColour);

      LeadingFrame.Visibility.Set(LeadingFrame.Content != null);

      TitleLabel.Font.Size = Layout.IsDense ? 13 : 16;
      TitleLabel.Font.In(IsEnabledField ? TitleColour : Inv.Theme.SubtleColour);

      DescriptionLabel.Visibility.Set(Layout.Format != ListContentLayoutFormat.SingleLine);
      DescriptionLabel.LineWrapping = Layout.Format == ListContentLayoutFormat.ThreeLine;
      DescriptionLabel.Font.Size = Layout.IsDense ? 13 : 14;
      DescriptionLabel.Font.In(IsEnabledField ? DescriptionColour : Inv.Theme.SoftColour);
      DescriptionLabel.Size.SetMaximumHeight(40);

      if (LeadingIcon != null)
      {
        LeadingIcon.Colour = IsEnabledField ? LeadingIconColour : Inv.Theme.SubtleColour;
        LeadingIcon.Size.Set(Layout.IsDense ? (Layout.HasAvatars ? 36 : 20) : (Layout.HasAvatars ? 40 : 24));

        Dock.Padding.Set(0);
      }
      else if (LeadingAvatarGraphic != null)
      {
        LeadingAvatarGraphic.Size.Set(Layout.IsDense ? 36 : 40);

        if (Layout.Format == ListContentLayoutFormat.ThreeLine)
          LeadingAvatarGraphic.Alignment.TopCenter();
        else
          LeadingAvatarGraphic.Alignment.Center();

        Dock.Padding.Set(0);
      }
      else
      {
        Dock.Padding.Set(16, 0, 0, 0);
      }

      var DividerHeight = Layout.ShowDivider ? 1 : 0;
      Base.Border.Set(0, 0, 0, DividerHeight);
      Base.Border.Colour = Inv.Colour.Black.AdjustAlpha((int)(0.12 * 255));

      int ContentHeight;

      if (Layout.ContentHeight != null)
      {
        ContentHeight = Layout.ContentHeight.Value;
      }
      else if (Layout.Format == ListContentLayoutFormat.SingleLine)
      {
        if (LeadingContentType == LeadingType.Avatar)
          ContentHeight = Layout.IsDense ? 48 : 56;
        else
          ContentHeight = Layout.IsDense ? 40 : 48;
      }
      else if (Layout.Format == ListContentLayoutFormat.TwoLine)
      {
        ContentHeight = Layout.IsDense ? 60 : 72;
      }
      else
      {
        ContentHeight = Layout.IsDense ? 76 : 88;
      }

      Base.Size.SetHeight(ContentHeight + DividerHeight);
    }

    private readonly Inv.Dock Dock;
    private readonly Inv.Stack TextStack;
    private readonly Inv.Label TitleLabel;
    private readonly Inv.Label DescriptionLabel;
    private readonly Inv.Frame LeadingFrame;
    private readonly Inv.Frame TrailingFrame;
    private Inv.Material.Icon LeadingIcon;
    private Inv.Graphic LeadingAvatarGraphic;
    private LeadingType LeadingContentType;
    private bool IsEnabledField;
    private Inv.Colour DyeColour;
    private Inv.Colour TitleColour;
    private Inv.Colour DescriptionColour;
    private Inv.Colour LeadingIconColour;

    private enum LeadingType
    {
      None, Icon, Avatar
    }

    private static Inv.Image EmptyAvatarImage;
  }

  public sealed class ListContentLayout
  {
    internal ListContentLayout()
    {
    }

    public bool IsDense
    {
      get => IsDenseField;
      set
      {
        if (value != IsDenseField)
        {
          IsDenseField = value;
          ChangeInvoke();
        }
      }
    }
    public bool HasAvatars
    {
      get => HasAvatarsField;
      set
      {
        if (value != HasAvatarsField)
        {
          HasAvatarsField = value;
          ChangeInvoke();
        }
      }
    }
    public int? ContentHeight
    {
      get => ContentHeightField;
      set
      {
        if (value != ContentHeightField)
        {
          ContentHeightField = value;
          ChangeInvoke();
        }
      }
    }
    public bool ShowDivider
    {
      get => ShowDividerField;
      set
      {
        if (value != ShowDividerField)
        {
          ShowDividerField = value;
          ChangeInvoke();
        }
      }
    }

    public void FormatOneLine()
    {
      this.Format = ListContentLayoutFormat.SingleLine;

      ChangeInvoke();
    }
    public void FormatTwoLine()
    {
      this.Format = ListContentLayoutFormat.TwoLine;

      ChangeInvoke();
    }
    public void FormatThreeLine()
    {
      this.Format = ListContentLayoutFormat.ThreeLine;

      ChangeInvoke();
    }

    internal ListContentLayoutFormat Format { get; private set; }
    internal event Action ChangeEvent;

    internal void Clear()
    {
      this.Format = ListContentLayoutFormat.SingleLine;
      this.IsDenseField = Inv.Application.Access().Device.Keyboard;
      this.HasAvatarsField = false;
      this.ContentHeightField = null;
      this.ShowDividerField = false;

      ChangeInvoke();
    }

    private void ChangeInvoke()
    {
      ChangeEvent?.Invoke();
    }

    private bool IsDenseField;
    private bool HasAvatarsField;
    private int? ContentHeightField;
    private bool ShowDividerField;
  }

  internal enum ListContentLayoutFormat
  {
    SingleLine, TwoLine, ThreeLine
  }

  public sealed class ListSubheader : Inv.Panel<Inv.Button>
  {
    public ListSubheader()
    {
      void RefreshColour() => Base.Background.In(CanExpandField && Base.IsOver && ChangeAction != null ? Inv.Theme.Light.WhiteSmoke : Inv.Colour.Transparent);

      this.Base = Inv.Button.NewStark();
      Base.Size.SetHeight(48);
      Base.OverEvent += () => RefreshColour();
      Base.AwayEvent += () => RefreshColour();

      this.Dock = Inv.Dock.NewHorizontal();
      Base.Content = Dock;

      this.TrailIcon = new Inv.Material.Icon();
      Dock.AddFooter(TrailIcon);
      TrailIcon.Margin.Set(0, 0, 12, 0);
      TrailIcon.Size.Set(24);
      TrailIcon.Alignment.CenterRight();
      TrailIcon.Visibility.Collapse();

      this.ChevronIcon = new Inv.Material.Icon();
      Dock.AddFooter(ChevronIcon);
      ChevronIcon.Margin.Set(0, 0, 8, 0);
      ChevronIcon.Size.Set(24);
      ChevronIcon.Alignment.CenterRight();

      this.Label = Inv.Label.New();
      Dock.AddClient(Label);
      Label.Margin.Set(16, 0, 16, 0);
      Label.Font.Custom(13).Medium().In(Inv.Material.Theme.PrimaryColour);

      this.CanExpandField = true;
      this.IsExpandedField = true;

      RefreshIcon();
      RefreshChangeAction();
    }

    public Inv.Margin Margin => Base.Margin;
    public Inv.Border Border => Base.Border;
    public Inv.Size Size => Base.Size;
    public bool IsExpanded
    {
      get => IsExpandedField;
      set
      {
        if (IsExpandedField != value)
        {
          IsExpandedField = value;
          RefreshIcon();
        }
      }
    }
    public bool CanExpand
    {
      get => CanExpandField;
      set
      {
        if (CanExpandField != value)
        {
          CanExpandField = value;
          RefreshIcon();
        }
      }
    }
    public string Text
    {
      get => Label.Text;
      set => Label.Text = value;
    }
    public Inv.Image TrailImage
    {
      get => TrailIcon.Image;
      set
      {
        TrailIcon.Image = value;
        TrailIcon.Visibility.Set(TrailIcon.Image != null);
      }
    }
    public Inv.Tooltip TrailTooltip => TrailIcon.Tooltip;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ChangeEvent
    {
      add
      {
        ChangeAction += value;
        RefreshChangeAction();
      }
      remove
      {
        ChangeAction -= value;
        RefreshChangeAction();
      }
    }

    internal void Clear()
    {
      Text = null;

      TrailImage = null;
      TrailTooltip.Content = null;

      ChangeAction = null;

      IsExpandedField = true;

      RefreshChangeAction();
    }

    private void RefreshIcon()
    {
      ChevronIcon.Image = IsExpandedField ? Resources.Images.ExpandLess : Resources.Images.ExpandMore;
      ChevronIcon.Visibility.Set(CanExpandField && ChangeAction != null);
    }
    private void SingleTapInvoke()
    {
      if (!CanExpandField)
        return;

      IsExpanded = !IsExpanded;
      RefreshIcon();
      ChangeAction?.Invoke();
    }
    private void RefreshChangeAction()
    {
      RefreshIcon();

      if (ChangeAction != null && !Base.HasSingleTap())
        Base.SingleTapEvent += SingleTapInvoke;
      else if (ChangeAction == null && Base.HasSingleTap())
        Base.SingleTapEvent -= SingleTapInvoke;
    }

    private readonly Inv.Dock Dock;
    private readonly Inv.Label Label;
    private readonly Inv.Material.Icon TrailIcon;
    private readonly Inv.Material.Icon ChevronIcon;
    private bool CanExpandField;
    private bool IsExpandedField;
    private event Action ChangeAction;
  }

  public sealed class ListCheck : Inv.Panel<ListContent>
  {
    public ListCheck()
    {
      this.IsEnabledField = true;

      this.Base = new ListContent();
      Base.IsFocusable = true;

      Clear();
    }

    public bool IsChecked
    {
      get => IsCheckedField;
      set
      {
        if (value != IsCheckedField)
        {
          IsCheckedField = value;

          Refresh();
        }
      }
    }
    public string Title
    {
      get => Base.Title;
      set => Base.Title = value;
    }
    public Inv.Tooltip Tooltip => Base.Tooltip;
    public string Description
    {
      get => Base.Description;
      set => Base.Description = value;
    }
    public event Action ChangeEvent;

    public void FormatOneLine() => Base.Layout.FormatOneLine();
    public void FormatTwoLine() => Base.Layout.FormatTwoLine();
    public void FormatThreeLine() => Base.Layout.FormatThreeLine();

    internal bool IsEnabled
    {
      get => IsEnabledField;
      set
      {
        if (IsEnabledField != value)
        {
          this.IsEnabledField = value;
          Refresh();
        }
      }
    }
    internal bool IsOver => Base.IsOver;
    internal Inv.Focus Focus => Base.Focus;
    internal event Action OverEvent
    {
      add => Base.OverEvent += value;
      remove => Base.OverEvent -= value;
    }
    internal event Action AwayEvent
    {
      add => Base.AwayEvent += value;
      remove => Base.AwayEvent -= value;
    }

    internal void Clear()
    {
      IsChecked = false;
      ChangeEvent = null;

      Base.Clear();

      // Initialise the check box state.
      Base.Size.SetMinimumHeight(48);
      Base.SingleTapEvent += () =>
      {
        if (IsEnabled)
        {
          IsChecked = !IsChecked;
          ChangeEvent?.Invoke();
        }
      };

      Refresh();
    }

    private void Refresh()
    {
      Base.LeadIcon(IsCheckedField ? Resources.Images.CheckBox : Resources.Images.CheckBoxOutlineBlank, IsEnabled ? null : Inv.Theme.SubtleColour);
    }

    private bool IsCheckedField;
    private bool IsEnabledField;
  }

  public sealed class ListRadioFlow<T> : Inv.Panel<Inv.Flow>
  {
    public ListRadioFlow()
    {
      this.Base = new Inv.Flow();

      this.Section = new Inv.Material.ListRadioFlowSection<T>(Base);
    }

    public T SelectedContext
    {
      get => Section.SelectedContext;
      set => Section.SelectedContext = value;
    }
    /// <summary>
    /// Specifies the title of the null row, and also that null is available for selection.
    /// <para>Note: Currently this must be set prior to calling Load().</para>
    /// </summary>
    public string NullTitle
    {
      get => Section.NullTitle;
      set => Section.NullTitle = value;
    }
    public bool IsEnabled
    {
      get => Section.IsEnabled;
      set => Section.IsEnabled = value;
    }
    public ListSearch<T> Search => Section.Search;
    public IEqualityComparer<T> EqualityComparer
    {
      get => Section.EqualityComparer;
      set => Section.EqualityComparer = value;
    }
    public Inv.Size Size => Base.Size;
    public Inv.Padding Padding => Base.Padding;
    public event Action<ListSelectItem<T>> ComposeEvent
    {
      add => Section.ComposeEvent += value;
      remove => Section.ComposeEvent -= value;
    }
    /// <summary>
    /// Fires whenever a row is single-tapped.
    /// <para>Note: this is fired even if the already selected row is tapped again.</para>
    /// </summary>
    public event Action SingleTapEvent
    {
      add => Section.SingleTapEvent += value;
      remove => Section.SingleTapEvent -= value;
    }
    /// <summary>
    /// Fires whenever SelectedContext changes by a different row being selected.
    /// <para>Note: this does not fire when the already selected row is tapped again.</para>
    /// </summary>
    public event Action SelectionChangeEvent
    {
      add => Section.SelectionChangeEvent += value;
      remove => Section.SelectionChangeEvent -= value;
    }
    public event Action<Inv.Tooltip, T> ShowTooltipEvent
    {
      add => Section.ShowTooltipEvent += value;
      remove => Section.ShowTooltipEvent -= value;

    }

    public void OrderByTitle() => Section.OrderByTitle();
    public void OrderByComposition() => Section.OrderByComposition();
    public void OrderBy(Comparison<T> OrderByComparison) => Section.OrderBy(OrderByComparison);
    public ListCollation<T, string> GroupBy(Func<T, string> KeyQuery) => Section.GroupBy(KeyQuery);
    public ListCollation<T, G> GroupBy<G>(Func<T, G> KeyQuery, Action<ListHeader<T, G>> HeaderAction) => Section.GroupBy(KeyQuery, HeaderAction);
    public void Load(IEnumerable<T> Values) => Section.Load(Values);
    /// <summary>
    /// Set the header panel for the section.
    /// </summary>
    /// <param name="HeaderPanel"></param>
    public void SetHeader(Inv.Panel HeaderPanel) => Section.SetHeader(HeaderPanel);
    /// <summary>
    /// Set the footer panel for the section.
    /// </summary>
    /// <param name="FooterPanel"></param>
    public void SetFooter(Inv.Panel FooterPanel) => Section.SetFooter(FooterPanel);

    internal bool IsEqualTo(T A, T B) => Section.IsEqualTo(A, B);
    internal string GetTitle(T Value, bool IncludeHeader) => Section.GetTitle(Value, IncludeHeader);

    private readonly ListRadioFlowSection<T> Section;
  }

  public sealed class ListRadioFlowSection<T>
  {
    internal ListRadioFlowSection(Inv.Flow Flow)
    {
      this.Search = new ListSearch<T>();
      Search.ChangeEvent += () => RefreshItems();

      void Select(T Context)
      {
        if (!IsEqualTo(this.SelectedContext, Context))
        {
          this.SelectedContext = Context;
          Base.Reload();
          SelectionChangeEvent?.Invoke();
        }

        SingleTapEvent?.Invoke();
      }

      this.Base = new Inv.Material.ListFlowSection<ListSelectItem<T>>(Flow);
      Base.ComposeEvent += (Item) =>
      {
        var SelectItem = Item.Context;

        if (SelectItem.IsNull)
        {
          var Tile = Item.AsTile();
          Tile.Title = NullTitle;
          Tile.TitleIn(Inv.Theme.Light.DimGray);
          Tile.LeadIcon(SelectedContext == null ? Resources.Images.RadioButtonChecked : Resources.Images.RadioButtonUnchecked);
          Tile.SingleTapEvent += () => Select(default);
        }
        else if (SelectItem.IsHeader)
        {
          var Subheader = Item.AsSubheader();
          Subheader.Text = $"{SelectItem.HeaderText} ({SelectItem.Header.ChildItem.Count(I => I.IsSearchMatch)})";
          Subheader.TrailImage = SelectItem.Header.Image;

          if (!string.IsNullOrWhiteSpace(SelectItem.Header.ImageTooltip))
          {
            var Tooltip = new Inv.Material.TooltipText();
            Subheader.TrailTooltip.Content = Tooltip;
            Tooltip.Text = SelectItem.Header.ImageTooltip;
          }

          Subheader.CanExpand = !Search.Active;
          Subheader.IsExpanded = Search.Active || SelectItem.HeaderExpanded;
          Subheader.ChangeEvent += () =>
          {
            SelectItem.HeaderExpanded = Subheader.IsExpanded;
            RefreshItems();
          };
        }
        else
        {
          var Context = SelectItem.Context;

          var ListTile = Item.AsTile();
          ListTile.Title = SelectItem.Title;
          ListTile.Description = SelectItem.Description;
          ListTile.LeadIcon(IsEqualTo(Context, SelectedContext) ? Resources.Images.RadioButtonChecked : Resources.Images.RadioButtonUnchecked);
          ListTile.SingleTapEvent += () => Select(Context);
          ListTile.Tooltip.ShowEvent += () => ShowTooltipEvent?.Invoke(ListTile.Tooltip, Context);
          if (ListTile.Description != null)
            ListTile.Layout.FormatTwoLine();
          else
            ListTile.Layout.FormatOneLine();
        }
      };
    }

    public T SelectedContext
    {
      get => SelectedContextField;
      set
      {
        this.SelectedContextField = value;
        if (IsLoaded)
          Base.Reload();
      }
    }
    /// <summary>
    /// Specifies the title of the null row, and also that null is available for selection.
    /// <para>Note: Currently this must be set prior to calling Load().</para>
    /// </summary>
    public string NullTitle { get; set; }
    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set => Base.IsEnabled = value;
    }
    public ListSearch<T> Search { get; }
    public IEqualityComparer<T> EqualityComparer { get; set; }
    public event Action<ListSelectItem<T>> ComposeEvent;
    /// <summary>
    /// Fires whenever a row is single-tapped.
    /// <para>Note: this is fired even if the already selected row is tapped again.</para>
    /// </summary>
    public event Action SingleTapEvent;
    /// <summary>
    /// Fires whenever SelectedContext changes by a different row being selected.
    /// <para>Note: this does not fire when the already selected row is tapped again.</para>
    /// </summary>
    public event Action SelectionChangeEvent;
    public event Action<Inv.Tooltip, T> ShowTooltipEvent;

    public void OrderByTitle()
    {
      this.OrderByTitleField = true;
      this.OrderByComparison = null;
    }
    public void OrderByComposition()
    {
      this.OrderByTitleField = false;
      this.OrderByComparison = null;
    }
    public void OrderBy(Comparison<T> OrderByComparison)
    {
      this.OrderByTitleField = false;
      this.OrderByComparison = OrderByComparison;
    }
    public ListCollation<T, string> GroupBy(Func<T, string> KeyQuery)
    {
      return GroupBy(KeyQuery, H => H.Title = H.Context);
    }
    public ListCollation<T, G> GroupBy<G>(Func<T, G> KeyQuery, Action<ListHeader<T, G>> HeaderAction)
    {
      var Result = new ListCollation<T, G>(KeyQuery, HeaderAction);
      this.GroupByCollation = Result;
      return Result;
    }
    public void Load(IEnumerable<T> Values)
    {
      if (TraceClass.IsActive)
      {
        var ValueCount = Values?.Count() ?? 0;
        TraceClass.Enter($"{ValueCount} {"value".Plural(ValueCount)}");
      }

      if (Values == null)
        this.MasterItemList = new Inv.DistinctList<ListSelectItem<T>>();
      else if (GroupByCollation != null)
        this.MasterItemList = GroupByCollation.GetItems(Values);
      else
        this.MasterItemList = Values.Select(R => ListSelectItem<T>.NewInline(R)).ToDistinctList();

      if (TraceClass.IsActive)
        TraceClass.Write($"compiled");

      foreach (var Item in MasterItemList.Where(I => !I.IsHeader))
      {
        if (ComposeEvent != null)
          ComposeEvent(Item);
        else
          Item.Title = Item.Context.ToString();
      }

      if (!string.IsNullOrWhiteSpace(NullTitle))
        MasterItemList.Add(ListSelectItem<T>.NewNull());

      if (TraceClass.IsActive)
        TraceClass.Write($"composed");

      MasterItemList.Sort((A, B) =>
      {
        var Result = 0;

        if (GroupByCollation != null)
        {
          if (GroupByCollation.HasCompare())
            Result = GroupByCollation.Compare(A.HeaderContext ?? A.HeaderItem.HeaderContext, B.HeaderContext ?? B.HeaderItem.HeaderContext);
          else
            Result = A.HeaderText.CompareTo(B.HeaderText);

          if (Result == 0)
            Result = A.IsHeader.CompareTo(B.IsHeader) * -1; // Headers first.
        }

        if (Result == 0 && !A.IsHeader)
          Result = A.IsNull.CompareTo(B.IsNull) * -1; // Nulls first.

        if (Result == 0 && !A.IsHeader && !A.IsNull)
        {
          if (OrderByTitleField)
            Result = A.Title.CompareTo(B.Title);
          else if (OrderByComparison != null)
            Result = OrderByComparison(A.Context, B.Context);
        }

        return Result;
      });

      if (TraceClass.IsActive)
        TraceClass.Write($"sorted");

      Search.SetItems(MasterItemList);

      if (TraceClass.IsActive)
        TraceClass.Write($"set search");

      RefreshItems();

      if (TraceClass.IsActive)
        TraceClass.Write($"set visibility");

      this.IsLoaded = true;

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    /// <summary>
    /// Set the header panel for the section.
    /// </summary>
    /// <param name="HeaderPanel"></param>
    public void SetHeader(Inv.Panel HeaderPanel) => Base.SetHeader(HeaderPanel);
    /// <summary>
    /// Set the footer panel for the section.
    /// </summary>
    /// <param name="FooterPanel"></param>
    public void SetFooter(Inv.Panel FooterPanel) => Base.SetFooter(FooterPanel);

    internal bool IsEqualTo(T A, T B)
    {
      if (EqualityComparer != null)
        return EqualityComparer.Equals(A, B);

      return object.Equals(A, B);
    }
    internal string GetTitle(T Value, bool IncludeHeader)
    {
      if (Value == null)
        return NullTitle;

      var Result = "";

      if (IncludeHeader && GroupByCollation != null)
        Result = GroupByCollation.GetHeader(Value) + " | ";

      if (ComposeEvent != null)
      {
        var SelectItem = ListSelectItem<T>.NewInline(Value);
        ComposeEvent(SelectItem);
        Result += SelectItem.Title;
      }
      else
      {
        Result += Value.ToString();
      }

      return Result;
    }

    private void RefreshItems()
    {
      Base.Load(MasterItemList.Where(I => I.IsVisible(Search.Active)));
    }

    private readonly Inv.Material.ListFlowSection<ListSelectItem<T>> Base;
    private Inv.DistinctList<ListSelectItem<T>> MasterItemList;
    private T SelectedContextField;
    private bool OrderByTitleField;
    private Comparison<T> OrderByComparison;
    private ListCollation<T> GroupByCollation;
    private bool IsLoaded;

    private static readonly Inv.TraceClass TraceClass = Inv.Trace.NewClass<Inv.Material.ListRadioFlowSection<T>>(Inv.Trace.PlatformFeature);
  }

  public sealed class ListSearch<T>
  {
    internal ListSearch()
    {
    }

    public bool Active => !string.IsNullOrWhiteSpace(LastTerm);

    public void Filter(string Term)
    {
      var NewTerm = Term.EmptyOrWhitespaceAsNull();

      if (!string.Equals(LastTerm, NewTerm, StringComparison.CurrentCultureIgnoreCase))
      {
        RefreshItems(NewTerm);

        ChangeEvent?.Invoke();
      }
    }

    internal event Action ChangeEvent;

    internal void SetItems(IEnumerable<ListSelectItem<T>> Items)
    {
      this.ItemArray = Items.ToArray();

      RefreshItems(LastTerm);
    }

    private void RefreshItems(string Term)
    {
      this.LastTerm = Term;

      if (!string.IsNullOrWhiteSpace(Term))
      {
        var SearchArray = Term.Split(' ', StringSplitOptions.RemoveEmptyEntries);

        bool IsSearchMatch(string Value)
        {
          var ValueArray = Value.Split(new[] { ' ', '.', '\\' }, StringSplitOptions.RemoveEmptyEntries);

          foreach (var SearchTerm in SearchArray)
          {
            if (!ValueArray.Any(V => V.StartsWith(SearchTerm, StringComparison.CurrentCultureIgnoreCase)))
              return false;
          }

          return true;
        }

        ItemArray.Where(I => !I.IsHeader).ForEach(I => I.IsSearchMatch = IsSearchMatch(I.Title.NullAsEmpty()) || IsSearchMatch(I.Description.NullAsEmpty()));
      }
      else
      {
        ItemArray.ForEach(I => I.IsSearchMatch = true);
      }
    }

    private ListSelectItem<T>[] ItemArray;
    private string LastTerm;
  }

  public sealed class ListCheckFlow<T> : Inv.Panel<Inv.Material.ListFlow<ListSelectItem<T>>>
  {
    public ListCheckFlow()
    {
      this.Search = new ListSearch<T>();
      Search.ChangeEvent += () => RefreshItems();

      this.CheckedSet = new HashSet<T>();

      this.Base = new Inv.Material.ListFlow<ListSelectItem<T>>();
      Base.ComposeEvent += (Item) =>
      {
        var SelectItem = Item.Context;

        if (SelectItem.IsHeader)
        {
          var Subheader = Item.AsSubheader();
          Subheader.Text = $"{SelectItem.HeaderText} ({SelectItem.Header.ChildItem.Count(I => I.IsSearchMatch)})";
          Subheader.CanExpand = !Search.Active;
          Subheader.IsExpanded = Search.Active || SelectItem.HeaderExpanded;
          Subheader.ChangeEvent += () =>
          {
            SelectItem.HeaderExpanded = Subheader.IsExpanded;
            RefreshItems();
          };
        }
        else
        {
          var Context = SelectItem.Context;

          var ListCheck = Item.AsCheck();
          ListCheck.Title = SelectItem.Title;
          ListCheck.Description = SelectItem.Description;
          ListCheck.IsChecked = CheckedSet.Contains(Context);
          ListCheck.ChangeEvent += () =>
          {
            CheckedSet.Toggle(Context);
            CheckChangeEvent?.Invoke();
          };

          if (string.IsNullOrWhiteSpace(SelectItem.Description))
            ListCheck.FormatOneLine();
          else
            ListCheck.FormatTwoLine();
        }
      };
    }

    public IEnumerable<T> SelectedContexts
    {
      get => CheckedSet.ToDistinctList();
      set
      {
        if (value == null)
        {
          CheckedSet.Clear();
          return;
        }

        CheckedSet = value.ToHashSetX(EqualityComparer);
      }
    }
    public ListSearch<T> Search { get; }
    public IEqualityComparer<T> EqualityComparer
    {
      get => CheckedSet?.Comparer;
      set
      {
        if (EqualityComparer != value)
          this.CheckedSet = CheckedSet?.ToHashSetX(value) ?? new HashSet<T>(value);
      }
    }
    public event Action<ListSelectItem<T>> ComposeEvent;
    public event Action CheckChangeEvent;

    public void Load(IEnumerable<T> Values)
    {
      if (Values == null)
        this.MasterItemList = new Inv.DistinctList<ListSelectItem<T>>();
      else if (GroupByCollation != null)
        this.MasterItemList = GroupByCollation.GetItems(Values);
      else
        this.MasterItemList = Values.Select(R => ListSelectItem<T>.NewInline(R)).ToDistinctList();

      foreach (var Item in MasterItemList.Where(I => !I.IsHeader))
      {
        if (ComposeEvent != null)
          ComposeEvent(Item);
        else
          Item.Title = Item.Context.ToString();
      }

      var HasOrderBy = OrderByTitleField || OrderByComparison != null;

      MasterItemList.Sort((A, B) =>
      {
        var Result = 0;

        if (GroupByCollation != null)
        {
          Result = A.HeaderText.CompareTo(B.HeaderText);

          if (Result == 0)
            Result = A.IsHeader.CompareTo(B.IsHeader) * -1; // Headers first.
        }

        if (Result == 0)
          Result = (A.Context != null).CompareTo(B.Context != null);

        if (Result == 0 && !A.IsHeader && HasOrderBy)
          Result = OrderByTitleField ? A.Title.CompareTo(B.Title) : OrderByComparison(A.Context, B.Context);

        return Result;
      });

      Search.SetItems(MasterItemList);

      RefreshItems();
    }
    public void OrderByTitle()
    {
      this.OrderByTitleField = true;
      this.OrderByComparison = null;
    }
    public void OrderByComposition()
    {
      this.OrderByTitleField = false;
      this.OrderByComparison = null;
    }
    public void OrderBy(Func<T, T, int> CompareQuery)
    {
      this.OrderByComparison = CompareQuery;
      this.OrderByTitleField = false;
    }
    public ListCollation<T, string> GroupBy(Func<T, string> KeyQuery)
    {
      return GroupBy(KeyQuery, H => H.Title = H.Context);
    }
    public ListCollation<T, G> GroupBy<G>(Func<T, G> KeyQuery, Action<ListHeader<T, G>> HeaderAction)
    {
      var Result = new ListCollation<T, G>(KeyQuery, HeaderAction);
      this.GroupByCollation = Result;
      return Result;
    }

    /// <summary>
    /// Used by other Material components that need to render the titles of the selected items.
    /// </summary>
    /// <param name="Value"></param>
    /// <returns></returns>
    internal string GetTitle(T Value)
    {
      if (ComposeEvent != null)
      {
        var Item = ListSelectItem<T>.NewInline(Value);
        ComposeEvent(Item);
        return Item.Title;
      }

      return Value.ToString();
    }

    private void RefreshItems()
    {
      Base.Load(MasterItemList.Where(I => I.IsVisible(Search.Active)));
    }

    private bool OrderByTitleField;
    private Func<T, T, int> OrderByComparison;
    private ListCollation<T> GroupByCollation;
    private HashSet<T> CheckedSet;
    private Inv.DistinctList<ListSelectItem<T>> MasterItemList;
  }

  public sealed class ListSelectItem<T>
  {
    private ListSelectItem()
    {
      this.IsSearchMatch = true;
    }

    public T Context { get; private set; }
    public string Title { get; set; }
    public string Description { get; set; }

    internal bool IsNull { get; set; }
    internal ListHeader<T> Header { get; set; }
    internal bool IsSearchMatch { get; set; }
    internal bool IsHeader => Header != null;
    internal object HeaderContext => Header?.Context;
    internal string HeaderText => Header?.Title ?? HeaderItem?.HeaderText;
    internal bool HeaderExpanded
    {
      get => Header.IsExpanded;
      set => Header.IsExpanded = value;
    }

    internal bool IsVisible(bool IsSearchActive)
    {
      return (IsHeader && Header.ChildItem.Any(C => C.IsSearchMatch)) || (!IsHeader && IsSearchMatch && (HeaderItem == null || HeaderItem.HeaderExpanded || IsSearchActive));
    }

    internal ListSelectItem<T> HeaderItem { get; set; }

    internal static ListSelectItem<T> NewNull()
    {
      return new ListSelectItem<T>()
      {
        IsNull = true
      };
    }
    internal static ListSelectItem<T> NewHeader(ListHeader<T> Header)
    {
      return new ListSelectItem<T>()
      {
        Header = Header
      };
    }
    internal static ListSelectItem<T> NewInline(T Context)
    {
      return new ListSelectItem<T>()
      {
        Context = Context
      };
    }
    internal static ListSelectItem<T> NewGrouped(T Context, ListSelectItem<T> HeaderItem)
    {
      return new ListSelectItem<T>()
      {
        Context = Context,
        HeaderItem = HeaderItem
      };
    }
  }

  public sealed class ListHeader<T>
  {
    internal ListHeader(object Context)
    {
      this.Context = Context;
      this.ChildItem = new List<ListSelectItem<T>>();
    }

    public object Context { get; }
    public string Title { get; set; }
    public Inv.Image Image { get; set; }
    public string ImageTooltip { get; set; }
    public bool IsExpanded { get; set; }

    internal List<ListSelectItem<T>> ChildItem { get; set; }
  }

  public sealed class ListHeader<T, G>
  {
    internal ListHeader(ListHeader<T> Base)
    {
      this.Base = Base;
      this.Context = (G)Base.Context;
    }

    public G Context { get; }
    public string Title
    {
      get => Base.Title;
      set => Base.Title = value;
    }
    public Inv.Image Image
    {
      get => Base.Image;
      set => Base.Image = value;
    }
    public string ImageTooltip
    {
      get => Base.ImageTooltip;
      set => Base.ImageTooltip = value;
    }
    public bool IsExpanded
    {
      get => Base.IsExpanded;
      set => Base.IsExpanded = value;
    }

    private readonly ListHeader<T> Base;
  }

  public sealed class ListCollation<T>
  {
    internal ListCollation()
    {
    }

    internal event Func<T, object> KeyQuery;
    internal event Action<ListHeader<T>> ComposeEvent;
    internal event Comparison<object> CompareQuery;

    internal string GetHeader(T Context)
    {
      var Header = new ListHeader<T>(KeyQuery(Context));

      ComposeEvent(Header);

      return Header.Title;
    }
    internal Inv.DistinctList<ListSelectItem<T>> GetItems(IEnumerable<T> ContextList)
    {
      var Result = new Inv.DistinctList<ListSelectItem<T>>();

      foreach (var ContextGroup in ContextList.GroupBy(R => KeyQuery(R)))
      {
        var Header = new ListHeader<T>(ContextGroup.Key);
        ComposeEvent(Header);

        var HeaderItem = ListSelectItem<T>.NewHeader(Header);

        Result.Add(HeaderItem);

        foreach (var Context in ContextGroup)
          Header.ChildItem.Add(ListSelectItem<T>.NewGrouped(Context, HeaderItem));

        Result.AddRange(Header.ChildItem);
      }

      return Result;
    }
    internal bool HasCompare() => CompareQuery != null;
    internal int Compare(object A, object B)
    {
      if (CompareQuery != null)
        return CompareQuery(A, B);

      return 0;
    }
  }

  public sealed class ListCollation<T, G>
  {
    internal ListCollation(Func<T, G> KeyQuery, Action<ListHeader<T, G>> ComposeQuery)
    {
      this.Base = new ListCollation<T>();
      Base.KeyQuery += (Context) =>
      {
        if (KeyQuery != null)
          return KeyQuery(Context);

        return null;
      };
      if (ComposeQuery != null)
      {
        Base.ComposeEvent += (Header) =>
        {
          ComposeQuery(new ListHeader<T, G>(Header));
        };
      };
    }

    public event Comparison<G> CompareQuery
    {
      add
      {
        Debug.Assert(CompareQueryField == null, $"{nameof(CompareQueryField)} is already assigned");
        if (CompareQueryField == null)
        {
          CompareQueryField = value;
          Base.CompareQuery += Compare;
        }
      }
      remove
      {
        Debug.Assert(CompareQueryField != null, $"{nameof(CompareQueryField)} has not been assigned");
        if (CompareQueryField != null)
        {
          CompareQueryField = null;
          Base.CompareQuery -= Compare;
        }
      }
    }

    public static implicit operator ListCollation<T>(ListCollation<T, G> Collation) => Collation?.Base;

    private int Compare(object A, object B)
    {
      if (CompareQueryField != null)
        return CompareQueryField((G)A, (G)B);

      return 0;
    }

    private readonly ListCollation<T> Base;
    private Comparison<G> CompareQueryField;
  }
}