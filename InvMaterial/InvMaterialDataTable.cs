﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using Inv.Support;

namespace Inv.Material
{
  public sealed class DataTable : Inv.Panel<Inv.Flow>
  {
    public DataTable()
    {
      this.IsDenseField = false;

      this.Base = Inv.Flow.New();
      Base.Background.In(Inv.Theme.BackgroundColour);

      this.CheckedSet = new HashSet<object>();
      this.SortList = new Inv.DistinctList<DataSort>();
      this.ColumnList = new Inv.DistinctList<DataColumn>();
      this.ItemRowDictionary = new Dictionary<DataItem, DataContentRow>();
      this.ContextRowDictionary = new Dictionary<object, DataContentRow>();

      this.GroupNullHeaderItem = null;
      this.GroupHeaderItemDictionary = new Dictionary<object, DataItem>();

      this.Header = new DataHeader(this);
      Base.SetFixture(Header);

      this.InactiveContextRowList = new Inv.DistinctList<DataContentRow>();
      this.InactiveGroupRowList = new Inv.DistinctList<DataContentRow>();

      this.Section = Base.AddSection();
      Section.ItemQuery += (Index) =>
      {
        var Item = VisibleItemList.ElementAtOrDefault(Index);
        if (Item == null)
          return null;

        var Row = ItemRowDictionary.GetValueOrDefault(Item);
        if (Row == null)
        {
          Row = Item.IsGroup ? InactiveGroupRowList.RemoveFirstOrDefault() : InactiveContextRowList.RemoveFirstOrDefault();
          
          if (Row == null)
            Row = new DataContentRow(this);

          // Whether this is a new row or an inactive row, the Context dictionary must be updated for checking to work correctly.
          ItemRowDictionary.Add(Item, Row);
          if (!Item.IsGroup)
            ContextRowDictionary[Item.Context] = Row;
        }

        if (Item.IsGroup)
        {
          var GroupRow = Row.AsGroup();
          
          GroupRow.IsChecking = IsCheckingField;
          
          GroupRow.AttachCheckChange(() =>
          {
            foreach (var ChildContext in Item.Group.ContextSet)
            {
              if (GroupRow.IsChecked == null || GroupRow.IsChecked.Value)
                CheckedSet.Add(ChildContext);
              else
                CheckedSet.Remove(ChildContext);

              var ChildRow = ContextRowDictionary.GetValueOrDefault(ChildContext)?.AsContext();
              if (ChildRow != null)
              {
                ChildRow.IsChecking = IsCheckingField;
                ChildRow.IsChecked = GroupRow.IsChecked == null || GroupRow.IsChecked.Value;
              }
            }

            CheckChangeEvent?.Invoke();
          });

          GroupRow.AttachExpandChange(() =>
          {
            var Group = Item.Group;
            Group.IsExpanded = !Group.IsExpanded;

            // Use the current expand state rather than toggling the HashSet state,
            //   to handle the case where ExpandSet may have become out of sync.
            if (Group.IsExpanded)
              ActiveCollation.ExpandSet.Add(Group.Context);
            else
              ActiveCollation.ExpandSet.Remove(Group.Context);

            Reload();
          });

          if (ActiveCollation.ShowCount)
            GroupRow.Compose($"{Item.Group.Text} ({Item.Group.ContextSet.Count})", Item.Group.IsExpanded);
          else
            GroupRow.Compose(Item.Group.Text, Item.Group.IsExpanded);

          if (IsCheckingField)
            RefreshGroupChecks(Item);
        }
        else
        {
          var ContextRow = Row.AsContext();
          ContextRow.IsChecking = IsCheckingField;
          ContextRow.IsChecked = CheckedSet.Contains(Item.Context);
          ContextRow.IsCollated = ActiveCollation != null;
          ContextRow.Prepare(Item.Context);
          ContextRow.AttachCheckChange(() =>
          {
            if (ContextRow.IsChecked)
              CheckedSet.Add(ContextRow.Context);
            else
              CheckedSet.Remove(ContextRow.Context);

            if (ActiveCollation != null)
            {
              var GroupReference = ActiveCollation.Get(ContextRow.Context);
              var GroupHeaderItem = GetGroupHeaderItemByReference(GroupReference);
              if (GroupHeaderItem != null)
                RefreshGroupChecks(GroupHeaderItem);
            }

            CheckChangeEvent?.Invoke();
          });

          foreach (var Column in ColumnList)
            Column.Compose(ContextRow);
        }

        return Row;
      };

      Section.RecycleEvent += (Index, Panel) =>
      {
        var Row = Panel as Inv.Material.DataContentRow;
        if (Row != null)
          Row.Recycle();
      };

      this.FooterRow = new DataContentRow(this).AsFooter();
      Section.SetFooter(FooterRow);
    }

    public bool IsChecking
    {
      get => IsCheckingField;
      set
      {
        if (IsCheckingField != value)
        {
          this.IsCheckingField = value;

          Header.IsChecking = value;
          foreach (var Row in ContextRowDictionary.Values)
          {
            Row.ContextRow.IsChecking = value;

            if (!value)
              Row.ContextRow.IsChecked = false;
          }

          if (!value && CheckedSet.Any())
          {
            CheckedSet.Clear();
            CheckChangeEvent?.Invoke();
          }
        }
      }
    }
    /// <summary>
    /// Gets or sets the objects that are currently checked in the DataTable.
    /// <para>Note: When setting the checked objects, it is the caller's responsibility to reload the table as needed.</para>
    /// </summary>
    public IEnumerable<object> Checks
    {
      get => CheckedSet.ExceptNull(); // Just in case a header row null Context ends up in the checked set, we exclude it as it causes downstream issues.
      set
      {
        Base.Window.Application.RequireThreadAffinity();
        this.CheckedSet = new HashSet<object>(value ?? Array.Empty<object>(), EqualityComparerField);
      }
    }
    public object SelectedContext
    {
      get
      {
        Debug.Assert(!IsChecking, $"Querying {nameof(SelectedContext)} is not allowed when {nameof(IsChecking)} is true");

        var CheckArray = Checks.ToArray();
        if (CheckArray.Length != 1)
          return null;

        return CheckArray[0];
      }
      set
      {
        this.Checks = value != null ? new[] { value } : null;
      }
    }
    public IEqualityComparer<object> EqualityComparer
    {
      get => EqualityComparerField;
      set
      {
        this.EqualityComparerField = value;
        this.CheckedSet = new HashSet<object>(CheckedSet, value);
        this.ContextRowDictionary = new Dictionary<object, DataContentRow>(ContextRowDictionary, value);
        Reload();
      }
    }
    public string ActiveSortID => ActiveSort?.ID;
    public DataSortDirection ActiveSortDirection { get; private set; }
    public DataSort TiebreakSort { get; set; }
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Background Background => Base.Background;
    public Inv.Margin Margin => Base.Margin;
    public int TotalCount => ContextList.Count;
    public DataHeader Header { get; }
    public event Action AdjustEvent
    {
      add => Base.AdjustEvent += value;
      remove => Base.AdjustEvent -= value;
    }
    public event Action DoubleTapEvent;
    public event Action CheckChangeEvent;
    public event Action FooterEvent;

    public void Sparse()
    {
      this.IsDenseField = false;
    }
    public bool IsSparse() => !IsDenseField;
    public void Dense()
    {
      this.IsDenseField = true;
    }
    public bool IsDense() => IsDenseField;
    public BlockDataColumn AddBlockColumn(string ID, int Width)
    {
      return new BlockDataColumn(AddColumn(ID, Width));
    }
    public ButtonDataColumn AddButtonColumn(string ID, int Width)
    {
      return new ButtonDataColumn(AddColumn(ID, Width));
    }
    public ColourDataColumn AddColourColumn(string ID)
    {
      return new ColourDataColumn(AddColumn(ID, 32));
    }
    public CountDataColumn AddCountColumn(string ID, int Width)
    {
      return new CountDataColumn(AddColumn(ID, Width));
    }
    public DateDataColumn AddDateColumn(string ID, int Width)
    {
      return new DateDataColumn(AddColumn(ID, Width));
    }
    public DecimalDataColumn AddDecimalColumn(string ID, int Width)
    {
      return new DecimalDataColumn(AddColumn(ID, Width));
    }
    public ImageDataColumn AddImageColumn(string ID, int Width)
    {
      return new ImageDataColumn(AddColumn(ID, Width));
    }
    public IntegerDataColumn AddIntegerColumn(string ID, int Width)
    {
      return new IntegerDataColumn(AddColumn(ID, Width));
    }
    public MoneyDataColumn AddMoneyColumn(string ID, int Width)
    {
      return new MoneyDataColumn(AddColumn(ID, Width));
    }
    public StringDataColumn AddStringColumn(string ID, int Width)
    {
      return new StringDataColumn(AddColumn(ID, Width));
    }
    public TimeDataColumn AddTimeColumn(string ID, int Width)
    {
      return new TimeDataColumn(AddColumn(ID, Width));
    }
    public TimestampDataColumn AddTimestampColumn(string ID, int Width)
    {
      return new TimestampDataColumn(AddColumn(ID, Width));
    }
    public RecyclePanelDataColumn<TPanel> AddRecyclePanelColumn<TPanel>(string ID, int Width)
      where TPanel : Inv.Panel, new()
    {
      return new RecyclePanelDataColumn<TPanel>(AddColumn(ID, Width));
    }
    public void RemoveColumns()
    {
      ColumnList.Clear();

      this.ActiveSort = null;
      this.ActiveSortDirection = DataSortDirection.Ascending;
    }
    public DataSort AddSort(string ID, Comparison<object> Comparison)
    {
      Debug.Assert(Comparison != null, $"{nameof(Comparison)} must be provided");

      var Result = new DataSort(this, ID, Comparison);
      SortList.Add(Result);
      return Result;
    }
    public DataSort AddSort(DataColumnContract Column, Comparison<object> Comparison)
    {
      Debug.Assert(Comparison != null, $"{nameof(Comparison)} must be provided");

      var ColumnControl = Column.Control;
      if (ColumnControl.Sort != null)
        SortList.Remove(ColumnControl.Sort);

      var Result = AddSort(ColumnControl.ID, Comparison);
      ColumnControl.Sort = Result;
      return Result;
    }
    public DataSort FindSort(string ID) => SortList.Find(S => S.ID.Equals(ID, StringComparison.InvariantCultureIgnoreCase));
    public DataCollationControl AddCollation(Func<object, object> GetQuery)
    {
      Debug.Assert(GetQuery != null, $"{nameof(GetQuery)} must be provided");
      return new DataCollationControl(this, GetQuery);
    }
    public void RemoveGrouping()
    {
      if (ActiveCollation != null)
      {
        this.ActiveCollation = null;

        RefreshGroups();
        RefreshSort();

        Reload();
      }
    }
    public void Load(IEnumerable<object> Contexts)
    {
      if (DebugHelper.IsDebugMode())
      {
        // Ensure there are no clashes on column IDs.
        var ColumnGroupList = ColumnList.GroupBy(C => C.ID, StringComparer.InvariantCultureIgnoreCase).Where(G => G.Count() > 1);
        if (ColumnGroupList.Any())
          throw new Exception($"The following column IDs have been registered multiple times: {ColumnGroupList.Select(G => $"'{G.Key}'").OrderBy().AsSeparatedText(", ")}");

        // Ensure there are no clashes on sort IDs.
        var SortGroupList = SortList.GroupBy(S => S.ID, StringComparer.InvariantCultureIgnoreCase).Where(G => G.Count() > 1);
        if (SortGroupList.Any())
          throw new Exception($"The following sort IDs have been registered multiple times: {SortGroupList.Select(G => $"'{G.Key}'").OrderBy().AsSeparatedText(", ")}");
      }

      RefreshDensity();

      InactiveContextRowList.AddRange(ItemRowDictionary.Values.Where(R => R.ContextRow != null));
      InactiveGroupRowList.AddRange(ItemRowDictionary.Values.Where(R => R.GroupRow != null));
      ItemRowDictionary.Clear();
      ContextRowDictionary.Clear();

      foreach (var Column in ColumnList)
        Column.Recycle();

      this.ContextList = Contexts?.ToList() ?? new List<object>();

      RefreshGroups();
      RefreshSort();
      RefreshFooter();

      Header.Set(ColumnList.Select(C => C.HeaderCell));

      Reload();
    }
    public void Reload()
    {
      if (ItemList != null)
      {
        this.VisibleItemList = ItemList.Where(I => I.IsVisible()).ToList();
        Section.SetItemCount(VisibleItemList.Count);
      }
      RefreshFooter();
    }
    public void RemoveRow(object Context)
    {
      if (ItemList != null)
      {
        var Item = ItemList.Find(I => I.Context == Context);
        if (Item != null)
        {
          ItemList.Remove(Item);

          var InCheckSet = CheckedSet.Remove(Item.Context);
          if (InCheckSet)
            CheckChangeEvent?.Invoke();

          if (ActiveCollation != null)
          {
            // In the case that this item is the last remaining item in its group, the group header also must be removed.

            var GroupItemArray = ItemList.Where(I => ActiveCollation.Compare(I.Group.Context, Item.Group.Context) == 0).ToArray();
            if (GroupItemArray.Length == 1 && GroupItemArray[0].IsGroup)
            {
              ItemList.Remove(GroupItemArray[0]);
            }
            else
            {
              var GroupReference = ActiveCollation.Get(Item.Context);
              var GroupItem = GetGroupHeaderItemByReference(GroupReference);
              if (GroupItem != null)
                GroupItem.Group.ContextSet.Remove(Item.Context);
            }
          }
        }
      }

      Reload();
    }
    public void RemoveRows()
    {
      Load(Array.Empty<object>());
    }
    public IEnumerable<object> Retrieve() => ContextList;
    public void ExportCsv(Inv.File File)
    {
      using (var CsvWriter = File.AsCsv().Create())
      {
        var Context = new ExportContext();
        Context.WriteColumnHeaderPrefixEvent += () => CsvWriter.WriteRecordBegin();
        Context.WriteColumnHeaderContentEvent += (Column, ColumnIndex) => CsvWriter.WriteRecordField(Column.Title);
        Context.WriteColumnHeaderSuffixEvent += () => CsvWriter.WriteRecordEnd();
        Context.WriteItemPrefixEvent += () => CsvWriter.WriteRecordBegin();
        Context.WriteItemCellEvent += (Cell, ColumnIndex, ItemIndex) =>
        {
          var ExportText = string.Empty;

          if (Cell != null)
          {
            var ExportValue = Cell.Value;

            switch (Cell.Type)
            {
              case null:
                break;

              case DataExportType.Date:
                ExportText = ((Inv.Date?)ExportValue)?.ToString("dd/MM/yyyy");
                break;

              case DataExportType.Decimal:
                ExportText = ((decimal?)ExportValue)?.ToString();
                break;

              case DataExportType.Integer:
                ExportText = ((int?)ExportValue)?.ToString();
                break;

              case DataExportType.Integer64:
                ExportText = ((long?)ExportValue)?.ToString();
                break;

              case DataExportType.Money:
                ExportText = ((Inv.Money?)ExportValue)?.ToString();
                break;

              case DataExportType.Text:
                ExportText = (string)ExportValue;
                break;

              case DataExportType.Time:
                ExportText = ((Inv.Time?)ExportValue)?.ToString("HH:mm");
                break;

              case DataExportType.Timestamp:
                ExportText = ((DateTimeOffset?)ExportValue)?.ToString("dd/MM/yyyy HH:mm:ss.FFFFFFF zzz");
                break;

              default:
                throw EnumHelper.UnexpectedValueException(Cell.Type);
            }
          }

          CsvWriter.WriteRecordField(string.IsNullOrWhiteSpace(ExportText) ? string.Empty : ExportText);
        };
        Context.WriteItemSuffixEvent += () => CsvWriter.WriteRecordEnd();

        Export(Context);
      }
    }
    public void ExportExcel(Inv.File File)
    {
      using (var ExcelFile = Inv.Office.ExcelFile.Create(File, TrackEvents: false))
      using (var ExcelSheet = ExcelFile.AddSheet("Sheet"))
      {
        var Context = new ExportContext();

        Context.WriteColumnHeaderContentEvent += (Column, Index) =>
        {
          // Inv.Office.ExcelFile is 1-based.
          var RowNumber = 1;
          // Note: The +1 is because the column index passed to this event is 0-based, but Inv.Office.ExcelFile is 1-based.
          var ColumnNumber = Index + 1;

          ExcelSheet.GetCell(RowNumber, ColumnNumber).ValueAsString = Column.Title;
        };

        Context.WriteItemCellEvent += (Cell, ColumnIndex, ItemIndex) =>
        {
          // Note: The +2 can be accounted for as follows:
          //   +1 for the header row
          //   +1 because ColumnIndex is 0-based and Inv.Office.ExcelFile is 1-based.
          var RowNumber = ItemIndex + 2;
          // Note: The +1 is because the column index passed to this event is 0-based, but Inv.Office.ExcelFile is 1-based.
          var ColumnNumber = ColumnIndex + 1;

          var ExportValue = Cell?.Value;
          if (ExportValue != null)
          {
            var ExcelCell = ExcelSheet.GetCell(RowNumber, ColumnNumber);

            switch (Cell.Type)
            {
              case null:
                break;

              case DataExportType.Date:
                ExcelCell.ValueAsDate = (Inv.Date)ExportValue;
                break;

              case DataExportType.Decimal:
                ExcelCell.ValueAsDecimal = (decimal)ExportValue;
                break;

              case DataExportType.Integer:
                ExcelCell.ValueAsInteger32 = (int)ExportValue;
                break;

              case DataExportType.Integer64:
                ExcelCell.ValueAsInteger64 = (long)ExportValue;
                break;

              case DataExportType.Money:
                ExcelCell.ValueAsMoney = (Inv.Money)ExportValue;
                break;

              case DataExportType.Text:
                ExcelCell.ValueAsString = (string)ExportValue;
                break;

              case DataExportType.Time:
                ExcelCell.ValueAsTime = (Inv.Time)ExportValue;
                break;

              case DataExportType.Timestamp:
                // Excel doesn't support time zones as a concept.
                ExcelCell.ValueAsDateTime = ((DateTimeOffset)ExportValue).LocalDateTime;
                break;

              default:
                throw EnumHelper.UnexpectedValueException(Cell.Type);
            }
          }
        };
        
        Export(Context);

        ExcelFile.Save();
      }
    }
    public Inv.Dimension GetDimension() => Base.GetDimension();
    public void RemoveChecks() => CheckedSet.Clear();

    internal DataColumn AddColumn(string ID, int Width)
    {
      Debug.Assert(!string.IsNullOrWhiteSpace(ID), $"{nameof(ID)} must be provided");

      var Result = new DataColumn(this, ID);
      Result.HorizontalPadding = GetColumnPadding();

      ColumnList.Add(Result);

      Result.Width = Width;

      return Result;
    }
    internal IEnumerable<DataColumn> GetColumns() => ColumnList;
    internal void SelectRow(DataContextRow Row)
    {
      var Keyboard = Inv.Application.Access().Keyboard;
      if (IsChecking && Keyboard.KeyModifier.IsCtrl)
      {
        this.LastSelectContext = Row.Context;

        Row.IsChecked = !Row.IsChecked;
        if (Row.IsChecked)
          CheckedSet.Add(Row.Context);
        else
          CheckedSet.Remove(Row.Context);

        CheckChangeEvent?.Invoke();
      }
      else if (IsChecking && Keyboard.KeyModifier.IsShift && LastSelectContext != null)
      {
        var LastSelectIndex = ItemList.FindIndex(I => I.Context == LastSelectContext);
        var ThisIndex = ItemList.FindIndex(I => I.Context == Row.Context);
        var BottomIndex = Math.Min(LastSelectIndex, ThisIndex);
        var TopIndex = Math.Max(LastSelectIndex, ThisIndex);

        var WasCheckedSet = CheckedSet.ToHashSetX(EqualityComparerField);

        var CheckItemSet = ItemList.Skip(BottomIndex).Take(TopIndex - BottomIndex + 1).Where(I => !I.IsGroup);
        foreach (var CheckItem in CheckItemSet)
        {
          var CheckRow = ItemRowDictionary.GetValueOrDefault(CheckItem);
          if (CheckRow != null && CheckRow.ContextRow != null)
            CheckRow.ContextRow.IsChecked = true;

          WasCheckedSet.Remove(CheckItem.Context);
        }

        foreach (var UncheckContext in WasCheckedSet)
        {
          var UncheckRow = ContextRowDictionary.GetValueOrDefault(UncheckContext);
          if (UncheckRow != null && UncheckRow.ContextRow != null)
            UncheckRow.ContextRow.IsChecked = false;
        }

        CheckedSet.Clear();
        CheckedSet.AddRange(CheckItemSet.Select(I => I.Context));

        CheckChangeEvent?.Invoke();
      }
      else
      {
        if (CheckedSet.Count == 1 && CheckedSet.Contains(Row.Context))
        {
          DoubleTapEvent?.Invoke();
        }
        else
        {
          this.LastSelectContext = Row.Context;

          foreach (var UncheckContext in CheckedSet.Except(Row.Context, EqualityComparerField))
          {
            var UncheckRow = ContextRowDictionary.GetValueOrDefault(UncheckContext);
            if (UncheckRow != null && UncheckRow.ContextRow != null)
              UncheckRow.ContextRow.IsChecked = false;
          }

          CheckedSet.Clear();
          CheckedSet.Add(Row.Context);

          Row.IsChecked = true;

          CheckChangeEvent?.Invoke();
        }
      }

      if (ActiveCollation != null)
      {
        if (GroupNullHeaderItem != null)
          RefreshGroupChecks(GroupNullHeaderItem);

        foreach (var GroupHeaderItem in GroupHeaderItemDictionary.Values)
          RefreshGroupChecks(GroupHeaderItem);
      }
    }
    internal void Sort(DataSort Sort, DataSortDirection Direction)
    {
      this.ActiveSort = Sort;
      this.ActiveSortDirection = Direction;

      RefreshSort();

      Reload();
    }
    internal void Group(DataCollationControl Group)
    {
      if (Group != ActiveCollation)
      {
        this.ActiveCollation = Group;

        RefreshGroups();
        RefreshSort();

        Reload();
      }
    }
    internal void RefreshColumn(DataColumn Column)
    {
      Header.RefreshCellByColumn(Column);

      foreach (var Row in ContextRowDictionary.Values.Union(InactiveContextRowList))
        Row.ContextRow.RefreshCellByColumn(Column);
    }
    internal int GetFontSize() => IsDenseField ? 12 : 14;
    internal int GetRowHeight() => IsDenseField ? 40 : 48;
    internal int GetColumnPadding() => 12;

    private void Export(ExportContext Context)
    {
      // Skip the first column as it is the blank column reserved for spacing.
      var ColumnArray = ColumnList.Where(C => C.HasExport).ToArray();

      Context.WriteColumnHeaderPrefix();

      var ColumnIndex = 0;
      foreach (var Column in ColumnArray)
        Context.WriteColumnHeaderContent(Column, ColumnIndex++);

      Context.WriteColumnHeaderSuffix();

      var ItemIndex = 0;
      foreach (var Item in ItemList)
      {
        if (Item.IsGroup)
        {
          // TODO: Export group headers.
        }
        else
        {
          Context.WriteItemPrefix();

          ColumnIndex = 0;
          foreach (var Column in ColumnArray)
          {
            var ExportCell = new DataExportCell(Item.Context);

            Column.Export(ExportCell);

            Context.WriteItemCell(ExportCell, ColumnIndex++, ItemIndex);
          }

          Context.WriteItemSuffix();

          ItemIndex++;
        }
      }
    }
    private void RefreshDensity()
    {
      Header.RefreshDensity();

      var ColumnPadding = GetColumnPadding();
      foreach (var Column in ColumnList)
      {
        Column.HorizontalPadding = ColumnPadding;
        RefreshColumn(Column);
      }

      foreach (var ItemRowPair in ItemRowDictionary)
        ItemRowPair.Value.RefreshDensity();
    }
    private void RefreshGroups()
    {
      GroupNullHeaderItem = null;
      GroupHeaderItemDictionary.Clear();

      ItemRowDictionary.Clear();

      Header.IsCollated = ActiveCollation != null;

      if (ContextList == null || ContextList.Count == 0)
      {
        this.ItemList = new List<DataItem>();

        return;
      }

      if (ActiveCollation != null)
      {
        this.ItemList = new List<DataItem>();
        this.GroupHeaderItemDictionary = new Dictionary<object, DataItem>(ActiveCollation.EqualityComparer);

        foreach (var Context in ContextList)
        {
          var GroupReference = ActiveCollation.Get(Context);

          var GroupHeaderItem = GetGroupHeaderItemByReference(GroupReference);
          if (GroupHeaderItem == null)
          {
            GroupHeaderItem = DataItem.NewGroupHeader(GroupReference, ActiveCollation.Compose(GroupReference), EqualityComparerField);
            
            ItemList.Add(GroupHeaderItem);

            if (GroupReference == null)
              GroupNullHeaderItem = GroupHeaderItem;
            else
              GroupHeaderItemDictionary.Add(GroupReference, GroupHeaderItem);
          }

          if (ActiveCollation.CacheSet.Add(GroupReference))
          {
            GroupHeaderItem.Group.IsExpanded = ActiveCollation.IsExpanded(GroupReference);
            if (GroupHeaderItem.Group.IsExpanded)
              ActiveCollation.ExpandSet.Add(GroupReference);
          }
          else
          {
            GroupHeaderItem.Group.IsExpanded = ActiveCollation.ExpandSet.Contains(GroupReference);
          }

          GroupHeaderItem.Group.ContextSet.Add(Context);

          var GroupItem = DataItem.NewGroupItem(Context, GroupHeaderItem.Group);
          ItemList.Add(GroupItem);
        }
      }
      else
      {
        this.ItemList = ContextList.Select(C => DataItem.NewInlineItem(C)).ToList();
      }
    }
    private void RefreshSort()
    {
      foreach (var Column in ColumnList)
        Column.SortDirection = Column.IsSort(ActiveSort) ? ActiveSortDirection : (DataSortDirection?)null;

      if (ItemList == null)
        return;

      if (ActiveSort != null || TiebreakSort != null || ActiveCollation != null)
      {
        // Maintain composed order if no override has been provided.

        ItemList.Sort((A, B) =>
        {
          var Result = 0;

          if (ActiveCollation != null)
          {
            Result = ActiveCollation.Compare(A.Group.Context, B.Group.Context);

            if (Result == 0)
              Result = A.Group.Text.CompareTo(B.Group.Text);

            // Group headers precede group items.
            if (Result == 0)
              Result = A.IsGroup.CompareTo(B.IsGroup) * -1;
          }

          if (Result == 0 && !A.IsGroup)
          {
            if (ActiveSort != null)
              Result = ActiveSort.Compare(A.Context, B.Context);

            if (Result == 0 && TiebreakSort != null)
              Result = TiebreakSort.Compare(A.Context, B.Context);

            if (ActiveSortDirection == DataSortDirection.Descending)
              Result *= -1;
          }

          return Result;
        });
      }
    }
    private void RefreshFooter()
    {
      FooterEvent?.Invoke();
      
      if (ColumnList.Any(C => !string.IsNullOrWhiteSpace(C.Footer.Content)))
      {
        FooterRow.Visibility.Set(true);
        FooterRow.Prepare();

        foreach (var Column in ColumnList)
          FooterRow.Compose(Column);
      }
      else
      {
        FooterRow.Visibility.Set(false);
      }
    }
    private void RefreshGroupChecks(DataItem GroupItem)
    {
      Debug.Assert(GroupItem != null, $"{nameof(GroupItem)} should not be null");
      if (GroupItem == null)
        return;

      var GroupRow = ItemRowDictionary.GetValueOrDefault(GroupItem)?.AsGroup();

      if (GroupRow != null)
      {
        if (CheckedSet.ContainsAll(GroupItem.Group.ContextSet))
          GroupRow.IsChecked = true;
        else if (CheckedSet.ContainsAny(GroupItem.Group.ContextSet))
          GroupRow.IsChecked = null;
        else
          GroupRow.IsChecked = false;
      }
    }
    private DataItem GetGroupHeaderItemByReference(object Reference)
    {
      if (Reference == null)
        return GroupNullHeaderItem;

      return GroupHeaderItemDictionary.GetValueOrDefault(Reference);
    }

    private readonly Inv.DistinctList<DataColumn> ColumnList;
    private readonly Inv.DistinctList<DataSort> SortList;
    private Dictionary<DataItem, DataContentRow> ItemRowDictionary;
    private Dictionary<object, DataContentRow> ContextRowDictionary;
    private DataItem GroupNullHeaderItem;
    private Dictionary<object, DataItem> GroupHeaderItemDictionary;
    private readonly Inv.DistinctList<DataContentRow> InactiveContextRowList;
    private readonly Inv.DistinctList<DataContentRow> InactiveGroupRowList;
    private readonly Inv.FlowSection Section;
    private bool IsDenseField;
    private bool IsCheckingField;
    private object LastSelectContext;
    private List<object> ContextList;
    private List<DataItem> ItemList;
    private List<DataItem> VisibleItemList;
    private HashSet<object> CheckedSet;
    private IEqualityComparer<object> EqualityComparerField;
    private DataSort ActiveSort;
    private DataCollationControl ActiveCollation;
    private DataFooterRow FooterRow;

    private sealed class ExportContext
    {
      /// <summary>
      /// Attach to export any content that is required to precede the column headers.
      /// </summary>
      public event Action WriteColumnHeaderPrefixEvent;
      /// <summary>
      /// Attach to export the header content for each column. The int parameter is the 0-based visual index of the column.
      /// </summary>
      public event Action<DataColumn, int> WriteColumnHeaderContentEvent;
      /// <summary>
      /// Attach to export any content that is required to follow the column headers.
      /// </summary>
      public event Action WriteColumnHeaderSuffixEvent;
      /// <summary>
      /// Attach to export any content that is required to precede each item.
      /// </summary>
      public event Action WriteItemPrefixEvent;
      /// <summary>
      /// Attach to export the content for each cell of an item. The int parameters are the 0-based visual index of the column and the item respectively.
      /// </summary>
      public event Action<DataExportCell, int, int> WriteItemCellEvent;
      /// <summary>
      /// Attach to export any content that is required to follow each item.
      /// </summary>
      public event Action WriteItemSuffixEvent;

      public void WriteColumnHeaderPrefix() => WriteColumnHeaderPrefixEvent?.Invoke();
      public void WriteColumnHeaderContent(DataColumn Column, int Index) => WriteColumnHeaderContentEvent?.Invoke(Column, Index);
      public void WriteColumnHeaderSuffix() => WriteColumnHeaderSuffixEvent?.Invoke();
      public void WriteItemPrefix() => WriteItemPrefixEvent?.Invoke();
      public void WriteItemCell(DataExportCell Cell, int ColumnIndex, int ItemIndex) => WriteItemCellEvent?.Invoke(Cell, ColumnIndex, ItemIndex);
      public void WriteItemSuffix() => WriteItemSuffixEvent?.Invoke();
    }
  }

  public sealed class DataTable<T> : Inv.Panel<Inv.Material.DataTable>
  {
    public DataTable()
    {
      this.Base = new Inv.Material.DataTable();
    }

    public bool IsChecking
    {
      get => Base.IsChecking;
      set => Base.IsChecking = value;
    }
    public IEnumerable<T> Checks
    {
      get => Base.Checks.Convert<T>();
      set => Base.Checks = value?.Convert<object>();
    }
    public T SelectedContext
    {
      get => (T)Base.SelectedContext;
      set => Base.SelectedContext = value;
    }
    public IEqualityComparer<T> EqualityComparer
    {
      set => Base.EqualityComparer = value != null ? new ObjectEqualityComparer<T>(value) : null;
    }
    public string ActiveSortID => Base.ActiveSortID;
    public DataSortDirection ActiveSortDirection => Base.ActiveSortDirection;
    public DataSort TiebreakSort
    {
      get => Base.TiebreakSort;
      set => Base.TiebreakSort = value;
    }
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Background Background => Base.Background;
    public Inv.Margin Margin => Base.Margin;
    public int TotalCount => Base.TotalCount;
    public DataHeader Header => Base.Header;
    public event Action CheckChangeEvent
    {
      add => Base.CheckChangeEvent += value;
      remove => Base.CheckChangeEvent -= value;
    }
    public event Action DoubleTapEvent
    {
      add => Base.DoubleTapEvent += value;
      remove => Base.DoubleTapEvent -= value;
    }
    public event Action AdjustEvent
    {
      add => Base.AdjustEvent += value;
      remove => Base.AdjustEvent -= value;
    }
    public event Action FooterEvent
    {
      add => Base.FooterEvent += value;
      remove => Base.FooterEvent -= value;
    }

    public void Sparse() => Base.Sparse();
    public bool IsSparse() => Base.IsSparse();
    public void Dense() => Base.Dense();
    public bool IsDense() => Base.IsDense();
    public DataSort AddSort(string ID, Comparison<T> Comparison)
    {
      Debug.Assert(Comparison != null, $"{nameof(Comparison)} must be provided");
      return Base.AddSort(ID, (A, B) => Comparison((T)A, (T)B));
    }
    public DataSort AddSort(DataColumnContract Column, Comparison<T> Comparison)
    {
      Debug.Assert(Comparison != null, $"{nameof(Comparison)} must be provided");
      return Base.AddSort(Column, (A, B) => Comparison((T)A, (T)B));
    }
    public DataSort FindSort(string ID) => Base.FindSort(ID);
    public DataCollationControl<T, G> AddCollation<G>(Func<T, G> GetQuery)
    {
      Debug.Assert(GetQuery != null, $"{nameof(GetQuery)} must be provided");
      return Base.AddCollation(C => GetQuery((T)C)).Wrap<T, G>();
    }
    public void RemoveColumns()
    {
      Base.RemoveColumns();
    }
    public BlockDataColumn<T> AddBlockColumn(string ID, int Width, Action<T, Inv.Block> ComposeAction)
    {
      var Result = Base.AddBlockColumn(ID, Width);

      if (ComposeAction != null)
      {
        Result.ComposeEvent += (Row) =>
        {
          Row.Compose(Result, B => ComposeAction((T)Row.Context, B));
        };
      }

      return Result.Wrap<T>();
    }
    public ButtonDataColumn<T> AddButtonColumn(string ID, int Width, Action<T, ButtonDataCell> ComposeAction)
    {
      var Result = Base.AddButtonColumn(ID, Width);

      if (ComposeAction != null)
      {
        Result.ComposeEvent += (Row) =>
        {
          var ButtonCell = Row.Compose(Result);
          ComposeAction((T)Row.Context, ButtonCell);
        };
      }

      return Result.Wrap<T>();
    }
    public ColourDataColumn AddColourColumn(string ID, Func<T, Inv.Colour> ProjectQuery)
    {
      var Result = Base.AddColourColumn(ID);

      if (ProjectQuery != null)
      {
        Result.ComposeEvent += (Row) =>
        {
          Row.Compose(Result, ProjectQuery?.Invoke((T)Row.Context));
        };
      }

      return Result;
    }
    public RecyclePanelDataColumn<T, TPanel> AddRecyclePanelColumn<TPanel>(string ID, int Width, Action<T, TPanel> ComposeAction)
      where TPanel : Inv.Panel, new()
    {
      var Result = Base.AddRecyclePanelColumn<TPanel>(ID, Width);
      
      if (ComposeAction != null)
      {
        Result.ComposeEvent += (Row) =>
        {
          Result.EqualityComparer = Base.EqualityComparer;

          var Container = Result.GetCell(Row.Context);

          ComposeAction((T)Row.Context, Container.Content);

          Row.Compose(Result, Container);
        };
      }

      return Result.Wrap<T>();
    }
    public CountDataColumn AddCountColumn(string ID, int Width, Func<T, int> ProjectQuery, Action<T, CountDataCell> ComposeAction = null)
    {
      var Result = Base.AddCountColumn(ID, Width);

      if (ProjectQuery != null)
      {
        Result.ComposeEvent += (Row) =>
        {
          var Context = (T)Row.Context;

          var CountCell = Row.Compose(Result);
          CountCell.Content = ProjectQuery(Context);

          ComposeAction?.Invoke(Context, CountCell);
        };
        Result.ExportEvent += (Cell) => Cell.AsInteger(ProjectQuery((T)Cell.Context));
      }

      return Result;
    }
    public DateDataColumn AddDateColumn(string ID, int Width, Func<T, Inv.Date?> ProjectQuery, Action<T, DateDataCell> ComposeAction = null)
    {
      var Result = Base.AddDateColumn(ID, Width);

      if (ProjectQuery != null)
      {
        Result.Sort = AddSort(ID, GetComparison(ProjectQuery, (A, B) => A.CompareTo(B)));
        Result.ComposeEvent += (Row) =>
        {
          var Context = (T)Row.Context;
          var Cell = Row.Compose(Result, ProjectQuery(Context));
          ComposeAction?.Invoke(Context, Cell);
        };
        Result.ExportEvent += (Cell) => Cell.AsDate(ProjectQuery((T)Cell.Context));
      }

      return Result;
    }
    public DecimalDataColumn AddDecimalColumn(string ID, int Width, Func<T, decimal?> ProjectQuery)
    {
      var Result = Base.AddDecimalColumn(ID, Width);

      if (ProjectQuery != null)
      {
        Result.Sort = AddSort(ID, GetComparison(ProjectQuery, (A, B) => A.CompareTo(B)));
        Result.ComposeEvent += (Row) =>
        {
          Row.Compose(Result, ProjectQuery((T)Row.Context));
        };
        Result.ExportEvent += (Cell) => Cell.AsDecimal(ProjectQuery((T)Cell.Context));
      }

      return Result;
    }
    public StringDataColumn AddIdentifierColumn(string ID, int Width, Func<T, string> ProjectQuery, Action<T, StringDataCell> ComposeAction = null)
    {
      var Result = Base.AddStringColumn(ID, Width);

      if (ProjectQuery != null)
      {
        Result.Sort = AddSort(ID, GetComparison(ProjectQuery, (A, B) =>
        {
          var SortResult = A.Length.CompareTo(B.Length);

          if (SortResult == 0)
            SortResult = A.CompareTo(B);

          return SortResult;
        }));

        Result.ComposeEvent += (Row) =>
        {
          var Context = (T)Row.Context;
          var Cell = Row.Compose(Result, ProjectQuery(Context));
          ComposeAction?.Invoke(Context, Cell);
        };
        Result.ExportEvent += (Cell) => Cell.AsText(ProjectQuery((T)Cell.Context));
      }

      return Result;
    }
    public ImageDataColumn AddImageColumn(string ID, int Width, Func<T, Inv.Image> ProjectQuery)
    {
      var Result = Base.AddImageColumn(ID, Width);

      if (ProjectQuery != null)
      {
        Result.ComposeEvent += (Row) =>
        {
          Row.Compose(Result, ProjectQuery((T)Row.Context));
        };
      }

      return Result;
    }
    public ImageDataColumn AddImageColumn(string ID, int Width, Action<T, ImageDataCell> ComposeAction)
    {
      var Result = Base.AddImageColumn(ID, Width);

      if (ComposeAction != null)
      {
        Result.ComposeEvent += (Row) =>
        {
          var ImageCell = Row.Compose(Result);

          ComposeAction((T)Row.Context, ImageCell);
        };
      }

      return Result;
    }
    public IntegerDataColumn AddIntegerColumn(string ID, int Width, Func<T, int?> ProjectQuery)
    {
      var Result = Base.AddIntegerColumn(ID, Width);

      if (ProjectQuery != null)
      {
        Result.Sort = AddSort(ID, GetComparison(ProjectQuery, (A, B) => A.CompareTo(B)));

        Result.ComposeEvent += (Row) =>
        {
          Row.Compose(Result, ProjectQuery((T)Row.Context));
        };
        Result.ExportEvent += (Cell) => Cell.AsInteger(ProjectQuery((T)Cell.Context));
      }

      return Result;
    }
    public IntegerDataColumn AddIntegerColumn(string ID, int Width, Func<T, long?> ProjectQuery)
    {
      var Result = Base.AddIntegerColumn(ID, Width);

      if (ProjectQuery != null)
      {
        Result.Sort = AddSort(ID, GetComparison(ProjectQuery, (A, B) => A.CompareTo(B)));

        Result.ComposeEvent += (Row) =>
        {
          Row.Compose(Result, ProjectQuery((T)Row.Context));
        };
        Result.ExportEvent += (Cell) => Cell.AsInteger(ProjectQuery((T)Cell.Context));
      }

      return Result;
    }
    public MoneyDataColumn AddMoneyColumn(string ID, int Width, Func<T, Inv.Money?> ProjectQuery)
    {
      var Result = Base.AddMoneyColumn(ID, Width);
      if (ProjectQuery != null)
      {
        Result.Sort = AddSort(ID, GetComparison(ProjectQuery, (A, B) => A.CompareTo(B)));

        Result.ComposeEvent += (Row) =>
        {
          Row.Compose(Result, ProjectQuery((T)Row.Context));
        };
        Result.ExportEvent += (Cell) => Cell.AsMoney(ProjectQuery((T)Cell.Context));
      }

      return Result;
    }
    public StringDataColumn AddStringColumn(string ID, int Width, Func<T, string> ProjectQuery, Action<T, StringDataCell> ComposeAction = null, Comparison<T> Comparison = null)
    {
      var Result = Base.AddStringColumn(ID, Width);

      if (ProjectQuery != null)
      {
        Result.Sort = AddSort(ID, Comparison ?? GetComparison(ProjectQuery, (A, B) => A.CompareTo(B)));
        Result.ComposeEvent += (Row) =>
        {
          var Context = (T)Row.Context;
          var Cell = Row.Compose(Result, ProjectQuery(Context));
          ComposeAction?.Invoke(Context, Cell);
        };

        Result.ExportEvent += (Cell) => Cell.AsText(ProjectQuery((T)Cell.Context));
      }

      return Result;
    }
    public TimeDataColumn AddTimeColumn(string ID, int Width, Func<T, Inv.Time?> ProjectQuery, Action<T, TimeDataCell> ComposeAction = null)
    {
      var Result = Base.AddTimeColumn(ID, Width);

      if (ProjectQuery != null)
      {
        Result.Sort = AddSort(ID, GetComparison(ProjectQuery, (A, B) => A.CompareTo(B)));
        Result.ComposeEvent += (Row) =>
        {
          var Context = (T)Row.Context;
          var Cell = Row.Compose(Result, ProjectQuery(Context));
          ComposeAction?.Invoke(Context, Cell);
        };
        Result.ExportEvent += (Cell) => Cell.AsTime(ProjectQuery((T)Cell.Context));
      }

      return Result;
    }
    public TimestampDataColumn AddTimestampColumn(string ID, int Width, Func<T, DateTimeOffset?> ProjectQuery)
    {
      var Result = Base.AddTimestampColumn(ID, Width);

      if (ProjectQuery != null)
      {
        Result.Sort = AddSort(ID, GetComparison(ProjectQuery, (A, B) => A.CompareTo(B)));
        Result.ComposeEvent += (Row) =>
        {
          Row.Compose(Result, ProjectQuery((T)Row.Context));
        };
        Result.ExportEvent += (Cell) => Cell.AsTimestamp(ProjectQuery((T)Cell.Context));
      }

      return Result;
    }
    public void Load(IEnumerable<T> Contexts) => Base.Load(Contexts?.Convert<object>());
    public void Reload() => Base.Reload();
    public void RemoveRow(T Context) => Base.RemoveRow(Context);
    public void RemoveRows() => Base.RemoveRows();
    public void RemoveGrouping() => Base.RemoveGrouping();
    public IEnumerable<T> Retrieve() => Base.Retrieve().Convert<T>();
    public void ExportCsv(Inv.File File)
    {
      Base.ExportCsv(File);
    }
    public void ExportExcel(Inv.File File)
    {
      Base.ExportExcel(File);
    }
    public Inv.Dimension GetDimension() => Base.GetDimension();
    public void RemoveChecks() => Base.RemoveChecks();
    private Comparison<T> GetComparison<V>(Func<T, V> ProjectQuery, Comparison<V> Comparison)
    {
      return (A, B) =>
      {
        var ProjectA = ProjectQuery(A);
        var ProjectB = ProjectQuery(B);

        var Result = (ProjectA != null).CompareTo(ProjectB != null);

        if (Result == 0 && ProjectA != null)
          Result = Comparison(ProjectA, ProjectB);

        return Result;
      };
    }
  }

  public sealed class DataSort
  {
    internal DataSort(DataTable DataTable, string ID, Comparison<object> Comparison)
    {
      this.DataTable = DataTable;
      this.ID = ID;
      this.CompareEvent = Comparison;
    }

    /// <summary>
    /// Used for get/set of active sort. Not displayed to user.
    /// </summary>
    public string ID { get; }

    /// <summary>
    /// Applies this sort in ascending order to the owning data table.
    /// </summary>
    public void Ascending() => DataTable.Sort(this, DataSortDirection.Ascending);
    /// <summary>
    /// Applies this sort in descending order to the owning data table.
    /// </summary>
    public void Descending() => DataTable.Sort(this, DataSortDirection.Descending);

    internal int Compare(object A, object B) => CompareEvent(A, B);

    private readonly DataTable DataTable;
    private readonly Comparison<object> CompareEvent;
  }

  public enum DataSortDirection
  {
    Ascending, Descending
  }

  public sealed class DataFooter
  {
    public string Content { get; private set; }

    public void AsText(string Footer) => Content = Footer;
    public void AsMoney(Inv.Money Footer) => Content = Footer.ToString();
  }

  public sealed class DataCollationControl
  {
    internal DataCollationControl(DataTable DataTable, Func<object, object> GetQuery)
    {
      this.DataTable = DataTable;
      this.GetQuery = GetQuery;
      this.CacheSet = new HashSet<object>();
      this.ExpandSet = new HashSet<object>();
      this.ShowCount = true;
    }

    public IEqualityComparer<object> EqualityComparer
    {
      get => EqualityComparerField;
      set
      {
        if (EqualityComparerField != value)
        {
          this.EqualityComparerField = value;
          this.CacheSet = new HashSet<object>(EqualityComparerField);
          this.ExpandSet = new HashSet<object>(EqualityComparerField);
        }
      }
    }
    /// <summary>
    /// Specifies how to render a text string representation of the projected grouping item. If null, .NET's default ToString() is used.
    /// <para>Note: Could consider supporting non-string representations although this would involve creating more Inv.Panels.</para>
    /// </summary>
    public event Func<object, string> ComposeQuery;
    /// <summary>
    /// Specifies how to compare two groups for sorting purposes.
    /// </summary>
    public event Comparison<object> CompareQuery;
    /// <summary>
    /// Specifies whether a given group should begin expanded.
    /// </summary>
    public event Func<object, bool> IsExpandedQuery;
    public bool ShowCount { get; set; }

    public void Group() => DataTable.Group(this);

    internal HashSet<object> CacheSet { get; private set; }
    internal HashSet<object> ExpandSet { get; private set; }

    internal bool HasComposeQuery => ComposeQuery != null;
    internal bool HasCompareQuery => CompareQuery != null;
    internal bool HasIsExpandedQuery => IsExpandedQuery != null;
    internal object Get(object Context) => GetQuery?.Invoke(Context);
    internal string Compose(object Context) => ComposeQuery?.Invoke(Context) ?? Context?.ToString();
    internal int Compare(object A, object B)
    {
      if (CompareQuery == null)
        return 0;

      return CompareQuery(A, B);
    }
    internal bool IsExpanded(object Context) => IsExpandedQuery?.Invoke(Context) ?? true;

    internal DataCollationControl<T, G> Wrap<T, G>() => new DataCollationControl<T, G>(this);

    private readonly DataTable DataTable;
    private readonly Func<object, object> GetQuery;
    private IEqualityComparer<object> EqualityComparerField;
  }

  public sealed class DataCollationControl<T, G>
  {
    internal DataCollationControl(DataCollationControl Base)
    {
      this.Base = Base;
    }

    public IEqualityComparer<G> EqualityComparer
    {
      set => Base.EqualityComparer = new Inv.Support.ObjectEqualityComparer<G>(value);
    }
    public event Func<G, string> ComposeQuery
    {
      add
      {
        if (value != null)
        {
          if (ComposeQueryDelegate == null && !Base.HasComposeQuery)
            Base.ComposeQuery += ComposeQueryHandler;

          this.ComposeQueryDelegate += value;
        }
      }
      remove
      {
        if (value != null)
        {
          this.ComposeQueryDelegate -= value;

          if (ComposeQueryDelegate == null && Base.HasComposeQuery)
            Base.ComposeQuery -= ComposeQueryHandler;
        }
      }
    }
    public event Comparison<G> CompareQuery
    {
      add
      {
        if (value != null)
        {
          if (CompareQueryDelegate == null && !Base.HasCompareQuery)
            Base.CompareQuery += CompareQueryHandler;

          this.CompareQueryDelegate += value;
        }
      }
      remove
      {
        if (value != null)
        {
          this.CompareQueryDelegate -= value;

          if (CompareQueryDelegate == null && Base.HasCompareQuery)
            Base.CompareQuery -= CompareQueryHandler;
        }
      }
    }
    public event Func<G, bool> IsExpandedQuery
    {
      add
      {
        if (value != null)
        {
          if (IsExpandedQueryDelegate == null && !Base.HasIsExpandedQuery)
            Base.IsExpandedQuery += IsExpandedQueryHandler;

          this.IsExpandedQueryDelegate += value;
        }
      }
      remove
      {
        if (value != null)
        {
          this.IsExpandedQueryDelegate -= value;

          if (IsExpandedQueryDelegate == null && Base.HasIsExpandedQuery)
            Base.IsExpandedQuery -= IsExpandedQueryHandler;
        }
      }
    }
    public bool ShowCount 
    { 
      get => Base.ShowCount; 
      set => Base.ShowCount = value; 
    }

    public void Group() => Base.Group();

    private string ComposeQueryHandler(object Context) => ComposeQueryDelegate((G)Context);
    private int CompareQueryHandler(object A, object B) => CompareQueryDelegate((G)A, (G)B);
    private bool IsExpandedQueryHandler(object Context) => IsExpandedQueryDelegate((G)Context);

    private readonly DataCollationControl Base;
    private Func<G, string> ComposeQueryDelegate;
    private Comparison<G> CompareQueryDelegate;
    private Func<G, bool> IsExpandedQueryDelegate;
  }

  public interface DataColumnContract
  {
    DataColumn Control { get; }
  }

  public sealed class DataColumnVisibility
  {
    internal DataColumnVisibility()
    {
      this.IsVisible = true;
    }

    public event Action ChangeEvent;

    public bool Get()
    {
      return IsVisible;
    }
    public void Set(bool Value)
    {
      if (Value != this.IsVisible)
      {
        this.IsVisible = Value;
        ChangeEvent?.Invoke();
      }
    }

    private bool IsVisible;
  }

  public sealed class DataColumn : DataColumnContract
  {
    internal DataColumn(DataTable Table, string ID)
    {
      this.Table = Table;
      this.ID = ID;

      this.Visibility = new DataColumnVisibility();
      Visibility.ChangeEvent += () => Table.RefreshColumn(this);

      this.HeaderCell = new DataCell();
      HeaderCell.Column = this;
      HeaderCell.Size.SetHeight(40);

      this.Button = Inv.Button.NewStark();
      HeaderCell.Content = Button;
      Button.Background.In(Inv.Theme.BackgroundColour);
      Button.Alignment.Stretch();
      Button.OverEvent += () => Refresh();
      Button.AwayEvent += () => Refresh();
      Button.SingleTapEvent += () =>
      {
        if (SortField != null)
          Table.Sort(SortField, SortDirectionField == DataSortDirection.Ascending ? DataSortDirection.Descending : DataSortDirection.Ascending);
      };

      this.Stack = Inv.Stack.NewHorizontal();
      Button.Content = Stack;

      this.IconGraphic = Inv.Graphic.New();
      Stack.AddPanel(IconGraphic);
      IconGraphic.Size.Set(24);
      IconGraphic.Visibility.Collapse();

      this.TitleLabel = Inv.Label.New();
      Stack.AddPanel(TitleLabel);
      TitleLabel.Text = ID; // Default to the ID.
      TitleLabel.Font.Medium().Custom(14).In(Inv.Theme.OnBackgroundColour);
      TitleLabel.LineWrapping = false;

      this.SortGraphic = Inv.Graphic.New();
      Stack.AddPanel(SortGraphic);
      SortGraphic.Size.Set(16);
      SortGraphic.Visibility.Collapse();

      this.Justify = new DataColumnJustify();
      Justify.ChangeEvent += () =>
      {
        TitleLabel.Justify.Set(Justify.Get());
        Stack.Alignment.Set(Justify.IsLeft() ? Placement.CenterLeft : Placement.CenterRight);

        var GraphicIndex = Justify.IsLeft() ? 2 : 0;
        if (Stack.Panels.IndexOf(SortGraphic) != GraphicIndex)
        {
          Stack.RemovePanel(SortGraphic);
          Stack.InsertPanel(GraphicIndex, SortGraphic);
        }
      };

      this.Footer = new DataFooter();
    }

    public string ID { get; }
    public string Title
    {
      get => TitleLabel.Text;
      set => TitleLabel.Text = value;
    }
    public string Definition
    {
      get => DefinitionField;
      set
      {
        if (DefinitionField != value)
        {
          if (DefinitionField == null && value != null)
          {
            Button.Tooltip.ShowEvent += TooltipHandler;
          }
          else if (DefinitionField != null && value == null)
          {
            Button.Tooltip.ShowEvent -= TooltipHandler;
            Button.Tooltip.Content = null;
          }

          this.DefinitionField = value;
        }
      }
    }
    public Inv.Image Image
    {
      get => IconGraphic.Image;
      set
      {
        IconGraphic.Image = value;
        IconGraphic.Visibility.Set(IconGraphic.Image != null);
      }
    }
    public int Width
    {
      get => WidthField;
      set
      {
        if (WidthField != value)
        {
          this.WidthField = value;
          Table.RefreshColumn(this);
        }
      }
    }
    public DataColumnVisibility Visibility { get; }
    public DataColumnJustify Justify { get; }
    public DataSort Sort
    {
      get => SortField;
      set
      {
        if (SortField != value)
        {
          this.SortField = value;
          Refresh();
        }
      }
    }
    public DataFooter Footer { get; }
    internal DataTable Table { get; }
    internal int ActualWidth => WidthField + (2 * HorizontalPaddingField);
    internal DataCell HeaderCell { get; }
    internal DataSortDirection? SortDirection
    {
      get => SortDirectionField;
      set
      {
        if (SortDirectionField != value)
        {
          this.SortDirectionField = value;

          if (SortDirectionField == null)
          {
            SortGraphic.Visibility.Collapse();
          }
          else
          {
            SortGraphic.Visibility.Show();
            SortGraphic.Image = SortDirectionField == DataSortDirection.Ascending ? AscendingImage : DescendingImage;
          }
        }
      }
    }
    internal int HorizontalPadding
    {
      get => HorizontalPaddingField;
      set
      {
        if (HorizontalPaddingField != value)
        {
          this.HorizontalPaddingField = value;

          Button.Padding.Set(value, 0, value, 0);
        }
      }
    }
    internal bool HasExport => ExportEvent != null;
    internal event Action<DataExportCell> ExportEvent;
    internal event Action<DataContextRow> ComposeEvent;
    internal event Action RecycleEvent;

    internal void Export(DataExportCell Cell) => ExportEvent?.Invoke(Cell);
    internal void Compose(DataContextRow Row) => ComposeEvent?.Invoke(Row);
    internal void Recycle() => RecycleEvent?.Invoke();
    internal bool IsSort(DataSort SortControl) => SortControl != null && SortControl == SortField;

    DataColumn DataColumnContract.Control => this;

    private void Refresh()
    {
      var Colour = SortField != null && Button.IsOver ? Inv.Theme.Light.WhiteSmoke : Inv.Theme.BackgroundColour;

      HeaderCell.Background.In(Colour);
      Button.Background.In(Colour);
    }
    private void TooltipHandler()
    {
      if (Definition != null)
      {
        if (DefinitionTooltip == null)
        {
          this.DefinitionTooltip = new TooltipText();
          Button.Tooltip.Content = DefinitionTooltip;
        }

        DefinitionTooltip.Text = Definition;
      }
    }

    private readonly Inv.Button Button;
    private readonly Inv.Stack Stack;
    private readonly Inv.Graphic IconGraphic;
    private readonly Inv.Label TitleLabel;
    private readonly Inv.Graphic SortGraphic;
    private TooltipText DefinitionTooltip;
    private string DefinitionField;
    private int HorizontalPaddingField;
    private int WidthField;
    private DataSort SortField;
    private DataSortDirection? SortDirectionField;

    private static Inv.Image AscendingImage;
    private static Inv.Image DescendingImage;

    static DataColumn()
    {
      var Graphics = Inv.Application.Access().Graphics;
      AscendingImage = Graphics.Tint(Resources.Images.ArrowUpward, Inv.Theme.OnBackgroundColour);
      DescendingImage = Graphics.Tint(Resources.Images.ArrowDownward, Inv.Theme.OnBackgroundColour);
    }
  }

  public sealed class DataExportCell
  {
    internal DataExportCell(object Context)
    {
      this.Context = Context;
    }

    public object Context { get; }

    public void AsDate(Inv.Date? Value)
    {
      this.Type = DataExportType.Date;
      this.Value = Value;
    }
    public void AsDecimal(decimal? Value)
    {
      this.Type = DataExportType.Decimal;
      this.Value = Value;
    }
    public void AsInteger(int? Value)
    {
      this.Type = DataExportType.Integer;
      this.Value = Value;
    }
    public void AsInteger(long? Value)
    {
      this.Type = DataExportType.Integer64;
      this.Value = Value;
    }
    public void AsMoney(Inv.Money? Value)
    {
      this.Type = DataExportType.Money;
      this.Value = Value;
    }
    public void AsText(string Value)
    {
      this.Type = DataExportType.Text;
      this.Value = Value;
    }
    public void AsTime(Inv.Time? Value)
    {
      this.Type = DataExportType.Time;
      this.Value = Value;
    }
    public void AsTimestamp(DateTimeOffset? Value)
    {
      this.Type = DataExportType.Timestamp;
      this.Value = Value;
    }

    internal DataExportType? Type { get; private set; }
    internal object Value { get; private set; }

    internal DataExportCell<T> Wrap<T>() => new DataExportCell<T>(this);
  }

  public sealed class DataExportCell<T>
  {
    internal DataExportCell(DataExportCell Base)
    {
      this.Base = Base;
    }

    public T Context => (T)Base.Context;

    public void AsDate(Inv.Date? Value) => Base.AsDate(Value);
    public void AsDecimal(decimal? Value) => Base.AsDecimal(Value);
    public void AsInteger(int? Value) => Base.AsInteger(Value);
    public void AsMoney(Inv.Money Value) => Base.AsMoney(Value);
    public void AsText(string Value) => Base.AsText(Value);
    public void AsTime(Inv.Time? Value) => Base.AsTime(Value);
    public void AsTimestamp(DateTimeOffset? Value) => Base.AsTimestamp(Value);

    private readonly DataExportCell Base;
  }

  public sealed class DataColumnJustify
  {
    public void Left()
    {
      this.Type = DataColumnJustifyType.Left;
      ChangeInvoke();
    }
    public bool IsLeft() => Type == DataColumnJustifyType.Left;
    public void Right()
    {
      this.Type = DataColumnJustifyType.Right;
      ChangeInvoke();
    }
    public bool IsRight() => Type == DataColumnJustifyType.Right;

    internal event Action ChangeEvent;

    internal Inv.Justification Get() => IsLeft() ? Inv.Justification.Left : Inv.Justification.Right;

    private void ChangeInvoke() => ChangeEvent?.Invoke();

    private DataColumnJustifyType Type;

    private enum DataColumnJustifyType
    {
      Left, Right
    }
  }

  internal sealed class DataCheckButton : Inv.Panel<Inv.Button>
  {
    internal DataCheckButton()
    {
      this.Base = Inv.Button.NewStark();
      Base.Margin.Set(0, 0, 8, 0);
      Base.Corner.Set(24);
      Base.Size.Set(36);
      Base.Alignment.Center();
      Base.Visibility.Collapse();

      this.Icon = new Inv.Material.Icon();
      Base.Content = Icon;
      Icon.Size.Set(24);
      Icon.Alignment.Center();
    }

    public bool IsOver => Base.IsOver;
    public Inv.Background Background => Base.Background;
    public Inv.Image Image
    {
      get => Icon.Image;
      set => Icon.Image = value;
    }
    public Inv.Colour ImageColour
    {
      get => Icon.Colour;
      set => Icon.Colour = value;
    }
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action OverEvent
    {
      add => Base.OverEvent += value;
      remove => Base.OverEvent -= value;
    }
    public event Action AwayEvent
    {
      add => Base.AwayEvent += value;
      remove => Base.AwayEvent -= value;
    }
    public event Action SingleTapEvent
    {
      add => Base.SingleTapEvent += value;
      remove => Base.SingleTapEvent -= value;
    }

    internal void SetDensity(bool IsDense)
    {
      Base.Size.Set(IsDense ? 30 : 36);
      Icon.Size.Set(IsDense ? 20 : 24);
    }

    private readonly Inv.Material.Icon Icon;
  }

  public sealed class DataHeader : Inv.Panel<Inv.Stack>
  {
    internal DataHeader(Inv.Material.DataTable Table)
    {
      this.Table = Table;

      this.ColumnCellDictionary = new Dictionary<DataColumn, DataCell>();

      this.Base = Inv.Stack.NewHorizontal();
      Base.Border.Set(0, 0, 0, 1);
      Base.Border.Colour = Inv.Theme.EdgeColour;
      Base.Background.Colour = Inv.Colour.Transparent;

      this.CheckButton = new DataCheckButton();
      Base.AddPanel(CheckButton);
      CheckButton.ImageColour = Inv.Theme.BackgroundColour;
      CheckButton.Image = Inv.Material.Resources.Images.CheckBoxOutlineBlank;

      this.ColumnNameStack = Inv.Stack.NewHorizontal();
      Base.AddPanel(ColumnNameStack);

      Refresh();
    }

    public Inv.Visibility Visibility => Base.Visibility;

    public bool IsActionBar => ActionBar != null && Base.Panels.Contains(ActionBar);
    public DataHeaderActionBar AsActionBar()
    {
      if (Base.Panels.Contains(ColumnNameStack))
        Base.RemovePanel(ColumnNameStack);

      if (!Base.Panels.Contains(ActionBar))
      {
        if (ActionBar == null)
          ActionBar = new DataHeaderActionBar();

        Base.AddPanel(ActionBar);
      }

      Refresh();

      return ActionBar;
    }
    public bool IsColumnNames => Base.Panels.Contains(ColumnNameStack);
    public void AsColumnNames()
    {
      if (Base.Panels.Contains(ActionBar))
        Base.RemovePanel(ActionBar);

      if (!Base.Panels.Contains(ColumnNameStack))
        Base.AddPanel(ColumnNameStack);

      Refresh();
    }

    internal bool IsChecking
    {
      get => IsCheckingField;
      set
      {
        if (IsCheckingField != value)
        {
          this.IsCheckingField = value;
          Refresh();
        }
      }
    }
    internal bool IsCollated
    {
      get => IsCollatedField;
      set
      {
        if (IsCollatedField != value)
        {
          this.IsCollatedField = value;
          Refresh();
        }
      }
    }

    internal void RefreshDensity()
    {
      CheckButton.SetDensity(Table.IsDense());
    }
    internal void RefreshCellByColumn(DataColumn Column)
    {
      var Cell = ColumnCellDictionary.GetValueOrDefault(Column);
      if (Cell != null)
        RefreshCell(Cell);
    }
    internal void Set(IEnumerable<DataCell> Cells)
    {
      var CellArray = Cells.ToArray();
      var CurrentPanelArray = ColumnNameStack.Panels.ToArray();
      
      while (CurrentPanelArray.Length > CellArray.Length)
        ColumnNameStack.RemovePanel(CurrentPanelArray.LastOrDefault());

      for (var CellIndex = 0; CellIndex < CellArray.Length; CellIndex++)
      {
        var SetCell = CellArray[CellIndex];
        var CurrentPanel = CurrentPanelArray.ElementAtOrDefault(CellIndex);

        if (CurrentPanel == null)
        {
          ColumnNameStack.AddPanel(SetCell);
        }
        else if (CurrentPanel != SetCell)
        {
          ColumnNameStack.RemovePanel(CurrentPanel);

          // Note that the checkbox cell is the first element in the stack panel.
          ColumnNameStack.InsertPanel(CellIndex + 1, SetCell);
        }

        ColumnCellDictionary[SetCell.Column] = SetCell;

        RefreshCell(SetCell);
      }
    }

    private void Refresh()
    {
      // To keep the column header labels horizontally aligned with the column content.
      Base.Padding.Set(!IsActionBar ? DataContentRow.LeftPadding + (IsCollatedField ? DataContentRow.ExpandIconSize + DataContentRow.ExpandIconMargin : 0): 0, 0, 0, 0);

      CheckButton.Visibility.Set(IsCheckingField && IsColumnNames);
    }
    private void RefreshCell(DataCell Cell)
    {
      Cell.Size.SetWidth(Cell.Column.ActualWidth);
      Cell.Visibility.Set(Cell.Column.Visibility.Get());

      // Note: don't set the padding on the header cells.
    }

    private readonly Inv.Material.DataTable Table;
    private readonly DataCheckButton CheckButton;
    private readonly Dictionary<DataColumn, DataCell> ColumnCellDictionary;
    private readonly Inv.Stack ColumnNameStack;
    private DataHeaderActionBar ActionBar;
    private bool IsCheckingField;
    private bool IsCollatedField;
  }

  public sealed class DataHeaderActionBar : Inv.Panel<Inv.Stack>
  {
    internal DataHeaderActionBar()
    {
      this.Base = Inv.Stack.NewHorizontal();
      Base.Margin.Set(4, 4, 0, 4);

      this.ActionList = new Inv.DistinctList<DataHeaderAction>();
    }

    public DataHeaderAction Add(Inv.Image Image, string Caption)
    {
      var Result = new DataHeaderAction();
      Result.Image = Image;
      Result.Caption = Caption;
      
      Base.AddPanel(Result);
      ActionList.Add(Result);

      return Result;
    }

    private readonly Inv.DistinctList<DataHeaderAction> ActionList;
  }

  public sealed class DataHeaderAction : Inv.Panel<Inv.Material.Button>
  {
    internal DataHeaderAction()
    {
      this.Base = new Inv.Material.Button();
      Base.AsText();
      Base.Margin.Set(0, 0, 4, 0);

      Refresh();
    }

    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set
      {
        if (Base.IsEnabled != value)
        {
          Base.IsEnabled = value;
          Refresh();
        }
      }
    }
    public Inv.Visibility Visibility => Base.Visibility;
    public Inv.Image Image
    {
      get => Base.Image;
      set => Base.Image = value;
    }
    public string Caption
    {
      get => Base.Caption;
      set => Base.Caption = value;
    }
    public event Action SingleTapEvent
    {
      add => Base.SingleTapEvent += value;
      remove => Base.SingleTapEvent -= value;
    }

    private void Refresh()
    {
      //Base.ForegroundColour = Base.IsEnabled ? Inv.Theme.SubtleColour.Darken(0.10F) : Inv.Theme.EdgeColour;
    }
  }

  public sealed class DataContentRow : Inv.Panel<Inv.Button>
  {
    internal DataContentRow(DataTable Table)
    {
      this.IsCheckedField = false;

      this.Table = Table;

      this.Base = Inv.Button.NewStark();
      Base.IsFocusable = false;
      Base.Focus.GotEvent += () => Refresh();
      Base.Focus.LostEvent += () => Refresh();
      Base.OverEvent += () => Refresh();
      Base.AwayEvent += () => Refresh();
      Base.Margin.Set(0, 0, 10, 0); // To offset the vertical scrollbar width.
      Base.Background.Colour = Inv.Colour.Transparent;

      this.Stack = Inv.Stack.NewHorizontal();
      Base.Content = Stack;
      Stack.Padding.Set(LeftPadding, 0, 0, 0);
      Stack.Border.Set(0, 0, 0, 1);

      this.ExpandIcon = new Inv.Material.Icon();
      Stack.AddPanel(ExpandIcon);
      ExpandIcon.Margin.Set(0, 0, ExpandIconMargin, 0);
      ExpandIcon.Size.Set(ExpandIconSize);
      ExpandIcon.Padding.Set(0, 4);
      ExpandIcon.Alignment.Center();
      ExpandIcon.Visibility.Collapse();

      this.CheckButton = new DataCheckButton();
      Stack.AddPanel(CheckButton);
      CheckButton.OverEvent += () => Refresh();
      CheckButton.AwayEvent += () => Refresh();
      CheckButton.SingleTapEvent += () =>
      {
        this.IsCheckedField = IsCheckedField == null || !IsCheckedField.Value;
        Refresh();
        CheckChangeEvent?.Invoke();
      };

      Refresh();
    }

    public Inv.Panel Content
    {
      get => Base.Content;
      set => Base.Content = value;
    }
    public Inv.Background Background => Base.Background;
    public Inv.Size Size => Base.Size;
    public Inv.Border Border => Stack.Border;
    public Inv.Margin Margin => Base.Margin;
    public Inv.Padding Padding => Stack.Padding;
    public Inv.Visibility Visibility => Base.Visibility;
    public bool IsFocusable
    {
      get => Base.IsFocusable;
      set => Base.IsFocusable = value;
    }
    public bool IsChecking
    {
      get => IsCheckingField;
      set
      {
        if (IsCheckingField != value)
        {
          this.IsCheckingField = value;
          CheckButton.Visibility.Set(IsCheckingField);
          if (!IsCheckingField)
            this.IsCheckedField = false;
          Refresh();
        }
      }
    }
    public bool? IsChecked
    {
      get => IsCheckedField;
      set
      {
        if (IsCheckedField != value)
        {
          this.IsCheckedField = value;
          Refresh();
        }
      }
    }
    public event Action SingleTapEvent
    {
      add => Base.SingleTapEvent += value;
      remove => Base.SingleTapEvent -= value;
    }

    public DataContextRow AsContext()
    {
      Debug.Assert(GroupRow == null, $"{nameof(DataContentRow)} cannot change between {nameof(DataContextRow)} and {nameof(DataGroupRow)}");

      if (ContextRow == null)
        ContextRow = new DataContextRow(this);

      ContextRow.Refresh();

      Refresh();
      RefreshDensity();

      return ContextRow;
    }
    public DataGroupRow AsGroup()
    {
      Debug.Assert(ContextRow == null, $"{nameof(DataContentRow)} cannot change between {nameof(DataContextRow)} and {nameof(DataGroupRow)}");

      if (GroupRow == null)
        GroupRow = new DataGroupRow(this);

      GroupRow.Refresh();

      Refresh();
      RefreshDensity();

      return GroupRow;
    }
    public DataFooterRow AsFooter()
    {
      Debug.Assert(ContextRow == null, $"{nameof(DataContentRow)} cannot change between {nameof(DataContextRow)} and {nameof(DataFooterRow)}");

      if (FooterRow == null)
        FooterRow = new DataFooterRow(this);

      FooterRow.Refresh();

      Refresh();
      RefreshDensity();

      return FooterRow;
    }

    internal bool IsCollated
    {
      get => IsCollatedField;
      set
      {
        if (IsCollatedField != value)
        {
          this.IsCollatedField = value;
          ExpandIcon.Visibility.Set(IsCollatedField);
          Refresh();
        }
      }
    }
    internal bool IsExpanded
    {
      get => IsExpandedField;
      set
      {
        if (IsExpandedField != value)
        {
          this.IsExpandedField = value;
          Refresh();
        }
      }
    }
    internal DataContextRow ContextRow { get; private set; }
    internal DataGroupRow GroupRow { get; private set; }
    internal DataFooterRow FooterRow { get; private set; }
    internal DataTable Table { get; }

    internal void RefreshDensity()
    {
      CheckButton.SetDensity(Table.IsDense());

      GroupRow?.RefreshDensity();
    }
    internal void AttachCheckChange(Action Action)
    {
      CheckChangeEvent = null;
      CheckChangeEvent += Action;
    }
    internal void AddCell(Inv.Panel Panel) => Stack.AddPanel(Panel);
    internal void RemoveCell(Inv.Panel Panel) => Stack.RemovePanel(Panel);
    internal void Recycle()
    {
      ContextRow?.Recycle();
    }

    private void Refresh()
    {
      Base.Background.Colour = Base.IsFocusable ? IsCheckedField == true ? CheckBackgroundColour : Base.IsOver ? HoverBackgroundColour : Inv.Colour.Transparent : (IsCollatedField && GroupRow != null && Base.IsOver ? HoverBackgroundColour : Inv.Colour.Transparent);

      if (IsCheckedField == null || IsCheckedField.Value)
      {
        if (CheckButton.IsOver || Base.Focus.Has())
          CheckButton.Background.In(CheckHighlightColour);
        else
          CheckButton.Background.In(Inv.Colour.Transparent);

        CheckButton.ImageColour = Inv.Material.Theme.PrimaryColour;

        if (IsCheckedField == null)
          CheckButton.Image = Inv.Material.Resources.Images.CheckBoxPartial;
        else
          CheckButton.Image = Inv.Material.Resources.Images.CheckBox;
      }
      else
      {
        if (CheckButton.IsOver || Base.Focus.Has())
          CheckButton.Background.In(Inv.Theme.Light.LightGray);
        else
          CheckButton.Background.In(Inv.Colour.Transparent);

        CheckButton.Image = Inv.Material.Resources.Images.CheckBoxOutlineBlank;
        CheckButton.ImageColour = Base.IsFocusable ? Inv.Theme.Light.DimGray : Inv.Material.Theme.PrimaryColour;
      }

      if (IsCollatedField)
        ExpandIcon.Image = GroupRow != null ? IsExpandedField ? Inv.Material.Resources.Images.ExpandLess : Inv.Material.Resources.Images.ExpandMore : null;
    }

    private readonly Inv.Stack Stack;
    private readonly Inv.Material.Icon ExpandIcon;
    private readonly DataCheckButton CheckButton;
    private bool IsCollatedField;
    private bool IsCheckingField;
    private bool? IsCheckedField;
    private bool IsExpandedField;
    private event Action CheckChangeEvent;

    static DataContentRow()
    {
      CheckHighlightColour = Inv.Material.Theme.PrimaryColour.Opacity(0.25F);
      CheckBackgroundColour = Inv.Material.Theme.PrimaryColour.Opacity(0.15F);
      HoverBackgroundColour = Inv.Theme.EdgeColour.Opacity(0.25F);
    }

    internal const int LeftPadding = 8;
    internal const int ExpandIconSize = 36;
    internal const int ExpandIconMargin = 4;

    private static Inv.Colour CheckHighlightColour { get; set; }
    private static Inv.Colour CheckBackgroundColour { get; set; }
    private static Inv.Colour HoverBackgroundColour { get; set; }
  }

  public sealed class DataContextRow : Inv.Panel<DataContentRow>
  {
    internal DataContextRow(DataContentRow Base)
    {
      this.Base = Base;
      Base.SingleTapEvent += () => Base.Table.SelectRow(this);

      this.ActiveCellList = new List<DataCell>();
      this.InactiveCellList = new List<DataCell>();
      this.ColumnCellDictionary = new Dictionary<DataColumn, DataCell>();
    }

    public object Context { get; private set; }

    public BlockDataCell Compose(BlockDataColumn Column, Action<Inv.Block> ComposeAction)
    {
      var Result = EnsureCell(Column).AsBlock();
      ComposeAction?.Invoke(Result.Content);
      return Result;
    }
    public ButtonDataCell Compose(ButtonDataColumn Column)
    {
      return EnsureCell(Column).AsButton();
    }
    public void Compose(ColourDataColumn Column, Inv.Colour Value)
    {
      EnsureCell(Column).AsColour().Content = Value;
    }
    public CountDataCell Compose(CountDataColumn Column)
    {
      return EnsureCell(Column).AsCount();
    }
    public DateDataCell Compose(DateDataColumn Column, Inv.Date? Value)
    {
      var Result = EnsureCell(Column).AsDate();
      Result.Format = Column.Format;
      Result.Content = Value;
      return Result;
    }
    public DecimalDataCell Compose(DecimalDataColumn Column, decimal? Value)
    {
      var Result = EnsureCell(Column).AsDecimal();
      Result.Justify.Set(Column.Justify.Get());
      Result.Content = Value;
      return Result;
    }
    public ImageDataCell Compose(ImageDataColumn Column, Inv.Image Value)
    {
      var Result = Compose(Column);
      Result.Content = Value;
      return Result;
    }
    public ImageDataCell Compose(ImageDataColumn Column)
    {
      var Result = EnsureCell(Column).AsImage();
      Result.Size = Column.Width;
      return Result;
    }
    public IntegerDataCell Compose(IntegerDataColumn Column, int? Value)
    {
      var Result = EnsureCell(Column).AsInteger();
      Result.Justify.Set(Column.Justify.Get());
      Result.Content = Value;
      return Result;
    }
    public Integer64DataCell Compose(IntegerDataColumn Column, long? Value)
    {
      var Result = EnsureCell(Column).AsInteger64();
      Result.Justify.Set(Column.Justify.Get());
      Result.Content = Value;
      return Result;
    }
    public MoneyDataCell Compose(MoneyDataColumn Column, Inv.Money? Value)
    {
      var Result = EnsureCell(Column).AsMoney();
      Result.Justify.Set(Column.Justify.Get());
      Result.Content = Value;
      return Result;
    }
    public StringDataCell Compose(StringDataColumn Column, string Value)
    {
      var Result = EnsureCell(Column).AsString();
      Result.Justify.Set(Column.Justify.Get());
      Result.LineWrapping = Column.LineWrapping;
      Result.Content = Value;
      return Result;
    }
    public TimeDataCell Compose(TimeDataColumn Column, Inv.Time? Value)
    {
      var Result = EnsureCell(Column).AsTime();
      Result.Justify.Set(Column.Justify.Get());
      Result.Content = Value;
      return Result;
    }
    public TimestampDataCell Compose(TimestampDataColumn Column, DateTimeOffset? Value)
    {
      var Result = EnsureCell(Column).AsTimestamp();
      Result.Justify.Set(Column.Justify.Get());
      Result.Content = Value;
      return Result;
    }

    internal bool IsChecked
    {
      get => Base.IsChecked ?? false;
      set => Base.IsChecked = value;
    }
    internal bool IsChecking
    {
      get => Base.IsChecking;
      set => Base.IsChecking = value;
    }
    internal bool IsCollated
    {
      get => Base.IsCollated;
      set => Base.IsCollated = value;
    }

    internal void Compose<TPanel>(RecyclePanelDataColumn<TPanel> Column, RecyclePanelContainer<TPanel> Container)
      where TPanel : Inv.Panel, new()
    {
      EnsureCell(Column).AsCustom(Container);
    }
    internal void AttachCheckChange(Action CheckAction) => Base.AttachCheckChange(CheckAction);
    internal void Refresh()
    {
      Base.IsFocusable = true;
      Base.Size.SetHeight(Base.Table.GetRowHeight());
    }
    internal void Prepare(object Context)
    {
      this.Context = Context;

      Base.Margin.Set(0);
      Base.Border.In(Inv.Theme.EdgeColour);

      var ColumnArray = Base.Table.GetColumns().ToArray();

      while (ActiveCellList.Count > ColumnArray.Length)
      {
        var Cell = ActiveCellList.RemoveLastOrDefault();
        Cell.Clear();

        ColumnCellDictionary.Remove(Cell.Column);
        Cell.Column = null;

        InactiveCellList.Add(Cell);
        Base.RemoveCell(Cell);
      }

      for (var ColumnIndex = 0; ColumnIndex < ColumnArray.Length; ColumnIndex++)
      {
        var Column = ColumnArray[ColumnIndex];

        var Cell = ActiveCellList.ElementAtOrDefault(ColumnIndex);

        if (Cell == null)
        {
          Cell = InactiveCellList.RemoveLastOrDefault();

          if (Cell == null)
            Cell = new DataCell();

          ActiveCellList.Add(Cell);
          Base.AddCell(Cell);
        }

        if (Cell.Column != Column)
        {
          Cell.Column = Column;
          ColumnCellDictionary[Column] = Cell;
        }

        RefreshCell(Cell);

        Cell.Clear();
      }
    }
    internal void RefreshCellByColumn(DataColumn Column)
    {
      var Cell = GetCell(Column);
      if (Cell != null)
        RefreshCell(Cell);
    }
    internal void Recycle()
    {
      this.Context = null;
      Base.IsFocusable = false;

      foreach (var Cell in ActiveCellList)
        Cell.Clear();
    }

    private void RefreshCell(DataCell Cell)
    {
      Cell.Size.SetWidth(Cell.Column.ActualWidth);
      Cell.Visibility.Set(Cell.Column.Visibility.Get());
      Cell.Padding.Set(Cell.Column.HorizontalPadding, 0, Cell.Column.HorizontalPadding, 0);
    }
    private DataCell GetCell(DataColumnContract Column) => ColumnCellDictionary.GetValueOrDefault(Column.Control);
    private DataCell EnsureCell(DataColumnContract Column)
    {
      var Result = GetCell(Column);

      if (Result == null)
        throw new Exception($"Cell for column {Column.Control.Definition} was not available");

      return Result;
    }

    private readonly List<DataCell> ActiveCellList;
    private readonly List<DataCell> InactiveCellList;
    private readonly Dictionary<DataColumn, DataCell> ColumnCellDictionary;
  }

  public sealed class DataGroupRow : Inv.Panel<DataContentRow>
  {
    internal DataGroupRow(DataContentRow Base)
    {
      this.Base = Base;
      Base.Background.In(Inv.Material.Theme.PrimaryColour.Lighten(0.80F));
      Base.SingleTapEvent += () => ExpandChangeEvent?.Invoke();

      this.Label = Inv.Label.New();
      Base.AddCell(Label);
      Label.Justify.Left();
      Label.Margin.Set(0, 0, 0, 1);
      Label.Alignment.CenterStretch();
      Label.Font.Medium().In(Inv.Material.Theme.PrimaryColour);
    }

    public void Compose(string Text, bool IsExpanded)
    {
      Label.Text = Text;
      Base.IsExpanded = IsExpanded;
    }

    internal bool IsChecking
    {
      get => Base.IsChecking;
      set => Base.IsChecking = value;
    }
    internal bool? IsChecked
    {
      get => Base.IsChecked;
      set => Base.IsChecked = value;
    }

    internal void AttachCheckChange(Action CheckAction) => Base.AttachCheckChange(CheckAction);
    internal void AttachExpandChange(Action ExpandAction)
    {
      ExpandChangeEvent = null;
      ExpandChangeEvent += ExpandAction;
    }
    internal void Refresh()
    {
      Base.IsCollated = true;
      Base.Border.In(Inv.Theme.PrimaryColour);
    }
    internal void RefreshDensity()
    {
      Base.Size.SetHeight(Base.Table.IsDense() ? 36 : 42);

      Label.Font.Custom(Base.Table.GetFontSize());
      Label.Padding.Set(Base.Table.GetColumnPadding(), 0);
    }

    private readonly Inv.Label Label;
    private event Action ExpandChangeEvent;
  }
  public sealed class DataFooterRow : Inv.Panel<DataContentRow>
  {
    internal DataFooterRow(DataContentRow Base)
    {
      this.Base = Base;
      Base.Border.Set(0);
      
      this.ColumnCellDictionary = new Dictionary<DataColumn, DataCell>();
    }

    public Visibility Visibility => Base.Visibility;
    
    public void Compose(DataColumn Column)
    {
      var Cell = EnsureCell(Column).AsString();
      Cell.Justify.Set(Column.Justify.Get());
      Cell.Content = Column.Footer.Content;
    }
    internal void Refresh()
    {
      Base.IsFocusable = false;
      Base.Size.SetHeight(Base.Table.GetRowHeight());
    }
    internal void Prepare()
    {
      Base.Margin.Set(0);
      Base.Border.In(Inv.Theme.EdgeColour);

      var ColumnArray = Base.Table.GetColumns().ToArray();
      ColumnCellDictionary.Values.ForEach(C => Base.RemoveCell(C));
      ColumnCellDictionary.Clear();

      foreach (var Column in ColumnArray)
      {
        var Cell = new DataCell();
        Cell.Column = Column;
        Base.AddCell(Cell);
        ColumnCellDictionary[Column] = Cell;
      }
    }

    private void RefreshCell(DataCell Cell)
    {
      Cell.Size.SetWidth(Cell.Column.ActualWidth);
      Cell.Visibility.Set(Cell.Column.Visibility.Get());
      Cell.Padding.Set(Cell.Column.HorizontalPadding, 0, Cell.Column.HorizontalPadding, 0);
    }
    private DataCell GetCell(DataColumnContract Column) => ColumnCellDictionary.GetValueOrDefault(Column.Control);
    private DataCell EnsureCell(DataColumnContract Column)
    {
      var Result = GetCell(Column);

      if (Result == null)
        throw new Exception($"Cell for column {Column.Control.Definition} was not available");

      RefreshCell(Result);

      return Result;
    }
    private readonly Dictionary<DataColumn, DataCell> ColumnCellDictionary;
  }
  internal sealed class DataItem
  {
    public bool IsGroup { get; private set; }
    public DataGroup Group { get; private set; }
    public object Context { get; private set; }

    internal bool IsVisible() => Group == null || IsGroup || Group.IsExpanded;

    internal static DataItem NewGroupHeader(object Context, string Text, IEqualityComparer<object> EqualityComparer)
    {
      return new DataItem()
      {
        Context = null,
        IsGroup = true,
        Group = new DataGroup(Context, Text, EqualityComparer)
      };
    }
    internal static DataItem NewGroupItem(object Context, DataGroup Group)
    {
      return new DataItem()
      {
        Context = Context,
        IsGroup = false,
        Group = Group
      };
    }
    internal static DataItem NewInlineItem(object Context)
    {
      return new DataItem()
      {
        Context = Context,
        IsGroup = false,
        Group = null
      };
    }
  }

  internal sealed class DataGroup
  {
    internal DataGroup(object Context, string Text, IEqualityComparer<object> EqualityComparer)
    {
      this.Context = Context;
      this.Text = Text;
      this.ContextSet = new HashSet<object>(EqualityComparer);
      this.IsExpanded = true;
    }

    public object Context { get; private set; }
    public string Text { get; private set; }
    public HashSet<object> ContextSet { get; private set; }
    public bool IsExpanded { get; set; }
  }

  internal interface DataCellContract
  {
    Inv.Panel Panel { get; }
  }

  internal sealed class DataCell : Inv.Panel<Inv.Frame>
  {
    internal DataCell()
    {
      this.Base = Inv.Frame.New();
    }

    public DataCellContract Active
    {
      get => ActiveCell;
      set
      {
        if (value != ActiveCell)
        {
          this.Content = value?.Panel;
          ActiveCell = value;
        }
      }
    }
    public Inv.Visibility Visibility => Base.Visibility;

    internal Inv.Panel Content
    {
      get => Base.Content;
      set => Base.Content = value;
    }
    internal DataColumn Column { get; set; }
    internal Inv.Margin Margin => Base.Margin;
    internal Inv.Padding Padding => Base.Padding;
    internal Inv.Size Size => Base.Size;
    internal Inv.Border Border => Base.Border;
    internal Inv.Background Background => Base.Background;

    internal void Clear()
    {
      //this.Active = null;

      BlockCell?.Clear();
      ButtonCell?.Clear();
      CountCell?.Clear();
      ImageCell?.Clear();
      IntegerCell?.Clear();
      StringCell?.Clear();
      DecimalCell?.Clear();
      TimeCell?.Clear();
      TimestampCell?.Clear();
    }
    internal void AsCustom<TPanel>(RecyclePanelContainer<TPanel> Container)
      where TPanel : Inv.Panel, new()
    {
      Container.Parent(Base);

      ActiveCell = null;
    }
    internal BlockDataCell AsBlock()
    {
      if (BlockCell == null)
        BlockCell = new BlockDataCell();

      BlockCell.SetFontSize(Column.Table.GetFontSize());

      this.Active = BlockCell;

      return BlockCell;
    }
    internal ButtonDataCell AsButton()
    {
      if (ButtonCell == null)
        ButtonCell = new ButtonDataCell();

      ButtonCell.SetFontSize(Column.Table.GetFontSize());

      this.Active = ButtonCell;

      return ButtonCell;
    }
    internal ColourDataCell AsColour()
    {
      if (ColourCell == null)
        ColourCell = new ColourDataCell();

      this.Active = ColourCell;

      return ColourCell;
    }
    internal CountDataCell AsCount()
    {
      if (CountCell == null)
        CountCell = new CountDataCell();

      CountCell.SetFontSize(Column.Table.GetFontSize());

      this.Active = CountCell;

      return CountCell;
    }
    internal DateDataCell AsDate()
    {
      if (DateCell == null)
        DateCell = new DateDataCell();

      DateCell.SetFontSize(Column.Table.GetFontSize());

      this.Active = DateCell;

      return DateCell;
    }
    internal DecimalDataCell AsDecimal()
    {
      if (DecimalCell == null)
        DecimalCell = new DecimalDataCell();

      DecimalCell.SetFontSize(Column.Table.GetFontSize());

      this.Active = DecimalCell;

      return DecimalCell;
    }
    internal ImageDataCell AsImage()
    {
      if (ImageCell == null)
        ImageCell = new ImageDataCell();

      this.Active = ImageCell;

      return ImageCell;
    }
    internal IntegerDataCell AsInteger()
    {
      if (IntegerCell == null)
        IntegerCell = new IntegerDataCell();

      IntegerCell.SetFontSize(Column.Table.GetFontSize());

      this.Active = IntegerCell;

      return IntegerCell;
    }
    internal Integer64DataCell AsInteger64()
    {
      if (Integer64Cell == null)
        Integer64Cell = new Integer64DataCell();

      Integer64Cell.SetFontSize(Column.Table.GetFontSize());

      this.Active = Integer64Cell;

      return Integer64Cell;
    }
    internal MoneyDataCell AsMoney()
    {
      if (MoneyCell == null)
        MoneyCell = new MoneyDataCell();

      MoneyCell.SetFontSize(Column.Table.GetFontSize());

      this.Active = MoneyCell;

      return MoneyCell;
    }
    internal StringDataCell AsString()
    {
      if (StringCell == null)
        StringCell = new StringDataCell();

      StringCell.SetFontSize(Column.Table.GetFontSize());

      this.Active = StringCell;

      return StringCell;
    }
    internal TimeDataCell AsTime()
    {
      if (TimeCell == null)
        TimeCell = new TimeDataCell();

      TimeCell.SetFontSize(Column.Table.GetFontSize());

      this.Active = TimeCell;

      return TimeCell;
    }
    internal TimestampDataCell AsTimestamp()
    {
      if (TimestampCell == null)
        TimestampCell = new TimestampDataCell();

      TimestampCell.SetFontSize(Column.Table.GetFontSize());

      this.Active = TimestampCell;

      return TimestampCell;
    }

    private DataCellContract ActiveCell;
    private BlockDataCell BlockCell;
    private ButtonDataCell ButtonCell;
    private ColourDataCell ColourCell;
    private CountDataCell CountCell;
    private DateDataCell DateCell;
    private DecimalDataCell DecimalCell;
    private ImageDataCell ImageCell;
    private IntegerDataCell IntegerCell;
    private Integer64DataCell Integer64Cell;
    private MoneyDataCell MoneyCell;
    private StringDataCell StringCell;
    private TimeDataCell TimeCell;
    private TimestampDataCell TimestampCell;
  }

  internal enum DataExportType
  {
    Date,
    Decimal,
    Integer,
    Integer64,
    Money,
    Text,
    Time,
    Timestamp
  }

  public sealed class BlockDataColumn : DataColumnContract
  {
    internal BlockDataColumn(DataColumn Base)
    {
      this.Base = Base;
    }

    public string Title
    {
      get => Base.Title;
      set => Base.Title = value;
    }
    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public int Width
    {
      get => Base.Width;
      set => Base.Width = value;
    }
    public DataSort Sort
    {
      set => Base.Sort = value;
    }
    public DataFooter Footer => Base.Footer;
    public DataColumnVisibility Visibility => Base.Visibility;
    public event Action<DataExportCell> ExportEvent
    {
      add => Base.ExportEvent += value;
      remove => Base.ExportEvent -= value;
    }

    internal bool HasExport => Base.HasExport;
    internal event Action<DataContextRow> ComposeEvent
    {
      add => Base.ComposeEvent += value;
      remove => Base.ComposeEvent -= value;
    }

    internal BlockDataColumn<T> Wrap<T>() => new BlockDataColumn<T>(this);

    DataColumn DataColumnContract.Control => Base;

    private readonly DataColumn Base;
  }

  public sealed class BlockDataColumn<T> : DataColumnContract
  {
    internal BlockDataColumn(BlockDataColumn Base)
    {
      this.Base = Base;
    }

    public string Title
    {
      get => Base.Title;
      set => Base.Title = value;
    }
    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public int Width
    {
      get => Base.Width;
      set => Base.Width = value;
    }
    public DataSort Sort
    {
      set => Base.Sort = value;
    }
    public DataColumnVisibility Visibility => Base.Visibility;
    public event Action<DataExportCell<T>> ExportEvent
    {
      add
      {
        if (value != null)
        {
          if (ExportDelegate == null && !Base.HasExport)
            Base.ExportEvent += ExportHandler;

          this.ExportDelegate += value;
        }
      }
      remove
      {
        if (value != null)
        {
          this.ExportDelegate -= value;

          if (ExportDelegate == null && Base.HasExport)
            Base.ExportEvent -= ExportHandler;
        }
      }
    }

    DataColumn DataColumnContract.Control => (Base as DataColumnContract).Control;

    private void ExportHandler(DataExportCell Cell) => ExportDelegate(Cell.Wrap<T>());

    private readonly BlockDataColumn Base;
    private Action<DataExportCell<T>> ExportDelegate;
  }

  public sealed class BlockDataCell : Inv.Panel<Inv.Block>, DataCellContract
  {
    internal BlockDataCell()
    {
      this.Base = Inv.Block.New();
      Base.Font.In(Inv.Theme.OnBackgroundColour);
      Base.LineWrapping = false;
    }

    public Inv.Block Content => Base;

    internal void Clear()
    {
      Base.RemoveSpans();
      Base.Tooltip.Content = null;
      Base.Tooltip.RemoveShow();
      Base.Tooltip.RemoveHide();
    }
    internal void SetFontSize(int Size)
    {
      Base.Font.Custom(Size);
    }

    Inv.Panel DataCellContract.Panel => Base;
  }

  public sealed class ButtonDataColumn : DataColumnContract
  {
    internal ButtonDataColumn(DataColumn Base)
    {
      this.Base = Base;
    }

    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public string Title
    {
      get => Base.Title;
      set => Base.Title = value;
    }
    public DataColumnVisibility Visibility => Base.Visibility;
    public event Action<DataExportCell> ExportEvent
    {
      add => Base.ExportEvent += value;
      remove => Base.ExportEvent -= value;
    }
    public DataFooter Footer => Base.Footer;

    internal bool HasExport => Base.HasExport;
    internal event Action<DataContextRow> ComposeEvent
    {
      add => Base.ComposeEvent += value;
      remove => Base.ComposeEvent -= value;
    }

    internal ButtonDataColumn<T> Wrap<T>() => new ButtonDataColumn<T>(this);

    DataColumn DataColumnContract.Control => Base;

    private readonly DataColumn Base;
  }

  public sealed class ButtonDataColumn<T> : DataColumnContract
  {
    internal ButtonDataColumn(ButtonDataColumn Base)
    {
      this.Base = Base;
    }

    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public DataColumnVisibility Visibility => Base.Visibility;
    public DataFooter Footer => Base.Footer;
    public event Action<DataExportCell<T>> ExportEvent
    {
      add
      {
        if (value != null)
        {
          if (ExportDelegate == null && !Base.HasExport)
            Base.ExportEvent += ExportHandler;

          this.ExportDelegate += value;
        }
      }
      remove
      {
        if (value != null)
        {
          this.ExportDelegate -= value;

          if (ExportDelegate == null && Base.HasExport)
            Base.ExportEvent -= ExportHandler;
        }
      }
    }

    DataColumn DataColumnContract.Control => (Base as DataColumnContract).Control;

    private void ExportHandler(DataExportCell Cell) => ExportDelegate(Cell.Wrap<T>());

    private readonly ButtonDataColumn Base;
    private Action<DataExportCell<T>> ExportDelegate;
  }

  public sealed class ButtonDataCell : Inv.Panel<Inv.Button>, DataCellContract
  {
    internal ButtonDataCell()
    {
      this.Base = Inv.Button.NewStark();
      Base.Alignment.Stretch();
      Base.Codepoint.Caller();
      Base.Corner.Set(2);
      Base.Size.SetHeight(36);
      Base.Size.SetMinimumWidth(64);
      Base.OverEvent += () => Refresh();
      Base.AwayEvent += () => Refresh();
      Base.SingleTapEvent += () => SingleTapEvent?.Invoke();

      this.Dock = Inv.Dock.NewHorizontal();
      Base.Content = Dock;

      this.Label = Inv.Label.New();
      Dock.AddClient(Label);
      Label.Alignment.CenterLeft();
      Label.Justify.Left();
      Label.Margin.Set(8, 8);
      Label.Font.In(Inv.Theme.OnBackgroundColour);
      Label.LineWrapping = false;

      this.Icon = new Inv.Material.Icon();
      Dock.AddFooter(Icon);
      Icon.Size.Set(32);
      Icon.Alignment.Center();
      Icon.Padding.Set(8, 0);
      Icon.Visibility.Collapse();
      Icon.Colour = Inv.Theme.SoftColour;

      this.BackgroundColour = Inv.Colour.Transparent;

      Refresh();
    }

    public string Title
    {
      get => Label.Text;
      set => Label.Text = value;
    }
    public Font Font
    {
      get => Label.Font;
    }
    public Margin TitleMargin
    {
      get => Label.Margin;
    }
    public Inv.Orientation Orientation
    {
      get => Dock.Orientation;
      set => Dock.SetOrientation(value);
    }

    public Inv.Alignment TitleAlignment 
    { 
      get => Label.Alignment; 
    }
    public Inv.Image Image
    {
      get => Icon.Image;
      set
      {
        Icon.Image = value;
        Icon.Visibility.Set(value != null);
      }
    }
    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set => Base.IsEnabled = value;
    }
    public event Action SingleTapEvent;
    public void SetBackgroundColour(Inv.Colour Colour)
    {
      BackgroundColour = Colour;
      Refresh();
    }

    internal void Clear()
    {
      //Label.Text = string.Empty;
      SingleTapEvent = null;
    }
    internal void SetFontSize(int Size)
    {
      Label.Font.Custom(Size);
    }

    Inv.Panel DataCellContract.Panel => Base;

    private void Refresh()
    {
      Base.Background.In(Base.IsOver ? Inv.Theme.EdgeColour : BackgroundColour);
    }

    private Inv.Colour BackgroundColour;

    private readonly Inv.Label Label;
    private readonly Inv.Material.Icon Icon;
    private readonly Inv.Dock Dock;
  }

  public sealed class RecyclePanelDataColumn<TPanel> : DataColumnContract
    where TPanel : Inv.Panel, new()
  {
    internal RecyclePanelDataColumn(DataColumn Base)
    {
      this.Base = Base;
      Base.RecycleEvent += () =>
      {
        // If there were previously used containers that were not used since the last recycle, then dispose of them.
        InactiveContainerDictionary.Clear();

        // Cache the previously available container dictionary.
        InactiveContainerDictionary.AddRange(ContainerDictionary);

        ContainerDictionary.Clear();
      };

      this.InactiveContainerDictionary = new Dictionary<object, RecyclePanelContainer<TPanel>>();
      this.ContainerDictionary = new Dictionary<object, RecyclePanelContainer<TPanel>>();
    }

    public string Title
    {
      get => Base.Title;
      set => Base.Title = value;
    }
    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public DataSort Sort
    {
      get => Base.Sort;
      set => Base.Sort = value;
    }
    public DataColumnVisibility Visibility => Base.Visibility;
    public event Action<DataExportCell> ExportEvent
    {
      add => Base.ExportEvent += value;
      remove => Base.ExportEvent -= value;
    }

    public IEnumerable<TPanel> GetCells() => ContainerDictionary.Values.Select(C => C.Content);

    internal bool HasExport => Base.HasExport;
    internal IEqualityComparer<object> EqualityComparer
    {
      set
      {
        if (EqualityComparerField != value)
        {
          ContainerDictionary = new Dictionary<object, RecyclePanelContainer<TPanel>>(ContainerDictionary, value);
          InactiveContainerDictionary = new Dictionary<object, RecyclePanelContainer<TPanel>>(InactiveContainerDictionary, value);
          this.EqualityComparerField = value;
        }
      }
    }
    internal event Action<DataContextRow> ComposeEvent
    {
      add => Base.ComposeEvent += value;
      remove => Base.ComposeEvent -= value;
    }

    internal RecyclePanelContainer<TPanel> GetCell(object Context)
    {
      var Result = ContainerDictionary.GetValueOrDefault(Context);
      
      if (Result == null)
      {
        Result = InactiveContainerDictionary.RemoveValueOrDefault(Context) ?? new RecyclePanelContainer<TPanel>();
        ContainerDictionary.Add(Context, Result);
      }

      return Result;
    }
    internal RecyclePanelDataColumn<T, TPanel> Wrap<T>() => new RecyclePanelDataColumn<T, TPanel>(this);

    DataColumn DataColumnContract.Control => ((DataColumnContract)Base).Control;

    private readonly DataColumn Base;
    private IEqualityComparer<object> EqualityComparerField;
    private Dictionary<object, RecyclePanelContainer<TPanel>> ContainerDictionary;
    private Dictionary<object, RecyclePanelContainer<TPanel>> InactiveContainerDictionary;
  }

  internal sealed class RecyclePanelContainer<TPanel>
    where TPanel : Inv.Panel, new()
  { 
    public RecyclePanelContainer()
    {
      this.Content = new TPanel();
    }

    public TPanel Content { get; }

    public void Parent(Inv.Frame Frame)
    {
      // 2023-07-24 KJV
      // Keep a reference to the owner (ParentFrame) of this reused panel (Content) so that it can be reparented appropriately.
      // This was introduced because the recycling of the TPanel was causing the panel to be parented to a new parent without
      //  being disconnected from its previous parent.

      // The commented out check for whether ParentFrame was changing was insufficient as Frame.Content was being nulled somewhere external to this code in some scenarios.
      //  In this case, the if() check would fail, Frame.Content would not be reset to the expected value, and the cell would appear blank. I was unable to determine where this was occurring though.
      // Always setting Frame.Content if it is not the expected value appears to have addressed this problem.

      // if (ParentFrame != Frame)
      // {

      if (ParentFrame?.Content == (Inv.Panel)Content)
        ParentFrame.Content = null;

      this.ParentFrame = Frame;

      if (Frame?.Content != (Inv.Panel)Content)
        Frame.Content = Content;

      // }
    }

    private Inv.Frame ParentFrame;
  }

  public sealed class RecyclePanelDataColumn<T, TPanel> : DataColumnContract
      where TPanel : Inv.Panel, new()
  {
    internal RecyclePanelDataColumn(RecyclePanelDataColumn<TPanel> Base)
    {
      this.Base = Base;
    }

    public string Title
    {
      get => Base.Title;
      set => Base.Title = value;
    }
    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public DataSort Sort
    {
      get => Base.Sort;
      set => Base.Sort = value;
    }
    public DataColumnVisibility Visibility => Base.Visibility;
    public event Action<DataExportCell<T>> ExportEvent
    {
      add
      {
        if (value != null)
        {
          if (ExportDelegate == null && !Base.HasExport)
            Base.ExportEvent += ExportHandler;

          this.ExportDelegate += value;
        }
      }
      remove
      {
        if (value != null)
        {
          this.ExportDelegate -= value;

          if (ExportDelegate == null && Base.HasExport)
            Base.ExportEvent -= ExportHandler;
        }
      }
    }

    public IEnumerable<TPanel> GetCells() => Base.GetCells();

    DataColumn DataColumnContract.Control => ((DataColumnContract)Base).Control;

    private void ExportHandler(DataExportCell Cell) => ExportDelegate(Cell.Wrap<T>());

    private readonly RecyclePanelDataColumn<TPanel> Base;
    private Action<DataExportCell<T>> ExportDelegate;
  }

  public sealed class ColourDataColumn : DataColumnContract
  {
    internal ColourDataColumn(DataColumn Base)
    {
      this.Base = Base;
    }
    public DataFooter Footer => Base.Footer;

    internal event Action<DataContextRow> ComposeEvent
    {
      add => Base.ComposeEvent += value;
      remove => Base.ComposeEvent -= value;
    }
    DataColumn DataColumnContract.Control => Base;

    private readonly DataColumn Base;
  }

  public sealed class ColourDataCell : Inv.Panel<Inv.Frame>, DataCellContract
  {
    internal ColourDataCell()
    {
      this.Base = Inv.Frame.New();
      Base.Alignment.Center();
      Base.Corner.Set(3);
      Base.Size.Set(28);
    }

    public Inv.Colour Content
    {
      get => Value;
      set
      {
        if (Value != value)
        {
          this.Value = value;
          Refresh();
        }
      }
    }

    Inv.Panel DataCellContract.Panel => Base;

    private void Refresh()
    {
      Base.Background.In(Value ?? Inv.Colour.Transparent);
    }

    private Inv.Colour Value;
  }

  public sealed class CountDataColumn : DataColumnContract
  {
    internal CountDataColumn(DataColumn Base)
    {
      this.Base = Base;
    }

    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public DataColumnJustify Justify => Base.Justify;
    public DataSort Sort
    {
      set => Base.Sort = value;
    }
    public DataFooter Footer => Base.Footer;
    public DataColumnVisibility Visibility => Base.Visibility;

    internal event Action<DataContextRow> ComposeEvent
    {
      add => Base.ComposeEvent += value;
      remove => Base.ComposeEvent -= value;
    }
    internal event Action<DataExportCell> ExportEvent
    {
      add => Base.ExportEvent += value;
      remove => Base.ExportEvent -= value;
    }

    DataColumn DataColumnContract.Control => Base;

    private readonly DataColumn Base;
  }

  public sealed class CountDataCell : Inv.Panel<Inv.Dock>, DataCellContract
  {
    internal CountDataCell()
    {
      this.Base = Inv.Dock.NewHorizontal();
      Base.Alignment.CenterStretch();

      this.MinusButton = new IconButton();
      Base.AddHeader(MinusButton);
      MinusButton.Image = Inv.Material.Resources.Images.ChevronLeft;
      MinusButton.ForegroundColour = Inv.Theme.SubtleColour;
      MinusButton.Small();
      MinusButton.SingleTapEvent += () =>
      {
        Value = Math.Max(0, Value - 1);
        Refresh();
        Change();
      };

      this.ValueLabel = new Inv.Label();
      Base.AddClient(ValueLabel);
      ValueLabel.Alignment.CenterStretch();
      ValueLabel.Justify.Center();
      ValueLabel.Margin.Set(8, 0);
      ValueLabel.Font.In(Inv.Theme.OnBackgroundColour);
      ValueLabel.LineWrapping = false;
      ValueLabel.Text = "0";

      this.PlusButton = new IconButton();
      Base.AddFooter(PlusButton);
      PlusButton.Image = Inv.Material.Resources.Images.ChevronRight;
      PlusButton.ForegroundColour = Inv.Theme.SubtleColour;
      PlusButton.Small();
      PlusButton.SingleTapEvent += () =>
      {
        Value = Math.Min(int.MaxValue, Value + 1);
        Refresh();
        Change();
      };
    }

    public int Content
    {
      get => Value;
      set
      {
        if (Value != value)
        {
          this.Value = value;
          Refresh();
        }
      }
    }
    public Inv.Font Font => ValueLabel.Font;
    public event Action ChangeEvent;

    internal void Clear()
    {
      //this.Value = 0;
      //Refresh();
      ChangeEvent = null;
    }
    internal void SetFontSize(int Size)
    {
      ValueLabel.Font.Custom(Size);
    }

    Inv.Panel DataCellContract.Panel => Base;

    private void Change()
    {
      ChangeEvent?.Invoke();
    }
    private void Refresh()
    {
      ValueLabel.Text = Value.ToString();
    }

    private readonly Inv.Material.IconButton MinusButton;
    private readonly Inv.Label ValueLabel;
    private readonly Inv.Material.IconButton PlusButton;
    private int Value;
  }

  public sealed class DateDataColumn : DataColumnContract
  {
    internal DateDataColumn(DataColumn Base)
    {
      this.Base = Base;

      this.Format = "dd/MM/yyyy";
    }

    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public string Format { get; set; }
    public DataSort Sort
    {
      get => Base.Sort;
      set => Base.Sort = value;
    }
    public DataFooter Footer => Base.Footer;
    public DataColumnVisibility Visibility => Base.Visibility;

    internal event Action<DataContextRow> ComposeEvent
    {
      add => Base.ComposeEvent += value;
      remove => Base.ComposeEvent -= value;
    }
    internal event Action<DataExportCell> ExportEvent
    {
      add => Base.ExportEvent += value;
      remove => Base.ExportEvent -= value;
    }

    DataColumn DataColumnContract.Control => Base;

    private readonly DataColumn Base;
  }

  public sealed class DateDataCell : Inv.Panel<Inv.Label>, DataCellContract
  {
    internal DateDataCell()
    {
      this.Base = Inv.Label.New();
      Base.Font.In(Inv.Theme.OnBackgroundColour);
    }

    public string Format
    {
      get => FormatField;
      set
      {
        if (FormatField != value)
        {
          this.FormatField = value;
          Refresh();
        }
      }
    }
    public Inv.Date? Content
    {
      get => Value;
      set
      {
        if (value != Value)
        {
          this.Value = value;
          Refresh();
        }
      }
    }
    public Inv.Font Font => Base.Font;
    public Inv.Justify Justify => Base.Justify;

    internal void Clear()
    {
      this.Value = null;
      Base.Text = string.Empty;
    }
    internal void SetFontSize(int Size)
    {
      Base.Font.Custom(Size);
    }

    Inv.Panel DataCellContract.Panel => Base;

    private void Refresh()
    {
      Base.Text = Value?.ToString(FormatField) ?? string.Empty;
    }

    private Inv.Date? Value;
    private string FormatField;
  }

  public sealed class DecimalDataColumn : DataColumnContract
  {
    internal DecimalDataColumn(DataColumn Base)
    {
      this.Base = Base;

      // Right-aligned by default.
      Justify.Right();
    }

    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public DataColumnJustify Justify => Base.Justify;
    public DataSort Sort
    {
      set => Base.Sort = value;
    }
    public DataFooter Footer => Base.Footer;
    public DataColumnVisibility Visibility => Base.Visibility;

    internal event Action<DataContextRow> ComposeEvent
    {
      add => Base.ComposeEvent += value;
      remove => Base.ComposeEvent -= value;
    }
    internal event Action<DataExportCell> ExportEvent
    {
      add => Base.ExportEvent += value;
      remove => Base.ExportEvent -= value;
    }

    DataColumn DataColumnContract.Control => Base;

    private readonly DataColumn Base;
  }

  public sealed class DecimalDataCell : Inv.Panel<Inv.Label>, DataCellContract
  {
    internal DecimalDataCell()
    {
      this.Base = Inv.Label.New();
      Base.Font.In(Inv.Theme.OnBackgroundColour);
      Base.LineWrapping = false;
    }

    public decimal? Content
    {
      get => Value;
      set
      {
        if (value != Value)
        {
          this.Value = value;
          Base.Text = value?.ToString() ?? string.Empty;
        }
      }
    }

    internal Inv.Justify Justify => Base.Justify;

    internal void Clear()
    {
      //this.Value = null;
      //Base.Text = string.Empty;
    }
    internal void SetFontSize(int Size)
    {
      Base.Font.Custom(Size);
    }

    Inv.Panel DataCellContract.Panel => Base;

    private decimal? Value;
  }

  public sealed class ImageDataColumn : DataColumnContract
  {
    internal ImageDataColumn(DataColumn Base)
    {
      this.Base = Base;
    }

    public string Title
    {
      get => Base.Title;
      set => Base.Title = value;
    }
    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public int Width
    {
      get => Base.Width;
      set => Base.Width = value;
    }
    public Inv.Image Image
    {
      get => Base.Image;
      set => Base.Image = value;
    }
    public DataColumnVisibility Visibility => Base.Visibility;
    public DataSort Sort
    {
      set => Base.Sort = value;
    }
    public DataFooter Footer => Base.Footer;

    internal event Action<DataContextRow> ComposeEvent
    {
      add => Base.ComposeEvent += value;
      remove => Base.ComposeEvent -= value;
    }

    DataColumn DataColumnContract.Control => Base;

    private readonly DataColumn Base;
  }

  public sealed class ImageDataCell : Inv.Panel<Inv.Graphic>, DataCellContract
  {
    internal ImageDataCell()
    {
      this.Base = Inv.Graphic.New();
      Base.Size.Set(24);
      Base.Alignment.CenterLeft();
      //Base.Margin.Set(0, 0, 12, 0);
    }

    public int Size
    {
      set => Base.Size.Set(value);
    }
    public Inv.Image Content
    {
      get => Base.Image;
      set
      {
        Base.Image = value;
        Base.Visibility.Set(Base.Image != null);
      }
    }
    public Inv.Tooltip Tooltip => Base.Tooltip;

    internal void Clear()
    {
      //Base.Image = null;
      Base.Tooltip.Content = null;
      Base.Tooltip.RemoveShow();
      Base.Tooltip.RemoveHide();
      Base.Visibility.Collapse();
    }

    Inv.Panel DataCellContract.Panel => Base;
  }

  public sealed class IntegerDataColumn : DataColumnContract
  {
    internal IntegerDataColumn(DataColumn Base)
    {
      this.Base = Base;
    }

    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public DataColumnJustify Justify => Base.Justify;
    public DataSort Sort
    {
      set => Base.Sort = value;
    }
    public DataFooter Footer => Base.Footer;
    public bool LineWrapping { get; set; }
    public DataColumnVisibility Visibility => Base.Visibility;

    internal event Action<DataContextRow> ComposeEvent
    {
      add => Base.ComposeEvent += value;
      remove => Base.ComposeEvent -= value;
    }
    internal event Action<DataExportCell> ExportEvent
    {
      add => Base.ExportEvent += value;
      remove => Base.ExportEvent -= value;
    }

    DataColumn DataColumnContract.Control => Base;

    private readonly DataColumn Base;
  }

  public sealed class IntegerDataCell : Inv.Panel<Inv.Label>, DataCellContract
  {
    internal IntegerDataCell()
    {
      this.Base = Inv.Label.New();
      Base.Font.In(Inv.Theme.OnBackgroundColour);
      Base.LineWrapping = false;
    }

    public int? Content
    {
      get => Value;
      set
      {
        if (value != Value)
        {
          this.Value = value;
          Base.Text = value?.ToString() ?? string.Empty;
        }
      }
    }
    public Inv.Font Font => Base.Font;
    public Inv.Justify Justify => Base.Justify;
    public bool LineWrapping
    {
      get => Base.LineWrapping;
      set => Base.LineWrapping = value;
    }

    internal void Clear()
    {
      //this.Value = null;
      //Base.Text = string.Empty;
    }
    internal void SetFontSize(int Size)
    {
      Base.Font.Custom(Size);
    }

    Inv.Panel DataCellContract.Panel => Base;

    private int? Value;
  }
  public sealed class Integer64DataCell : Inv.Panel<Inv.Label>, DataCellContract
  {
    internal Integer64DataCell()
    {
      this.Base = Inv.Label.New();
      Base.Font.In(Inv.Theme.OnBackgroundColour);
      Base.LineWrapping = false;
    }

    public long? Content
    {
      get => Value;
      set
      {
        if (value != Value)
        {
          this.Value = value;
          Base.Text = value?.ToString() ?? string.Empty;
        }
      }
    }
    public Inv.Font Font => Base.Font;
    public Inv.Justify Justify => Base.Justify;
    public bool LineWrapping
    {
      get => Base.LineWrapping;
      set => Base.LineWrapping = value;
    }

    internal void Clear()
    {
      //this.Value = null;
      //Base.Text = string.Empty;
    }
    internal void SetFontSize(int Size)
    {
      Base.Font.Custom(Size);
    }

    Inv.Panel DataCellContract.Panel => Base;

    private long? Value;
  }

  public sealed class MoneyDataColumn : DataColumnContract
  {
    internal MoneyDataColumn(DataColumn Base)
    {
      this.Base = Base;
    }

    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public DataColumnJustify Justify => Base.Justify;
    public DataSort Sort
    {
      set => Base.Sort = value;
    }
    public DataFooter Footer => Base.Footer;
    public DataColumnVisibility Visibility => Base.Visibility;

    internal event Action<DataContextRow> ComposeEvent
    {
      add => Base.ComposeEvent += value;
      remove => Base.ComposeEvent -= value;
    }
    internal event Action<DataExportCell> ExportEvent
    {
      add => Base.ExportEvent += value;
      remove => Base.ExportEvent -= value;
    }
    DataColumn DataColumnContract.Control => Base;

    private readonly DataColumn Base;
  }

  public sealed class MoneyDataCell : Inv.Panel<Inv.Label>, DataCellContract
  {
    internal MoneyDataCell()
    {
      this.Base = Inv.Label.New();
      Base.Font.In(Inv.Theme.OnBackgroundColour);
      Base.LineWrapping = false;
    }

    public Inv.Money? Content
    {
      get => Value;
      set
      {
        if (value != Value)
        {
          this.Value = value;
          Base.Text = value?.ToString() ?? string.Empty;
        }
      }
    }

    internal Inv.Justify Justify => Base.Justify;

    internal void Clear()
    {
      this.Value = null;
      Base.Text = string.Empty;
    }
    internal void SetFontSize(int Size)
    {
      Base.Font.Custom(Size);
    }

    Inv.Panel DataCellContract.Panel => Base;

    private Inv.Money? Value;
  }

  public sealed class StringDataColumn : DataColumnContract
  {
    internal StringDataColumn(DataColumn Base)
    {
      this.Base = Base;
    }

    public string Title
    {
      get => Base.Title;
      set => Base.Title = value;
    }
    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public DataColumnJustify Justify => Base.Justify;
    public DataSort Sort
    {
      get => Base.Sort;
      set => Base.Sort = value;
    }
    public DataFooter Footer => Base.Footer;
    public bool LineWrapping { get; set; }
    public DataColumnVisibility Visibility => Base.Visibility;

    internal event Action<DataContextRow> ComposeEvent
    {
      add => Base.ComposeEvent += value;
      remove => Base.ComposeEvent -= value;
    }
    internal event Action<DataExportCell> ExportEvent
    {
      add => Base.ExportEvent += value;
      remove => Base.ExportEvent -= value;
    }

    DataColumn DataColumnContract.Control => Base;

    private readonly DataColumn Base;
  }

  public sealed class StringDataCell : Inv.Panel<Inv.Label>, DataCellContract
  {
    internal StringDataCell()
    {
      this.Base = Inv.Label.New();
      Base.Font.In(Inv.Theme.OnBackgroundColour);
      Base.LineWrapping = false;
    }

    public string Content
    {
      get => Base.Text;
      set => Base.Text = value;
    }
    public Inv.Font Font => Base.Font;
    public Inv.Justify Justify => Base.Justify;
    public bool LineWrapping
    {
      get => Base.LineWrapping;
      set => Base.LineWrapping = value;
    }

    internal void Clear()
    {
      //Base.Text = string.Empty;
    }
    internal void SetFontSize(int Size)
    {
      Base.Font.Custom(Size);
    }

    Inv.Panel DataCellContract.Panel => Base;
  }

  public sealed class TimeDataColumn : DataColumnContract
  {
    internal TimeDataColumn(DataColumn Base)
    {
      this.Base = Base;
    }

    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public DataColumnJustify Justify => Base.Justify;
    public DataSort Sort
    {
      set => Base.Sort = value;
    }
    public DataFooter Footer => Base.Footer;
    public DataColumnVisibility Visibility => Base.Visibility;

    internal event Action<DataContextRow> ComposeEvent
    {
      add => Base.ComposeEvent += value;
      remove => Base.ComposeEvent -= value;
    }
    internal event Action<DataExportCell> ExportEvent
    {
      add => Base.ExportEvent += value;
      remove => Base.ExportEvent -= value;
    }

    DataColumn DataColumnContract.Control => Base;

    private readonly DataColumn Base;
  }

  public sealed class TimeDataCell : Inv.Panel<Inv.Label>, DataCellContract
  {
    internal TimeDataCell()
    {
      this.Base = Inv.Label.New();
      Base.Font.In(Inv.Theme.OnBackgroundColour);
    }

    public Inv.Time? Content
    {
      get => Value;
      set
      {
        if (value != Value)
        {
          this.Value = value;
          Base.Text = value?.ToString() ?? string.Empty;
        }
      }
    }
    public Inv.Font Font => Base.Font;
    public Inv.Justify Justify => Base.Justify;

    internal void Clear()
    {
      //this.Value = null;
      //Base.Text = string.Empty;
    }
    internal void SetFontSize(int Size)
    {
      Base.Font.Custom(Size);
    }

    Inv.Panel DataCellContract.Panel => Base;

    private Inv.Time? Value;
  }

  public sealed class TimestampDataColumn : DataColumnContract
  {
    internal TimestampDataColumn(DataColumn Base)
    {
      this.Base = Base;
    }

    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public DataColumnJustify Justify => Base.Justify;
    public DataSort Sort
    {
      set => Base.Sort = value;
    }
    public DataFooter Footer => Base.Footer;
    public DataColumnVisibility Visibility => Base.Visibility;

    internal event Action<DataContextRow> ComposeEvent
    {
      add => Base.ComposeEvent += value;
      remove => Base.ComposeEvent -= value;
    }
    internal event Action<DataExportCell> ExportEvent
    {
      add => Base.ExportEvent += value;
      remove => Base.ExportEvent -= value;
    }

    DataColumn DataColumnContract.Control => Base;

    private readonly DataColumn Base;
  }

  public sealed class TimestampDataCell : Inv.Panel<Inv.Label>, DataCellContract
  {
    internal TimestampDataCell()
    {
      this.Base = Inv.Label.New();
      Base.Font.In(Inv.Theme.OnBackgroundColour);
    }

    public DateTimeOffset? Content
    {
      get => Value;
      set
      {
        if (value != Value)
        {
          this.Value = value;
          Base.Text = value?.ToString() ?? string.Empty;
        }
      }
    }
    public Inv.Font Font => Base.Font;
    public Inv.Justify Justify => Base.Justify;

    internal void Clear()
    {
      //this.Value = null;
      //Base.Text = string.Empty;
    }
    internal void SetFontSize(int Size)
    {
      Base.Font.Custom(Size);
    }

    Inv.Panel DataCellContract.Panel => Base;

    private DateTimeOffset? Value;
  }
}