﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv.Material
{
  public sealed class Mesh : Inv.Panel<Inv.Overlay>
  {
    public Mesh()
    {
      this.Base = Inv.Overlay.New();

      this.Dock = Inv.Dock.NewHorizontal();
      Base.AddPanel(Dock);
      Dock.Elevation.Set(4);

      this.ClientDock = Inv.Dock.NewVertical();
      Dock.AddClient(ClientDock);
      ClientDock.Margin.Set(0, 0, 8, 0);
      ClientDock.Background.In(Inv.Theme.BackgroundColour);

      var RowScroll = Inv.Scroll.NewVertical();
      ClientDock.AddClient(RowScroll);
      RowScroll.Margin.Set(0, 0, 0, 8);

      this.RowStack = Inv.Stack.NewVertical();
      RowScroll.Content = RowStack;

      RowStack.Margin.Set(0, 0, 10, 0);
      RowStack.Border.Set(0, 0, 1, 1).In(Inv.Theme.Light.LightGray);
      RowStack.Alignment.TopStretch();

      this.FooterStack = Inv.Stack.NewVertical();
      ClientDock.AddFooter(FooterStack);

      this.ActionDock = Inv.Dock.NewHorizontal();
      ClientDock.AddFooter(ActionDock);
      ActionDock.Padding.Set(8);
      ActionDock.Visibility.Collapse();

      this.SideSheet = new Inv.Material.SideSheet();
      Dock.AddFooter(SideSheet);

      this.RowList = new Inv.DistinctList<MeshRow>();
      this.CheckList = new Inv.DistinctList<MeshCheck>();

      var Keyboard = Inv.Application.Access().Keyboard;
      var WasAlt = false;

      this.KeyboardBinding = Keyboard.NewBinding();
      KeyboardBinding.KeyModifierEvent += () =>
      {
        var IsAlt = Keyboard.KeyModifier.IsAlt;

        if (WasAlt != IsAlt)
        {
          foreach (var Cell in GetKeyCells())
            Cell.ShowKey(IsAlt);

          WasAlt = IsAlt;
        }
      };
      KeyboardBinding.KeyPressEvent += (Keystroke) =>
      {
        Inv.Material.MeshCell ShiftVertical(int Direction)
        {
          // Attempt to more organically select the correct cell on the relevant row, based on the relative star position of the active cell.
          if (ActiveCell == null)
            return null;

          var CellList = ActiveCell.Row.GetCells().ToList();
          var CellX = 0;
          var CellTotal = 0;
          var CellMatch = false;
          foreach (var Cell in CellList)
          {
            CellTotal += Cell.Star;

            if (Cell == ActiveCell)
              CellMatch = true;

            if (!CellMatch)
              CellX += Cell.Star;
          }
          var CellPercent = CellTotal != 0 ? CellX / (double)CellTotal : 0.0;

          var RowIndex = RowList.IndexOf(ActiveCell.Row);
          var ShiftRow = (Inv.Material.MeshRow)null;

          do
          {
            RowIndex += Direction;

            var Row = RowList.ExistsAt(RowIndex) ? RowList[RowIndex] : null;
            if (Row == null)
            {
              ShiftRow = null;
              break;
            }

            if (Row.Visibility.Get())
              ShiftRow = Row;
          }
          while (ShiftRow == null);

          if (ShiftRow == null)
            return null;

          var ShiftRowX = 0;
          var ShiftRowCellList = ShiftRow.GetCells().ToList();
          var ShiftRowTotal = ShiftRowCellList.Count > 0 ? ShiftRowCellList.Sum(C => C.Star) : 0;
          if (ShiftRowTotal > 0)
          {
            foreach (var Cell in ShiftRowCellList)
            {
              var ShiftRowPercent = ShiftRowX / (double)ShiftRowTotal;

              if (ShiftRowPercent >= CellPercent)
                return Cell;

              ShiftRowX += Cell.Star;
            }
          }

          return ShiftRowCellList.FirstOrDefault();
        }
        Inv.Material.MeshCell ShiftHorizontal(int Direction)
        {
          if (ActiveCell == null)
            return null;

          var CellList = ActiveCell.Row.GetCells().ToList();
          var CellIndex = CellList.IndexOf(ActiveCell) + Direction;
          return CellList.ExistsAt(CellIndex) ? CellList[CellIndex] : null;
        }

        switch (Keystroke.Key)
        {
          case Inv.Key.Space:
            ActiveCell?.FocusSheet();
            break;

          case Inv.Key.Left:
            ShiftHorizontal(-1)?.SetActive(true);
            break;

          case Inv.Key.Right:
            ShiftHorizontal(+1)?.SetActive(true);
            break;

          case Inv.Key.Up:
            ShiftVertical(-1)?.SetActive(true);
            break;

          case Inv.Key.Down:
            ShiftVertical(+1)?.SetActive(true);
            break;

          default:
            if (Keystroke.Modifier.IsAlt)
            {
              if (Keystroke.Key == Inv.Key.Grave)
              {
                //ActiveCell?.FocusSwitch();
              }
              else if (Keystroke.Key.IsLetter())
              {
                var CellArray = RowList.SelectMany(R => R.GetCells()).Where(C => C.Key != null).ToArray();

                var Cell = CellArray.FirstOrDefault(C => C.Key == Keystroke.Key);
                if (Cell != null)
                {
                  Cell.SetActive(true);
                  Cell.FocusSheet();
                }
              }
            }
            break;
        }
      };
    }

    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Margin Margin => Base.Margin;
    public MeshCell FocusCell { get; set; }

    public MeshRow AddRow()
    {
      var Result = new MeshRow(this);
      RowStack.AddPanel(Result);
      RowList.Add(Result);
      return Result;
    }
    public void AddHeader(Inv.Panel Panel)
    {
      ClientDock.AddHeader(Panel);
    }
    public void AddFooter(Inv.Panel Panel)
    {
      FooterStack.AddPanel(Panel);
    }
    public MeshAction AddLeftAction()
    {
      var Result = new MeshAction();
      Result.Codepoint.Caller();
      ActionDock.AddHeader(Result);
      ActionDock.Visibility.Show();
      return Result;
    }
    public MeshAction AddRightAction()
    {
      var Result = new MeshAction();
      Result.Codepoint.Caller();
      ActionDock.AddFooter(Result);
      ActionDock.Visibility.Show();
      return Result;
    }
    public MeshCheck AddCheck(params MeshCellContract[] DependencyCells)
    {
      var Check = new MeshCheck(DependencyCells);
      CheckList.Add(Check);
      return Check;
    }
    public void RemoveCheck(MeshCheck Check)
    {
      Check.Remove();
      CheckList.Remove(Check);
    }
    public bool Validate()
    {
      var ItemList = new List<MeshCheckReportItem>();

      foreach (var Check in CheckList)
      {
        var Result = Check.Run();

        // TODO: Include warnings in the report and handle downstream.
        if (!string.IsNullOrWhiteSpace(Result.ErrorText)) 
          ItemList.Add(new MeshCheckReportItem(Check.ResponsibleCell, Result));
      }

      var Report = new MeshCheckReport(ItemList);

      if (Report.ItemArray.Any())
      {
        Report.ItemArray.FirstOrDefault().Cell?.Control.Activate();
        return false;
      }

      return true;
    }
    public void Focus()
    {
      if (FocusCell != null)
      {
        FocusCell.SetActive(true);
        return;
      }

      // Activate the first cell if no specific cell has been specified.
      RowList.SelectMany(R => R.GetCells()).FirstOrDefault()?.SetActive(true);
    }
    public void Activate()
    {
      KeyboardBinding.Capture();

      foreach (var Check in CheckList)
        Check.Update();
    }
    public void Deactivate()
    {
      KeyboardBinding.Release();
    }

    internal void Activate(MeshCell Cell)
    {
      if (TraceClass.IsActive)
        TraceClass.Enter(Cell.TraceInstance.Format());

      if (Cell == ActiveCell)
      {
        if (TraceClass.IsActive)
          TraceClass.Leave("already active");
        return;
      }

      if (ActiveCell != null)
        ActiveCell.SetActive(false);

      this.ActiveCell = Cell;

      if (ActiveCell != null)
      {
        var Sheet = ActiveCell.Sheet;
        Sheet.Show();

        if (TraceClass.IsActive)
          TraceClass.Write("completed show");

        if (Sheet.Content != null)
          SideSheet.Content = Sheet;
        else
          SideSheet.Content = null;
      }

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    internal bool IsActive(MeshCell Cell)
    {
      if (TraceClass.IsActive)
        TraceClass.Write($"{nameof(ActiveCell)}=({ActiveCell?.TraceInstance.Format()}), {nameof(Cell)}=({Cell?.TraceInstance.Format()})");

      return Cell == ActiveCell;
    }

    private MeshCell[] GetKeyCells() => RowList.SelectMany(R => R.GetCells()).Where(C => C.Key != null).ToArray();

    private readonly Inv.Dock Dock;
    private readonly Inv.Dock ClientDock;
    private readonly Inv.Stack RowStack;
    private readonly Inv.DistinctList<MeshRow> RowList;
    private readonly Inv.Stack FooterStack;
    private readonly Inv.Dock ActionDock;
    private readonly Inv.Material.SideSheet SideSheet;
    private readonly Inv.DistinctList<MeshCheck> CheckList;
    private readonly Inv.KeyboardBinding KeyboardBinding;
    private MeshCell ActiveCell;

    private static readonly Inv.TraceClass TraceClass = Inv.Trace.NewClass<Inv.Material.Mesh>(Inv.Trace.PlatformFeature);
  }

  public sealed class MeshCheck
  {
    internal MeshCheck(IEnumerable<MeshCellContract> DependencyCells)
    {
      this.ResponsibleCell = DependencyCells.FirstOrDefault();

      foreach (var DependencyCell in DependencyCells)
        DependencyCell.Control.ChangeEvent += () => Update();
    }

    public MeshCellContract ResponsibleCell { get; set; }
    public event Action<MeshCheckResult> RunEvent
    {
      add
      {
        RunAction += value;
        Update();
      }
      remove
      {
        RunAction -= value;
        Update();
      }
    }

    internal MeshCheckResult Run()
    {
      var Result = new MeshCheckResult();
      RunAction?.Invoke(Result);
      return Result;
    }
    internal void Update()
    {
      var Result = Run();

      Debug.Assert(ResponsibleCell != null, $"{nameof(ResponsibleCell)} has not been provided. Result cannot be visualised.");

      ResponsibleCell?.Control.UpdateCheck(this, Result);
    }
    internal void Remove()
    {
      if (ResponsibleCell == null)
        return;

      ResponsibleCell?.Control.UpdateCheck(this, null);
    }

    private Action<MeshCheckResult> RunAction;
  }

  public sealed class MeshCheckReport
  {
    internal MeshCheckReport(IEnumerable<MeshCheckReportItem> Items)
    {
      this.ItemArray = Items.ToArray();
    }

    public MeshCheckReportItem[] ItemArray { get; }
  }

  public sealed class MeshCheckReportItem
  {
    internal MeshCheckReportItem(MeshCellContract Cell, MeshCheckResult Result)
    {
      this.Cell = Cell;
      this.Result = Result;
    }

    public MeshCellContract Cell { get; }
    public MeshCheckResult Result { get; }
  }

  public sealed class MeshCheckResult
  {
    public void AsError(string Text)
    {
      this.ErrorText = Text;
      this.WarningText = null;
    }
    public void AsWarning(string Text)
    {
      this.ErrorText = null;
      this.WarningText = Text;
    }

    internal string ErrorText { get; private set; }
    internal string WarningText { get; private set; }

    internal void Clear()
    {
      this.ErrorText = null;
      this.WarningText = null;
    }
    internal void CopyFrom(MeshCheckResult OtherResult)
    {
      this.ErrorText = OtherResult.ErrorText;
      this.WarningText = OtherResult.WarningText;
    }
    internal bool IsEqualTo(MeshCheckResult CompareResult)
    {
      bool IsEqualText(string A, string B)
      {
        var HasText = !string.IsNullOrWhiteSpace(A);
        var HasCompareText = !string.IsNullOrWhiteSpace(B);

        if (!HasText && !HasCompareText)
          return true;

        if (HasText && HasCompareText && A.Equals(B, StringComparison.CurrentCultureIgnoreCase))
          return true;

        return false;
      }

      return IsEqualText(ErrorText, CompareResult.ErrorText) && IsEqualText(WarningText, CompareResult.WarningText);
    }
  }

  public sealed class MeshAction : Inv.Panel<Inv.Material.Button>
  {
    internal MeshAction()
    {
      this.Base = new Inv.Material.Button();
      Base.AsContained();
      Base.Codepoint.Caller();
    }

    public Inv.Codepoint Codepoint => Base.Codepoint;
    public string Caption
    {
      get => Base.Caption;
      set => Base.Caption = value;
    }
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action SingleTapEvent
    {
      add => Base.SingleTapEvent += value;
      remove => Base.SingleTapEvent -= value;
    }

    public void AsText() => Base.AsText();
    public void AsContained(Inv.Colour Colour = null) => Base.AsContained(Colour);
  }

  public sealed class MeshRow : Inv.Panel<Inv.Table>
  {
    internal MeshRow(Mesh Mesh)
    {
      this.Mesh = Mesh;
      this.Base = Inv.Table.New();
      this.Row = Base.AddAutoRow();
      this.CellList = new Inv.DistinctList<MeshCell>();
    }

    public Inv.Visibility Visibility => Base.Visibility;

    public MeshCell AddCell(int Star = 1)
    {
      var Result = AddCell(null, Star);
      Result.Codepoint.Caller();
      return Result;
    }
    public MeshCell AddCell(string Title, int Star = 1)
    {
      var Result = new MeshCell(Mesh, this, Star);
      Result.Codepoint.Caller();

      if (Title != null)
        Result.Title = Title;

      Base.GetCell(Base.AddStarColumn(Star), Row).Content = Result;

      CellList.Add(Result);

      return Result;
    }
    public void SetHeight(int Value) => Row.Fixed(Value);

    internal IEnumerable<MeshCell> GetCells() => CellList;

    private readonly Mesh Mesh;
    private readonly Inv.TableRow Row;
    private readonly Inv.DistinctList<MeshCell> CellList;
  }

  public interface MeshCellContract
  {
    Inv.Material.MeshCell Control { get; }
  }

  public sealed class MeshCell : Inv.Panel<Inv.Button>, MeshCellContract
  {
    internal MeshCell(Mesh Mesh, MeshRow Row, int Star)
    {
      this.Mesh = Mesh;
      this.Row = Row;
      this.Star = Star;
      this.CheckResultDictionary = new Dictionary<MeshCheck, MeshCheckResult>();
      this.TraceInstance = TraceClass.NewInstance();

      // Can be activated by default.
      this.CanActivate = true;

      this.Base = Inv.Button.NewStark();
      Base.IsFocusable = false;
      Base.Background.In(Inv.Theme.Light.White);
      Base.OverEvent += () =>
      {
        if (TraceInstance.IsActive)
          TraceInstance.Enter(CallerMemberName: nameof(Base.OverEvent));

        RefreshBackground();

        if (TraceInstance.IsActive)
          TraceInstance.Leave(CallerMemberName: nameof(Base.OverEvent));
      };
      Base.AwayEvent += () =>
      {
        if (TraceInstance.IsActive)
          TraceInstance.Enter(CallerMemberName: nameof(Base.AwayEvent));

        RefreshBackground();

        if (TraceInstance.IsActive)
          TraceInstance.Leave(CallerMemberName: nameof(Base.AwayEvent));
      };
      Base.Focus.GotEvent += () =>
      {
        if (TraceInstance.IsActive)
          TraceInstance.Enter(CallerMemberName: $"{nameof(Base.Focus)}.{nameof(Base.Focus.GotEvent)}");

        SetActive(true);

        if (TraceInstance.IsActive)
          TraceInstance.Leave(CallerMemberName: $"{nameof(Base.Focus)}.{nameof(Base.Focus.GotEvent)}");
      };
      Base.SingleTapEvent += () =>
      {
        if (TraceInstance.IsActive)
          TraceInstance.Enter(CallerMemberName: nameof(Base.SingleTapEvent));

        if (!IsVisible || !CanActivate)
        {
          if (TraceInstance.IsActive)
            TraceInstance.Leave(Text: $"cannot activate {nameof(IsVisible)}={IsVisible}, {nameof(CanActivate)}={CanActivate}");
          return;
        }

        var IsActive = Mesh.IsActive(this);

        if (TraceInstance.IsActive)
          TraceInstance.Write($"{nameof(IsActive)}={IsActive}");

        if (IsActive)
          FocusSheet();
        else
          SetActive(true);

        if (TraceInstance.IsActive)
          TraceInstance.Leave(CallerMemberName: nameof(Base.SingleTapEvent));
      };

      this.Overlay = Inv.Overlay.New();
      Base.Content = Overlay;

      this.Stack = Inv.Stack.NewVertical();
      Overlay.AddPanel(Stack);
      Stack.Alignment.TopStretch();

      this.TitleLabel = Inv.Label.New();
      Stack.AddPanel(TitleLabel);
      TitleLabel.Font.Medium().Custom(13).In(Inv.Theme.Light.DimGray);
      TitleLabel.Margin.Set(0, 0, 0, 6);
      TitleLabel.Visibility.Collapse();

      this.CheckGraphic = Inv.Graphic.New();
      Overlay.AddPanel(CheckGraphic);
      CheckGraphic.Size.Set(24);
      CheckGraphic.Alignment.TopRight();
      CheckGraphic.Visibility.Collapse();

      this.KeyLabel = Inv.Label.New();
      Overlay.AddPanel(KeyLabel);
      KeyLabel.Padding.Set(6, 3, 6, 3);
      KeyLabel.Background.In(Inv.Material.Theme.OnPrimaryColour.Opacity(0.80F));
      KeyLabel.Font.Custom(13).Medium().In(Inv.Material.Theme.PrimaryColour);
      KeyLabel.Alignment.BottomRight();
      KeyLabel.Visibility.Collapse();

      this.Sheet = new MeshSheet(Mesh, this);

      SetActive(false);
    }

    public string Title
    {
      get => TitleLabel.Text;
      set
      {
        TitleLabel.Text = value;
        TitleLabel.Visibility.Set(!string.IsNullOrWhiteSpace(value));

        Sheet.Title = value;

        TraceInstance.Name = value;
      }
    }
    public Inv.Codepoint Codepoint => Base.Codepoint;
    public Inv.Panel Content
    {
      get => ValuePanel;
      set
      {
        if (ValuePanel != value)
        {
          if (ValuePanel != null)
            Stack.RemovePanel(ValuePanel);

          if (value != null)
            Stack.AddPanel(value);

          this.ValuePanel = value;
        }
      }
    }
    public Inv.Key? Key { get; set; }
    public MeshSheet Sheet { get; }
    public bool CanActivate { get; set; }
    public bool IsVisible
    {
      get => Base.Content != null;
      set
      {
        if (IsVisible != value)
        {
          Base.Content = value ? Overlay : null;

          // If the active cell is being hidden, deactivate it from the mesh.
          if (!value && Mesh.IsActive(this))
            Mesh.Activate(null);

          SetActive(false);

          RefreshBackground();

          // Setting visibility determines whether the validation is relevant and thus any checks need to be re-run.
          Change();
        }
      }
    }
    public event Action ChangeEvent;

    /// <summary>
    /// Makes this cell the active cell in the mesh, highlighting it and displaying its side sheet.
    /// </summary>
    public void Activate()
    {
      SetActive(true);
    }
    /// <summary>
    /// Programmatically indicate that this cell's logical content has changed.
    /// <para>This can be used to manually inform the mesh that any relevant checks need to be run.</para>
    /// </summary>
    public void Change()
    {
      ChangeEvent?.Invoke();
    }
    public bool IsRequired() => RequiredCheck != null;
    /// <summary>
    /// Indicates that data entry into the cell is required.
    /// </summary>
    /// <param name="IsEmptyQuery">Calculates whether data entry into the cell has been completed.</param>
    public void Required(Func<bool> IsEmptyQuery)
    {
      // Remove the original requirement as IsEmptyQuery may be changing.
      Optional();

      if (IsEmptyQuery == null)
        return;

      this.RequiredCheck = Mesh.AddCheck(this);
      RequiredCheck.RunEvent += (Result) =>
      {
        if (!IsVisible)
          return;

        var IsEmpty = IsEmptyQuery();
        if (IsEmpty)
          Result.AsError($"{Title} is required");
      };
    }
    public bool IsOptional() => RequiredCheck == null;
    /// <summary>
    /// Indicates that data entry into the cell is optional.
    /// </summary>
    public void Optional()
    {
      if (RequiredCheck == null)
        return;

      Mesh.RemoveCheck(RequiredCheck);

      this.RequiredCheck = null;
    }
    public MeshLabelCell AsLabel(string Text = null) => new MeshLabelCell(this, Text);
    public MeshMemoCell AsMemo() => new MeshMemoCell(this);
    public MeshRadioListCell<T> AsRadioList<T>() => new MeshRadioListCell<T>(this);
    public MeshCheckListCell<T> AsCheckList<T>() => new MeshCheckListCell<T>(this);
    public MeshCheck AddCheck() => Mesh.AddCheck(this);

    internal MeshRow Row { get; }
    internal int Star { get; }
    internal Inv.TraceInstance TraceInstance { get; }
    internal static Inv.Image ErrorImage { get; }
    internal static Inv.Colour ErrorColour { get; }
    internal static Inv.Image WarningImage { get; }
    internal static Inv.Colour WarningColour { get; }

    internal void ShowKey(bool Value)
    {
      KeyLabel.Text = "Alt+" + Key?.ToString();
      KeyLabel.Visibility.Set(IsVisible && Value && Key != null);
    }
    internal void SetActive(bool Value)
    {
      if (TraceInstance.IsActive)
        TraceInstance.Enter(Text: Value.ToString());

      if (Value && IsVisible)
        Mesh.Activate(this);

      Base.Border.In(Value && IsVisible ? Inv.Material.Theme.PrimaryColour : Inv.Theme.Light.LightGray).Set(Value ? 4 : 1, Value ? 4 : 1, Value ? 3 : 0, Value ? 3 : 0);
      Base.Padding.Set(Value && IsVisible ? 5 : 8);

      if (TraceInstance.IsActive)
        TraceInstance.Leave();
    }
    internal void FocusSheet()
    {
      if (TraceInstance.IsActive)
        TraceInstance.Enter();

      Sheet?.Focus?.Set();

      if (TraceInstance.IsActive)
        TraceInstance.Leave();
    }
    internal void FocusSwitch()
    {
      if (Base.Focus.Has())
        FocusSheet();
    }
    internal void UpdateCheck(MeshCheck Check, MeshCheckResult Result)
    {
      var PreviousResult = CheckResultDictionary.GetValueOrDefault(Check);

      if (Result == null)
      {
        // The check is being removed and can be cleared.

        CheckResultDictionary.Remove(Check);
        RefreshChecks();
      }
      else if (PreviousResult == null || !PreviousResult.IsEqualTo(Result))
      {
        // The check has just been run, so update the dictionary and refresh the UI if required.

        CheckResultDictionary[Check] = Result;
        RefreshChecks();
      }
    }

    Inv.Material.MeshCell MeshCellContract.Control => this;

    private void RefreshBackground()
    {
      Base.Background.In(Base.IsOver && IsVisible && CanActivate ? Inv.Theme.Light.WhiteSmoke : Inv.Theme.Light.White);
    }
    private void RefreshChecks()
    {
      var ErrorTextList = CheckResultDictionary.Values.Select(R => R.ErrorText.EmptyOrWhitespaceAsNull()).ExceptNull().ToHashSetX().ToArray();
      var WarningTextList = CheckResultDictionary.Values.Select(R => R.WarningText.EmptyOrWhitespaceAsNull()).ExceptNull().ToHashSetX().ToArray();

      var HasCheckText = ErrorTextList.Any() || WarningTextList.Any();
      CheckGraphic.Image = HasCheckText ? ErrorTextList.Any() ? ErrorImage : WarningImage : null;
      CheckGraphic.Visibility.Set(HasCheckText);

      CheckBlock?.RemoveSpans();
      if (HasCheckText)
      {
        if (CheckBlock == null)
        {
          this.CheckBlock = Inv.Block.New();
          CheckGraphic.Tooltip.Content = CheckBlock;
          CheckBlock.Padding.Set(8);
          CheckBlock.Background.In(Inv.Theme.BackgroundColour);
          CheckBlock.Font.Medium().Custom(13).In(Inv.Theme.OnBackgroundColour);
        }

        void WriteCheck(string[] MessageArray, Inv.Colour Colour)
        {
          foreach (var Message in MessageArray)
          {
            if (CheckBlock.Spans.Count > 0)
              CheckBlock.AddBreak();

            CheckBlock.AddRun(Message).Font.In(Colour);
          }
        }

        WriteCheck(ErrorTextList, ErrorColour);
        WriteCheck(WarningTextList, WarningColour);
      }

      Sheet.UpdateChecks(ErrorTextList, WarningTextList);
    }

    private readonly Mesh Mesh;
    private readonly Inv.Overlay Overlay;
    private readonly Inv.Stack Stack;
    private readonly Inv.Label TitleLabel;
    private readonly Inv.Label KeyLabel;
    private Inv.Panel ValuePanel;
    private readonly Inv.Graphic CheckGraphic;
    private Inv.Block CheckBlock;
    private readonly Dictionary<MeshCheck, MeshCheckResult> CheckResultDictionary;
    private MeshCheck RequiredCheck;

    static MeshCell()
    {
      var Graphics = Inv.Application.Access().Graphics;
      ErrorColour = Inv.Colour.DarkRed.Lighten(0.10F);
      ErrorImage = Graphics.Tint(Inv.Material.Resources.Images.ErrorOutline, ErrorColour);
      WarningColour = Inv.Colour.Goldenrod;
      WarningImage = Graphics.Tint(Inv.Material.Resources.Images.Warning, WarningColour);
    }
    private static readonly Inv.TraceClass TraceClass = Inv.Trace.NewClass<MeshCell>(Inv.Trace.PlatformFeature);
  }

  public sealed class MeshLabelCell : MeshCellContract
  {
    internal MeshLabelCell(MeshCell Base, string Text = null)
    {
      this.Base = Base;

      this.Label = Inv.Label.New();
      Label.LineWrapping = false;
      Label.Font.Large().In(Inv.Theme.OnBackgroundColour);
      Base.Content = Label;

      this.Text = Text;
    }

    public string Text
    {
      get => Label.Text;
      set => Label.Text = value;
    }
    public bool LineWrapping
    {
      get => Label.LineWrapping;
      set => Label.LineWrapping = value;
    }
    public Inv.Font Font => Label.Font;

    Inv.Material.MeshCell MeshCellContract.Control => Base;

    private readonly MeshCell Base;
    private readonly Inv.Label Label;
  }

  public sealed class MeshMemoCell
  {
    internal MeshMemoCell(MeshCell Base)
    {
      this.Base = Base;

      var PropertySheet = Base.Sheet.AsPropertySheet();

      var MemoField = PropertySheet.AddGroup(null).Stack.AddMemoField();
      MemoField.ShowCaption = false;
      MemoField.ChangeEvent += () =>
      {
        Label.Text = MemoField.Content;

        Base.Change();
      };

      Base.Sheet.Focus = MemoField.GetFocus();
      Base.Sheet.ShowEvent += () =>
      {
        MemoField.CaptionText = Base.Title;
        MemoField.Content = Text;
      };

      this.Label = Base.AsLabel();
      Label.LineWrapping = true;
    }

    public string Text
    {
      get => Label.Text;
      set => Label.Text = value;
    }

    private readonly MeshCell Base;
    private readonly MeshLabelCell Label;
  }

  public sealed class MeshRadioListCell<T> : MeshCellContract
  {
    internal MeshRadioListCell(MeshCell Base)
    {
      this.Base = Base;

      this.Label = Base.AsLabel();

      Base.Sheet.ShowEvent += () =>
      {
        if (RadioFlow == null)
        {
          var Dock = Inv.Dock.NewVertical();
          Base.Sheet.Content = Dock;

          var SearchEdit = new MeshSearchEdit();
          Dock.AddHeader(SearchEdit);
          Base.Sheet.Focus = SearchEdit.Focus;
          SearchEdit.ChangeEvent += () =>
          {
            RadioFlow.Search.Filter(SearchEdit.Text);
          };
          
          this.RadioFlow = new Inv.Material.ListRadioFlow<T>();
          Dock.AddClient(RadioFlow);
          RadioFlow.EqualityComparer = EqualityComparer;
          RadioFlow.ComposeEvent += I =>
          {
            if (ComposeEventField != null)
            {
              ComposeEventField(I);
              return;
            }

            I.Title = I.Context.ToString();
          };
          RadioFlow.SelectionChangeEvent += () => this.Content = RadioFlow.SelectedContext;
        }

        SetNullTitle();

        var Values = GetQuery?.Invoke() ?? Array.Empty<T>();
        RadioFlow.SelectedContext = ContentField;
        if (OrderByComparison != null)
          RadioFlow.OrderBy(OrderByComparison);
        else
          RadioFlow.OrderByTitle();
        RadioFlow.Load(Values);
      };
    }

    public T Content
    {
      get => ContentField;
      set
      {
        this.ContentField = value;
        Refresh();
        Base.Change();
      }
    }
    public IEqualityComparer<T> EqualityComparer { get; set; }
    public event Func<IEnumerable<T>> GetQuery;
    /// <summary>
    /// Event to compose the select item for rendering.
    /// <para>Note: Since the internal RadioFlow is created on demand, this cannot be a pass through.</para>
    /// </summary>
    public event Action<ListSelectItem<T>> ComposeEvent
    {
      add
      {
        ComposeEventField += value;
        Refresh();
      }
      remove => ComposeEventField -= value;
    }

    public void Required()
    {
      Base.Required(() => this.ContentField == null);
      SetNullTitle();
    }
    public void Optional()
    {
      Base.Optional();
      SetNullTitle();
    }
    public void OrderBy(Comparison<T> OrderByComparison)
    {
      RadioFlow?.OrderBy(OrderByComparison);
      this.OrderByComparison = OrderByComparison;
    }

    Inv.Material.MeshCell MeshCellContract.Control => Base;

    private void Refresh()
    {
      if (ContentField == null)
      {
        Label.Text = string.Empty;
        return;
      }

      if (ComposeEventField == null)
      {
        Label.Text = ContentField.ToString();
        return;
      }

      var SelectItem = ListSelectItem<T>.NewInline(ContentField);
      ComposeEventField?.Invoke(SelectItem);
      Label.Text = SelectItem.Title;
    }
    private void SetNullTitle()
    {
      if (RadioFlow != null)
        RadioFlow.NullTitle = Base.IsRequired() ? null : "None";
    }

    private readonly MeshCell Base;
    private readonly MeshLabelCell Label;
    private T ContentField;
    private Inv.Material.ListRadioFlow<T> RadioFlow;
    private Action<ListSelectItem<T>> ComposeEventField;
    private Comparison<T> OrderByComparison;
  }

  public sealed class MeshCheckListCell<T> : MeshCellContract
  {
    internal MeshCheckListCell(MeshCell Base)
    {
      this.Base = Base;

      this.Label = Base.AsLabel();

      this.CheckListSheet = Base.Sheet.AsCheckList<T>();
      CheckListSheet.ChangeEvent += () =>
      {
        RefreshLabel();
        Base.Change();
      };
    }

    public IEnumerable<T> Content
    {
      get => CheckListSheet.Content;
      set => CheckListSheet.Content = value;
    }
    public IEqualityComparer<T> EqualityComparer
    {
      get => CheckListSheet.EqualityComparer;
      set => CheckListSheet.EqualityComparer = value;
    }
    public event Func<IEnumerable<T>> GetQuery
    {
      add => CheckListSheet.GetQuery += value;
      remove => CheckListSheet.GetQuery -= value;
    }
    public event Action<ListSelectItem<T>> ComposeEvent
    {
      add => CheckListSheet.ComposeEvent += value;
      remove => CheckListSheet.ComposeEvent -= value;
    }
    public event Action ChangeEvent
    {
      add => CheckListSheet.ChangeEvent += value;
      remove => CheckListSheet.ChangeEvent -= value;
    }

    public void Load() => CheckListSheet.Load();
    public bool IsRequired() => Base.IsRequired();
    public void Required() => Base.Required(() => !CheckListSheet.Content.Any());
    public bool IsOptional() => Base.IsOptional();
    public void Optional() => Base.Optional();
    public void OrderBy(Func<T, T, int> CompareQuery)
    {
      CheckListSheet.OrderBy(CompareQuery);
    }
    public void GroupBy(Func<T, string> KeyQuery)
    {
      CheckListSheet.GroupBy(KeyQuery);
    }
    public void GroupBy<G>(Func<T, G> KeyQuery, Action<ListHeader<T, G>> HeaderAction)
    {
      CheckListSheet.GroupBy(KeyQuery, HeaderAction);
    }

    Inv.Material.MeshCell MeshCellContract.Control => Base;

    private void RefreshLabel()
    {
      Label.Text = CheckListSheet.Content.Select(C => CheckListSheet.GetTitle(C)).OrderBy().AsSeparatedText(", ");
    }

    private readonly MeshCell Base;
    private readonly MeshLabelCell Label;
    private readonly MeshCheckListSheet<T> CheckListSheet;
  }

  public sealed class MeshSheet : Inv.Panel<Inv.Dock>
  {
    public MeshSheet(Mesh Mesh, MeshCell Cell)
    {
      this.Mesh = Mesh;
      this.Cell = Cell;

      this.Base = Inv.Dock.NewVertical();
      //Base.Background.In(Exp.Theme.Light.White);

      this.TitleLabel = new MeshSheetTitleLabel();
      Base.AddHeader(TitleLabel);

      this.CheckStack = Inv.Stack.NewVertical();
      Base.AddHeader(CheckStack);
      CheckStack.Margin.Set(0, 0, 0, 8);
    }

    public string Title
    {
      get => TitleLabel.Text;
      set => TitleLabel.Text = value;
    }
    public Inv.Focus Focus { get; set; }
    public Inv.Panel Content
    {
      get => Base.Clients.FirstOrDefault();
      set
      {
        Base.RemoveClients();
        if (value != null)
          Base.AddClient(value);
      }
    }
    public event Action ShowEvent;
    public event Action AdjustEvent
    {
      add => Base.AdjustEvent += value;
      remove => Base.AdjustEvent -= value;
    }

    public bool IsActive() => Mesh.IsActive(Cell);
    public Inv.Dimension GetDimension() => Base.GetDimension();
    public MeshCheckListSheet<T> AsCheckList<T>()
    {
      return new MeshCheckListSheet<T>(this);
    }
    public PropertySheet AsPropertySheet()
    {
      var Space = new Inv.Material.Space();
      this.Content = Space;

      var Result = Space.NewPropertySheet();
      Space.ShowPanel(Result);

      return Result;
    }

    internal void Show()
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      ShowEvent?.Invoke();

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    internal void UpdateChecks(string[] ErrorTextArray, string[] WarningTextArray)
    {
      CheckStack.RemovePanels();

      void AddCheck(string Text, Inv.Image Image)
      {
        var Dock = Inv.Dock.NewHorizontal();
        CheckStack.AddPanel(Dock);
        Dock.Padding.Set(8);

        var Graphic = Inv.Graphic.New();
        Dock.AddHeader(Graphic);
        Graphic.Image = Image;
        Graphic.Margin.Set(0, 0, 8, 0);
        Graphic.Size.Set(24);

        var Label = Inv.Label.New();
        Dock.AddClient(Label);
        Label.Text = Text;
        Label.Alignment.CenterStretch();
        Label.Justify.Left();
        Label.Font.Custom(13).In(Inv.Theme.OnBackgroundColour);
      }

      foreach (var ErrorText in ErrorTextArray)
        AddCheck(ErrorText, MeshCell.ErrorImage);

      foreach (var WarningText in WarningTextArray)
        AddCheck(WarningText, MeshCell.WarningImage);

      if (CheckStack.Panels.Count > 0)
        CheckStack.AddPanel(Divider.NewVertical());

      CheckStack.Visibility.Set(CheckStack.Panels.Count > 0);
      TitleLabel.Margin.Set(0, 0, 0, CheckStack.Visibility.Get() ? 0 : 8);
    }

    private readonly Mesh Mesh;
    private readonly MeshCell Cell;
    private readonly MeshSheetTitleLabel TitleLabel;
    private readonly Inv.Stack CheckStack;

    private static readonly Inv.TraceClass TraceClass = Inv.Trace.NewClass<MeshSheet>(Inv.Trace.PlatformFeature);
  }

  public sealed class MeshSheetTitleLabel : Inv.Panel<Inv.Stack>
  {
    public MeshSheetTitleLabel()
    {
      this.Base = Inv.Stack.NewVertical();

      this.Label = Inv.Label.New();
      Base.AddPanel(Label);
      Label.Margin.Set(12);
      Label.Font.Custom(22).Medium().In(Inv.Theme.OnBackgroundColour);

      Base.AddPanel(Divider.NewVertical());
    }

    public string Text
    {
      get => Label.Text;
      set => Label.Text = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Visibility Visibility => Base.Visibility;

    private readonly Inv.Label Label;
  }

  internal sealed class MeshSearchEdit : Inv.Panel<Inv.Dock>
  {
    public MeshSearchEdit()
    {
      this.Base = Inv.Dock.NewHorizontal();
      Base.Margin.Set(12, 0, 12, 4);
      Base.Border.In(Inv.Theme.Light.DarkGray).Set(0, 0, 0, 1);

      var Graphic = new Inv.Material.Icon();
      Base.AddHeader(Graphic);
      Graphic.Image = SearchImage;
      Graphic.Size.Set(20, 20);
      Graphic.Alignment.Center();
      Graphic.Margin.Set(4, 4, 8, 4);

      this.Edit = Inv.Edit.NewSearch();
      Base.AddClient(Edit);
      Edit.Font.In(Inv.Theme.OnBackgroundColour);
      Edit.Alignment.CenterStretch();
      Edit.ChangeEvent += () => ChangeEvent?.Invoke();
    }

    public string Text => Edit.Text;
    public Inv.Focus Focus => Edit.Focus;
    public event Action ChangeEvent;

    private readonly Inv.Edit Edit;

    static MeshSearchEdit()
    {
      SearchImage = Inv.Application.Access().Graphics.Tint(Inv.Material.Resources.Images.Search, Inv.Theme.OnBackgroundColour);
    }
    private static readonly Inv.Image SearchImage;
  }

  public sealed class MeshCheckListSheet<T>
  {
    internal MeshCheckListSheet(MeshSheet Base)
    {
      this.Base = Base;

      this.ContentField = new HashSet<T>();

      var Dock = Inv.Dock.NewVertical();
      Base.Content = Dock;

      var SearchEdit = new MeshSearchEdit();
      Dock.AddHeader(SearchEdit);
      Base.Focus = SearchEdit.Focus;
      SearchEdit.ChangeEvent += () =>
      {
        CheckFlow.Search.Filter(SearchEdit.Text);
      };

      this.CheckFlow = new Inv.Material.ListCheckFlow<T>();
      Dock.AddClient(CheckFlow);
      CheckFlow.OrderByTitle();
      CheckFlow.CheckChangeEvent += () => this.Content = CheckFlow.SelectedContexts.ToDistinctList();

      Base.ShowEvent += () =>
      {
        CheckFlow.EqualityComparer = EqualityComparer;

        Load();
      };
    }

    public IEnumerable<T> Content
    {
      get => ContentField;
      set
      {
        this.ContentField = value != null ? value.ToHashSetX(EqualityComparer) : new HashSet<T>(EqualityComparer);
        ChangeInvoke();
      }
    }
    public IEqualityComparer<T> EqualityComparer
    {
      get => ContentField.Comparer;
      set
      {
        this.ContentField = ContentField.ToHashSetX(value);
        CheckFlow.EqualityComparer = EqualityComparer;
        ChangeInvoke();
      }
    }
    public event Func<IEnumerable<T>> GetQuery;
    public event Action<ListSelectItem<T>> ComposeEvent
    {
      add
      {
        CheckFlow.ComposeEvent += value;
        ChangeInvoke();
      }
      remove => CheckFlow.ComposeEvent -= value;
    }
    public event Action ChangeEvent;

    public void Load()
    {
      var Values = GetQuery?.Invoke() ?? Array.Empty<T>();
      CheckFlow.SelectedContexts = ContentField;
      CheckFlow.Load(Values);
    }
    public void OrderBy(Func<T, T, int> CompareQuery)
    {
      CheckFlow.OrderBy(CompareQuery);
    }
    public void GroupBy(Func<T, string> KeyQuery)
    {
      CheckFlow.GroupBy(KeyQuery);
    }
    public void GroupBy<G>(Func<T, G> KeyQuery, Action<ListHeader<T, G>> HeaderAction)
    {
      CheckFlow.GroupBy(KeyQuery, HeaderAction);
    }

    internal string GetTitle(T Context) => CheckFlow.GetTitle(Context);

    private void ChangeInvoke() => ChangeEvent?.Invoke();

    private readonly MeshSheet Base;
    private HashSet<T> ContentField;
    private Inv.Material.ListCheckFlow<T> CheckFlow;
  }
}