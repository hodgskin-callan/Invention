﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inv.Support;

namespace Inv.Material
{
  public sealed class PropertySheet : Inv.Panel<Inv.Scroll>
  {
    public PropertySheet(Inv.Material.Space Space)
    {
      this.Space = Space;
      this.Validator = new PropertyValidator();

      this.HasSets = false;
      this.GroupList = new List<PropertyGroup>();

      this.Base = Inv.Scroll.NewVertical();

      this.Dock = Inv.Dock.NewVertical();
      Base.Content = Dock;

      this.Stack = Inv.Stack.NewVertical();
      Dock.AddClient(Stack);
      Stack.Background.Colour = Inv.Theme.BackgroundColour;
      Stack.Padding.Set(0, 12, 0, 0);
    }

    public Inv.Size Size => Base.Size;
    public Inv.Margin Margin => Dock.Margin;
    public PropertyValidator Validator { get; }

    public void AddHeader(Inv.Panel Panel)
    {
      Dock.AddHeader(Panel);
    }
    public PropertyBand AddBand(Inv.Image Image)
    {
      var Result = new PropertyBand();
      Result.SetIcon(Image);

      Stack.AddPanel(Result);

      return Result;
    }
    public PropertyGroup AddGroup(Inv.Image Image = null)
    {
      var Result = new PropertyGroup(Space, Validator);
      Result.ReserveSpace(HasSets);
      
      GroupList.Add(Result);

      if (Image != null)
        Result.SetIcon(Image);

      Stack.AddPanel(Result);

      return Result;
    }
    public PropertySet<T> AddSet<T>(Inv.Image Image, string Caption)
      where T : new()
    {
      var Result = new PropertySet<T>(this, Space, AddBand(Image), Caption);

      this.HasSets = true;

      foreach (var Group in GroupList)
        Group.ReserveSpace(true);

      return Result;
    }
    public PropertySwitchGroup AddSwitchGroup(string Caption)
    {
      var Result = new PropertySwitchGroup(Caption);

      Stack.AddPanel(Result);

      return Result;
    }
    public void AddDivider()
    {
      Stack.AddPanel(Divider.NewVertical());
    }

    private readonly Inv.Material.Space Space;
    private readonly Inv.Dock Dock;
    private readonly Inv.Stack Stack;
    private readonly List<PropertyGroup> GroupList;
    private bool HasSets;
  }

  /// <summary>
  /// A Band is a divided 'section' of a property sheet, adorned by an icon.
  /// </summary>
  public sealed class PropertyBand : Inv.Panel<Inv.Dock>
  {
    public PropertyBand()
    {
      this.Base = Inv.Dock.NewHorizontal();
      Base.Margin.Set(0, 0, 0, 4);

      this.Icon = new Inv.Material.Icon();
      Base.AddHeader(Icon);
      Icon.Margin.Set(16, 12, 16, 12);
      Icon.Alignment.TopLeft();
      Icon.Size.Set(24);
      Icon.Visibility.Collapse();

      this.ContentFrame = Inv.Frame.New();
      Base.AddClient(ContentFrame);
      ContentFrame.Margin.Set(16, 0, 0, 0);
    }

    public Inv.Panel Content
    {
      get => ContentFrame.Content;
      set => ContentFrame.Content = value;
    }
    public Inv.Visibility Visibility => Base.Visibility;

    public void SetIcon(Inv.Image Image)
    {
      Icon.Image = Image;
      Icon.Visibility.Set(Image != null);
    }

    private readonly Inv.Material.Icon Icon;
    private readonly Inv.Frame ContentFrame;
  }

  /// <summary>
  /// A Group is a specialised band, containing several logically-related property fields.
  /// </summary>
  public sealed class PropertyGroup : Inv.Panel<PropertyBand>
  {
    public PropertyGroup(Inv.Material.Space Space, PropertyValidator Validator)
    {
      this.Space = Space;

      this.Base = new PropertyBand();

      this.Stack = new Inv.Material.PropertyStack(Space, Validator);
      Base.Content = Stack;
    }

    public Inv.Visibility Visibility => Base.Visibility;
    public PropertyStack Stack { get; }

    public void SetIcon(Inv.Image Image)
    {
      Base.SetIcon(Image);
    }

    internal void ReserveSpace(bool Value)
    {
      Stack.Margin.Set(0, 0, Value ? 72 : 16, 0);
    }

    private readonly Inv.Material.Space Space;
  }

  /// <summary>
  /// A Stack is a vertically-arranged collection of property fields.
  /// </summary>
  public sealed class PropertyStack : Inv.Panel<Inv.Stack>
  {
    public PropertyStack(Inv.Material.Space Space, PropertyValidator Validator)
    {
      this.Space = Space;
      this.Validator = Validator;

      this.Span = new PropertySpan(Space, Validator);
      Span.AddPanelEvent += (Panel) =>
      {
        Panel.Control.Margin.Set(0, 0, 0, 8);
        Base.AddPanel(Panel);
      };
      Span.ChangeEvent += () => ChangeInvoke();

      this.Base = Inv.Stack.NewVertical();
    }

    public PropertyValidator Validator { get; }
    public PropertySpan Span { get; }
    public event Action ChangeEvent;

    internal Inv.Margin Margin => Base.Margin;

    /// <summary>
    /// Only one memo field can be used in a stack.
    /// </summary>
    /// <param name="Caption"></param>
    /// <returns></returns>
    public Inv.Material.MemoField AddMemoField(string Caption = null)
    {
      var Result = new Inv.Material.MemoField(Span.AddField());
      Result.Margin.Set(0, 0, 0, 8);
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.PropertyRow AddRow()
    {
      var Result = new Inv.Material.PropertyRow(Space, Validator);
      Base.AddPanel(Result);
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }

    internal void ChangeInvoke()
    {
      ChangeEvent?.Invoke();
    }

    private readonly Inv.Material.Space Space;
  }

  public sealed class PropertyRow : Inv.Panel<Inv.Dock>
  {
    internal PropertyRow(Inv.Material.Space Space, PropertyValidator Validator)
    {
      this.Span = new PropertySpan(Space, Validator);
      Span.AddPanelEvent += (Panel) =>
      {
        if (Base.PanelCount > 0)
          Panel.Control.Margin.Set(8, 0, 0, 0);

        Base.AddClient(Panel);
      };

      this.Base = Inv.Dock.NewHorizontal();
      Base.Margin.Set(0, 0, 0, 8);
    }

    public PropertySpan Span { get; }

    internal event Action ChangeEvent
    {
      add { Span.ChangeEvent += value; }
      remove { Span.ChangeEvent -= value; }
    }
  }

  public sealed class PropertySpan
  {
    internal PropertySpan(Inv.Material.Space Space, PropertyValidator Validator)
    {
      this.Space = Space;
      this.Validator = Validator;
    }

    public void AddPanel(Inv.Panel Panel)
    {
      AddPanelEvent?.Invoke(Panel);
    }
    public Inv.Material.Integer32Field AddInteger32Field(string Caption)
    {
      var Result = new Inv.Material.Integer32Field(AddField());
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.DecimalField AddDecimalField(string Caption)
    {
      var Result = new Inv.Material.DecimalField(AddField());
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.MoneyField AddMoneyField(string Caption)
    {
      var Result = new Inv.Material.MoneyField(AddField());
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.EditField AddEditField(string Caption, Inv.EditInput EditInput)
    {
      var Result = new Inv.Material.EditField(AddField(), EditInput);
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.EditField AddEmailField(string Caption)
    {
      return AddEditField(Caption, EditInput.Email);
    }
    public Inv.Material.EditField AddNameField(string Caption)
    {
      return AddEditField(Caption, EditInput.Name);
    }
    public Inv.Material.EditField AddPasswordField(string Caption)
    {
      return AddEditField(Caption, EditInput.Password);
    }
    public Inv.Material.EditField AddPhoneField(string Caption)
    {
      return AddEditField(Caption, EditInput.Phone);
    }
    public Inv.Material.EditField AddTextField(string Caption)
    {
      return AddEditField(Caption, EditInput.Text);
    }
    public Inv.Material.EditField AddUsernameField(string Caption)
    {
      return AddEditField(Caption, EditInput.Username);
    }
    public Inv.Material.RadioSelectField<T> AddRadioSelectField<T>(string Caption)
    {
      var Result = new Inv.Material.RadioSelectField<T>(AddField(), Space);
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.CheckSelectField<T> AddCheckSelectField<T>(string Caption)
    {
      var Result = new Inv.Material.CheckSelectField<T>(AddField(), Space);
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.DateOfBirthField AddDateOfBirthField(string Caption)
    {
      var Result = new Inv.Material.DateOfBirthField(AddField());
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.DateField AddDateField(string Caption)
    {
      var Result = new Inv.Material.DateField(AddField());
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.TimeField AddTimeField(string Caption)
    {
      var Result = new Inv.Material.TimeField(AddField());
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }

    internal event Action ChangeEvent;
    internal event Action<Inv.Panel> AddPanelEvent;

    internal PropertyField AddField()
    {
      var Result = new PropertyField(Validator);

      AddPanelEvent?.Invoke(Result);

      return Result;
    }

    private void ChangeInvoke()
    {
      ChangeEvent?.Invoke();
    }

    private readonly Inv.Material.Space Space;
    private readonly PropertyValidator Validator;
  }

  /// <summary>
  /// A Set represents a collection of Items, supporting addition and removal of items with the collection.
  /// </summary>
  internal sealed class PropertySet
  {
    internal PropertySet(PropertySheet Sheet, Inv.Material.Space Space, Inv.Material.PropertyBand Band, string Caption)
    {
      this.Sheet = Sheet;
      this.Space = Space;
      this.Band = Band;

      var Dock = Inv.Dock.NewVertical();
      Band.Content = Dock;

      this.Stack = Inv.Stack.NewVertical();
      Dock.AddClient(Stack);

      this.NewButton = Inv.Button.NewFlat();
      Dock.AddFooter(NewButton);
      NewButton.Background.Colour = Inv.Theme.BackgroundColour;
      NewButton.Corner.Set(4);
      NewButton.Padding.Set(12);
      NewButton.Margin.Set(0, 8, 0, 8);
      NewButton.Alignment.StretchLeft();
      NewButton.SingleTapEvent += () =>
      {
        AddItem();

        RefreshItems();
      };

      var NewLabel = Inv.Label.New();
      NewButton.Content = NewLabel;
      NewLabel.Text = "ADD " + Caption.ToUpper();
      NewLabel.Font.Custom(12).Medium().In(Inv.Theme.SubtleColour);

      this.AllowRemoveLastField = true;
      this.ItemList = new Inv.DistinctList<PropertyItem>();
    }

    public bool AllowRemoveLast
    {
      get => AllowRemoveLastField;
      set
      {
        this.AllowRemoveLastField = value;

        RefreshItems();
      }
    }
    public event Func<object> NewQuery;
    public event Action<PropertyItem> BuildItemEvent;

    public void SetIcon(Inv.Image Image = null)
    {
      Band.SetIcon(Image);
    }
    public Inv.DistinctList<object> Get()
    {
      foreach (var Item in ItemList)
        Item.SaveInvoke();

      return ItemList.Select(I => I.Context).ToDistinctList();
    }
    public void Set(Inv.DistinctList<object> ContextList)
    {
      // TODO: Recycle items rather than completely flushing.
      RemoveItems(ItemList.ToArray());

      foreach (var Context in ContextList)
      {
        var EditItem = AddItem();
        EditItem.Context = Context;
        EditItem.LoadInvoke();
      }

      RefreshItems();
    }
    public PropertyItem AddItem()
    {
      var Result = new PropertyItem(Sheet, Space);
      Result.Index = ItemList.Count;
      Result.Context = NewQuery();
      Result.ClearEvent += () => RemoveItems(Result);

      BuildItemEvent?.Invoke(Result);

      ItemList.Add(Result);

      AddItemEvent?.Invoke(Result);

      Result.ShuffleInvoke();

      Stack.AddPanel(Result);

      if (Result.Primary != null)
        Result.Primary.Activate();

      return Result;
    }
    public void RemoveItems(params PropertyItem[] ItemArray)
    {
      foreach (var Item in ItemArray)
      {
        ItemList.Remove(Item);

        Sheet.Validator.RemoveValidator(Item.Stack.Validator);

        RemoveItemEvent?.Invoke(Item);

        Stack.RemovePanel(Item);
      }

      var Sequence = 0;
      foreach (var Item in ItemList)
      {
        Item.Index = Sequence++;
        Item.ShuffleInvoke();
      }

      RefreshItems();
    }

    internal event Action<PropertyItem> AddItemEvent;
    internal event Action<PropertyItem> RemoveItemEvent;

    private void RefreshItems()
    {
      if (!ItemList.Any())
        return;

      foreach (var Item in ItemList.Take(ItemList.Count - 1))
      {
        Item.IsSeparator = true;
        Item.ClearButtonVisibility.Show();
      }

      var LastItem = ItemList.LastOrDefault();
      LastItem.IsSeparator = false;
      LastItem.ClearButtonVisibility.Show();

      if (ItemList.Count == 1 && !AllowRemoveLastField)
        ItemList.FirstOrDefault().ClearButtonVisibility.Collapse();
    }

    private readonly PropertySheet Sheet;
    private readonly Inv.Material.Space Space;
    private readonly Inv.Material.PropertyBand Band;
    private readonly Inv.Stack Stack;
    private readonly Inv.DistinctList<PropertyItem> ItemList;
    private readonly Inv.Button NewButton;
    private bool AllowRemoveLastField;
  }

  /// <summary>
  /// An Item provides the user interface for entering several property fields related to one C# object.
  /// </summary>
  public sealed class PropertyItem : Inv.Panel<Inv.Dock>
  {
    internal PropertyItem(PropertySheet Sheet, Inv.Material.Space Space)
    {
      this.Base = Inv.Dock.NewHorizontal();

      var StackDock = Inv.Dock.NewVertical();
      Base.AddClient(StackDock);

      this.Stack = new Inv.Material.PropertyStack(Space, Sheet.Validator.AddValidator());
      StackDock.AddClient(Stack);

      this.Divider = Divider.NewVertical();
      StackDock.AddFooter(Divider);
      Divider.Margin.Set(0, 0, 0, 8);
      Divider.Visibility.Collapse();

      this.Frame = Inv.Frame.New();
      Base.AddFooter(Frame);
      Frame.Margin.Set(24, 0, 24, 32);
      Frame.Size.Set(24);

      this.ClearButton = new Inv.Material.IconButton();
      Frame.Content = ClearButton;
      ClearButton.Alignment.Center();
      ClearButton.Normal();
      ClearButton.Image = Inv.Material.Resources.Images.Clear;
      ClearButton.SingleTapEvent += () =>
      {
        if (RemoveQuery != null)
        {
          var Query = new PropertyItemQuery(Context);
          Query.AllowEvent += () => ClearInvoke();

          RemoveQuery(Query);
        }
        else
        {
          ClearInvoke();
        }
      };
    }

    public int Index { get; internal set; }
    public bool IsNew { get; private set; }
    public PropertyField Primary { get; set; }
    public PropertyStack Stack { get; }
    public event Action<object> LoadEvent;
    public event Action<object> SaveEvent;
    public event Action<PropertyItemQuery> RemoveQuery;
    public event Action ShuffleEvent;

    internal object Context { get; set; }
    internal bool IsSeparator
    {
      set => Divider.Visibility.Set(value);
    }
    internal Inv.Material.PropertyValidator Validator { get; }
    internal Inv.Visibility ClearButtonVisibility => ClearButton.Visibility;
    internal event Action ClearEvent;

    internal void NewInvoke()
    {
      this.IsNew = true;

      LoadEvent?.Invoke(Context);
    }
    internal void LoadInvoke()
    {
      this.IsNew = false;

      LoadEvent?.Invoke(Context);
    }
    internal void SaveInvoke()
    {
      SaveEvent?.Invoke(Context);
    }
    internal void ClearInvoke()
    {
      ClearEvent?.Invoke();
    }
    internal void ShuffleInvoke()
    {
      ShuffleEvent?.Invoke();
    }

    private readonly Inv.Frame Frame;
    private readonly Inv.Material.IconButton ClearButton;
    private readonly Inv.Material.Divider Divider;
  }

  public sealed class PropertyItemQuery
  {
    internal PropertyItemQuery(object Context)
    {
      this.Context = Context;
    }

    public object Context { get; }

    public void Allow()
    {
      Debug.Assert(!HasAllowed, "Query has already been allowed.");
      Debug.Assert(AllowEvent != null, "Query has not been correctly initialised.");

      if (HasAllowed)
        return;

      this.HasAllowed = true;
      AllowEvent();
    }

    internal event Action AllowEvent;

    internal PropertyItemQuery<T> Wrap<T>() => new PropertyItemQuery<T>(this);

    private bool HasAllowed;
  }

  public sealed class PropertySet<T>
    where T : new()
  {
    internal PropertySet(PropertySet Base)
    {
      this.Base = Base;
      Base.NewQuery += () => new T();
      Base.BuildItemEvent += (Item) => BuildItemEvent?.Invoke(new PropertyItem<T>(Item));
    }
    internal PropertySet(PropertySheet Sheet, Inv.Material.Space Space, Inv.Material.PropertyBand Band, string Caption)
      : this(new PropertySet(Sheet, Space, Band, Caption))
    {
    }

    public event Action<PropertyItem<T>> BuildItemEvent;

    public void SetIcon(Inv.Image Image = null)
    {
      Base.SetIcon(Image);
    }
    public Inv.DistinctList<T> Get()
    {
      return Base.Get().Convert<T>().ToDistinctList();
    }
    public void Set(IEnumerable<T> Contexts)
    {
      Base.Set(Contexts.Convert<object>().ToDistinctList());
    }
    public PropertyItem<T> AddItem()
    {
      return new PropertyItem<T>(Base.AddItem());
    }

    private readonly PropertySet Base;
  }

  public sealed class PropertyItem<T> : Inv.Panel<PropertyItem>
    where T : new()
  {
    internal PropertyItem(PropertyItem Base)
    {
      this.Base = Base;
      Base.LoadEvent += O => LoadEvent?.Invoke((T)O);
      Base.SaveEvent += O => SaveEvent?.Invoke((T)O);
      Base.RemoveQuery += Q => RemoveQuery?.Invoke(Q.Wrap<T>());
    }

    public int Index => Base.Index;
    public bool IsNew => Base.IsNew;
    public PropertyField Primary
    {
      get => Base.Primary;
      set => Base.Primary = value;
    }
    public PropertyStack Stack => Base.Stack;
    public event Action<T> LoadEvent;
    public event Action<T> SaveEvent;
    public event Action ShuffleEvent
    {
      add => Base.ShuffleEvent += value;
      remove => Base.ShuffleEvent -= value;
    }
    public event Action<PropertyItemQuery<T>> RemoveQuery;
  }

  public sealed class PropertyItemQuery<T>
  {
    internal PropertyItemQuery(PropertyItemQuery Base)
    {
      this.Base = Base;
    }

    public T Context => (T)Base.Context;

    public void Allow() => Base.Allow();

    private readonly PropertyItemQuery Base;
  }

  public sealed class PropertySwitchGroup : Inv.Panel<Inv.Stack>
  {
    internal PropertySwitchGroup(string Caption)
    {
      this.Base = Inv.Stack.NewVertical();

      var Subheader = new Inv.Material.ListSubheader();
      Base.AddPanel(Subheader);
      Subheader.Text = Caption;
      Subheader.Visibility.Set(!string.IsNullOrWhiteSpace(Caption));
    }

    public Inv.Visibility Visibility => Base.Visibility;

    public Inv.Material.Switch Add(string Caption)
    {
      var Dock = Inv.Dock.NewHorizontal();
      Base.AddPanel(Dock);
      Dock.Padding.Set(16, 12, 16, 16);

      var Label = Inv.Label.New();
      Dock.AddClient(Label);
      Label.Text = Caption;
      Label.Font.Custom(16).In(Inv.Theme.OnBackgroundColour);

      var Switch = new Inv.Material.Switch();
      Dock.AddFooter(Switch);
      Switch.Alignment.CenterRight();

      return Switch;
    }
  }

  public sealed class PropertyValidator
  {
    public PropertyValidator()
    {
      this.IsEnabled = true;
      this.RuleList = new Inv.DistinctList<PropertyRule>();
      this.ValidatorList = new Inv.DistinctList<PropertyValidator>();
    }

    public bool IsEnabled { get; set; }

    public PropertyRuleReport Run()
    {
      var Result = (PropertyRuleReport)null;

      foreach (var Rule in RuleList)
      {
        Rule.Check();

        if (Result == null && Rule.Report.IsFailure)
          Result = Rule.Report;
      }

      foreach (var Validator in ValidatorList.Where(V => V.IsEnabled))
      {
        var ValidatorReport = Validator.Run();

        if (Result == null && ValidatorReport != null && ValidatorReport.IsFailure)
          Result = ValidatorReport;
      }

      return Result;
    }
    public PropertyRule AddRule(PropertyField TargetField, params PropertyField[] FieldArray)
    {
      var Result = new PropertyRule(TargetField, FieldArray);

      RuleList.Add(Result);

      return Result;
    }
    public PropertyValidator AddValidator()
    {
      var Result = new PropertyValidator();

      ValidatorList.Add(Result);

      return Result;
    }
    public void RemoveValidator(PropertyValidator Validator)
    {
      ValidatorList.Remove(Validator);
    }

    private readonly Inv.DistinctList<PropertyRule> RuleList;
    private readonly Inv.DistinctList<PropertyValidator> ValidatorList;
  }

  public sealed class PropertyRule
  {
    internal PropertyRule(PropertyField TargetField, params PropertyField[] FieldArray)
    {
      this.IsEnabledField = true;

      this.Report = new PropertyRuleReport();
      Report.DefaultField = TargetField;
      TargetField.AddInherentReport(Report);

      Observe(TargetField);
      foreach (var Field in FieldArray)
        Observe(Field);
    }

    public bool IsEnabled
    {
      get => IsEnabledField;
      set
      {
        if (IsEnabledField != value)
        {
          IsEnabledField = value;

          Check();
        }
      }
    }
    public PropertyRuleReport Report { get; }
    // Only a Func for syntax convenience.
    public event Func<PropertyRuleReport, bool> CheckEvent;

    internal void Check()
    {
      if (!IsEnabled)
        return;

      CheckEvent.Invoke(Report);
    }
    internal void Observe(PropertyField Field)
    {
      Field.ChangeEvent += () => Check();
    }

    private bool IsEnabledField;
  }

  public sealed class PropertyRuleReport
  {
    public PropertyRuleReport()
    {
    }

    public bool IsFailure { get; private set; }
    public string Message { get; private set; }
    public Inv.Material.PropertyField ActivateField { get; private set; }
    public Inv.Focus Focus { get; private set; }

    public bool Pass()
    {
      this.IsFailure = false;
      this.Message = null;
      this.ActivateField = null;
      this.Focus = null;

      // Clear the last message.
      if (DefaultField != null)
      {
        DefaultField.RemoveTransientReports();
        DefaultField.ReportRefresh();
      }

      return true;
    }
    public bool Fail(string Message, Inv.Material.PropertyField PropertyField = null, Inv.Focus SetFocus = null)
    {
      this.IsFailure = true;
      this.Message = Message;
      this.ActivateField = PropertyField ?? DefaultField;
      this.Focus = SetFocus ?? PropertyField?.Focus ?? DefaultField.Focus;

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Focus != null, "Could not determine appropriate Focus");

      if (DefaultField == null && PropertyField != null)
      {
        PropertyField.RemoveTransientReports();
        PropertyField.AddTransientReport(this);
      }

      var RefreshField = DefaultField ?? PropertyField;
      if (RefreshField != null)
        RefreshField.ReportRefresh();

      return false;
    }
    public bool Fail(string Message, Inv.Focus SetFocus)
    {
      return Fail(Message, null, SetFocus);
    }
    public void Highlight()
    {
      if (ActivateField != null)
        ActivateField.Activate();

      if (Focus != null)
        Focus.Set();
    }

    internal PropertyField DefaultField { get; set; }
  }

  public sealed class PropertyField : Inv.Panel<Inv.Stack>
  {
    internal PropertyField(PropertyValidator Validator)
    {
      this.Validator = Validator;
      this.InherentReportList = new Inv.DistinctList<PropertyRuleReport>();
      this.TransientReportList = new Inv.DistinctList<PropertyRuleReport>();

      this.IsEmpty = true;
      this.IsFocused = false;
      this.CanActivate = true;

      this.Base = Inv.Stack.NewVertical();

      this.Button = Inv.Button.NewStark();
      Base.AddPanel(Button);
      Button.Size.SetMinimumHeight(56);
      Button.Corner.Set(4, 4, 0, 0);
      Button.Background.Colour = Inv.Theme.SmokeColour;
      Button.Border.Set(0, 0, 0, 1).Colour = Inv.Theme.SoftColour;
      Button.Padding.Set(12, 0, 0, 0);
      Button.OverEvent += () => Button.Border.Colour = Inv.Material.Theme.PrimaryColour;
      Button.AwayEvent += () => Button.Border.Colour = Button.Focus.Has() ? Inv.Material.Theme.PrimaryColour : Inv.Theme.SoftColour;
      Button.PressEvent += () => Button.Background.Colour = Inv.Theme.EdgeColour;
      Button.ReleaseEvent += () =>
      {
        Button.Background.Colour = Inv.Theme.SmokeColour;
        Button.Border.Colour = Button.IsOver || Button.Focus.Has() ? Inv.Material.Theme.PrimaryColour : Inv.Theme.SoftColour;
      };
      Button.SingleTapEvent += ActivateHandler;
      Button.Focus.LostEvent += () =>
      {
        Button.Background.Colour = Inv.Theme.SmokeColour;
        Button.Border.Colour = Button.IsOver ? Inv.Material.Theme.PrimaryColour : Inv.Theme.SoftColour;
      };

      var ContentOverlay = Inv.Overlay.New();
      Button.Content = ContentOverlay;

      this.GhostEdit = Inv.Edit.NewText();
      ContentOverlay.AddPanel(GhostEdit);
      GhostEdit.Size.Set(1);
      GhostEdit.Focus.GotEvent += () => Activate();

      this.GhostLabel = Inv.Label.New();
      ContentOverlay.AddPanel(GhostLabel);
      GhostLabel.Alignment.CenterStretch();
      GhostLabel.Margin.Set(0, 0, 0, 1);
      GhostLabel.Font.Custom(16).In(Inv.Theme.SubtleColour.Lighten(0.05F));

      this.ClientDock = Inv.Dock.NewVertical();
      ContentOverlay.AddPanel(ClientDock);
      ClientDock.Margin.Set(0, 0, 0, 1);
      ClientDock.Visibility.Collapse();

      this.CaptionLabel = Inv.Label.New();
      ClientDock.AddHeader(CaptionLabel);
      CaptionLabel.Margin.Set(0, 8, 0, 0);
      CaptionLabel.Font.Custom(12).In(Inv.Theme.OnBackgroundColour);

      var ContentDock = Inv.Dock.NewHorizontal();
      ClientDock.AddClient(ContentDock);
      ContentDock.Padding.Set(0, 0, 8, 0);

      this.ContentFrame = Inv.Frame.New();
      ContentDock.AddClient(ContentFrame);
      ContentFrame.Padding.Set(0, 4, 0, 4);
      ContentFrame.Border.Set(0, 0, 0, 2);

      this.ClearButton = new Inv.Material.IconButton();
      ContentDock.AddFooter(ClearButton);
      ClearButton.Margin.Set(8, 0, 0, 4);
      ClearButton.Small();
      ClearButton.Image = Inv.Material.Resources.Images.Clear;
      ClearButton.Alignment.Center();
      ClearButton.Visibility.Collapse();
      ClearButton.SingleTapEvent += () =>
      {
        ClearAction?.Invoke();

        Refresh();
      };

      this.ErrorIcon = new Inv.Material.Icon();
      ContentDock.AddFooter(ErrorIcon);
      ErrorIcon.Size.Set(24);
      ErrorIcon.Colour = Inv.Colour.Red;
      ErrorIcon.Image = Inv.Material.Resources.Images.Error;
      ErrorIcon.Margin.Set(8, 0, 0, 4);
      ErrorIcon.Alignment.Center();

      this.HelperLabel = Inv.Label.New();
      Base.AddPanel(HelperLabel);
      HelperLabel.Margin.Set(12, 6, 0, 0);
      HelperLabel.Size.SetHeight(16);
      HelperLabel.Alignment.BottomLeft();
      HelperLabel.Font.Custom(11).In(Inv.Theme.OnBackgroundColour);

      this.MandatoryRule = Validator.AddRule(this);
      MandatoryRule.CheckEvent += (Report) =>
      {
        // Don't enforce entering mandatory fields if the field is disabled or read only.
        if (!IsReadOnlyField && IsEnabled && IsMandatoryField && IsEmpty)
          return Report.Fail(CaptionText + " " + Inv.Support.GrammarHelper.Is(IsMandatoryPlural ? 2 : 1) + " required");

        return Report.Pass();
      };

      Refresh();
    }

    public bool IsMandatory
    {
      get => IsMandatoryField;
      set
      {
        this.IsMandatoryField = value;
        Refresh();
      }
    }
    /// <summary>
    /// Determines whether the activating button itself is enabled or not.
    /// </summary>
    public bool IsEnabled
    {
      get => Button.IsEnabled;
      set => Button.IsEnabled = value;
    }
    public bool IsReadOnly
    {
      get => IsReadOnlyField;
      set
      {
        if (value != IsReadOnlyField)
        {
          this.IsReadOnlyField = value;
          ReadOnlyChangeInvoke();
          Refresh();
        }
      }
    }
    public bool ShowCaption
    {
      get => CaptionLabel.Visibility.Get();
      set
      {
        CaptionLabel.Visibility.Set(value);
        ClientDock.Padding.Set(0, value ? 0 : 8, 0, 0);
      }
    }
    public string CaptionText
    {
      get => CaptionLabel.Text;
      set
      {
        CaptionLabel.Text = value;
        GhostLabel.Text = value;
      }
    }
    public string ErrorText
    {
      get => ErrorTextField;
      set
      {
        this.ErrorTextField = value;

        Refresh();
      }
    }
    public string HelperText
    {
      get => HelperTextField;
      set
      {
        this.HelperTextField = value;
        Refresh();
      }
    }
    public Inv.Panel Content
    {
      set { ContentFrame.Content = value; }
    }
    public Inv.Focus Focus { get; set; }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public PropertyValidator Validator { get; }
    public event Action ChangeEvent;

    public void Activate(Inv.Focus OverrideFocus = null)
    {
      if (!CanActivate)
        return;

      if (!IsActivated)
      {
        this.IsActivated = true;

        RefreshActivated();

        var Focus = OverrideFocus ?? this.Focus;
        if (Focus != null)
          Focus.Set();

        Button.SingleTapEvent -= ActivateHandler;
      }
    }
    public void AsButton(Action SingleTapAction, Action ClearAction = null)
    {
      this.CanActivate = false;
      this.Focus = Button.Focus;
      this.SingleTapAction = SingleTapAction;
      this.ClearAction = ClearAction;
      Button.IsFocusable = true;
    }

    internal bool IsEmpty { get; private set; }
    internal bool IsMandatoryPlural { get; set; }
    internal event Action ReadOnlyChangeEvent;

    internal void Deactivate()
    {
      if (IsActivated)
      {
        this.IsActivated = false;

        RefreshActivated();

        Button.SingleTapEvent += ActivateHandler;
      }
    }
    internal void GotFocus()
    {
      if (!IsFocused)
      {
        this.IsFocused = true;

        Refresh();
      }
    }
    internal void LostFocus()
    {
      if (IsFocused)
      {
        this.IsFocused = false;

        Deactivate();

        Refresh();
      }
    }
    internal void ReadOnlyChangeInvoke()
    {
      ReadOnlyChangeEvent?.Invoke();
    }
    internal void SetEmpty(bool IsEmpty)
    {
      this.IsEmpty = IsEmpty;

      Refresh();
    }
    internal void ChangeInvoke()
    {
      ChangeEvent?.Invoke();
    }
    internal PropertyRule AddRule()
    {
      return Validator.AddRule(this);
    }
    internal void AddInherentReport(PropertyRuleReport Report)
    {
      InherentReportList.Add(Report);
    }
    internal void AddTransientReport(PropertyRuleReport Report)
    {
      TransientReportList.Add(Report);
    }
    internal void RemoveTransientReports()
    {
      TransientReportList.Clear();
    }
    internal void ReportSet(string Message)
    {
      ErrorText = Message;
    }
    internal void ReportRefresh()
    {
      foreach (var Report in InherentReportList.Union(TransientReportList))
      {
        if (!string.IsNullOrWhiteSpace(Report.Message))
        {
          ReportSet(Report.Message);
          return;
        }
      }

      ReportSet(null);
    }

    private void Refresh()
    {
      GhostLabel.Visibility.Set(IsEmpty && !IsActivated && !IsFocused);
      GhostEdit.Visibility.Set(GhostLabel.Visibility.Get());
      ClearButton.Visibility.Set(!IsMandatoryField && (!CanActivate || IsActivated && IsFocused) && !IsEmpty && ClearAction != null);

      ClientDock.Visibility.Set(!IsEmpty || IsActivated || IsFocused);
      CaptionLabel.Font.Colour = !string.IsNullOrWhiteSpace(ErrorText) ? Inv.Colour.Red : IsFocused ? Inv.Material.Theme.PrimaryColour : Inv.Theme.SubtleColour;

      var SetHelperText = IsMandatoryField && IsEmpty ? "Required" : HelperTextField;
      HelperLabel.Text = string.IsNullOrWhiteSpace(ErrorTextField) ? SetHelperText : ErrorTextField;
      HelperLabel.Font.Colour = string.IsNullOrWhiteSpace(ErrorTextField) ? Inv.Theme.SubtleColour : Inv.Colour.Red;
      ErrorIcon.Visibility.Set(IsFocused && !string.IsNullOrWhiteSpace(ErrorTextField));

      if (!string.IsNullOrWhiteSpace(ErrorTextField))
        Button.Border.Colour = Inv.Colour.Red;
      else
        Button.Border.Colour = IsFocused ? Inv.Material.Theme.PrimaryColour : Inv.Theme.SoftColour;

      Button.Border.Set(0, 0, 0, IsFocused ? 2 : 1);
      GhostLabel.Margin.Set(0, 0, 0, IsFocused ? 0 : 1);
      ClientDock.Margin.Set(0, 0, 0, IsFocused ? 0 : 1);
    }
    private void RefreshActivated()
    {
      GhostLabel.Visibility.Set(!IsActivated);
      GhostEdit.Visibility.Set(GhostLabel.Visibility.Get());
      ClientDock.Visibility.Set(IsActivated);
    }
    private void ActivateHandler()
    {
      SingleTapAction?.Invoke();

      if (CanActivate)
        Activate();
    }

    private readonly Inv.Button Button;
    private readonly Inv.Edit GhostEdit;
    private readonly Inv.Label GhostLabel;
    private readonly Inv.Dock ClientDock;
    private readonly Inv.Label CaptionLabel;
    private readonly Inv.Frame ContentFrame;
    private readonly Inv.Material.IconButton ClearButton;
    private readonly Inv.Material.Icon ErrorIcon;
    private readonly Inv.Label HelperLabel;
    private bool IsReadOnlyField;
    private bool IsActivated;
    private bool IsFocused;
    private bool IsMandatoryField;
    private bool CanActivate;
    private string ErrorTextField;
    private string HelperTextField;
    private readonly PropertyRule MandatoryRule;
    private readonly Inv.DistinctList<PropertyRuleReport> InherentReportList;
    private readonly Inv.DistinctList<PropertyRuleReport> TransientReportList;
    private Action SingleTapAction;
    private Action ClearAction;
  }

  public sealed class Integer32Field : Inv.Panel<PropertyField>
  {
    public Integer32Field(PropertyField Base)
    {
      this.Base = Base;
      Base.ReadOnlyChangeEvent += () =>
      {
        Edit.IsReadOnly = Base.IsReadOnly;
      };

      this.Edit = Inv.Edit.NewInteger();
      Base.Focus = Edit.Focus;
      Base.Content = Edit;
      Edit.Font.Custom(16).In(Inv.Theme.OnBackgroundColour);
      Edit.ChangeEvent += () =>
      {
        Base.SetEmpty(IsEmpty());

        Base.ChangeInvoke();

        ChangeEvent?.Invoke();
      };
      Edit.Focus.GotEvent += () => Base.GotFocus();
      Edit.Focus.LostEvent += () => Base.LostFocus();

      var ValidRule = Base.AddRule();
      ValidRule.CheckEvent += (Report) =>
      {
        if (!Base.IsEmpty && Content == null)
          return Report.Fail("Must be a valid number");

        return Report.Pass();
      };

      this.MinimumValueRule = Base.AddRule();
      MinimumValueRule.CheckEvent += (Report) =>
      {
        if (MinimumValueField != null && Content != null && Content < MinimumValueField)
          return Report.Fail("Must be greater than " + MinimumValueField.Value.ToString());

        return Report.Pass();
      };
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public bool IsReadOnly
    {
      get => Base.IsReadOnly;
      set => Base.IsReadOnly = value;
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public int? Content
    {
      get
      {
        var Text = Edit.Text.NullAsEmpty();

        if (int.TryParse(Text, out var Result))
          return Result;

        return null;
      }
      set => Edit.Text = value?.ToString();
    }
    public int? MinimumValue
    {
      get => MinimumValueField;
      set
      {
        MinimumValueField = value;
        MinimumValueRule.IsEnabled = value != null;
      }
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ReturnEvent
    {
      add => Edit.ReturnEvent += value;
      remove => Edit.ReturnEvent -= value;
    }
    public event Action ChangeEvent;

    public Inv.Focus GetFocus() => Edit.Focus;
    public bool IsEmpty() => string.IsNullOrWhiteSpace(Edit.Text);

    private readonly Inv.Edit Edit;
    private readonly PropertyRule MinimumValueRule;
    private int? MinimumValueField;
  }

  public sealed class DecimalField : Inv.Panel<PropertyField>
  {
    public DecimalField(PropertyField Base)
    {
      this.Base = Base;

      this.Edit = Inv.Edit.NewDecimal();
      Base.Focus = Edit.Focus;
      Base.Content = Edit;
      Edit.Font.Custom(16).In(Inv.Theme.OnBackgroundColour);
      Edit.ChangeEvent += () =>
      {
        Base.SetEmpty(IsEmpty());

        Base.ChangeInvoke();

        ChangeEvent?.Invoke();
      };
      Edit.Focus.GotEvent += () => Base.GotFocus();
      Edit.Focus.LostEvent += () => Base.LostFocus();
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public decimal? Content
    {
      get
      {
        var TrimValue = Edit.Text.NullAsEmpty();

        if (decimal.TryParse(TrimValue, out var Result))
          return Result;

        return null;
      }
      set => Edit.Text = value?.ToString();
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ReturnEvent
    {
      add => Edit.ReturnEvent += value;
      remove => Edit.ReturnEvent -= value;
    }
    public event Action ChangeEvent;
    public event Action<Inv.Keystroke> KeyPressEvent
    {
      add => Edit.KeyPressEvent += value;
      remove => Edit.KeyPressEvent -= value;
    }

    public Inv.Focus GetFocus() => Edit.Focus;
    public bool IsEmpty() => string.IsNullOrWhiteSpace(Edit.Text);

    private readonly Inv.Edit Edit;
  }

  public sealed class MoneyField : Inv.Panel<PropertyField>
  {
    public MoneyField(PropertyField Base)
    {
      this.Base = Base;
      this.DecimalField = new Inv.Material.DecimalField(Base);
    }

    public bool IsMandatory
    {
      get => DecimalField.IsMandatory;
      set => DecimalField.IsMandatory = value;
    }
    public string CaptionText
    {
      get => DecimalField.CaptionText;
      set => DecimalField.CaptionText = value;
    }
    public Inv.Money? Content
    {
      get
      {
        var DecimalValue = DecimalField.Content;

        return DecimalValue != null ? new Inv.Money(DecimalValue.Value) : (Inv.Money?)null;
      }
      set => DecimalField.Content = value?.GetAmount();
    }
    public string ErrorText
    {
      get => DecimalField.ErrorText;
      set => DecimalField.ErrorText = value;
    }
    public string HelperText
    {
      get => DecimalField.HelperText;
      set => DecimalField.HelperText = value;
    }
    public Inv.Margin Margin => DecimalField.Margin;
    public Inv.Alignment Alignment => DecimalField.Alignment;
    public Inv.Visibility Visibility => DecimalField.Visibility;
    public event Action ReturnEvent
    {
      add => DecimalField.ReturnEvent += value;
      remove => DecimalField.ReturnEvent -= value;
    }
    public event Action ChangeEvent
    {
      add => DecimalField.ChangeEvent += value;
      remove => DecimalField.ChangeEvent -= value;
    }
    public event Action<Inv.Keystroke> KeyPressEvent
    {
      add => DecimalField.KeyPressEvent += value;
      remove => DecimalField.KeyPressEvent -= value;
    }

    public Inv.Focus GetFocus() => DecimalField.GetFocus();
    public bool IsEmpty() => DecimalField.IsEmpty();

    private readonly Inv.Material.DecimalField DecimalField;
  }

  public sealed class EditField : Inv.Panel<PropertyField>
  {
    public EditField(PropertyField Base, Inv.EditInput EditInput)
    {
      this.Base = Base;
      Base.ReadOnlyChangeEvent += () =>
      {
        Edit.IsReadOnly = Base.IsReadOnly;
      };

      this.Edit = Inv.Edit.New(EditInput);
      Base.Focus = Edit.Focus;
      Base.Content = Edit;
      Edit.Font.Custom(16).In(Inv.Theme.OnBackgroundColour);
      Edit.ChangeEvent += () =>
      {
        Base.SetEmpty(IsEmpty());

        Base.ChangeInvoke();

        ChangeEvent?.Invoke();
      };
      Edit.Focus.GotEvent += () => Base.GotFocus();
      Edit.Focus.LostEvent += () => Base.LostFocus();

      var Rule = Base.AddRule();
      Rule.CheckEvent += (Report) =>
      {
        if (CheckEvent != null)
          return CheckEvent(Report);

        return Report.Pass();
      };
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public bool IsReadOnly
    {
      get => Base.IsReadOnly;
      set => Base.IsReadOnly = value;
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public string Content
    {
      get => Edit.Text;
      set => Edit.Text = value;
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ReturnEvent
    {
      add => Edit.ReturnEvent += value;
      remove => Edit.ReturnEvent -= value;
    }
    public event Action ChangeEvent;
    public event Func<PropertyRuleReport, bool> CheckEvent;

    public Inv.Focus GetFocus() => Edit.Focus;
    public bool IsEmpty() => string.IsNullOrWhiteSpace(Edit.Text);
    public void Activate() => Base.Activate();

    public static implicit operator Inv.Material.PropertyField(EditField Self) => Self?.Base;

    private readonly Inv.Edit Edit;
  }

  public sealed class MemoField : Inv.Panel<PropertyField>
  {
    public MemoField(PropertyField Base)
    {
      this.Base = Base;

      this.Memo = Inv.Memo.New();
      Base.Focus = Memo.Focus;
      Base.Content = Memo;
      Memo.Font.Custom(16).In(Inv.Theme.OnBackgroundColour);
      Memo.ChangeEvent += () =>
      {
        Base.SetEmpty(string.IsNullOrWhiteSpace(Memo.Text));

        Base.ChangeInvoke();

        ChangeEvent?.Invoke();
      };
      Memo.Focus.GotEvent += () => Base.GotFocus();
      Memo.Focus.LostEvent += () => Base.LostFocus();
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public bool ShowCaption
    {
      get => Base.ShowCaption;
      set => Base.ShowCaption = value;
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public string Content
    {
      get => Memo.Text;
      set => Memo.Text = value;
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ChangeEvent;

    public Inv.Focus GetFocus() => Memo.Focus;
    public bool IsEmpty() => string.IsNullOrWhiteSpace(Memo.Text);

    private readonly Inv.Memo Memo;
  }

  public sealed class DateOfBirthField : Inv.Panel<PropertyField>
  {
    public DateOfBirthField(PropertyField Base)
    {
      this.Base = Base;

      var Overlay = Inv.Overlay.New();
      Base.Content = Overlay;

      this.Dock = Inv.Dock.NewHorizontal();
      Overlay.AddPanel(Dock);

      this.DayEdit = Inv.Edit.NewInteger();
      Dock.AddHeader(DayEdit);
      DayEdit.Font.Custom(16).In(Inv.Theme.OnBackgroundColour);
      DayEdit.Size.SetMinimumWidth(35);
      DayEdit.ChangeEvent += () =>
      {
        Refresh();

        ChangeInvoke();

        if (DayEdit.Text.Length >= 2)
          MonthEdit.Focus.Set();
      };
      DayEdit.Focus.GotEvent += () => Refresh();
      DayEdit.Focus.LostEvent += () => Refresh();

      this.DayMonthLabel = Inv.Label.New();
      Dock.AddHeader(DayMonthLabel);
      DayMonthLabel.Visibility.Collapse();
      DayMonthLabel.Margin.Set(4, 0);
      DayMonthLabel.Alignment.Center();
      DayMonthLabel.Font.Custom(16).In(Inv.Theme.OnBackgroundColour);
      DayMonthLabel.Text = "/";

      this.MonthEdit = Inv.Edit.NewInteger();
      Dock.AddHeader(MonthEdit);
      MonthEdit.Font.Custom(16).In(Inv.Theme.OnBackgroundColour);
      MonthEdit.Size.SetMinimumWidth(35);
      MonthEdit.ChangeEvent += () =>
      {
        Refresh();

        ChangeInvoke();

        if (MonthEdit.Text.Length >= 2)
          YearEdit.Focus.Set();
      };
      MonthEdit.Focus.GotEvent += () => Refresh();
      MonthEdit.Focus.LostEvent += () => Refresh();

      this.MonthYearLabel = Inv.Label.New();
      Dock.AddHeader(MonthYearLabel);
      MonthYearLabel.Visibility.Collapse();
      MonthYearLabel.Margin.Set(4, 0);
      MonthYearLabel.Alignment.Center();
      MonthYearLabel.Font.Custom(16).In(Inv.Theme.OnBackgroundColour);
      MonthYearLabel.Text = "/";

      this.YearEdit = Inv.Edit.NewInteger();
      Dock.AddClient(YearEdit);
      YearEdit.Font.Custom(16).In(Inv.Theme.OnBackgroundColour);
      YearEdit.Size.SetMinimumWidth(35);
      YearEdit.ChangeEvent += () =>
      {
        Refresh();

        ChangeInvoke();
      };
      YearEdit.Focus.GotEvent += () => Refresh();
      YearEdit.Focus.LostEvent += () => Refresh();

      this.FocusEdit = Inv.Edit.NewText();
      Base.Focus = FocusEdit.Focus;
      Overlay.AddPanel(FocusEdit);
      FocusEdit.Focus.GotEvent += () =>
      {
        DayMonthLabel.Visibility.Show();
        MonthYearLabel.Visibility.Show();

        DayEdit.Focus.Set();
      };

      var Rule = Base.AddRule();
      Rule.CheckEvent += (Report) =>
      {
        var LeapYear = 2016;

        var DayValue = Day;
        var MonthValue = Month;
        var YearValue = Year;

        if (DayValue == null && MonthValue == null && YearValue == null)
          return Report.Pass();

        if (!IsValidInput())
          return Report.Fail("Invalid date", GetDayFocus());

        if (DayValue == null)
          return Report.Fail("Day is required", GetDayFocus());

        if (DayValue < 1)
          return Report.Fail($"'{ DayValue }' is not a valid day", GetDayFocus());

        if (MonthValue == null)
          return Report.Fail("Month is required", GetMonthFocus());

        if (MonthValue < 1 || MonthValue > 12)
          return Report.Fail($"'{ MonthValue }' is not a valid month", GetMonthFocus());

        if (YearValue == null)
        {
          if (DayValue > DateTime.DaysInMonth(LeapYear, MonthValue.Value))
          {
            var Date = new Inv.Date(LeapYear, MonthValue.Value, 1);

            return Report.Fail($"{ Date.ToString("MMMM") } does not have { DayValue } days", GetDayFocus());
          }
        }
        else if (YearValue <= 0 || (YearValue > 99 && YearValue < 1800))
        {
          return Report.Fail($"Unrecognisable year", GetYearFocus());
        }
        else if (DayValue > DateTime.DaysInMonth(YearValue.Value, MonthValue.Value))
        {
          var Date = new Inv.Date(YearValue.Value, MonthValue.Value, 1);

          return Report.Fail($"{ Date.ToString("MMMM yyyy") } does not have { DayValue } days", GetDayFocus());
        }

        return Report.Pass();
      };
    }

    public int? Day
    {
      get => int.TryParse(DayEdit.Text, out var Result) ? Result : (int?)null;
      set => DayEdit.Text = value != null ? value.ToString().PadLeft(2, '0') : "";
    }
    public int? Month
    {
      get => int.TryParse(MonthEdit.Text, out var Result) ? Result : (int?)null;
      set => MonthEdit.Text = value != null ? value.ToString().PadLeft(2, '0') : "";
    }
    public int? Year
    {
      get => int.TryParse(YearEdit.Text, out var Result) ? Result : (int?)null;
      set => YearEdit.Text = value != null ? (value % 100).ToString() : "";
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ChangeEvent;

    public bool IsValidInput()
    {
      return (string.IsNullOrWhiteSpace(DayEdit.Text) || DayEdit.Text.IsNumeric()) && (string.IsNullOrWhiteSpace(MonthEdit.Text) || MonthEdit.Text.IsNumeric()) && (string.IsNullOrWhiteSpace(YearEdit.Text) || YearEdit.Text.IsNumeric());
    }
    public Inv.Focus GetFocus() => DayEdit.Focus;
    public Inv.Focus GetDayFocus() => DayEdit.Focus;
    public Inv.Focus GetMonthFocus() => MonthEdit.Focus;
    public Inv.Focus GetYearFocus() => YearEdit.Focus;

    private void Refresh()
    {
      var IsFocused = false;

      if (DayEdit.Focus.Has() || MonthEdit.Focus.Has() || YearEdit.Focus.Has())
      {
        IsFocused = true;

        FocusEdit.Visibility.Collapse();

        Base.GotFocus();
      }
      else
      {
        Base.LostFocus();

        FocusEdit.Visibility.Show();
      }

      DayMonthLabel.Visibility.Set(IsFocused || !IsEmpty());
      MonthYearLabel.Visibility.Set(DayMonthLabel.Visibility.Get());
    }
    private void ChangeInvoke()
    {
      Base.SetEmpty(IsEmpty());

      Base.ChangeInvoke();

      ChangeEvent?.Invoke();
    }
    private bool IsEmpty()
    {
      return Day == null && Month == null && Year == null;
    }

    private readonly Inv.Edit FocusEdit;
    private readonly Inv.Dock Dock;
    private readonly Inv.Edit DayEdit;
    private readonly Inv.Label DayMonthLabel;
    private readonly Inv.Edit MonthEdit;
    private readonly Inv.Label MonthYearLabel;
    private readonly Inv.Edit YearEdit;
  }

  public sealed class DateField : Inv.Panel<PropertyField>
  {
    public DateField(PropertyField Base)
    {
      this.Base = Base;
      Base.AsButton(() =>
      {
        var DatePicker = Inv.CalendarPicker.NewDate();
        DatePicker.Value = (ContentField ?? Inv.Date.Now).ToDateTime();
        DatePicker.SelectEvent += () => Content = DatePicker.Value.AsDate();
        DatePicker.Show();
      }, () =>
      {
        this.Content = null;
      });

      this.Label = Inv.Label.New();
      Base.Content = Label;
      Label.Font.Custom(16).In(Inv.Theme.OnBackgroundColour);

      var Rule = Base.AddRule();
      Rule.CheckEvent += (Report) =>
      {
        if (!IsEmpty() && !IsValidInput())
          return Report.Fail("Invalid date", GetFocus());

        return Report.Pass();
      };
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public Inv.Date? Content
    {
      get
      {
        return ContentField;
      }
      set
      {
        if (ContentField != value)
        {
          ContentField = value;

          Change();
        }
      }
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ChangeEvent;

    public bool IsValidInput()
    {
      return ContentField != null;
    }
    public Inv.Focus GetFocus() => Base.Focus;

    public static implicit operator Inv.Material.PropertyField(DateField Self) => Self?.Base;

    private void Change()
    {
      if (ContentField != null)
        Label.Text = ContentField.Value.ToString("dd / MM / yyyy");
      else
        Label.Text = "   /    /     ";

      Base.SetEmpty(ContentField == null);

      Base.ChangeInvoke();

      ChangeEvent?.Invoke();
    }
    private bool IsEmpty()
    {
      return ContentField == null;
    }

    private readonly Inv.Label Label;
    private Inv.Date? ContentField;
  }

  public sealed class TimeField : Inv.Panel<PropertyField>
  {
    public TimeField(PropertyField Base)
    {
      this.Base = Base;
      Base.AsButton(() =>
      {
        var TimePicker = Inv.CalendarPicker.NewTime();
        TimePicker.Value = (ContentField ?? DateTime.Now.AsTime()).ToDateTime();
        TimePicker.SelectEvent += () => Content = TimePicker.Value.AsTime();
        TimePicker.Show();
      }, () =>
      {
        this.Content = null;
      });

      this.Label = Inv.Label.New();
      Base.Content = Label;
      Label.Font.Custom(16).In(Inv.Theme.OnBackgroundColour);

      var Rule = Base.AddRule();
      Rule.CheckEvent += (Report) =>
      {
        if (!IsEmpty() && !IsValidInput())
          return Report.Fail("Invalid time", GetFocus());

        return Report.Pass();
      };
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public Inv.Time? Content
    {
      get => ContentField;
      set
      {
        if (ContentField != value)
        {
          ContentField = value;

          Change();

        }
      }
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ChangeEvent;

    public bool IsValidInput()
    {
      return ContentField != null;
    }
    public Inv.Focus GetFocus() => Base.Focus;

    private void Change()
    {
      if (ContentField != null)
        Label.Text = ContentField.Value.ToString("HH : mm");
      else
        Label.Text = "   :   ";

      Base.SetEmpty(ContentField == null);

      Base.ChangeInvoke();

      ChangeEvent?.Invoke();
    }
    private bool IsEmpty()
    {
      return ContentField == null;
    }

    public static implicit operator Inv.Material.PropertyField(TimeField Self) => Self?.Base;

    private readonly Inv.Label Label;
    private Inv.Time? ContentField;
  }

  public sealed class RadioSelectField<T> : Inv.Panel<PropertyField>
  {
    public RadioSelectField(PropertyField Base, Space Space)
    {
      var IsShown = false;

      this.Base = Base;
      Base.AsButton(() =>
      {
        if (IsShown)
          return;

        IsShown = true;

        if (ValueList == null || ValueList.Count == 0)
        {
          var AlertDialog = Space.NewAlertDialog();
          AlertDialog.Title = CaptionText;
          AlertDialog.Message = $"No { CaptionText.Plural().ToLower() } are available.";
          AlertDialog.AddRightAction("CLOSE", () => AlertDialog.Hide());
          AlertDialog.HideEvent += () => IsShown = false;
          AlertDialog.Show();

          return;
        }

        SelectDialog.Title = Base.CaptionText;
        SelectDialog.Content = ContentField;
        SelectDialog.Show(ValueList);
      });

      this.Label = Inv.Label.New();
      Base.Content = Label;
      Label.Font.Custom(16).In(Inv.Theme.OnBackgroundColour);

      this.SelectDialog = new RadioSelectDialog<T>(Space);
      SelectDialog.HideEvent += () => IsShown = false;
      SelectDialog.AcceptEvent += () => this.Content = SelectDialog.Content;

      var Rule = Base.AddRule();
      Rule.CheckEvent += (Report) =>
      {
        if (CheckEvent != null)
          return CheckEvent(Report);

        return Report.Pass();
      };
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public T Content
    {
      get => ContentField;
      set
      {
        if (!SelectDialog.IsEqualTo(value, ContentField))
        {
          this.ContentField = value;

          Refresh();

          Change();
        }
      }
    }
    public bool HasContent => ContentField != null;
    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set => Base.IsEnabled = value;
    }
    public string NullTitle
    {
      get => SelectDialog.NullTitle;
      set
      {
        SelectDialog.NullTitle = value;

        Refresh();
      }
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public Inv.Font Font => Label.Font;
    public IEqualityComparer<T> EqualityComparer
    {
      get => SelectDialog.EqualityComparer;
      set => SelectDialog.EqualityComparer = value;
    }
    public event Action ChangeEvent;
    public event Action<ListSelectItem<T>> ComposeEvent
    {
      add
      {
        SelectDialog.ComposeEvent += value;
        Refresh();
      }
      remove => SelectDialog.ComposeEvent -= value;
    }
    public event Func<PropertyRuleReport, bool> CheckEvent;

    public Inv.Focus GetFocus() => Base.Focus;
    public void Compose(IEnumerable<T> ValueList)
    {
      this.ValueList = ValueList.ToDistinctList();
    }
    public void OrderByTitle()
    {
      SelectDialog.OrderByTitle();
    }
    public void OrderByComposition()
    {
      SelectDialog.OrderByComposition();
    }
    public void OrderBy(Comparison<T> OrderByComparison)
    {
      SelectDialog.OrderBy(OrderByComparison);
    }
    public ListCollation<T, G> GroupBy<G>(Func<T, G> KeyQuery, Action<ListHeader<T, G>> HeaderAction)
    {
      return SelectDialog.GroupBy(KeyQuery, HeaderAction);
    }

    public static implicit operator Inv.Material.PropertyField(RadioSelectField<T> Self) => Self?.Base;

    private void Refresh()
    {
      Label.Text = SelectDialog.GetTitle(ContentField, true);

      Base.SetEmpty(ContentField == null && string.IsNullOrWhiteSpace(NullTitle));
    }
    private void Change()
    {
      Base.ChangeInvoke();

      ChangeEvent?.Invoke();
    }

    private readonly RadioSelectDialog<T> SelectDialog;
    private readonly Inv.Label Label;
    private Inv.DistinctList<T> ValueList;
    private T ContentField;
  }

  public sealed class CheckSelectField<T> : Inv.Panel<PropertyField>
  {
    public CheckSelectField(PropertyField Base, Space Space)
    {
      var IsShown = false;

      this.CheckFlow = new ListCheckFlow<T>();

      this.Base = Base;
      Base.IsMandatoryPlural = true;
      Base.AsButton(() =>
      {
        if (IsShown)
          return;

        IsShown = true;

        if (ConfirmationDialog == null)
        {
          this.ConfirmationDialog = Space.NewConfirmationDialog();
          ConfirmationDialog.Content = CheckFlow;
          ConfirmationDialog.Title = Base.CaptionText;
          ConfirmationDialog.HideEvent += () => IsShown = false;
          ConfirmationDialog.AddRightCancelAction();
          ConfirmationDialog.AddRightAction("OK", () =>
          {
            Change();
            ConfirmationDialog.Hide();
          }).AsOutlined();
        }

        ConfirmationDialog.Show();
      });

      var Overlay = Inv.Overlay.New();
      Base.Content = Overlay;

      this.Label = Inv.Label.New();
      Overlay.AddPanel(Label);
      Label.Font.Custom(16).In(Inv.Theme.OnBackgroundColour);

      this.Wrap = Inv.Wrap.NewHorizontal();
      Overlay.AddPanel(Wrap);
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set => Base.IsEnabled = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public Inv.DistinctList<T> Content
    {
      get => CheckFlow.SelectedContexts.ToDistinctList();
      set
      {
        if (value == null)
          return;

        CheckFlow.SelectedContexts = value.ToDistinctList();

        Change();
      }
    }
    public IEqualityComparer<T> EqualityComparer
    {
      get => CheckFlow.EqualityComparer;
      set => CheckFlow.EqualityComparer = value;
    }
    public event Action<ListSelectItem<T>> ComposeEvent
    {
      add => CheckFlow.ComposeEvent += value;
      remove => CheckFlow.ComposeEvent -= value;
    }
    public event Action ChangeEvent;

    public Inv.Focus GetFocus() => Base.Focus;
    public void Compose(IEnumerable<T> Values)
    {
      CheckFlow.Load(Values);
    }
    public void SetCollation<G>(Func<T, G> KeyQuery, Action<ListHeader<T, G>> HeaderAction)
    {
      CheckFlow.GroupBy(KeyQuery, HeaderAction);
    }

    public static implicit operator Inv.Material.PropertyField(CheckSelectField<T> Self) => Self?.Base;

    private void Change()
    {
      Wrap.RemovePanels();

      var SelectedContextArray = CheckFlow.SelectedContexts.ToArray();
      foreach (var Content in SelectedContextArray.OrderBy(C => CheckFlow.GetTitle(C)))
      {
        var Chip = new Inv.Material.Chip();
        Wrap.AddPanel(Chip);
        Chip.Text = CheckFlow.GetTitle(Content);
        Chip.Margin.Set(0, 4, 8, 4);
      }

      Base.SetEmpty(SelectedContextArray.Length == 0);

      Base.ChangeInvoke();

      ChangeEvent?.Invoke();
    }

    private readonly Inv.Label Label;
    private readonly Inv.Wrap Wrap;
    private readonly Inv.Material.ListCheckFlow<T> CheckFlow;
    private ConfirmationDialog ConfirmationDialog;
  }

  public sealed class TimePicker : Inv.Panel<Inv.Stack>
  {
    public TimePicker()
    {
      this.Base = Inv.Stack.NewHorizontal();
      Base.Size.SetHeight(80);

      this.HourEntry = new Entry();
      Base.AddPanel(HourEntry);
      HourEntry.MinimumValue = MinimumHourValue;
      HourEntry.MaximumValue = MaximumHourValue;
      HourEntry.ChangeEvent += () => ChangeInvoke();
      HourEntry.CompleteEvent += () => MinuteEntry.Focus.Set();

      var DelimiterLabel = new Inv.Label();
      Base.AddPanel(DelimiterLabel);
      DelimiterLabel.Margin.Set(0, 0, 0, 16);
      DelimiterLabel.Size.SetWidth(24);
      DelimiterLabel.Justify.Center();
      DelimiterLabel.Alignment.Center();
      DelimiterLabel.Text = ":";
      DelimiterLabel.Font.Custom(60).In(Inv.Theme.OnBackgroundColour);

      this.MinuteEntry = new Entry();
      Base.AddPanel(MinuteEntry);
      MinuteEntry.MinimumValue = MinimumMinuteValue;
      MinuteEntry.MaximumValue = MaximumMinuteValue;
      MinuteEntry.ChangeEvent += () => ChangeInvoke();
    }

    public Inv.Time? Content
    {
      get
      {
        var HourValue = HourEntry.Content;
        var MinuteValue = MinuteEntry.Content;

        if (HourValue == null || MinuteValue == null)
          return null;

        if (HourValue < MinimumHourValue || HourValue > MaximumHourValue || MinuteValue < MinimumMinuteValue || MinuteValue > MaximumMinuteValue)
          return null;

        return new Inv.Time(HourValue.Value, MinuteValue.Value);
      }
      set
      {
        IsInternalChange = true;
        try
        {
          HourEntry.Content = value?.Hour;
          MinuteEntry.Content = value?.Minute;
        }
        finally
        {
          IsInternalChange = false;
        }
      }
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public event Action ChangeEvent;

    private void ChangeInvoke()
    {
      if (IsInternalChange)
        return;

      ChangeEvent?.Invoke();
    }

    private readonly Entry HourEntry;
    private readonly Entry MinuteEntry;
    private bool IsInternalChange;

    private const int MinimumHourValue = 0;
    private const int MaximumHourValue = 23;
    private const int MinimumMinuteValue = 0;
    private const int MaximumMinuteValue = 59;

    private sealed class Entry : Inv.Panel<Inv.Button>
    {
      public Entry()
      {
        this.Base = Inv.Button.NewStark();
        Base.Size.SetWidth(96);
        Base.Corner.Set(4);
        Base.Border.Set(3);
        Base.Padding.Set(0, 4, 0, 8);
        Base.OverEvent += () => Refresh();
        Base.AwayEvent += () => Refresh();
        Base.SingleTapEvent += () => ValueEdit.Focus.Set();

        this.ValueEdit = Inv.Edit.NewInteger();
        Base.Content = ValueEdit;
        ValueEdit.Justify.Center();
        ValueEdit.Font.Custom(48);
        ValueEdit.Focus.GotEvent += () => Refresh();
        ValueEdit.Focus.LostEvent += () => Refresh();
        ValueEdit.KeyPressEvent += (Keystroke) =>
        {
          void Set(int SetValue)
          {
            var ContentValue = SetValue;
            if (MaximumValue != null)
              ContentValue = Math.Min(ContentValue, MaximumValue.Value);
            if (MinimumValue != null)
              ContentValue = Math.Max(ContentValue, MinimumValue.Value);

            this.Content = ContentValue;

            ChangeInvoke();
          }

          var Value = this.Content;

          if (Keystroke.Key == Inv.Key.Up)
            Set(Value == null ? (MinimumValue ?? 0) : Value <= (MinimumValue ?? 0) ? (MaximumValue ?? 0) : (Value.Value - 1));
          else if (Keystroke.Key == Inv.Key.Down)
            Set(Value == null || Value >= MaximumValue ? (MinimumValue ?? 0) : (Value.Value + 1));
        };
        ValueEdit.ChangeEvent += () =>
        {
          if (IsInternalChange)
            return;

          if (ValueEdit.Text != null)
          {
            // TODO: Remove this once Inv.Edit.NewInteger() can ensure no non-numeric characters are allowed.
            var StripText = ValueEdit.Text.KeepNumeric();

            if (StripText.Length != ValueEdit.Text.Length)
            {
              ValueEdit.Text = StripText;
              return;
            }
          }

          if (CompleteEvent != null)
          {
            var Value = this.Content;
            if (Value != null && (ValueEdit.Text.Length == 2 || (ValueEdit.Text.Length == 1 && Value > 2)))
              CompleteEvent();
          }

          ChangeInvoke();
        };

        void Refresh()
        {
          Base.Background.In(ValueEdit.Focus.Has() || Base.IsOver ? Inv.Material.Theme.PrimaryColour.AdjustAlpha(127) : Inv.Theme.Light.WhiteSmoke);
          Base.Border.In(ValueEdit.Focus.Has() ? Inv.Material.Theme.PrimaryColour : Base.IsOver ? Inv.Material.Theme.PrimaryColour.AdjustAlpha(127) : Inv.Theme.Light.WhiteSmoke);
          ValueEdit.Font.In(ValueEdit.Focus.Has() ? Inv.Material.Theme.PrimaryColour : Inv.Theme.OnBackgroundColour);
        }

        Refresh();
      }

      public int? Content
      {
        get => ValueEdit.Text != null && int.TryParse(ValueEdit.Text, out var Value) ? Value : (int?)null;
        set
        {
          IsInternalChange = true;
          try
          {
            ValueEdit.Text = value == null ? string.Empty : value.ToString().PadLeft(2, '0');
          }
          finally
          {
            IsInternalChange = false;
          }
        }
      }
      public int? MinimumValue { get; set; }
      public int? MaximumValue { get; set; }
      public Inv.Focus Focus => ValueEdit.Focus;
      public event Action ChangeEvent;
      public event Action CompleteEvent;

      private void ChangeInvoke() => ChangeEvent?.Invoke();

      private readonly Inv.Edit ValueEdit;
      private bool IsInternalChange;
    }
  }
}