﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices.WindowsRuntime;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// The Universal Windows implementation of the portable Invention application.
  /// </summary>
  public static class UwaShell
  {
    static UwaShell()
    {
      // never crash the app when there is an unhandled exception.
      Windows.UI.Xaml.Application.Current.UnhandledException += (Sender, Event) => Event.Handled = true;

      XboxLive = new UwaXboxLive();
    }

    /// <summary>
    /// Ask if the Windows platform is an Xbox.
    /// </summary>
    public static bool IsXbox => string.Equals(Windows.System.Profile.AnalyticsInfo.VersionInfo.DeviceFamily, "Windows.Xbox", StringComparison.OrdinalIgnoreCase);
    /// <summary>
    /// Disable layout scaling to use the native resolution of the device.
    /// </summary>
    public static void DisableLayoutScaling()
    {
      // Turn off default scaling as the HD TV becomes ~1024x768.
      Windows.UI.ViewManagement.ApplicationViewScaling.TrySetDisableLayoutScaling(true);
    }
    public static UwaXboxLive XboxLive { get; }

    /// <summary>
    /// Bridge the portable application to execute in a page of a Universal Windows application.
    /// </summary>
    /// <param name="UwaPage"></param>
    /// <param name="BridgeAction"></param>
    public static void Bridge(Windows.UI.Xaml.Controls.Page UwaPage, Action<UwaBridge> BridgeAction)
    {
      try
      {
        // Xbox overrides.
        if (IsXbox)
        {
          // Use the entire screen instead of just the TV-safe area.
          Windows.UI.ViewManagement.ApplicationView.GetForCurrentView().SetDesiredBoundsMode(Windows.UI.ViewManagement.ApplicationViewBoundsMode.UseCoreWindow);

          // Prevent default UWP sounds.
          Windows.UI.Xaml.ElementSoundPlayer.State = Windows.UI.Xaml.ElementSoundPlayerState.Off;

          // Disable mouse mode (not yet, it's useful!!!)
          //Windows.UI.Xaml.Application.Current.RequiresPointerMode = Windows.UI.Xaml.ApplicationRequiresPointerMode.WhenRequested;
        }

        var Bridge = new UwaBridge(UwaPage);

        BridgeAction?.Invoke(Bridge);

        Bridge.Engine.Start();

#if DEBUG
        //throw new Exception("I'm a fail");
#endif
      }
      catch (Exception Exception)
      {
        var Grid = new Windows.UI.Xaml.Controls.Grid();
        UwaPage.Content = Grid;
        Grid.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
        Grid.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;

        var Scroll = new Windows.UI.Xaml.Controls.ScrollViewer();
        Grid.Children.Add(Scroll);
        Scroll.Margin = new Windows.UI.Xaml.Thickness(0, 100, 0, 0);
        Scroll.VerticalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;
        Scroll.HorizontalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;

        var TextBox = new Windows.UI.Xaml.Controls.TextBox();
        Scroll.Content = TextBox;
        TextBox.IsReadOnly = true;
        TextBox.TextWrapping = Windows.UI.Xaml.TextWrapping.Wrap;

        var Message = "Windows Store v" + GetPackageVersion() + Environment.NewLine + Environment.NewLine + Exception.AsReport();
        TextBox.Text = Message;

        var Button = new Windows.UI.Xaml.Controls.Button();
        Grid.Children.Add(Button);
        Button.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
        Button.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
        Button.Margin = new Windows.UI.Xaml.Thickness(0, 30, 0, 0);
        Button.Content = "Send this start up fault to the author";
        Button.Click += (Sender, Event) => MailTo("hodgskin.callan@gmail.com", "Start Up Fault", Message);
      }
    }
    /// <summary>
    /// Attach the application to execute in a page of a Universal Windows application.
    /// </summary>
    /// <param name="UwaPage"></param>
    /// <param name="ApplicationAction"></param>
    public static void Attach(Windows.UI.Xaml.Controls.Page UwaPage, Action<Inv.Application> ApplicationAction)
    {
      Bridge(UwaPage, B => ApplicationAction(B.Application));
    }

    internal static void MailTo(string To, string Subject, string Body)
    {
      var MailUri = new Uri($"mailto:{To}?subject={Uri.EscapeDataString(Subject)}&body={Uri.EscapeDataString(Body)}");

      // TODO: attachments not supported by mailto protocol.

      var MailTask = Windows.System.Launcher.LaunchUriAsync(MailUri).AsTask();
      MailTask.Wait();
    }
    internal static Windows.UI.Xaml.ResourceDictionary LoadResourceDictionary(string Name)
    {
      var BaseName = "ms-appx:///" + typeof(UwaShell).Assembly.GetName().Name + "/";
      var Dictionary = new Windows.UI.Xaml.ResourceDictionary();

      if (Dictionary.TrySetSource(new Uri(BaseName + "resources/" + Name, UriKind.Absolute)) || Dictionary.TrySetSource(new Uri(BaseName + Name, UriKind.Absolute)))
      {
        return Dictionary;
      }
      else
      {
        Debug.WriteLine("Unable to find resource dictionary: " + Name);
        return null;
      }
    }
    [DebuggerNonUserCode]
    internal static bool TrySetSource(this Windows.UI.Xaml.ResourceDictionary ResourceDictionary, Uri Uri)
    {
      try
      {
        ResourceDictionary.Source = Uri;

        return true;
      }
      catch
      {
        return false;
      }
    }
    internal static string GetPackageVersion()
    {
      try
      {
        var UwaVersion = Windows.ApplicationModel.Package.Current.Id.Version;

        return $"{UwaVersion.Major}.{UwaVersion.Minor}.{UwaVersion.Build}";
      }
      catch
      {
        return "?";
      }
    }
  }

  public sealed class UwaXboxLive
  {
    internal UwaXboxLive()
    {
      this.AutoSignIn = false;

      Microsoft.Xbox.Services.System.XboxLiveUser.SignOutCompleted += (Sender, Event) =>
      {
        SignOutInvoke();
        UserTag = null;

        if (UwaShell.IsXbox)
          InsistDialog();
      };
    }

    /// <summary>
    /// Automatically sign in to Xbox Live when the app is started.
    /// </summary>
    public bool AutoSignIn { get; set; }
    /// <summary>
    /// The active Xbox Live User Tag, once a user has been signed-in.
    /// </summary>
    public string UserTag { get; internal set; }
    public event Action SignInEvent;
    public event Action SignOutEvent;

    public void SignIn()
    {
      if (UserTag == null)
        Initialise(UserInteraction: true);
    }

    internal async void Initialise(bool UserInteraction)
    {
      var CoreDispatcher = Windows.ApplicationModel.Core.CoreApplication.GetCurrentView().Dispatcher;

      SignOutInvoke();
      UserTag = null;
      try
      {
        var User = new Microsoft.Xbox.Services.System.XboxLiveUser();
        var Result = await User.SignInSilentlyAsync(CoreDispatcher); // should only return Success or UserInteractionRequired.

        var SignedIn = Result.Status == Microsoft.Xbox.Services.System.SignInStatus.Success;

        if (UserInteraction && Result.Status == Microsoft.Xbox.Services.System.SignInStatus.UserInteractionRequired)
        {
          Result = await User.SignInAsync(CoreDispatcher); // should only return Success or UserCancel.

          if (Result.Status == Microsoft.Xbox.Services.System.SignInStatus.Success)
          {
            SignedIn = true;
          }
          else if (Result.Status == Microsoft.Xbox.Services.System.SignInStatus.UserCancel)
          {
            if (UwaShell.IsXbox)
              InsistDialog();
          }
        }

        if (SignedIn)
        {
          //var m_xboxLiveContext = new Microsoft.Xbox.Services.XboxLiveContext(User);

          UserTag = User.Gamertag;
          SignInInvoke();
        }
      }
      catch// (Exception Exception)
      {
        // TODO: suppress sign in errors?
      }
    }
    internal async void InsistDialog()
    {
      // present in-game UX that allows the user to retry the sign-in operation.
      var ContentDialog = new Windows.UI.Xaml.Controls.ContentDialog()
      {
        Title = "Xbox Sign-In Required",
        Content = "You must be signed-in to Xbox Live to use this application.",
        IsPrimaryButtonEnabled = true,
        IsSecondaryButtonEnabled = false,
        PrimaryButtonText = "Sign-In"
      };
      ContentDialog.PrimaryButtonClick += (Sender, Event) => Initialise(UserInteraction: true);
      await ContentDialog.ShowAsync();
    }
    internal void SignInInvoke() => SignInEvent?.Invoke();
    internal void SignOutInvoke() => SignOutEvent?.Invoke();
  }

  internal sealed class UwaPlatform : Inv.Platform
  {
    public UwaPlatform(UwaEngine Engine)
    {
      this.Engine = Engine;
      this.Vault = new UwaVault();
      this.LocalFolder = Windows.Storage.ApplicationData.Current.LocalFolder;

      this.UwaCanvasDevice = new Microsoft.Graphics.Canvas.CanvasDevice();
    }

    int Platform.ThreadAffinity()
    {
      return Environment.CurrentManagedThreadId;
    }
    string Platform.CalendarTimeZoneName()
    {
      return TimeZoneInfo.Local.DisplayName;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      Engine.ShowCalendarPicker(CalendarPicker);
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      // NOTE: attachments still do not work with Outlook (it will only work if you use the Windows 10 Mail app).

      var em = new Windows.ApplicationModel.Email.EmailMessage();
      foreach (var To in EmailMessage.GetTos())
        em.To.Add(new Windows.ApplicationModel.Email.EmailRecipient(To.Address, To.Name));
      em.Subject = EmailMessage.Subject ?? "";
      em.Body = EmailMessage.Body ?? "";

      foreach (var Attachment in EmailMessage.GetAttachments())
      {
        var AttachmentFile = SelectStorageFile(Attachment.File);
        var AttachmentStream = Windows.Storage.Streams.RandomAccessStreamReference.CreateFromFile(AttachmentFile);

        em.Attachments.Add(new Windows.ApplicationModel.Email.EmailAttachment(Attachment.Name, AttachmentStream));
      }

      Windows.ApplicationModel.Email.EmailManager.ShowComposeNewEmailAsync(em).AsTask().Forget();

      //UwaShell.MailTo(EmailMessage.GetTos().Select(T => T.Address).AsSeparatedText(","), EmailMessage.Subject ?? "", EmailMessage.Body ?? "");

      return true;
    }
    bool Platform.PhoneIsSupported
    {
      get => Windows.Foundation.Metadata.ApiInformation.IsApiContractPresent("Windows.ApplicationModel.Calls.CallsPhoneContract", 1, 0); // TODO: Windows 10 Mobile Phone Support.
    }
    void Platform.PhoneDial(string PhoneNumber)
    {
      Windows.ApplicationModel.Calls.PhoneCallManager.ShowPhoneCallUI(PhoneNumber, "");
    }
    void Platform.PhoneSMS(string PhoneNumber)
    {
      var ChatMessage = new Windows.ApplicationModel.Chat.ChatMessage();

      ChatMessage.Recipients.Add(PhoneNumber);

      Windows.ApplicationModel.Chat.ChatMessageManager.ShowComposeSmsMessageAsync(ChatMessage).AsTask().Forget();
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      var StorageFile = SelectStorageFile(File);

      var FileTask = StorageFile.GetBasicPropertiesAsync().AsTask();
      FileTask.Wait();

      return (long)FileTask.Result.Size;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      var StorageFile = SelectStorageFile(File);

      var PropertiesTask = StorageFile.GetBasicPropertiesAsync().AsTask();
      PropertiesTask.Wait();

      return PropertiesTask.Result.DateModified.UtcDateTime;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      /*
      var StorageFile = SelectStorageFile(File);

      var PropertiesTask = FileTask.Result.Properties.SavePropertiesAsync(new List<System.Collections.Generic.KeyValuePair<string, object>> 
      { 
        //new System.Collections.Generic.KeyValuePair<string, object>("{B725F130-47EF-101A-A5F1-02608C9EEBAC}", Timestamp.ToString("dd/MM/yyyy hh:mm:ss tt +00:00")) 
        new System.Collections.Generic.KeyValuePair<string, object>("{B725F130-47EF-101A-A5F1-02608C9EEBAC}", new DateTimeOffset(Timestamp, TimeSpan.Zero)) 
      }).AsTask();
      PropertiesTask.Wait();
      */

      // TODO: WinRT equivalent of System.IO.File.SetLastWriteTimeUtc(SelectFilePath(File), Timestamp);
    }
    System.IO.Stream Platform.DirectoryCreateFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var WriteTask = System.IO.WindowsRuntimeStorageExtensions.OpenStreamForWriteAsync(StorageFolder, File.Name, Windows.Storage.CreationCollisionOption.ReplaceExisting);
      WriteTask.Wait();

      return WriteTask.Result;
    }
    System.IO.Stream Platform.DirectoryAppendFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var WriteTask = System.IO.WindowsRuntimeStorageExtensions.OpenStreamForWriteAsync(StorageFolder, File.Name, Windows.Storage.CreationCollisionOption.OpenIfExists);
      WriteTask.Wait();

      WriteTask.Result.Seek(0, System.IO.SeekOrigin.End);

      return WriteTask.Result;
    }
    System.IO.Stream Platform.DirectoryOpenFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var ReadTask = System.IO.WindowsRuntimeStorageExtensions.OpenStreamForReadAsync(StorageFolder, File.Name);
      ReadTask.Wait();

      return ReadTask.Result;
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var FileTask = StorageFolder.TryGetItemAsync(File.Name).AsTask();
      FileTask.Wait();

      return FileTask.Result != null;
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      var StorageFile = SelectStorageFile(File);

      var DeleteTask = StorageFile.DeleteAsync().AsTask();
      DeleteTask.Wait();
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      var SourceStorageFile = SelectStorageFile(SourceFile);
      var TargetStorageFolder = SelectStorageFolder(TargetFile.Folder);

      var CopyTask = SourceStorageFile.CopyAsync(TargetStorageFolder, TargetFile.Name).AsTask();
      CopyTask.Wait();
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      var SourceStorageFile = SelectStorageFile(SourceFile);
      var TargetStorageFolder = SelectStorageFolder(TargetFile.Folder);

      var MoveTask = SourceStorageFile.MoveAsync(TargetStorageFolder, TargetFile.Name).AsTask();
      MoveTask.Wait();
    }
    void Platform.DirectoryReplaceFile(File SourceFile, File TargetFile)
    {
      var SourceStorageFile = SelectStorageFile(SourceFile);
      var TargetStorageFile = SelectStorageFile(TargetFile);

      var MoveTask = SourceStorageFile.MoveAndReplaceAsync(TargetStorageFile).AsTask();
      MoveTask.Wait();
    }
    IEnumerable<Folder> Platform.DirectoryGetFolderSubfolders(Folder Folder, string FolderMask)
    {
      var StorageFolder = SelectStorageFolder(Folder);

      var FoldersTask = StorageFolder.GetFoldersAsync().AsTask();
      FoldersTask.Wait();

      var Regex = new System.Text.RegularExpressions.Regex("^" + System.Text.RegularExpressions.Regex.Escape(FolderMask).Replace(@"\*", ".*").Replace(@"\?", ".") + "$");

      foreach (var ResultFolder in FoldersTask.Result.OrderBy(F => F.Name))
      {
        if (Regex.IsMatch(ResultFolder.Name))
          yield return Folder.NewFolder(ResultFolder.Name);
      }
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      var StorageFolder = SelectStorageFolder(Folder);

      var FilesTask = StorageFolder.GetFilesAsync(Windows.Storage.Search.CommonFileQuery.DefaultQuery).AsTask(); // DefaultQuery is the only shallow folder search.
      FilesTask.Wait();

      var Regex = new System.Text.RegularExpressions.Regex("^" + System.Text.RegularExpressions.Regex.Escape(FileMask).Replace(@"\*", ".*").Replace(@"\?", ".") + "$");

      foreach (var ResultFile in FilesTask.Result.OrderBy(F => F.Name))
      {
        if (Regex.IsMatch(ResultFile.Name))
          yield return new File(Folder, ResultFile.Name);
      }
    }
    IEnumerable<Asset> Platform.DirectoryGetAssets(string AssetMask)
    {
      var InstallFolder = SelectStorageAssetFolder();

      var FolderTask = InstallFolder.TryGetItemAsync("Assets").AsTask();
      FolderTask.Wait();
      var AssetsFolder = FolderTask.Result as Windows.Storage.StorageFolder;

      if (AssetsFolder != null)
      {
        var AssetsTask = AssetsFolder.GetFilesAsync(Windows.Storage.Search.CommonFileQuery.DefaultQuery).AsTask(); // DefaultQuery is the only shallow folder search.
        AssetsTask.Wait();

        var Regex = new System.Text.RegularExpressions.Regex("^" + System.Text.RegularExpressions.Regex.Escape(AssetMask).Replace(@"\*", ".*").Replace(@"\?", ".") + "$");

        foreach (var ResultFile in AssetsTask.Result.OrderBy(F => F.Name))
        {
          if (Regex.IsMatch(ResultFile.Name))
            yield return new Asset(Engine.InvApplication.Directory, ResultFile.Name);
        }
      }
    }
    long Platform.DirectoryGetLengthAsset(Asset Asset)
    {
      var StorageAsset = SelectStorageAsset(Asset);

      var AssetTask = StorageAsset.GetBasicPropertiesAsync().AsTask();
      AssetTask.Wait();

      return (long)AssetTask.Result.Size;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcAsset(Asset Asset)
    {
      var StorageAsset = SelectStorageAsset(Asset);

      var PropertiesTask = StorageAsset.GetBasicPropertiesAsync().AsTask();
      PropertiesTask.Wait();

      return PropertiesTask.Result.DateModified.UtcDateTime;
    }
    System.IO.Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      var StorageAsset = SelectStorageAsset(Asset);

      var StreamTask = StorageAsset.OpenStreamForReadAsync();
      StreamTask.Wait();

      return StreamTask.Result;
    }
    bool Platform.DirectoryExistsAsset(Asset Asset)
    {
      var FileTask = SelectStorageAssetFolder().TryGetItemAsync(System.IO.Path.Combine("Assets", Asset.Name)).AsTask();
      FileTask.Wait();
      return FileTask.Result != null;
    }
    void Platform.DirectoryShowFilePicker(DirectoryFilePicker FilePicker)
    {
      var FilePick = new Windows.Storage.Pickers.FileOpenPicker();

      switch (FilePicker.PickType)
      {
        case PickType.Any:
          FilePick.ViewMode = Windows.Storage.Pickers.PickerViewMode.List;
          FilePick.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
          FilePick.FileTypeFilter.Add("*");
          break;

        case PickType.Image:
          FilePick.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
          FilePick.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
          FilePick.FileTypeFilter.Add(".bmp");
          FilePick.FileTypeFilter.Add(".cmp");
          FilePick.FileTypeFilter.Add(".gif");
          FilePick.FileTypeFilter.Add(".jpeg");
          FilePick.FileTypeFilter.Add(".jpg");
          FilePick.FileTypeFilter.Add(".png");
          FilePick.FileTypeFilter.Add(".tif");
          FilePick.FileTypeFilter.Add(".tiff");
          break;

        case PickType.Sound:
          FilePick.ViewMode = Windows.Storage.Pickers.PickerViewMode.List;
          FilePick.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.MusicLibrary;
          FilePick.FileTypeFilter.Add(".mp3");
          FilePick.FileTypeFilter.Add(".wav");
          break;

        default:
          throw new Exception("FileType not handled: " + FilePicker.PickType);
      }

      // NOTE: can't be blocking here because it will break the Windows FileDialog.
      //       the good news is this dialog is old-style modal and prevents input to the window anyway.
      var FileTask = FilePick.PickSingleFileAsync().AsTask();
      FileTask.ContinueWith(Task =>
      {
        var Result = Task.Result;

        if (Result == null)
        {
          Engine.Post(() => FilePicker.CancelInvoke());
        }
        else
        {
          Engine.Post(() => FilePicker.SelectInvoke(new Pick(System.IO.Path.GetFileName(Result.Name), () =>
          {
            var ReadTask = Result.OpenReadAsync().AsTask();
            ReadTask.Wait();
            return ReadTask.Result.AsStreamForRead();
          })));
        }
      });
    }
    string Platform.DirectoryGetFolderPath(Inv.Folder Folder)
    {
      return SelectStorageFolder(Folder).Path;
    }
    string Platform.DirectoryGetFilePath(Inv.File File)
    {
      return SelectStorageFile(File).Path;
    }
    bool Platform.LocationIsSupported => false;
    void Platform.LocationShowMap(string Location)
    {
      var Uri = new Uri(@"bingmaps:?q=" + Location.Replace(" ", "%20"));

      var LauncherOptions = new Windows.System.LauncherOptions();
      LauncherOptions.TargetApplicationPackageFamilyName = "Microsoft.WindowsMaps_8wekyb3d8bbwe";

      var LaunchUriTask = Windows.System.Launcher.LaunchUriAsync(Uri, LauncherOptions).AsTask();
      LaunchUriTask.Wait();
    }
    void Platform.LocationLookup(LocationResult LocationLookup)
    {
      // TODO: implement windows 8 app location services.
      LocationLookup.SetPlacemarks(Array.Empty<LocationPlacemark>());
    }
    void Platform.AudioPlaySound(Sound Sound, float Volume, float Rate, float Pan)
    {
      UwaSoundPlayer.Play(Sound, Volume, Rate, Pan, Looped: false, OneShot: true);
    }
    TimeSpan Platform.AudioGetSoundLength(Inv.Sound Sound)
    {
      return UwaSoundPlayer.GetLength(Sound);
    }
    void Platform.AudioReclaim(IReadOnlyList<Inv.Sound> SoundList)
    {
      Engine.ReclaimSound(SoundList);
    }
    void Platform.AudioPlayClip(AudioClip Clip)
    {
      Clip.Node = UwaSoundPlayer.Play(Clip.Sound, Clip.Volume, Clip.Rate, Clip.Pan, Clip.Looped, OneShot: false);
    }
    void Platform.AudioPauseClip(AudioClip Clip)
    {
      var UwaMediaPlayer = Clip.Node as Windows.Media.Playback.MediaPlayer;
      if (UwaMediaPlayer != null)
        UwaMediaPlayer.Pause();
    }
    void Platform.AudioResumeClip(AudioClip Clip)
    {
      var UwaMediaPlayer = Clip.Node as Windows.Media.Playback.MediaPlayer;
      if (UwaMediaPlayer != null)
        UwaMediaPlayer.Play();
    }
    void Platform.AudioStopClip(AudioClip Clip)
    {
      var UwaMediaPlayer = Clip.Node as Windows.Media.Playback.MediaPlayer;
      Clip.Node = null;

      if (UwaMediaPlayer != null)
        UwaMediaPlayer.Dispose();
    }
    void Platform.AudioModulateClip(AudioClip Clip)
    {
      var UwaMediaPlayer = Clip.Node as Windows.Media.Playback.MediaPlayer;
      if (UwaMediaPlayer != null)
      {
        UwaMediaPlayer.Volume = Clip.Volume;
        UwaMediaPlayer.AudioBalance = Clip.Pan;
        UwaMediaPlayer.PlaybackSession.PlaybackRate = Clip.Rate;
        UwaMediaPlayer.IsLoopingEnabled = Clip.Looped;
      }
    }
    void Platform.AudioStartSpeechRecognition(Inv.SpeechRecognition SpeechRecognition)
    {
      // TODO: Cortana integration.
    }
    void Platform.AudioStopSpeechRecognition(Inv.SpeechRecognition SpeechRecognition)
    {
      // TODO: Cortana integration.
    }
    bool Platform.AudioSpeechRecognitionIsSupported
    {
      get => false; // TODO: Cortana integration.
    }
    void Platform.WindowBrowse(Inv.File File)
    {
      var StorageFile = SelectStorageFile(File);

      var LaunchFileTask = Windows.System.Launcher.LaunchFileAsync(StorageFile).AsTask();
      LaunchFileTask.Wait();
    }
    void Platform.WindowShare(Inv.File File)
    {
      var FilePick = new Windows.Storage.Pickers.FileSavePicker();

      FilePick.SuggestedFileName = File.Name;
      FilePick.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
      FilePick.FileTypeChoices.Add(File.Extension.Trim('.').ToSentenceCase() + " File", new List<string>() { File.Extension });

      // NOTE: can't be blocking here because it will break the Windows FileDialog.
      //       the good news is this dialog is old-style modal and prevents input to the window anyway.
      var FileTask = FilePick.PickSaveFileAsync().AsTask();
      FileTask.ContinueWith(Task =>
      {
        var Result = Task.Result;

        if (Result != null)
        {
          var SourceStorageFile = SelectStorageFile(File);

          var CopyTask = SourceStorageFile.CopyAndReplaceAsync(Result).AsTask();
          CopyTask.Wait();
        }
      });
    }
    void Platform.WindowPost(Action Action)
    {
      Engine.Post(() => Action());
    }
    void Platform.WindowCall(Action Action)
    {
      Engine.Call(() => Action());
    }
    Inv.Dimension Platform.WindowGetDimension(Inv.Panel Panel)
    {
      var Result = Engine.GetPanel(Panel);

      return Result == null ? Inv.Dimension.Zero : new Dimension((int)Result.ActualWidth, (int)Result.ActualHeight);
    }
    void Platform.WindowStartAnimation(Inv.Animation Animation)
    {
      Engine.StartAnimation(Animation);
    }
    void Platform.WindowStopAnimation(Inv.Animation Animation)
    {
      Engine.StopAnimation(Animation);
    }
    void Platform.WindowShowPopup(Inv.Popup Popup)
    {
      Engine.ShowPopup(Popup);
    }
    void Platform.WindowHidePopup(Inv.Popup Popup)
    {
      Engine.HidePopup(Popup);
    }
    long Platform.ProcessMemoryUsedBytes()
    {
      return (long)Windows.System.MemoryManager.AppMemoryUsage;
      //return GC.GetTotalMemory(false);
    }
    string Platform.ProcessAncillaryInformation()
    {
      return "";
    }
    void Platform.WebClientConnect(WebClient WebClient)
    {
      var TcpClient = new Inv.Tcp.Client(WebClient.Host, WebClient.Port);
      TcpClient.Connect();

      WebClient.Node = TcpClient;
      WebClient.SetTransportStream(TcpClient.Stream);
    }
    void Platform.WebClientDisconnect(WebClient WebClient)
    {
      var TcpClient = (Inv.Tcp.Client)WebClient.Node;
      if (TcpClient != null)
      {
        TcpClient.Disconnect();

        WebClient.Node = null;
        WebClient.SetTransportStream(null);
      }
    }
    void Platform.WebServerConnect(WebServer WebServer)
    {
      var TcpServer = new Inv.Tcp.Server(WebServer.Host, WebServer.Port);
      TcpServer.AcceptEvent += (TcpChannel) => WebServer.AcceptChannel(TcpChannel, TcpChannel.Stream);
      TcpServer.RejectEvent += (TcpChannel) => WebServer.RejectChannel(TcpChannel);

      WebServer.Node = TcpServer;
      WebServer.DropDelegate = (Node) => ((Inv.Tcp.Channel)Node).Drop();

      TcpServer.Connect();
    }
    void Platform.WebServerDisconnect(WebServer WebServer)
    {
      var TcpServer = (Inv.Tcp.Server)WebServer.Node;
      if (TcpServer != null)
      {
        TcpServer.Disconnect();

        WebServer.Node = null;
        WebServer.DropDelegate = null;
      }
    }
    void Platform.WebBroadcastConnect(WebBroadcast WebBroadcast)
    {
      var UdpBroadcast = new Inv.Udp.Broadcast();

      WebBroadcast.Node = UdpBroadcast;
      WebBroadcast.SetBroadcast(UdpBroadcast);
    }
    void Platform.WebBroadcastDisconnect(WebBroadcast WebBroadcast)
    {
      var UdpBroadcast = (Inv.Udp.Broadcast)WebBroadcast.Node;
      if (UdpBroadcast != null)
      {
        UdpBroadcast.Close();

        WebBroadcast.Node = null;
        WebBroadcast.SetBroadcast(null);
      }
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      Windows.System.Launcher.LaunchUriAsync(Uri).AsTask().Wait();
    }
    void Platform.WebInstallUri(Uri Uri)
    {
      Windows.System.Launcher.LaunchUriAsync(Uri).AsTask().Wait();
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      Windows.System.Launcher.LaunchUriAsync(new Uri("ms-windows-store:REVIEW?PFN=" + WindowsStoreID)).AsTask().Wait();
    }
    void Platform.VaultLoadSecret(Secret Secret)
    {
      Vault.Load(Secret);
    }
    void Platform.VaultSaveSecret(Secret Secret)
    {
      Vault.Save(Secret);
    }
    void Platform.VaultDeleteSecret(Secret Secret)
    {
      Vault.Delete(Secret);
    }
    bool Platform.ClipboardIsTextSupported
    {
      get => true;
    }
    string Platform.ClipboardGetText()
    {
      var TextTask = Windows.ApplicationModel.DataTransfer.Clipboard.GetContent().GetTextAsync().AsTask();
      TextTask.Wait();
      return TextTask.Result;
    }
    void Platform.ClipboardSetText(string Text)
    {
      var DataPackage = new Windows.ApplicationModel.DataTransfer.DataPackage();
      DataPackage.SetText(Text);
      Windows.ApplicationModel.DataTransfer.Clipboard.SetContent(DataPackage);
    }
    bool Platform.ClipboardIsImageSupported
    {
      get => true;
    }
    Inv.Image Platform.ClipboardGetImage()
    {
      var DataPackage = Windows.ApplicationModel.DataTransfer.Clipboard.GetContent();

      if (DataPackage.Contains("PNG"))
      {
        var PngTask = DataPackage.GetDataAsync("PNG").AsTask();
        PngTask.Wait();

        using (var PngStream = PngTask.Result as Windows.Storage.Streams.IRandomAccessStream)
        {
          var LoadBuffer = new byte[PngStream.Size];

          var LoadTask = PngStream.ReadAsync(LoadBuffer.AsBuffer(), (uint)PngStream.Size, Windows.Storage.Streams.InputStreamOptions.None).AsTask();
          LoadTask.Wait();

          return new Inv.Image(LoadBuffer, ".png");
        }
      }

      if (DataPackage.Contains(Windows.ApplicationModel.DataTransfer.StandardDataFormats.Bitmap))
      {
        var BitmapTask = DataPackage.GetBitmapAsync().AsTask();
        BitmapTask.Wait();

        var ReadTask = BitmapTask.Result.OpenReadAsync().AsTask();
        ReadTask.Wait();

        using (var MemoryStream = ReadTask.Result)
        {
          var LoadBuffer = new byte[MemoryStream.Size];

          var LoadTask = MemoryStream.ReadAsync(LoadBuffer.AsBuffer(), (uint)MemoryStream.Size, Windows.Storage.Streams.InputStreamOptions.None).AsTask();
          LoadTask.Wait();

          // .bmp.
          return new Inv.Image(LoadBuffer, MemoryStream.ContentType.Replace("/image", "."));
        }
      }

      return null;
    }
    void Platform.ClipboardSetImage(Inv.Image Image)
    {
      var DataPackage = new Windows.ApplicationModel.DataTransfer.DataPackage();
      DataPackage.SetBitmap(Windows.Storage.Streams.RandomAccessStreamReference.CreateFromStream(new MemoryStream(Image.GetBuffer()).AsRandomAccessStream()));
      Windows.ApplicationModel.DataTransfer.Clipboard.SetContent(DataPackage);
    }
    void Platform.HapticFeedback(HapticFeedback Feedback)
    {
      Engine.HapticFeedback(Feedback);
    }
    Inv.Dimension Platform.GraphicsGetDimension(Inv.Image Image)
    {
      var Result = Engine.TranslateMediaImage(Image);

      //var ScaleFactor = Windows.Graphics.Display.DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;

      return Result == null ? 
        Inv.Dimension.Zero :
        new Inv.Dimension(Result.PixelWidth, Result.PixelHeight);
      //new Inv.Dimension((int)(Result.PixelWidth * ScaleFactor), (int)(Result.PixelHeight * ScaleFactor));
    }
    Inv.Image Platform.GraphicsGrayscale(Inv.Image Image)
    {
      return ConvertImage(Image, (UwaImageRecord) =>
      {
        var UwaImageWidth = UwaImageRecord.PixelWidth;
        var UwaImageHeight = UwaImageRecord.PixelHeight;
        var UwaPixelArray = UwaImageRecord.PixelArray;

        for (var y = 0; y < UwaImageHeight; y++)
        {
          for (var x = 0; x < UwaImageWidth; x++)
          {
            var i = (x + y * UwaImageWidth) * 4;
            var Avg = (byte)((UwaPixelArray[i] + UwaPixelArray[i + 1] + UwaPixelArray[i + 2]) / 3); // simple average of RGB.

            UwaPixelArray[i    ] = Avg;     // B
            UwaPixelArray[i + 1] = Avg; // G
            UwaPixelArray[i + 2] = Avg; // R
            //UwaPixelArray[i + 3] = unchanged Alpha.
          }
        }
      });
    }
    Inv.Image Platform.GraphicsTint(Inv.Image Image, Inv.Colour Colour)
    {
      var Argb = Colour.GetARGBRecord();
      var AlphaTint = Argb.A / 255.0F;

      return ConvertImage(Image, (UwaImageRecord) =>
      {
        var UwaImageWidth = UwaImageRecord.PixelWidth;
        var UwaImageHeight = UwaImageRecord.PixelHeight;
        var UwaPixelArray = UwaImageRecord.PixelArray;

        float blue;
        float green;
        float red;

        for (var y = 0; y < UwaImageHeight; y++)
        {
          for (var x = 0; x < UwaImageWidth; x++)
          {
            var k = (x + y * UwaImageWidth) * 4;

            blue = (Argb.B - UwaPixelArray[k]) * AlphaTint + UwaPixelArray[k];
            green = (Argb.G - UwaPixelArray[k + 1]) * AlphaTint + UwaPixelArray[k + 1];
            red = (Argb.R - UwaPixelArray[k + 2]) * AlphaTint + UwaPixelArray[k + 2];

            if (blue > 255)
              blue = 255;

            if (green > 255)
              green = 255;

            if (red > 255)
              red = 255;

            UwaPixelArray[k    ] = (byte)blue;     // B
            UwaPixelArray[k + 1] = (byte)green; // G
            UwaPixelArray[k + 2] = (byte)red; // R
            //UwaPixelArray[k + 3] = unchanged Alpha.
          }
        }
      });
    }
    Inv.Image Platform.GraphicsResize(Inv.Image Image, Inv.Dimension Dimension)
    {
      var Result = Engine.ResizeMediaImage(Image, Dimension.Width, Dimension.Height);

      // TODO: this isn't correct - we haven't found a way to get a new buffer of the resized image.
      var InvImage = new Inv.Image(Image.GetBuffer(), ".png");

      // TODO: but we have attached a correctly sized media image to the InvImage.Node.
      InvImage.Node = new UwaImageNode(Result, null);

      return InvImage;

      // TODO: this hangs when starting up or has catastrophic failure if you use Window.Post().
      /*
      var UwaBitmap = Engine.LoadWriteableBitmap(Image, Dimension.Width, Dimension.Height);

      var UwaBuffer = Engine.GetImageAsByteAsync(UwaBitmap, Dimension.Width, Dimension.Height);
      UwaBuffer.Wait();

      // TODO: this isn't correct - we haven't found a way to get a new buffer of the resized image.
      var InvImage = new Inv.Image(UwaBuffer.Result, ".png");

      InvImage.Node = null;

      return InvImage;
      */
    }
    Inv.Pixels Platform.GraphicsReadPixels(Inv.Image Image)
    {
      var UwaImageRecord = ImportImage(Image);

      return new Inv.BGRAByteArrayPixels(UwaImageRecord.PixelWidth, UwaImageRecord.PixelHeight, UwaImageRecord.PixelArray);
    }
    Inv.Image Platform.GraphicsWritePixels(Inv.Pixels Pixels)
    {
      var BGRAByteArrayPixels = (Pixels as Inv.BGRAByteArrayPixels) ?? new BGRAByteArrayPixels(Pixels);

      var UwaImageRecord = new UwaImageRecord()
      {
        PixelWidth = BGRAByteArrayPixels.Width,
        PixelHeight = BGRAByteArrayPixels.Height,
        PixelArray = BGRAByteArrayPixels.PixelArray
      };

      return ExportImage(UwaImageRecord, 96.0, 96.0);
    }
    void Platform.GraphicsRender(Inv.Panel Panel, Action<Inv.Image> ReturnAction)
    {
      var UwaElement = Engine.GetPanel(Panel);

      var RenderTask = RenderUIElement(UwaElement);

      RenderTask.ContinueWith(A => Engine.Post(() =>
      {
        var Image = A.Result;
        try
        {
          ReturnAction(Image);
        }
        finally
        {
          Engine.ReclaimImage(new[] { Image });
        }
      }));
    }
    void Platform.GraphicsReclaim(IReadOnlyList<Inv.Image> ImageList)
    {
      Engine.ReclaimImage(ImageList);
    }
    Inv.Dimension Platform.CanvasCalculateText(string TextFragment, Inv.DrawFont TextFont, int MaximumTextWidth, int MaximumTextHeight)
    {
      using (var TextLayout = new Microsoft.Graphics.Canvas.Text.CanvasTextLayout(UwaCanvasDevice, TextFragment, Engine.NewTextFormat(TextFont), MaximumTextWidth, MaximumTextHeight))
        return new Inv.Dimension((int)(TextLayout.DrawBounds.Width), (int)(TextLayout.DrawBounds.Height));
    }

    private async Task<Inv.Image> RenderUIElement(Windows.UI.Xaml.UIElement UwaElement)
    {
      var UwaBitmap = new Windows.UI.Xaml.Media.Imaging.RenderTargetBitmap();
      await UwaBitmap.RenderAsync(UwaElement);

      var UwaPixelBuffer = await UwaBitmap.GetPixelsAsync();

      var UwaImageRecord = new UwaImageRecord()
      {
        PixelWidth = UwaBitmap.PixelWidth,
        PixelHeight = UwaBitmap.PixelHeight,
        PixelArray = UwaPixelBuffer.ToArray()
      };

      var DisplayInformation = Windows.Graphics.Display.DisplayInformation.GetForCurrentView();

      return await WriteImage(UwaImageRecord, DisplayInformation.RawDpiX, DisplayInformation.RawDpiY);
    }
    private async Task<UwaImageRecord> ReadImage(Inv.Image InvImage)
    {
      var UwaImage = Engine.TranslateMediaImage(InvImage);

      var UwaPixelWidth = UwaImage.PixelWidth;
      var UwaPixelHeight = UwaImage.PixelHeight;

      // create the array to hold the pixel data.
      var UwaPixelArray = new byte[Inv.BGRAByteArrayPixels.PixelSize * UwaPixelWidth * UwaPixelHeight];

      // load the media image stream.
      using (var UwaImageStream = await Engine.LoadMediaImageStream(InvImage))
      {
        // create a writeable bitmap and set the source as the media image stream.
        var UwaBitmap = new Windows.UI.Xaml.Media.Imaging.WriteableBitmap(UwaPixelWidth, UwaPixelHeight);
        UwaBitmap.SetSource(UwaImageStream);

        // load pixel data from the media image stream.
        using (var UwaPixelStream = System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeBufferExtensions.AsStream(UwaBitmap.PixelBuffer))
          UwaPixelStream.Read(UwaPixelArray, 0, UwaPixelArray.Length);
      }

      return new UwaImageRecord()
      {
        PixelWidth = UwaPixelWidth,
        PixelHeight = UwaPixelHeight,
        PixelArray = UwaPixelArray
      };
    }
    private async Task<Inv.Image> WriteImage(UwaImageRecord UwaImageRecord, double DpiX, double DpiY)
    {
      // encode the image as a png byte array.
      using (var MemoryStream = new Windows.Storage.Streams.InMemoryRandomAccessStream())
      {
        var Encoder = await Windows.Graphics.Imaging.BitmapEncoder.CreateAsync(Windows.Graphics.Imaging.BitmapEncoder.PngEncoderId, MemoryStream);

        Encoder.SetPixelData(Windows.Graphics.Imaging.BitmapPixelFormat.Bgra8, Windows.Graphics.Imaging.BitmapAlphaMode.Straight, (uint)UwaImageRecord.PixelWidth, (uint)UwaImageRecord.PixelHeight, DpiX, DpiY, UwaImageRecord.PixelArray);

        await Encoder.FlushAsync();

        await MemoryStream.FlushAsync();

        using (var DataReader = new Windows.Storage.Streams.DataReader(MemoryStream))
        {
          var InvBuffer = new byte[MemoryStream.Size];

          await DataReader.LoadAsync((uint)MemoryStream.Size);

          DataReader.ReadBytes(InvBuffer);

          var InvResult = new Inv.Image(InvBuffer, ".png");

          InvResult.Node = null; // defer the UwaImageNode because we don't actually have a MediaImage.

          return InvResult;
        }
      }
    }
    private UwaImageRecord ImportImage(Inv.Image Image)
    {
      // can only create the UWP BitmapImage in the foreground thread.
      return ReadImage(Image).Result;
    }
    private Inv.Image ExportImage(UwaImageRecord UwaImageRecord, double DpiX, double DpiY)
    {
      return Task.Run(() => WriteImage(UwaImageRecord, DpiX, DpiY)).Result;
    }
    private Inv.Image ConvertImage(Inv.Image Image, Action<UwaImageRecord> ConvertAction)
    {
      var UwaImageRecord = ImportImage(Image);

      // custom algorithm to convert pixel data.
      ConvertAction(UwaImageRecord);

      // NOTE: this Task.Run is important to avoiding an async deadlock in the foreground thread.
      return ExportImage(UwaImageRecord, 96.0, 96.0);
    }
    private Windows.Storage.StorageFolder SelectStorageFolder(Folder Folder)
    {
      var CurrentFolder = LocalFolder;

      foreach (var ItemName in Folder.GetRelativePath())
      {
        var FolderTask = CurrentFolder.TryGetItemAsync(ItemName).AsTask();
        FolderTask.Wait();
        var NextFolder = FolderTask.Result as Windows.Storage.StorageFolder;

        if (NextFolder == null)
        {
          var CreateTask = CurrentFolder.CreateFolderAsync(ItemName).AsTask();
          CreateTask.Wait();
          NextFolder = CreateTask.Result;
        }

        CurrentFolder = NextFolder;
      }

      return CurrentFolder;
    }
    private Windows.Storage.StorageFile SelectStorageFile(File File)
    {
      var StorageFolder = SelectStorageFolder(File.Folder);

      var FileTask = StorageFolder.GetFileAsync(File.Name).AsTask();
      FileTask.Wait();
      return FileTask.Result;
    }
    private Windows.Storage.StorageFolder SelectStorageAssetFolder()
    {
      return Windows.ApplicationModel.Package.Current.InstalledLocation;
    }
    private Windows.Storage.StorageFile SelectStorageAsset(Asset Asset)
    {
      var FileTask = SelectStorageAssetFolder().GetFileAsync(System.IO.Path.Combine("Assets", Asset.Name)).AsTask();
      FileTask.Wait();
      return FileTask.Result;
    }

    private readonly UwaEngine Engine;
    private readonly UwaVault Vault;
    private readonly Windows.Storage.StorageFolder LocalFolder;
    private readonly Microsoft.Graphics.Canvas.CanvasDevice UwaCanvasDevice;
  }

  /// <summary>
  /// The bridge can be used to break the abstraction and reach into the implementation controls.
  /// This is for workarounds where Invention does not adequately handle a native scenario.
  /// </summary>
  public sealed class UwaBridge
  {
    internal UwaBridge(Windows.UI.Xaml.Controls.Page UwaPage)
    {
      this.Application = new Inv.Application();
      this.Engine = new UwaEngine(UwaPage, Application);
    }

    /// <summary>
    /// The portable application.
    /// </summary>
    public Inv.Application Application { get; private set; }

    /// <summary>
    /// Retrieve the <see cref="UwaSurface"/>
    /// </summary>
    /// <param name="InvSurface"></param>
    /// <returns></returns>
    public UwaSurface GetSurface(Inv.Surface InvSurface)
    {
      return Engine.TranslateSurface(InvSurface);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaBlock"/>
    /// </summary>
    /// <param name="InvBlock"></param>
    /// <returns></returns>
    public UwaBlock GetBlock(Inv.Block InvBlock)
    {
      return Engine.TranslateBlock(InvBlock);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaBoard"/>
    /// </summary>
    /// <param name="InvBoard"></param>
    /// <returns></returns>
    public UwaBoard GetBoard(Inv.Board InvBoard)
    {
      return Engine.TranslateBoard(InvBoard);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaBrowser"/>
    /// </summary>
    /// <param name="InvBrowser"></param>
    /// <returns></returns>
    public UwaBrowser GetBrowser(Inv.Browser InvBrowser)
    {
      return Engine.TranslateBrowser(InvBrowser);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaButton"/>
    /// </summary>
    /// <param name="InvButton"></param>
    /// <returns></returns>
    public UwaButton GetButton(Inv.Button InvButton)
    {
      return Engine.TranslateButton(InvButton);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaCanvas"/>
    /// </summary>
    /// <param name="InvCanvas"></param>
    /// <returns></returns>
    public UwaCanvas GetCanvas(Inv.Canvas InvCanvas)
    {
      return Engine.TranslateCanvas(InvCanvas);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaDock"/>
    /// </summary>
    /// <param name="InvDock"></param>
    /// <returns></returns>
    public UwaDock GetDock(Inv.Dock InvDock)
    {
      return Engine.TranslateDock(InvDock);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaEdit"/>
    /// </summary>
    /// <param name="InvEdit"></param>
    /// <returns></returns>
    public UwaEdit GetEdit(Inv.Edit InvEdit)
    {
      return Engine.TranslateEdit(InvEdit);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaFlow"/>
    /// </summary>
    /// <param name="InvFlow"></param>
    /// <returns></returns>
    public UwaFlow GetFlow(Inv.Flow InvFlow)
    {
      return Engine.TranslateFlow(InvFlow);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaFrame"/>
    /// </summary>
    /// <param name="InvFrame"></param>
    /// <returns></returns>
    public UwaFrame GetFrame(Inv.Frame InvFrame)
    {
      return Engine.TranslateFrame(InvFrame);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaGraphic"/>
    /// </summary>
    /// <param name="InvGraphic"></param>
    /// <returns></returns>
    public UwaGraphic GetGraphic(Inv.Graphic InvGraphic)
    {
      return Engine.TranslateGraphic(InvGraphic);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaLabel"/>
    /// </summary>
    /// <param name="InvLabel"></param>
    /// <returns></returns>
    public UwaLabel GetLabel(Inv.Label InvLabel)
    {
      return Engine.TranslateLabel(InvLabel);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaMemo"/>
    /// </summary>
    /// <param name="InvMemo"></param>
    /// <returns></returns>
    public UwaMemo GetMemo(Inv.Memo InvMemo)
    {
      return Engine.TranslateMemo(InvMemo);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaNative"/>
    /// </summary>
    /// <param name="InvNative"></param>
    /// <returns></returns>
    public UwaNative GetNative(Inv.Native InvNative)
    {
      return Engine.TranslateNative(InvNative);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaOverlay"/>
    /// </summary>
    /// <param name="InvOverlay"></param>
    /// <returns></returns>
    public UwaOverlay GetOverlay(Inv.Overlay InvOverlay)
    {
      return Engine.TranslateOverlay(InvOverlay);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaScroll"/>
    /// </summary>
    /// <param name="InvScroll"></param>
    /// <returns></returns>
    public UwaScroll GetScroll(Inv.Scroll InvScroll)
    {
      return Engine.TranslateScroll(InvScroll);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaShape"/>
    /// </summary>
    /// <param name="InvShape"></param>
    /// <returns></returns>
    public UwaShape GetShape(Inv.Shape InvShape)
    {
      return Engine.TranslateShape(InvShape);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaStack"/>
    /// </summary>
    /// <param name="InvStack"></param>
    /// <returns></returns>
    public UwaStack GetStack(Inv.Stack InvStack)
    {
      return Engine.TranslateStack(InvStack);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaTable"/>
    /// </summary>
    /// <param name="InvTable"></param>
    /// <returns></returns>
    public UwaTable GetTable(Inv.Table InvTable)
    {
      return Engine.TranslateTable(InvTable);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaVideo"/>
    /// </summary>
    /// <param name="InvVideo"></param>
    /// <returns></returns>
    public UwaVideo GetVideo(Inv.Video InvVideo)
    {
      return Engine.TranslateVideo(InvVideo);
    }
    /// <summary>
    /// Retrieve the <see cref="UwaWrap"/>
    /// </summary>
    /// <param name="InvWrap"></param>
    /// <returns></returns>
    public UwaWrap GetWrap(Inv.Wrap InvWrap)
    {
      return Engine.TranslateWrap(InvWrap);
    }

    internal UwaEngine Engine { get; private set; }
  }

  internal sealed class UwaEngine
  {
    public UwaEngine(Windows.UI.Xaml.Controls.Page UwaPage, Inv.Application Application)
    {
      this.UwaPage = UwaPage;
      this.InvApplication = Application;

      this.UwaMaster = new Windows.UI.Xaml.Controls.Grid();
      UwaPage.Content = UwaMaster;
      UwaMaster.Background = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);

      this.MediaBrushDictionary = new Dictionary<Colour, Windows.UI.Xaml.Media.Brush>();

      #region Key mapping.
      this.KeyDictionary = new Dictionary<Windows.System.VirtualKey, Inv.Key>()
      {
        { Windows.System.VirtualKey.Number0, Key.n0 },
        { Windows.System.VirtualKey.Number1, Key.n1 },
        { Windows.System.VirtualKey.Number2, Key.n2 },
        { Windows.System.VirtualKey.Number3, Key.n3 },
        { Windows.System.VirtualKey.Number4, Key.n4 },
        { Windows.System.VirtualKey.Number5, Key.n5 },
        { Windows.System.VirtualKey.Number6, Key.n6 },
        { Windows.System.VirtualKey.Number7, Key.n7 },
        { Windows.System.VirtualKey.Number8, Key.n8 },
        { Windows.System.VirtualKey.Number9, Key.n9 },
        { Windows.System.VirtualKey.A, Key.A },
        { Windows.System.VirtualKey.B, Key.B },
        { Windows.System.VirtualKey.C, Key.C },
        { Windows.System.VirtualKey.D, Key.D },
        { Windows.System.VirtualKey.E, Key.E },
        { Windows.System.VirtualKey.F, Key.F },
        { Windows.System.VirtualKey.G, Key.G },
        { Windows.System.VirtualKey.H, Key.H },
        { Windows.System.VirtualKey.I, Key.I },
        { Windows.System.VirtualKey.J, Key.J },
        { Windows.System.VirtualKey.K, Key.K },
        { Windows.System.VirtualKey.L, Key.L },
        { Windows.System.VirtualKey.M, Key.M },
        { Windows.System.VirtualKey.N, Key.N },
        { Windows.System.VirtualKey.O, Key.O },
        { Windows.System.VirtualKey.P, Key.P },
        { Windows.System.VirtualKey.Q, Key.Q },
        { Windows.System.VirtualKey.R, Key.R },
        { Windows.System.VirtualKey.S, Key.S },
        { Windows.System.VirtualKey.T, Key.T },
        { Windows.System.VirtualKey.U, Key.U },
        { Windows.System.VirtualKey.V, Key.V },
        { Windows.System.VirtualKey.W, Key.W },
        { Windows.System.VirtualKey.X, Key.X },
        { Windows.System.VirtualKey.Y, Key.Y },
        { Windows.System.VirtualKey.Z, Key.Z },
        { Windows.System.VirtualKey.F1, Key.F1 },
        { Windows.System.VirtualKey.F2, Key.F2 },
        { Windows.System.VirtualKey.F3, Key.F3 },
        { Windows.System.VirtualKey.F4, Key.F4 },
        { Windows.System.VirtualKey.F5, Key.F5 },
        { Windows.System.VirtualKey.F6, Key.F6 },
        { Windows.System.VirtualKey.F7, Key.F7 },
        { Windows.System.VirtualKey.F8, Key.F8 },
        { Windows.System.VirtualKey.F9, Key.F9 },
        { Windows.System.VirtualKey.F10, Key.F10 },
        { Windows.System.VirtualKey.F11, Key.F11 },
        { Windows.System.VirtualKey.F12, Key.F12 },
        { Windows.System.VirtualKey.Escape, Key.Escape },
        { Windows.System.VirtualKey.Enter, Key.Enter },
        { Windows.System.VirtualKey.Tab, Key.Tab },
        { Windows.System.VirtualKey.Space, Key.Space },
        { Windows.System.VirtualKey.Decimal, Key.Period },
        { (Windows.System.VirtualKey)186, Key.Colon },
        { (Windows.System.VirtualKey)188, Key.Comma },
        { (Windows.System.VirtualKey)192, Key.Grave },
        { (Windows.System.VirtualKey)222, Key.Quote },
        { Windows.System.VirtualKey.Add, Key.Plus },
        { Windows.System.VirtualKey.Subtract, Key.Minus },
        { Windows.System.VirtualKey.Up, Key.Up },
        { Windows.System.VirtualKey.Down, Key.Down },
        { Windows.System.VirtualKey.Left, Key.Left },
        { Windows.System.VirtualKey.Right, Key.Right },
        { Windows.System.VirtualKey.Home, Key.Home },
        { Windows.System.VirtualKey.End, Key.End },
        { Windows.System.VirtualKey.PageUp, Key.PageUp },
        { Windows.System.VirtualKey.PageDown, Key.PageDown },
        { Windows.System.VirtualKey.Clear, Key.Clear },
        { Windows.System.VirtualKey.Insert, Key.Insert },
        { Windows.System.VirtualKey.Delete, Key.Delete },
        { Windows.System.VirtualKey.Divide, Key.Slash },
        { Windows.System.VirtualKey.Multiply, Key.Asterisk },
        { Windows.System.VirtualKey.Back, Key.Backspace },
        { Windows.System.VirtualKey.LeftShift, Key.LeftShift },
        { Windows.System.VirtualKey.RightShift, Key.RightShift },
        { Windows.System.VirtualKey.LeftControl, Key.LeftCtrl },
        { Windows.System.VirtualKey.RightControl, Key.RightCtrl },
        { Windows.System.VirtualKey.LeftMenu, Key.LeftAlt },
        { Windows.System.VirtualKey.RightMenu, Key.RightAlt },
        { (Windows.System.VirtualKey)190, Key.Period },
        { (Windows.System.VirtualKey)191, Key.Slash },
        { (Windows.System.VirtualKey)219, Key.OpenBracket },
        { (Windows.System.VirtualKey)221, Key.CloseBracket },
        { (Windows.System.VirtualKey)220, Key.Backslash },
        //{ (Windows.System.VirtualKey)255,  }, // Happens when F10 key is spammed?
      };
#if DEBUG
      var MissingKeySet = Inv.Support.EnumHelper.GetEnumerable<Inv.Key>().Except(KeyDictionary.Values).ToHashSet();
      if (MissingKeySet.Count > 0)
        System.Diagnostics.Debug.WriteLine("Unhandled keys: " + MissingKeySet.Select(K => K.ToString()).AsSeparatedText(", "));
#endif
      #endregion

      this.RouteArray = new Inv.EnumArray<Inv.ControlType, Func<Inv.Control, Windows.UI.Xaml.Controls.Control>>()
      {
        { Inv.ControlType.Block, P => TranslateBlock((Inv.Block)P) },
        { Inv.ControlType.Board, P => TranslateBoard((Inv.Board)P) },
        { Inv.ControlType.Browser, P => TranslateBrowser((Inv.Browser)P) },
        { Inv.ControlType.Button, P => TranslateButton((Inv.Button)P) },
        { Inv.ControlType.Dock, P => TranslateDock((Inv.Dock)P) },
        { Inv.ControlType.Edit, P => TranslateEdit((Inv.Edit)P) },
        { Inv.ControlType.Flow, P => TranslateFlow((Inv.Flow)P) },
        { Inv.ControlType.Frame, P => TranslateFrame((Inv.Frame)P) },
        { Inv.ControlType.Graphic, P => TranslateGraphic((Inv.Graphic)P) },
        { Inv.ControlType.Label, P => TranslateLabel((Inv.Label)P) },
        { Inv.ControlType.Memo, P => TranslateMemo((Inv.Memo)P) },
        { Inv.ControlType.Overlay, P => TranslateOverlay((Inv.Overlay)P) },
        { Inv.ControlType.Canvas, P => TranslateCanvas((Inv.Canvas)P) },
        { Inv.ControlType.Scroll, P => TranslateScroll((Inv.Scroll)P) },
        { Inv.ControlType.Shape, P => TranslateShape((Inv.Shape)P) },
        { Inv.ControlType.Stack, P => TranslateStack((Inv.Stack)P) },
        { Inv.ControlType.Switch, P => TranslateSwitch((Inv.Switch)P) },
        { Inv.ControlType.Table, P => TranslateTable((Inv.Table)P) },
        { Inv.ControlType.Video, P => TranslateVideo((Inv.Video)P) },
        { Inv.ControlType.Wrap, P => TranslateWrap((Inv.Wrap)P) },
      };

      this.GamepadButtonArray = new Inv.EnumArray<Inv.ControllerButtonType, Windows.Gaming.Input.GamepadButtons>()
      {
        { Inv.ControllerButtonType.View, Windows.Gaming.Input.GamepadButtons.View },
        { Inv.ControllerButtonType.Menu, Windows.Gaming.Input.GamepadButtons.Menu },
        { Inv.ControllerButtonType.A, Windows.Gaming.Input.GamepadButtons.A },
        { Inv.ControllerButtonType.B, Windows.Gaming.Input.GamepadButtons.B },
        { Inv.ControllerButtonType.X, Windows.Gaming.Input.GamepadButtons.X },
        { Inv.ControllerButtonType.Y, Windows.Gaming.Input.GamepadButtons.Y },
        { Inv.ControllerButtonType.DPadUp, Windows.Gaming.Input.GamepadButtons.DPadUp },
        { Inv.ControllerButtonType.DPadDown, Windows.Gaming.Input.GamepadButtons.DPadDown },
        { Inv.ControllerButtonType.DPadLeft, Windows.Gaming.Input.GamepadButtons.DPadLeft },
        { Inv.ControllerButtonType.DPadRight, Windows.Gaming.Input.GamepadButtons.DPadRight },
        { Inv.ControllerButtonType.LeftShoulder, Windows.Gaming.Input.GamepadButtons.LeftShoulder },
        { Inv.ControllerButtonType.RightShoulder, Windows.Gaming.Input.GamepadButtons.RightShoulder },
        { Inv.ControllerButtonType.LeftThumbstick, Windows.Gaming.Input.GamepadButtons.LeftThumbstick },
        { Inv.ControllerButtonType.RightThumbstick, Windows.Gaming.Input.GamepadButtons.RightThumbstick },
      };

      InvApplication.SetPlatform(new UwaPlatform(this));

      Resize();

      // TODO: unique hardware identifier: Windows.System.Profile.HardwareIdentification.GetPackageSpecificToken(null);

      InvApplication.Version = UwaShell.GetPackageVersion();

      InvApplication.Directory.Installation = Windows.ApplicationModel.Package.Current.Id.Name;

      InvApplication.Device.Target = Inv.DeviceTarget.UniversalWindows;
      InvApplication.Device.Theme = GetOperatingSystemTheme();

      var DeviceInfo = new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation();

      var DeviceFamilyVersion = Windows.System.Profile.AnalyticsInfo.VersionInfo.DeviceFamilyVersion;

      ulong version = ulong.Parse(DeviceFamilyVersion);
      ulong major = (version & 0xFFFF000000000000L) >> 48;
      ulong minor = (version & 0x0000FFFF00000000L) >> 32;
      ulong build = (version & 0x00000000FFFF0000L) >> 16;
      ulong revision = (version & 0x000000000000FFFFL);
      var osVersion = $"{major}.{minor}.{build}.{revision}";

      InvApplication.Device.Name = DeviceInfo.FriendlyName;
      InvApplication.Device.Model = DeviceInfo.SystemProductName.Trim();
      InvApplication.Device.Manufacturer = DeviceInfo.SystemManufacturer.Trim();
      InvApplication.Device.System = DeviceInfo.OperatingSystem + " " + osVersion;

      InvApplication.Device.Keyboard = true; // TODO: ?
      InvApplication.Device.Mouse = true; // TODO: ?
      InvApplication.Device.Touch = true; // TODO: ?
      InvApplication.Device.ProportionalFontName = Windows.UI.Xaml.Media.FontFamily.XamlAutoFontFamily.Source;
      InvApplication.Device.MonospacedFontName = "Consolas";
      InvApplication.Device.PixelDensity = 1.0F; // TODO: get actual density.

      InvApplication.Process.Id = (int)Windows.System.Diagnostics.ProcessDiagnosticInfo.GetForCurrentProcess().ProcessId;

      InvApplication.Window.NativePanelType = typeof(Windows.UI.Xaml.Controls.Control);

      try
      {
        var VibrationAccessTask = Windows.Devices.Haptics.VibrationDevice.RequestAccessAsync().AsTask();
        VibrationAccessTask.Wait();

        if (VibrationAccessTask.Result == Windows.Devices.Haptics.VibrationAccessStatus.Allowed)
        {
          var VibrationDeviceTask = Windows.Devices.Haptics.VibrationDevice.GetDefaultAsync().AsTask();
          VibrationDeviceTask.Wait();

          this.UwaVibrationDevice = VibrationDeviceTask.Result;
        }
        else
        {
          this.UwaVibrationDevice = null;
        }

        InvApplication.Haptics.IsSupported = UwaVibrationDevice != null;
      }
      catch
      {
        // haptics not supported.
        InvApplication.Haptics.IsSupported = false;
      }
    }

    private void VisibilityChanged(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.VisibilityChangedEventArgs Event)
    {
      if (!Event.Visible)
      {
        Debug.WriteLine("Window hidden.");
        // TODO: this can be used to trap 'shutdown' but will also be fired on minimise and alt+tab.
        //InvApplication.StopInvoke();
      }
    }
    private void KeyDown(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.KeyEventArgs Event)
    {
      // ignore keystrokes while transitioning.
      if (!IsTransitioning)
      {
        Guard(() =>
        {
          var Key = Event.VirtualKey;

          // ignore all gamepad keys because they are handled by the Inv Controller API.
          switch (Key)
          {
            case Windows.System.VirtualKey.GamepadDPadDown:
            case Windows.System.VirtualKey.GamepadDPadUp:
            case Windows.System.VirtualKey.GamepadDPadLeft:
            case Windows.System.VirtualKey.GamepadDPadRight:
            case Windows.System.VirtualKey.GamepadA:
            case Windows.System.VirtualKey.GamepadB:
            case Windows.System.VirtualKey.GamepadX:
            case Windows.System.VirtualKey.GamepadY:
            case Windows.System.VirtualKey.GamepadLeftShoulder:
            case Windows.System.VirtualKey.GamepadRightShoulder:
            case Windows.System.VirtualKey.GamepadView:
              Key = Windows.System.VirtualKey.None;
              break;
          }

          var Modifier = GetModifier();
          InvApplication.Keyboard.CheckModifier(Modifier);

          if (Key != Windows.System.VirtualKey.None)
            KeyPress(Key, Modifier);
        });
      }

      Event.Handled = true;
    }
    private void KeyUp(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.KeyEventArgs Event)
    {
      var Modifier = GetModifier();
      InvApplication.Keyboard.CheckModifier(Modifier);

      Event.Handled = true;
    }
    private void AcceleratorKeyActivated(Windows.UI.Core.CoreDispatcher Sender, Windows.UI.Core.AcceleratorKeyEventArgs Event)
    {
      Guard(() =>
      {
        var Modifier = GetModifier();
        InvApplication.Keyboard.CheckModifier(Modifier);

        var EventType = Event.EventType;
        var VirtualKey = Event.VirtualKey;
        var KeyStatus = Event.KeyStatus;

        var IsSystemKey = (EventType == Windows.UI.Core.CoreAcceleratorKeyEventType.KeyDown || EventType == Windows.UI.Core.CoreAcceleratorKeyEventType.SystemKeyDown) &&
          (VirtualKey == Windows.System.VirtualKey.Tab || VirtualKey == Windows.System.VirtualKey.Menu || VirtualKey == Windows.System.VirtualKey.LeftMenu || VirtualKey == Windows.System.VirtualKey.RightMenu);

        var IsFunctionKey = !KeyStatus.IsMenuKeyDown && VirtualKey >= Windows.System.VirtualKey.F1 && VirtualKey <= Windows.System.VirtualKey.F12 && EventType == Windows.UI.Core.CoreAcceleratorKeyEventType.SystemKeyDown;

        if (IsSystemKey || IsFunctionKey)
        {
          // ignore keystrokes while transitioning.
          if (UwaMaster.Children.Count == 1)
            KeyPress(VirtualKey, Modifier);

          Event.Handled = true;
        }
        else if (KeyStatus.IsMenuKeyDown && EventType == Windows.UI.Core.CoreAcceleratorKeyEventType.SystemKeyDown && VirtualKey >= Windows.System.VirtualKey.F4)
        {
          // we can prevent the closing of the app from Alt+F4?
          Event.Handled = !InvApplication.ExitQueryInvoke();
        }

        Debug.WriteLine(VirtualKey);
      });
    }
    private void KeyPress(Windows.System.VirtualKey VirtualKey, Inv.KeyModifier Modifier)
    {
      var InvKey = TranslateKey(VirtualKey);
      if (InvKey != null)
      {
        InvApplication.Keyboard.KeyPress(new Inv.Keystroke(InvKey.Value, Modifier));

        Process();
      }
    }
    private void GamepadAdded(object Sender, Windows.Gaming.Input.Gamepad Gamepad)
    {
      InvApplication.Window.Post(() => ResolveController(Gamepad));
    }
    private void GamepadRemoved(object Sender, Windows.Gaming.Input.Gamepad Gamepad)
    {
      InvApplication.Window.Post(() =>
      {
        var Controller = InvApplication.Device.FindController(Gamepad);

        if (Controller != null)
          InvApplication.Device.RemoveController(Controller);
      });
    }

    public void Start()
    {
      System.Threading.Tasks.TaskScheduler.UnobservedTaskException += UnobservedTaskException;

      Windows.ApplicationModel.Core.CoreApplication.UnhandledErrorDetected += UnhandledErrorDetected;
      Windows.ApplicationModel.Core.CoreApplication.Exiting += Exiting;
      Windows.ApplicationModel.Core.CoreApplication.Suspending += Suspending;
      Windows.ApplicationModel.Core.CoreApplication.Resuming += Resuming;

      this.CoreWindow = Windows.UI.Core.CoreWindow.GetForCurrentThread();
      CoreWindow.KeyDown += KeyDown;
      CoreWindow.KeyUp += KeyUp;
      CoreWindow.Closed += Closed;
      CoreWindow.SizeChanged += SizeChanged;
      CoreWindow.PointerPressed += PointerPressed;
      CoreWindow.PointerReleased += PointerReleased;
      CoreWindow.PointerMoved += PointerMoved;
      CoreWindow.VisibilityChanged += VisibilityChanged;

      this.CoreDispatcher = Windows.ApplicationModel.Core.CoreApplication.GetCurrentView().Dispatcher;
      CoreDispatcher.AcceleratorKeyActivated += AcceleratorKeyActivated;

      Windows.Gaming.Input.Gamepad.GamepadAdded += GamepadAdded;
      Windows.Gaming.Input.Gamepad.GamepadRemoved += GamepadRemoved;

      // NOTE: this put a special back arrow button in the top left corner of the app.
      /* 
      var NavigationManager = Windows.UI.Core.SystemNavigationManager.GetForCurrentView();
      NavigationManager.AppViewBackButtonVisibility = Windows.UI.Core.AppViewBackButtonVisibility.Visible;
      NavigationManager.BackRequested += (Sender, Event) =>
      {
        if (!IsTransitioning)
        {
          var InvSurface = InvApplication.Window.ActiveSurface;
          if (InvSurface != null)
            InvSurface.GestureBackwardInvoke();
        }
      };
      */

      try
      {
        InvApplication.StartInvoke();
      }
      catch (Exception Exception)
      {
        HandleException(Exception);

        // NOTE: can't let the application start if the StartEvent failed so have the application crash instead.
        throw Exception.Preserve();
      }

      // ensure first process before the rendering loop kicks on.
      Guard(() => Process());

      Windows.UI.Xaml.Media.CompositionTarget.Rendering += Rendering;

      // silently sign into Xbox Live (if already done), but only require user interaction if we are on an Xbox.
      if (UwaShell.XboxLive.AutoSignIn)
        UwaShell.XboxLive.Initialise(UserInteraction: UwaShell.IsXbox);
    }
    public void Stop()
    {
      // NOTE: InvApplication.StopInvoke() is called by Exiting.

      Windows.UI.Xaml.Media.CompositionTarget.Rendering -= Rendering;

      Windows.Gaming.Input.Gamepad.GamepadAdded -= GamepadAdded;
      Windows.Gaming.Input.Gamepad.GamepadRemoved -= GamepadRemoved;

      Windows.ApplicationModel.Core.CoreApplication.Suspending -= Suspending;
      Windows.ApplicationModel.Core.CoreApplication.Resuming -= Resuming;
      Windows.ApplicationModel.Core.CoreApplication.Exiting -= Exiting;
      Windows.ApplicationModel.Core.CoreApplication.UnhandledErrorDetected -= UnhandledErrorDetected;

      if (CoreDispatcher != null)
        CoreDispatcher.AcceleratorKeyActivated -= AcceleratorKeyActivated;

      if (CoreWindow != null)
      {
        CoreWindow.Closed -= Closed;
        CoreWindow.KeyDown -= KeyDown;
        CoreWindow.SizeChanged -= SizeChanged;
        CoreWindow.PointerPressed -= PointerPressed;
        CoreWindow.PointerReleased -= PointerReleased;
        CoreWindow.PointerMoved -= PointerMoved;
        CoreWindow.VisibilityChanged -= VisibilityChanged;
      }

      System.Threading.Tasks.TaskScheduler.UnobservedTaskException -= UnobservedTaskException;
    }

    internal Application InvApplication { get; }
    internal Windows.UI.Core.CoreWindow CoreWindow { get; private set; }
    internal Windows.UI.Core.CoreDispatcher CoreDispatcher { get; private set; }
    internal bool IsTransitioning => TransitionCount > 0;
    internal bool IsInputBlocked => IsTransitioning || (InvApplication?.Window.InputPrevented ?? false);

    internal KeyModifier GetModifier()
    {
      return new KeyModifier()
      {
        IsLeftShift = (CoreWindow.GetKeyState(Windows.System.VirtualKey.LeftShift) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down,
        IsRightShift = (CoreWindow.GetKeyState(Windows.System.VirtualKey.RightShift) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down,
        IsLeftAlt = (CoreWindow.GetKeyState(Windows.System.VirtualKey.LeftMenu) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down,
        IsRightAlt = (CoreWindow.GetKeyState(Windows.System.VirtualKey.RightMenu) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down,
        IsLeftCtrl = (CoreWindow.GetKeyState(Windows.System.VirtualKey.LeftControl) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down,
        IsRightCtrl = (CoreWindow.GetKeyState(Windows.System.VirtualKey.RightControl) & Windows.UI.Core.CoreVirtualKeyStates.Down) == Windows.UI.Core.CoreVirtualKeyStates.Down
      };
    }
    internal void ReclaimImage(IReadOnlyList<Inv.Image> ImageList)
    {
      foreach (var Image in ImageList)
      {
        var UwaImageNode = Image.Node as UwaImageNode;
        Image.Node = null;
        UwaImageNode?.CanvasBitmap?.Dispose();
        // NOTE: UwaImageNode.MediaImage does not need disposal.
      }
    }
    internal void ReclaimSound(IReadOnlyList<Inv.Sound> SoundList)
    {
      // TODO: not implemented.
    }
    internal void ShowCalendarPicker(CalendarPicker CalendarPicker)
    {
      var UwaOverlay = new UwaOverlay();
      UwaMaster.Children.Add(UwaOverlay);
      UwaOverlay.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
      UwaOverlay.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;

      var UwaShadeBrush = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black) { Opacity = 0.50 };

      var UwaShade = new UwaButton();
      UwaOverlay.AddChild(UwaShade);
      UwaShade.NormalBackgroundBrush = UwaShadeBrush;
      UwaShade.HoverBackgroundBrush = UwaShadeBrush;
      UwaShade.PressedBackgroundBrush = UwaShadeBrush;
      UwaShade.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
      UwaShade.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
      UwaShade.Click += (Sender, Event) =>
      {
        UwaMaster.Children.Remove(UwaOverlay);
        CalendarPicker.CancelInvoke();
      };

      var UwaFrame = new Windows.UI.Xaml.Controls.Border();
      UwaOverlay.AddChild(UwaFrame);
      UwaFrame.Background = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.White);
      UwaFrame.Padding = new Windows.UI.Xaml.Thickness(10);
      UwaFrame.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
      UwaFrame.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;

      var UwaStack = new Windows.UI.Xaml.Controls.StackPanel();
      UwaFrame.Child = UwaStack;
      UwaStack.Orientation = Windows.UI.Xaml.Controls.Orientation.Vertical;

      var TitleLabel = new UwaLabel();
      UwaStack.Children.Add(TitleLabel);
      TitleLabel.Border.Margin = new Windows.UI.Xaml.Thickness(10);
      TitleLabel.TextBlock.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);
      TitleLabel.TextBlock.FontSize = 30;
      TitleLabel.TextBlock.TextAlignment = Windows.UI.Xaml.TextAlignment.Center;
      TitleLabel.TextBlock.Text = "Select" + (CalendarPicker.SetDate ? " Date" : "") + (CalendarPicker.SetTime ? " Time" : "");

      var AcceptButton = new UwaButton();

      var SelectedValue = CalendarPicker.Value.Date + CalendarPicker.Value.TimeOfDay.TruncateSeconds();

      if (CalendarPicker.SetDate)
      {
        var UwaCalendar = new Windows.UI.Xaml.Controls.CalendarView();
        UwaStack.Children.Add(UwaCalendar);
        UwaCalendar.SelectionMode = Windows.UI.Xaml.Controls.CalendarViewSelectionMode.Single;
        UwaCalendar.SelectedDates.Add(SelectedValue);
        UwaCalendar.DisplayMode = Windows.UI.Xaml.Controls.CalendarViewDisplayMode.Month;
        UwaCalendar.SetDisplayDate(SelectedValue);
        UwaCalendar.IsTodayHighlighted = true;
        UwaCalendar.SelectedDatesChanged += (Sender, Event) =>
        {
          if (UwaCalendar.SelectedDates.Count == 1)
            SelectedValue = UwaCalendar.SelectedDates[0].Date + SelectedValue.TimeOfDay.TruncateSeconds();
        };
      }

      if (CalendarPicker.SetTime)
      {
        var UwaTimeEdit = new UwaEdit(false, false);
        UwaStack.Children.Add(UwaTimeEdit);
        if (CalendarPicker.SetDate)
          UwaTimeEdit.Border.Margin = new Windows.UI.Xaml.Thickness(0, 10, 0, 0);
        UwaTimeEdit.Border.BorderBrush = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.LightGray);
        UwaTimeEdit.Border.BorderThickness = new Windows.UI.Xaml.Thickness(1);
        UwaTimeEdit.Border.Background = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.WhiteSmoke);
        UwaTimeEdit.Border.Padding = new Windows.UI.Xaml.Thickness(10);
        UwaTimeEdit.TextBox.FontSize = 20;
        UwaTimeEdit.TextBox.TextAlignment = Windows.UI.Xaml.TextAlignment.Center;
        UwaTimeEdit.TextBox.IsReadOnly = false;
        UwaTimeEdit.TextBox.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);
        UwaTimeEdit.TextBox.Text = CalendarPicker.Value.ToString("HH:mm");
        UwaTimeEdit.TextBox.TextChanged += (Sender, Event) =>
        {
          if (DateTime.TryParseExact(UwaTimeEdit.TextBox.Text, "H:mm", null, System.Globalization.DateTimeStyles.AssumeLocal, out DateTime Time))
          {
            SelectedValue = SelectedValue.Date + Time.TimeOfDay.TruncateSeconds();

            UwaTimeEdit.TextBox.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);
            AcceptButton.IsEnabled = true;
          }
          else
          {
            UwaTimeEdit.TextBox.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Red);
            AcceptButton.IsEnabled = false;
          }
        };
      }

      var ButtonDock = new UwaDock();
      UwaStack.Children.Add(ButtonDock);
      ButtonDock.Border.Margin = new Windows.UI.Xaml.Thickness(0, 10, 0, 0);

      AcceptButton.Border.Margin = new Windows.UI.Xaml.Thickness(0, 0, 5, 0);
      AcceptButton.NormalBackgroundBrush = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.ForestGreen);
      AcceptButton.HoverBackgroundBrush = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Green);
      AcceptButton.PressedBackgroundBrush = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.DarkGreen);
      AcceptButton.Border.Padding = new Windows.UI.Xaml.Thickness(10);
      AcceptButton.Click += (Sender, Event) =>
      {
        UwaMaster.Children.Remove(UwaOverlay);

        CalendarPicker.Value = SelectedValue;
        CalendarPicker.SelectInvoke();
      };

      var AcceptLabel = new UwaLabel();
      AcceptButton.Border.Child = AcceptLabel;
      AcceptLabel.TextBlock.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.White);
      AcceptLabel.TextBlock.FontSize = 20;
      AcceptLabel.TextBlock.TextAlignment = Windows.UI.Xaml.TextAlignment.Center;
      AcceptLabel.TextBlock.Text = "ACCEPT";

      var CancelButton = new UwaButton();
      CancelButton.Border.Margin = new Windows.UI.Xaml.Thickness(5, 0, 0, 0);
      CancelButton.NormalBackgroundBrush = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.LightGray);
      CancelButton.HoverBackgroundBrush = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.WhiteSmoke);
      CancelButton.PressedBackgroundBrush = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.DarkGray);
      CancelButton.Border.Padding = new Windows.UI.Xaml.Thickness(10);
      CancelButton.Click += (Sender, Event) =>
      {
        UwaMaster.Children.Remove(UwaOverlay);
        CalendarPicker.CancelInvoke();
      };

      var CancelLabel = new UwaLabel();
      CancelButton.Border.Child = CancelLabel;
      CancelLabel.TextBlock.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);
      CancelLabel.TextBlock.FontSize = 20;
      CancelLabel.TextBlock.TextAlignment = Windows.UI.Xaml.TextAlignment.Center;
      CancelLabel.TextBlock.Text = "CANCEL";

      ButtonDock.IsHorizontal = true;
      ButtonDock.Compose(Array.Empty<Windows.UI.Xaml.Controls.Control>(), new Windows.UI.Xaml.Controls.Control[] { AcceptButton, CancelButton }, Array.Empty<Windows.UI.Xaml.Controls.Control>());
    }
    internal void Post(Action Action)
    {
      CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => Guard(Action)).AsTask().Forget();
    }
    internal void Call(Action Action)
    {
      CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => Guard(Action)).AsTask().Wait();
    }
    internal Microsoft.Graphics.Canvas.Brushes.ICanvasBrush TranslateCanvasBrush(Microsoft.Graphics.Canvas.CanvasDrawingSession DS, Inv.Colour InvColour)
    {
      if (InvColour == null)
      {
        return null;
      }
      else
      {
        var Result = InvColour.Node as Microsoft.Graphics.Canvas.Brushes.CanvasSolidColorBrush;

        if (Result == null || Result.Device != DS.Device)
        {
          try
          {
            Result?.Dispose();
          }
          catch (Exception Exception)
          {
            HandleException(Exception);
          }

          Result = new Microsoft.Graphics.Canvas.Brushes.CanvasSolidColorBrush(DS, TranslateMediaColour(InvColour));
          InvColour.Node = Result;
        }

        return Result;
      }
    }
    internal Windows.UI.Color TranslateMediaColour(Inv.Colour Colour)
    {
      var ArgbArray = BitConverter.GetBytes(Colour.RawValue);

      return Windows.UI.Color.FromArgb(ArgbArray[3], ArgbArray[2], ArgbArray[1], ArgbArray[0]);
    }
    internal Windows.UI.Text.FontWeight TranslateFontWeight(FontWeight InvFontWeight)
    {
      return InvFontWeight switch
      {
        FontWeight.Thin => Windows.UI.Text.FontWeights.Thin,
        FontWeight.Light => Windows.UI.Text.FontWeights.Light,
        FontWeight.Regular => Windows.UI.Text.FontWeights.Normal,
        FontWeight.Medium => Windows.UI.Text.FontWeights.Medium,
        FontWeight.Bold => Windows.UI.Text.FontWeights.Bold,
        FontWeight.Heavy => Windows.UI.Text.FontWeights.Black,
        _ => throw new Exception("FontWeight not handled: " + InvFontWeight),
      };
    }
    internal Microsoft.Graphics.Canvas.CanvasBitmap TranslateCanvasBitmap(Microsoft.Graphics.Canvas.CanvasDrawingSession DS, Inv.Image InvImage)
    {
      if (InvImage == null)
      {
        return null;
      }
      else
      {
        var Result = InvImage.Node as UwaImageNode;

        if (Result == null || Result.CanvasBitmap == null || Result.CanvasBitmap.Device != DS.Device)
        {
          Microsoft.Graphics.Canvas.CanvasBitmap CanvasBitmap;

          using (var MemoryStream = new Windows.Storage.Streams.InMemoryRandomAccessStream())
          {
            using (var DataWriter = new Windows.Storage.Streams.DataWriter(MemoryStream))
            {
              // Write the bytes to the stream
              DataWriter.WriteBytes(InvImage.GetBuffer());

              // Store the bytes to the MemoryStream
              DataWriter.StoreAsync().AsTask().Wait();

              // Not necessary, but do it anyway
              DataWriter.FlushAsync().AsTask().Wait();

              // Detach from the Memory stream so we don't close it
              DataWriter.DetachStream();
            }

            MemoryStream.Seek(0);

            var LoadTask = Microsoft.Graphics.Canvas.CanvasBitmap.LoadAsync(DS, MemoryStream, dpi: 96).AsTask();
            LoadTask.Wait();

            CanvasBitmap = LoadTask.Result;
          }

          try
          {
            Result?.CanvasBitmap?.Dispose(); // dispose the previous one.
          }
          catch (Exception Exception)
          {
            HandleException(Exception);
          }

          Result = new UwaImageNode(Result?.MediaImage, CanvasBitmap);
          InvImage.Node = Result;
        }

        return Result.CanvasBitmap;
      }
    }
    internal Windows.UI.Xaml.Media.Imaging.BitmapImage TranslateMediaImage(Inv.Image InvImage)
    {
      if (InvImage == null)
      {
        return null;
      }
      else
      {
        var Result = InvImage.Node as UwaImageNode;

        if (Result == null || Result.MediaImage == null)
        {
          var MediaImage = LoadMediaImage(InvImage);

          Result = new UwaImageNode(MediaImage, Result?.CanvasBitmap);
          InvImage.Node = Result;
        }

        return Result.MediaImage;
      }
    }
    internal void HandleException(Exception Exception)
    {
      var Application = InvApplication;
      if (Application != null)
      {
        if (Exception is UwaAddChildException)
          Debug.WriteLine(Exception.Message);
        else
          Application.HandleExceptionInvoke(Exception);
      }
    }
    internal void Guard(Action Action)
    {
      try
      {
        Action();
      }
      catch (Exception Exception)
      {
        HandleException(Exception);
      }
    }
    internal T Guard<T>(Func<T> Action)
    {
      try
      {
        return Action();
      }
      catch (Exception Exception)
      {
        HandleException(Exception);

        return default;
      }
    }
    internal Windows.UI.Xaml.Controls.Control GetPanel(Inv.Panel InvPanel)
    {
      return RoutePanel(InvPanel);
    }
    internal UwaSurface TranslateSurface(Inv.Surface InvSurface)
    {
      if (InvSurface.Node == null)
      {
        var Result = new UwaSurface();

        InvSurface.Node = Result;

        return Result;
      }
      else
      {
        return (UwaSurface)InvSurface.Node;
      }
    }
    internal UwaBlock TranslateBlock(Inv.Block InvBlock)
    {
      return TranslatePanel(InvBlock, () =>
      {
        var Result = new UwaBlock();
        return Result;
      }, UwaBlock =>
      {
        var UwaTextBlock = UwaBlock.TextBlock;

        TranslateLayout(InvBlock, UwaBlock);
        TranslateFont(InvBlock.Font, UwaTextBlock);
        TranslateTooltip(InvBlock.Tooltip, UwaTextBlock);

        UwaBlock.TextBlock.TextWrapping = InvBlock.LineWrapping ? Windows.UI.Xaml.TextWrapping.Wrap : Windows.UI.Xaml.TextWrapping.NoWrap;
        UwaBlock.TextBlock.TextAlignment = TranslateJustify(InvBlock.Justify);

        if (InvBlock.SpanCollection.Render())
        {
          // TODO: do we have to always recreate the entire textblock?
          UwaTextBlock.Inlines.Clear();

          foreach (var InvSpan in InvBlock.SpanCollection)
          {
            Windows.UI.Xaml.Documents.Inline UwaInline;

            switch (InvSpan.Style)
            {
              case BlockStyle.Run:
                UwaInline = new Windows.UI.Xaml.Documents.Run() { Text = InvSpan.Text };
                break;

              case BlockStyle.Break:
                UwaInline = new Windows.UI.Xaml.Documents.LineBreak();
                break;

              default:
                throw new Exception("Inv.BlockStyle not handled: " + InvSpan.Style);
            }

            InvSpan.Background.Render();
            //UwaInline.Background = TranslateMediaColour(InvSpan.Background.Colour); NOTE: UWP does not support background colors on an Inline.

            TranslateFont(InvSpan.Font, UwaInline);

            UwaTextBlock.Inlines.Add(UwaInline);
          }
        }
      });
    }
    internal UwaBoard TranslateBoard(Inv.Board InvBoard)
    {
      return TranslatePanel(InvBoard, () =>
      {
        var Result = new UwaBoard();
        return Result;
      }, UwaBoard =>
      {
        TranslateLayout(InvBoard, UwaBoard);

        if (InvBoard.PinCollection.Render())
        {
          // detach previous pinned panels (to workaround bug in UWP framework when you reuse panels: 'Value does not fall within the expected range.').
          if (UwaBoard.Canvas.Children.Count > 0)
          {
            foreach (var UwaElement in UwaBoard.Canvas.Children.Cast<Windows.UI.Xaml.Controls.Border>())
              UwaElement.Child = null;

            UwaBoard.Canvas.Children.Clear();
          }

          foreach (var InvPin in InvBoard.PinCollection)
          {
            var UwaElement = new Windows.UI.Xaml.Controls.Border();
            UwaBoard.Canvas.Children.Add(UwaElement);
            Windows.UI.Xaml.Controls.Canvas.SetLeft(UwaElement, InvPin.Rect.Left);
            Windows.UI.Xaml.Controls.Canvas.SetTop(UwaElement, InvPin.Rect.Top);
            UwaElement.Width = InvPin.Rect.Width;
            UwaElement.Height = InvPin.Rect.Height;

            var UwaPanel = RoutePanel(InvPin.Panel);
            UwaElement.SafeSetChild(UwaPanel);
          }
        }
      });
    }
    internal UwaBrowser TranslateBrowser(Inv.Browser InvBrowser)
    {
      return TranslatePanel(InvBrowser, () =>
      {
        var Result = new UwaBrowser();

        // TODO: there doesn't seem to be a way to capture the mouse back/forward button clicks and use the surface gesture backward/forward methods instead.

        Result.BlockQuery += (Uri) =>
        {
          var Fetch = new Inv.BrowserFetch(Uri);
          Guard(() => InvBrowser.FetchInvoke(Fetch));
          return Fetch.IsCancelled;
        };
        Result.ReadyEvent += (Uri) => Guard(() => InvBrowser.ReadyInvoke(new Inv.BrowserReady(Uri)));

        return Result;
      }, UwaBrowser =>
      {
        TranslateLayout(InvBrowser, UwaBrowser);

        var Navigate = InvBrowser.UriSingleton.Render();
        if (InvBrowser.HtmlSingleton.Render())
          Navigate = true;

        if (Navigate)
          UwaBrowser.Navigate(InvBrowser.UriSingleton.Data, InvBrowser.HtmlSingleton.Data);
      });
    }
    internal UwaButton TranslateButton(Inv.Button InvButton)
    {
      var IsFlat = InvButton.Style == Inv.ButtonStyle.Flat;

      return TranslatePanel(InvButton, () =>
      {
        var Result = new UwaButton();
        Result.IsFlat = IsFlat;
        Result.GotFocus += (Sender, Event) =>
        {
          Guard(() => InvButton.Focus.GotInvoke());
        };
        Result.LostFocus += (Sender, Event) =>
        {
          Guard(() => InvButton.Focus.LostInvoke());
        };
        Result.HoverOverEvent += () =>
        {
          Guard(() => InvButton.OverInvoke());
        };
        Result.HoverAwayEvent += () =>
        {
          Guard(() => InvButton.AwayInvoke());
        };
        Result.LeftClickEvent += () =>
        {
          Guard(() =>
          {
            if (!IsInputBlocked)
              InvButton.SingleTapInvoke();
          });
        };
        Result.RightClickEvent += () =>
        {
          Guard(() =>
          {
            if (!IsInputBlocked)
              InvButton.ContextTapInvoke();
          });
        };
        Result.PressEvent += () =>
        {
          Guard(() =>
          {
            if (!IsInputBlocked)
              InvButton.PressInvoke();
          });
        };
        Result.ReleaseEvent += () =>
        {
          Guard(() =>
          {
            if (!IsInputBlocked)
              InvButton.ReleaseInvoke();
          });
        };

        return Result;
      }, UwaButton =>
      {
        var IsBackgroundChanged = IsFlat && InvButton.Background.IsChanged;

        TranslateLayout(InvButton, UwaButton, UwaButton.Border);
        TranslateFocus(InvButton.Focus, UwaButton);
        TranslateTooltip(InvButton.Tooltip, UwaButton);
        TranslateHint(UwaButton, InvButton.Hint);

        UwaButton.IsEnabled = InvButton.IsEnabled;
        UwaButton.IsTabStop = InvButton.IsFocusable; // TODO: we don't want tab stops when the button is disabled? Using IsTabStop doesn't seem to do what we want.
        UwaButton.AllowFocusOnInteraction = InvButton.IsFocusable;

        if (IsBackgroundChanged)
        {
          UwaButton.NormalBackgroundBrush = TranslateMediaBrush(InvButton.Background.Colour);
          UwaButton.HoverBackgroundBrush = TranslateMediaBrush(InvButton.Background.Colour.Lighten(0.25F));
          UwaButton.PressedBackgroundBrush = TranslateMediaBrush(InvButton.Background.Colour.Darken(0.25F));
          UwaButton.RefreshBackground();
        }

        if (InvButton.ContentSingleton.Render())
          UwaButton.Border.SafeSetChild(RoutePanel(InvButton.ContentSingleton.Data));
      });
    }
    internal UwaDock TranslateDock(Inv.Dock InvDock)
    {
      return TranslatePanel(InvDock, () =>
      {
        var Result = new UwaDock();
        Result.IsHorizontal = InvDock.IsHorizontal;
        return Result;
      }, UwaDock =>
      {
        TranslateLayout(InvDock, UwaDock);

        if (InvDock.IsHorizontal != UwaDock.IsHorizontal || InvDock.CollectionRender())
        {
          UwaDock.IsHorizontal = InvDock.IsHorizontal;

          UwaDock.Compose(InvDock.HeaderCollection.Select(H => RoutePanel(H)), InvDock.ClientCollection.Select(H => RoutePanel(H)), InvDock.FooterCollection.Select(H => RoutePanel(H)));
        }
      });
    }
    internal UwaEdit TranslateEdit(Inv.Edit InvEdit)
    {
      var IsPassword = InvEdit.Input == EditInput.Password;
      var IsSearch = InvEdit.Input == EditInput.Search;

      return TranslatePanel(InvEdit, () =>
      {
        var Result = new UwaEdit(IsSearch, IsPassword);

        // TODO: Uwa allowed input.
        switch (InvEdit.Input)
        {
          case EditInput.Decimal:
            break;

          case EditInput.Email:
            break;

          case EditInput.Integer:
            break;

          case EditInput.Name:
            break;

          case EditInput.Number:
            break;

          case EditInput.Password:
            break;

          case EditInput.Phone:
            break;

          case EditInput.Search:
            break;

          case EditInput.Text:
            break;

          case EditInput.Uri:
            break;

          case EditInput.Username:
            break;

          default:
            throw new Exception("EditInput not handled: " + InvEdit.Input);
        }

        if (IsSearch)
        {
          Result.SearchBox.GotFocus += (Sender, Event) => Guard(() => InvEdit.Focus.GotInvoke());
          Result.SearchBox.LostFocus += (Sender, Event) => Guard(() => InvEdit.Focus.LostInvoke());
          Result.SearchBox.TextChanged += (Sender, Event) => Guard(() => InvEdit.ChangeText(Result.SearchBox.Text));
          Result.SearchBox.KeyDown += (Sender, Event) =>
          {
            Guard(() =>
            {
              if (Event.Key == Windows.System.VirtualKey.Enter)
                InvEdit.Return();
              else if (Event.Key == Windows.System.VirtualKey.Escape)
                Result.SearchBox.Text = "";
            });
          };
        }
        else if (IsPassword)
        {
          Result.PasswordBox.GotFocus += (Sender, Event) => Guard(() => InvEdit.Focus.GotInvoke());
          Result.PasswordBox.LostFocus += (Sender, Event) => Guard(() => InvEdit.Focus.LostInvoke());
          Result.PasswordBox.PasswordChanged += (Sender, Event) => Guard(() => InvEdit.ChangeText(Result.PasswordBox.Password));
          Result.PasswordBox.KeyDown += (Sender, Event) =>
          {
            Guard(() =>
            {
              if (Event.Key == Windows.System.VirtualKey.Enter)
                InvEdit.Return();
            });
          };
        }
        else
        {
          Result.TextBox.GotFocus += (Sender, Event) => Guard(() => InvEdit.Focus.GotInvoke());
          Result.TextBox.LostFocus += (Sender, Event) => Guard(() => InvEdit.Focus.LostInvoke());
          Result.TextBox.TextChanged += (Sender, Event) => Guard(() => InvEdit.ChangeText(Result.TextBox.Text));
          Result.TextBox.KeyDown += (Sender, Event) =>
          {
            Guard(() =>
            {
              if (Event.Key == Windows.System.VirtualKey.Enter)
                InvEdit.Return();
            });
          };
        }

        return Result;
      }, UwaEdit =>
      {
        TranslateLayout(InvEdit, UwaEdit);

        if (IsSearch)
        {
          TranslateFont(InvEdit.Font, UwaEdit.SearchBox);

          UwaEdit.SearchBox.IsEnabled = !InvEdit.IsReadOnly;
          UwaEdit.SearchBox.TextAlignment = TranslateJustify(InvEdit.Justify);
          UwaEdit.SearchBox.Text = InvEdit.Text ?? "";
          UwaEdit.RefreshSearchButton();

          TranslateFocus(InvEdit.Focus, UwaEdit.SearchBox);
          TranslateHint(UwaEdit.SearchBox, InvEdit.Hint);
        }
        else if (IsPassword)
        {
          TranslateFont(InvEdit.Font, UwaEdit.PasswordBox);

          UwaEdit.PasswordBox.IsEnabled = !InvEdit.IsReadOnly;
          //UwaEdit.PasswordBox.TextAlignment = TranslateJustify(InvEdit.Justify); // TODO: not supported.
          UwaEdit.PasswordBox.Password = InvEdit.Text ?? "";

          TranslateFocus(InvEdit.Focus, UwaEdit.PasswordBox);
          TranslateHint(UwaEdit.PasswordBox, InvEdit.Hint);
        }
        else
        {
          TranslateFont(InvEdit.Font, UwaEdit.TextBox);

          UwaEdit.TextBox.IsReadOnly = InvEdit.IsReadOnly;
          UwaEdit.TextBox.TextAlignment = TranslateJustify(InvEdit.Justify);
          UwaEdit.TextBox.Text = InvEdit.Text ?? "";

          TranslateFocus(InvEdit.Focus, UwaEdit.TextBox);
          TranslateHint(UwaEdit.TextBox, InvEdit.Hint);
        }
      });
    }
    internal UwaFlow TranslateFlow(Inv.Flow InvFlow)
    {
      return TranslatePanel(InvFlow, () =>
      {
        var Result = new UwaFlow();
        Result.SectionCountQuery = () => InvFlow.SectionCount;
        Result.HeaderContentQuery = (Section) => Guard(() => RoutePanel(InvFlow.Sections[Section]?.Header));
        Result.FooterContentQuery = (Section) => Guard(() => RoutePanel(InvFlow.Sections[Section]?.Footer));
        Result.ItemCountQuery = (Section) => Guard(() => InvFlow.Sections[Section]?.ItemCount ?? 0);
        Result.ItemContentQuery = (Section, Item) => Guard(() => RoutePanel(InvFlow.Sections[Section]?.ItemInvoke(Item)));
        return Result;
      }, UwaFlow =>
      {
        TranslateLayout(InvFlow, UwaFlow);

        UwaFlow.Fixture = RoutePanel(InvFlow.Fixture);

        if (InvFlow.IsRefresh)
        {
          InvFlow.IsRefresh = false;

          // TODO: animate refresh.
        }

        if (InvFlow.IsReload)
        {
          InvFlow.IsReload = false;
          UwaFlow.Reload();
        }

        if (InvFlow.ScrollSection != null || InvFlow.ScrollIndex != null)
        {
          if (InvFlow.ScrollSection != null && InvFlow.ScrollIndex != null)
            UwaFlow.ScrollTo(InvFlow.ScrollSection.Value, InvFlow.ScrollIndex.Value);

          InvFlow.ScrollSection = null;
          InvFlow.ScrollIndex = null;
        }
      });
    }
    internal UwaGraphic TranslateGraphic(Inv.Graphic InvGraphic)
    {
      return TranslatePanel(InvGraphic, () =>
      {
        var Result = new UwaGraphic();
        return Result;
      }, UwaGraphic =>
      {
        var SizeChanged = InvGraphic.Size.IsChanged;

        TranslateLayout(InvGraphic, UwaGraphic);
        TranslateTooltip(InvGraphic.Tooltip, UwaGraphic);

        if (InvGraphic.Fit.Render())
          UwaGraphic.Image.Stretch = TranslateFit(InvGraphic.Fit.Method);

        if (InvGraphic.ImageSingleton.Render() || SizeChanged)
          UwaGraphic.Image.Source = TranslateMediaImage(InvGraphic.Image);
      });
    }
    internal UwaLabel TranslateLabel(Inv.Label InvLabel)
    {
      return TranslatePanel(InvLabel, () =>
      {
        var Result = new UwaLabel();
        return Result;
      }, UwaLabel =>
      {
        TranslateLayout(InvLabel, UwaLabel);
        TranslateFont(InvLabel.Font, UwaLabel.TextBlock);
        TranslateTooltip(InvLabel.Tooltip, UwaLabel);

        UwaLabel.TextBlock.TextWrapping = InvLabel.LineWrapping ? Windows.UI.Xaml.TextWrapping.Wrap : Windows.UI.Xaml.TextWrapping.NoWrap;
        UwaLabel.TextBlock.TextAlignment = TranslateJustify(InvLabel.Justify);
        UwaLabel.TextBlock.Text = InvLabel.Text ?? "";
      });
    }
    internal UwaMemo TranslateMemo(Inv.Memo InvMemo)
    {
      return TranslatePanel(InvMemo, () =>
      {
        var Result = new UwaMemo();
        Result.GotFocusEvent += () => Guard(() => InvMemo.Focus.GotInvoke());
        Result.LostFocusEvent += () => Guard(() => InvMemo.Focus.LostInvoke());
        Result.TextChangedEvent += () => Guard(() => InvMemo.ChangeText(Result.Text));
        return Result;
      }, UwaMemo =>
      {
        //var IsAligning = InvMemo.Alignment.IsChanged;

        TranslateLayout(InvMemo, UwaMemo);

        //var UwaControl = UwaMemo.GetControl();
        //if (IsAligning)
        //{
        // NOTE: this USED to be a workaround to get the textbox to autosize properly.
        //UwaControl.HorizontalAlignment = UwaMemo.Border.HorizontalAlignment;
        //UwaControl.VerticalAlignment = UwaMemo.Border.VerticalAlignment;
        //}

        UwaMemo.IsReadOnly = InvMemo.IsReadOnly;

        var RenderText = UwaMemo.Text != InvMemo.Text || InvMemo.Font.IsChanged;
        if (InvMemo.MarkupCollection.Render())
          RenderText = true;

        if (RenderText)
        {
          if (InvMemo.MarkupCollection.Count == 0)
          {
            UwaMemo.SetPlainText(InvMemo.Text);
            TranslateFont(InvMemo.Font, UwaMemo.GetPlainTextBox());
          }
          else
          {
            UwaMemo.SetRichText(InvMemo.Text, R =>
            {
              TranslateFont(InvMemo.Font, R);

              foreach (var Markup in InvMemo.MarkupCollection)
              {
                if (Markup.RangeList.Count > 0)
                {
                  var MarkupName = Markup.Font.Name;
                  var MarkupColour = Markup.Font.Colour != null ? TranslateMediaColour(Markup.Font.Colour) : (Windows.UI.Color?)null;
                  var MarkupSize = Markup.Font.Size;

                  foreach (var Range in Markup.RangeList)
                  {
                    // ignore the number of return characters as they skew the GetRange method.
                    var ReturnCount = 0;
                    for (var ReturnIndex = 0; ReturnIndex < Range.Index; ReturnIndex++)
                    {
                      if (InvMemo.Text[ReturnIndex] == '\r')
                        ReturnCount++;
                    }

                    var StartIndex = Range.Index - ReturnCount;
                    var EndIndex = StartIndex + Range.Count;

                    var UwaRange = R.Document.GetRange(StartIndex, EndIndex);

                    if (MarkupName != null)
                      UwaRange.CharacterFormat.Name = MarkupName;

                    if (MarkupColour != null)
                      UwaRange.CharacterFormat.ForegroundColor = MarkupColour.Value;

                    if (MarkupSize != null)
                      UwaRange.CharacterFormat.Size = MarkupSize.Value;
                  }
                }
              }
            });
          }
        }

        var PlainTextBox = UwaMemo.GetPlainTextBox();
        if (PlainTextBox != null)
        {
          TranslateFocus(InvMemo.Focus, PlainTextBox);
        }
        else
        {
          var RichEditBox = UwaMemo.GetRichEditBox();
          TranslateFocus(InvMemo.Focus, RichEditBox);
        }
      });
    }
    internal UwaOverlay TranslateOverlay(Inv.Overlay InvOverlay)
    {
      return TranslatePanel(InvOverlay, () =>
      {
        var Result = new UwaOverlay();
        return Result;
      }, UwaOverlay =>
      {
        TranslateLayout(InvOverlay, UwaOverlay);

        if (InvOverlay.PanelCollection.Render())
          UwaOverlay.Compose(InvOverlay.PanelCollection.Select(P => RoutePanel(P)));
      });
    }
    internal UwaCanvas TranslateCanvas(Inv.Canvas InvCanvas)
    {
      return TranslatePanel(InvCanvas, () =>
      {
        var Result = new UwaCanvas(this);

        var IsLeftPressed = false;
        var IsRightPressed = false;
        var LeftPressedTimestamp = DateTime.Now;
        var RightPressedTimestamp = DateTime.Now;

        Result.ManipulationMode = Windows.UI.Xaml.Input.ManipulationModes.Scale;
        Result.PointerMoved += (Sender, Event) =>
        {
          if (IsLeftPressed || IsRightPressed || InvCanvas.IsCaptured)
          {
            var MovePoint = TranslatePoint(Event.GetCurrentPoint(Result).Position);

            Guard(() => InvCanvas.MoveInvoke(new Inv.CanvasCommand(MovePoint, IsLeftPressed, IsRightPressed)));
          }
        };
        Result.PointerPressed += (Sender, Event) =>
        {
          Result.CapturePointer(Event.Pointer);

          var PressedPoint = Event.GetCurrentPoint(Result);

          if (PressedPoint.PointerDevice.PointerDeviceType != Windows.Devices.Input.PointerDeviceType.Mouse || PressedPoint.Properties.IsLeftButtonPressed)
          {
            IsLeftPressed = true;
            LeftPressedTimestamp = DateTime.Now;

            Guard(() => InvCanvas.PressInvoke(new Inv.CanvasCommand(TranslatePoint(PressedPoint.Position), LeftMouseButton: true, RightMouseButton: false)));
          }
          else if (PressedPoint.PointerDevice.PointerDeviceType == Windows.Devices.Input.PointerDeviceType.Mouse && PressedPoint.Properties.IsRightButtonPressed)
          {
            IsRightPressed = true;
            RightPressedTimestamp = DateTime.Now;

            Guard(() => InvCanvas.PressInvoke(new Inv.CanvasCommand(TranslatePoint(PressedPoint.Position), LeftMouseButton: false, RightMouseButton: true)));
          }
        };
        Result.PointerReleased += (Sender, Event) =>
        {
          Result.ReleasePointerCapture(Event.Pointer);

          var ReleasedPoint = Event.GetCurrentPoint(Result);

          var ReleasePoint = TranslatePoint(ReleasedPoint.Position);

          if (IsLeftPressed && (ReleasedPoint.PointerDevice.PointerDeviceType != Windows.Devices.Input.PointerDeviceType.Mouse || !ReleasedPoint.Properties.IsLeftButtonPressed))
          {
            IsLeftPressed = false;

            Guard(() =>
            {
              InvCanvas.ReleaseInvoke(new Inv.CanvasCommand(ReleasePoint, LeftMouseButton: true, RightMouseButton: false));

              if (DateTime.Now - LeftPressedTimestamp <= TimeSpan.FromMilliseconds(250))
                InvCanvas.SingleTapInvoke(ReleasePoint);
            });
          }
          else if (IsRightPressed && ReleasedPoint.PointerDevice.PointerDeviceType == Windows.Devices.Input.PointerDeviceType.Mouse && !ReleasedPoint.Properties.IsRightButtonPressed)
          {
            IsRightPressed = false;

            Guard(() =>
            {
              InvCanvas.ReleaseInvoke(new Inv.CanvasCommand(ReleasePoint, LeftMouseButton: false, RightMouseButton: true));

              //if (DateTime.Now - LeftPressedTimestamp <= TimeSpan.FromMilliseconds(250))
              //  InvCanvas.ContextTapInvoke(ReleasePoint);
            });
          }
        };
        Result.RightTapped += (Sender, Event) =>
        {
          var RightTappedPoint = Event.GetPosition(Result);

          Guard(() => InvCanvas.ContextTapInvoke(TranslatePoint(RightTappedPoint)));

          IsLeftPressed = false;

          Event.Handled = true;
        };
        Result.DoubleTapped += (Sender, Event) =>
        {
          var DoubleTappedPoint = TranslatePoint(Event.GetPosition(Result));

          Guard(() =>
          {
            InvCanvas.ReleaseInvoke(new Inv.CanvasCommand(DoubleTappedPoint, LeftMouseButton: true, RightMouseButton: false));
            InvCanvas.DoubleTapInvoke(DoubleTappedPoint);
          });

          IsLeftPressed = false;

          Event.Handled = true;
        };
        Result.PointerWheelChanged += (Sender, Event) =>
        {
          var WheelChangedPoint = Event.GetCurrentPoint(Result);
          var WheelZoom = new Inv.Zoom(TranslatePoint(WheelChangedPoint.Position), WheelChangedPoint.Properties.MouseWheelDelta > 0 ? +1 : -1);

          Guard(() => InvCanvas.ZoomInvoke(WheelZoom));
        };
        Result.ManipulationDelta += (Sender, Event) =>
        {
          var TouchZoom = new Inv.Zoom(TranslatePoint(Event.Position), Event.Delta.Scale > 1.0 ? +1 : -1);

          Guard(() => InvCanvas.ZoomInvoke(TouchZoom));
        };
        Result.DrawAction = () => Guard(() => InvCanvas.DrawInvoke(Result));
        Result.MeasureFunc = (Constraint) => Guard(() =>
        {
          var Measure = new Inv.CanvasMeasure(new Inv.Dimension((int)Constraint.Width, (int)Constraint.Height));

          InvCanvas.MeasureInvoke(Measure);

          if (Measure.Dimension == null)
            return (Windows.Foundation.Size?)null;

          return new Windows.Foundation.Size(Measure.Dimension.Value.Width, Measure.Dimension.Value.Height);
        });

        return Result;
      }, UwaCanvas =>
      {
        TranslateLayout(InvCanvas, UwaCanvas);

        if (InvCanvas.IsInvalidated)
          UwaCanvas.Invalidate();
      });
    }
    internal UwaScroll TranslateScroll(Inv.Scroll InvScroll)
    {
      return TranslatePanel(InvScroll, () =>
      {
        var Result = new UwaScroll();
        Result.SetOrientation(InvScroll.IsHorizontal);
        return Result;
      }, UwaScroll =>
      {
        TranslateLayout(InvScroll, UwaScroll);

        UwaScroll.SetOrientation(InvScroll.IsHorizontal);

        if (InvScroll.ContentSingleton.Render())
          UwaScroll.ScrollViewer.Content = RoutePanel(InvScroll.ContentSingleton.Data);

        var Request = InvScroll.HandleRequest();
        if (Request != null)
        {
          switch (Request.Value)
          {
            case ScrollRequest.Start:
              if (InvScroll.IsHorizontal)
                UwaScroll.ScrollViewer.ChangeView(0, null, null, disableAnimation: true);
              else
                UwaScroll.ScrollViewer.ChangeView(null, 0, null, disableAnimation: true);
              break;

            case ScrollRequest.End:
              if (InvScroll.IsHorizontal)
                UwaScroll.ScrollViewer.ChangeView(double.MaxValue, null, null, disableAnimation: true);
              else
                UwaScroll.ScrollViewer.ChangeView(null, double.MaxValue, null, disableAnimation: true);
              break;

            default:
              throw EnumHelper.UnexpectedValueException(Request.Value);
          }
        }
      });
    }
    internal UwaShape TranslateShape(Inv.Shape InvShape)
    {
      return TranslatePanel(InvShape, () =>
      {
        var Result = new UwaShape();
        return Result;
      }, UwaShape =>
      {
        TranslateLayout(InvShape, UwaShape);

        if (InvShape.Fit.Render())
          UwaShape.Stretch = TranslateFit(InvShape.Fit.Method);

        if (InvShape.Fill.Render())
          UwaShape.Fill = TranslateMediaBrush(InvShape.Fill.Colour);

        if (InvShape.Stroke.Render())
        {
          UwaShape.Stroke = TranslateMediaBrush(InvShape.Stroke.Colour);
          UwaShape.StrokeThickness = InvShape.Stroke.Thickness;
          UwaShape.StrokeLineJoin = TranslateLineJoin(InvShape.Stroke.Join);
          UwaShape.StrokeStartLineCap = TranslateLineCap(InvShape.Stroke.Cap);
          UwaShape.StrokeEndLineCap = UwaShape.StrokeStartLineCap;
          if (InvShape.Stroke.DashPattern == null)
          {
            UwaShape.StrokeDashArray = null;
          }
          else
          {
            var DoubleCollection = new Windows.UI.Xaml.Media.DoubleCollection();
            DoubleCollection.AddRange(InvShape.Stroke.DashPattern.Select(I => (double)I));
            UwaShape.StrokeDashArray = DoubleCollection;
          }
        }

        if (InvShape.FigureCollection.Render())
        {
          var UwaGroup = new Windows.UI.Xaml.Media.GeometryGroup();

          foreach (var InvFigure in InvShape.FigureCollection)
          {
            if (InvFigure.Polygon != null)
            {
              var PathArray = InvFigure.Polygon.PathArray;
              if (PathArray != null && PathArray.Length > 0)
              {
                var UwaGeometry = new Windows.UI.Xaml.Media.PathGeometry();

                var StartPoint = TranslatePoint(PathArray[0]);
                var LinePoints = PathArray.Skip(1).Select(P => TranslatePoint(P));

                var UwaSegment = new Windows.UI.Xaml.Media.PolyLineSegment();
                UwaSegment.Points.AddRange(LinePoints);

                var UwaFigure = new Windows.UI.Xaml.Media.PathFigure();
                UwaFigure.StartPoint = StartPoint;
                UwaFigure.Segments.Add(UwaSegment);
                UwaFigure.IsClosed = true;

                UwaGeometry.Figures.Add(UwaFigure);

                UwaGroup.Children.Add(UwaGeometry);
              }
            }
            else if (InvFigure.Ellipse != null)
            {
              var UwaGeometry = new Windows.UI.Xaml.Media.EllipseGeometry();

              UwaGeometry.Center = TranslatePoint(InvFigure.Ellipse.Center);
              UwaGeometry.RadiusX = InvFigure.Ellipse.Radius.X;
              UwaGeometry.RadiusY = InvFigure.Ellipse.Radius.Y;

              UwaGroup.Children.Add(UwaGeometry);
            }
            else if (InvFigure.Line != null)
            {
              var UwaGeometry = new Windows.UI.Xaml.Media.LineGeometry();

              UwaGeometry.StartPoint = TranslatePoint(InvFigure.Line.Start);
              UwaGeometry.EndPoint = TranslatePoint(InvFigure.Line.End);

              UwaGroup.Children.Add(UwaGeometry);
            }
          }

          var NotionalDimension = InvShape.GetNotionalDimension();
          UwaShape.Set(UwaGroup, NotionalDimension.Width, NotionalDimension.Height);
        }
      });
    }
    internal UwaFrame TranslateFrame(Inv.Frame InvFrame)
    {
      return TranslatePanel(InvFrame, () =>
      {
        var Result = new UwaFrame();
        return Result;
      }, UwaFrame =>
      {
        TranslateLayout(InvFrame, UwaFrame);

        if (InvFrame.ContentSingleton.Render())
        {
          var UwaToContent = RoutePanel(InvFrame.ContentSingleton.Data);

          var InvTransition = InvFrame.ActiveTransition;

          if (InvTransition == null)
          {
            UwaFrame.Grid.Children.Clear();
            UwaFrame.Grid.SafeAddChild(UwaToContent);
          }
          else if (UwaFrame.IsTransitioning)
          {
            InvFrame.ContentSingleton.Change();
          }
          else
          {
            var UwaFromContent = UwaFrame.Grid.Children.Count == 0 ? null : (Windows.UI.Xaml.Controls.Control)UwaFrame.Grid.Children[0];

            // NOTE: give the previous panel a chance to process before it is animated away.
            if (InvFrame.FromPanel != null)
            {
              RoutePanel(InvFrame.FromPanel);
              InvFrame.FromPanel = null;
            }

            if (UwaFromContent != UwaToContent)
            {
              UwaFrame.IsTransitioning = true;
              ExecuteTransition(InvTransition, UwaFrame.Grid, UwaFromContent, UwaToContent, () => UwaFrame.IsTransitioning = false);
            }

            InvFrame.ActiveTransition = null;
          }
        }
      });
    }
    internal UwaNative TranslateNative(Inv.Native InvNative)
    {
      return TranslatePanel(InvNative, () =>
      {
        var Result = new UwaNative();
        return Result;
      }, UwaNative =>
      {
        TranslateLayout(InvNative, UwaNative);

        if (InvNative.ContentSingleton.Render())
          UwaNative.Border.SafeSetChild((Windows.UI.Xaml.Controls.Control)InvNative.ContentSingleton.Data);
      });
    }
    internal UwaStack TranslateStack(Inv.Stack InvStack)
    {
      return TranslatePanel(InvStack, () =>
      {
        var Result = new UwaStack();
        Result.Orientation = InvStack.IsHorizontal ? Windows.UI.Xaml.Controls.Orientation.Horizontal : Windows.UI.Xaml.Controls.Orientation.Vertical;
        return Result;
      }, UwaStack =>
      {
        TranslateLayout(InvStack, UwaStack);

        UwaStack.Orientation = InvStack.IsHorizontal ? Windows.UI.Xaml.Controls.Orientation.Horizontal : Windows.UI.Xaml.Controls.Orientation.Vertical;

        if (InvStack.PanelCollection.Render())
          UwaStack.Compose(InvStack.PanelCollection.Select(P => RoutePanel(P)));
      });
    }
    internal UwaSwitch TranslateSwitch(Inv.Switch InvSwitch)
    {
      return TranslatePanel(InvSwitch, () =>
      {
        return new UwaSwitch();
      }, UwaSwitch =>
      {
        TranslateLayout(InvSwitch, UwaSwitch);

        UwaSwitch.IsOn = InvSwitch.IsOn;
        UwaSwitch.IsEnabled = InvSwitch.IsEnabled;
        UwaSwitch.ChangeEvent += (Sender, Event) =>
        {
          InvSwitch.ChangeChecked(UwaSwitch.IsOn);
        };
      });
    }
    internal UwaTable TranslateTable(Inv.Table InvTable)
    {
      return TranslatePanel(InvTable, () =>
      {
        var Result = new UwaTable();
        return Result;
      }, UwaTable =>
      {
        TranslateLayout(InvTable, UwaTable);

        if (InvTable.CollectionRender())
        {
          UwaTable.Grid.Children.Clear();
          UwaTable.Grid.RowDefinitions.Clear();
          UwaTable.Grid.ColumnDefinitions.Clear();

          foreach (var TableColumn in InvTable.ColumnCollection)
          {
            UwaTable.Grid.ColumnDefinitions.Add(new Windows.UI.Xaml.Controls.ColumnDefinition() { Width = TranslateTableLength(TableColumn, true) });

            var UwaColumn = RoutePanel(TableColumn.Content);

            if (UwaColumn != null)
            {
              UwaTable.AddChild(UwaColumn);

              Windows.UI.Xaml.Controls.Grid.SetColumn(UwaColumn, TableColumn.Index);
              Windows.UI.Xaml.Controls.Grid.SetRow(UwaColumn, 0);
              Windows.UI.Xaml.Controls.Grid.SetRowSpan(UwaColumn, InvTable.ColumnCollection.Count);
            }
          }

          foreach (var TableRow in InvTable.RowCollection)
          {
            UwaTable.Grid.RowDefinitions.Add(new Windows.UI.Xaml.Controls.RowDefinition() { Height = TranslateTableLength(TableRow, false) });

            var UwaRow = RoutePanel(TableRow.Content);

            if (UwaRow != null)
            {
              UwaTable.AddChild(UwaRow);

              Windows.UI.Xaml.Controls.Grid.SetRow(UwaRow, TableRow.Index);
              Windows.UI.Xaml.Controls.Grid.SetColumn(UwaRow, 0);
              Windows.UI.Xaml.Controls.Grid.SetColumnSpan(UwaRow, InvTable.ColumnCollection.Count);
            }
          }

          foreach (var TableCell in InvTable.CellCollection)
          {
            var UwaCell = RoutePanel(TableCell.Content);

            if (UwaCell != null)
            {
              UwaTable.AddChild(UwaCell);

              Windows.UI.Xaml.Controls.Grid.SetColumn(UwaCell, TableCell.Column.Index);
              Windows.UI.Xaml.Controls.Grid.SetRow(UwaCell, TableCell.Row.Index);
            }
          }
        }
      });
    }
    internal UwaVideo TranslateVideo(Inv.Video InvVideo)
    {
      return TranslatePanel(InvVideo, () =>
      {
        var Result = new UwaVideo();
        return Result;
      }, UwaVideo =>
      {
        TranslateLayout(InvVideo, UwaVideo);

        if (InvVideo.SourceSingleton.Render())
        {
          var InvSource = InvVideo.SourceSingleton.Data;

          if (InvSource?.Asset != null)
            UwaVideo.LoadUri(new Uri(System.IO.Path.Combine(Windows.ApplicationModel.Package.Current.InstalledLocation.Path, "Assets", InvSource.Asset.Name)));
          else if (InvSource?.File != null)
            UwaVideo.LoadUri(new Uri(System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, InvSource.File.Folder.Name ?? "", InvSource.File.Name)));
          else if (InvSource?.Uri != null)
            UwaVideo.LoadUri(InvSource.Uri);
          else
            UwaVideo.LoadUri(null);
        }

        if (InvVideo.StateSingleton.Render())
        {
          switch (InvVideo.StateSingleton.Data)
          {
            case VideoState.Stop:
              UwaVideo.Stop();
              break;

            case VideoState.Pause:
              UwaVideo.Pause();
              break;

            case VideoState.Play:
              UwaVideo.Play();
              break;

            case VideoState.Restart:
              UwaVideo.Stop();
              UwaVideo.Play();
              break;

            default:
              throw new Exception("VideoState not handled: " + InvVideo.StateSingleton.Data);
          }
        }
      });
    }
    internal UwaWrap TranslateWrap(Inv.Wrap InvWrap)
    {
      return TranslatePanel(InvWrap, () =>
      {
        var Result = new UwaWrap();
        Result.WrapPanel.Orientation = InvWrap.IsHorizontal ? Windows.UI.Xaml.Controls.Orientation.Horizontal : Windows.UI.Xaml.Controls.Orientation.Vertical;
        return Result;
      }, UwaWrap =>
      {
        TranslateLayout(InvWrap, UwaWrap);

        UwaWrap.WrapPanel.Orientation = InvWrap.IsHorizontal ? Windows.UI.Xaml.Controls.Orientation.Horizontal : Windows.UI.Xaml.Controls.Orientation.Vertical;

        if (InvWrap.PanelCollection.Render())
          UwaWrap.Compose(InvWrap.PanelCollection.Select(P => RoutePanel(P)));
      });
    }
    internal void StartAnimation(Inv.Animation InvAnimation)
    {
      // NOTE: UWP does not support removing storyboards.
      //       This means once a property is animated, it can only be changed by subsequent animations.
      //       There is also a flaw when an animation is stopped, there must be at least one render frame before you can start an animation on the same property.

      var UwaStoryboard = new Windows.UI.Xaml.Media.Animation.Storyboard();
      InvAnimation.Node = UwaStoryboard;
      UwaStoryboard.Completed += (Sender, Event) => InvAnimation.Complete();

      foreach (var InvTarget in InvAnimation.Targets)
      {
        var InvPanel = InvTarget.Panel;
        var UwaPanel = RoutePanel(InvPanel);
        foreach (var InvTransform in InvTarget.Transforms)
        {
          foreach (var UwaTimeline in TranslateAnimationTransform(UwaStoryboard, InvPanel, UwaPanel, InvTransform))
            UwaStoryboard.Children.Add(UwaTimeline);
        }
      }

      // NOTE: multiple transforms on the same target is not supported by UWP and fails with this exception:
      //      Multiple animations in the same containing Storyboard cannot target the same property on a single element.

      UwaStoryboard.Begin();
    }
    internal void StopAnimation(Inv.Animation InvAnimation)
    {
      if (InvAnimation.Node != null)
      {
        var UwaStoryboard = (Windows.UI.Xaml.Media.Animation.Storyboard)InvAnimation.Node;
        InvAnimation.Node = null;

        foreach (var InvTarget in InvAnimation.Targets)
        {
          var InvPanel = InvTarget.Panel;

          var UwaPanel = RoutePanel(InvPanel);
          if (UwaPanel != null)
          {
            // NOTE: this line ACTUALLY forces the opacity to match the currently animating opacity.
            UwaPanel.Opacity = UwaPanel.Opacity;
            InvPanel.Control.Opacity.BypassSet((float)UwaPanel.Opacity);

            // NOTE: we have to replace the animating transform with a non-animated version with the same values.
            var TransformGroup = UwaPanel.RenderTransform as Windows.UI.Xaml.Media.TransformGroup;
            if (TransformGroup != null)
            {
              var RenderTransform = new Windows.UI.Xaml.Media.TransformGroup();

              var ScaleTransform = GetTransform<Windows.UI.Xaml.Media.ScaleTransform>(TransformGroup);
              if (ScaleTransform != null)
              {
                RenderTransform.Children.Add(new Windows.UI.Xaml.Media.ScaleTransform()
                {
                  ScaleX = ScaleTransform.ScaleX,
                  ScaleY = ScaleTransform.ScaleY,
                  CenterX = ScaleTransform.CenterX,
                  CenterY = ScaleTransform.CenterY
                });
              }

              var RotateTransform = GetTransform<Windows.UI.Xaml.Media.RotateTransform>(TransformGroup);
              if (RotateTransform != null)
              {
                RenderTransform.Children.Add(new Windows.UI.Xaml.Media.RotateTransform()
                {
                  Angle = RotateTransform.Angle,
                  CenterX = RotateTransform.CenterX,
                  CenterY = RotateTransform.CenterY
                });
              }

              var TranslateTransform = GetTransform<Windows.UI.Xaml.Media.TranslateTransform>(TransformGroup);
              if (TranslateTransform != null)
              {
                RenderTransform.Children.Add(new Windows.UI.Xaml.Media.TranslateTransform()
                {
                  X = TranslateTransform.X,
                  Y = TranslateTransform.Y
                });
              }

              UwaPanel.RenderTransform = RenderTransform;
            }
          }
        }

        // NOTE: stop needs to go at the end, otherwise it upsets the animating Opacity of the panels.
        UwaStoryboard.Stop();
      }
    }
    internal void ShowPopup(Inv.Popup InvPopup)
    {
      TranslatePopup(InvPopup).Show();
    }
    internal void HidePopup(Inv.Popup InvPopup)
    {
      TranslatePopup(InvPopup).Hide();
    }
    internal UwaPopup TranslatePopup(Inv.Popup InvPopup)
    {
      var WpfPopup = (Inv.UwaPopup)InvPopup.Node;
      if (WpfPopup == null)
      {
        WpfPopup = new Inv.UwaPopup();
        InvPopup.Node = WpfPopup;
        WpfPopup.FontFamily = Windows.UI.Xaml.Media.FontFamily.XamlAutoFontFamily;
        WpfPopup.ShowEvent += () => InvPopup.ShowInvoke();
        WpfPopup.HideEvent += () => InvPopup.HideInvoke();

        InvPopup.ChangeAction = () =>
        {
          WpfPopup.Background = TranslateMediaBrush(Inv.Theme.BackgroundColour);
          WpfPopup.PlacementMode = InvPopup.Position.Location switch
          {
            Inv.PopupLocation.Default => Windows.UI.Xaml.Controls.Primitives.FlyoutPlacementMode.Full, // TODO: this covers too much.
            Inv.PopupLocation.Above => Windows.UI.Xaml.Controls.Primitives.FlyoutPlacementMode.TopEdgeAlignedRight,
            Inv.PopupLocation.Below => Windows.UI.Xaml.Controls.Primitives.FlyoutPlacementMode.BottomEdgeAlignedRight,
            Inv.PopupLocation.LeftOf => Windows.UI.Xaml.Controls.Primitives.FlyoutPlacementMode.LeftEdgeAlignedTop,
            Inv.PopupLocation.RightOf => Windows.UI.Xaml.Controls.Primitives.FlyoutPlacementMode.RightEdgeAlignedTop,
            Inv.PopupLocation.CenterOver => Windows.UI.Xaml.Controls.Primitives.FlyoutPlacementMode.Top, // TODO: Center
            Inv.PopupLocation.Pointer => Windows.UI.Xaml.Controls.Primitives.FlyoutPlacementMode.Auto, // TODO: this isn't user pointer x,y
            _ => throw Inv.Support.EnumHelper.UnexpectedValueException(InvPopup.Position.Location)
          };
          WpfPopup.PlacementTarget = (InvPopup.Position.Location == PopupLocation.Default ? null : (Windows.UI.Xaml.FrameworkElement)RoutePanel(InvPopup.Position.Target)) ?? UwaMaster;
          WpfPopup.Content = RoutePanel(InvPopup.Content);
        };

        // manually invoke the change action for this first render.
        InvPopup.ChangeInvoke();
      }

      return WpfPopup;
    }
    internal void HapticFeedback(Inv.HapticFeedback InvHapticFeedback)
    {
      if (UwaVibrationDevice != null)
      {
        var SupportedFeedbackArray = UwaVibrationDevice.SimpleHapticsController.SupportedFeedback.OrderBy(F => F.Duration).ToArray();

        if (SupportedFeedbackArray.Length > 0)
        {
          var FirstFeedback = SupportedFeedbackArray.First();
          var LastFeedback = SupportedFeedbackArray.Last();

          double Intensity;
          Windows.Devices.Haptics.SimpleHapticsControllerFeedback Feedback;

          switch (InvHapticFeedback)
          {
            case Inv.HapticFeedback.LightImpact: Intensity = 0.50; Feedback = FirstFeedback; break;
            case Inv.HapticFeedback.MediumImpact: Intensity = 0.75; Feedback = FirstFeedback; break;
            case Inv.HapticFeedback.HeavyImpact: Intensity = 1.00; Feedback = FirstFeedback; break;

            case Inv.HapticFeedback.SuccessNotify: Intensity = 0.50; Feedback = LastFeedback; break;
            case Inv.HapticFeedback.WarningNotify: Intensity = 0.75; Feedback = LastFeedback; break;
            case Inv.HapticFeedback.ErrorNotify: Intensity = 1.00; Feedback = LastFeedback; break;
            default:
              throw new Exception("Inv.HapticFeedback not handled: " + InvHapticFeedback);
          }

          UwaVibrationDevice.SimpleHapticsController.SendHapticFeedback(Feedback, Intensity);
        }
      }
    }
    internal async Task<Windows.Storage.Streams.IRandomAccessStream> LoadMediaImageStream(Inv.Image InvImage)
    {
      var MemoryStream = new Windows.Storage.Streams.InMemoryRandomAccessStream();

      using (var DataWriter = new Windows.Storage.Streams.DataWriter(MemoryStream))
      {
        // Write the bytes to the stream
        DataWriter.WriteBytes(InvImage.GetBuffer());

        // Store the bytes to the MemoryStream
        await DataWriter.StoreAsync();

        // Not necessary, but do it anyway
        await DataWriter.FlushAsync();

        // Detach from the Memory stream so we don't close it
        DataWriter.DetachStream();
      }

      MemoryStream.Seek(0);

      return MemoryStream;
    }
    internal Windows.UI.Xaml.Media.Imaging.BitmapImage LoadMediaImage(Inv.Image InvImage)
    {
      var MemoryStreamTask = LoadMediaImageStream(InvImage);
      MemoryStreamTask.Wait();

      using (var MemoryStream = MemoryStreamTask.Result)
      {
        var Result = new Windows.UI.Xaml.Media.Imaging.BitmapImage();

        Result.SetSource(MemoryStream);

        return Result;
      }
    }
    internal Windows.UI.Xaml.Media.Imaging.WriteableBitmap LoadWriteableBitmap(Inv.Image InvImage, int Width, int Height)
    {
      var MemoryStreamTask = LoadMediaImageStream(InvImage);
      MemoryStreamTask.Wait();

      using (var MemoryStream = MemoryStreamTask.Result)
      {
        var Result = new Windows.UI.Xaml.Media.Imaging.WriteableBitmap(Width, Height);

        Result.SetSource(MemoryStream);

        return Result;
      }
    }
    internal Windows.UI.Xaml.Media.Imaging.BitmapImage ResizeMediaImage(Inv.Image InvImage, int Width, int Height)
    {
      var MemoryStreamTask = LoadMediaImageStream(InvImage);
      MemoryStreamTask.Wait();

      using (var MemoryStream = MemoryStreamTask.Result)
      {
        var Result = new Windows.UI.Xaml.Media.Imaging.BitmapImage();

        Result.SetSource(MemoryStream);
        Result.DecodePixelWidth = Width;
        Result.DecodePixelHeight = Height;

        return Result;
      }
    }
    internal async Task<byte[]> GetImageAsByteAsync(Windows.UI.Xaml.Media.Imaging.WriteableBitmap bitmap, int desiredWidth, int desiredHeight)
    {
      byte[] pixels;
      var pixelsWidth = (uint)bitmap.PixelWidth;
      var pixelsHeight = (uint)bitmap.PixelHeight;

      if (desiredWidth != 0 || desiredHeight != 0)
      {
        var widthRatio = (double)desiredWidth / (double)bitmap.PixelWidth;
        var heightRatio = (double)desiredHeight / (double)bitmap.PixelHeight;

        var scaleRatio = Math.Min(widthRatio, heightRatio);

        if (desiredWidth == 0)
          scaleRatio = heightRatio;

        if (desiredHeight == 0)
          scaleRatio = widthRatio;

        var aspectWidth = (uint)((double)bitmap.PixelWidth * scaleRatio);
        var aspectHeight = (uint)((double)bitmap.PixelHeight * scaleRatio);

        using (var tempStream = new Windows.Storage.Streams.InMemoryRandomAccessStream())
        {
          var tempPixels = await GetBytesFromBitmapAsync(bitmap);

          var encoder = await Windows.Graphics.Imaging.BitmapEncoder.CreateAsync(Windows.Graphics.Imaging.BitmapEncoder.PngEncoderId, tempStream);
          encoder.SetPixelData(Windows.Graphics.Imaging.BitmapPixelFormat.Bgra8, Windows.Graphics.Imaging.BitmapAlphaMode.Premultiplied, pixelsWidth, pixelsHeight, 96, 96, tempPixels);
          await encoder.FlushAsync();
          tempStream.Seek(0);

          var decoder = await Windows.Graphics.Imaging.BitmapDecoder.CreateAsync(tempStream);
          var transform = new Windows.Graphics.Imaging.BitmapTransform()
          {
            ScaledWidth = aspectWidth,
            ScaledHeight = aspectHeight,
            InterpolationMode = Windows.Graphics.Imaging.BitmapInterpolationMode.Linear
          };
          var pixelData = await decoder.GetPixelDataAsync(
              Windows.Graphics.Imaging.BitmapPixelFormat.Bgra8,
              Windows.Graphics.Imaging.BitmapAlphaMode.Premultiplied,
              transform,
              Windows.Graphics.Imaging.ExifOrientationMode.RespectExifOrientation,
              Windows.Graphics.Imaging.ColorManagementMode.DoNotColorManage);

          pixels = pixelData.DetachPixelData();
          pixelsWidth = aspectWidth;
          pixelsHeight = aspectHeight;
        }
      }
      else
      {
        pixels = await GetBytesFromBitmapAsync(bitmap);
      }

      using (var stream = new Windows.Storage.Streams.InMemoryRandomAccessStream())
      {
        var encoder = await Windows.Graphics.Imaging.BitmapEncoder.CreateAsync(Windows.Graphics.Imaging.BitmapEncoder.PngEncoderId, stream);

        encoder.SetPixelData(Windows.Graphics.Imaging.BitmapPixelFormat.Bgra8, Windows.Graphics.Imaging.BitmapAlphaMode.Premultiplied, pixelsWidth, pixelsHeight, 96, 96, pixels);
        await encoder.FlushAsync();
        stream.Seek(0);

        var bytes = new byte[stream.Size];
        await stream.ReadAsync(bytes.AsBuffer(), (uint)stream.Size, Windows.Storage.Streams.InputStreamOptions.None);

        return bytes;
      }
    }
    internal Microsoft.Graphics.Canvas.Text.CanvasTextFormat NewTextFormat(Inv.DrawFont TextFont)
    {
      var TextFormat = new Microsoft.Graphics.Canvas.Text.CanvasTextFormat()
      {
        //FontFamily = TextFont.Name, // TODO: test.
        FontSize = TextFont.Size,
        WordWrapping = Microsoft.Graphics.Canvas.Text.CanvasWordWrapping.NoWrap,
        FontWeight = TranslateFontWeight(TextFont.Weight)
      };
      return TextFormat;
    }

    private async Task<byte[]> GetBytesFromBitmapAsync(Windows.UI.Xaml.Media.Imaging.WriteableBitmap bitmap)
    {
      byte[] tempPixels;
      using (var sourceStream = bitmap.PixelBuffer.AsStream())
      {
        tempPixels = new byte[sourceStream.Length];
        await sourceStream.ReadAsync(tempPixels, 0, tempPixels.Length).ConfigureAwait(false);
      }

      return tempPixels;
    }
    private void Resize()
    {
      var ResolutionWidthPixels = (int)Windows.UI.Xaml.Window.Current.Bounds.Width;
      var ResolutionHeightPixels = (int)Windows.UI.Xaml.Window.Current.Bounds.Height;

      InvApplication.Window.Width = ResolutionWidthPixels;
      InvApplication.Window.Height = ResolutionHeightPixels;
    }
    private void Process()
    {
      if (InvApplication.IsExit)
      {
        Windows.ApplicationModel.Core.CoreApplication.Exit();
        Stop();
      }
      else
      {
        var InvWindow = InvApplication.Window;
        var InvKeyboard = InvApplication.Keyboard;

        InvWindow.ProcessInvoke();

        if (InvWindow.ActiveTimerSet.Count > 0)
        {
          foreach (var InvTimer in InvWindow.ActiveTimerSet)
          {
            var UwaTimer = AccessTimer(InvTimer, S =>
            {
              var Result = new Windows.UI.Xaml.DispatcherTimer();
              Result.Tick += (Sender, Event) =>
              {
                if (InvTimer.IsEnabled)
                  InvTimer.IntervalInvoke();
              };
              return Result;
            });

            if (InvTimer.IsRestarting)
            {
              InvTimer.IsRestarting = false;
              UwaTimer.Stop();
            }

            if (UwaTimer.Interval != InvTimer.IntervalTime)
              UwaTimer.Interval = InvTimer.IntervalTime;

            if (InvTimer.IsEnabled && !UwaTimer.IsEnabled)
              UwaTimer.Start();
            else if (!InvTimer.IsEnabled && UwaTimer.IsEnabled)
              UwaTimer.Stop();
          }

          InvWindow.ActiveTimerSet.RemoveWhere(T => !T.IsEnabled);
        }

        if (Windows.Gaming.Input.Gamepad.Gamepads.Count > 0)
        {
          foreach (var Gamepad in Windows.Gaming.Input.Gamepad.Gamepads)
          {
            var Controller = ResolveController(Gamepad);
            var UwaController = Controller.Contract as UwaController;

            var OldReading = UwaController.LastReading;
            var NewReading = Gamepad.GetCurrentReading();

            Controller.LeftTrigger.Depression = NewReading.LeftTrigger;
            Controller.RightTrigger.Depression = NewReading.RightTrigger;
            Controller.LeftThumbstick.X = NewReading.LeftThumbstickX;
            Controller.LeftThumbstick.Y = -NewReading.LeftThumbstickY; // fix Y inversion.
            Controller.RightThumbstick.X = NewReading.RightThumbstickX;
            Controller.RightThumbstick.Y = -NewReading.RightThumbstickY; // fix Y inversion.

            if (OldReading.Buttons != Windows.Gaming.Input.GamepadButtons.None || NewReading.Buttons != Windows.Gaming.Input.GamepadButtons.None)
            {
              foreach (var CheckButtonEntry in GamepadButtonArray.GetTuples())
              {
                var CheckButton = CheckButtonEntry.Value;

                if ((Windows.Gaming.Input.GamepadButtons.None == (OldReading.Buttons & CheckButton)) && (CheckButton == (NewReading.Buttons & CheckButton)))
                  Controller.GetButton(CheckButtonEntry.Key).PressInvoke();
                else if ((CheckButton == (OldReading.Buttons & CheckButton)) && (Windows.Gaming.Input.GamepadButtons.None == (NewReading.Buttons & CheckButton)))
                  Controller.GetButton(CheckButtonEntry.Key).ReleaseInvoke();
              }
            }

            UwaController.LastReading = NewReading;
          }
        }

        var InvSurface = InvWindow.ActiveSurface;

        if (InvSurface != null)
        {
          var UwaSurface = TranslateSurface(InvSurface);

          if (!UwaMaster.Children.Contains(UwaSurface))
            InvSurface.ArrangeInvoke();

          ProcessTransition(UwaSurface);

          InvSurface.ComposeInvoke();

          UpdateSurface(InvSurface, UwaSurface);
        }
        else
        {
          UwaPage.Content = null;
        }

        InvWindow.ProcessChanges(P => RoutePanel(P));

        if (InvWindow.Render())
        {
          //var AppView = Windows.UI.ViewManagement.ApplicationView.GetForCurrentView();
          //AppView.Title = InvApplication.Title ?? "";

          if (InvWindow.Background.Render())
            UwaMaster.Background = TranslateMediaBrush(InvWindow.Background.Colour ?? Inv.Colour.Black);

          if (UwaPage.IsEnabled == InvWindow.InputPrevented) // check in case setting this boolean has side effects even when it's the same value.
          {
            if (InvWindow.InputPrevented)
              this.LastInputElement = Windows.UI.Xaml.Input.FocusManager.GetFocusedElement() as Windows.UI.Xaml.Controls.Control;

            UwaPage.IsEnabled = !InvWindow.InputPrevented;

            if (!InvWindow.InputPrevented && LastInputElement != null)
              this.LastInputElement.Focus(Windows.UI.Xaml.FocusState.Programmatic);
          }

          // TODO: Panel.IsEnabled does not work consistently in UWP - panels are left disabled when they should be enabled.
          //       Panel.IsHitTestVisible seems to work better?
          foreach (var InvPanel in InvWindow.DisablePanelList)
          {
            var UwaPanel = RoutePanel(InvPanel);
            if (UwaPanel != null)
              UwaPanel.IsHitTestVisible = false;
          }
          InvWindow.DisablePanelList.Clear();

          foreach (var InvPanel in InvWindow.EnablePanelList)
          {
            var UwaPanel = RoutePanel(InvPanel);
            if (UwaPanel != null)
              UwaPanel.IsHitTestVisible = true;
          }
          InvWindow.EnablePanelList.Clear();
        }

        // NOTE: focus doesn't completely work on startup (but if you alt+tab away and then back to the window, it will have the focus in the expected control).
        if (InvKeyboard.Focus != null)
        {
          var UwaOverrideFocus = InvKeyboard.Focus.Control?.Node as UwaOverrideFocusContract;

          // NOTE: Post is required if the control receiving focus is not yet loaded into the visual tree.
          if (UwaOverrideFocus != null)
          {
            Post(() => UwaOverrideFocus.OverrideFocus());
          }
          else
          {
            var UwaControl = InvKeyboard.Focus.Control?.Node as Windows.UI.Xaml.Controls.Control;
            if (UwaControl != null)
              Post(() => UwaControl.Focus(Windows.UI.Xaml.FocusState.Programmatic));
          }

          InvKeyboard.Focus = null;
        }

        InvWindow.DisplayRate.Calculate();
      }
    }
    private void UpdateSurface(Surface InvSurface, UwaSurface UwaSurface)
    {
      if (InvSurface.Render())
      {
        UwaSurface.Border.Background = TranslateMediaBrush(InvSurface.Background.Colour);
        UwaSurface.Border.SafeSetChild(RoutePanel(InvSurface.Content));
      }
    }
    private void ProcessTransition(UwaSurface UwaSurface)
    {
      var InvWindow = InvApplication.Window;

      var InvTransition = InvWindow.ActiveTransition;
      if (InvTransition == null)
      {
        //Debug.Assert(UwaMaster.Children.Contains(UwaSurface));
      }
      else if (!IsTransitioning) // don't transition while animating a previous transition.
      {
        var UwaFromSurface = UwaMaster.Children.Count == 0 ? null : (UwaSurface)UwaMaster.Children[0];
        var UwaToSurface = UwaSurface;

        if (UwaFromSurface == UwaToSurface)
        {
          Debug.WriteLine("Transition to self");
        }
        else
        {
          if (InvWindow.FromSurface != null)
          {
            if (TranslateSurface(InvWindow.FromSurface) == UwaFromSurface)
              UpdateSurface(InvWindow.FromSurface, UwaFromSurface);

            InvWindow.FromSurface = null;
          }

          ExecuteTransition(InvTransition, UwaMaster, UwaFromSurface, UwaToSurface, () => { });
        }

        InvWindow.ActiveTransition = null;
      }
    }
    private void ExecuteTransition(Inv.Transition InvTransition, Windows.UI.Xaml.Controls.Grid UwaGrid, Windows.UI.Xaml.Controls.Control UwaFromElement, Windows.UI.Xaml.Controls.Control UwaToElement, Action CompletedAction)
    {
      void CompleteTransition()
      {
        CompletedAction();
        InvTransition.Complete();
      }

      var UwaTransitionOut = UwaFromElement != null;
      var UwaTransitionIn = UwaToElement != null;

      switch (InvTransition.Type)
      {
        case TransitionType.None:
          UwaGrid.Children.Clear();

          if (UwaTransitionIn)
            UwaGrid.Children.Add(UwaToElement);

          CompleteTransition();
          break;

        case TransitionType.Fade:
          if (!UwaTransitionOut && !UwaTransitionIn)
          {
            CompleteTransition();
          }
          else
          {
            this.TransitionCount++;

            var UwaFadeOutStoryboard = UwaTransitionOut ? new Windows.UI.Xaml.Media.Animation.Storyboard() : null;
            var UwaFadeInStoryboard = UwaTransitionIn ? new Windows.UI.Xaml.Media.Animation.Storyboard() : null;

            var UwaFadeDuration = UwaTransitionOut && UwaTransitionIn ? new Windows.UI.Xaml.Duration(TimeSpan.FromMilliseconds(InvTransition.Duration.TotalMilliseconds / 2)) : new Windows.UI.Xaml.Duration(InvTransition.Duration);

            if (UwaTransitionOut)
            {
              UwaFromElement.IsHitTestVisible = false;

              var UwaFadeOutAnimation = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
              {
                FillBehavior = Windows.UI.Xaml.Media.Animation.FillBehavior.HoldEnd,
                From = 1.0,
                To = 0.0,
                Duration = UwaFadeDuration
              };
              Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(UwaFadeOutAnimation, UwaFromElement);
              Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(UwaFadeOutAnimation, "Opacity");
              UwaFadeOutStoryboard.Children.Add(UwaFadeOutAnimation);
              UwaFadeOutAnimation.Completed += (Sender, Event) =>
              {
                UwaFromElement.IsHitTestVisible = true;
                UwaFromElement.Opacity = 1.0;
                UwaGrid.Children.Remove(UwaFromElement);

                if (UwaTransitionIn)
                {
                  UwaGrid.Children.Add(UwaToElement);
                  UwaFadeInStoryboard.Begin();
                }
                else
                {
                  this.TransitionCount--; // only if not fading in.
                  CompleteTransition();
                }
              };
            }

            if (UwaTransitionIn)
            {
              if (!UwaTransitionOut)
                UwaGrid.Children.Add(UwaToElement);

              UwaToElement.IsHitTestVisible = false;
              UwaToElement.Opacity = 0.0;

              var UwaFadeInAnimation = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
              {
                FillBehavior = Windows.UI.Xaml.Media.Animation.FillBehavior.HoldEnd,
                From = 0.0,
                To = 1.0,
                Duration = UwaFadeDuration
              };
              Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(UwaFadeInAnimation, UwaToElement);
              Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(UwaFadeInAnimation, "Opacity");
              UwaFadeInStoryboard.Children.Add(UwaFadeInAnimation);
              UwaFadeInAnimation.Completed += (Sender, Event) =>
              {
                UwaToElement.IsHitTestVisible = true;
                UwaToElement.Opacity = 1.0;

                this.TransitionCount--;
                CompleteTransition();
              };
            }

            if (UwaTransitionOut)
              UwaFadeOutStoryboard.Begin();
            else if (UwaTransitionIn)
              UwaFadeInStoryboard.Begin();
          }
          break;

        case TransitionType.CarouselPrevious:
        case TransitionType.CarouselNext:
        case TransitionType.CarouselAscend:
        case TransitionType.CarouselDescend:
          if (!UwaTransitionOut && !UwaTransitionIn)
          {
            CompleteTransition();
          }
          else
          {
            this.TransitionCount++;

            var CarouselForward = InvTransition.Type == TransitionType.CarouselNext || InvTransition.Type == TransitionType.CarouselAscend;
            var CarouselHorizontal = InvTransition.Type == TransitionType.CarouselNext || InvTransition.Type == TransitionType.CarouselPrevious;

            var UwaCarouselDuration = new Windows.UI.Xaml.Duration(InvTransition.Duration);

            var UwaCarouselStoryboard = new Windows.UI.Xaml.Media.Animation.Storyboard();

            var UwaActualDimension = CarouselHorizontal ? UwaGrid.ActualWidth : UwaGrid.ActualHeight;
            var UwaTargetProperty = CarouselHorizontal ? "X" : "Y";

            if (UwaTransitionOut)
            {
              var FromCarouselTransform = new Windows.UI.Xaml.Media.TranslateTransform() { X = 0, Y = 0 };
              UwaFromElement.RenderTransform = FromCarouselTransform;
              UwaFromElement.IsHitTestVisible = false;

              var UwaFromAnimation = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
              {
                //AccelerationRatio = 0.5,
                //DecelerationRatio = 0.5,
                Duration = UwaCarouselDuration,
                From = 0,
                To = CarouselForward ? -UwaActualDimension : UwaActualDimension
              };
              Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(UwaFromAnimation, FromCarouselTransform);
              Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(UwaFromAnimation, UwaTargetProperty);
              UwaCarouselStoryboard.Children.Add(UwaFromAnimation);
            }

            if (UwaTransitionIn)
            {
              var ToCarouselTransform = new Windows.UI.Xaml.Media.TranslateTransform() { X = 0, Y = 0 };
              UwaToElement.RenderTransform = ToCarouselTransform;
              UwaToElement.IsHitTestVisible = false;

              UwaGrid.Children.Add(UwaToElement);

              var UwaToAnimation = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
              {
                //AccelerationRatio = 0.5,
                //DecelerationRatio = 0.5,
                Duration = UwaCarouselDuration,
                From = CarouselForward ? UwaActualDimension : -UwaActualDimension,
                To = 0
              };
              Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(UwaToAnimation, ToCarouselTransform);
              Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(UwaToAnimation, UwaTargetProperty);
              UwaCarouselStoryboard.Children.Add(UwaToAnimation);
            }

            UwaCarouselStoryboard.Completed += (Sender, Event) =>
            {
              if (UwaTransitionOut)
              {
                UwaFromElement.IsHitTestVisible = true;
                UwaGrid.Children.Remove(UwaFromElement);
                UwaFromElement.RenderTransform = null;
              }

              if (UwaTransitionIn)
              {
                UwaToElement.IsHitTestVisible = true;
                UwaToElement.RenderTransform = null;
              }

              this.TransitionCount--;
              CompleteTransition();
            };

            UwaCarouselStoryboard.Begin();
          }
          break;

        default:
          throw new Exception("TransitionType not handled: " + InvTransition.Type);
      }
    }
    private TTransform GetTransform<TTransform>(Windows.UI.Xaml.Media.TransformGroup TransformGroup)
      where TTransform : Windows.UI.Xaml.Media.Transform
    {
      return (TTransform)TransformGroup.Children.Find(C => C.GetType() == typeof(TTransform));
    }
    private TTransform ForceTransform<TTransform>(Windows.UI.Xaml.Controls.Control UwaPanel)
      where TTransform : Windows.UI.Xaml.Media.Transform
    {
      var TransformGroup = UwaPanel.RenderTransform as Windows.UI.Xaml.Media.TransformGroup;
      if (TransformGroup == null)
      {
        TransformGroup = new Windows.UI.Xaml.Media.TransformGroup();
        UwaPanel.RenderTransform = TransformGroup;
      }

      var Result = GetTransform<TTransform>(TransformGroup);

      if (Result == null)
      {
        Result = Activator.CreateInstance<TTransform>();
        TransformGroup.Children.Add(Result);
      }

      return Result;
    }
    private IEnumerable<Windows.UI.Xaml.Media.Animation.Timeline> TranslateAnimationTransform(Windows.UI.Xaml.Media.Animation.Storyboard UwaStoryboard, Inv.Panel InvPanel, Windows.UI.Xaml.Controls.Control UwaPanel, AnimationTransform InvTransform)
    {
      return InvTransform.Type switch
      {
        AnimationType.Fade => TranslateAnimationFadeTransform(UwaStoryboard, InvPanel, UwaPanel, (AnimationFadeTransform)InvTransform),
        AnimationType.Rotate => TranslateAnimationRotateTransform(UwaStoryboard, InvPanel, UwaPanel, (AnimationRotateTransform)InvTransform),
        AnimationType.Scale => TranslateAnimationScaleTransform(UwaStoryboard, InvPanel, UwaPanel, (AnimationScaleTransform)InvTransform),
        AnimationType.Translate => TranslateAnimationTranslateTransform(UwaStoryboard, InvPanel, UwaPanel, (AnimationTranslateTransform)InvTransform),
        _ => throw new Exception("Animation Transform not handled: " + InvTransform.Type),
      };
    }
    private IEnumerable<Windows.UI.Xaml.Media.Animation.Timeline> TranslateAnimationFadeTransform(Windows.UI.Xaml.Media.Animation.Storyboard UwaStoryboard, Inv.Panel InvPanel, Windows.UI.Xaml.Controls.Control UwaPanel, AnimationFadeTransform InvTransform)
    {
      var Result = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
      {
        From = float.IsNaN(InvTransform.FromOpacity) ? UwaPanel.Opacity : InvTransform.FromOpacity,
        To = InvTransform.ToOpacity,
        Duration = InvTransform.Duration,
        FillBehavior = Windows.UI.Xaml.Media.Animation.FillBehavior.Stop
      };

      Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(Result, UwaPanel);
      Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(Result, "Opacity");

      InvPanel.Control.Opacity.BypassSet(InvTransform.ToOpacity);

      if (InvTransform.Offset != null && InvTransform.Offset.Value > TimeSpan.Zero)
      {
        var Duration = InvTransform.Offset.Value;

        // set to the starting opacity, because we are used a delayed start animation.
        if (!float.IsNaN(InvTransform.FromOpacity))
          UwaPanel.Opacity = InvTransform.FromOpacity;

        // NOTE: if you set BeginTime to null, the animation will not run!
        Result.BeginTime = Duration;

        // there's no animation 'Started' so we need to apply the end opacity value using a background task.
        Task.Delay(Duration).ContinueWith(T => Post(() =>
        {
          if (UwaStoryboard.GetCurrentState() != Windows.UI.Xaml.Media.Animation.ClockState.Stopped)
            UwaPanel.Opacity = InvTransform.ToOpacity;
        }));
      }
      else
      {
        // this animation starts immediately, so we can manually set the panel to the final opacity.
        UwaPanel.Opacity = InvTransform.ToOpacity;
      }

      yield return Result;
    }
    private IEnumerable<Windows.UI.Xaml.Media.Animation.Timeline> TranslateAnimationRotateTransform(Windows.UI.Xaml.Media.Animation.Storyboard UwaStoryboard, Inv.Panel InvPanel, Windows.UI.Xaml.Controls.Control UwaPanel, AnimationRotateTransform InvTransform)
    {
      var RotateTransform = ForceTransform<Windows.UI.Xaml.Media.RotateTransform>(UwaPanel);

      var RenderSize = TranslateRenderSize(UwaPanel);
      RotateTransform.CenterX = RenderSize.Width / 2;
      RotateTransform.CenterY = RenderSize.Height / 2;

      var Result = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
      {
        From = float.IsNaN(InvTransform.FromAngle) ? (double?)null : InvTransform.FromAngle,
        To = InvTransform.ToAngle,
        Duration = InvTransform.Duration,
        FillBehavior = Windows.UI.Xaml.Media.Animation.FillBehavior.HoldEnd
      };

      // NOTE: if you set BeginTime to null, the animation will not run!
      if (InvTransform.Offset != null)
        Result.BeginTime = InvTransform.Offset;

      Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(Result, RotateTransform);
      Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(Result, "Angle");

      Result.Completed += (Sender, Event) => RotateTransform.Angle = InvTransform.ToAngle;

      yield return Result;
    }
    private IEnumerable<Windows.UI.Xaml.Media.Animation.Timeline> TranslateAnimationScaleTransform(Windows.UI.Xaml.Media.Animation.Storyboard UwaStoryboard, Inv.Panel InvPanel, Windows.UI.Xaml.Controls.Control UwaPanel, AnimationScaleTransform InvTransform)
    {
      var ScaleTransform = ForceTransform<Windows.UI.Xaml.Media.ScaleTransform>(UwaPanel);

      var RenderSize = TranslateRenderSize(UwaPanel);
      ScaleTransform.CenterX = RenderSize.Width / 2;
      ScaleTransform.CenterY = RenderSize.Height / 2;

      Windows.UI.Xaml.Media.Animation.Timeline ScaleAnimation(float From, float To, string DependencyProperty)
      {
        var Result = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
        {
          From = float.IsNaN(From) ? (double?)null : From,
          To = To,
          Duration = InvTransform.Duration,
          FillBehavior = Windows.UI.Xaml.Media.Animation.FillBehavior.HoldEnd
        };

        // NOTE: if you set BeginTime to null, the animation will not run!
        if (InvTransform.Offset != null)
          Result.BeginTime = InvTransform.Offset;

        Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(Result, ScaleTransform);
        Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(Result, DependencyProperty);

        return Result;
      }

      if (!float.IsNaN(InvTransform.ToWidth))
      {
        if (!float.IsNaN(InvTransform.FromWidth))
          ScaleTransform.ScaleX = InvTransform.FromWidth;

        var ResultX = ScaleAnimation(InvTransform.FromWidth, InvTransform.ToWidth, "ScaleX");
        ResultX.Completed += (Sender, Event) => ScaleTransform.ScaleX = InvTransform.ToWidth;
        yield return ResultX;
      }

      if (!float.IsNaN(InvTransform.ToHeight))
      {
        if (!float.IsNaN(InvTransform.FromHeight))
          ScaleTransform.ScaleY = InvTransform.FromHeight;

        var ResultY = ScaleAnimation(InvTransform.FromHeight, InvTransform.ToHeight, "ScaleY");
        ResultY.Completed += (Sender, Event) => ScaleTransform.ScaleY = InvTransform.ToHeight;
        yield return ResultY;
      }
    }
    private IEnumerable<Windows.UI.Xaml.Media.Animation.Timeline> TranslateAnimationTranslateTransform(Windows.UI.Xaml.Media.Animation.Storyboard UwaStoryboard, Inv.Panel InvPanel, Windows.UI.Xaml.Controls.Control UwaPanel, AnimationTranslateTransform InvTransform)
    {
      var TranslateTransform = ForceTransform<Windows.UI.Xaml.Media.TranslateTransform>(UwaPanel);

      var RenderSize = TranslateRenderSize(UwaPanel);

      Windows.UI.Xaml.Media.Animation.Timeline TranslateAnimation(int? From, int To, string DependencyProperty)
      {
        var Result = new Windows.UI.Xaml.Media.Animation.DoubleAnimation()
        {
          From = From == null ? (double?)null : From.Value,
          To = To,
          Duration = InvTransform.Duration,
          FillBehavior = Windows.UI.Xaml.Media.Animation.FillBehavior.HoldEnd
        };

        // NOTE: if you set BeginTime to null, the animation will not run!
        if (InvTransform.Offset != null)
          Result.BeginTime = InvTransform.Offset;

        Windows.UI.Xaml.Media.Animation.Storyboard.SetTarget(Result, TranslateTransform);
        Windows.UI.Xaml.Media.Animation.Storyboard.SetTargetProperty(Result, DependencyProperty);

        return Result;
      }

      if (InvTransform.ToX != null)
      {
        if (InvTransform.FromX != null)
          TranslateTransform.X = InvTransform.FromX.Value;

        var ResultX = TranslateAnimation(InvTransform.FromX, InvTransform.ToX.Value, "X");
        ResultX.Completed += (Sender, Event) => TranslateTransform.X = InvTransform.ToX.Value;
        yield return ResultX;
      }

      if (InvTransform.ToY != null)
      {
        if (InvTransform.FromY != null)
          TranslateTransform.Y = InvTransform.FromY.Value;

        var ResultY = TranslateAnimation(InvTransform.FromY, InvTransform.ToY.Value, "Y");
        ResultY.Completed += (Sender, Event) => TranslateTransform.Y = InvTransform.ToY.Value;
        yield return ResultY;
      }
    }
    private Windows.UI.Xaml.Controls.Control RoutePanel(Inv.Panel InvPanel)
    {
      var InvControl = InvPanel?.Control;

      if (InvControl == null)
        return null;
      else
        return RouteArray[InvControl.ControlType](InvControl);
    }
    private TElement TranslatePanel<TControl, TElement>(TControl InvControl, Func<TElement> BuildFunction, Action<TElement> RenderAction)
      where TControl : Inv.Control
      where TElement : Windows.UI.Xaml.Controls.Control
    {
      TElement Result;

      // construct the panel if required.
      if (InvControl.Node == null)
      {
        Result = BuildFunction();
        InvControl.Node = Result;
        Result.Tag = InvControl;
      }
      else
      {
        Result = (TElement)InvControl.Node;
      }

      // render the panel if required.
      if (InvControl.Render())
      {
        // trap any exceptions as it will cascade to disrupt other panels that may not have any problems.
        try
        {
          RenderAction(Result);
        }
        catch (Exception Exception)
        {
          HandleException(Exception);
        }
      }

      return Result;
    }
    private Windows.UI.Xaml.DispatcherTimer AccessTimer(Inv.WindowTimer InvTimer, Func<Inv.WindowTimer, Windows.UI.Xaml.DispatcherTimer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (Windows.UI.Xaml.DispatcherTimer)InvTimer.Node;
      }
    }
    private void TranslateLayout(Inv.Control InvControl, UwaPanel UwaPanel)
    {
      TranslateLayout(InvControl, UwaPanel, UwaPanel.Border);
    }
    private void TranslateLayout(Inv.Control InvControl, Windows.UI.Xaml.Controls.Control UwaControl, Windows.UI.Xaml.Controls.Border UwaBorder)
    {
      if (InvControl.Background.Render())
        UwaBorder.Background = TranslateMediaBrush(InvControl.Background.Colour);

      if (InvControl.Corner.Render())
        UwaBorder.CornerRadius = TranslateCorner(InvControl.Corner);

      var InvBorder = InvControl.Border;
      if (InvBorder.Render())
      {
        UwaBorder.BorderBrush = TranslateMediaBrush(InvBorder.Colour);
        UwaBorder.BorderThickness = TranslateEdge(InvBorder);
      }

      var InvMargin = InvControl.Margin;
      if (InvMargin.Render())
        UwaControl.Margin = TranslateEdge(InvMargin); // this has to be the layout control, otherwise the margin will on the inside of the button clickable area.

      var InvPadding = InvControl.Padding;
      if (InvPadding.Render())
        UwaBorder.Padding = TranslateEdge(InvPadding);

      var InvElevation = InvControl.Elevation;
      if (InvElevation.Render())
      {
        // TODO: apply elevation.
        // InvElevation.Get();
        /*
        if (InvElevation.Get() > 0)
        {
          UwaControl.Loaded += (Sender, Event) =>
          {
            var Parent = (UwaControl.Parent as Windows.UI.Xaml.Controls.Control).Parent as Windows.UI.Xaml.Controls.Control;

            var compositor = Windows.UI.Xaml.Hosting.ElementCompositionPreview.GetElementVisual(Parent).Compositor;
            var spriteVisual = compositor.CreateSpriteVisual();
            //spriteVisual.Offset = Windows.UI.Xaml.Controls.Control.POint
            spriteVisual.Size = System.Numerics.VectorExtensions.ToVector2(UwaControl.RenderSize);

            var dropShadow = compositor.CreateDropShadow();
            //dropShadow.Mask = this.txtBlock.GetAlphaMask();
            dropShadow.Offset = new System.Numerics.Vector3(10, 10, 0);
            spriteVisual.Shadow = dropShadow;

            Windows.UI.Xaml.Hosting.ElementCompositionPreview.SetElementChildVisual(Parent, spriteVisual);
          };
        }*/
      }

      var InvOpacity = InvControl.Opacity;
      if (InvOpacity.Render())
        UwaControl.Opacity = InvOpacity.Get();

      var InvVisibility = InvControl.Visibility;
      if (InvVisibility.Render())
        UwaControl.Visibility = InvVisibility.Get() ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;

      TranslateSize(InvControl.Size, UwaControl);

      TranslateAlignment(InvControl.Alignment, UwaControl);

      // only connect SizedChanged for tracked panels.
      UwaControl.SizeChanged -= PanelSizedChanged;
      if (InvControl.HasAdjust)
        UwaControl.SizeChanged += PanelSizedChanged;
    }
    private Windows.Foundation.Size TranslateRenderSize(Windows.UI.Xaml.Controls.Control UwaPanel)
    {
      var RenderSize = UwaPanel.RenderSize;
      if (RenderSize.Width <= 0 || RenderSize.Height <= 0)
        RenderSize = new Windows.Foundation.Size(UwaPanel.Width, UwaPanel.Height);

      Debug.Assert(RenderSize.Width > 0 && RenderSize.Height > 0);

      return RenderSize;
    }
    private Windows.UI.Xaml.CornerRadius TranslateCorner(Inv.Corner InvCorner)
    {
      return new Windows.UI.Xaml.CornerRadius(InvCorner.TopLeft, InvCorner.TopRight, InvCorner.BottomRight, InvCorner.BottomLeft);
    }
    private void TranslateSize(Inv.Size InvSize, Windows.UI.Xaml.Controls.Control UwaElement)
    {
      if (InvSize.Render())
      {
        if (InvSize.Width != null)
          UwaElement.Width = InvSize.Width.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.Control.WidthProperty);

        if (InvSize.Height != null)
          UwaElement.Height = InvSize.Height.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.Control.HeightProperty);

        if (InvSize.MinimumWidth != null)
          UwaElement.MinWidth = InvSize.MinimumWidth.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.Control.MinWidthProperty);

        if (InvSize.MinimumHeight != null)
          UwaElement.MinHeight = InvSize.MinimumHeight.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.Control.MinHeightProperty);

        if (InvSize.MaximumWidth != null)
          UwaElement.MaxWidth = InvSize.MaximumWidth.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.Control.MaxWidthProperty);

        if (InvSize.MaximumHeight != null)
          UwaElement.MaxHeight = InvSize.MaximumHeight.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.Control.MaxHeightProperty);
      }
    }
    private void TranslateAlignment(Inv.Alignment InvAlignment, Windows.UI.Xaml.Controls.Control UwaElement)
    {
      if (InvAlignment.Render())
      {
        switch (InvAlignment.Get())
        {
          case Inv.Placement.Stretch:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchLeft:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchCenter:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchRight:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.TopStretch:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopLeft:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopCenter:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopRight:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            break;

          case Inv.Placement.CenterStretch:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            break;

          case Inv.Placement.CenterLeft:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            break;

          case Inv.Placement.Center:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            break;

          case Inv.Placement.CenterRight:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            break;

          case Inv.Placement.BottomStretch:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomLeft:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomCenter:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomRight:
            UwaElement.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
            UwaElement.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Bottom;
            break;

          default:
            throw new Exception("Inv.Placement not handled: " + InvAlignment.Get());
        }
      }
    }
    private Windows.UI.Xaml.Media.Brush TranslateMediaBrush(Inv.Colour InvColour)
    {
      if (InvColour == null)
        return null;

      return MediaBrushDictionary.GetOrAdd(InvColour, C =>
      {
        var Result = new Windows.UI.Xaml.Media.SolidColorBrush(TranslateMediaColour(C));
        return Result;
      });
    }
    private Windows.UI.Xaml.Media.PenLineJoin TranslateLineJoin(Inv.LineJoin InvLineJoin)
    {
      return InvLineJoin switch
      {
        Inv.LineJoin.Miter => Windows.UI.Xaml.Media.PenLineJoin.Miter,
        Inv.LineJoin.Bevel => Windows.UI.Xaml.Media.PenLineJoin.Bevel,
        Inv.LineJoin.Round => Windows.UI.Xaml.Media.PenLineJoin.Round,
        _ => throw new NotSupportedException("LineJoin not handled: " + InvLineJoin.ToString()),
      };
    }
    private Windows.UI.Xaml.Media.PenLineCap TranslateLineCap(Inv.LineCap InvLineCap)
    {
      return InvLineCap switch
      {
        Inv.LineCap.Butt => Windows.UI.Xaml.Media.PenLineCap.Flat,
        Inv.LineCap.Round => Windows.UI.Xaml.Media.PenLineCap.Round,
        Inv.LineCap.Square => Windows.UI.Xaml.Media.PenLineCap.Square,
        _ => throw new NotSupportedException("LineCap not handled: " + InvLineCap),
      };
    }
    private Windows.UI.Xaml.Media.Stretch TranslateFit(Inv.FitMethod InvFitMethod)
    {
      return InvFitMethod switch
      {
        Inv.FitMethod.Original => Windows.UI.Xaml.Media.Stretch.None,
        Inv.FitMethod.Contain => Windows.UI.Xaml.Media.Stretch.Uniform,
        Inv.FitMethod.Stretch => Windows.UI.Xaml.Media.Stretch.Fill,
        Inv.FitMethod.Cover => Windows.UI.Xaml.Media.Stretch.UniformToFill,
        _ => throw new Exception("FitMethod not handled: " + InvFitMethod),
      };
    }
    private Windows.UI.Xaml.TextAlignment TranslateJustify(Inv.Justify InvJustify)
    {
      return InvJustify.Get() == Justification.Left ? Windows.UI.Xaml.TextAlignment.Left : InvJustify.Get() == Justification.Right ? Windows.UI.Xaml.TextAlignment.Right : Windows.UI.Xaml.TextAlignment.Center;
    }
    private Inv.Point TranslatePoint(Windows.Foundation.Point UwaPoint)
    {
      return new Inv.Point((int)UwaPoint.X, (int)UwaPoint.Y);
    }
    private Windows.Foundation.Point TranslatePoint(Inv.Point InvPoint)
    {
      return new Windows.Foundation.Point(InvPoint.X, InvPoint.Y);
    }
    private Windows.UI.Xaml.Thickness TranslateEdge(Inv.Edge InvEdge)
    {
      return new Windows.UI.Xaml.Thickness(InvEdge.Left, InvEdge.Top, InvEdge.Right, InvEdge.Bottom);
    }
    private void TranslateFont(Inv.Font InvFont, Windows.UI.Xaml.Documents.Inline UwaElement)
    {
      InvFont.Render(); // NOTE: we always have to apply the font, because the Inline is recreated each time.
      {
        if (InvFont.Name != null)
          UwaElement.FontFamily = new Windows.UI.Xaml.Media.FontFamily(InvFont.Name);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Documents.Inline.FontFamilyProperty);

        if (InvFont.Size != null)
          UwaElement.FontSize = InvFont.Size.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Documents.Inline.FontSizeProperty);

        if (InvFont.Colour != null)
          UwaElement.Foreground = TranslateMediaBrush(InvFont.Colour);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Documents.Inline.ForegroundProperty); // doesn't need to be black because we want to inherit from the TextBlock.

        if (InvFont.Weight != null)
          UwaElement.FontWeight = TranslateFontWeight(InvFont.Weight.Value);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Documents.Inline.FontWeightProperty);

        if (InvFont.Axis != null)
          Windows.UI.Xaml.Documents.Typography.SetVariants(UwaElement, TranslateFontAxisVariants(InvFont.Axis.Value));
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Documents.Typography.VariantsProperty);

        if (InvFont.IsSmallCaps != null)
          Windows.UI.Xaml.Documents.Typography.SetCapitals(UwaElement, InvFont.IsSmallCaps.Value ? Windows.UI.Xaml.FontCapitals.SmallCaps : Windows.UI.Xaml.FontCapitals.Normal);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Documents.Typography.CapitalsProperty);

        if (InvFont.IsItalics != null)
          UwaElement.FontStyle = InvFont.IsItalics.Value ? Windows.UI.Text.FontStyle.Italic : Windows.UI.Text.FontStyle.Normal;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Documents.Inline.FontStyleProperty);

        UwaElement.TextDecorations = TranslateFontDecorations(InvFont);
      }
    }
    private void TranslateFont(Inv.Font InvFont, Windows.UI.Xaml.Controls.TextBlock UwaElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          UwaElement.FontFamily = new Windows.UI.Xaml.Media.FontFamily(InvFont.Name);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBlock.FontFamilyProperty);

        if (InvFont.Size != null)
          UwaElement.FontSize = InvFont.Size.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBlock.FontSizeProperty);

        if (InvFont.Colour != null)
          UwaElement.Foreground = TranslateMediaBrush(InvFont.Colour);
        else
          UwaElement.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);

        if (InvFont.Weight != null)
          UwaElement.FontWeight = TranslateFontWeight(InvFont.Weight.Value);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBlock.FontWeightProperty);

        if (InvFont.Axis != null)
          Windows.UI.Xaml.Documents.Typography.SetVariants(UwaElement, TranslateFontAxisVariants(InvFont.Axis.Value));
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Documents.Typography.VariantsProperty);

        if (InvFont.IsSmallCaps != null)
          Windows.UI.Xaml.Documents.Typography.SetCapitals(UwaElement, InvFont.IsSmallCaps.Value ? Windows.UI.Xaml.FontCapitals.SmallCaps : Windows.UI.Xaml.FontCapitals.Normal);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Documents.Typography.CapitalsProperty);

        if (InvFont.IsItalics != null)
          UwaElement.FontStyle = InvFont.IsItalics.Value ? Windows.UI.Text.FontStyle.Italic : Windows.UI.Text.FontStyle.Normal;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBlock.FontStyleProperty);

        UwaElement.TextDecorations = TranslateFontDecorations(InvFont);
      }
    }
    private void TranslateFont(Inv.Font InvFont, Windows.UI.Xaml.Controls.TextBox UwaElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          UwaElement.FontFamily = new Windows.UI.Xaml.Media.FontFamily(InvFont.Name);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBox.FontFamilyProperty);

        if (InvFont.Size != null)
          UwaElement.FontSize = InvFont.Size.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBox.FontSizeProperty);

        var UwaBrush = TranslateMediaBrush(InvFont.Colour);

        if (UwaBrush != null)
          UwaElement.Foreground = UwaBrush;
        else
          UwaElement.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);

        if (InvFont.Weight != null)
          UwaElement.FontWeight = TranslateFontWeight(InvFont.Weight.Value);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBox.FontWeightProperty);

        if (InvFont.Axis != null)
          Windows.UI.Xaml.Documents.Typography.SetVariants(UwaElement, TranslateFontAxisVariants(InvFont.Axis.Value));
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Documents.Typography.VariantsProperty);

        if (InvFont.IsSmallCaps != null)
          Windows.UI.Xaml.Documents.Typography.SetCapitals(UwaElement, InvFont.IsSmallCaps.Value ? Windows.UI.Xaml.FontCapitals.SmallCaps : Windows.UI.Xaml.FontCapitals.Normal);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Documents.Typography.CapitalsProperty);

        if (InvFont.IsItalics != null)
          UwaElement.FontStyle = InvFont.IsItalics.Value ? Windows.UI.Text.FontStyle.Italic : Windows.UI.Text.FontStyle.Normal;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.TextBox.FontStyleProperty);

        //UwaElement.TextDecorations = TranslateFontDecorations(InvFont);
      }
    }
    private void TranslateFont(Inv.Font InvFont, Windows.UI.Xaml.Controls.PasswordBox UwaElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          UwaElement.FontFamily = new Windows.UI.Xaml.Media.FontFamily(InvFont.Name);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.PasswordBox.FontFamilyProperty);

        if (InvFont.Size != null)
          UwaElement.FontSize = InvFont.Size.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.PasswordBox.FontSizeProperty);

        var UwaBrush = TranslateMediaBrush(InvFont.Colour);

        if (UwaBrush != null)
          UwaElement.Foreground = UwaBrush;
        else
          UwaElement.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);

        if (InvFont.Weight != null)
          UwaElement.FontWeight = TranslateFontWeight(InvFont.Weight.Value);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.PasswordBox.FontWeightProperty);

        if (InvFont.Axis != null)
          Windows.UI.Xaml.Documents.Typography.SetVariants(UwaElement, TranslateFontAxisVariants(InvFont.Axis.Value));
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Documents.Typography.VariantsProperty);

        if (InvFont.IsSmallCaps != null)
          Windows.UI.Xaml.Documents.Typography.SetCapitals(UwaElement, InvFont.IsSmallCaps.Value ? Windows.UI.Xaml.FontCapitals.SmallCaps : Windows.UI.Xaml.FontCapitals.Normal);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Documents.Typography.CapitalsProperty);

        if (InvFont.IsItalics != null)
          UwaElement.FontStyle = InvFont.IsItalics.Value ? Windows.UI.Text.FontStyle.Italic : Windows.UI.Text.FontStyle.Normal;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.PasswordBox.FontStyleProperty);

        //UwaElement.TextDecorations = TranslateFontDecorations(InvFont);
      }
    }
    private void TranslateFont(Inv.Font InvFont, Windows.UI.Xaml.Controls.RichEditBox UwaElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          UwaElement.FontFamily = new Windows.UI.Xaml.Media.FontFamily(InvFont.Name);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.RichEditBox.FontFamilyProperty);

        if (InvFont.Size != null)
          UwaElement.FontSize = InvFont.Size.Value;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.RichEditBox.FontSizeProperty);

        var UwaBrush = InvFont.Colour == null ? Windows.UI.Colors.Black : TranslateMediaColour(InvFont.Colour);

        var DefaultFormat = UwaElement.Document.GetDefaultCharacterFormat();
        DefaultFormat.ForegroundColor = UwaBrush;
        UwaElement.Document.SetDefaultCharacterFormat(DefaultFormat);

        if (InvFont.Weight != null)
          UwaElement.FontWeight = TranslateFontWeight(InvFont.Weight.Value);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.RichEditBox.FontWeightProperty);

        if (InvFont.Axis != null)
          Windows.UI.Xaml.Documents.Typography.SetVariants(UwaElement, TranslateFontAxisVariants(InvFont.Axis.Value));
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Documents.Typography.VariantsProperty);

        if (InvFont.IsSmallCaps != null)
          Windows.UI.Xaml.Documents.Typography.SetCapitals(UwaElement, InvFont.IsSmallCaps.Value ? Windows.UI.Xaml.FontCapitals.SmallCaps : Windows.UI.Xaml.FontCapitals.Normal);
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Documents.Typography.CapitalsProperty);

        if (InvFont.IsItalics != null)
          UwaElement.FontStyle = InvFont.IsItalics.Value ? Windows.UI.Text.FontStyle.Italic : Windows.UI.Text.FontStyle.Normal;
        else
          UwaElement.ClearValue(Windows.UI.Xaml.Controls.RichEditBox.FontStyleProperty);

        //UwaElement.TextDecorations = TranslateFontDecorations(InvFont);
      }
    }
    private void TranslateFocus(Inv.Focus InvFocus, Windows.UI.Xaml.Controls.Control UwaElement)
    {
      if (InvFocus.Render())
      {
        // TODO: (dis)connect the GotFocus / LostFocus events from the control?
      }
    }
    private void TranslateHint(Windows.UI.Xaml.Controls.Control UwaElement, string Hint)
    {
      if (Hint != null)
        UwaElement.SetValue(Windows.UI.Xaml.Automation.AutomationProperties.NameProperty, Hint);
      else
        UwaElement.ClearValue(Windows.UI.Xaml.Automation.AutomationProperties.NameProperty);
    }
    private void TranslateTooltip(Inv.Tooltip InvTooltip, Windows.UI.Xaml.FrameworkElement UwaElement)
    {
      if (InvTooltip.Render())
      {
        if (InvTooltip.IsBound)
        {
          var UwaToolTip = Windows.UI.Xaml.Controls.ToolTipService.GetToolTip(UwaElement) as Windows.UI.Xaml.Controls.ToolTip;

          if (UwaToolTip == null)
          {
            // TODO: tooltip placement doesn't seem to work properly in UWP (or at least radically differs from WPF's implementation).

            UwaToolTip = new Windows.UI.Xaml.Controls.ToolTip()
            {
              //PlacementTarget = UwaElement,
              Placement = Windows.UI.Xaml.Controls.Primitives.PlacementMode.Bottom,
              Padding = new Windows.UI.Xaml.Thickness(0),
              VerticalOffset = 5,
              HorizontalOffset = 5
            };
            Windows.UI.Xaml.Controls.ToolTipService.SetToolTip(UwaElement, UwaToolTip);
            //Windows.UI.Xaml.Controls.ToolTipService.SetPlacement(UwaElement, Windows.UI.Xaml.Controls.Primitives.PlacementMode.Bottom);
            //Windows.UI.Xaml.Controls.ToolTipService.SetPlacementTarget(UwaElement, UwaElement);
            UwaToolTip.Opened += (Sender, Event) => Guard(() => InvTooltip.ShowInvoke());
            UwaToolTip.Closed += (Sender, Event) => Guard(() => InvTooltip.HideInvoke());
          }

          UwaToolTip.SafeSetContent(RoutePanel(InvTooltip.Content));
        }
        else
        {
          var UwaToolTip = Windows.UI.Xaml.Controls.ToolTipService.GetToolTip(UwaElement);

          if (UwaToolTip != null)
            Windows.UI.Xaml.Controls.ToolTipService.SetToolTip(UwaElement, null);
        }
      }
    }
    internal Windows.UI.Xaml.FontVariants TranslateFontAxisVariants(Inv.FontAxis InvFontAxis)
    {
      // When using FontVariants.Superscript in Run in XAML, only some characters are superscripted. Namely, numbers and the character n for example, but not other characters (x, q, f, ...).
      //
      // APPARENTLY: Typography.Variants isn't intended to force rendering of all characters as superscript characters - it only enables support for superscript characters the font itself actually declares as superscript variants.
      // Arial and Times New Roman don't actually have superscript or subscript support inside their fonts, where as Segoe UI does for all numbers and i & n.Calibri on the otherhand supports superscript for all base Latin letters, but not subscript.
      // It's up to the font to support the features in the Typography.Variants, as all that does is enable support for OpenType features the font declares.
      // You'll see the same behaviour using Typographic features at the DirectWrite level underneath (which makes sense given that's what UWP XAML uses).
      // So it's technically correct behaviour at a font level - you're asking for the Typographic Superscript feature that the font doesn't actually support, and as far as I'm aware forced / faked superscript has never been a feature in Microsoft XAML engines.

      return InvFontAxis switch
      {
        Inv.FontAxis.Baseline => Windows.UI.Xaml.FontVariants.Normal,
        Inv.FontAxis.Superscript => Windows.UI.Xaml.FontVariants.Superscript,
        Inv.FontAxis.Topline => Windows.UI.Xaml.FontVariants.Superscript,
        Inv.FontAxis.Subscript => Windows.UI.Xaml.FontVariants.Subscript,
        Inv.FontAxis.Bottomline => Windows.UI.Xaml.FontVariants.Subscript,
        _ => throw Inv.Support.EnumHelper.UnexpectedValueException(InvFontAxis),
      };
    }
    private Windows.UI.Text.TextDecorations TranslateFontDecorations(Inv.Font InvFont)
    {
      var Result = Windows.UI.Text.TextDecorations.None;

      if (InvFont.IsUnderlined ?? false)
        Result |= Windows.UI.Text.TextDecorations.Underline;

      if (InvFont.IsStrikethrough ?? false)
        Result |= Windows.UI.Text.TextDecorations.Strikethrough;

      return Result;
    }
    private Windows.UI.Xaml.GridLength TranslateTableLength(TableAxis InvTableLength, bool Horizontal)
    {
      return InvTableLength.LengthType switch
      {
        TableAxisLength.Auto => Windows.UI.Xaml.GridLength.Auto,
        TableAxisLength.Fixed => new Windows.UI.Xaml.GridLength(Horizontal ? InvTableLength.LengthValue : InvTableLength.LengthValue, Windows.UI.Xaml.GridUnitType.Pixel),
        TableAxisLength.Star => new Windows.UI.Xaml.GridLength(InvTableLength.LengthValue, Windows.UI.Xaml.GridUnitType.Star),
        _ => throw new Exception("Inv.TableLength not handled: " + InvTableLength.LengthType),
      };
    }
    private Inv.Key? TranslateKey(Windows.System.VirtualKey UwaKey)
    {
      if (KeyDictionary.TryGetValue(UwaKey, out var Result))
        return Result;
      else
        return null;
    }
    private Inv.DeviceTheme GetOperatingSystemTheme()
    {
      try
      {
        // NOTE this doesn't work:
        //return Windows.UI.Xaml.Application.Current.RequestedTheme == Windows.UI.Xaml.ApplicationTheme.Dark ? Inv.DeviceTheme.Dark : Inv.DeviceTheme.Light;

        var UISettings = new Windows.UI.ViewManagement.UISettings();

        var BackgroundColor = UISettings.GetColorValue(Windows.UI.ViewManagement.UIColorType.Background);

        // BackgroundColor == Black -> Dark Theme.
        // BackgroundColor == White -> White Theme.

        return BackgroundColor == Windows.UI.Colors.Black ? Inv.DeviceTheme.Dark : Inv.DeviceTheme.Light;
      }
      catch
      {
        // in case of access errors, default theme is not important enough to prevent startup.
      }

      return DeviceTheme.Light;
    }
    private Inv.Controller ResolveController(Windows.Gaming.Input.Gamepad Gamepad)
    {
      var Controller = InvApplication.Device.FindController(Gamepad);

      if (Controller == null)
      {
        Controller = new Inv.Controller(InvApplication.Device, new UwaController(Gamepad));
        Controller.UserId = Gamepad.User.NonRoamableId;

        Gamepad.UserChanged += (Sender, Event) => Controller.UserId = Event.User.NonRoamableId;

        InvApplication.Device.AddController(Controller);
      }

      return Controller;
    }
    private void Rendering(object Sender, object Event)
    {
      Guard(() => Process());
    }
    private void Suspending(object Sender, object Event)
    {
      Guard(() => InvApplication.SuspendInvoke());
    }
    private void Resuming(object Sender, object Event)
    {
      Guard(() => InvApplication.ResumeInvoke());
    }
    private void Exiting(object Sender, object Event)
    {
      // TODO: this is not running in the UI thread... so we have to wait for it to run before exiting -- NOTE the call to Wait() at the end.
      Call(() => InvApplication.StopInvoke());
    }
    private void Closed(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.CoreWindowEventArgs Event)
    {
      // TODO: doesn't seem to get called.
      Debug.WriteLine("closed");
    }
    private void SizeChanged(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.WindowSizeChangedEventArgs Event)
    {
      Resize();

      Guard(() =>
      {
        var InvSurface = InvApplication.Window.ActiveSurface;
        if (InvSurface != null)
          InvSurface.ArrangeInvoke();
      });
    }
    private void PointerPressed(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.PointerEventArgs Event)
    {
      Guard(() =>
      {
        var InvSurface = InvApplication.Window.ActiveSurface;

        if (InvSurface != null)
        {
          if (Event.CurrentPoint.Properties.IsXButton1Pressed && InvSurface.HasGestureBackward)
          {
            if (!IsTransitioning)
              InvSurface.GestureBackwardInvoke();
          }
          else if (Event.CurrentPoint.Properties.IsXButton2Pressed && InvSurface.HasGestureForward)
          {
            if (!IsTransitioning)
              InvSurface.GestureForwardInvoke();
          }
          else
          {
            var PointerPosition = Event.CurrentPoint.Position;

            LeftEdgeSwipe = InvSurface.HasGestureBackward && PointerPosition.X <= 10;
            RightEdgeSwipe = InvSurface.HasGestureForward && PointerPosition.X >= UwaPage.ActualWidth - 10;
          }
        }
      });
    }
    private void PointerReleased(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.PointerEventArgs Event)
    {
      LeftEdgeSwipe = false;
      RightEdgeSwipe = false;
    }
    private void PointerMoved(Windows.UI.Core.CoreWindow Sender, Windows.UI.Core.PointerEventArgs Event)
    {
      if (LeftEdgeSwipe || RightEdgeSwipe)
      {
        Guard(() =>
        {
          var InvSurface = InvApplication.Window.ActiveSurface;

          if (InvSurface != null && !IsTransitioning)
          {
            var PointerPosition = Event.CurrentPoint.Position;

            if (LeftEdgeSwipe && PointerPosition.X >= 0)
            {
              if (PointerPosition.X >= 20)
              {
                LeftEdgeSwipe = false;
                InvSurface.GestureBackwardInvoke();
              }
            }
            else if (RightEdgeSwipe && PointerPosition.X <= UwaPage.ActualWidth)
            {
              if (PointerPosition.X <= UwaPage.ActualWidth - 20)
              {
                RightEdgeSwipe = false;
                InvSurface.GestureForwardInvoke();
              }
            }
            else
            {
              LeftEdgeSwipe = false;
              RightEdgeSwipe = false;
            }
          }
          else
          {
            LeftEdgeSwipe = false;
            RightEdgeSwipe = false;
          }
        });

        Event.Handled = true;
      }
    }
    private void UnhandledErrorDetected(object Sender, Windows.ApplicationModel.Core.UnhandledErrorDetectedEventArgs Event)
    {
      // Unhandled errors result in the app being terminated once this event propagates to the Windows Runtime system.
      if (!Event.UnhandledError.Handled)
      {
        try
        {
          Event.UnhandledError.Propagate();
        }
        catch (Exception Exception)
        {
          HandleException(Exception);
        }
      }
    }
    private void PanelSizedChanged(object sender, Windows.UI.Xaml.SizeChangedEventArgs e)
    {
      Guard(() =>
      {
        var InvControl = (Inv.Control)(((Windows.UI.Xaml.Controls.Control)sender).Tag);
        InvControl?.AdjustInvoke();
      });
    }
    private void UnobservedTaskException(object Sender, System.Threading.Tasks.UnobservedTaskExceptionEventArgs Event)
    {
      var Exception = Event.Exception;
      if (Exception != null)
        HandleException(Exception);
    }

    private readonly Windows.UI.Xaml.Controls.Page UwaPage;
    private readonly Windows.UI.Xaml.Controls.Grid UwaMaster;
    private readonly Inv.EnumArray<Inv.ControlType, Func<Inv.Control, Windows.UI.Xaml.Controls.Control>> RouteArray;
    private readonly Inv.EnumArray<Inv.ControllerButtonType, Windows.Gaming.Input.GamepadButtons> GamepadButtonArray;
    private readonly Dictionary<Windows.System.VirtualKey, Key> KeyDictionary;
    private readonly Dictionary<Inv.Colour, Windows.UI.Xaml.Media.Brush> MediaBrushDictionary;
    private readonly Windows.Devices.Haptics.VibrationDevice UwaVibrationDevice;
    private Windows.UI.Xaml.Controls.Control LastInputElement;
    private bool LeftEdgeSwipe;
    private bool RightEdgeSwipe;
    private int TransitionCount;
  }

  internal sealed class UwaImageRecord
  {
    public int PixelWidth { get; set; }
    public int PixelHeight { get; set; }
    public byte[] PixelArray { get; set; }
  }

  internal sealed class UwaImageNode
  {
    public UwaImageNode(Windows.UI.Xaml.Media.Imaging.BitmapImage MediaImage, Microsoft.Graphics.Canvas.CanvasBitmap CanvasBitmap)
    {
      this.MediaImage = MediaImage;
      this.CanvasBitmap = CanvasBitmap;
    }

    public readonly Windows.UI.Xaml.Media.Imaging.BitmapImage MediaImage;
    public readonly Microsoft.Graphics.Canvas.CanvasBitmap CanvasBitmap;
  }

  internal static class UwaSoundPlayer
  {
    static UwaSoundPlayer()
    {
    }

    public static Windows.Media.Playback.MediaPlayer Play(Inv.Sound InvSound, float VolumeScale, float RateScale, float PanScale, bool Looped, bool OneShot)
    {
      if (InvSound == null)
        return null;

      Windows.Media.Playback.MediaPlayer MediaElement;

      void NewElement()
      {
        MediaElement = ProduceMediaElement(InvSound, VolumeScale, RateScale, PanScale, Looped, AutoPlay: true);
        if (OneShot)
          MediaElement.MediaEnded += (Sender, Event) => MediaElement.Dispose();
      }

      if (VolumeScale == 1.0F && RateScale == 1.0F && PanScale == 0.0F && !Looped)
      {
        MediaElement = GetElement(InvSound);

        // can only overlap the same sound effect by having two media elements.
        if (MediaElement.PlaybackSession.PlaybackState == Windows.Media.Playback.MediaPlaybackState.Playing)
          NewElement();
        else
          MediaElement.Play();
      }
      else
      {
        NewElement();
      }

      return MediaElement;
    }
    public static TimeSpan GetLength(Inv.Sound InvSound)
    {
      var MediaElement = GetElement(InvSound);

      if (MediaElement == null)
        return TimeSpan.Zero;

      // TODO: docs say NaturalDuration is not accurate until the Media Opened event has fired.
      //       how do you open a media element without playing it?

      return MediaElement.PlaybackSession.NaturalDuration;
    }

    private static Windows.Media.Playback.MediaPlayer GetElement(Inv.Sound InvSound)
    {
      if (InvSound == null)
        return null;

      var MediaElement = InvSound.Node as Windows.Media.Playback.MediaPlayer;

      if (MediaElement == null)
      {
        MediaElement = ProduceMediaElement(InvSound, 1.0F, 1.0F, 0.0F, false, AutoPlay: false);

        InvSound.Node = MediaElement;
      }

      return MediaElement;
    }
    private static Windows.Media.Playback.MediaPlayer ProduceMediaElement(Inv.Sound Sound, float Volume, float Rate, float Pan, bool Looped, bool AutoPlay)
    {
      var Result = new Windows.Media.Playback.MediaPlayer();

      var MemoryStream = new Windows.Storage.Streams.InMemoryRandomAccessStream();

      using (var DataWriter = new Windows.Storage.Streams.DataWriter(MemoryStream))
      {
        // Write the bytes to the stream
        DataWriter.WriteBytes(Sound.GetBuffer());

        // Store the bytes to the MemoryStream
        DataWriter.StoreAsync().AsTask().Wait();

        // Not necessary, but do it anyway
        DataWriter.FlushAsync().AsTask().Wait();

        // Detach from the Memory stream so we don't close it
        DataWriter.DetachStream();
      }

      Result.AutoPlay = AutoPlay;
      Result.Volume = Volume;
      Result.AudioBalance = Pan;
      Result.PlaybackSession.PlaybackRate = Rate;
      Result.IsLoopingEnabled = Looped;

      Result.Source = Windows.Media.Core.MediaSource.CreateFromStream(MemoryStream, Sound.GetFormat().EmptyOrWhitespaceAsNull() ?? "mp3");

      Result.MediaEnded += (Sender, Event) => Result.Pause();

      return Result;
    }
  }

  internal sealed class UwaController : Inv.ControllerContract
  {
    public UwaController(Windows.Gaming.Input.Gamepad Gamepad)
    {
      this.Gamepad = Gamepad;
    }

    public Windows.Gaming.Input.Gamepad Gamepad { get; }
    public Windows.Gaming.Input.GamepadReading LastReading { get; set; }

    object ControllerContract.Node => Gamepad;
    ControllerVibration ControllerContract.GetVibration()
    {
      return new ControllerVibration()
      {
        LeftMotor = Gamepad.Vibration.LeftMotor,
        RightMotor = Gamepad.Vibration.RightMotor,
        LeftTrigger = Gamepad.Vibration.LeftTrigger,
        RightTrigger = Gamepad.Vibration.RightTrigger,
      };
    }

    void ControllerContract.SetVibration(ControllerVibration Vibration)
    {
      // Remember that these two motors are not identical so setting these properties to the same value doesn't produce the same vibration in one motor as in the other. 
      // For any value, the left motor produces a stronger vibration at a lower frequency than the 
      // right motor which—for the same value—produces a gentler vibration at a higher frequency. 
      // Even at the maximum value, the left motor can't produce the high frequencies of the right motor, 
      // nor can the right motor produce the high forces of the left motor. Still, because the motors are rigidly connected by the gamepad body, 
      // players don't experience the vibrations fully independently even though the motors have different characteristics and can vibrate with different intensities. 
      // This arrangement allows for a wider, more expressive range of sensations to be produced than if the motors were identical.
      // 
      // 
      // Unlike the others, the two vibration motors inside the triggers are identical so they produce the same vibration in either motor for the same value. 
      // However, because these motors are not rigidly connected in any way, players experience the vibrations independently. 
      // This arrangement allows for fully independent sensations to be directed to both triggers simultaneously, 
      // and helps them to convey more specific information than the motors in the gamepad body can.

      Gamepad.Vibration = new Windows.Gaming.Input.GamepadVibration()
      {
        LeftMotor = Vibration.LeftMotor,
        RightMotor = Vibration.RightMotor,
        LeftTrigger = Vibration.LeftTrigger,
        RightTrigger = Vibration.RightTrigger,
      };
    }
  }
}