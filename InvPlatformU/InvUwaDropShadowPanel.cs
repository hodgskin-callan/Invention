﻿// ******************************************************************
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THE CODE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
// THE CODE OR THE USE OR OTHER DEALINGS IN THE CODE.
// ******************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  /// <summary>
  /// The <see cref="DropShadowPanel"/> control allows the creation of a DropShadow for any Xaml FrameworkElement in markup
  /// making it easier to add shadows to Xaml without having to directly drop down to Windows.UI.Composition APIs.
  /// </summary>
  [Windows.UI.Xaml.TemplatePart(Name = PartShadow, Type = typeof(Border))]
  internal partial class UwaDropShadowPanel : Windows.UI.Xaml.Controls.ContentControl
  {
    public UwaDropShadowPanel()
    {
      this.DefaultStyleKey = typeof(DropShadowPanel);

      var compositor = Windows.UI.Xaml.Hosting.ElementCompositionPreview.GetElementVisual(this).Compositor;

      _shadowVisual = compositor.CreateSpriteVisual();

      _dropShadow = compositor.CreateDropShadow();
      _shadowVisual.Shadow = _dropShadow;

      SizeChanged += OnSizeChanged;
    }

    protected override void OnApplyTemplate()
    {
      _border = GetTemplateChild(PartShadow) as Windows.UI.Xaml.Controls.Border;

      if (_border != null)
      {
        Windows.UI.Xaml.Hosting.ElementCompositionPreview.SetElementChildVisual(_border, _shadowVisual);
      }

      ConfigureShadowVisualForCastingElement();

      base.OnApplyTemplate();
    }

    private void OnSizeChanged(object sender, Windows.UI.Xaml.SizeChangedEventArgs e)
    {
      UpdateShadowSize();
    }
    private void ConfigureShadowVisualForCastingElement()
    {
      UpdateShadowMask();

      UpdateShadowSize();
    }
    private void OnBlurRadiusChanged(double newValue)
    {
      if (_dropShadow != null)
      {
        _dropShadow.BlurRadius = (float)newValue;
      }
    }
    private void OnColorChanged(Windows.UI.Color newValue)
    {
      if (_dropShadow != null)
      {
        _dropShadow.Color = newValue;
      }
    }
    private void OnOffsetXChanged(double newValue)
    {
      if (_dropShadow != null)
      {
        UpdateShadowOffset((float)newValue, _dropShadow.Offset.Y, _dropShadow.Offset.Z);
      }
    }
    private void OnOffsetYChanged(double newValue)
    {
      if (_dropShadow != null)
      {
        UpdateShadowOffset(_dropShadow.Offset.X, (float)newValue, _dropShadow.Offset.Z);
      }
    }
    private void OnOffsetZChanged(double newValue)
    {
      if (_dropShadow != null)
      {
        UpdateShadowOffset(_dropShadow.Offset.X, _dropShadow.Offset.Y, (float)newValue);
      }
    }
    private void OnShadowOpacityChanged(double newValue)
    {
      if (_dropShadow != null)
      {
        _dropShadow.Opacity = (float)newValue;
      }
    }
    private void UpdateShadowMask()
    {
      if (Content != null)
      {
        Windows.UI.Composition.CompositionBrush mask = null;

        if (Content is Windows.UI.Xaml.Controls.Image)
        {
          mask = ((Windows.UI.Xaml.Controls.Image)Content).GetAlphaMask();
        }
        else if (Content is Windows.UI.Xaml.Shapes.Shape)
        {
          mask = ((Windows.UI.Xaml.Shapes.Shape)Content).GetAlphaMask();
        }
        else if (Content is Windows.UI.Xaml.Controls.TextBlock)
        {
          mask = ((Windows.UI.Xaml.Controls.TextBlock)Content).GetAlphaMask();
        }
        //else if (Content is ImageExBase)
        //{
        //  var imageExBase = (ImageExBase)Content;
        //
        //  imageExBase.ImageExInitialized += ImageExInitialized;
        //
        //  if (imageExBase.IsInitialized)
        //  {
        //    imageExBase.ImageExInitialized -= ImageExInitialized;
        //
        //    mask = ((ImageExBase)Content).GetAlphaMask();
        //  }
        //}

        _dropShadow.Mask = mask;
      }
      else
      {
        _dropShadow.Mask = null;
      }
    }
    //private void ImageExInitialized(object sender, EventArgs e)
    //{
    //  var imageExBase = (ImageExBase)Content;
    //
    //  imageExBase.ImageExInitialized -= ImageExInitialized;
    //
    //  var mask = ((ImageExBase)Content).GetAlphaMask();
    //
    //  _dropShadow.Mask = mask;
    //}
    private void UpdateShadowOffset(float x, float y, float z)
    {
      if (_dropShadow != null)
      {
        _dropShadow.Offset = new System.Numerics.Vector3(x, y, z);
      }
    }
    private void UpdateShadowSize()
    {
      if (_shadowVisual != null)
      {
        var newSize = new System.Numerics.Vector2(0, 0);
        var contentFE = Content as Windows.UI.Xaml.FrameworkElement;
        if (contentFE != null)
        {
          newSize = new System.Numerics.Vector2((float)contentFE.ActualWidth, (float)contentFE.ActualHeight);
        }

        _shadowVisual.Size = newSize;
      }
    }

    private const string PartShadow = "ShadowElement";

    private readonly Windows.UI.Composition.DropShadow _dropShadow;
    private readonly Windows.UI.Composition.SpriteVisual _shadowVisual;
    private Windows.UI.Xaml.Controls.Border _border;
  }
}
