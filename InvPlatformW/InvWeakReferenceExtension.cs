﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Reflection;
using Inv.Support;

namespace Inv
{
  public static class WeakReferenceExtension
  {
    public static Action NewAction(Action Action, WeakReferenceExpireDelegate ExpireDelegate = null)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Action != null, "Action must be specified.");
        Inv.Assert.Check(!Action.Method.IsStatic && Action.Target != null, "Action must be an instance method.");
      }

      var Type = typeof(WeakReferenceAction<>).MakeGenericType(Action.Method.DeclaringType);
      var Constructor = Type.GetConstructor(new Type[] { typeof(Action), typeof(WeakReferenceExpireDelegate) });
      var Interface = (IWeakReferenceAction)Constructor.Invoke(new object[] { Action, ExpireDelegate });

      return Interface.Reference;
    }
    public static Action<P> NewAction<P>(Action<P> Action, WeakReferenceExpireDelegate<P> ExpireDelegate = null)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Action != null, "Action must be specified.");
        Inv.Assert.Check(!Action.Method.IsStatic && Action.Target != null, "Action must be an instance method.");
      }

      var Type = typeof(WeakReferenceAction<,>).MakeGenericType(Action.Method.DeclaringType, typeof(P));
      var Constructor = Type.GetConstructor(new Type[] { typeof(Action<P>), typeof(WeakReferenceExpireDelegate<P>) });
      var Interface = (IWeakReferenceAction<P>)Constructor.Invoke(new object[] { Action, ExpireDelegate });

      return Interface.Reference;
    }
    public static Action<P1, P2> NewAction<P1, P2>(Action<P1, P2> Action, WeakReferenceExpireDelegate<P1, P2> ExpireDelegate = null)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Action != null, "Action must be specified.");
        Inv.Assert.Check(!Action.Method.IsStatic && Action.Target != null, "Action must be an instance method.");
      }

      var Type = typeof(WeakReferenceAction<,,>).MakeGenericType(Action.Method.DeclaringType, typeof(P1), typeof(P2));
      var Constructor = Type.GetConstructor(new Type[] { typeof(Action<P1, P2>), typeof(WeakReferenceExpireDelegate<P1, P2>) });
      var Interface = (IWeakReferenceAction<P1, P2>)Constructor.Invoke(new object[] { Action, ExpireDelegate });

      return Interface.Reference;
    }
  }

  public delegate void WeakReferenceExpireDelegate(Action Action);

  public interface IWeakReferenceAction
  {
    Action Reference { get; }
  }

  public class WeakReferenceAction<T> : IWeakReferenceAction where T : class
  {
    public WeakReferenceAction(Action Action, WeakReferenceExpireDelegate ExpireDelegate)
    {
      Debug.Assert(Action.GetMethodInfo().DeclaringType == typeof(T), "Action method type must match generic argument.");
      Debug.Assert(ExpireDelegate == null || ExpireDelegate.GetMethodInfo().DeclaringType == typeof(T), "Expiry Method type must match generic argument.");

      this.ActionReference = new WeakReference(Action.Target);
      this.OpenAction = (OpenActionDelegate)Delegate.CreateDelegate(typeof(OpenActionDelegate), null, Action.GetMethodInfo());
      this.ActionDelegate = ActionMethod;

      if (ExpireDelegate != null)
      {
        this.ExpiryReference = new WeakReference(ExpireDelegate.Target);
        this.OpenExpiry = (OpenExpiryDelegate)Delegate.CreateDelegate(typeof(OpenExpiryDelegate), null, ExpireDelegate.GetMethodInfo());
      }
      else
      {
        this.ExpiryReference = null;
        this.OpenExpiry = null;
      }
    }

    private void ActionMethod()
    {
      var TargetObject = (T)ActionReference.Target;

      if (TargetObject != null)
      {
        OpenAction.Invoke(TargetObject);
      }
      else if (ExpiryReference != null)
      {
        var ExpiryObject = (T)ExpiryReference.Target;
        if (ExpiryObject != null)
          OpenExpiry.Invoke(ExpiryObject, ActionDelegate);
      }
    }

    Action IWeakReferenceAction.Reference
    {
      get { return ActionDelegate; }
    }

    private readonly WeakReference ActionReference;
    private readonly OpenActionDelegate OpenAction;
    private readonly Action ActionDelegate;
    private readonly WeakReference ExpiryReference;
    private readonly OpenExpiryDelegate OpenExpiry;

    private delegate void OpenActionDelegate(T @this);
    private delegate void OpenExpiryDelegate(T @this, Action Action);

    public static implicit operator Action(WeakReferenceAction<T> WeakReferenceAction)
    {
      return WeakReferenceAction.ActionDelegate;
    }
  }

  public delegate void WeakReferenceExpireDelegate<P>(Action<P> Action);

  public interface IWeakReferenceAction<P>
  {
    Action<P> Reference { get; }
  }

  public class WeakReferenceAction<T, P> : IWeakReferenceAction<P> where T : class
  {
    public WeakReferenceAction(Action<P> Action, WeakReferenceExpireDelegate<P> ExpireDelegate)
    {
      this.TargetReference = new WeakReference(Action.Target);
      this.OpenAction = (OpenActionDelegate)Delegate.CreateDelegate(typeof(OpenActionDelegate), null, Action.Method);
      this.ActionDelegate = ActionMethod;

      if (ExpireDelegate != null)
      {
        this.ExpiryReference = new WeakReference(ExpireDelegate.Target);
        this.OpenExpiry = (OpenExpiryDelegate)Delegate.CreateDelegate(typeof(OpenExpiryDelegate), null, ExpireDelegate.Method);
      }
      else
      {
        this.ExpiryReference = null;
        this.OpenExpiry = null;
      }
    }

    private void ActionMethod(P Package)
    {
      var TargetObject = (T)TargetReference.Target;

      if (TargetObject != null)
      {
        OpenAction.Invoke(TargetObject, Package);
      }
      else if (ExpiryReference != null)
      {
        var ExpiryObject = (T)ExpiryReference.Target;
        if (ExpiryObject != null)
          OpenExpiry.Invoke(ExpiryObject, ActionDelegate);
      }
    }

    Action<P> IWeakReferenceAction<P>.Reference
    {
      get { return ActionDelegate; }
    }

    private readonly WeakReference TargetReference;
    private readonly OpenActionDelegate OpenAction;
    private readonly Action<P> ActionDelegate;
    private readonly WeakReference ExpiryReference;
    private readonly OpenExpiryDelegate OpenExpiry;

    private delegate void OpenActionDelegate(T @this, P Package);
    private delegate void OpenExpiryDelegate(T @this, Action<P> Action);

    public static implicit operator Action<P>(WeakReferenceAction<T, P> WeakReferenceEvent)
    {
      return WeakReferenceEvent.ActionDelegate;
    }
  }

  public delegate void WeakReferenceExpireDelegate<P1, P2>(Action<P1, P2> Action);

  public interface IWeakReferenceAction<P1, P2>
  {
    Action<P1, P2> Reference { get; }
  }

  public class WeakReferenceAction<T, P1, P2> : IWeakReferenceAction<P1, P2> where T : class
  {
    public WeakReferenceAction(Action<P1, P2> Action, WeakReferenceExpireDelegate<P1, P2> ExpireDelegate)
    {
      this.TargetReference = new WeakReference(Action.Target);
      this.OpenAction = (OpenActionDelegate)Delegate.CreateDelegate(typeof(OpenActionDelegate), null, Action.Method);
      this.ActionDelegate = ActionMethod;

      if (ExpireDelegate != null)
      {
        this.ExpiryReference = new WeakReference(ExpireDelegate.Target);
        this.OpenExpiry = (OpenExpiryDelegate)Delegate.CreateDelegate(typeof(OpenExpiryDelegate), null, ExpireDelegate.Method);
      }
      else
      {
        this.ExpiryReference = null;
        this.OpenExpiry = null;
      }
    }

    private void ActionMethod(P1 Package1, P2 Package2)
    {
      var TargetObject = (T)TargetReference.Target;

      if (TargetObject != null)
      {
        OpenAction.Invoke(TargetObject, Package1, Package2);
      }
      else if (ExpiryReference != null)
      {
        var ExpiryObject = (T)ExpiryReference.Target;
        if (ExpiryObject != null)
          OpenExpiry.Invoke(ExpiryObject, ActionDelegate);
      }
    }

    Action<P1, P2> IWeakReferenceAction<P1, P2>.Reference
    {
      get { return ActionDelegate; }
    }

    private readonly WeakReference TargetReference;
    private readonly OpenActionDelegate OpenAction;
    private readonly Action<P1, P2> ActionDelegate;
    private readonly WeakReference ExpiryReference;
    private readonly OpenExpiryDelegate OpenExpiry;

    private delegate void OpenActionDelegate(T @this, P1 Package1, P2 Package2);
    private delegate void OpenExpiryDelegate(T @this, Action<P1, P2> Action);

    public static implicit operator Action<P1, P2>(WeakReferenceAction<T, P1, P2> WeakReferenceEvent)
    {
      return WeakReferenceEvent.ActionDelegate;
    }
  }
}