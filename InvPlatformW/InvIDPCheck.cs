﻿using System;
using System.Runtime.InteropServices;
using Inv.Support;

namespace Inv
{
  public static class IDPCheck
  {
    public static void Run()
    {
      var tSec = new Win32.Kernel32.SECURITY_ATTRIBUTES();
      tSec.nLength = Marshal.SizeOf(tSec);

      //Signal used to tell the thread to finish
      StopSignal = new ManualResetSignal(false, "IDPCheckStop");
      DoneSignal = new ManualResetSignal(false, "IDPCheckDone");

      //Use a native thread because managed threads are not guaranteed to use the same underlying thread
      //everytime and we want it to be stable in order to hide the thread from debuggers
      Handle = Win32.Kernel32.CreateThread(ref tSec, 0, CheckBody, IntPtr.Zero, 0, out var ThreadId);
    }
    public static void CleanUp()
    {
      if (StopSignal != null && DoneSignal != null)
      {
        StopSignal.Set();

        DoneSignal.WaitOne(TimeSpan.FromSeconds(2));

        if (Handle != IntPtr.Zero)
          Win32.Kernel32.CloseHandle(Handle);

        StopSignal.Dispose();
        DoneSignal.Dispose();

        StopSignal = null;
        DoneSignal = null;
      }
    }

    public static event Action<string> NotifyEvent;
    public static event Action<Exception> ExceptionEvent;

    private static void CheckBody()
    {
      try
      {
        if (Handle != IntPtr.Zero)
          Win32.Ntdll.NtSetInformationThread(Handle, Win32.Ntdll.ThreadInformationClass.ThreadHideFromDebugger, IntPtr.Zero, 0);

        var NotifyRandom = new Random();
        var ProcessHandle = System.Diagnostics.Process.GetCurrentProcess().Handle;
        var DebugPort = IntPtr.Zero;
        var PortInUse = new IntPtr(-1);

        while (!StopSignal.WaitOne(Timeout))
        {
          /* dnSpy has what appears to be function hooks to circumvent any IsDebuggerPresent checks so we need to go a little more undocumented and check if the process debug port is in use.
          * It is possible that this behaviour could change in the future so we want to maintain the the IsDebuggerPresent() check because that is well documented.
          * Another option could be to do our own version of IsDebuggerPresent by reading the process execution block (PEB) directly and checking the BeingDebugged flag but that would be a lot more work
          * and would also be prone to changing in the future.
          */
          var Status = Win32.Ntdll.NtQueryInformationProcess(ProcessHandle, Win32.Ntdll.PROCESSINFOCLASS.ProcessDebugPort, out DebugPort, Marshal.SizeOf(DebugPort), out var ReturnLength);

          //If the process debug port is in use if it returns 0xFFFFFFFF or -1 in decimal
          if (Win32.Kernel32.IsDebuggerPresent() || (Status == 0 /*success*/ && DebugPort == PortInUse))
          {
            try
            {
              NotifyEvent?.Invoke("Debugger Detected");
            }
            finally
            {
              //Randomly adjust the timing for obfuscation purposes
              System.Threading.Thread.Sleep(NotifyRandom.Next(100, 200));

              Environment.Exit(0);
            }
          }
        }

        DoneSignal.Set();
      }
      catch (Exception E) 
      {
        try { ExceptionEvent?.Invoke(E.Preserve()); } catch { }
      }
    }

    private static Inv.ManualResetSignal DoneSignal;
    private static Inv.ManualResetSignal StopSignal;
    private static IntPtr Handle;
    private static readonly int Timeout = 100;
  }
}
