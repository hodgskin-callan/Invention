﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public sealed class ApplicationManifest
  {
    public ApplicationManifest()
    {
      this.XmlDocument = new Inv.XmlDocument();
    }

    public bool IsValid => RootElement != null;

    public void LoadFromFile(string FileName)
    {
      XmlDocument.ReadFromFile(FileName);

      this.RootElement = XmlDocument.RootElement;
      if (!RootElement.LocalName.Equals(AssemblyElementName, StringComparison.InvariantCultureIgnoreCase) || !RootElement.Namespace.Equals(AssemblyElementNamespace, StringComparison.InvariantCultureIgnoreCase))
        this.RootElement = null;
    }
    public void SaveToFile(string FileName)
    {
      if (RootElement == null)
        return;

      XmlDocument.WriteToFile(FileName);
    }

    public bool? GetGdiScaling()
    {
      if (RootElement == null)
        return false;

      return GetGdiScalingElement()?.Content.Equals("true", StringComparison.InvariantCultureIgnoreCase);
    }
    public void SetGdiScaling(bool Value)
    {
      if (RootElement == null)
        return;

      GetGdiScalingElement(Force: true).Content = Value.ToString().ToLower();
    }

    private Inv.XmlElement GetGdiScalingElement(bool Force = false)
    {
      var ApplicationElement = RootElement.GetChildElements().Find(E => E.Namespace == ApplicationElementNamespace && E.LocalName == ApplicationElementName);

      if (ApplicationElement == null && Force)
        ApplicationElement = RootElement.AddChildElement(ApplicationElementPrefix, ApplicationElementName, ApplicationElementNamespace);

      var WindowsSettingsElement = ApplicationElement?.GetChildElements().Find(E =>
        E.LocalName == WindowsSettingsElementName &&
        E.Namespace == WindowsSettingsElementNamespace);

      if (WindowsSettingsElement == null)
      {
        WindowsSettingsElement = ApplicationElement?.GetChildElements().Find(E =>
          E.LocalName.Equals(WindowsSettingsElementName, StringComparison.InvariantCultureIgnoreCase) &&
          E.GetAttributes().Any(A =>
            (A.LocalName?.Equals(XmlNamespaceName, StringComparison.InvariantCultureIgnoreCase) ?? false) &&
            (A.Value?.Equals(WindowsSettingsElementNamespace, StringComparison.InvariantCultureIgnoreCase) ?? false)
          )
        );
      }

      if (WindowsSettingsElement == null && Force)
        WindowsSettingsElement = ApplicationElement.AddChildElement(ApplicationElementPrefix, WindowsSettingsElementName, WindowsSettingsElementNamespace);

      var Result = WindowsSettingsElement?.GetChildElements().Find(E => E.LocalName == GdiScalingElementName);

      if (Result == null && Force)
        Result = WindowsSettingsElement.AddChildElement(GdiScalingElementName);

      return Result;
    }

    private readonly Inv.XmlDocument XmlDocument;
    private Inv.XmlElement RootElement;

    private const string XmlNamespaceName = "xmlns";
    private const string AssemblyElementName = "assembly";
    private const string AssemblyElementNamespace = "urn:schemas-microsoft-com:asm.v1";
    private const string ApplicationElementPrefix = "asmv3";
    private const string ApplicationElementNamespace = "urn:schemas-microsoft-com:asm.v3";
    private const string ApplicationElementName = "application";
    private const string WindowsSettingsElementNamespace = @"http://schemas.microsoft.com/SMI/2017/WindowsSettings";
    private const string WindowsSettingsElementName = "windowsSettings";
    private const string GdiScalingElementName = "gdiScaling";
  }
}