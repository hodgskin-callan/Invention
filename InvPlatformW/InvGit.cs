﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Inv
{
  public static class Git
  {
    public static string GetPath()
    {
      if (System.IO.File.Exists(StandalonePath))
        return Inv.Git.StandalonePath;

      var ForkPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"Fork\gitInstance");
      if (System.IO.Directory.Exists(ForkPath))
      {
        var VersionList = new List<ForkVersion>();

        foreach (var VersionDirectory in System.IO.Directory.GetDirectories(ForkPath))
        {
          // Not actually a file, but this removes all the parent folders from the string.
          var Version = ForkVersion.FromFolder(Path.GetFileName(VersionDirectory));

          if (Version != null)
            VersionList.Add(Version);
        }

        var LatestVersion = VersionList.OrderBy(V => V.Major).ThenBy(V => V.Minor).ThenBy(V => V.Build).ThenBy(V => V.Release).LastOrDefault();
        if (LatestVersion != null)
        {
          var ForkVersionPath = Path.Combine(ForkPath, LatestVersion.Folder, "bin", "git.exe");
          if (System.IO.File.Exists(ForkVersionPath))
            return ForkVersionPath;
        }
      }

      return string.Empty;
    }

    public const string StandalonePath = @"C:\Program Files\Git\bin\git.exe";

    private sealed class ForkVersion
    {
      public string Folder { get; set; }
      public int Major { get; set; }
      public int Minor { get; set; }
      public int Build { get; set; }
      public int Release { get; set; }

      public static ForkVersion FromFolder(string Folder)
      {
        var FolderPartArray = Folder.Split('.');

        if (FolderPartArray.Length < 3 || FolderPartArray.Length > 4)
          return null;

        if (!int.TryParse(FolderPartArray[0], out var Major) || !int.TryParse(FolderPartArray[1], out var Minor) || !int.TryParse(FolderPartArray[2], out var Build))
          return null;

        var Release = 0;
        if (FolderPartArray.Length == 4 && !int.TryParse(FolderPartArray[0], out Release))
          return null;

        return new ForkVersion()
        {
          Folder = Folder,
          Major = Major,
          Minor = Minor,
          Build = Build,
          Release = Release
        };
      }
    }
  }
}