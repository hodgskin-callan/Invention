﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace Inv
{
  internal sealed class WpfVault
  {
    public WpfVault()
    {
      var MainAssembly = Assembly.GetEntryAssembly();

      if (MainAssembly != null)
        this.Configuration = ConfigurationManager.OpenExeConfiguration(MainAssembly.Location);
    }

    public void Save(Secret Secret)
    {
      var Serialized = Secret.Serialize();
      var Bytes = Encoding.UTF8.GetBytes(Serialized);
      var Protected = Protect(Bytes);
      var Encoded = Convert.ToBase64String(Protected);

      Configuration.AppSettings.Settings.Remove(Secret.Name);
      Configuration.AppSettings.Settings.Add(Secret.Name, Encoded);
      Configuration.Save();
    }
    public void Load(Secret Secret)
    {
      string Serialized = null;

      var Element = Configuration.AppSettings.Settings[Secret.Name];

      if (Element != null)
      {
        var Encoded = Element.Value;
        var Protected = Convert.FromBase64String(Encoded);
        var Bytes = Unprotect(Protected);

        if (Bytes != null)
          Serialized = Encoding.UTF8.GetString(Bytes);
      }

      if (Serialized != null)
        Secret.Deserialize(Serialized);
      else
        Secret.Properties.Clear();
    }
    public void Delete(Secret Secret)
    {
      Configuration.AppSettings.Settings.Remove(Secret.Name);
      Configuration.Save();
    }

    private static byte[] Protect(byte[] data)
    {
      try
      {
        // Encrypt the data using DataProtectionScope.CurrentUser. The result can be decrypted only by the same current user.
        return ProtectedData.Protect(data, Entropy, DataProtectionScope.CurrentUser);
      }
      catch (CryptographicException e)
      {
        Debug.WriteLine("Data was not encrypted. An error occurred.");
        Debug.WriteLine(e.ToString());
        return null;
      }
    }
    private static byte[] Unprotect(byte[] data)
    {
      try
      {
        //Decrypt the data using DataProtectionScope.CurrentUser.
        return ProtectedData.Unprotect(data, Entropy, DataProtectionScope.CurrentUser);
      }
      catch (CryptographicException e)
      {
        Debug.WriteLine("Data was not decrypted. An error occurred.");
        Debug.WriteLine(e.ToString());
        return null;
      }
    }

    private static readonly byte[] Entropy = { 9, 8, 7, 6, 5 };
    private readonly System.Configuration.Configuration Configuration;
  }
}
