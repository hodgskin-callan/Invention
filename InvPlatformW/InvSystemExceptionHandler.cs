﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  public static class SystemExceptionHandler
  {
    /// <summary>
    /// Register a global exception handler for otherwise unhandled exceptions.
    /// </summary>
    /// <param name="ExceptionAction"></param>
    public static void Recruit(Action<object> ExceptionAction)
    {
      if (ExceptionAction == null)
        return;

      lock (CriticalSection)
      {
        // can't recruit if already added.
        if (ExceptionDelegate != null && ExceptionDelegate.GetInvocationList().Contains(ExceptionAction))
          return;

        if (ExceptionDelegate == null)
        {
          System.AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionEventHandler;
          System.Threading.Tasks.TaskScheduler.UnobservedTaskException += UnhandledTaskExceptionEventHandler;
        }

        ExceptionDelegate += ExceptionAction;
      }
    }
    /// <summary>
    /// Remove a global exception handler for otherwise unhandled exceptions.
    /// </summary>
    /// <param name="ExceptionAction"></param>
    public static void Dismiss(Action<object> ExceptionAction)
    {
      if (ExceptionAction == null)
        return;

      lock (CriticalSection)
      {
        // can't dismiss if not added.
        if (ExceptionDelegate == null || !ExceptionDelegate.GetInvocationList().Contains(ExceptionAction))
          return;

        ExceptionDelegate -= ExceptionAction;

        if (ExceptionDelegate == null)
        {
          System.AppDomain.CurrentDomain.UnhandledException -= UnhandledExceptionEventHandler;
          System.Threading.Tasks.TaskScheduler.UnobservedTaskException -= UnhandledTaskExceptionEventHandler;
        }
      }
    }
    /// <summary>
    /// Route a manually caught exception through the recruited exception handlers.
    /// </summary>
    /// <param name="Exception"></param>
    public static void Route(Exception Exception)
    {
      ExceptionInvoke(Exception);
    }

    private static void UnhandledExceptionEventHandler(object sender, UnhandledExceptionEventArgs e)
    {
      ExceptionInvoke(e.ExceptionObject);
    }
    private static void UnhandledTaskExceptionEventHandler(object sender, System.Threading.Tasks.UnobservedTaskExceptionEventArgs e)
    {
      if (!e.Observed)
      {
        ExceptionInvoke(e.Exception);

        e.SetObserved();
      }
    }
    private static void ExceptionInvoke(object Object)
    {
      if (Object == null)
        return;

      Action<object> LocalDelegate;
      lock (CriticalSection)
        LocalDelegate = ExceptionDelegate;

      LocalDelegate?.Invoke(Object);
    }

    private static readonly object CriticalSection = new object();
    private static Action<object> ExceptionDelegate;
  }
}
