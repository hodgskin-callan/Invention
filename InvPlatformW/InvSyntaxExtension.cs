﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Inv.Support;

namespace Inv.Syntax
{
  public static class Extension
  {
    public static void AppendTextFile(Inv.Syntax.Grammar SyntaxGrammar, string FilePath, Action<Inv.Syntax.Writer> Action)
    {
      AppendFileStream(FilePath, FileStream => Inv.Syntax.Foundation.WriteTextStream(SyntaxGrammar, FileStream, Action));
    }
    public static void WriteTextFile(Inv.Syntax.Grammar SyntaxGrammar, string FilePath, Action<Inv.Syntax.Writer> Action)
    {
      WriteFileStream(FilePath, FileStream => Inv.Syntax.Foundation.WriteTextStream(SyntaxGrammar, FileStream, Action));
    }
    public static void ReadTextFile(Inv.Syntax.Grammar SyntaxGrammar, string FilePath, Action<Inv.Syntax.Reader> Action)
    {
      ReadFileStream(FilePath, FileStream => Inv.Syntax.Foundation.ReadTextStream(SyntaxGrammar, FileStream, Action, FilePath));
    }

    public static void WriteBinaryFile(Inv.Syntax.Grammar SyntaxGrammar, string FilePath, Action<Inv.Syntax.Writer> Action)
    {
      WriteFileStream(FilePath, FileStream => Inv.Syntax.Foundation.WriteBinaryStream(SyntaxGrammar, FileStream, Action));
    }
    public static void ReadBinaryFile(Inv.Syntax.Grammar SyntaxGrammar, string FilePath, Action<Inv.Syntax.Reader> Action)
    {
      ReadFileStream(FilePath, FileStream => Inv.Syntax.Foundation.ReadBinaryStream(SyntaxGrammar, FileStream, Action));
    }

    public static void WriteFileStream(string FilePath, Action<FileStream> Action)
    {
      using (var FileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read))
        Action(FileStream);
    }
    public static void ReadFileStream(string FilePath, Action<FileStream> Action)
    {
      using (var FileStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read, 65536))
        Action(FileStream);
    }
    public static void AppendFileStream(string FilePath, Action<FileStream> Action)
    {
      using (var FileStream = new FileStream(FilePath, FileMode.Append, FileAccess.Write, FileShare.Read))
        Action(FileStream);
    }
  }
}
