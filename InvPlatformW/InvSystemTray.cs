﻿using System;
using System.Diagnostics;
using System.Windows.Interop;
using System.Linq;
using Inv.Support;
using System.Runtime.InteropServices;

namespace Inv
{
  public sealed class SystemTray
  {
    public SystemTray()
    {
    }

    public string Tooltip
    {
      get => TooltipField;
      set
      {
        if (TooltipField != value)
        {
          this.TooltipField = value;

          if (IsVisible)
            SetIcon(Win32.Shell32.NotifyIconMessage.NIM_MODIFY);
        }
      }
    }
    public IntPtr IconHandle { get; set; }
    public IntPtr WindowHandle { get; set; }

    public void Show()
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(WindowHandle != IntPtr.Zero, $"{nameof(WindowHandle)} must be provided");
        Inv.Assert.Check(IconHandle != IntPtr.Zero, $"{nameof(IconHandle)} must be provided");
      }

      if (WindowHandle != IntPtr.Zero && IconHandle != IntPtr.Zero && !IsVisible)
      {
        SetIcon(Win32.Shell32.NotifyIconMessage.NIM_ADD);

        var Source = HwndSource.FromHwnd(WindowHandle);
        Source.AddHook(new HwndSourceHook(WndProc));
      }
    }
    public void Hide()
    {
      if (IsVisible)
        SetIcon(Win32.Shell32.NotifyIconMessage.NIM_DELETE);
    }
    public SystemToast NewToast()
    {
      return new SystemToast(this);
    }

    internal bool IsVisible { get; private set; }
    internal const int Identifier = 9999998;
    internal const int WindowsMessage = 0x8001;

    internal void ShowToast(Inv.SystemToast SystemToast)
    {
      this.RouteToast = SystemToast;
    }

    private void SetIcon(Win32.Shell32.NotifyIconMessage Message)
    {
      var NotifyIconData = new Win32.Shell32.NotifyIconData();
      NotifyIconData.cbSize = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Win32.Shell32.NotifyIconData));
      NotifyIconData.hwnd = WindowHandle;
      NotifyIconData.uID = Identifier;
      NotifyIconData.uFlags = Win32.Shell32.NotifyIconFlag.NIF_ICON;
      NotifyIconData.hIcon = IconHandle;

      if (!string.IsNullOrWhiteSpace(TooltipField))
      {
        NotifyIconData.uFlags |= Win32.Shell32.NotifyIconFlag.NIF_TIP;
        NotifyIconData.szTip = TooltipField;
      }

      if (!Win32.Shell32.Shell_NotifyIcon(Message, ref NotifyIconData))
      {
        // KJV 2022-02-25
        // We do not check the success or failure of this API call.
        // Microsoft's documentation does not explain under which conditions this call may fail other than due to incorrect API usage.
        // One possible cause of failure is that the Windows message sent from shell32.dll to explorer.exe has timed out,
        //  or Windows has deemed that the explorer.exe process has hung (hasn't processed a message in 5s).
        // For these reasons, consumers of this API anecdotally do not check for success or failure.
        // See https://www.geoffchappell.com/notes/windows/shell/missingicons.htm for more details.

        // if we are deleting the icon, probably no point throwing 'unspecified error' exceptions.
        //if (Message != Shell32.NotifyIconMessage.NIM_DELETE)
        //  throw new System.ComponentModel.Win32Exception(System.Runtime.InteropServices.Marshal.GetLastWin32Error());
      }

      IsVisible = Message == Win32.Shell32.NotifyIconMessage.NIM_ADD || Message == Win32.Shell32.NotifyIconMessage.NIM_MODIFY;
    }
    private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
    {
      if (msg != WindowsMessage)
        return IntPtr.Zero;

      if (wParam == IntPtr.Zero || wParam.ToInt32() != Identifier)
        return IntPtr.Zero;

      //Debug.WriteLine($"{lParam.ToInt32().ToString("X")}");

      if (lParam == IntPtr.Zero || lParam.ToInt32() != Win32.User32.NIN_BALLOONUSERCLICK)
        return IntPtr.Zero;

      var Toast = RouteToast;
      if (Toast != null)
        Toast.SingleTapInvoke();

      return IntPtr.Zero;
    }

    private string TooltipField;
    private Inv.SystemToast RouteToast;
  }

  public sealed class SystemToast
  {
    internal SystemToast(Inv.SystemTray SystemTray)
    {
      this.SystemTray = SystemTray;
    }

    public string Title { get; set; }
    public string Text { get; set; }
    // 2019-08-06 KV
    // IconHandle successfully working depends on the machine's system metrics, which isn't ideal.
    // Commented out to avoid someone accidentally exposing this issue.
    // see https://www.geoffchappell.com/studies/windows/shell/shell32/api/shlnot/notifyicon.htm for more details
    //public IntPtr IconHandle { get; set; }
    public event Action SingleTapEvent;

    public void ShowError()
    {
      Show(Win32.Shell32.NotifyIconInfotipFlag.NIIF_ERROR);
    }
    public void ShowWarning()
    {
      Show(Win32.Shell32.NotifyIconInfotipFlag.NIIF_WARNING);
    }
    public void ShowInformation()
    {
      Show(Win32.Shell32.NotifyIconInfotipFlag.NIIF_INFO);
    }

    internal void SingleTapInvoke()
    {
      SingleTapEvent?.Invoke();
    }

    private void Show(Win32.Shell32.NotifyIconInfotipFlag Flag)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(SystemTray.IsVisible, $"System tray has not been shown");

      void Fallback() => System.Windows.MessageBox.Show(Text, Title, System.Windows.MessageBoxButton.OK);

      if (!SystemTray.IsVisible)
      {
        Fallback();
        return;
      }

      var NotifyIconData = new Win32.Shell32.NotifyIconData();
      NotifyIconData.cbSize = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Win32.Shell32.NotifyIconData));
      NotifyIconData.hwnd = SystemTray.WindowHandle;
      NotifyIconData.uID = SystemTray.Identifier;
      NotifyIconData.uFlags = Win32.Shell32.NotifyIconFlag.NIF_ICON | Win32.Shell32.NotifyIconFlag.NIF_INFO;
      NotifyIconData.hIcon = SystemTray.IconHandle;
      NotifyIconData.szInfo = Text;
      NotifyIconData.szInfoTitle = Title;

      if (SingleTapEvent != null)
      {
        NotifyIconData.uFlags |= Win32.Shell32.NotifyIconFlag.NIF_MESSAGE;
        NotifyIconData.uCallbackMessage = SystemTray.WindowsMessage;
      }

      NotifyIconData.dwInfoFlags = Flag;
      NotifyIconData.hBalloonIcon = IntPtr.Zero;

      SystemTray.ShowToast(this);

      if (!Win32.Shell32.Shell_NotifyIcon(Win32.Shell32.NotifyIconMessage.NIM_MODIFY, ref NotifyIconData))
        Fallback();
    }

    private readonly Inv.SystemTray SystemTray;
  }
}