﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Win32
{
  public static class Mapi32
  {
    [DllImport("MAPI32.DLL", CharSet = CharSet.Ansi)]
    public static extern int MAPILogon(IntPtr hwnd, string prf, string pw, int flg, int rsv, ref IntPtr sess);
    [DllImport("MAPI32.DLL")]
    public static extern int MAPILogoff(IntPtr sess, IntPtr hwnd, int flg, int rsv);

    public const int MapiORIG = 0;
    public const int MapiTO = 1;
    public const int MapiCC = 2;
    public const int MapiBCC = 3;

    [DllImport("MAPI32.DLL")]
    public static extern int MAPISendMail(IntPtr sess, IntPtr hwnd, MapiMessage message, int flg, int rsv);

    public const int MapiLogonUI = 0x00000001;
    public const int MapiPasswordUI = 0x00020000;
    public const int MapiNewSession = 0x00000002;
    public const int MapiDialog = 0x00000008;
    public const int MapiDialogModeless = 0x00000004 | MapiDialog;
    public const int MapiForceDownload = 0x00001000;
    public const int MapiExtendedUI = 0x00000020;
    public const int MapiForceUnicode = 0x00040000;

    [DllImport("MAPI32.DLL", CharSet = CharSet.Ansi)]
    public static extern int MAPIFindNext(IntPtr sess, IntPtr hwnd, string typ, string seed, int flg, int rsv, StringBuilder id);

    public const int MapiUnreadOnly = 0x00000020;
    public const int MapiGuaranteeFiFo = 0x00000100;
    public const int MapiLongMsgID = 0x00004000;

    [DllImport("MAPI32.DLL", CharSet = CharSet.Ansi)]
    public static extern int MAPIReadMail(IntPtr sess, IntPtr hwnd, string id, int flg, int rsv, ref IntPtr ptrmsg);

    [DllImport("MAPI32.DLL")]
    public static extern int MAPIFreeBuffer(IntPtr ptr);

    [DllImport("MAPI32.DLL", CharSet = CharSet.Ansi)]
    public static extern int MAPIDeleteMail(IntPtr sess, IntPtr hwnd, string id, int flg, int rsv);

    public const int MapiPeek = 0x00000080;
    public const int MapiSuprAttach = 0x00000800;
    public const int MapiEnvOnly = 0x00000040;
    public const int MapiBodyAsFile = 0x00000200;

    public const int MapiUnread = 0x00000001;
    public const int MapiReceiptReq = 0x00000002;
    public const int MapiSent = 0x00000004;

    [DllImport("MAPI32.DLL", CharSet = CharSet.Ansi)]
    public static extern int MAPIAddress(IntPtr sess, IntPtr hwnd, string caption, int editfld, string labels, int recipcount, IntPtr ptrrecips, int flg, int rsv, ref int newrec, ref IntPtr ptrnew);

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public class MapiMessage
    {
      public int reserved;
      public string subject;
      public string noteText;
      public string messageType;
      public string dateReceived;
      public string conversationID;
      public int flags;
      public IntPtr originator;		// MapiRecipDesc* [1]
      public int recipCount;
      public IntPtr recips;			// MapiRecipDesc* [n]
      public int fileCount;
      public IntPtr files;			// MapiFileDesc*  [n]
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public class MapiRecipDesc
    {
      public int reserved;
      public int recipClass;
      public string name;
      public string address;
      public int eIDSize;
      public IntPtr entryID;			// void*
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public class MapiFileDesc
    {
      public int reserved;
      public int flags;
      public int position;
      public string path;
      public string name;
      public IntPtr type;
    }
  }
}
