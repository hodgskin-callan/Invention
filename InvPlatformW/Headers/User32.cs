﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace Win32
{
  public sealed class User32
  {
    public enum MapType : uint
    {
      MAPVK_VK_TO_VSC = 0x0,
      MAPVK_VSC_TO_VK = 0x1,
      MAPVK_VK_TO_CHAR = 0x2,
      MAPVK_VSC_TO_VK_EX = 0x3,
    }

    [DllImport("user32.dll")]
    public static extern int ToUnicode(
        uint wVirtKey,
        uint wScanCode,
        byte[] lpKeyState,
        [Out, MarshalAs(UnmanagedType.LPWStr, SizeParamIndex = 4)] 
            StringBuilder pwszBuff,
        int cchBuff,
        uint wFlags);

    [DllImport("user32.dll")]
    public static extern bool GetKeyboardState(byte[] lpKeyState);

    [DllImport("user32.dll")]
    public static extern uint MapVirtualKey(uint uCode, MapType uMapType);

    [DllImport("user32.dll")]
    public static extern int TrackMouseEvent(ref TRACKMOUSEEVENT lpEventTrack);
    [StructLayout(LayoutKind.Sequential)]
    public struct TRACKMOUSEEVENT
    {
      public Int32 cbSize;    // using Int32 instead of UInt32 is safe here, and this avoids casting the result  of Marshal.SizeOf()
      [MarshalAs(UnmanagedType.U4)]
      public TMEFlags dwFlags;
      public IntPtr hWnd;
      public UInt32 dwHoverTime;

      public TRACKMOUSEEVENT(TMEFlags dwFlags, IntPtr hWnd, UInt32 dwHoverTime)
      {
        this.cbSize = Marshal.SizeOf(typeof(TRACKMOUSEEVENT));
        this.dwFlags = dwFlags;
        this.hWnd = hWnd;
        this.dwHoverTime = dwHoverTime;
      }
    }

    /// <summary>
    /// The services requested. This member can be a combination of the following values. 
    /// 
    /// See also: http://msdn.microsoft.com/en-us/library/ms645604%28v=vs.85%29.aspx
    /// </summary>
    [Flags]
    public enum TMEFlags : uint
    {
      /// <summary>
      /// The caller wants to cancel a prior tracking request. The caller should also specify the type of tracking that it wants to cancel. For example, to cancel hover tracking, the caller must pass the TME_CANCEL and TME_HOVER flags.
      /// </summary>
      TME_CANCEL = 0x80000000,
      /// <summary>
      /// The caller wants hover notification. Notification is delivered as a WM_MOUSEHOVER message.
      /// If the caller requests hover tracking while hover tracking is already active, the hover timer will be reset.
      /// This flag is ignored if the mouse pointer is not over the specified window or area.
      /// </summary>
      TME_HOVER = 0x00000001,
      /// <summary>
      /// The caller wants leave notification. Notification is delivered as a WM_MOUSELEAVE message. If the mouse is not over the specified window or area, a leave notification is generated immediately and no further tracking is performed.
      /// </summary>
      TME_LEAVE = 0x00000002,
      /// <summary>
      /// The caller wants hover and leave notification for the nonclient areas. Notification is delivered as WM_NCMOUSEHOVER and WM_NCMOUSELEAVE messages.
      /// </summary>
      TME_NONCLIENT = 0x00000010,
      /// <summary>
      /// The function fills in the structure instead of treating it as a tracking request. The structure is filled such that had that structure been passed to TrackMouseEvent, it would generate the current tracking. The only anomaly is that the hover time-out returned is always the actual time-out and not HOVER_DEFAULT, if HOVER_DEFAULT was specified during the original TrackMouseEvent request. 
      /// </summary>
      TME_QUERY = 0x40000000,
    }

    public static void ProtectedLockWindowUpdate(IntPtr hWndLock)
    {
      Debug.Assert(!IsLockWindowUpdateInProgress, "Only one LockWindowUpdate pair may be active at any time.");

      LockWindowUpdate(hWndLock);
      IsLockWindowUpdateInProgress = true;
    }
    public static void ProtectedUnlockWindowUpdate()
    {
      Debug.Assert(IsLockWindowUpdateInProgress, "ProtectedLockWindowUpdate has not been called.");

      LockWindowUpdate(IntPtr.Zero);
      IsLockWindowUpdateInProgress = false;
    }

    private static volatile bool IsLockWindowUpdateInProgress;

    /// <summary>
    /// Synthesizes keystrokes, mouse motions, and button clicks.
    /// </summary>
    [DllImport("user32.dll", SetLastError = true)]
    public static extern uint SendInput(uint numberOfInputs, INPUT[] inputs, int sizeOfInputStructure);

    #region VirtualKeyCodes
    public enum VirtualKeyCode : ushort // UInt16
    {
      /// <summary>
      /// Left mouse button
      /// </summary>
      LBUTTON = 0x01,

      /// <summary>
      /// Right mouse button
      /// </summary>
      RBUTTON = 0x02,

      /// <summary>
      /// Control-break processing
      /// </summary>
      CANCEL = 0x03,

      /// <summary>
      /// Middle mouse button (three-button mouse) - NOT contiguous with LBUTTON and RBUTTON
      /// </summary>
      MBUTTON = 0x04,

      /// <summary>
      /// Windows 2000/XP: X1 mouse button - NOT contiguous with LBUTTON and RBUTTON
      /// </summary>
      XBUTTON1 = 0x05,

      /// <summary>
      /// Windows 2000/XP: X2 mouse button - NOT contiguous with LBUTTON and RBUTTON
      /// </summary>
      XBUTTON2 = 0x06,

      // 0x07 : Undefined

      /// <summary>
      /// BACKSPACE key
      /// </summary>
      BACK = 0x08,

      /// <summary>
      /// TAB key
      /// </summary>
      TAB = 0x09,

      // 0x0A - 0x0B : Reserved

      /// <summary>
      /// CLEAR key
      /// </summary>
      CLEAR = 0x0C,

      /// <summary>
      /// ENTER key
      /// </summary>
      RETURN = 0x0D,

      // 0x0E - 0x0F : Undefined

      /// <summary>
      /// SHIFT key
      /// </summary>
      SHIFT = 0x10,

      /// <summary>
      /// CTRL key
      /// </summary>
      CONTROL = 0x11,

      /// <summary>
      /// ALT key
      /// </summary>
      MENU = 0x12,

      /// <summary>
      /// PAUSE key
      /// </summary>
      PAUSE = 0x13,

      /// <summary>
      /// CAPS LOCK key
      /// </summary>
      CAPITAL = 0x14,

      /// <summary>
      /// Input Method Editor (IME) Kana mode
      /// </summary>
      KANA = 0x15,

      /// <summary>
      /// IME Hanguel mode (maintained for compatibility; use HANGUL)
      /// </summary>
      HANGEUL = 0x15,

      /// <summary>
      /// IME Hangul mode
      /// </summary>
      HANGUL = 0x15,

      // 0x16 : Undefined

      /// <summary>
      /// IME Junja mode
      /// </summary>
      JUNJA = 0x17,

      /// <summary>
      /// IME final mode
      /// </summary>
      FINAL = 0x18,

      /// <summary>
      /// IME Hanja mode
      /// </summary>
      HANJA = 0x19,

      /// <summary>
      /// IME Kanji mode
      /// </summary>
      KANJI = 0x19,

      // 0x1A : Undefined

      /// <summary>
      /// ESC key
      /// </summary>
      ESCAPE = 0x1B,

      /// <summary>
      /// IME convert
      /// </summary>
      CONVERT = 0x1C,

      /// <summary>
      /// IME nonconvert
      /// </summary>
      NONCONVERT = 0x1D,

      /// <summary>
      /// IME accept
      /// </summary>
      ACCEPT = 0x1E,

      /// <summary>
      /// IME mode change request
      /// </summary>
      MODECHANGE = 0x1F,

      /// <summary>
      /// SPACEBAR
      /// </summary>
      SPACE = 0x20,

      /// <summary>
      /// PAGE UP key
      /// </summary>
      PRIOR = 0x21,

      /// <summary>
      /// PAGE DOWN key
      /// </summary>
      NEXT = 0x22,

      /// <summary>
      /// END key
      /// </summary>
      END = 0x23,

      /// <summary>
      /// HOME key
      /// </summary>
      HOME = 0x24,

      /// <summary>
      /// LEFT ARROW key
      /// </summary>
      LEFT = 0x25,

      /// <summary>
      /// UP ARROW key
      /// </summary>
      UP = 0x26,

      /// <summary>
      /// RIGHT ARROW key
      /// </summary>
      RIGHT = 0x27,

      /// <summary>
      /// DOWN ARROW key
      /// </summary>
      DOWN = 0x28,

      /// <summary>
      /// SELECT key
      /// </summary>
      SELECT = 0x29,

      /// <summary>
      /// PRINT key
      /// </summary>
      PRINT = 0x2A,

      /// <summary>
      /// EXECUTE key
      /// </summary>
      EXECUTE = 0x2B,

      /// <summary>
      /// PRINT SCREEN key
      /// </summary>
      SNAPSHOT = 0x2C,

      /// <summary>
      /// INS key
      /// </summary>
      INSERT = 0x2D,

      /// <summary>
      /// DEL key
      /// </summary>
      DELETE = 0x2E,

      /// <summary>
      /// HELP key
      /// </summary>
      HELP = 0x2F,

      /// <summary>
      /// 0 key
      /// </summary>
      VK_0 = 0x30,

      /// <summary>
      /// 1 key
      /// </summary>
      VK_1 = 0x31,

      /// <summary>
      /// 2 key
      /// </summary>
      VK_2 = 0x32,

      /// <summary>
      /// 3 key
      /// </summary>
      VK_3 = 0x33,

      /// <summary>
      /// 4 key
      /// </summary>
      VK_4 = 0x34,

      /// <summary>
      /// 5 key
      /// </summary>
      VK_5 = 0x35,

      /// <summary>
      /// 6 key
      /// </summary>
      VK_6 = 0x36,

      /// <summary>
      /// 7 key
      /// </summary>
      VK_7 = 0x37,

      /// <summary>
      /// 8 key
      /// </summary>
      VK_8 = 0x38,

      /// <summary>
      /// 9 key
      /// </summary>
      VK_9 = 0x39,

      //
      // 0x3A - 0x40 : Udefined
      //

      /// <summary>
      /// A key
      /// </summary>
      VK_A = 0x41,

      /// <summary>
      /// B key
      /// </summary>
      VK_B = 0x42,

      /// <summary>
      /// C key
      /// </summary>
      VK_C = 0x43,

      /// <summary>
      /// D key
      /// </summary>
      VK_D = 0x44,

      /// <summary>
      /// E key
      /// </summary>
      VK_E = 0x45,

      /// <summary>
      /// F key
      /// </summary>
      VK_F = 0x46,

      /// <summary>
      /// G key
      /// </summary>
      VK_G = 0x47,

      /// <summary>
      /// H key
      /// </summary>
      VK_H = 0x48,

      /// <summary>
      /// I key
      /// </summary>
      VK_I = 0x49,

      /// <summary>
      /// J key
      /// </summary>
      VK_J = 0x4A,

      /// <summary>
      /// K key
      /// </summary>
      VK_K = 0x4B,

      /// <summary>
      /// L key
      /// </summary>
      VK_L = 0x4C,

      /// <summary>
      /// M key
      /// </summary>
      VK_M = 0x4D,

      /// <summary>
      /// N key
      /// </summary>
      VK_N = 0x4E,

      /// <summary>
      /// O key
      /// </summary>
      VK_O = 0x4F,

      /// <summary>
      /// P key
      /// </summary>
      VK_P = 0x50,

      /// <summary>
      /// Q key
      /// </summary>
      VK_Q = 0x51,

      /// <summary>
      /// R key
      /// </summary>
      VK_R = 0x52,

      /// <summary>
      /// S key
      /// </summary>
      VK_S = 0x53,

      /// <summary>
      /// T key
      /// </summary>
      VK_T = 0x54,

      /// <summary>
      /// U key
      /// </summary>
      VK_U = 0x55,

      /// <summary>
      /// V key
      /// </summary>
      VK_V = 0x56,

      /// <summary>
      /// W key
      /// </summary>
      VK_W = 0x57,

      /// <summary>
      /// X key
      /// </summary>
      VK_X = 0x58,

      /// <summary>
      /// Y key
      /// </summary>
      VK_Y = 0x59,

      /// <summary>
      /// Z key
      /// </summary>
      VK_Z = 0x5A,

      /// <summary>
      /// Left Windows key (Microsoft Natural keyboard)
      /// </summary>
      LWIN = 0x5B,

      /// <summary>
      /// Right Windows key (Natural keyboard)
      /// </summary>
      RWIN = 0x5C,

      /// <summary>
      /// Applications key (Natural keyboard)
      /// </summary>
      APPS = 0x5D,

      // 0x5E : reserved

      /// <summary>
      /// Computer Sleep key
      /// </summary>
      SLEEP = 0x5F,

      /// <summary>
      /// Numeric keypad 0 key
      /// </summary>
      NUMPAD0 = 0x60,

      /// <summary>
      /// Numeric keypad 1 key
      /// </summary>
      NUMPAD1 = 0x61,

      /// <summary>
      /// Numeric keypad 2 key
      /// </summary>
      NUMPAD2 = 0x62,

      /// <summary>
      /// Numeric keypad 3 key
      /// </summary>
      NUMPAD3 = 0x63,

      /// <summary>
      /// Numeric keypad 4 key
      /// </summary>
      NUMPAD4 = 0x64,

      /// <summary>
      /// Numeric keypad 5 key
      /// </summary>
      NUMPAD5 = 0x65,

      /// <summary>
      /// Numeric keypad 6 key
      /// </summary>
      NUMPAD6 = 0x66,

      /// <summary>
      /// Numeric keypad 7 key
      /// </summary>
      NUMPAD7 = 0x67,

      /// <summary>
      /// Numeric keypad 8 key
      /// </summary>
      NUMPAD8 = 0x68,

      /// <summary>
      /// Numeric keypad 9 key
      /// </summary>
      NUMPAD9 = 0x69,

      /// <summary>
      /// Multiply key
      /// </summary>
      MULTIPLY = 0x6A,

      /// <summary>
      /// Add key
      /// </summary>
      ADD = 0x6B,

      /// <summary>
      /// Separator key
      /// </summary>
      SEPARATOR = 0x6C,

      /// <summary>
      /// Subtract key
      /// </summary>
      SUBTRACT = 0x6D,

      /// <summary>
      /// Decimal key
      /// </summary>
      DECIMAL = 0x6E,

      /// <summary>
      /// Divide key
      /// </summary>
      DIVIDE = 0x6F,

      /// <summary>
      /// F1 key
      /// </summary>
      F1 = 0x70,

      /// <summary>
      /// F2 key
      /// </summary>
      F2 = 0x71,

      /// <summary>
      /// F3 key
      /// </summary>
      F3 = 0x72,

      /// <summary>
      /// F4 key
      /// </summary>
      F4 = 0x73,

      /// <summary>
      /// F5 key
      /// </summary>
      F5 = 0x74,

      /// <summary>
      /// F6 key
      /// </summary>
      F6 = 0x75,

      /// <summary>
      /// F7 key
      /// </summary>
      F7 = 0x76,

      /// <summary>
      /// F8 key
      /// </summary>
      F8 = 0x77,

      /// <summary>
      /// F9 key
      /// </summary>
      F9 = 0x78,

      /// <summary>
      /// F10 key
      /// </summary>
      F10 = 0x79,

      /// <summary>
      /// F11 key
      /// </summary>
      F11 = 0x7A,

      /// <summary>
      /// F12 key
      /// </summary>
      F12 = 0x7B,

      /// <summary>
      /// F13 key
      /// </summary>
      F13 = 0x7C,

      /// <summary>
      /// F14 key
      /// </summary>
      F14 = 0x7D,

      /// <summary>
      /// F15 key
      /// </summary>
      F15 = 0x7E,

      /// <summary>
      /// F16 key
      /// </summary>
      F16 = 0x7F,

      /// <summary>
      /// F17 key
      /// </summary>
      F17 = 0x80,

      /// <summary>
      /// F18 key
      /// </summary>
      F18 = 0x81,

      /// <summary>
      /// F19 key
      /// </summary>
      F19 = 0x82,

      /// <summary>
      /// F20 key
      /// </summary>
      F20 = 0x83,

      /// <summary>
      /// F21 key
      /// </summary>
      F21 = 0x84,

      /// <summary>
      /// F22 key
      /// </summary>
      F22 = 0x85,

      /// <summary>
      /// F23 key
      /// </summary>
      F23 = 0x86,

      /// <summary>
      /// F24 key
      /// </summary>
      F24 = 0x87,

      //
      // 0x88 - 0x8F : Unassigned
      //

      /// <summary>
      /// NUM LOCK key
      /// </summary>
      NUMLOCK = 0x90,

      /// <summary>
      /// SCROLL LOCK key
      /// </summary>
      SCROLL = 0x91,

      // 0x92 - 0x96 : OEM Specific

      // 0x97 - 0x9F : Unassigned

      //
      // L* & R* - left and right Alt, Ctrl and Shift virtual keys.
      // Used only as parameters to GetAsyncKeyState() and GetKeyState().
      // No other API or message will distinguish left and right keys in this way.
      //

      /// <summary>
      /// Left SHIFT key - Used only as parameters to GetAsyncKeyState() and GetKeyState()
      /// </summary>
      LSHIFT = 0xA0,

      /// <summary>
      /// Right SHIFT key - Used only as parameters to GetAsyncKeyState() and GetKeyState()
      /// </summary>
      RSHIFT = 0xA1,

      /// <summary>
      /// Left CONTROL key - Used only as parameters to GetAsyncKeyState() and GetKeyState()
      /// </summary>
      LCONTROL = 0xA2,

      /// <summary>
      /// Right CONTROL key - Used only as parameters to GetAsyncKeyState() and GetKeyState()
      /// </summary>
      RCONTROL = 0xA3,

      /// <summary>
      /// Left MENU key - Used only as parameters to GetAsyncKeyState() and GetKeyState()
      /// </summary>
      LMENU = 0xA4,

      /// <summary>
      /// Right MENU key - Used only as parameters to GetAsyncKeyState() and GetKeyState()
      /// </summary>
      RMENU = 0xA5,

      /// <summary>
      /// Windows 2000/XP: Browser Back key
      /// </summary>
      BROWSER_BACK = 0xA6,

      /// <summary>
      /// Windows 2000/XP: Browser Forward key
      /// </summary>
      BROWSER_FORWARD = 0xA7,

      /// <summary>
      /// Windows 2000/XP: Browser Refresh key
      /// </summary>
      BROWSER_REFRESH = 0xA8,

      /// <summary>
      /// Windows 2000/XP: Browser Stop key
      /// </summary>
      BROWSER_STOP = 0xA9,

      /// <summary>
      /// Windows 2000/XP: Browser Search key
      /// </summary>
      BROWSER_SEARCH = 0xAA,

      /// <summary>
      /// Windows 2000/XP: Browser Favorites key
      /// </summary>
      BROWSER_FAVORITES = 0xAB,

      /// <summary>
      /// Windows 2000/XP: Browser Start and Home key
      /// </summary>
      BROWSER_HOME = 0xAC,

      /// <summary>
      /// Windows 2000/XP: Volume Mute key
      /// </summary>
      VOLUME_MUTE = 0xAD,

      /// <summary>
      /// Windows 2000/XP: Volume Down key
      /// </summary>
      VOLUME_DOWN = 0xAE,

      /// <summary>
      /// Windows 2000/XP: Volume Up key
      /// </summary>
      VOLUME_UP = 0xAF,

      /// <summary>
      /// Windows 2000/XP: Next Track key
      /// </summary>
      MEDIA_NEXT_TRACK = 0xB0,

      /// <summary>
      /// Windows 2000/XP: Previous Track key
      /// </summary>
      MEDIA_PREV_TRACK = 0xB1,

      /// <summary>
      /// Windows 2000/XP: Stop Media key
      /// </summary>
      MEDIA_STOP = 0xB2,

      /// <summary>
      /// Windows 2000/XP: Play/Pause Media key
      /// </summary>
      MEDIA_PLAY_PAUSE = 0xB3,

      /// <summary>
      /// Windows 2000/XP: Start Mail key
      /// </summary>
      LAUNCH_MAIL = 0xB4,

      /// <summary>
      /// Windows 2000/XP: Select Media key
      /// </summary>
      LAUNCH_MEDIA_SELECT = 0xB5,

      /// <summary>
      /// Windows 2000/XP: Start Application 1 key
      /// </summary>
      LAUNCH_APP1 = 0xB6,

      /// <summary>
      /// Windows 2000/XP: Start Application 2 key
      /// </summary>
      LAUNCH_APP2 = 0xB7,

      //
      // 0xB8 - 0xB9 : Reserved
      //

      /// <summary>
      /// Used for miscellaneous characters; it can vary by keyboard. Windows 2000/XP: For the US standard keyboard, the ';:' key 
      /// </summary>
      OEM_1 = 0xBA,

      /// <summary>
      /// Windows 2000/XP: For any country/region, the '+' key
      /// </summary>
      OEM_PLUS = 0xBB,

      /// <summary>
      /// Windows 2000/XP: For any country/region, the ',' key
      /// </summary>
      OEM_COMMA = 0xBC,

      /// <summary>
      /// Windows 2000/XP: For any country/region, the '-' key
      /// </summary>
      OEM_MINUS = 0xBD,

      /// <summary>
      /// Windows 2000/XP: For any country/region, the '.' key
      /// </summary>
      OEM_PERIOD = 0xBE,

      /// <summary>
      /// Used for miscellaneous characters; it can vary by keyboard. Windows 2000/XP: For the US standard keyboard, the '/?' key 
      /// </summary>
      OEM_2 = 0xBF,

      /// <summary>
      /// Used for miscellaneous characters; it can vary by keyboard. Windows 2000/XP: For the US standard keyboard, the '`~' key 
      /// </summary>
      OEM_3 = 0xC0,

      //
      // 0xC1 - 0xD7 : Reserved
      //

      //
      // 0xD8 - 0xDA : Unassigned
      //

      /// <summary>
      /// Used for miscellaneous characters; it can vary by keyboard. Windows 2000/XP: For the US standard keyboard, the '[{' key
      /// </summary>
      OEM_4 = 0xDB,

      /// <summary>
      /// Used for miscellaneous characters; it can vary by keyboard. Windows 2000/XP: For the US standard keyboard, the '\|' key
      /// </summary>
      OEM_5 = 0xDC,

      /// <summary>
      /// Used for miscellaneous characters; it can vary by keyboard. Windows 2000/XP: For the US standard keyboard, the ']}' key
      /// </summary>
      OEM_6 = 0xDD,

      /// <summary>
      /// Used for miscellaneous characters; it can vary by keyboard. Windows 2000/XP: For the US standard keyboard, the 'single-quote/double-quote' key
      /// </summary>
      OEM_7 = 0xDE,

      /// <summary>
      /// Used for miscellaneous characters; it can vary by keyboard.
      /// </summary>
      OEM_8 = 0xDF,

      //
      // 0xE0 : Reserved
      //

      //
      // 0xE1 : OEM Specific
      //

      /// <summary>
      /// Windows 2000/XP: Either the angle bracket key or the backslash key on the RT 102-key keyboard
      /// </summary>
      OEM_102 = 0xE2,

      //
      // (0xE3-E4) : OEM specific
      //

      /// <summary>
      /// Windows 95/98/Me, Windows NT 4.0, Windows 2000/XP: IME PROCESS key
      /// </summary>
      PROCESSKEY = 0xE5,

      //
      // 0xE6 : OEM specific
      //

      /// <summary>
      /// Windows 2000/XP: Used to pass Unicode characters as if they were keystrokes. The PACKET key is the low word of a 32-bit Virtual Key value used for non-keyboard input methods. For more information, see Remark in KEYBDINPUT, SendInput, WM_KEYDOWN, and WM_KEYUP
      /// </summary>
      PACKET = 0xE7,

      //
      // 0xE8 : Unassigned
      //

      //
      // 0xE9-F5 : OEM specific
      //

      /// <summary>
      /// Attn key
      /// </summary>
      ATTN = 0xF6,

      /// <summary>
      /// CrSel key
      /// </summary>
      CRSEL = 0xF7,

      /// <summary>
      /// ExSel key
      /// </summary>
      EXSEL = 0xF8,

      /// <summary>
      /// Erase EOF key
      /// </summary>
      EREOF = 0xF9,

      /// <summary>
      /// Play key
      /// </summary>
      PLAY = 0xFA,

      /// <summary>
      /// Zoom key
      /// </summary>
      ZOOM = 0xFB,

      /// <summary>
      /// Reserved
      /// </summary>
      NONAME = 0xFC,

      /// <summary>
      /// PA1 key
      /// </summary>
      PA1 = 0xFD,

      /// <summary>
      /// Clear key
      /// </summary>
      OEM_CLEAR = 0xFE,
    }
    #endregion

    /// <summary>
    /// Used by SendInput to store information for synthesizing input events such as keystrokes, mouse movement, and mouse clicks.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct INPUT
    {
      public uint type;
      public InputUnion data;

      public static int Size
      {
        get { return Marshal.SizeOf(typeof(INPUT)); }
      }
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct InputUnion
    {
      [FieldOffset(0)]
      public MOUSEINPUT mi;
      [FieldOffset(0)]
      public KEYBDINPUT ki;
      [FieldOffset(0)]
      public HARDWAREINPUT hi;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MOUSEINPUT
    {
      internal int dx;
      internal int dy;
      internal uint mouseData;
      internal uint dwFlags;
      internal uint time;
      internal UIntPtr dwExtraInfo;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct KEYBDINPUT
    {
      public ushort wVk;
      public ushort wScan;
      public KEYEVENTF dwFlags;
      public int time;
      public UIntPtr dwExtraInfo;
    }

    /// <summary>
    /// Specifies various aspects of a keystroke. This member can be certain combinations of the following values.
    /// </summary>
    [Flags]
    public enum KEYEVENTF : uint
    {
      EXTENDEDKEY = 0x0001,
      KEYUP = 0x0002,
      SCANCODE = 0x0008,
      UNICODE = 0x0004
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct HARDWAREINPUT
    {
      internal int uMsg;
      internal short wParamL;
      internal short wParamH;
    }

    [DllImport("user32.dll")]
    internal static extern bool LockWindowUpdate(IntPtr hWndLock);

    [DllImport("user32.dll")]
    public static extern IntPtr GetActiveWindow();

    [DllImport("user32.dll", ExactSpelling = true)]
    public static extern int GetMessagePos();

    [DllImport("user32.dll", ExactSpelling = true)]
    public static extern int GetMessageTime();

    [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
    public static extern bool EnableWindow(IntPtr hWnd, bool enable);

    [DllImport("user32.dll", ExactSpelling = true)]
    public static extern bool IsWindowEnabled(IntPtr hWnd);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool SetForegroundWindow(IntPtr hWnd);

    [DllImport("user32.dll")]
    public static extern IntPtr GetDC(IntPtr hWnd);

    [DllImport("user32.dll")]
    public static extern int ReleaseDC(IntPtr hWnd, IntPtr hDC);

    //[DllImport("user32.dll", CharSet = CharSet.Auto)]
    //public static extern bool GetMonitorInfo(IntPtr hMonitor, ref MONITORINFOEX lpmi);
    [DllImport("User32.dll", CharSet = CharSet.Auto)]
    public static extern bool GetMonitorInfo(IntPtr hmonitor, [In, Out]MonitorInfoEx info);
    [DllImport("user32.dll")]
    public static extern bool GetMonitorInfo(IntPtr hMonitor, ref MonitorInfo lpmi);
    [DllImport("user32.dll")]
    public static extern IntPtr MonitorFromWindow(IntPtr handle, int flags);
    [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
    public static extern IntPtr MonitorFromPoint(System.Drawing.Point pt, UInt32 dwFlags);

    [DllImport("user32.dll")]
    public static extern bool EnumDisplayMonitors(IntPtr hdc, IntPtr lprcClip, MonitorEnumDelegate lpfnEnum, IntPtr dwData);

    public delegate bool MonitorEnumDelegate(IntPtr hMonitor, IntPtr hdcMonitor, ref Inv.Rect lprcMonitor, IntPtr dwData);

    public static class MonitorConstants
    {
      public const int MONITOR_DEFAULTTONULL = 0;
      public const int MONITOR_DEFAULTTOPRIMARY = 1;
      public const int MONITOR_DEFAULTTONEAREST = 2;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MonitorInfo
    {
      /// <summary>
      /// The size, in bytes, of the structure. Set this member to sizeof(MONITORINFO) (40) before calling the GetMonitorInfo function. 
      /// Doing so lets the function determine the type of structure you are passing to it.
      /// </summary>
      public int Size;
      /// <summary>
      /// A RECT structure that specifies the display monitor rectangle, expressed in virtual-screen coordinates. 
      /// Note that if the monitor is not the primary display monitor, some of the rectangle's coordinates may be negative values.
      /// </summary>
      public RectStruct Monitor;
      /// <summary>
      /// A RECT structure that specifies the work area rectangle of the display monitor that can be used by applications, 
      /// expressed in virtual-screen coordinates. Windows uses this rectangle to maximize an application on the monitor. 
      /// The rest of the area in rcMonitor contains system windows such as the task bar and side bars. 
      /// Note that if the monitor is not the primary display monitor, some of the rectangle's coordinates may be negative values.
      /// </summary>
      public RectStruct WorkArea;
      /// <summary>
      /// The attributes of the display monitor.
      /// 
      /// This member can be the following value:
      ///   1 : MONITORINFOF_PRIMARY
      /// </summary>
      public uint Flags;

      public void Init()
      {
        this.Size = 40;
      }
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto, Pack = 4)]
    public class MonitorInfoEx
    {
      public int cbSize = Marshal.SizeOf(typeof(MonitorInfoEx));
      public RectStruct rcMonitor = new RectStruct();
      public RectStruct rcWork = new RectStruct();
      public int dwFlags = 0;
      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
      public char[] szDevice = new char[32];
    }

    /// <summary>
    /// The RECT structure defines the coordinates of the upper-left and lower-right corners of a rectangle.
    /// </summary>
    /// <remarks>
    /// By convention, the right and bottom edges of the rectangle are normally considered exclusive. 
    /// In other words, the pixel whose coordinates are ( right, bottom ) lies immediately outside of the rectangle. 
    /// For example, when RECT is passed to the FillRect function, the rectangle is filled up to, but not including, 
    /// the right column and bottom row of pixels. This structure is identical to the RECTL structure.
    /// 
    /// See http://msdn.microsoft.com/en-us/library/dd162897%28VS.85%29.aspx
    /// </remarks>
    [StructLayout(LayoutKind.Sequential)]
    public struct RectStruct
    {
      /// <summary>
      /// The x-coordinate of the upper-left corner of the rectangle.
      /// </summary>
      public int Left;

      /// <summary>
      /// The y-coordinate of the upper-left corner of the rectangle.
      /// </summary>
      public int Top;

      /// <summary>
      /// The x-coordinate of the lower-right corner of the rectangle.
      /// </summary>
      public int Right;

      /// <summary>
      /// The y-coordinate of the lower-right corner of the rectangle.
      /// </summary>
      public int Bottom;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MINMAXINFO
    {
      public POINT ptReserved;
      public POINT ptMaxSize;
      public POINT ptMaxPosition;
      public POINT ptMinTrackSize;
      public POINT ptMaxTrackSize;
    }

    [Flags]
    public enum SetWindowPosFlags : uint
    {
      /// <summary>
      ///     If the calling thread and the thread that owns the window are attached to different input queues, the system posts the request to the thread that owns the window. This prevents the calling thread from blocking its execution while other threads process the request.
      /// </summary>
      SWP_ASYNCWINDOWPOS = 0x4000,
      /// <summary>
      ///     Prevents generation of the WM_SYNCPAINT message.
      /// </summary>
      SWP_DEFERERASE = 0x2000,
      /// <summary>
      ///     Draws a frame (defined in the window's class description) around the window.
      /// </summary>
      SWP_DRAWFRAME = 0x0020,
      /// <summary>
      ///     Applies new frame styles set using the SetWindowLong function. Sends a WM_NCCALCSIZE message to the window, even if the window's size is not being changed. If this flag is not specified, WM_NCCALCSIZE is sent only when the window's size is being changed.
      /// </summary>
      SWP_FRAMECHANGED = 0x0020,
      /// <summary>
      ///     Hides the window.
      /// </summary>
      SWP_HIDEWINDOW = 0x0080,
      /// <summary>
      ///     Does not activate the window. If this flag is not set, the window is activated and moved to the top of either the topmost or non-topmost group (depending on the setting of the hWndInsertAfter parameter).
      /// </summary>
      SWP_NOACTIVATE = 0x0010,
      /// <summary>
      ///     Discards the entire contents of the client area. If this flag is not specified, the valid contents of the client area are saved and copied back into the client area after the window is sized or repositioned.
      /// </summary>
      SWP_NOCOPYBITS = 0x0100,
      /// <summary>
      ///     Retains the current position (ignores X and Y parameters).
      /// </summary>
      SWP_NOMOVE = 0x0002,
      /// <summary>
      ///     Does not change the owner window's position in the Z order.
      /// </summary>
      SWP_NOOWNERZORDER = 0x0200,
      /// <summary>
      ///     Does not redraw changes. If this flag is set, no repainting of any kind occurs. This applies to the client area, the nonclient area (including the title bar and scroll bars), and any part of the parent window uncovered as a result of the window being moved. When this flag is set, the application must explicitly invalidate or redraw any parts of the window and parent window that need redrawing.
      /// </summary>
      SWP_NOREDRAW = 0x0008,
      /// <summary>
      ///     Same as the SWP_NOOWNERZORDER flag.
      /// </summary>
      SWP_NOREPOSITION = 0x0200,
      /// <summary>
      ///     Prevents the window from receiving the WM_WINDOWPOSCHANGING message.
      /// </summary>
      SWP_NOSENDCHANGING = 0x0400,
      /// <summary>
      ///     Retains the current size (ignores the cx and cy parameters).
      /// </summary>
      SWP_NOSIZE = 0x0001,
      /// <summary>
      ///     Retains the current Z order (ignores the hWndInsertAfter parameter).
      /// </summary>
      SWP_NOZORDER = 0x0004,
      /// <summary>
      ///     Displays the window.
      /// </summary>
      SWP_SHOWWINDOW = 0x0040,
    }

    private struct LastInputInfo
    {
      public uint cbSize;
      public uint dwTime;
    }

    [DllImport("User32.dll")]
    private static extern bool GetLastInputInfo(ref LastInputInfo plii);

    [DllImport("User32.dll")]
    public static extern bool LockWorkStation();

    [DllImport("user32.dll")]
    //[return: System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)]
    private static extern bool FlashWindowEx(ref FlashWindowInformation pwfi);

    [StructLayout(LayoutKind.Sequential)]
    private struct FlashWindowInformation
    {
      public uint StructSize;
      public IntPtr WindowHandle;
      public uint FlashStatus;
      public uint FlashCount;
      public uint FlashRate;
    }

    public static long GetIdleTickCount()
    {
      var LastInputInfo = new LastInputInfo();

      LastInputInfo.cbSize = (uint)Marshal.SizeOf(LastInputInfo);

      if (!GetLastInputInfo(ref LastInputInfo))
        throw new Exception(Marshal.GetLastWin32Error().ToString());

      return Environment.TickCount - LastInputInfo.dwTime;
    }
    public static long GetLastActiveTickCount()
    {
      var LastInputInfo = new LastInputInfo();

      LastInputInfo.cbSize = (uint)Marshal.SizeOf(LastInputInfo);

      if (!GetLastInputInfo(ref LastInputInfo))
        throw new Exception(Marshal.GetLastWin32Error().ToString());

      return LastInputInfo.dwTime;
    }
    public static bool StartFlashWindow(IntPtr WindowHandle)
    {
      var FlashWindowInformation = new FlashWindowInformation();
      FlashWindowInformation.StructSize = Convert.ToUInt32(System.Runtime.InteropServices.Marshal.SizeOf(FlashWindowInformation));
      FlashWindowInformation.WindowHandle = WindowHandle;
      FlashWindowInformation.FlashStatus = 3;
      FlashWindowInformation.FlashCount = uint.MaxValue;
      FlashWindowInformation.FlashRate = 0;

      return FlashWindowEx(ref FlashWindowInformation);
    }
    public static bool StopWindowFlash(IntPtr WindowHandle)
    {
      var FlashWindowInformation = new FlashWindowInformation();
      FlashWindowInformation.StructSize = Convert.ToUInt32(System.Runtime.InteropServices.Marshal.SizeOf(FlashWindowInformation));
      FlashWindowInformation.WindowHandle = WindowHandle;
      FlashWindowInformation.FlashStatus = 0;
      FlashWindowInformation.FlashCount = uint.MaxValue;
      FlashWindowInformation.FlashRate = 0;

      return FlashWindowEx(ref FlashWindowInformation);
    }

    [DllImport("user32.dll")]
    public static extern int GetSystemMetrics(SystemMetric MetricIndex);

    public const int SW_HIDE = 0;
    public const int SW_SHOWNORMAL = 1;
    public const int SW_SHOWMINIMIZED = 2;
    public const int SW_SHOWMAXIMIZED = 2;

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct WINDOWPLACEMENT
    {
      public int length;
      public int flags;
      public int showCmd;
      public POINT minPosition;
      public POINT maxPosition;
      public RECT normalPosition;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
      public int Left;
      public int Top;
      public int Width;
      public int Height;

      public RECT(int left, int top, int width, int height)
      {
        this.Left = left;
        this.Top = top;
        this.Width = width;
        this.Height = height;
      }
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct POINT
    {
      public int X;
      public int Y;

      public POINT(int x, int y)
      {
        this.X = x;
        this.Y = y;
      }
    }

    [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
    public static extern IntPtr GetParent(IntPtr hWnd);
    
    /// <summary>
    /// Retrieves the handle to the ancestor of the specified window. 
    /// </summary>
    /// <param name="hwnd">A handle to the window whose ancestor is to be retrieved. 
    /// If this parameter is the desktop window, the function returns NULL. </param>
    /// <param name="flags">The ancestor to be retrieved.</param>
    /// <returns>The return value is the handle to the ancestor window.</returns>
    [DllImport("user32.dll", ExactSpelling = true)]
    public static extern IntPtr GetAncestor(IntPtr hwnd, GetAncestorFlags flags);

    public enum GetAncestorFlags
    {
      /// <summary>
      /// Retrieves the parent window. This does not include the owner, as it does with the GetParent function. 
      /// </summary>
      GetParent = 1,
      /// <summary>
      /// Retrieves the root window by walking the chain of parent windows.
      /// </summary>
      GetRoot = 2,
      /// <summary>
      /// Retrieves the owned root window by walking the chain of parent and owner windows returned by GetParent. 
      /// </summary>
      GetRootOwner = 3
    }

    [DllImport("user32.dll", SetLastError = true)]
    public static extern IntPtr GetWindow(IntPtr hWnd, GetWindow_Cmd uCmd);

    public enum GetWindow_Cmd : uint
    {
      GW_HWNDFIRST = 0,
      GW_HWNDLAST = 1,
      GW_HWNDNEXT = 2,
      GW_HWNDPREV = 3,
      GW_OWNER = 4,
      GW_CHILD = 5,
      GW_ENABLEDPOPUP = 6
    }

    [DllImport("user32.dll")]
    public static extern bool SetWindowPlacement(IntPtr hWnd, [In] ref WINDOWPLACEMENT lpwndpl);
    [DllImport("user32.dll")]
    public static extern bool GetWindowPlacement(IntPtr hWnd, out WINDOWPLACEMENT lpwndpl);

    [DllImport("user32.dll")]
    public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, SetWindowPosFlags uFlags);

    [Flags]
    public enum ExtendedWindowStyles
    {
      // ...
      WS_EX_TOOLWINDOW = 0x00000080,
      // ...
    }

    public enum GetWindowLongFields
    {
      GWL_WNDPROC = (-4), // Sets a new address for the window procedure.
      GWL_HINSTANCE = (-6), // Sets a new application instance handle.
      GWL_ID = (-12), // Sets a new identifier of the child window. The window cannot be a top-level window.
      GWL_STYLE = (-16), // Sets a new window style.
      GWL_USERDATA = (-21), // Sets the user data associated with the window. This data is intended for use by the application that created the window. Its value is initially zero.
      GWL_EXSTYLE = (-20) // Sets a new extended window style. 
    }

    [DllImport("user32.dll")]
    public static extern IntPtr GetWindowLong(IntPtr hWnd, int nIndex);

    public static IntPtr SetWindowLong(IntPtr hWnd, int nIndex, IntPtr dwNewLong)
    {
      var error = 0;
      var result = IntPtr.Zero;
      // Win32 SetWindowLong doesn't clear error on success
      SetLastError(0);

      if (IntPtr.Size == 4)
      {
        // use SetWindowLong
        var tempResult = IntSetWindowLong(hWnd, nIndex, IntPtrToInt32(dwNewLong));
        error = Marshal.GetLastWin32Error();
        result = new IntPtr(tempResult);
      }
      else
      {
        // use SetWindowLongPtr
        result = IntSetWindowLongPtr(hWnd, nIndex, dwNewLong);
        error = Marshal.GetLastWin32Error();
      }

      if ((result == IntPtr.Zero) && (error != 0))
      {
        throw new System.ComponentModel.Win32Exception(error);
      }

      return result;
    }

    [DllImport("user32.dll", EntryPoint = "SetWindowLongPtr", SetLastError = true)]
    private static extern IntPtr IntSetWindowLongPtr(IntPtr hWnd, int nIndex, IntPtr dwNewLong);

    [DllImport("user32.dll", EntryPoint = "SetWindowLong", SetLastError = true)]
    private static extern Int32 IntSetWindowLong(IntPtr hWnd, int nIndex, Int32 dwNewLong);

    private static int IntPtrToInt32(IntPtr intPtr)
    {
      return unchecked((int)intPtr.ToInt64());
    }

    [DllImport("kernel32.dll", EntryPoint = "SetLastError")]
    public static extern void SetLastError(int dwErrorCode);

    [DllImport("user32.dll")]
    public static extern IntPtr GetWindowRect(IntPtr hWnd, ref RectStruct rect);

    [DllImport("user32.dll")]
    public static extern IntPtr GetDesktopWindow();

    [DllImport("user32.dll")]
    public static extern IntPtr GetWindowDC(IntPtr hWnd);

    /// <summary>
    /// The MoveWindow function changes the position and dimensions of the specified window. For a top-level window, the position and dimensions are relative to the upper-left corner of the screen. For a child window, they are relative to the upper-left corner of the parent window's client area.
    /// </summary>
    /// <param name="hWnd">Handle to the window.</param>
    /// <param name="X">Specifies the new position of the left side of the window.</param>
    /// <param name="Y">Specifies the new position of the top of the window.</param>
    /// <param name="nWidth">Specifies the new width of the window.</param>
    /// <param name="nHeight">Specifies the new height of the window.</param>
    /// <param name="bRepaint">Specifies whether the window is to be repainted. If this parameter is TRUE, the window receives a message. If the parameter is FALSE, no repainting of any kind occurs. This applies to the client area, the nonclient area (including the title bar and scroll bars), and any part of the parent window uncovered as a result of moving a child window.</param>
    /// <returns>If the function succeeds, the return value is nonzero.
    /// <para>If the function fails, the return value is zero. To get extended error information, call GetLastError.</para></returns>
    [DllImport("user32.dll", SetLastError = true)]
    public static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

    [DllImport("user32.dll")]
    public static extern uint MapVirtualKey(uint uCode, uint uMapType);

    public enum MAPVK : uint
    {
      MAPVK_VK_TO_VSC = 0x0,
      MAPVK_VSC_TO_VK = 0x1,
      MAPVK_VK_TO_CHAR = 0x2,
      MAPVK_VSC_TO_VK_EX = 0x3,
    }
    public const uint MAPVK_VK_TO_CHAR = 2;

    [StructLayout(LayoutKind.Sequential)]
    public struct ICONINFO
    {
      public bool fIcon;
      public Int32 xHotspot;
      public Int32 yHotspot;
      public IntPtr hbmMask;
      public IntPtr hbmColor;
    }

    [DllImport("user32.dll")]
    public static extern bool GetIconInfo(IntPtr hIcon, out ICONINFO piconinfo);

    [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
    static extern uint RegisterWindowMessage(string lpString);

    [DllImport("user32.dll", EntryPoint = "SendMessage", SetLastError = true)]
    public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, ref COPYDATASTRUCT lParam);

    [DllImport("user32.dll", EntryPoint = "SendMessage", SetLastError = true)]
    public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

    [DllImport("user32.dll", EntryPoint = "SendMessage", SetLastError = true)]
    public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, string lParam);

    [return: MarshalAs(UnmanagedType.Bool)]
    [DllImport("user32.dll", EntryPoint = "PostMessage", SetLastError = true)]
    public static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, ref COPYDATASTRUCT lParam);

    [return: MarshalAs(UnmanagedType.Bool)]
    [DllImport("user32.dll", SetLastError = true)]
    public static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

    [return: MarshalAs(UnmanagedType.Bool)]
    [DllImport("user32.dll", EntryPoint = "PostMessageA")]
    public static extern bool PostMessage(IntPtr hWnd, uint msg, int wParam, int lParam);

    public const uint HWND_BROADCAST = 0xFFFF;

    //Used for WM_COPYDATA for string messages
    public struct COPYDATASTRUCT
    {
      public IntPtr dwData;
      public int cbData;
      [MarshalAs(UnmanagedType.LPStr)]
      public string lpData;
    }

    [DllImport("user32.dll", SetLastError = true)]
    public static extern System.UInt16 RegisterClassW([System.Runtime.InteropServices.In] ref WNDCLASS lpWndClass);

    /// <summary>
    /// Unregisters a window class, freeing the memory required for the class.
    /// </summary>
    /// <param name="lpClassName">
    /// Type: LPCTSTR
    /// A null-terminated string or a class atom. If lpClassName is a string, it specifies the window class name. 
    /// This class name must have been registered by a previous call to the RegisterClass or RegisterClassEx function. 
    /// System classes, such as dialog box controls, cannot be unregistered. If this parameter is an atom, 
    ///   it must be a class atom created by a previous call to the RegisterClass or RegisterClassEx function. 
    /// The atom must be in the low-order word of lpClassName; the high-order word must be zero.
    /// 
    /// </param>
    /// <param name="hInstance">
    /// A handle to the instance of the module that created the class.
    /// 
    /// </param>
    /// <returns>
    /// Type: BOOL
    /// If the function succeeds, the return value is nonzero.
    /// If the class could not be found or if a window still exists that was created with the class, the return value is zero. 
    /// To get extended error information, call GetLastError.
    /// 
    /// </returns>
    [DllImport("user32.dll")]
    public static extern bool UnregisterClass(string lpClassName, IntPtr hInstance);

    public delegate IntPtr WndProc(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

    [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
    public static extern System.IntPtr DefWindowProcW(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet = System.Runtime.InteropServices.CharSet.Unicode)]
    public struct WNDCLASS
    {
      public uint style;
      public WndProc lpfnWndProc;
      public int cbClsExtra;
      public int cbWndExtra;
      public IntPtr hInstance;
      public IntPtr hIcon;
      public IntPtr hCursor;
      public IntPtr hbrBackground;
      [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPWStr)]
      public string lpszMenuName;
      [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPWStr)]
      public string lpszClassName;
    }

    [Flags]
    public enum ClassStyles : uint
    {
      /// <summary>
      /// Aligns the window's client area on a byte boundary (in the x direction). This style affects the width of the window and its horizontal placement on the display.
      /// </summary>
      ByteAlignClient = 0x1000,
      /// <summary>
      /// Aligns the window on a byte boundary (in the x direction). This style affects the width of the window and its horizontal placement on the display.
      /// </summary>
      ByteAlignWindow = 0x2000,
      /// <summary>
      /// Allocates one device context to be shared by all windows in the class.
      /// Because window classes are process specific, it is possible for multiple threads of an application to create a window of the same class.
      /// It is also possible for the threads to attempt to use the device context simultaneously. When this happens, the system allows only one thread to successfully finish its drawing operation.
      /// </summary>
      ClassDC = 0x40,
      /// <summary>
      /// Sends a double-click message to the window procedure when the user double-clicks the mouse while the cursor is within a window belonging to the class.
      /// </summary>
      DoubleClicks = 0x8,
      /// <summary>
      /// Enables the drop shadow effect on a window. The effect is turned on and off through SPI_SETDROPSHADOW.
      /// Typically, this is enabled for small, short-lived windows such as menus to emphasize their Z order relationship to other windows.
      /// </summary>
      DropShadow = 0x20000,
      /// <summary>
      /// Indicates that the window class is an application global class. For more information, see the "Application Global Classes" section of About Window Classes.
      /// </summary>
      GlobalClass = 0x4000,
      /// <summary>
      /// Redraws the entire window if a movement or size adjustment changes the width of the client area.
      /// </summary>
      HorizontalRedraw = 0x2,
      /// <summary>
      /// Disables Close on the window menu.</summary>
      NoClose = 0x200,
      /// <summary>
      /// Allocates a unique device context for each window in the class.
      /// </summary>
      OwnDC = 0x20,
      /// <summary>
      /// Sets the clipping rectangle of the child window to that of the parent window so that the child can draw on the parent.
      /// A window with the CS_PARENTDC style bit receives a regular device context from the system's cache of device contexts.
      /// It does not give the child the parent's device context or device context settings. Specifying CS_PARENTDC enhances an application's performance.
      /// </summary>
      ParentDC = 0x80,
      /// <summary>
      /// Saves, as a bitmap, the portion of the screen image obscured by a window of this class.
      /// When the window is removed, the system uses the saved bitmap to restore the screen image, including other windows that were obscured.
      /// Therefore, the system does not send WM_PAINT messages to windows that were obscured if the memory used by the bitmap has not been discarded and if other screen actions have not invalidated the stored image.
      /// This style is useful for small windows (for example, menus or dialog boxes) that are displayed briefly and then removed before other screen activity takes place.
      /// This style increases the time required to display the window, because the system must first allocate memory to store the bitmap.
      /// </summary>
      SaveBits = 0x800,
      /// <summary>
      /// Redraws the entire window if a movement or size adjustment changes the height of the client area.
      /// </summary>
      VerticalRedraw = 0x1
    }

    /// <summary>
    /// The CreateWindowEx function creates an overlapped, pop-up, or child window with an extended window style; otherwise, this function is identical to the CreateWindow function.
    /// </summary>
    /// <param name="dwExStyle">Specifies the extended window style of the window being created.</param>
    /// <param name="lpClassName">Pointer to a null-terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function. The atom must be in the low-order word of lpClassName; the high-order word must be zero. If lpClassName is a string, it specifies the window class name. The class name can be any name registered with RegisterClass or RegisterClassEx, provided that the module that registers the class is also the module that creates the window. The class name can also be any of the predefined system class names.</param>
    /// <param name="lpWindowName">Pointer to a null-terminated string that specifies the window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar. When using CreateWindow to create controls, such as buttons, check boxes, and static controls, use lpWindowName to specify the text of the control. When creating a static control with the SS_ICON style, use lpWindowName to specify the icon name or identifier. To specify an identifier, use the syntax "#num". </param>
    /// <param name="dwStyle">Specifies the style of the window being created. This parameter can be a combination of window styles, plus the control styles indicated in the Remarks section.</param>
    /// <param name="x">Specifies the initial horizontal position of the window. For an overlapped or pop-up window, the x parameter is the initial x-coordinate of the window's upper-left corner, in screen coordinates. For a child window, x is the x-coordinate of the upper-left corner of the window relative to the upper-left corner of the parent window's client area. If x is set to CW_USEDEFAULT, the system selects the default position for the window's upper-left corner and ignores the y parameter. CW_USEDEFAULT is valid only for overlapped windows; if it is specified for a pop-up or child window, the x and y parameters are set to zero.</param>
    /// <param name="y">Specifies the initial vertical position of the window. For an overlapped or pop-up window, the y parameter is the initial y-coordinate of the window's upper-left corner, in screen coordinates. For a child window, y is the initial y-coordinate of the upper-left corner of the child window relative to the upper-left corner of the parent window's client area. For a list box y is the initial y-coordinate of the upper-left corner of the list box's client area relative to the upper-left corner of the parent window's client area.
    /// <para>If an overlapped window is created with the WS_VISIBLE style bit set and the x parameter is set to CW_USEDEFAULT, then the y parameter determines how the window is shown. If the y parameter is CW_USEDEFAULT, then the window manager calls ShowWindow with the SW_SHOW flag after the window has been created. If the y parameter is some other value, then the window manager calls ShowWindow with that value as the nCmdShow parameter.</para></param>
    /// <param name="nWidth">Specifies the width, in device units, of the window. For overlapped windows, nWidth is the window's width, in screen coordinates, or CW_USEDEFAULT. If nWidth is CW_USEDEFAULT, the system selects a default width and height for the window; the default width extends from the initial x-coordinates to the right edge of the screen; the default height extends from the initial y-coordinate to the top of the icon area. CW_USEDEFAULT is valid only for overlapped windows; if CW_USEDEFAULT is specified for a pop-up or child window, the nWidth and nHeight parameter are set to zero.</param>
    /// <param name="nHeight">Specifies the height, in device units, of the window. For overlapped windows, nHeight is the window's height, in screen coordinates. If the nWidth parameter is set to CW_USEDEFAULT, the system ignores nHeight.</param> <param name="hWndParent">Handle to the parent or owner window of the window being created. To create a child window or an owned window, supply a valid window handle. This parameter is optional for pop-up windows.
    /// <para>Windows 2000/XP: To create a message-only window, supply HWND_MESSAGE or a handle to an existing message-only window.</para></param>
    /// <param name="hMenu">Handle to a menu, or specifies a child-window identifier, depending on the window style. For an overlapped or pop-up window, hMenu identifies the menu to be used with the window; it can be NULL if the class menu is to be used. For a child window, hMenu specifies the child-window identifier, an integer value used by a dialog box control to notify its parent about events. The application determines the child-window identifier; it must be unique for all child windows with the same parent window.</param>
    /// <param name="hInstance">Handle to the instance of the module to be associated with the window.</param> <param name="lpParam">Pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message. This message is sent to the created window by this function before it returns.
    /// <para>If an application calls CreateWindow to create a MDI client window, lpParam should point to a CLIENTCREATESTRUCT structure. If an MDI client window calls CreateWindow to create an MDI child window, lpParam should point to a MDICREATESTRUCT structure. lpParam may be NULL if no additional data is needed.</para></param>
    /// <returns>If the function succeeds, the return value is a handle to the new window.
    /// <para>If the function fails, the return value is NULL. To get extended error information, call GetLastError.</para>
    /// <para>This function typically fails for one of the following reasons:</para>
    /// <list type="">
    /// <item>an invalid parameter value</item>
    /// <item>the system class was registered by a different module</item>
    /// <item>The WH_CBT hook is installed and returns a failure code</item>
    /// <item>if one of the controls in the dialog template is not registered, or its window window procedure fails WM_CREATE or WM_NCCREATE</item>
    /// </list></returns>
    [DllImport("user32.dll", SetLastError = true)]
    public static extern IntPtr CreateWindowExW(
       WindowStylesEx dwExStyle,
       [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPWStr)]
       string lpClassName,
       [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPWStr)]
       string lpWindowName,
       WindowStyles dwStyle,
       Int32 x,
       Int32 y,
       Int32 nWidth,
       Int32 nHeight,
       IntPtr hWndParent,
       IntPtr hMenu,
       IntPtr hInstance,
       IntPtr lpParam
    );

    [Flags]
    public enum WindowStylesEx : uint
    {
      /// <summary>
      /// Specifies that a window created with this style accepts drag-drop files.
      /// </summary>
      WS_EX_ACCEPTFILES = 0x00000010,
      /// <summary>
      /// Forces a top-level window onto the taskbar when the window is visible.
      /// </summary>
      WS_EX_APPWINDOW = 0x00040000,
      /// <summary>
      /// Specifies that a window has a border with a sunken edge.
      /// </summary>
      WS_EX_CLIENTEDGE = 0x00000200,
      /// <summary>
      /// Windows XP: Paints all descendants of a window in bottom-to-top painting order using double-buffering. For more information, see Remarks. This cannot be used if the window has a class style of either CS_OWNDC or CS_CLASSDC.
      /// </summary>
      WS_EX_COMPOSITED = 0x02000000,
      /// <summary>
      /// Includes a question mark in the title bar of the window. When the user clicks the question mark, the cursor changes to a question mark with a pointer. If the user then clicks a child window, the child receives a WM_HELP message. The child window should pass the message to the parent window procedure, which should call the WinHelp function using the HELP_WM_HELP command. The Help application displays a pop-up window that typically contains help for the child window.
      /// WS_EX_CONTEXTHELP cannot be used with the WS_MAXIMIZEBOX or WS_MINIMIZEBOX styles.
      /// </summary>
      WS_EX_CONTEXTHELP = 0x00000400,
      /// <summary>
      /// The window itself contains child windows that should take part in dialog box navigation. If this style is specified, the dialog manager recurses into children of this window when performing navigation operations such as handling the TAB key, an arrow key, or a keyboard mnemonic.
      /// </summary>
      WS_EX_CONTROLPARENT = 0x00010000,
      /// <summary>
      /// Creates a window that has a double border; the window can, optionally, be created with a title bar by specifying the WS_CAPTION style in the dwStyle parameter.
      /// </summary>
      WS_EX_DLGMODALFRAME = 0x00000001,
      /// <summary>
      /// Windows 2000/XP: Creates a layered window. Note that this cannot be used for child windows. Also, this cannot be used if the window has a class style of either CS_OWNDC or CS_CLASSDC.
      /// </summary>
      WS_EX_LAYERED = 0x00080000,
      /// <summary>
      /// Arabic and Hebrew versions of Windows 98/Me, Windows 2000/XP: Creates a window whose horizontal origin is on the right edge. Increasing horizontal values advance to the left.
      /// </summary>
      WS_EX_LAYOUTRTL = 0x00400000,
      /// <summary>
      /// Creates a window that has generic left-aligned properties. This is the default.
      /// </summary>
      WS_EX_LEFT = 0x00000000,
      /// <summary>
      /// If the shell language is Hebrew, Arabic, or another language that supports reading order alignment, the vertical scroll bar (if present) is to the left of the client area. For other languages, the style is ignored.
      /// </summary>
      WS_EX_LEFTSCROLLBAR = 0x00004000,
      /// <summary>
      /// The window text is displayed using left-to-right reading-order properties. This is the default.
      /// </summary>
      WS_EX_LTRREADING = 0x00000000,
      /// <summary>
      /// Creates a multiple-document interface (MDI) child window.
      /// </summary>
      WS_EX_MDICHILD = 0x00000040,
      /// <summary>
      /// Windows 2000/XP: A top-level window created with this style does not become the foreground window when the user clicks it. The system does not bring this window to the foreground when the user minimizes or closes the foreground window.
      /// To activate the window, use the SetActiveWindow or SetForegroundWindow function.
      /// The window does not appear on the taskbar by default. To force the window to appear on the taskbar, use the WS_EX_APPWINDOW style.
      /// </summary>
      WS_EX_NOACTIVATE = 0x08000000,
      /// <summary>
      /// Windows 2000/XP: A window created with this style does not pass its window layout to its child windows.
      /// </summary>
      WS_EX_NOINHERITLAYOUT = 0x00100000,
      /// <summary>
      /// Specifies that a child window created with this style does not send the WM_PARENTNOTIFY message to its parent window when it is created or destroyed.
      /// </summary>
      WS_EX_NOPARENTNOTIFY = 0x00000004,
      /// <summary>
      /// Combines the WS_EX_CLIENTEDGE and WS_EX_WINDOWEDGE styles.
      /// </summary>
      WS_EX_OVERLAPPEDWINDOW = WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE,
      /// <summary>
      /// Combines the WS_EX_WINDOWEDGE, WS_EX_TOOLWINDOW, and WS_EX_TOPMOST styles.
      /// </summary>
      WS_EX_PALETTEWINDOW = WS_EX_WINDOWEDGE | WS_EX_TOOLWINDOW | WS_EX_TOPMOST,
      /// <summary>
      /// The window has generic "right-aligned" properties. This depends on the window class. This style has an effect only if the shell language is Hebrew, Arabic, or another language that supports reading-order alignment; otherwise, the style is ignored.
      /// Using the WS_EX_RIGHT style for static or edit controls has the same effect as using the SS_RIGHT or ES_RIGHT style, respectively. Using this style with button controls has the same effect as using BS_RIGHT and BS_RIGHTBUTTON styles.
      /// </summary>
      WS_EX_RIGHT = 0x00001000,
      /// <summary>
      /// Vertical scroll bar (if present) is to the right of the client area. This is the default.
      /// </summary>
      WS_EX_RIGHTSCROLLBAR = 0x00000000,
      /// <summary>
      /// If the shell language is Hebrew, Arabic, or another language that supports reading-order alignment, the window text is displayed using right-to-left reading-order properties. For other languages, the style is ignored.
      /// </summary>
      WS_EX_RTLREADING = 0x00002000,
      /// <summary>
      /// Creates a window with a three-dimensional border style intended to be used for items that do not accept user input.
      /// </summary>
      WS_EX_STATICEDGE = 0x00020000,
      /// <summary>
      /// Creates a tool window; that is, a window intended to be used as a floating toolbar. A tool window has a title bar that is shorter than a normal title bar, and the window title is drawn using a smaller font. A tool window does not appear in the taskbar or in the dialog that appears when the user presses ALT+TAB. If a tool window has a system menu, its icon is not displayed on the title bar. However, you can display the system menu by right-clicking or by typing ALT+SPACE.
      /// </summary>
      WS_EX_TOOLWINDOW = 0x00000080,
      /// <summary>
      /// Specifies that a window created with this style should be placed above all non-topmost windows and should stay above them, even when the window is deactivated. To add or remove this style, use the SetWindowPos function.
      /// </summary>
      WS_EX_TOPMOST = 0x00000008,
      /// <summary>
      /// Specifies that a window created with this style should not be painted until siblings beneath the window (that were created by the same thread) have been painted. The window appears transparent because the bits of underlying sibling windows have already been painted.
      /// To achieve transparency without these restrictions, use the SetWindowRgn function.
      /// </summary>
      WS_EX_TRANSPARENT = 0x00000020,
      /// <summary>
      /// Specifies that a window has a border with a raised edge.
      /// </summary>
      WS_EX_WINDOWEDGE = 0x00000100
    }

    /// <summary>
    /// Window Styles.
    /// The following styles can be specified wherever a window style is required. After the control has been created, these styles cannot be modified, except as noted.
    /// </summary>
    [Flags()]
    public enum WindowStyles : uint
    {
      /// <summary>
      /// The window has a thin-line border.
      /// </summary>
      WS_BORDER = 0x800000,
      /// <summary>
      /// The window has a title bar (includes the WS_BORDER style).
      /// </summary>
      WS_CAPTION = 0xc00000,
      /// <summary>
      /// The window is a child window. A window with this style cannot have a menu bar. This style cannot be used with the WS_POPUP style
      /// .</summary>
      WS_CHILD = 0x40000000,
      /// <summary>
      /// Excludes the area occupied by child windows when drawing occurs within the parent window. This style is used when creating the parent window.
      /// </summary>
      WS_CLIPCHILDREN = 0x2000000,
      /// <summary>
      /// Clips child windows relative to each other; that is, when a particular child window receives a WM_PAINT message, the WS_CLIPSIBLINGS style clips all other overlapping child windows out of the region of the child window to be updated.
      /// If WS_CLIPSIBLINGS is not specified and child windows overlap, it is possible, when drawing within the client area of a child window, to draw within the client area of a neighboring child window.
      /// </summary>
      WS_CLIPSIBLINGS = 0x4000000,
      /// <summary>
      /// The window is initially disabled. A disabled window cannot receive input from the user. To change this after a window has been created, use the EnableWindow function.
      /// </summary>
      WS_DISABLED = 0x8000000,
      /// <summary>
      /// The window has a border of a style typically used with dialog boxes. A window with this style cannot have a title bar.
      /// </summary>
      WS_DLGFRAME = 0x400000,
      /// <summary>
      /// The window is the first control of a group of controls. The group consists of this first control and all controls defined after it, up to the next control with the WS_GROUP style.
      /// The first control in each group usually has the WS_TABSTOP style so that the user can move from group to group. The user can subsequently change the keyboard focus from one control in the group to the next control in the group by using the direction keys.
      /// You can turn this style on and off to change dialog box navigation. To change this style after a window has been created, use the SetWindowLong function.
      /// </summary>
      WS_GROUP = 0x20000,
      /// <summary>
      /// The window has a horizontal scroll bar.
      /// </summary>
      WS_HSCROLL = 0x100000,
      /// <summary>
      /// The window is initially maximized.
      /// </summary>
      WS_MAXIMIZE = 0x1000000,
      /// <summary>
      /// The window has a maximize button. Cannot be combined with the WS_EX_CONTEXTHELP style. The WS_SYSMENU style must also be specified.
      /// </summary>
      WS_MAXIMIZEBOX = 0x10000,
      /// <summary>
      /// The window is initially minimized.
      /// </summary>
      WS_MINIMIZE = 0x20000000,
      /// <summary>
      /// The window has a minimize button. Cannot be combined with the WS_EX_CONTEXTHELP style. The WS_SYSMENU style must also be specified.
      /// </summary>
      WS_MINIMIZEBOX = 0x20000,
      /// <summary>
      /// The window is an overlapped window. An overlapped window has a title bar and a border.
      /// </summary>
      WS_OVERLAPPED = 0x0,
      /// <summary>
      /// The window is an overlapped window.
      /// </summary>
      WS_OVERLAPPEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_SIZEFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
      /// <summary>
      /// The window is a pop-up window. This style cannot be used with the WS_CHILD style.
      /// </summary>
      WS_POPUP = 0x80000000u,
      /// <summary>
      /// The window is a pop-up window. The WS_CAPTION and WS_POPUPWINDOW styles must be combined to make the window menu visible.
      /// </summary>
      WS_POPUPWINDOW = WS_POPUP | WS_BORDER | WS_SYSMENU,
      /// <summary>
      /// The window has a sizing border.
      /// </summary>
      WS_SIZEFRAME = 0x40000,
      /// <summary>
      /// The window has a window menu on its title bar. The WS_CAPTION style must also be specified.
      /// </summary>
      WS_SYSMENU = 0x80000,
      /// <summary>
      /// The window is a control that can receive the keyboard focus when the user presses the TAB key.
      /// Pressing the TAB key changes the keyboard focus to the next control with the WS_TABSTOP style.  
      /// You can turn this style on and off to change dialog box navigation. To change this style after a window has been created, use the SetWindowLong function.
      /// For user-created windows and modeless dialogs to work with tab stops, alter the message loop to call the IsDialogMessage function.
      /// </summary>
      WS_TABSTOP = 0x10000,
      /// <summary>
      /// The window is initially visible. This style can be turned on and off by using the ShowWindow or SetWindowPos function.
      /// </summary>
      WS_VISIBLE = 0x10000000,
      /// <summary>
      /// The window has a vertical scroll bar.
      /// </summary>
      WS_VSCROLL = 0x200000
    }

    /// <summary>
    /// <para>The DestroyWindow function destroys the specified window. The function sends WM_DESTROY and WM_NCDESTROY messages to the window to deactivate it and remove the keyboard focus from it. The function also destroys the window's menu, flushes the thread message queue, destroys timers, removes clipboard ownership, and breaks the clipboard viewer chain (if the window is at the top of the viewer chain).</para>
    /// <para>If the specified window is a parent or owner window, DestroyWindow automatically destroys the associated child or owned windows when it destroys the parent or owner window. The function first destroys child or owned windows, and then it destroys the parent or owner window.</para>
    /// <para>DestroyWindow also destroys modeless dialog boxes created by the CreateDialog function.</para>
    /// </summary>
    /// <param name="hwnd">Handle to the window to be destroyed.</param>
    /// <returns>If the function succeeds, the return value is nonzero. If the function fails, the return value is zero. To get extended error information, call GetLastError.</returns>
    [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool DestroyWindow(IntPtr hwnd);

    [DllImport("user32.dll", SetLastError = true)]
    public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

    public enum ShowWindowCommands : int
    {
      /// <summary>
      /// Hides the window and activates another window.
      /// </summary>
      Hide = 0,
      /// <summary>
      /// Activates and displays a window. If the window is minimized or 
      /// maximized, the system restores it to its original size and position.
      /// An application should specify this flag when displaying the window 
      /// for the first time.
      /// </summary>
      Normal = 1,
      /// <summary>
      /// Activates the window and displays it as a minimized window.
      /// </summary>
      ShowMinimized = 2,
      /// <summary>
      /// Maximizes the specified window.
      /// </summary>
      Maximize = 3, // is this the right value?
      /// <summary>
      /// Activates the window and displays it as a maximized window.
      /// </summary>       
      ShowMaximized = 3,
      /// <summary>
      /// Displays a window in its most recent size and position. This value 
      /// is similar to <see cref="Normal"/>, except 
      /// the window is not activated.
      /// </summary>
      ShowNoActivate = 4,
      /// <summary>
      /// Activates the window and displays it in its current size and position. 
      /// </summary>
      Show = 5,
      /// <summary>
      /// Minimizes the specified window and activates the next top-level 
      /// window in the Z order.
      /// </summary>
      Minimize = 6,
      /// <summary>
      /// Displays the window as a minimized window. This value is similar to
      /// <see cref="ShowMinimized"/>, except the 
      /// window is not activated.
      /// </summary>
      ShowMinNoActive = 7,
      /// <summary>
      /// Displays the window in its current size and position. This value is 
      /// similar to <see cref="Show"/>, except the 
      /// window is not activated.
      /// </summary>
      ShowNA = 8,
      /// <summary>
      /// Activates and displays the window. If the window is minimized or 
      /// maximized, the system restores it to its original size and position. 
      /// An application should specify this flag when restoring a minimized window.
      /// </summary>
      Restore = 9,
      /// <summary>
      /// Sets the show state based on the SW_* value specified in the 
      /// STARTUPINFO structure passed to the CreateProcess function by the 
      /// program that started the application.
      /// </summary>
      ShowDefault = 10,
      /// <summary>
      ///  <b>Windows 2000/XP:</b> Minimizes a window, even if the thread 
      /// that owns the window is not responding. This flag should only be 
      /// used when minimizing windows from a different thread.
      /// </summary>
      ForceMinimize = 11
    }

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool ShowWindow(IntPtr hWnd, ShowWindowCommands nCmdShow);

    [DllImport("user32.dll")]
    public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

    public const int WH_MOUSE_LL = 14;
    public const int WH_KEYBOARD_LL = 13;

    public const int WM_NULL = 0x00;
    public const int WM_CREATE = 0x01;
    public const int WM_DESTROY = 0x02;
    public const int WM_MOVE = 0x03;
    public const int WM_SIZE = 0x05;
    public const int WM_ACTIVATE = 0x06;
    public const int WM_SETFOCUS = 0x07;
    public const int WM_KILLFOCUS = 0x08;
    public const int WM_ENABLE = 0x0A;
    public const int WM_SETREDRAW = 0x0B;
    public const int WM_SETTEXT = 0x0C;
    public const int WM_GETTEXT = 0x0D;
    public const int WM_GETTEXTLENGTH = 0x0E;
    public const int WM_PAINT = 0x0F;
    public const int WM_CLOSE = 0x10;
    public const int WM_QUERYENDSESSION = 0x11;
    public const int WM_QUIT = 0x12;
    public const int WM_QUERYOPEN = 0x13;
    public const int WM_ERASEBKGND = 0x14;
    public const int WM_SYSCOLORCHANGE = 0x15;
    public const int WM_ENDSESSION = 0x16;
    public const int WM_SYSTEMERROR = 0x17;
    public const int WM_SHOWWINDOW = 0x18;
    public const int WM_CTLCOLOR = 0x19;
    public const int WM_WININICHANGE = 0x1A;
    public const int WM_SETTINGCHANGE = 0x1A;
    public const int WM_DEVMODECHANGE = 0x1B;
    public const int WM_ACTIVATEAPP = 0x1C;
    public const int WM_FONTCHANGE = 0x1D;
    public const int WM_TIMECHANGE = 0x1E;
    public const int WM_CANCELMODE = 0x1F;
    public const int WM_SETCURSOR = 0x20;
    public const int WM_MOUSEACTIVATE = 0x21;
    public const int WM_CHILDACTIVATE = 0x22;
    public const int WM_QUEUESYNC = 0x23;
    public const int WM_GETMINMAXINFO = 0x24;
    public const int WM_PAINTICON = 0x26;
    public const int WM_ICONERASEBKGND = 0x27;
    public const int WM_NEXTDLGCTL = 0x28;
    public const int WM_SPOOLERSTATUS = 0x2A;
    public const int WM_DRAWITEM = 0x2B;
    public const int WM_MEASUREITEM = 0x2C;
    public const int WM_DELETEITEM = 0x2D;
    public const int WM_VKEYTOITEM = 0x2E;
    public const int WM_CHARTOITEM = 0x2F;

    public const int WM_SETFONT = 0x30;
    public const int WM_GETFONT = 0x31;
    public const int WM_SETHOTKEY = 0x32;
    public const int WM_GETHOTKEY = 0x33;
    public const int WM_QUERYDRAGICON = 0x37;
    public const int WM_COMPAREITEM = 0x39;
    public const int WM_COMPACTING = 0x41;
    public const int WM_WINDOWPOSCHANGING = 0x46;
    public const int WM_WINDOWPOSCHANGED = 0x47;
    public const int WM_POWER = 0x48;
    public const int WM_COPYDATA = 0x4A;
    public const int WM_CANCELJOURNAL = 0x4B;
    public const int WM_NOTIFY = 0x4E;
    public const int WM_INPUTLANGCHANGEREQUEST = 0x50;
    public const int WM_INPUTLANGCHANGE = 0x51;
    public const int WM_TCARD = 0x52;
    public const int WM_HELP = 0x53;
    public const int WM_USERCHANGED = 0x54;
    public const int WM_NOTIFYFORMAT = 0x55;
    public const int WM_CONTEXTMENU = 0x7B;
    public const int WM_STYLECHANGING = 0x7C;
    public const int WM_STYLECHANGED = 0x7D;
    public const int WM_DISPLAYCHANGE = 0x7E;
    public const int WM_GETICON = 0x7F;
    public const int WM_SETICON = 0x80;

    public const int WM_NCCREATE = 0x81;
    public const int WM_NCDESTROY = 0x82;
    public const int WM_NCCALCSIZE = 0x83;
    public const int WM_NCHITTEST = 0x84;
    public const int WM_NCPAINT = 0x85;
    public const int WM_NCACTIVATE = 0x86;
    public const int WM_GETDLGCODE = 0x87;
    public const int WM_NCMOUSEMOVE = 0xA0;
    public const int WM_NCLBUTTONDOWN = 0xA1;
    public const int WM_NCLBUTTONUP = 0xA2;
    public const int WM_NCLBUTTONDBLCLK = 0xA3;
    public const int WM_NCRBUTTONDOWN = 0xA4;
    public const int WM_NCRBUTTONUP = 0xA5;
    public const int WM_NCRBUTTONDBLCLK = 0xA6;
    public const int WM_NCMBUTTONDOWN = 0xA7;
    public const int WM_NCMBUTTONUP = 0xA8;
    public const int WM_NCMBUTTONDBLCLK = 0xA9;

    public const int WM_KEYFIRST = 0x100;
    public const int WM_KEYDOWN = 0x100;
    public const int WM_KEYUP = 0x101;
    public const int WM_CHAR = 0x102;
    public const int WM_DEADCHAR = 0x103;
    public const int WM_SYSKEYDOWN = 0x104;
    public const int WM_SYSKEYUP = 0x105;
    public const int WM_SYSCHAR = 0x106;
    public const int WM_SYSDEADCHAR = 0x107;
    public const int WM_KEYLAST = 0x108;

    public const int WM_IME_STARTCOMPOSITION = 0x10D;
    public const int WM_IME_ENDCOMPOSITION = 0x10E;
    public const int WM_IME_COMPOSITION = 0x10F;
    public const int WM_IME_KEYLAST = 0x10F;

    public const int WM_INITDIALOG = 0x110;
    public const int WM_COMMAND = 0x111;
    public const int WM_SYSCOMMAND = 0x112;
    public const int WM_TIMER = 0x113;
    public const int WM_HSCROLL = 0x114;
    public const int WM_VSCROLL = 0x115;
    public const int WM_INITMENU = 0x116;
    public const int WM_INITMENUPOPUP = 0x117;
    public const int WM_MENUSELECT = 0x11F;
    public const int WM_MENUCHAR = 0x120;
    public const int WM_ENTERIDLE = 0x121;

    public const int WM_CTLCOLORMSGBOX = 0x132;
    public const int WM_CTLCOLOREDIT = 0x133;
    public const int WM_CTLCOLORLISTBOX = 0x134;
    public const int WM_CTLCOLORBTN = 0x135;
    public const int WM_CTLCOLORDLG = 0x136;
    public const int WM_CTLCOLORSCROLLBAR = 0x137;
    public const int WM_CTLCOLORSTATIC = 0x138;

    public const int WM_MOUSEFIRST = 0x200;
    public const int WM_MOUSEMOVE = 0x200;
    public const int WM_LBUTTONDOWN = 0x201;
    public const int WM_LBUTTONUP = 0x202;
    public const int WM_LBUTTONDBLCLK = 0x203;
    public const int WM_RBUTTONDOWN = 0x204;
    public const int WM_RBUTTONUP = 0x205;
    public const int WM_RBUTTONDBLCLK = 0x206;
    public const int WM_MBUTTONDOWN = 0x207;
    public const int WM_MBUTTONUP = 0x208;
    public const int WM_MBUTTONDBLCLK = 0x209;
    public const int WM_MOUSEWHEEL = 0x20A;
    public const int WM_MOUSEHWHEEL = 0x20E;
    public const int WM_XBUTTONDOWN = 0x020B;

    public const int WM_PARENTNOTIFY = 0x210;
    public const int WM_ENTERMENULOOP = 0x211;
    public const int WM_EXITMENULOOP = 0x212;
    public const int WM_NEXTMENU = 0x213;
    public const int WM_SIZING = 0x214;
    public const int WM_CAPTURECHANGED = 0x215;
    public const int WM_MOVING = 0x216;
    public const int WM_POWERBROADCAST = 0x218;
    public const int WM_DEVICECHANGE = 0x219;

    public const int WM_MDICREATE = 0x220;
    public const int WM_MDIDESTROY = 0x221;
    public const int WM_MDIACTIVATE = 0x222;
    public const int WM_MDIRESTORE = 0x223;
    public const int WM_MDINEXT = 0x224;
    public const int WM_MDIMAXIMIZE = 0x225;
    public const int WM_MDITILE = 0x226;
    public const int WM_MDICASCADE = 0x227;
    public const int WM_MDIICONARRANGE = 0x228;
    public const int WM_MDIGETACTIVE = 0x229;
    public const int WM_MDISETMENU = 0x230;
    public const int WM_ENTERSIZEMOVE = 0x231;
    public const int WM_EXITSIZEMOVE = 0x232;
    public const int WM_DROPFILES = 0x233;
    public const int WM_MDIREFRESHMENU = 0x234;

    public const int WM_IME_SETCONTEXT = 0x281;
    public const int WM_IME_NOTIFY = 0x282;
    public const int WM_IME_CONTROL = 0x283;
    public const int WM_IME_COMPOSITIONFULL = 0x284;
    public const int WM_IME_SELECT = 0x285;
    public const int WM_IME_CHAR = 0x286;
    public const int WM_IME_KEYDOWN = 0x290;
    public const int WM_IME_KEYUP = 0x291;

    public const int WM_MOUSEHOVER = 0x2A1;
    public const int WM_NCMOUSELEAVE = 0x2A2;
    public const int WM_MOUSELEAVE = 0x2A3;

    public const int WM_CUT = 0x300;
    public const int WM_COPY = 0x301;
    public const int WM_PASTE = 0x302;
    public const int WM_CLEAR = 0x303;
    public const int WM_UNDO = 0x304;

    public const int WM_RENDERFORMAT = 0x305;
    public const int WM_RENDERALLFORMATS = 0x306;
    public const int WM_DESTROYCLIPBOARD = 0x307;
    public const int WM_DRAWCLIPBOARD = 0x308;
    public const int WM_PAINTCLIPBOARD = 0x309;
    public const int WM_VSCROLLCLIPBOARD = 0x30A;
    public const int WM_SIZECLIPBOARD = 0x30B;
    public const int WM_ASKCBFORMATNAME = 0x30C;
    public const int WM_CHANGECBCHAIN = 0x30D;
    public const int WM_HSCROLLCLIPBOARD = 0x30E;
    public const int WM_QUERYNEWPALETTE = 0x30F;
    public const int WM_PALETTEISCHANGING = 0x310;
    public const int WM_PALETTECHANGED = 0x311;

    public const int WM_HOTKEY = 0x312;
    public const int WM_PRINT = 0x317;
    public const int WM_PRINTCLIENT = 0x318;

    public const int WM_HANDHELDFIRST = 0x358;
    public const int WM_HANDHELDLAST = 0x35F;
    public const int WM_PENWINFIRST = 0x380;
    public const int WM_PENWINLAST = 0x38F;
    public const int WM_COALESCE_FIRST = 0x390;
    public const int WM_COALESCE_LAST = 0x39F;
    public const int WM_DDE_FIRST = 0x3E0;
    public const int WM_DDE_INITIATE = 0x3E0;
    public const int WM_DDE_TERMINATE = 0x3E1;
    public const int WM_DDE_ADVISE = 0x3E2;
    public const int WM_DDE_UNADVISE = 0x3E3;
    public const int WM_DDE_ACK = 0x3E4;
    public const int WM_DDE_DATA = 0x3E5;
    public const int WM_DDE_REQUEST = 0x3E6;
    public const int WM_DDE_POKE = 0x3E7;
    public const int WM_DDE_EXECUTE = 0x3E8;
    public const int WM_DDE_LAST = 0x3E8;

    public const int WM_USER = 0x400;

    public const int NIN_BALLOONSHOW = (WM_USER + 2);
    public const int NIN_BALLOONHIDE = (WM_USER + 3);
    public const int NIN_BALLOONTIMEOUT = (WM_USER + 4);
    public const int NIN_BALLOONUSERCLICK = (WM_USER + 5);

    public const int WM_APP = 0x8000;

    public static string WindowsMessageToString(int Message)
    {
      switch (Message)
      {
        case WM_NULL:
          return "WM_NULL";

        case WM_CREATE:
          return "WM_CREATE";

        case WM_DESTROY:
          return "WM_DESTROY";

        case WM_MOVE:
          return "WM_MOVE";

        case WM_SIZE:
          return "WM_SIZE";

        case WM_ACTIVATE:
          return "WM_ACTIVATE";

        case WM_SETFOCUS:
          return "WM_SETFOCUS";

        case WM_KILLFOCUS:
          return "WM_KILLFOCUS";

        case WM_ENABLE:
          return "WM_ENABLE";

        case WM_SETREDRAW:
          return "WM_SETREDRAW";

        case WM_SETTEXT:
          return "WM_SETTEXT";

        case WM_GETTEXT:
          return "WM_GETTEXT";

        case WM_GETTEXTLENGTH:
          return "WM_GETTEXTLENGTH";

        case WM_PAINT:
          return "WM_PAINT";

        case WM_CLOSE:
          return "WM_CLOSE";

        case WM_QUERYENDSESSION:
          return "WM_QUERYENDSESSION";

        case WM_QUIT:
          return "WM_QUIT";

        case WM_QUERYOPEN:
          return "WM_QUERYOPEN";

        case WM_ERASEBKGND:
          return "WM_ERASEBKGND";

        case WM_SYSCOLORCHANGE:
          return "WM_SYSCOLORCHANGE";

        case WM_ENDSESSION:
          return "WM_ENDSESSION";

        case WM_SYSTEMERROR:
          return "WM_SYSTEMERROR";

        case WM_SHOWWINDOW:
          return "WM_SHOWWINDOW";

        case WM_CTLCOLOR:
          return "WM_CTLCOLOR";

        case WM_SETTINGCHANGE:
          return "WM_SETTINGCHANGE";

        case WM_DEVMODECHANGE:
          return "WM_DEVMODECHANGE";

        case WM_ACTIVATEAPP:
          return "WM_ACTIVATEAPP";

        case WM_FONTCHANGE:
          return "WM_FONTCHANGE";

        case WM_TIMECHANGE:
          return "WM_TIMECHANGE";

        case WM_CANCELMODE:
          return "WM_CANCELMODE";

        case WM_SETCURSOR:
          return "WM_SETCURSOR";

        case WM_MOUSEACTIVATE:
          return "WM_MOUSEACTIVATE";

        case WM_CHILDACTIVATE:
          return "WM_CHILDACTIVATE";

        case WM_QUEUESYNC:
          return "WM_QUEUESYNC";

        case WM_GETMINMAXINFO:
          return "WM_GETMINMAXINFO";

        case WM_PAINTICON:
          return "WM_PAINTICON";

        case WM_ICONERASEBKGND:
          return "WM_ICONERASEBKGND";

        case WM_NEXTDLGCTL:
          return "WM_NEXTDLGCTL";

        case WM_SPOOLERSTATUS:
          return "WM_SPOOLERSTATUS";

        case WM_DRAWITEM:
          return "WM_DRAWITEM";

        case WM_MEASUREITEM:
          return "WM_MEASUREITEM";

        case WM_DELETEITEM:
          return "WM_DELETEITEM";

        case WM_VKEYTOITEM:
          return "WM_VKEYTOITEM";

        case WM_CHARTOITEM:
          return "WM_CHARTOITEM";

        case WM_SETFONT:
          return "WM_SETFONT";

        case WM_GETFONT:
          return "WM_GETFONT";

        case WM_SETHOTKEY:
          return "WM_SETHOTKEY";

        case WM_GETHOTKEY:
          return "WM_GETHOTKEY";

        case WM_QUERYDRAGICON:
          return "WM_QUERYDRAGICON";

        case WM_COMPAREITEM:
          return "WM_COMPAREITEM";

        case WM_COMPACTING:
          return "WM_COMPACTING";

        case WM_WINDOWPOSCHANGING:
          return "WM_WINDOWPOSCHANGING";

        case WM_WINDOWPOSCHANGED:
          return "WM_WINDOWPOSCHANGED";

        case WM_POWER:
          return "WM_POWER";

        case WM_COPYDATA:
          return "WM_COPYDATA";

        case WM_CANCELJOURNAL:
          return "WM_CANCELJOURNAL";

        case WM_NOTIFY:
          return "WM_NOTIFY";

        case WM_INPUTLANGCHANGEREQUEST:
          return "WM_INPUTLANGCHANGEREQUEST";

        case WM_INPUTLANGCHANGE:
          return "WM_INPUTLANGCHANGE";

        case WM_TCARD:
          return "WM_TCARD";

        case WM_HELP:
          return "WM_HELP";

        case WM_USERCHANGED:
          return "WM_USERCHANGED";

        case WM_NOTIFYFORMAT:
          return "WM_NOTIFYFORMAT";

        case WM_CONTEXTMENU:
          return "WM_CONTEXTMENU";

        case WM_STYLECHANGING:
          return "WM_STYLECHANGING";

        case WM_STYLECHANGED:
          return "WM_STYLECHANGED";

        case WM_DISPLAYCHANGE:
          return "WM_DISPLAYCHANGE";

        case WM_GETICON:
          return "WM_GETICON";

        case WM_SETICON:
          return "WM_SETICON";

        case WM_NCCREATE:
          return "WM_NCCREATE";

        case WM_NCDESTROY:
          return "WM_NCDESTROY";

        case WM_NCCALCSIZE:
          return "WM_NCCALCSIZE";

        case WM_NCHITTEST:
          return "WM_NCHITTEST";

        case WM_NCPAINT:
          return "WM_NCPAINT";

        case WM_NCACTIVATE:
          return "WM_NCACTIVATE";

        case WM_GETDLGCODE:
          return "WM_GETDLGCODE";

        case WM_NCMOUSEMOVE:
          return "WM_NCMOUSEMOVE";

        case WM_NCLBUTTONDOWN:
          return "WM_NCLBUTTONDOWN";

        case WM_NCLBUTTONUP:
          return "WM_NCLBUTTONUP";

        case WM_NCLBUTTONDBLCLK:
          return "WM_NCLBUTTONDBLCLK";

        case WM_NCRBUTTONDOWN:
          return "WM_NCRBUTTONDOWN";

        case WM_NCRBUTTONUP:
          return "WM_NCRBUTTONUP";

        case WM_NCRBUTTONDBLCLK:
          return "WM_NCRBUTTONDBLCLK";

        case WM_NCMBUTTONDOWN:
          return "WM_NCMBUTTONDOWN";

        case WM_NCMBUTTONUP:
          return "WM_NCMBUTTONUP";

        case WM_NCMBUTTONDBLCLK:
          return "WM_NCMBUTTONDBLCLK";

        case WM_KEYDOWN:
          return "WM_KEYDOWN";

        case WM_KEYUP:
          return "WM_KEYUP";

        case WM_CHAR:
          return "WM_CHAR";

        case WM_DEADCHAR:
          return "WM_DEADCHAR";

        case WM_SYSKEYDOWN:
          return "WM_SYSKEYDOWN";

        case WM_SYSKEYUP:
          return "WM_SYSKEYUP";

        case WM_SYSCHAR:
          return "WM_SYSCHAR";

        case WM_SYSDEADCHAR:
          return "WM_SYSDEADCHAR";

        case WM_KEYLAST:
          return "WM_KEYLAST";

        case WM_IME_STARTCOMPOSITION:
          return "WM_IME_STARTCOMPOSITION";

        case WM_IME_ENDCOMPOSITION:
          return "WM_IME_ENDCOMPOSITION";

        case WM_IME_COMPOSITION:
          return "WM_IME_COMPOSITION";

        case WM_INITDIALOG:
          return "WM_INITDIALOG";

        case WM_COMMAND:
          return "WM_COMMAND";

        case WM_SYSCOMMAND:
          return "WM_SYSCOMMAND";

        case WM_TIMER:
          return "WM_TIMER";

        case WM_HSCROLL:
          return "WM_HSCROLL";

        case WM_VSCROLL:
          return "WM_VSCROLL";

        case WM_INITMENU:
          return "WM_INITMENU";

        case WM_INITMENUPOPUP:
          return "WM_INITMENUPOPUP";

        case WM_MENUSELECT:
          return "WM_MENUSELECT";

        case WM_MENUCHAR:
          return "WM_MENUCHAR";

        case WM_ENTERIDLE:
          return "WM_ENTERIDLE";

        case WM_CTLCOLORMSGBOX:
          return "WM_CTLCOLORMSGBOX";

        case WM_CTLCOLOREDIT:
          return "WM_CTLCOLOREDIT";

        case WM_CTLCOLORLISTBOX:
          return "WM_CTLCOLORLISTBOX";

        case WM_CTLCOLORBTN:
          return "WM_CTLCOLORBTN";

        case WM_CTLCOLORDLG:
          return "WM_CTLCOLORDLG";

        case WM_CTLCOLORSCROLLBAR:
          return "WM_CTLCOLORSCROLLBAR";

        case WM_CTLCOLORSTATIC:
          return "WM_CTLCOLORSTATIC";

        case WM_MOUSEMOVE:
          return "WM_MOUSEMOVE";

        case WM_LBUTTONDOWN:
          return "WM_LBUTTONDOWN";

        case WM_LBUTTONUP:
          return "WM_LBUTTONUP";

        case WM_LBUTTONDBLCLK:
          return "WM_LBUTTONDBLCLK";

        case WM_RBUTTONDOWN:
          return "WM_RBUTTONDOWN";

        case WM_RBUTTONUP:
          return "WM_RBUTTONUP";

        case WM_RBUTTONDBLCLK:
          return "WM_RBUTTONDBLCLK";

        case WM_MBUTTONDOWN:
          return "WM_MBUTTONDOWN";

        case WM_MBUTTONUP:
          return "WM_MBUTTONUP";

        case WM_MBUTTONDBLCLK:
          return "WM_MBUTTONDBLCLK";

        case WM_MOUSEWHEEL:
          return "WM_MOUSEWHEEL";

        case WM_MOUSEHWHEEL:
          return "WM_MOUSEHWHEEL";

        case WM_PARENTNOTIFY:
          return "WM_PARENTNOTIFY";

        case WM_ENTERMENULOOP:
          return "WM_ENTERMENULOOP";

        case WM_EXITMENULOOP:
          return "WM_EXITMENULOOP";

        case WM_NEXTMENU:
          return "WM_NEXTMENU";

        case WM_SIZING:
          return "WM_SIZING";

        case WM_CAPTURECHANGED:
          return "WM_CAPTURECHANGED";

        case WM_MOVING:
          return "WM_MOVING";

        case WM_POWERBROADCAST:
          return "WM_POWERBROADCAST";

        case WM_DEVICECHANGE:
          return "WM_DEVICECHANGE";

        case WM_MDICREATE:
          return "WM_MDICREATE";

        case WM_MDIDESTROY:
          return "WM_MDIDESTROY";

        case WM_MDIACTIVATE:
          return "WM_MDIACTIVATE";

        case WM_MDIRESTORE:
          return "WM_MDIRESTORE";

        case WM_MDINEXT:
          return "WM_MDINEXT";

        case WM_MDIMAXIMIZE:
          return "WM_MDIMAXIMIZE";

        case WM_MDITILE:
          return "WM_MDITILE";

        case WM_MDICASCADE:
          return "WM_MDICASCADE";

        case WM_MDIICONARRANGE:
          return "WM_MDIICONARRANGE";

        case WM_MDIGETACTIVE:
          return "WM_MDIGETACTIVE";

        case WM_MDISETMENU:
          return "WM_MDISETMENU";

        case WM_ENTERSIZEMOVE:
          return "WM_ENTERSIZEMOVE";

        case WM_EXITSIZEMOVE:
          return "WM_EXITSIZEMOVE";

        case WM_DROPFILES:
          return "WM_DROPFILES";

        case WM_MDIREFRESHMENU:
          return "WM_MDIREFRESHMENU";

        case WM_IME_SETCONTEXT:
          return "WM_IME_SETCONTEXT";

        case WM_IME_NOTIFY:
          return "WM_IME_NOTIFY";

        case WM_IME_CONTROL:
          return "WM_IME_CONTROL";

        case WM_IME_COMPOSITIONFULL:
          return "WM_IME_COMPOSITIONFULL";

        case WM_IME_SELECT:
          return "WM_IME_SELECT";

        case WM_IME_CHAR:
          return "WM_IME_CHAR";

        case WM_IME_KEYDOWN:
          return "WM_IME_KEYDOWN";

        case WM_IME_KEYUP:
          return "WM_IME_KEYUP";

        case WM_MOUSEHOVER:
          return "WM_MOUSEHOVER";

        case WM_NCMOUSELEAVE:
          return "WM_NCMOUSELEAVE";

        case WM_MOUSELEAVE:
          return "WM_MOUSELEAVE";

        case WM_CUT:
          return "WM_CUT";

        case WM_COPY:
          return "WM_COPY";

        case WM_PASTE:
          return "WM_PASTE";

        case WM_CLEAR:
          return "WM_CLEAR";

        case WM_UNDO:
          return "WM_UNDO";

        case WM_RENDERFORMAT:
          return "WM_RENDERFORMAT";

        case WM_RENDERALLFORMATS:
          return "WM_RENDERALLFORMATS";

        case WM_DESTROYCLIPBOARD:
          return "WM_DESTROYCLIPBOARD";

        case WM_DRAWCLIPBOARD:
          return "WM_DRAWCLIPBOARD";

        case WM_PAINTCLIPBOARD:
          return "WM_PAINTCLIPBOARD";

        case WM_VSCROLLCLIPBOARD:
          return "WM_VSCROLLCLIPBOARD";

        case WM_SIZECLIPBOARD:
          return "WM_SIZECLIPBOARD";

        case WM_ASKCBFORMATNAME:
          return "WM_ASKCBFORMATNAME";

        case WM_CHANGECBCHAIN:
          return "WM_CHANGECBCHAIN";

        case WM_HSCROLLCLIPBOARD:
          return "WM_HSCROLLCLIPBOARD";

        case WM_QUERYNEWPALETTE:
          return "WM_QUERYNEWPALETTE";

        case WM_PALETTEISCHANGING:
          return "WM_PALETTEISCHANGING";

        case WM_PALETTECHANGED:
          return "WM_PALETTECHANGED";

        case WM_HOTKEY:
          return "WM_HOTKEY";

        case WM_PRINT:
          return "WM_PRINT";

        case WM_PRINTCLIENT:
          return "WM_PRINTCLIENT";

        case WM_HANDHELDFIRST:
          return "WM_HANDHELDFIRST";

        case WM_HANDHELDLAST:
          return "WM_HANDHELDLAST";

        case WM_PENWINFIRST:
          return "WM_PENWINFIRST";

        case WM_PENWINLAST:
          return "WM_PENWINLAST";

        case WM_COALESCE_FIRST:
          return "WM_COALESCE_FIRST";

        case WM_COALESCE_LAST:
          return "WM_COALESCE_LAST";

        case WM_DDE_INITIATE:
          return "WM_DDE_INITIATE";

        case WM_DDE_TERMINATE:
          return "WM_DDE_TERMINATE";

        case WM_DDE_ADVISE:
          return "WM_DDE_ADVISE";

        case WM_DDE_UNADVISE:
          return "WM_DDE_UNADVISE";

        case WM_DDE_ACK:
          return "WM_DDE_ACK";

        case WM_DDE_DATA:
          return "WM_DDE_DATA";

        case WM_DDE_REQUEST:
          return "WM_DDE_REQUEST";

        case WM_DDE_POKE:
          return "WM_DDE_POKE";

        case WM_DDE_EXECUTE:
          return "WM_DDE_EXECUTE";

        case WM_USER:
          return "WM_USER";

        case WM_APP:
          return "WM_APP";

        default:
          return "Unhandled message: " + Message.ToString();
      }
    }

    [Flags]
    public enum ACCESS_MASK : uint
    {
      DELETE = 0x00010000,
      READ_CONTROL = 0x00020000,
      WRITE_DAC = 0x00040000,
      WRITE_OWNER = 0x00080000,
      SYNCHRONIZE = 0x00100000,

      STANDARD_RIGHTS_REQUIRED = 0x000f0000,

      STANDARD_RIGHTS_READ = 0x00020000,
      STANDARD_RIGHTS_WRITE = 0x00020000,
      STANDARD_RIGHTS_EXECUTE = 0x00020000,

      STANDARD_RIGHTS_ALL = 0x001f0000,

      SPECIFIC_RIGHTS_ALL = 0x0000ffff,

      ACCESS_SYSTEM_SECURITY = 0x01000000,

      MAXIMUM_ALLOWED = 0x02000000,

      GENERIC_READ = 0x80000000,
      GENERIC_WRITE = 0x40000000,
      GENERIC_EXECUTE = 0x20000000,
      GENERIC_ALL = 0x10000000,

      DESKTOP_READOBJECTS = 0x00000001,
      DESKTOP_CREATEWINDOW = 0x00000002,
      DESKTOP_CREATEMENU = 0x00000004,
      DESKTOP_HOOKCONTROL = 0x00000008,
      DESKTOP_JOURNALRECORD = 0x00000010,
      DESKTOP_JOURNALPLAYBACK = 0x00000020,
      DESKTOP_ENUMERATE = 0x00000040,
      DESKTOP_WRITEOBJECTS = 0x00000080,
      DESKTOP_SWITCHDESKTOP = 0x00000100,

      WINSTA_ENUMDESKTOPS = 0x00000001,
      WINSTA_READATTRIBUTES = 0x00000002,
      WINSTA_ACCESSCLIPBOARD = 0x00000004,
      WINSTA_CREATEDESKTOP = 0x00000008,
      WINSTA_WRITEATTRIBUTES = 0x00000010,
      WINSTA_ACCESSGLOBALATOMS = 0x00000020,
      WINSTA_EXITWINDOWS = 0x00000040,
      WINSTA_ENUMERATE = 0x00000100,
      WINSTA_READSCREEN = 0x00000200,

      WINSTA_ALL_ACCESS = 0x0000037f
    }

    public delegate bool EnumDesktopProc(string lpszDesktop, IntPtr lParam);
    public delegate bool EnumDesktopWindowsProc(IntPtr desktopHandle, IntPtr lParam);

    // Desktop.
    [DllImport("user32.dll", EntryPoint = "CreateDesktop", CharSet = CharSet.Unicode, SetLastError = true)]
    public static extern IntPtr CreateDesktop(
      [MarshalAs(UnmanagedType.LPWStr)] string desktopName,
      [MarshalAs(UnmanagedType.LPWStr)] string device, // must be null.
      [MarshalAs(UnmanagedType.LPWStr)] string deviceMode, // must be null,
      [MarshalAs(UnmanagedType.U4)] int flags,  // use 0
      [MarshalAs(UnmanagedType.U4)] ACCESS_MASK accessMask,
      IntPtr attributes);
    [DllImport("user32.dll")]
    public static extern IntPtr OpenDesktop(string lpszDesktop, uint dwFlags, bool fInherit, [MarshalAs(UnmanagedType.U4)] ACCESS_MASK dwDesiredAccess);
    [DllImport("user32.dll", SetLastError = true)]
    public static extern bool CloseDesktop(IntPtr hDesktop);
    [DllImport("user32.dll", SetLastError = true)]
    public static extern IntPtr OpenInputDesktop(uint dwFlags, bool fInherit, [MarshalAs(UnmanagedType.U4)] ACCESS_MASK dwDesiredAccess);
    [DllImport("user32.dll")]
    public static extern bool SwitchDesktop(IntPtr hDesktop);

    [DllImport("user32.dll")]
    public static extern bool EnumDesktops(IntPtr hwinsta, EnumDesktopProc lpEnumFunc, IntPtr lParam);
    [DllImport("user32.dll")]
    public static extern IntPtr GetProcessWindowStation();
    [DllImport("user32.dll")]
    public static extern bool EnumDesktopWindows(IntPtr hDesktop, EnumDesktopWindowsProc lpfn, IntPtr lParam);
    [DllImport("user32.dll")]
    public static extern bool SetThreadDesktop(IntPtr hDesktop);
    [DllImport("user32.dll")]
    public static extern IntPtr GetThreadDesktop(int dwThreadId);
    [DllImport("user32.dll")]
    public static extern bool GetUserObjectInformation(IntPtr hObj, int nIndex, IntPtr pvInfo, int nLength, ref int lpnLengthNeeded);

    public delegate IntPtr MouseHookHandlerDelegate(int nCode, IntPtr wParam, ref MSLLHOOKSTRUCT lParam);
    public delegate IntPtr KeyboardHookHandlerDelegate(int nCode, IntPtr wParam, ref KBDLLHOOKSTRUCT lParam);

    // Modifier keys.
    public const int VK_SHIFT = 0x10;
    public const int VK_CONTROL = 0x11;
    public const int VK_MENU = 0x12;
    public const int VK_CAPITAL = 0x14;

    public struct KBDLLHOOKSTRUCT
    {
      public int vkCode;
      public int scanCode;
      public int flags;
      public int time;
      public int dwExtraInfo;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MSLLHOOKSTRUCT
    {
      public POINT pt;
      public uint mouseData;
      public uint flags;
      public uint time;
      public uint dwExtraInfo;
    }
    
    [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    public static extern IntPtr GetModuleHandle(string lpModuleName);

    [DllImport("user32.dll", EntryPoint = "SetWindowsHookEx", CharSet = CharSet.Auto, SetLastError = true)]
    public static extern IntPtr SetWindowsHookEx(int idHook, KeyboardHookHandlerDelegate lpfn, IntPtr hMod, uint dwThreadId);

    [DllImport("user32.dll", EntryPoint = "SetWindowsHookEx", CharSet = CharSet.Auto, SetLastError = true)]
    public static extern IntPtr SetWindowsHookEx(int idHook, MouseHookHandlerDelegate lpfn, IntPtr hMod, uint dwThreadId);

    [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool UnhookWindowsHookEx(IntPtr hhk);

    [DllImport("user32.dll", EntryPoint = "CallNextHookEx", CharSet = CharSet.Auto, SetLastError = true)]
    public static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, ref KBDLLHOOKSTRUCT lParam);

    [DllImport("user32.dll", EntryPoint = "CallNextHookEx", CharSet = CharSet.Auto, SetLastError = true)]
    public static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, ref MSLLHOOKSTRUCT lParam);

    [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
    public static extern short GetKeyState(int keyCode);

    /// <summary>
    /// Flags used with the Windows API (User32.dll):GetSystemMetrics(SystemMetric smIndex)
    ///   
    /// This Enum and declaration signature was written by Gabriel T. Sharp
    /// ai_productions@verizon.net or osirisgothra@hotmail.com
    /// Obtained on pinvoke.net, please contribute your code to support the wiki!
    /// </summary>
    public enum SystemMetric : int
    {
      /// <summary>
      /// The flags that specify how the system arranged minimized windows. For more information, see the Remarks section in this topic.
      /// </summary>
      SM_ARRANGE = 56,

      /// <summary>
      /// The value that specifies how the system is started: 
      /// 0 Normal boot
      /// 1 Fail-safe boot
      /// 2 Fail-safe with network boot
      /// A fail-safe boot (also called SafeBoot, Safe Mode, or Clean Boot) bypasses the user startup files.
      /// </summary>
      SM_CLEANBOOT = 67,

      /// <summary>
      /// The number of display monitors on a desktop. For more information, see the Remarks section in this topic.
      /// </summary>
      SM_CMONITORS = 80,

      /// <summary>
      /// The number of buttons on a mouse, or zero if no mouse is installed.
      /// </summary>
      SM_CMOUSEBUTTONS = 43,

      /// <summary>
      /// The width of a window border, in pixels. This is equivalent to the SM_CXEDGE value for windows with the 3-D look.
      /// </summary>
      SM_CXBORDER = 5,

      /// <summary>
      /// The width of a cursor, in pixels. The system cannot create cursors of other sizes.
      /// </summary>
      SM_CXCURSOR = 13,

      /// <summary>
      /// This value is the same as SM_CXFIXEDFRAME.
      /// </summary>
      SM_CXDLGFRAME = 7,

      /// <summary>
      /// The width of the rectangle around the location of a first click in a double-click sequence, in pixels. ,
      /// The second click must occur within the rectangle that is defined by SM_CXDOUBLECLK and SM_CYDOUBLECLK for the system
      /// to consider the two clicks a double-click. The two clicks must also occur within a specified time.
      /// To set the width of the double-click rectangle, call SystemParametersInfo with SPI_SETDOUBLECLKWIDTH.
      /// </summary>
      SM_CXDOUBLECLK = 36,

      /// <summary>
      /// The number of pixels on either side of a mouse-down point that the mouse pointer can move before a drag operation begins. 
      /// This allows the user to click and release the mouse button easily without unintentionally starting a drag operation. 
      /// If this value is negative, it is subtracted from the left of the mouse-down point and added to the right of it.
      /// </summary>
      SM_CXDRAG = 68,

      /// <summary>
      /// The width of a 3-D border, in pixels. This metric is the 3-D counterpart of SM_CXBORDER.
      /// </summary>
      SM_CXEDGE = 45,

      /// <summary>
      /// The thickness of the frame around the perimeter of a window that has a caption but is not sizable, in pixels.
      /// SM_CXFIXEDFRAME is the height of the horizontal border, and SM_CYFIXEDFRAME is the width of the vertical border.
      /// This value is the same as SM_CXDLGFRAME.
      /// </summary>
      SM_CXFIXEDFRAME = 7,

      /// <summary>
      /// The width of the left and right edges of the focus rectangle that the DrawFocusRectdraws. 
      /// This value is in pixels. 
      /// Windows 2000:  This value is not supported.
      /// </summary>
      SM_CXFOCUSBORDER = 83,

      /// <summary>
      /// This value is the same as SM_CXSIZEFRAME.
      /// </summary>
      SM_CXFRAME = 32,

      /// <summary>
      /// The width of the client area for a full-screen window on the primary display monitor, in pixels. 
      /// To get the coordinates of the portion of the screen that is not obscured by the system taskbar or by application desktop toolbars, 
      /// call the SystemParametersInfofunction with the SPI_GETWORKAREA value.
      /// </summary>
      SM_CXFULLSCREEN = 16,

      /// <summary>
      /// The width of the arrow bitmap on a horizontal scroll bar, in pixels.
      /// </summary>
      SM_CXHSCROLL = 21,

      /// <summary>
      /// The width of the thumb box in a horizontal scroll bar, in pixels.
      /// </summary>
      SM_CXHTHUMB = 10,

      /// <summary>
      /// The default width of an icon, in pixels. The LoadIcon function can load only icons with the dimensions 
      /// that SM_CXICON and SM_CYICON specifies.
      /// </summary>
      SM_CXICON = 11,

      /// <summary>
      /// The width of a grid cell for items in large icon view, in pixels. Each item fits into a rectangle of size 
      /// SM_CXICONSPACING by SM_CYICONSPACING when arranged. This value is always greater than or equal to SM_CXICON.
      /// </summary>
      SM_CXICONSPACING = 38,

      /// <summary>
      /// The default width, in pixels, of a maximized top-level window on the primary display monitor.
      /// </summary>
      SM_CXMAXIMIZED = 61,

      /// <summary>
      /// The default maximum width of a window that has a caption and sizing borders, in pixels. 
      /// This metric refers to the entire desktop. The user cannot drag the window frame to a size larger than these dimensions. 
      /// A window can override this value by processing the WM_GETMINMAXINFO message.
      /// </summary>
      SM_CXMAXTRACK = 59,

      /// <summary>
      /// The width of the default menu check-mark bitmap, in pixels.
      /// </summary>
      SM_CXMENUCHECK = 71,

      /// <summary>
      /// The width of menu bar buttons, such as the child window close button that is used in the multiple document interface, in pixels.
      /// </summary>
      SM_CXMENUSIZE = 54,

      /// <summary>
      /// The minimum width of a window, in pixels.
      /// </summary>
      SM_CXMIN = 28,

      /// <summary>
      /// The width of a minimized window, in pixels.
      /// </summary>
      SM_CXMINIMIZED = 57,

      /// <summary>
      /// The width of a grid cell for a minimized window, in pixels. Each minimized window fits into a rectangle this size when arranged. 
      /// This value is always greater than or equal to SM_CXMINIMIZED.
      /// </summary>
      SM_CXMINSPACING = 47,

      /// <summary>
      /// The minimum tracking width of a window, in pixels. The user cannot drag the window frame to a size smaller than these dimensions. 
      /// A window can override this value by processing the WM_GETMINMAXINFO message.
      /// </summary>
      SM_CXMINTRACK = 34,

      /// <summary>
      /// The amount of border padding for captioned windows, in pixels. Windows XP/2000:  This value is not supported.
      /// </summary>
      SM_CXPADDEDBORDER = 92,

      /// <summary>
      /// The width of the screen of the primary display monitor, in pixels. This is the same value obtained by calling 
      /// GetDeviceCaps as follows: GetDeviceCaps( hdcPrimaryMonitor, HORZRES).
      /// </summary>
      SM_CXSCREEN = 0,

      /// <summary>
      /// The width of a button in a window caption or title bar, in pixels.
      /// </summary>
      SM_CXSIZE = 30,

      /// <summary>
      /// The thickness of the sizing border around the perimeter of a window that can be resized, in pixels. 
      /// SM_CXSIZEFRAME is the width of the horizontal border, and SM_CYSIZEFRAME is the height of the vertical border. 
      /// This value is the same as SM_CXFRAME.
      /// </summary>
      SM_CXSIZEFRAME = 32,

      /// <summary>
      /// The recommended width of a small icon, in pixels. Small icons typically appear in window captions and in small icon view.
      /// </summary>
      SM_CXSMICON = 49,

      /// <summary>
      /// The width of small caption buttons, in pixels.
      /// </summary>
      SM_CXSMSIZE = 52,

      /// <summary>
      /// The width of the virtual screen, in pixels. The virtual screen is the bounding rectangle of all display monitors. 
      /// The SM_XVIRTUALSCREEN metric is the coordinates for the left side of the virtual screen.
      /// </summary>
      SM_CXVIRTUALSCREEN = 78,

      /// <summary>
      /// The width of a vertical scroll bar, in pixels.
      /// </summary>
      SM_CXVSCROLL = 2,

      /// <summary>
      /// The height of a window border, in pixels. This is equivalent to the SM_CYEDGE value for windows with the 3-D look.
      /// </summary>
      SM_CYBORDER = 6,

      /// <summary>
      /// The height of a caption area, in pixels.
      /// </summary>
      SM_CYCAPTION = 4,

      /// <summary>
      /// The height of a cursor, in pixels. The system cannot create cursors of other sizes.
      /// </summary>
      SM_CYCURSOR = 14,

      /// <summary>
      /// This value is the same as SM_CYFIXEDFRAME.
      /// </summary>
      SM_CYDLGFRAME = 8,

      /// <summary>
      /// The height of the rectangle around the location of a first click in a double-click sequence, in pixels. 
      /// The second click must occur within the rectangle defined by SM_CXDOUBLECLK and SM_CYDOUBLECLK for the system to consider 
      /// the two clicks a double-click. The two clicks must also occur within a specified time. To set the height of the double-click 
      /// rectangle, call SystemParametersInfo with SPI_SETDOUBLECLKHEIGHT.
      /// </summary>
      SM_CYDOUBLECLK = 37,

      /// <summary>
      /// The number of pixels above and below a mouse-down point that the mouse pointer can move before a drag operation begins. 
      /// This allows the user to click and release the mouse button easily without unintentionally starting a drag operation. 
      /// If this value is negative, it is subtracted from above the mouse-down point and added below it.
      /// </summary>
      SM_CYDRAG = 69,

      /// <summary>
      /// The height of a 3-D border, in pixels. This is the 3-D counterpart of SM_CYBORDER.
      /// </summary>
      SM_CYEDGE = 46,

      /// <summary>
      /// The thickness of the frame around the perimeter of a window that has a caption but is not sizable, in pixels. 
      /// SM_CXFIXEDFRAME is the height of the horizontal border, and SM_CYFIXEDFRAME is the width of the vertical border. 
      /// This value is the same as SM_CYDLGFRAME.
      /// </summary>
      SM_CYFIXEDFRAME = 8,

      /// <summary>
      /// The height of the top and bottom edges of the focus rectangle drawn byDrawFocusRect. 
      /// This value is in pixels. 
      /// Windows 2000:  This value is not supported.
      /// </summary>
      SM_CYFOCUSBORDER = 84,

      /// <summary>
      /// This value is the same as SM_CYSIZEFRAME.
      /// </summary>
      SM_CYFRAME = 33,

      /// <summary>
      /// The height of the client area for a full-screen window on the primary display monitor, in pixels. 
      /// To get the coordinates of the portion of the screen not obscured by the system taskbar or by application desktop toolbars,
      /// call the SystemParametersInfo function with the SPI_GETWORKAREA value.
      /// </summary>
      SM_CYFULLSCREEN = 17,

      /// <summary>
      /// The height of a horizontal scroll bar, in pixels.
      /// </summary>
      SM_CYHSCROLL = 3,

      /// <summary>
      /// The default height of an icon, in pixels. The LoadIcon function can load only icons with the dimensions SM_CXICON and SM_CYICON.
      /// </summary>
      SM_CYICON = 12,

      /// <summary>
      /// The height of a grid cell for items in large icon view, in pixels. Each item fits into a rectangle of size 
      /// SM_CXICONSPACING by SM_CYICONSPACING when arranged. This value is always greater than or equal to SM_CYICON.
      /// </summary>
      SM_CYICONSPACING = 39,

      /// <summary>
      /// For double byte character set versions of the system, this is the height of the Kanji window at the bottom of the screen, in pixels.
      /// </summary>
      SM_CYKANJIWINDOW = 18,

      /// <summary>
      /// The default height, in pixels, of a maximized top-level window on the primary display monitor.
      /// </summary>
      SM_CYMAXIMIZED = 62,

      /// <summary>
      /// The default maximum height of a window that has a caption and sizing borders, in pixels. This metric refers to the entire desktop. 
      /// The user cannot drag the window frame to a size larger than these dimensions. A window can override this value by processing 
      /// the WM_GETMINMAXINFO message.
      /// </summary>
      SM_CYMAXTRACK = 60,

      /// <summary>
      /// The height of a single-line menu bar, in pixels.
      /// </summary>
      SM_CYMENU = 15,

      /// <summary>
      /// The height of the default menu check-mark bitmap, in pixels.
      /// </summary>
      SM_CYMENUCHECK = 72,

      /// <summary>
      /// The height of menu bar buttons, such as the child window close button that is used in the multiple document interface, in pixels.
      /// </summary>
      SM_CYMENUSIZE = 55,

      /// <summary>
      /// The minimum height of a window, in pixels.
      /// </summary>
      SM_CYMIN = 29,

      /// <summary>
      /// The height of a minimized window, in pixels.
      /// </summary>
      SM_CYMINIMIZED = 58,

      /// <summary>
      /// The height of a grid cell for a minimized window, in pixels. Each minimized window fits into a rectangle this size when arranged. 
      /// This value is always greater than or equal to SM_CYMINIMIZED.
      /// </summary>
      SM_CYMINSPACING = 48,

      /// <summary>
      /// The minimum tracking height of a window, in pixels. The user cannot drag the window frame to a size smaller than these dimensions. 
      /// A window can override this value by processing the WM_GETMINMAXINFO message.
      /// </summary>
      SM_CYMINTRACK = 35,

      /// <summary>
      /// The height of the screen of the primary display monitor, in pixels. This is the same value obtained by calling 
      /// GetDeviceCaps as follows: GetDeviceCaps( hdcPrimaryMonitor, VERTRES).
      /// </summary>
      SM_CYSCREEN = 1,

      /// <summary>
      /// The height of a button in a window caption or title bar, in pixels.
      /// </summary>
      SM_CYSIZE = 31,

      /// <summary>
      /// The thickness of the sizing border around the perimeter of a window that can be resized, in pixels. 
      /// SM_CXSIZEFRAME is the width of the horizontal border, and SM_CYSIZEFRAME is the height of the vertical border. 
      /// This value is the same as SM_CYFRAME.
      /// </summary>
      SM_CYSIZEFRAME = 33,

      /// <summary>
      /// The height of a small caption, in pixels.
      /// </summary>
      SM_CYSMCAPTION = 51,

      /// <summary>
      /// The recommended height of a small icon, in pixels. Small icons typically appear in window captions and in small icon view.
      /// </summary>
      SM_CYSMICON = 50,

      /// <summary>
      /// The height of small caption buttons, in pixels.
      /// </summary>
      SM_CYSMSIZE = 53,

      /// <summary>
      /// The height of the virtual screen, in pixels. The virtual screen is the bounding rectangle of all display monitors. 
      /// The SM_YVIRTUALSCREEN metric is the coordinates for the top of the virtual screen.
      /// </summary>
      SM_CYVIRTUALSCREEN = 79,

      /// <summary>
      /// The height of the arrow bitmap on a vertical scroll bar, in pixels.
      /// </summary>
      SM_CYVSCROLL = 20,

      /// <summary>
      /// The height of the thumb box in a vertical scroll bar, in pixels.
      /// </summary>
      SM_CYVTHUMB = 9,

      /// <summary>
      /// Nonzero if User32.dll supports DBCS; otherwise, 0.
      /// </summary>
      SM_DBCSENABLED = 42,

      /// <summary>
      /// Nonzero if the debug version of User.exe is installed; otherwise, 0.
      /// </summary>
      SM_DEBUG = 22,

      /// <summary>
      /// Nonzero if the current operating system is Windows 7 or Windows Server 2008 R2 and the Tablet PC Input 
      /// service is started; otherwise, 0. The return value is a bitmask that specifies the type of digitizer input supported by the device. 
      /// For more information, see Remarks. 
      /// Windows Server 2008, Windows Vista, and Windows XP/2000:  This value is not supported.
      /// </summary>
      SM_DIGITIZER = 94,

      /// <summary>
      /// Nonzero if Input Method Manager/Input Method Editor features are enabled; otherwise, 0. 
      /// SM_IMMENABLED indicates whether the system is ready to use a Unicode-based IME on a Unicode application. 
      /// To ensure that a language-dependent IME works, check SM_DBCSENABLED and the system ANSI code page.
      /// Otherwise the ANSI-to-Unicode conversion may not be performed correctly, or some components like fonts
      /// or registry settings may not be present.
      /// </summary>
      SM_IMMENABLED = 82,

      /// <summary>
      /// Nonzero if there are digitizers in the system; otherwise, 0. SM_MAXIMUMTOUCHES returns the aggregate maximum of the 
      /// maximum number of contacts supported by every digitizer in the system. If the system has only single-touch digitizers, 
      /// the return value is 1. If the system has multi-touch digitizers, the return value is the number of simultaneous contacts 
      /// the hardware can provide. Windows Server 2008, Windows Vista, and Windows XP/2000:  This value is not supported.
      /// </summary>
      SM_MAXIMUMTOUCHES = 95,

      /// <summary>
      /// Nonzero if the current operating system is the Windows XP, Media Center Edition, 0 if not.
      /// </summary>
      SM_MEDIACENTER = 87,

      /// <summary>
      /// Nonzero if drop-down menus are right-aligned with the corresponding menu-bar item; 0 if the menus are left-aligned.
      /// </summary>
      SM_MENUDROPALIGNMENT = 40,

      /// <summary>
      /// Nonzero if the system is enabled for Hebrew and Arabic languages, 0 if not.
      /// </summary>
      SM_MIDEASTENABLED = 74,

      /// <summary>
      /// Nonzero if a mouse is installed; otherwise, 0. This value is rarely zero, because of support for virtual mice and because 
      /// some systems detect the presence of the port instead of the presence of a mouse.
      /// </summary>
      SM_MOUSEPRESENT = 19,

      /// <summary>
      /// Nonzero if a mouse with a horizontal scroll wheel is installed; otherwise 0.
      /// </summary>
      SM_MOUSEHORIZONTALWHEELPRESENT = 91,

      /// <summary>
      /// Nonzero if a mouse with a vertical scroll wheel is installed; otherwise 0.
      /// </summary>
      SM_MOUSEWHEELPRESENT = 75,

      /// <summary>
      /// The least significant bit is set if a network is present; otherwise, it is cleared. The other bits are reserved for future use.
      /// </summary>
      SM_NETWORK = 63,

      /// <summary>
      /// Nonzero if the Microsoft Windows for Pen computing extensions are installed; zero otherwise.
      /// </summary>
      SM_PENWINDOWS = 41,

      /// <summary>
      /// This system metric is used in a Terminal Services environment to determine if the current Terminal Server session is 
      /// being remotely controlled. Its value is nonzero if the current session is remotely controlled; otherwise, 0. 
      /// You can use terminal services management tools such as Terminal Services Manager (tsadmin.msc) and shadow.exe to 
      /// control a remote session. When a session is being remotely controlled, another user can view the contents of that session 
      /// and potentially interact with it.
      /// </summary>
      SM_REMOTECONTROL = 0x2001,

      /// <summary>
      /// This system metric is used in a Terminal Services environment. If the calling process is associated with a Terminal Services 
      /// client session, the return value is nonzero. If the calling process is associated with the Terminal Services console session, 
      /// the return value is 0. 
      /// Windows Server 2003 and Windows XP:  The console session is not necessarily the physical console. 
      /// For more information, seeWTSGetActiveConsoleSessionId.
      /// </summary>
      SM_REMOTESESSION = 0x1000,

      /// <summary>
      /// Nonzero if all the display monitors have the same color format, otherwise, 0. Two displays can have the same bit depth, 
      /// but different color formats. For example, the red, green, and blue pixels can be encoded with different numbers of bits, 
      /// or those bits can be located in different places in a pixel color value.
      /// </summary>
      SM_SAMEDISPLAYFORMAT = 81,

      /// <summary>
      /// This system metric should be ignored; it always returns 0.
      /// </summary>
      SM_SECURE = 44,

      /// <summary>
      /// The build number if the system is Windows Server 2003 R2; otherwise, 0.
      /// </summary>
      SM_SERVERR2 = 89,

      /// <summary>
      /// Nonzero if the user requires an application to present information visually in situations where it would otherwise present 
      /// the information only in audible form; otherwise, 0.
      /// </summary>
      SM_SHOWSOUNDS = 70,

      /// <summary>
      /// Nonzero if the current session is shutting down; otherwise, 0. Windows 2000:  This value is not supported.
      /// </summary>
      SM_SHUTTINGDOWN = 0x2000,

      /// <summary>
      /// Nonzero if the computer has a low-end (slow) processor; otherwise, 0.
      /// </summary>
      SM_SLOWMACHINE = 73,

      /// <summary>
      /// Nonzero if the current operating system is Windows 7 Starter Edition, Windows Vista Starter, or Windows XP Starter Edition; otherwise, 0.
      /// </summary>
      SM_STARTER = 88,

      /// <summary>
      /// Nonzero if the meanings of the left and right mouse buttons are swapped; otherwise, 0.
      /// </summary>
      SM_SWAPBUTTON = 23,

      /// <summary>
      /// Nonzero if the current operating system is the Windows XP Tablet PC edition or if the current operating system is Windows Vista
      /// or Windows 7 and the Tablet PC Input service is started; otherwise, 0. The SM_DIGITIZER setting indicates the type of digitizer 
      /// input supported by a device running Windows 7 or Windows Server 2008 R2. For more information, see Remarks.
      /// </summary>
      SM_TABLETPC = 86,

      /// <summary>
      /// The coordinates for the left side of the virtual screen. The virtual screen is the bounding rectangle of all display monitors. 
      /// The SM_CXVIRTUALSCREEN metric is the width of the virtual screen.
      /// </summary>
      SM_XVIRTUALSCREEN = 76,

      /// <summary>
      /// The coordinates for the top of the virtual screen. The virtual screen is the bounding rectangle of all display monitors. 
      /// The SM_CYVIRTUALSCREEN metric is the height of the virtual screen.
      /// </summary>
      SM_YVIRTUALSCREEN = 77,
    }

    [DllImport("user32.dll", SetLastError = true)]
    public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

    [DllImport("user32.dll", SetLastError = true)]
    public static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle);

    public static IntPtr GetConsoleWindow(int ProcessID)
    {
      var hWnd = FindWindowEx(IntPtr.Zero, IntPtr.Zero, "ConsoleWindowClass", null);

      GetWindowThreadProcessId(hWnd, out var wpid);

      while (ProcessID != wpid)
      {
        if ((hWnd = FindWindowEx(IntPtr.Zero, hWnd, "ConsoleWindowClass", null)) != IntPtr.Zero)
          GetWindowThreadProcessId(hWnd, out wpid);
        else
          return IntPtr.Zero;
      }

      return hWnd;
    }

    public enum SysCommands : int
    {
      SC_SIZE = 0xF000,
      SC_MOVE = 0xF010,
      SC_MINIMIZE = 0xF020,
      SC_MAXIMIZE = 0xF030,
      SC_NEXTWINDOW = 0xF040,
      SC_PREVWINDOW = 0xF050,
      SC_CLOSE = 0xF060,
      SC_VSCROLL = 0xF070,
      SC_HSCROLL = 0xF080,
      SC_MOUSEMENU = 0xF090,
      SC_KEYMENU = 0xF100,
      SC_ARRANGE = 0xF110,
      SC_RESTORE = 0xF120,
      SC_TASKLIST = 0xF130,
      SC_SCREENSAVE = 0xF140,
      SC_HOTKEY = 0xF150,
      //#if(WINVER >= 0x0400) //Win95
      SC_DEFAULT = 0xF160,
      SC_MONITORPOWER = 0xF170,
      SC_CONTEXTHELP = 0xF180,
      SC_SEPARATOR = 0xF00F,
      //#endif /* WINVER >= 0x0400 */

      //#if(WINVER >= 0x0600) //Vista
      SCF_ISSECURE = 0x00000001,
      //#endif /* WINVER >= 0x0600 */

      /*
        * Obsolete names
        */
      SC_ICON = SC_MINIMIZE,
      SC_ZOOM = SC_MAXIMIZE,
    }


    [System.Runtime.InteropServices.DllImport("user32.dll")]
    [return: System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)]
    private static extern bool GetCursorPos(ref Win32Point pt);

    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
    public struct Win32Point
    {
      public int X;
      public int Y;
    };
    public static System.Windows.Point GetMousePosition()
    {
      var w32Mouse = new Win32Point();
      GetCursorPos(ref w32Mouse);
      return new System.Windows.Point(w32Mouse.X, w32Mouse.Y);
    }
  }
}