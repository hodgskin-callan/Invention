﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Win32
{
  public sealed class Gdi32
  {
    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);

    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern IntPtr CreateCompatibleDC(IntPtr hdc);

    [DllImport("gdi32.dll", CharSet = CharSet.Unicode)]
    public static extern IntPtr CreateDC(string szdriver, string szdevice, string szoutput, IntPtr devmode);

    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern bool DeleteDC(IntPtr hdc);

    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern bool DeleteObject(IntPtr hgdiobj);

    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern int GetDeviceCaps(IntPtr hDC, int nIndex);

    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);

    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern int StretchDIBits(IntPtr hdc, int xdest, int ydest, int nDestWidth, int nDestHeight, int XSrc, int YSrc, int nSrcWidth, int nSrcHeight, IntPtr bitsptr, IntPtr bmiptr, int iUsage, int dwRop);

    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern int SetDIBitsToDevice(IntPtr hdc, int xdst, int ydst, int width, int height, int xsrc, int ysrc, int start, int lines, IntPtr bitsptr, IntPtr bmiptr, int color);

    [DllImport("gdi32.dll")]
    public static extern bool BitBlt(IntPtr hObject, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hObjectSource, int nXSrc, int nYSrc, int dwRop);

    public const int SRCCOPY = 13369376;

    [StructLayout(LayoutKind.Sequential, Pack = 2)]
    public sealed class BITMAPINFOHEADER
    {
      public int biSize = 0;
      public int biWidth = 0;
      public int biHeight = 0;
      public short biPlanes = 0;
      public short biBitCount = 0;
      public int biCompression = 0;
      public int biSizeImage = 0;
      public int biXPelsPerMeter = 0;
      public int biYPelsPerMeter = 0;
      public int biClrUsed = 0;
      public int biClrImportant = 0;
    }

    public enum DeviceCap
    {
      #region http://pinvoke.net/default.aspx/gdi32/GetDeviceCaps.html
      /// <summary>
      /// Device driver version
      /// </summary>
      DRIVERVERSION = 0,
      /// <summary>
      /// Device classification
      /// </summary>
      TECHNOLOGY = 2,
      /// <summary>
      /// Horizontal size in millimeters
      /// </summary>
      HORZSIZE = 4,
      /// <summary>
      /// Vertical size in millimeters
      /// </summary>
      VERTSIZE = 6,
      /// <summary>
      /// Horizontal width in pixels
      /// </summary>
      HORZRES = 8,
      /// <summary>
      /// Vertical height in pixels
      /// </summary>
      VERTRES = 10,
      /// <summary>
      /// Number of bits per pixel
      /// </summary>
      BITSPIXEL = 12,
      /// <summary>
      /// Number of planes
      /// </summary>
      PLANES = 14,
      /// <summary>
      /// Number of brushes the device has
      /// </summary>
      NUMBRUSHES = 16,
      /// <summary>
      /// Number of pens the device has
      /// </summary>
      NUMPENS = 18,
      /// <summary>
      /// Number of markers the device has
      /// </summary>
      NUMMARKERS = 20,
      /// <summary>
      /// Number of fonts the device has
      /// </summary>
      NUMFONTS = 22,
      /// <summary>
      /// Number of colors the device supports
      /// </summary>
      NUMCOLORS = 24,
      /// <summary>
      /// Size required for device descriptor
      /// </summary>
      PDEVICESIZE = 26,
      /// <summary>
      /// Curve capabilities
      /// </summary>
      CURVECAPS = 28,
      /// <summary>
      /// Line capabilities
      /// </summary>
      LINECAPS = 30,
      /// <summary>
      /// Polygonal capabilities
      /// </summary>
      POLYGONALCAPS = 32,
      /// <summary>
      /// Text capabilities
      /// </summary>
      TEXTCAPS = 34,
      /// <summary>
      /// Clipping capabilities
      /// </summary>
      CLIPCAPS = 36,
      /// <summary>
      /// Bitblt capabilities
      /// </summary>
      RASTERCAPS = 38,
      /// <summary>
      /// Length of the X leg
      /// </summary>
      ASPECTX = 40,
      /// <summary>
      /// Length of the Y leg
      /// </summary>
      ASPECTY = 42,
      /// <summary>
      /// Length of the hypotenuse
      /// </summary>
      ASPECTXY = 44,
      /// <summary>
      /// Shading and Blending caps
      /// </summary>
      SHADEBLENDCAPS = 45,

      /// <summary>
      /// Logical pixels inch in X
      /// </summary>
      LOGPIXELSX = 88,
      /// <summary>
      /// Logical pixels inch in Y
      /// </summary>
      LOGPIXELSY = 90,

      /// <summary>
      /// Number of entries in physical palette
      /// </summary>
      SIZEPALETTE = 104,
      /// <summary>
      /// Number of reserved entries in palette
      /// </summary>
      NUMRESERVED = 106,
      /// <summary>
      /// Actual color resolution
      /// </summary>
      COLORRES = 108,

      // Printing related DeviceCaps. These replace the appropriate Escapes
      /// <summary>
      /// Physical Width in device units
      /// </summary>
      PHYSICALWIDTH = 110,
      /// <summary>
      /// Physical Height in device units
      /// </summary>
      PHYSICALHEIGHT = 111,
      /// <summary>
      /// Physical Printable Area x margin
      /// </summary>
      PHYSICALOFFSETX = 112,
      /// <summary>
      /// Physical Printable Area y margin
      /// </summary>
      PHYSICALOFFSETY = 113,
      /// <summary>
      /// Scaling factor x
      /// </summary>
      SCALINGFACTORX = 114,
      /// <summary>
      /// Scaling factor y
      /// </summary>
      SCALINGFACTORY = 115,

      /// <summary>
      /// Current vertical refresh rate of the display device (for displays only) in Hz
      /// </summary>
      VREFRESH = 116,
      /// <summary>
      /// Vertical height of entire desktop in pixels
      /// </summary>
      DESKTOPVERTRES = 117,
      /// <summary>
      /// Horizontal width of entire desktop in pixels
      /// </summary>
      DESKTOPHORZRES = 118,
      /// <summary>
      /// Preferred blt alignment
      /// </summary>
      BLTALIGNMENT = 119
      #endregion
    }

    public class SafeHBitmapHandle : Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
    {
      [System.Security.SecurityCritical]
      public SafeHBitmapHandle(IntPtr preexistingHandle, bool ownsHandle)
        : base(ownsHandle)
      {
        SetHandle(preexistingHandle);
      }

      protected override bool ReleaseHandle()
      {
        return Gdi32.DeleteObject(handle);
      }
    }
  }
}
