﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Win32
{
  public static class WinMM
  {
    [DllImport("winmm.dll")]
    public static extern uint mciSendString(string command, StringBuilder returnValue, int returnLength, IntPtr winHandle);
  }
}
