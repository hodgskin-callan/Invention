﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Win32
{
  public static class Ole32
  {
    [DllImport("ole32.dll")]
    public static extern int ProgIDFromCLSID([In()] ref Guid clsid, [MarshalAs(UnmanagedType.LPWStr)] out string lplpszProgID);

    [DllImport("ole32.dll")]
    public static extern void CoTaskMemFree(IntPtr pv);

    [DllImport("ole32.dll")]
    public static extern void ReleaseStgMedium(ref System.Runtime.InteropServices.ComTypes.STGMEDIUM pmedium);

    [DllImport("ole32")]
    public static extern int CLSIDFromProgIDEx([MarshalAs(UnmanagedType.LPWStr)] string lpszProgID, out Guid lpclsid);
  }

  public static class Oleaut32
  {
    [DllImport("oleaut32")]
    public static extern int GetActiveObject([MarshalAs(UnmanagedType.LPStruct)] Guid rclsid, IntPtr pvReserved, [MarshalAs(UnmanagedType.IUnknown)] out object ppunk);
  }
}
