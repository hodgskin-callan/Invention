﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  /// <summary>
  /// Helper class for launching ASP.NET Core web applications and optionally displaying the site in a browser.
  /// </summary>
  public sealed class AspNetCoreLauncher
  {
    /// <summary>
    /// Configures a new ASP.NET Core web application launcher.
    /// </summary>
    /// <param name="ApplicationPath">Path to the executable for the web application.</param>
    /// <param name="WorkingDirectory">Path to the working directory for the web application.</param>
    /// <param name="UseHttps">Whether or not to use HTTPS.</param>
    /// <param name="Port">Port for the application to listen on, or null for the default.</param>
    public AspNetCoreLauncher(string ApplicationPath, string WorkingDirectory = null, bool UseHttps = true, int? Port = null)
    {
      this.ApplicationPath = ApplicationPath;
      this.WorkingDirectory = WorkingDirectory;
      this.UseHttps = UseHttps;
      // Default port number is arbitrary, but the number should be static rather than a random number so that you can set a browser exemption for the HTTPS URL with dev certificate and have it stick for every launch.
      this.Port = Port ?? 32789;
    }

    /// <summary>
    /// Whether or not the application has been launched.
    /// </summary>
    public bool IsRunning => WebProcess != null;
    /// <summary>
    /// The local URL for the web application.
    /// </summary>
    public string Url => $"{(UseHttps ? "https" : "http")}://localhost:{Port}";

    /// <summary>
    /// Starts the web application. This will open in a new console window with debug output displayed.
    /// </summary>
    public void Start()
    {
      AssignNewProcess();
      WebProcess.Start();
    }
    /// <summary>
    /// Starts the web application and opens a browser window once it starts accepting requests. The web application will open in a new console window with no output displayed.
    /// </summary>
    public void StartAndLaunchBrowser()
    {
      var HasLaunchedBrowser = false;

      AssignNewProcess();
      WebProcess.StartInfo.RedirectStandardOutput = true;
      WebProcess.StartInfo.UseShellExecute = false;
      WebProcess.OutputDataReceived += (_, Args) =>
      {
        Debug.WriteLine(Args.Data);

        if (!HasLaunchedBrowser && Args.Data.StartsWith("Now listening on:", StringComparison.InvariantCultureIgnoreCase))
        {
          HasLaunchedBrowser = true;
          LaunchBrowser();
        }
      };

      WebProcess.Start();
      WebProcess.BeginOutputReadLine();
    }
    /// <summary>
    /// Stops the web application.
    /// </summary>
    public void Stop()
    {
      try
      {
        WebProcess?.Kill();
      }
      catch
      {
      }

      WebProcess = null;
    }
    /// <summary>
    /// Opens the web application in a browser.
    /// </summary>
    public void LaunchBrowser()
    {
      System.Diagnostics.Process.Start(Url);
    }

    private void EnsureNotRunning()
    {
      if (WebProcess != null)
        throw new InvalidOperationException("Web application is already running.");
    }
    private void AssignNewProcess()
    {
      EnsureNotRunning();

      this.WebProcess = new System.Diagnostics.Process();
      WebProcess.StartInfo.FileName = ApplicationPath;
      WebProcess.StartInfo.Arguments = $"--urls={Url}";
      WebProcess.StartInfo.WorkingDirectory = WorkingDirectory;
    }

    private System.Diagnostics.Process WebProcess;
    private readonly string ApplicationPath;
    private readonly string WorkingDirectory;
    private readonly bool UseHttps;
    private readonly int Port;
  }
}