﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using Inv.Support;

// for access to WpfEngine.
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("InvPlay, PublicKey=00240000048000009400000006020000002400005253413100040000010001006dbeea36eb5ced1aa13e22b63bffb9abc55372b8801d77a8fd022ce31a8c09a0d395bbcc1cda900768373cfe7b17813192e8a750bc0a681b295ba28d0d66ad5e98993894fd93876fdf2878ca4798ffb9d944f75a5ad0fdfbb4f549a7b6457339d898e21a744b88ac5bdfb878c9b0a9b17f2a910a778cf65b4903aaeef51b6fd2")]

namespace Inv
{
  /// <summary>
  /// The Windows Desktop (WPF) implementation of the portable Invention application.
  /// </summary>
  public static class WpfShell
  {
    static WpfShell()
    {
      // used for this assembly's resource streams.
      ThisAssembly = Assembly.GetExecutingAssembly();

      // the entry assembly won't change during execution.
      var MainAssembly = Assembly.GetEntryAssembly();
      var MainPath = MainAssembly == null ? Path.GetTempPath() : Path.GetDirectoryName(MainAssembly.Location);

      OptionsThreadLocal = new ThreadLocal<WpfOptions>(() =>
      {
        var Result = new WpfOptions();

        Result.MainFolderPath = MainPath;
        Result.MainAssembly = MainAssembly ?? ThisAssembly;

        return Result;
      });

      System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
    }

    public static WpfOptions Options => OptionsThreadLocal.Value;

    /// <summary>
    /// Width in logical pixels of the primary screen.
    /// </summary>
    public static int PrimaryScreenWidth => (int)System.Windows.SystemParameters.PrimaryScreenWidth;
    /// <summary>
    /// Height in logical pixels of the primary screen.
    /// </summary>
    public static int PrimaryScreenHeight => (int)System.Windows.SystemParameters.PrimaryScreenHeight;

    /// <summary>
    /// Check that .NET 4.8 is installed.
    /// </summary>
    /// <param name="Action"></param>
    public static void CheckRequirements(Action Action)
    {
      // NOTE: class "ReflectionContext" exists from .NET 4.5 onwards.
      if (Type.GetType("System.Reflection.ReflectionContext", throwOnError: false) == null)
      {
        System.Windows.MessageBox.Show("Please install the Microsoft .NET 4.8 runtime.", "");
        return;
      }

      try
      {
        Action();
      }
      catch (Exception Exception)
      {
        System.Windows.MessageBox.Show(Exception.AsReport());
      }
    }
    /// <summary>
    /// Run the application in a window.
    /// </summary>
    /// <param name="BridgeAction"></param>
    public static void RunBridge(Action<WpfBridge> BridgeAction)
    {
      var WpfEngine = new WpfEngine();

      var WpfBridge = new WpfBridge(WpfEngine.WpfHost);
      WpfEngine.Install(WpfBridge.Application);

      BridgeAction?.Invoke(WpfBridge);

      WpfEngine.Run();
    }
    /// <summary>
    /// Run the application in a window.
    /// </summary>
    /// <param name="InvAction"></param>
    public static void Run(Action<Inv.Application> InvAction)
    {
      RunBridge(B =>
      {
        InvAction?.Invoke(B.Application);
      });
    }
    /// <summary>
    /// Host the application in a WPF content control.
    /// </summary>
    /// <param name="WpfContainer"></param>
    public static WpfBridge EmbedBridge(System.Windows.Controls.ContentControl WpfContainer)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(WpfContainer != null, "WpfContainer must be specified.");

      var WpfHost = new WpfHost(WpfContainer);

      var WpfBridge = new WpfBridge(WpfHost);
      WpfHost.Install(WpfBridge.Application);

      return WpfBridge;
    }

    internal static Stream GetResourceStream(string Location)
    {
      return ThisAssembly.GetManifestResourceStream(Location);
    }
    internal static System.Windows.ResourceDictionary LoadResourceDictionary(string Name)
    {
      var Assembly = typeof(WpfShell).Assembly;

      var BaseName = "pack://application:,,,/" + Assembly.GetName().Name + ";component/";
      var Dictionary = new System.Windows.ResourceDictionary();

      if (Dictionary.TrySetSource(new Uri(BaseName + "resources/" + Name, UriKind.Absolute)) || Dictionary.TrySetSource(new Uri(BaseName + Name, UriKind.Absolute)))
      {
        return Dictionary;
      }
      else
      {
        Debug.WriteLine("Unable to find resource dictionary: " + Name);
        return null;
      }
    }
    [DebuggerNonUserCode]
    internal static bool TrySetSource(this System.Windows.ResourceDictionary ResourceDictionary, Uri Uri)
    {
      try
      {
        ResourceDictionary.Source = Uri;

        return true;
      }
      catch
      {
        return false;
      }
    }
    internal static System.Windows.Media.FontFamily NewFontFamily(string FontName)
    {
      var BaseUri = FontName != null && FontName.StartsWith("./", StringComparison.InvariantCultureIgnoreCase) ? new Uri("pack://application:,,,/") : null;

      return new System.Windows.Media.FontFamily(BaseUri, FontName);
    }

    private static readonly ThreadLocal<WpfOptions> OptionsThreadLocal;
    private static readonly Assembly ThisAssembly;
  }

  public sealed class WpfSystemTray
  {
    internal WpfSystemTray(WpfHost WpfHost)
    {
      this.WpfHost = WpfHost;
      this.SystemTray = new Inv.SystemTray();
    }

    public string Tooltip
    {
      get => SystemTray.Tooltip;
      set => SystemTray.Tooltip = value;
    }
    public void Show()
    {
      SystemTray.WindowHandle = WpfHost.GetInteropWindowHandle(); // System.Diagnostics.Process.GetCurrentProcess().Handle;
      SystemTray.IconHandle = WpfHost.GetWindow()?.Icon.ConvertToDrawingBitmap().GetHicon() ?? IntPtr.Zero;
      SystemTray.Show();
    }
    public void Hide()
    {
      SystemTray.Hide();
    }

    public void ToastError(string Title, string Text) => NewToast(Title, Text).ShowError();
    public void ToastWarning(string Title, string Text) => NewToast(Title, Text).ShowWarning();
    public void ToastInformation(string Title, string Text) => NewToast(Title, Text).ShowInformation();

    private Inv.SystemToast NewToast(string Title, string Text)
    {
      var Toast = SystemTray.NewToast();
      Toast.Title = Title;
      Toast.Text = Text;
      return Toast;
    }

    private readonly WpfHost WpfHost;
    private readonly Inv.SystemTray SystemTray;
  }

  public sealed class WpfOptions
  {
    internal WpfOptions()
    {
      this.FullScreenMode = true;
      this.DefaultWindowX = null;
      this.DefaultWindowY = null;
      this.DefaultWindowWidth = 800;
      this.DefaultWindowHeight = 600;
      this.DefaultWindowColour = Inv.Colour.Black;
      this.DefaultFontName = "Segoe UI";
      this.DeviceEmulationArray = WpfDeviceEmulation.Array;
      this.DeviceEmulationFramed = true;
#if DEBUG
      this.PreventDeviceEmulation = false;
#else
      this.PreventDeviceEmulation = true;
#endif

      this.MaximumNumberOfControllers = 0; // controller support is opt-in.
    }

    /// <summary>
    /// Run the window in full screen.
    /// </summary>
    public bool FullScreenMode { get; set; }
    /// <summary>
    /// Optional default X position for the window.
    /// </summary>
    public int? DefaultWindowX { get; set; }
    /// <summary>
    /// Optional default Y position for the window.
    /// </summary>
    public int? DefaultWindowY { get; set; }
    /// <summary>
    /// Make the window maximized by default.
    /// </summary>
    public bool DefaultWindowMaximised { get; set; }
    /// <summary>
    /// Default width of the window.
    /// </summary>
    public int DefaultWindowWidth { get; set; }
    /// <summary>
    /// Default height of the window.
    /// </summary>
    public int DefaultWindowHeight { get; set; }
    /// <summary>
    /// Default background colour for the window.
    /// </summary>
    public Inv.Colour DefaultWindowColour { get; set; }
    /// <summary>
    /// Default opacity for the window.
    /// <para>Note: this is the opacity of the background of the window, and will not affect child controls of the window.</para>
    /// </summary>
    public double? DefaultWindowOpacity { get; set; }
    /// <summary>
    /// Default font name used as the fallback when not explicitly set.
    /// </summary>
    public string DefaultFontName { get; set; }
    /// <summary>
    /// Prevent the device emulation user interface (recommended for release builds).
    /// </summary>
    public bool PreventDeviceEmulation { get; set; }
    /// <summary>
    /// Optional unique identity string to prevent multiple instances of this program.
    /// </summary>
    public string SingletonIdentity { get; set; }
    /// <summary>
    /// Currently active emulated device.
    /// </summary>
    public WpfDeviceEmulation DeviceEmulation { get; set; }
    /// <summary>
    /// The subset of devices that can be emulated in this program.
    /// </summary>
    public WpfDeviceEmulation[] DeviceEmulationArray { get; set; }
    /// <summary>
    /// Rotate the device by default.
    /// </summary>
    public bool DeviceEmulationRotated { get; set; }
    /// <summary>
    /// Display the device frame by default.
    /// </summary>
    public bool DeviceEmulationFramed { get; set; }
    /// <summary>
    /// Override the main folder path, otherwise uses the entry assembly location.
    /// </summary>
    public string MainFolderPath { get; set; }
    /// <summary>
    /// Override where the application finds assets, otherwise is in the main folder path \Assets.
    /// </summary>
    public string OverrideAssetFolderPath { get; set; }
    /// <summary>
    /// Suppress the warning dialog when the audio driver is not installed or not correct for playback.
    /// </summary>
    public bool SuppressAudioDriverWarnings { get; set; }
    /// <summary>
    /// Application wide disabling of mouse-over tooltips.
    /// </summary>
    public bool DisableTooltips { get; set; }
    /// <summary>
    /// Maximum number of gamepad controllers this application supports.
    /// </summary>
    public int MaximumNumberOfControllers { get; set; }

    public bool DarkTheme { get; set; }

    internal Assembly MainAssembly { get; set; }
  }

  /// <summary>
  /// The bridge can be used to break the abstraction and reach into the implementation controls.
  /// This is for workarounds where Invention does not adequately handle a native scenario.
  /// </summary>
  public sealed class WpfBridge
  {
    internal WpfBridge(WpfHost Host)
    {
      this.Host = Host;
      this.Application = new Inv.Application();
      this.SystemTray = new WpfSystemTray(Host);
    }

    /// <summary>
    /// The portable application.
    /// </summary>
    public Inv.Application Application { get; }
    /// <summary>
    /// Access to the Windows System Tray.
    /// </summary>
    public WpfSystemTray SystemTray { get; }
    /// <summary>
    /// Whether or not the bridge is currently active (started and not yet stopped).
    /// </summary>
    public bool IsActive => Host.IsActive;

    /// <summary>
    /// Retrieve the <see cref="WpfSurface"/>
    /// </summary>
    /// <param name="InvSurface"></param>
    /// <returns></returns>
    public WpfSurface GetSurface(Inv.Surface InvSurface)
    {
      return Host.TranslateSurface(InvSurface);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfBlock"/>
    /// </summary>
    /// <param name="InvBlock"></param>
    /// <returns></returns>
    public WpfBlock GetBlock(Inv.Block InvBlock)
    {
      return Host.TranslateBlock(InvBlock);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfBoard"/>
    /// </summary>
    /// <param name="InvBoard"></param>
    /// <returns></returns>
    public WpfBoard GetBoard(Inv.Board InvBoard)
    {
      return Host.TranslateBoard(InvBoard);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfBrowser"/>
    /// </summary>
    /// <param name="InvBrowser"></param>
    /// <returns></returns>
    public WpfBrowser GetBrowser(Inv.Browser InvBrowser)
    {
      return Host.TranslateBrowser(InvBrowser);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfButton"/>
    /// </summary>
    /// <param name="InvButton"></param>
    /// <returns></returns>
    public WpfButton GetButton(Inv.Button InvButton)
    {
      return Host.TranslateButton(InvButton);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfCanvas"/>
    /// </summary>
    /// <param name="InvCanvas"></param>
    /// <returns></returns>
    public WpfCanvas GetCanvas(Inv.Canvas InvCanvas)
    {
      return Host.TranslateCanvas(InvCanvas);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfDock"/>
    /// </summary>
    /// <param name="InvDock"></param>
    /// <returns></returns>
    public WpfDock GetDock(Inv.Dock InvDock)
    {
      return Host.TranslateDock(InvDock);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfEdit"/>
    /// </summary>
    /// <param name="InvEdit"></param>
    /// <returns></returns>
    public WpfEdit GetEdit(Inv.Edit InvEdit)
    {
      return Host.TranslateEdit(InvEdit);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfFlow"/>
    /// </summary>
    /// <param name="InvFlow"></param>
    /// <returns></returns>
    public WpfFlow GetFlow(Inv.Flow InvFlow)
    {
      return Host.TranslateFlow(InvFlow);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfFrame"/>
    /// </summary>
    /// <param name="InvFrame"></param>
    /// <returns></returns>
    public WpfFrame GetFrame(Inv.Frame InvFrame)
    {
      return Host.TranslateFrame(InvFrame);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfGraphic"/>
    /// </summary>
    /// <param name="InvGraphic"></param>
    /// <returns></returns>
    public WpfGraphic GetGraphic(Inv.Graphic InvGraphic)
    {
      return Host.TranslateGraphic(InvGraphic);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfLabel"/>
    /// </summary>
    /// <param name="InvLabel"></param>
    /// <returns></returns>
    public WpfLabel GetLabel(Inv.Label InvLabel)
    {
      return Host.TranslateLabel(InvLabel);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfMemo"/>
    /// </summary>
    /// <param name="InvMemo"></param>
    /// <returns></returns>
    public WpfMemo GetMemo(Inv.Memo InvMemo)
    {
      return Host.TranslateMemo(InvMemo);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfNative"/>
    /// </summary>
    /// <param name="InvNative"></param>
    /// <returns></returns>
    public WpfNative GetNative(Inv.Native InvNative)
    {
      return Host.TranslateNative(InvNative);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfOverlay"/>
    /// </summary>
    /// <param name="InvOverlay"></param>
    /// <returns></returns>
    public WpfOverlay GetOverlay(Inv.Overlay InvOverlay)
    {
      return Host.TranslateOverlay(InvOverlay);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfScroll"/>
    /// </summary>
    /// <param name="InvScroll"></param>
    /// <returns></returns>
    public WpfScroll GetScroll(Inv.Scroll InvScroll)
    {
      return Host.TranslateScroll(InvScroll);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfShape"/>
    /// </summary>
    /// <param name="InvShape"></param>
    /// <returns></returns>
    public WpfShape GetShape(Inv.Shape InvShape)
    {
      return Host.TranslateShape(InvShape);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfStack"/>
    /// </summary>
    /// <param name="InvStack"></param>
    /// <returns></returns>
    public WpfStack GetStack(Inv.Stack InvStack)
    {
      return Host.TranslateStack(InvStack);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfTable"/>
    /// </summary>
    /// <param name="InvTable"></param>
    /// <returns></returns>
    public WpfTable GetTable(Inv.Table InvTable)
    {
      return Host.TranslateTable(InvTable);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfVideo"/>
    /// </summary>
    /// <param name="InvVideo"></param>
    /// <returns></returns>
    public WpfVideo GetVideo(Inv.Video InvVideo)
    {
      return Host.TranslateVideo(InvVideo);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfWrap"/>
    /// </summary>
    /// <param name="InvWrap"></param>
    /// <returns></returns>
    public WpfWrap GetWrap(Inv.Wrap InvWrap)
    {
      return Host.TranslateWrap(InvWrap);
    }
    /// <summary>
    /// Retrieve the <see cref="WpfPopup"/>
    /// </summary>
    /// <param name="InvPopup"></param>
    /// <returns></returns>
    public WpfPopup GetPopup(Inv.Popup InvPopup)
    {
      return Host.TranslatePopup(InvPopup);
    }
    /// <summary>
    /// Retrieve the <see cref="System.Windows.FrameworkElement"/>
    /// </summary>
    /// <param name="InvPanel"></param>
    /// <returns></returns>
    public System.Windows.FrameworkElement GetPanel(Inv.Panel InvPanel)
    {
      return Host.GetPanel(InvPanel);
    }

    // for use in friend Exp.Application.
    internal event Action ShutdownEvent
    {
      add => Host.ShutdownEvent += value;
      remove => Host.ShutdownEvent -= value;
    }
    internal void Start()
    {
      Host.Start();
    }
    internal void Stop()
    {
      Host.Stop();
    }

    private readonly WpfHost Host;
  }

  internal sealed class WpfPlatform : Inv.Platform
  {
    internal WpfPlatform(WpfHost Host)
    {
      this.Host = Host;
      this.Vault = new WpfVault();
      this.CurrentProcess = System.Diagnostics.Process.GetCurrentProcess();
    }

    int Platform.ThreadAffinity()
    {
      return Thread.CurrentThread.ManagedThreadId;
    }
    string Platform.CalendarTimeZoneName()
    {
      return TimeZoneInfo.Local.Id;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      Host.ShowCalendarPicker(CalendarPicker);
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      var Mapi = new Inv.Mapi();

      foreach (var To in EmailMessage.GetTos())
        Mapi.AddRecipient(To.Name, To.Address, false);

      foreach (var Attachment in EmailMessage.GetAttachments())
        Mapi.Attach(Host.SelectFilePath(Attachment.File), Attachment.Name);

      // NOTE: if subject is not supplied, then body will be ignored by Mapi (this is a workaround).
      var Subject = EmailMessage.Subject;
      if (string.IsNullOrEmpty(Subject) && !string.IsNullOrEmpty(EmailMessage.Body))
        Subject = " ";

      var Result = Mapi.Send(Subject, EmailMessage.Body);

      // NOTE: seems to mess up the full screen mode slightly in the bottom right edge.
      //if (Result)
      //  WpfWindow.WindowState = System.Windows.WindowState.Minimized;

      return Result;
    }
    bool Platform.PhoneIsSupported => false;
    void Platform.PhoneDial(string PhoneNumber)
    {
    }
    void Platform.PhoneSMS(string PhoneNumber)
    {
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      return new System.IO.FileInfo(Host.SelectFilePath(File)).Length;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      return new System.IO.FileInfo(Host.SelectFilePath(File)).LastWriteTimeUtc;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      System.IO.File.SetLastWriteTimeUtc(Host.SelectFilePath(File), Timestamp);
    }
    Stream Platform.DirectoryCreateFile(File File)
    {
      return new System.IO.FileStream(Host.SelectFilePath(File), FileMode.Create, FileAccess.Write, FileShare.Read, 65536);
    }
    Stream Platform.DirectoryAppendFile(File File)
    {
      return new System.IO.FileStream(Host.SelectFilePath(File), FileMode.Append, FileAccess.Write, FileShare.Read, 65536);
    }
    Stream Platform.DirectoryOpenFile(File File)
    {
      return new System.IO.FileStream(Host.SelectFilePath(File), FileMode.Open, FileAccess.Read, FileShare.Read, 65536);
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      return System.IO.File.Exists(Host.SelectFilePath(File));
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      System.IO.File.Delete(Host.SelectFilePath(File));
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Copy(Host.SelectFilePath(SourceFile), Host.SelectFilePath(TargetFile));
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Move(Host.SelectFilePath(SourceFile), Host.SelectFilePath(TargetFile));
    }
    void Platform.DirectoryReplaceFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Replace(Host.SelectFilePath(SourceFile), Host.SelectFilePath(TargetFile), destinationBackupFileName: null);
    }
    IEnumerable<Folder> Platform.DirectoryGetFolderSubfolders(Folder Folder, string FolderMask)
    {
      return new System.IO.DirectoryInfo(Host.SelectFolderPath(Folder)).GetDirectories(FolderMask).Select(F => Folder.NewFolder(F.Name));
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      return new System.IO.DirectoryInfo(Host.SelectFolderPath(Folder)).GetFiles(FileMask).Select(F => Folder.NewFile(F.Name));
    }
    IEnumerable<Asset> Platform.DirectoryGetAssets(string FileMask)
    {
      return new System.IO.DirectoryInfo(Host.SelectAssetFolderPath()).GetFiles(FileMask).Select(F => Host.InvApplication.Directory.NewAsset(F.Name));
    }
    long Platform.DirectoryGetLengthAsset(Asset Asset)
    {
      return new System.IO.FileInfo(Host.SelectAssetPath(Asset)).Length;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcAsset(Asset Asset)
    {
      return new System.IO.FileInfo(Host.SelectAssetPath(Asset)).LastWriteTimeUtc;
    }
    Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      return new System.IO.FileStream(Host.SelectAssetPath(Asset), FileMode.Open, FileAccess.Read, FileShare.Read, 65536);
    }
    bool Platform.DirectoryExistsAsset(Asset Asset)
    {
      return System.IO.File.Exists(Host.SelectAssetPath(Asset));
    }
    void Platform.DirectoryShowFilePicker(DirectoryFilePicker FilePicker)
    {
      var WpfFileDialog = new WpfFileDialog(FilePicker.Title);

      switch (FilePicker.PickType)
      {
        case PickType.Any:
          break;

        case PickType.Image:
          WpfFileDialog.Filter.AddImageFile();
          break;

        case PickType.Sound:
          WpfFileDialog.Filter.AddMp3File();
          break;

        default:
          throw new Exception("FileType not handled: " + FilePicker.PickType);
      }

      var Result = WpfFileDialog.OpenSingleFile();

      if (Result != null)
        FilePicker.SelectInvoke(new Pick(System.IO.Path.GetFileName(Result), () => new System.IO.FileStream(Result, FileMode.Open, FileAccess.Read, FileShare.Read, 65536)));
      else
        FilePicker.CancelInvoke();
    }
    string Platform.DirectoryGetFolderPath(Inv.Folder Folder)
    {
      return Host.SelectFolderPath(Folder);
    }
    string Platform.DirectoryGetFilePath(Inv.File File)
    {
      return Host.SelectFilePath(File);
    }
    bool Platform.LocationIsSupported
    {
#if DEBUG
      get => true;
#else
      get => false;
#endif
    }
    void Platform.LocationShowMap(string Location)
    {
      System.Diagnostics.Process.Start("https://www.google.com/maps/place/" + Location.Replace([' ', ','], '+'));
    }
    void Platform.LocationLookup(LocationResult LocationLookup)
    {
#if DEBUG
      if (LocationLookup.Coordinate.Latitude == -31.900982 && LocationLookup.Coordinate.Longitude == 115.828734)
      {
        var Task = System.Threading.Tasks.Task.Run(() =>
        {
          Thread.Sleep(1000);

          LocationLookup.SetPlacemarks([new Inv.LocationPlacemark
          (
            Name: "138 Main Street",
            Locality: "Osborne Park",
            SubLocality: "Perth",
            PostalCode: "6017",
            AdministrativeArea: "WA",
            SubAdministrativeArea: null,
            CountryName: "Australia",
            CountryCode : "AU",
            Latitude: -31.900982,
            Longitude: 115.828734
          )]);
        });
      }
      else
#endif
        // TODO: implement using Bing web services?
        LocationLookup.SetPlacemarks([]);
    }
    void Platform.AudioPlaySound(Sound Sound, float Volume, float Rate, float Pan)
    {
      if (Host.HasSound)
        Host.PlaySound(Sound, Volume, Rate, Pan, Loop: false);
    }
    TimeSpan Platform.AudioGetSoundLength(Inv.Sound Sound)
    {
      if (Host.HasSound)
        return Host.GetSoundLength(Sound);
      else
        return TimeSpan.Zero;
    }
    void Platform.AudioPlayClip(AudioClip Clip)
    {
      if (Host.HasSound)
        Clip.Node = Host.PlaySound(Clip.Sound, Clip.Volume, Clip.Rate, Clip.Pan, Clip.Looped);
    }
    void Platform.AudioPauseClip(AudioClip Clip)
    {
      if (Host.HasSound)
        Host.PauseClip(Clip);
    }
    void Platform.AudioResumeClip(AudioClip Clip)
    {
      if (Host.HasSound)
        Host.ResumeClip(Clip);
    }
    void Platform.AudioStopClip(AudioClip Clip)
    {
      if (Host.HasSound)
        Host.StopClip(Clip);
    }
    void Platform.AudioModulateClip(AudioClip Clip)
    {
      if (Host.HasSound)
        Host.ModulateClip(Clip);
    }
    void Platform.AudioStartSpeechRecognition(Inv.SpeechRecognition SpeechRecognition)
    {
      Host.StartSpeechRecognition(SpeechRecognition);
    }
    void Platform.AudioStopSpeechRecognition(Inv.SpeechRecognition SpeechRecognition)
    {
      Host.StopSpeechRecognition(SpeechRecognition);
    }
    bool Platform.AudioSpeechRecognitionIsSupported => !Host.WpfOptions.PreventDeviceEmulation;
    void Platform.WindowBrowse(File File)
    {
      System.Diagnostics.Process.Start(Host.SelectFilePath(File));
    }
    void Platform.WindowShare(File File)
    {
      var WpfFileDialog = new WpfFileDialog(File.Name);

      WpfFileDialog.SetInitialFilePath(File.Name);
      WpfFileDialog.SetDefaultFileExtension(File.Extension);
      WpfFileDialog.Filter.AddFile(File.Extension.Trim('.').ToSentenceCase() + " File", File.Extension);
      WpfFileDialog.Filter.AddAllFile();

      var Result = WpfFileDialog.SaveSingleFile();

      if (Result != null)
        System.IO.File.Copy(Host.SelectFilePath(File), Result, true);
    }
    void Platform.WindowPost(Action Action)
    {
      Host.Post(Action);
    }
    void Platform.WindowCall(Action Action)
    {
      Host.Call(Action);
    }
    Inv.Dimension Platform.WindowGetDimension(Inv.Panel Panel)
    {
      var Result = Host.GetPanel(Panel);

      return Result == null ? Inv.Dimension.Zero : new Dimension((int)Result.ActualWidth, (int)Result.ActualHeight);
    }
    void Platform.WindowStartAnimation(Inv.Animation Animation)
    {
      Host.StartAnimation(Animation);
    }
    void Platform.WindowStopAnimation(Inv.Animation Animation)
    {
      Host.StopAnimation(Animation);
    }
    void Platform.WindowShowPopup(Inv.Popup Popup)
    {
      Host.ShowPopup(Popup);
    }
    void Platform.WindowHidePopup(Inv.Popup Popup)
    {
      Host.HidePopup(Popup);
    }
    long Platform.ProcessMemoryUsedBytes()
    {
      CurrentProcess.Refresh();
      return CurrentProcess.PrivateMemorySize64;
    }
    string Platform.ProcessAncillaryInformation()
    {
      return "(tier " + (System.Windows.Media.RenderCapability.Tier >> 16) + ")";
    }
    void Platform.WebClientConnect(WebClient WebClient)
    {
      var TcpClient = new Inv.Tcp.Client(WebClient.Host, WebClient.Port);
      TcpClient.Connect();

      WebClient.Node = TcpClient;
      WebClient.SetTransportStream(TcpClient.Stream);
    }
    void Platform.WebClientDisconnect(WebClient WebClient)
    {
      var TcpClient = (Inv.Tcp.Client)WebClient.Node;
      if (TcpClient != null)
      {
        WebClient.Node = null;
        WebClient.SetTransportStream(null);

        TcpClient.Disconnect();
      }
    }
    void Platform.WebServerConnect(WebServer WebServer)
    {
      var TcpServer = new Inv.Tcp.Server(WebServer.Host, WebServer.Port);
      TcpServer.AcceptEvent += (TcpChannel) => WebServer.AcceptChannel(TcpChannel, TcpChannel.Stream);
      TcpServer.RejectEvent += (TcpChannel) => WebServer.RejectChannel(TcpChannel);

      WebServer.Node = TcpServer;
      WebServer.DropDelegate = (Node) => ((Inv.Tcp.Channel)Node).Drop();

      TcpServer.Connect();
    }
    void Platform.WebServerDisconnect(WebServer WebServer)
    {
      var TcpServer = (Inv.Tcp.Server)WebServer.Node;
      if (TcpServer != null)
      {
        TcpServer.Disconnect();

        WebServer.Node = null;
        WebServer.DropDelegate = null;
      }
    }
    void Platform.WebBroadcastConnect(WebBroadcast WebBroadcast)
    {
      var UdpBroadcast = new Inv.Udp.Broadcast();

      WebBroadcast.Node = UdpBroadcast;
      WebBroadcast.SetBroadcast(UdpBroadcast);
    }
    void Platform.WebBroadcastDisconnect(WebBroadcast WebBroadcast)
    {
      var UdpBroadcast = (Inv.Udp.Broadcast)WebBroadcast.Node;
      if (UdpBroadcast != null)
      {
        UdpBroadcast.Close();

        WebBroadcast.Node = null;
        WebBroadcast.SetBroadcast(null);
      }
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      System.Diagnostics.Process.Start(Uri.AbsoluteUri);
    }
    void Platform.WebInstallUri(Uri Uri)
    {
      System.Diagnostics.Process.Start(Uri.AbsoluteUri);
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
    }
    void Platform.VaultLoadSecret(Secret Secret)
    {
      Vault.Load(Secret);
    }
    void Platform.VaultSaveSecret(Secret Secret)
    {
      Vault.Save(Secret);
    }
    void Platform.VaultDeleteSecret(Secret Secret)
    {
      Vault.Delete(Secret);
    }
    bool Platform.ClipboardIsTextSupported
    {
      get => true;
    }
    string Platform.ClipboardGetText()
    {
      return System.Windows.Clipboard.GetText();
    }
    void Platform.ClipboardSetText(string Text)
    {
      System.Windows.Clipboard.SetText(Text);
    }
    bool Platform.ClipboardIsImageSupported
    {
      get => true;
    }
    Inv.Image Platform.ClipboardGetImage()
    {
      var PngMemoryStream = System.Windows.Clipboard.GetData("PNG") as MemoryStream;

      if (PngMemoryStream != null)
      {
        using (PngMemoryStream)
          return WpfImage.Decode(PngMemoryStream).Encode(); // TODO: can we just put the memorystream bytes into an Inv.Image?
      }

      var DibMemoryStream = System.Windows.Clipboard.GetData("DeviceIndependentBitmap") as MemoryStream;
      if (DibMemoryStream != null)
      {
        var DibBuffer = new byte[DibMemoryStream.Length];
        DibMemoryStream.ReadByteArray(DibBuffer);

        var InfoHeader = BinaryStructConverter.FromByteArray<BITMAPINFOHEADER>(DibBuffer);

        var FileHeaderSize = System.Runtime.InteropServices.Marshal.SizeOf(typeof(BITMAPFILEHEADER));
        var InfoHeaderSize = InfoHeader.biSize;
        var FileSize = FileHeaderSize + InfoHeader.biSize + InfoHeader.biSizeImage;

        var FileHeader = new BITMAPFILEHEADER();
        FileHeader.bfType = BITMAPFILEHEADER.BM;
        FileHeader.bfSize = FileSize;
        FileHeader.bfReserved1 = 0;
        FileHeader.bfReserved2 = 0;
        FileHeader.bfOffBits = FileHeaderSize + InfoHeaderSize + InfoHeader.biClrUsed * 4;

        var FileHeaderBytes = BinaryStructConverter.ToByteArray(FileHeader);

        var ResultStream = new MemoryStream();
        ResultStream.Write(FileHeaderBytes, 0, FileHeaderSize);
        ResultStream.Write(DibBuffer, 0, DibBuffer.Length);
        ResultStream.Seek(0, SeekOrigin.Begin);

        return WpfImage.Decode(ResultStream).Encode();
      }

      return null;
    }
    void Platform.ClipboardSetImage(Inv.Image Image)
    {
      // NOTE: clipboard does not support transparency for images by default.
      //System.Windows.Clipboard.SetImage(Image.ConvertToMediaBitmapAndFreeze());

      System.Windows.Clipboard.Clear();

      var DataObject = new System.Windows.DataObject();

      using (var pngMemStream = new MemoryStream(Image.GetBuffer()))
      {
        // Standard bitmap, without transparency support.
        DataObject.SetData(System.Windows.DataFormats.Bitmap, Image.ConvertToMediaBitmapAndFreeze(), autoConvert: true);

        // Explicit PNG.
        DataObject.SetData("PNG", pngMemStream, autoConvert: false);

        // The 'copy=true' argument means the MemoryStream can be disposed.
        System.Windows.Clipboard.SetDataObject(DataObject, copy: true);
      }
    }
    void Platform.HapticFeedback(HapticFeedback Feedback)
    {
      Host.HapticFeedback(Feedback);
    }
    Inv.Dimension Platform.GraphicsGetDimension(Inv.Image Image)
    {
      var Result = Host.TranslateImage(Image)?.FirstBitmapSource;

      return Result == null ?
        Inv.Dimension.Zero :
        new Inv.Dimension(Result.PixelWidth, Result.PixelHeight);
      //new Inv.Dimension((int)(Result.PixelWidth / Host.ScaleFactorX), (int)(Result.PixelHeight / Host.ScaleFactorY));
    }
    Inv.Image Platform.GraphicsGrayscale(Inv.Image Image)
    {
      var WpfImage = Host.TranslateImage(Image);

      var WpfResult = WpfImage.Convert(F => F.AsGrayscale());

      return WpfResult.Encode();
    }
    Inv.Image Platform.GraphicsTint(Inv.Image Image, Inv.Colour Colour)
    {
      var WpfTintBrush = Host.TranslateBrush(Colour);

      var WpfImage = Host.TranslateImage(Image);

      var WpfResult = WpfImage.Convert(F => F.AsTint(WpfTintBrush));

      var InvResult = WpfResult.Encode();

      InvResult.Node = null; // TODO: requires a new translation to be used after tint?

      return InvResult;
    }
    Inv.Image Platform.GraphicsResize(Inv.Image Image, Inv.Dimension Dimension)
    {
      var WpfImage = Host.TranslateImage(Image);

      // TODO: resizing animated gifs causes the background of each frame to become black.
      if (WpfImage.Type == WpfImageType.Gif && WpfImage.IndividualFrames != null)
        return Image;

      var WpfResult = WpfImage.Convert(F => F.Resize(Dimension.Width, Dimension.Height));

      var InvResult = WpfResult.Encode();

      InvResult.Node = null; // TODO: requires a new translation to be used after resize?

      return InvResult;
    }
    Inv.Pixels Platform.GraphicsReadPixels(Inv.Image Image)
    {
      // TODO: this only supports a single frame image.
      return Host.TranslateImage(Image)?.FirstBitmapSource.ReadPixels();
    }
    Inv.Image Platform.GraphicsWritePixels(Inv.Pixels Pixels)
    {
      // TODO: this only supports a single frame image and we have lost the original image type.
      var BGRAPixels = (Pixels as Inv.BGRAByteArrayPixels) ?? new Inv.BGRAByteArrayPixels(Pixels);

      return BGRAPixels.WriteImage().ConvertToInvImagePng();
    }
    void Platform.GraphicsRender(Inv.Panel Panel, Action<Inv.Image> ReturnAction)
    {
      var WpfImage = Host.GetPanel(Panel).RenderToBitmapSource(300.0);

      var InvResult = WpfImage.ConvertToInvImagePng();

      try
      {
        ReturnAction(InvResult);
      }
      finally
      {
        Host.ReclaimImage([InvResult]);
      }
    }
    void Platform.GraphicsReclaim(IReadOnlyList<Inv.Image> ImageList)
    {
      Host.ReclaimImage(ImageList);
    }
    void Platform.AudioReclaim(IReadOnlyList<Inv.Sound> SoundList)
    {
      Host.ReclaimSound(SoundList);
    }
    Inv.Dimension Platform.CanvasCalculateText(string TextFragment, Inv.DrawFont TextFont, int MaximumTextWidth, int MaximumTextHeight)
    {
      var WpfFormattedText = Host.NewFormattedText(TextFragment, TextFont, MaximumTextWidth, MaximumTextHeight);

      return new Inv.Dimension((int)WpfFormattedText.Width, (int)WpfFormattedText.Height);
    }

    private readonly WpfHost Host;
    private readonly WpfVault Vault;
    private readonly System.Diagnostics.Process CurrentProcess;

    private static class BinaryStructConverter
    {
      public static T FromByteArray<T>(byte[] bytes) where T : struct
      {
        var ptr = IntPtr.Zero;
        try
        {
          var size = System.Runtime.InteropServices.Marshal.SizeOf(typeof(T));
          ptr = System.Runtime.InteropServices.Marshal.AllocHGlobal(size);
          System.Runtime.InteropServices.Marshal.Copy(bytes, 0, ptr, size);
          var obj = System.Runtime.InteropServices.Marshal.PtrToStructure(ptr, typeof(T));
          return (T)obj;
        }
        finally
        {
          if (ptr != IntPtr.Zero)
            System.Runtime.InteropServices.Marshal.FreeHGlobal(ptr);
        }
      }
      public static byte[] ToByteArray<T>(T obj) where T : struct
      {
        var ptr = IntPtr.Zero;
        try
        {
          var size = System.Runtime.InteropServices.Marshal.SizeOf(typeof(T));
          ptr = System.Runtime.InteropServices.Marshal.AllocHGlobal(size);
          System.Runtime.InteropServices.Marshal.StructureToPtr(obj, ptr, true);
          var bytes = new byte[size];
          System.Runtime.InteropServices.Marshal.Copy(ptr, bytes, 0, size);
          return bytes;
        }
        finally
        {
          if (ptr != IntPtr.Zero)
            System.Runtime.InteropServices.Marshal.FreeHGlobal(ptr);
        }
      }
    }

    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = 2)]
    private struct BITMAPFILEHEADER
    {
      public static readonly short BM = 0x4d42; // BM

      public short bfType;
      public int bfSize;
      public short bfReserved1;
      public short bfReserved2;
      public int bfOffBits;
    }

    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
    private struct BITMAPINFOHEADER
    {
      public int biSize;
      public int biWidth;
      public int biHeight;
      public short biPlanes;
      public short biBitCount;
      public int biCompression;
      public int biSizeImage;
      public int biXPelsPerMeter;
      public int biYPelsPerMeter;
      public int biClrUsed;
      public int biClrImportant;
    }
  }

  internal sealed class WpfEngine
  {
    internal WpfEngine()
    {
      this.WpfOptions = WpfShell.Options;

      // NOTE: this seems to be required if you need to use BackgroundWorker threads before any window has been shown.
      //       otherwise, the RunCompletedEvent will run in a random thread, instead of the originating thread.
      System.Threading.SynchronizationContext.SetSynchronizationContext(new System.Windows.Threading.DispatcherSynchronizationContext(System.Windows.Threading.Dispatcher.CurrentDispatcher));

      if (System.Windows.Application.Current == null)
      {
        // TODO: this is not re-entrant.
        var WpfApplication = new System.Windows.Application();
        Debug.Assert(System.Windows.Application.Current == WpfApplication);
        WpfApplication.ShutdownMode = System.Windows.ShutdownMode.OnExplicitShutdown;  // try to avoid the application from being explicitly shutdown as it means it cannot be run again.

        Inv.SystemExceptionHandler.Recruit(Object =>
        {
          var Exception = Object as Exception;

          if (Exception == null && Object != null)
            System.Windows.MessageBox.Show(Object.GetType().AssemblyQualifiedName + " - " + Object.ToString());
          else
            InvApplication.HandleExceptionInvoke(Exception);
        });
      }

      // NOTE: the first window becomes the MainWindow.
      this.WpfWindow = new WpfWindow()
      {
        Owner = null,
        UseLayoutRounding = true,
        SnapsToDevicePixels = true,
        Topmost = false,
        IsTabStop = false,
        //TaskbarItemInfo = new System.Windows.Shell.TaskbarItemInfo(),
        Width = WpfOptions.DefaultWindowWidth,
        Height = WpfOptions.DefaultWindowHeight,
        FontFamily = WpfShell.NewFontFamily(WpfOptions.DefaultFontName),
        //SizeToContent = System.Windows.SizeToContent.Manual
      };

      WpfWindow.FlowDirection = (System.Globalization.CultureInfo.DefaultThreadCurrentCulture ?? System.Globalization.CultureInfo.CurrentCulture).TextInfo.IsRightToLeft ? System.Windows.FlowDirection.RightToLeft : System.Windows.FlowDirection.LeftToRight;

      if (WpfOptions.PreventDeviceEmulation && WpfOptions.DefaultWindowOpacity != null && WpfOptions.DefaultWindowOpacity != 1.0)
      {
        WpfWindow.AllowsTransparency = true;
        WpfWindow.WindowStyle = System.Windows.WindowStyle.None;
      }

      if (WpfOptions.DisableTooltips)
        WpfWindow.SetValue(ToolTipBehavior.ToolTipEnabledProperty, false);

      // TODO: custom max/min/restore buttons?
      //var WpfChrome = new WpfChrome(WpfWindow);
      //WpfChrome.Toggle(false);

      if (WpfOptions.DefaultWindowX == null && WpfOptions.DefaultWindowY == null)
      {
        WpfWindow.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
      }
      else
      {
        WpfWindow.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;

        if (WpfOptions.DefaultWindowX != null)
          WpfWindow.Left = WpfOptions.DefaultWindowX.Value;

        if (WpfOptions.DefaultWindowY != null)
          WpfWindow.Top = WpfOptions.DefaultWindowY.Value;
      }

      if (WpfOptions.DefaultWindowMaximised)
        WpfWindow.WindowState = System.Windows.WindowState.Maximized;

      //System.Windows.Media.TextOptions.SetTextFormattingMode(WpfWindow, System.Windows.Media.TextFormattingMode.Display); // TODO: does this help with anything?
      WpfWindow.Closing += (Sender, Event) =>
      {
        Event.Cancel = !InvApplication.ExitQueryInvoke();
      };
      WpfWindow.Closed += (Sender, Event) =>
      {
        Shutdown();
      };
      WpfWindow.Activated += (Sender, Event) =>
      {
        WpfHost.CheckModifier();

        InvApplication.Window.ActivateInvoke();
      };
      WpfWindow.Deactivated += (Sender, Event) =>
      {
        InvApplication.Window.DeactivateInvoke();

        WpfHost.CheckModifier();
      };
      WpfWindow.PreviewKeyDown += (Sender, Event) =>
      {
        var IsAlt = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftAlt) || System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightAlt);

        if (IsAlt && Event.Key == System.Windows.Input.Key.System && Event.SystemKey == System.Windows.Input.Key.Return)
        {
          WpfOptions.FullScreenMode = !WpfOptions.FullScreenMode;

          if (WpfOptions.FullScreenMode)
            EnterFullScreen();
          else
            ExitFullScreen();

          WpfHost.Rearrange();

          Event.Handled = true;
        }
      };
      WpfWindow.MouseLeftButtonDown += (Sender, Event) =>
      {
        WpfWindow.DragMove();
      };

      if (WpfOptions.FullScreenMode)
        EnterFullScreen();

      this.WpfHost = new WpfHost(WpfWindow);
      WpfHost.ShutdownEvent += () => Shutdown();
    }

    public void Install(Inv.Application InvApplication)
    {
      this.InvApplication = InvApplication;
      WpfHost.Install(InvApplication);

      if (Debugger.IsAttached && WpfWindow.Icon == null)
      {
        var FileAssembly = Assembly.GetEntryAssembly();

        if (FileAssembly != null)
        {
          var FileLocation = FileAssembly.Location;

          if (string.Equals(System.IO.Path.GetExtension(FileLocation), ".dll", StringComparison.InvariantCultureIgnoreCase))
          {
            var ExeLocation = System.IO.Path.ChangeExtension(FileLocation, ".exe");
            if (System.IO.File.Exists(ExeLocation))
              FileLocation = ExeLocation;
          }

          var Icon = new Inv.Icon(FileLocation);
          WpfWindow.Icon = Icon.LoadSource((int)(32 * WpfHost.ScaleFactorX), (int)(32 * WpfHost.ScaleFactorY));
        }
      }
    }
    public void Uninstall()
    {
      WpfHost.Stop();

      // NOTE: The matching call to Recruit is inside the Inv.Application constructor (which is passed to the Install method).
      if (InvApplication != null)
      {
        InvApplication.Dismiss();
        this.InvApplication = null;
      }
    }
    /// <summary>
    /// Only called by the Play live editor to restart the engine after it has been reinstalled.
    /// </summary>
    public void Restart()
    {
      WpfHost.Start();

      Load();
    }
    public void Run()
    {
      var MainIdentity = WpfOptions.SingletonIdentity;
      if (MainIdentity == null)
      {
        var MainAssembly = Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly();
        MainIdentity = MainAssembly?.Location.Replace('\\', '_');
      }

      using (var SingletonMutex = MainIdentity != null ? new Inv.GlobalMutex(MainIdentity) : null)
      {
        var SingletonBound = SingletonMutex == null;

        if (!SingletonBound)
        {
          var SingletonAttempt = 0;
          while (SingletonAttempt < 10)
          {
            SingletonBound = SingletonMutex.Lock(TimeSpan.FromMilliseconds(500));

            if (SingletonBound)
            {
              break; // We control the mutex, can continue as the singleton process.
            }
            else
            {
              SingletonAttempt++;
            }
          }
        }

        if (SingletonBound)
        {
          WpfHost.Start();
          try
          {
            Load();

            if (WpfWindow.Visibility != System.Windows.Visibility.Visible)
              WpfWindow.Show();

            this.DispatcherFrame = new System.Windows.Threading.DispatcherFrame();
            DispatcherFrame.Dispatcher.UnhandledException += UnhandledException;
            try
            {
              System.Windows.Threading.Dispatcher.PushFrame(DispatcherFrame);
            }
            finally
            {
              if (DispatcherFrame != null)
              {
                DispatcherFrame.Dispatcher.UnhandledException -= UnhandledException;
                this.DispatcherFrame = null;
              }
            }
          }
          finally
          {
            WpfHost.Stop();
          }
        }
      }
    }
    public void Post(Action Action)
    {
      WpfHost.Post(Action);
    }

    internal Inv.Application InvApplication { get; private set; }
    internal WpfWindow WpfWindow { get; }
    internal WpfHost WpfHost { get; }
    internal WpfOptions WpfOptions { get; }

    internal void EnterFullScreen()
    {
      WpfWindow.WindowStyle = System.Windows.WindowStyle.None;
      WpfWindow.ResizeMode = System.Windows.ResizeMode.NoResize;

      this.WpfWindowChrome = System.Windows.Shell.WindowChrome.GetWindowChrome(WpfWindow);
      System.Windows.Shell.WindowChrome.SetWindowChrome(WpfWindow, null);

      WpfWindow.WindowState = System.Windows.WindowState.Maximized;
    }
    internal void ExitFullScreen()
    {
      WpfWindow.WindowStyle = System.Windows.WindowStyle.SingleBorderWindow;
      WpfWindow.ResizeMode = System.Windows.ResizeMode.CanResize;

      System.Windows.Shell.WindowChrome.SetWindowChrome(WpfWindow, WpfWindowChrome);

      WpfWindow.WindowState = System.Windows.WindowState.Normal;
    }

    private void Load()
    {
      WpfWindow.Title = InvApplication.Title ?? "";
    }
    private void Shutdown()
    {
      if (DispatcherFrame != null)
      {
        DispatcherFrame.Continue = false;
        DispatcherFrame.Dispatcher.UnhandledException -= UnhandledException;
        this.DispatcherFrame = null;
      }
    }
    private void UnhandledException(object Sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs Event)
    {
      if (!Event.Handled)
      {
        Inv.SystemExceptionHandler.Route(Event.Exception);

        Event.Handled = true;
      }
    }

    private System.Windows.Shell.WindowChrome WpfWindowChrome;
    private System.Windows.Threading.DispatcherFrame DispatcherFrame;
  }

  internal sealed class WpfHost
  {
    internal WpfHost(System.Windows.Controls.ContentControl WpfContainer)
    {
      this.WpfContainer = WpfContainer;
      this.WpfOptions = WpfShell.Options; // take a copy of the UI thread's options.

      this.WpfBorder = new System.Windows.Controls.Border();
      WpfContainer.Content = WpfBorder;

      // NOTE: this may have a positive affect on performance (unproven) but it definitely makes rounded edges ugly.
      //System.Windows.Media.RenderOptions.SetEdgeMode(WpfBorder, System.Windows.Media.EdgeMode.Aliased);

      this.WpfApplicationMaster = new WpfMaster();

      this.WpfClearFocusBorder = new System.Windows.Controls.Border();
      WpfApplicationMaster.AddChild(WpfClearFocusBorder);
      WpfClearFocusBorder.Focusable = true;
      WpfClearFocusBorder.Visibility = System.Windows.Visibility.Collapsed;

      this.WpfSurfaceMaster = new WpfMaster();
      WpfApplicationMaster.AddChild(WpfSurfaceMaster);
      WpfSurfaceMaster.ClipToBounds = true;
      System.Windows.NameScope.SetNameScope(WpfSurfaceMaster, new System.Windows.NameScope());

      this.WpfEmulatedSpeechRecognitionList = [];

      if (WpfOptions.PreventDeviceEmulation && WpfOptions.DefaultWindowOpacity != null && WpfOptions.DefaultWindowOpacity != 1.0)
      {
        var BackgroundBrush = new System.Windows.Media.SolidColorBrush(TranslateColour(WpfOptions.DefaultWindowColour))
        {
          Opacity = WpfOptions.DefaultWindowOpacity.Value
        };
        BackgroundBrush.Freeze();

        WpfContainer.Background = BackgroundBrush;
      }
      else
      {
        WpfBorder.Background = System.Windows.Media.Brushes.DimGray;
        WpfApplicationMaster.Background = TranslateBrush(WpfOptions.DefaultWindowColour);
      }

      if (WpfOptions.PreventDeviceEmulation)
      {
        WpfBorder.Child = WpfApplicationMaster;
      }
      else
      {
        var WpfEmulationOverlay = new WpfOverlay();
        WpfBorder.Child = WpfEmulationOverlay;
        WpfEmulationOverlay.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        WpfEmulationOverlay.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;

        const int IndentSize = 15;
        const int FontSize = 20;

        this.WpfEmulationDeviceLabel = new WpfLabel();
        WpfEmulationOverlay.AddChild(WpfEmulationDeviceLabel);
        WpfEmulationDeviceLabel.Foreground = System.Windows.Media.Brushes.White;
        WpfEmulationDeviceLabel.Padding = new System.Windows.Thickness(IndentSize);
        WpfEmulationDeviceLabel.FontSize = FontSize;
        WpfEmulationDeviceLabel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
        WpfEmulationDeviceLabel.VerticalAlignment = System.Windows.VerticalAlignment.Top;

        this.WpfEmulationFrameLabel = new WpfLabel();
        WpfEmulationOverlay.AddChild(WpfEmulationFrameLabel);
        WpfEmulationFrameLabel.Foreground = System.Windows.Media.Brushes.White;
        WpfEmulationFrameLabel.Padding = new System.Windows.Thickness(IndentSize);
        WpfEmulationFrameLabel.FontSize = FontSize;
        WpfEmulationFrameLabel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
        WpfEmulationFrameLabel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;

        this.WpfEmulationInstructionLabel = new WpfLabel();
        WpfEmulationOverlay.AddChild(WpfEmulationInstructionLabel);
        WpfEmulationInstructionLabel.Foreground = System.Windows.Media.Brushes.White;
        WpfEmulationInstructionLabel.Padding = new System.Windows.Thickness(IndentSize);
        WpfEmulationInstructionLabel.FontSize = FontSize;
        WpfEmulationInstructionLabel.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
        WpfEmulationInstructionLabel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;

        var WpfEmulationActionDock = new WpfDock();
        WpfEmulationOverlay.AddChild(WpfEmulationActionDock);
        WpfEmulationActionDock.Padding = new System.Windows.Thickness(IndentSize);
        WpfEmulationActionDock.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
        WpfEmulationActionDock.VerticalAlignment = System.Windows.VerticalAlignment.Top;

        var WpfEmulationSuspendButton = new WpfButton();
        WpfEmulationSuspendButton.AsFlat(System.Windows.Media.Brushes.DarkGray, System.Windows.Media.Brushes.LightGray, System.Windows.Media.Brushes.Gray);
        WpfEmulationSuspendButton.Padding = new System.Windows.Thickness(IndentSize);
        WpfEmulationSuspendButton.Margin = new System.Windows.Thickness(0, 0, 0, 5);
        WpfEmulationSuspendButton.IsEnabled = true;

        var WpfEmulationSuspendLabel = new WpfLabel();
        WpfEmulationSuspendButton.Content = WpfEmulationSuspendLabel;
        WpfEmulationSuspendLabel.Foreground = System.Windows.Media.Brushes.White;
        WpfEmulationSuspendLabel.FontSize = FontSize;
        WpfEmulationSuspendLabel.TextAlignment = System.Windows.TextAlignment.Center;
        WpfEmulationSuspendLabel.Text = "SUSPEND";

        var WpfEmulationResumeButton = new WpfButton();
        WpfEmulationResumeButton.AsFlat(System.Windows.Media.Brushes.DarkGray, System.Windows.Media.Brushes.LightGray, System.Windows.Media.Brushes.Gray);
        WpfEmulationResumeButton.Padding = new System.Windows.Thickness(IndentSize);
        WpfEmulationResumeButton.IsEnabled = false;

        var WpfEmulationResumeLabel = new WpfLabel();
        WpfEmulationResumeButton.Content = WpfEmulationResumeLabel;
        WpfEmulationResumeLabel.Foreground = System.Windows.Media.Brushes.White;
        WpfEmulationResumeLabel.FontSize = FontSize;
        WpfEmulationResumeLabel.TextAlignment = System.Windows.TextAlignment.Center;
        WpfEmulationResumeLabel.Text = "RESUME";

        WpfEmulationSuspendButton.LeftClick += () =>
        {
          WpfEmulationSuspendButton.IsEnabled = false;
          WpfEmulationResumeButton.IsEnabled = true;

          InvApplication.SuspendInvoke();
        };
        WpfEmulationResumeButton.LeftClick += () =>
        {
          WpfEmulationSuspendButton.IsEnabled = true;
          WpfEmulationResumeButton.IsEnabled = false;

          InvApplication.ResumeInvoke();
        };

        this.WpfEmulationHapticLabel = new WpfLabel();
        WpfEmulationOverlay.AddChild(WpfEmulationHapticLabel);
        WpfEmulationHapticLabel.Padding = new System.Windows.Thickness(0, IndentSize, 0, IndentSize);
        WpfEmulationHapticLabel.FontSize = FontSize;
        WpfEmulationHapticLabel.TextAlignment = System.Windows.TextAlignment.Center;
        WpfEmulationHapticLabel.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        WpfEmulationHapticLabel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
        WpfEmulationHapticLabel.Foreground = System.Windows.Media.Brushes.LightGray;
        WpfEmulationHapticLabel.Text = "HAPTICS";

        this.WpfEmulationRecognitionEdit = new WpfEdit(SearchControl: false, PasswordMask: false);
        WpfEmulationOverlay.AddChild(WpfEmulationRecognitionEdit);
        WpfEmulationRecognitionEdit.Padding = new System.Windows.Thickness(0, IndentSize, 0, IndentSize);
        WpfEmulationRecognitionEdit.AsTextBox().Background = System.Windows.Media.Brushes.WhiteSmoke;
        WpfEmulationRecognitionEdit.AsTextBox().BorderBrush = System.Windows.Media.Brushes.DarkGray;
        WpfEmulationRecognitionEdit.AsTextBox().BorderThickness = new System.Windows.Thickness(1);
        WpfEmulationRecognitionEdit.AsTextBox().FontSize = FontSize;
        WpfEmulationRecognitionEdit.AsTextBox().TextAlignment = System.Windows.TextAlignment.Center;
        WpfEmulationRecognitionEdit.AsTextBox().CaretBrush = System.Windows.Media.Brushes.Black;
        WpfEmulationRecognitionEdit.AsTextBox().IsTabStop = false;
        WpfEmulationRecognitionEdit.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        WpfEmulationRecognitionEdit.VerticalAlignment = System.Windows.VerticalAlignment.Top;
        WpfEmulationRecognitionEdit.Foreground = System.Windows.Media.Brushes.DarkGray;
        WpfEmulationRecognitionEdit.Text = "SPEECH";
        WpfEmulationRecognitionEdit.GotFocus += (Sender, Event) =>
        {
          WpfEmulationRecognitionEdit.Foreground = System.Windows.Media.Brushes.Black;
          WpfEmulationRecognitionEdit.Text = "";
        };
        WpfEmulationRecognitionEdit.LostFocus += (Sender, Event) =>
        {
          WpfEmulationRecognitionEdit.Foreground = System.Windows.Media.Brushes.DarkGray;
          WpfEmulationRecognitionEdit.Text = "SPEECH";
        };

        var WpfEmulationRecognitionTimer = new System.Windows.Threading.DispatcherTimer();
        WpfEmulationRecognitionTimer.Interval = TimeSpan.FromMilliseconds(500);
        WpfEmulationRecognitionTimer.Tick += (Sender, Event) =>
        {
          if (!string.IsNullOrWhiteSpace(WpfEmulationRecognitionEdit.Text))
          {
            var Transcription = new Inv.SpeechTranscription()
            {
              IsFinal = false,
              Text = WpfEmulationRecognitionEdit.Text
            };
            foreach (var SpeechRecognition in WpfEmulatedSpeechRecognitionList)
              SpeechRecognition.TranscriptionInvoke(Transcription);
          }
        };

        WpfEmulationRecognitionEdit.TextChanged += (Sender, Event) =>
        {
          WpfEmulationRecognitionTimer.Stop();
          WpfEmulationRecognitionTimer.Start();
        };
        WpfEmulationRecognitionEdit.KeyDown += (Sender, Event) =>
        {
          if (Event.Key == System.Windows.Input.Key.Enter)
          {
            StopEmulatedRecognition();

            //System.Windows.Input.Keyboard.ClearFocus();
            //Event.Handled = true;
          }
        };

        WpfEmulationOverlay.AddChild(WpfApplicationMaster);

        this.WpfEmulationDeviceGraphic = new WpfGraphic();
        WpfEmulationOverlay.AddChild(WpfEmulationDeviceGraphic);
        WpfEmulationDeviceGraphic.IsHitTestVisible = false;
        WpfEmulationDeviceGraphic.Background = null;
        WpfEmulationDeviceGraphic.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
        WpfEmulationDeviceGraphic.VerticalAlignment = System.Windows.VerticalAlignment.Center;

        WpfEmulationActionDock.Compose([], [WpfEmulationSuspendButton, WpfEmulationResumeButton, WpfEmulationHapticLabel, WpfEmulationRecognitionEdit], []);
      }

      var SizeChangedOnceOnly = false;

      var LeftEdgeSwipe = false;
      var RightEdgeSwipe = false;
      var OriginalSwipeY = 0.0;

      WpfApplicationMaster.MouseLeftButtonDown += (Sender, Event) =>
      {
        Event.Handled = true;
      };
      WpfApplicationMaster.PreviewMouseDown += (Sender, Event) =>
      {
        if (WpfFlyout == null)
        {
          var InvSurface = InvApplication.Window.ActiveSurface;
          if (InvSurface != null)
          {
            var MousePoint = Event.GetPosition(WpfApplicationMaster);

            LeftEdgeSwipe = InvSurface.HasGestureBackward && MousePoint.X <= 10;
            RightEdgeSwipe = InvSurface.HasGestureForward && MousePoint.X >= WpfApplicationMaster.ActualWidth - 10;
            OriginalSwipeY = MousePoint.Y;
          }

          if (Event.ButtonState == System.Windows.Input.MouseButtonState.Pressed)
          {
            if (Event.ChangedButton == System.Windows.Input.MouseButton.XButton1 && InvSurface != null && InvSurface.HasGestureBackward)
            {
              InvSurface.GestureBackwardInvoke();

              Event.Handled = true;
            }
            else if (Event.ChangedButton == System.Windows.Input.MouseButton.XButton2 && InvSurface != null && InvSurface.HasGestureForward)
            {
              InvSurface.GestureForwardInvoke();

              Event.Handled = true;
            }
          }
        }
      };
      WpfApplicationMaster.PreviewMouseUp += (Sender, Event) =>
      {
        LeftEdgeSwipe = false;
        RightEdgeSwipe = false;
      };
      WpfApplicationMaster.PreviewMouseMove += (Sender, Event) =>
      {
        if (LeftEdgeSwipe || RightEdgeSwipe)
        {
          var InvSurface = InvApplication.Window.ActiveSurface;

          if (InvSurface != null)
          {
            var MousePoint = Event.GetPosition(WpfApplicationMaster);

            var DragDistanceY = Math.Abs(MousePoint.Y - OriginalSwipeY);

            if (LeftEdgeSwipe && MousePoint.X >= 0 && DragDistanceY <= 20)
            {
              if (MousePoint.X >= 20)
              {
                InvSurface.GestureBackwardInvoke();
                LeftEdgeSwipe = false;

                Event.Handled = true;
              }
            }
            else if (RightEdgeSwipe && MousePoint.X <= WpfApplicationMaster.ActualWidth && DragDistanceY <= 20)
            {
              if (MousePoint.X <= WpfApplicationMaster.ActualWidth - 20)
              {
                InvSurface.GestureForwardInvoke();
                RightEdgeSwipe = false;

                Event.Handled = true;
              }
            }
            else
            {
              LeftEdgeSwipe = false;
              RightEdgeSwipe = false;
            }
          }
          else
          {
            LeftEdgeSwipe = false;
            RightEdgeSwipe = false;
          }
        }
      };
      WpfContainer.SizeChanged += (Sender, Event) =>
      {
        if (!SizeChangedOnceOnly)
        {
          SizeChangedOnceOnly = true;

          WpfContainer.Dispatcher.BeginInvoke((Action)delegate
          {
            SizeChangedOnceOnly = false;

            Rearrange();
          }, System.Windows.Threading.DispatcherPriority.Render);
        }
      };
      WpfContainer.PreviewKeyDown += (Sender, Event) =>
      {
        if (WpfFlyout == null)
        {
          var InvModifier = GetModifier();

          InvApplication.Keyboard.CheckModifier(InvModifier);

          if (!WpfOptions.PreventDeviceEmulation)
          {
            if (InvModifier.IsCtrl && (Event.Key == System.Windows.Input.Key.Left || Event.Key == System.Windows.Input.Key.Right))
            {
              WpfOptions.DeviceEmulationRotated = !WpfOptions.DeviceEmulationRotated;
              Rearrange();
            }
            else if (InvModifier.IsCtrl && (Event.Key == System.Windows.Input.Key.Add || Event.Key == System.Windows.Input.Key.OemPlus))
            {
              WpfOptions.DeviceEmulationFramed = true;
              Resize();
            }
            else if (InvModifier.IsCtrl && (Event.Key == System.Windows.Input.Key.Subtract || Event.Key == System.Windows.Input.Key.OemMinus))
            {
              WpfOptions.DeviceEmulationFramed = false;
              Resize();
            }
            else if (InvModifier.IsCtrl && (Event.Key == System.Windows.Input.Key.Up || Event.Key == System.Windows.Input.Key.Down))
            {
              var EmulationIndex = WpfOptions.DeviceEmulation != null ? WpfOptions.DeviceEmulationArray.FindIndex(E => E == WpfOptions.DeviceEmulation) : -1;

              if (Event.Key == System.Windows.Input.Key.Down)
                EmulationIndex++;
              else
                EmulationIndex--;

              if (EmulationIndex >= WpfOptions.DeviceEmulationArray.Length)
                EmulationIndex = 0;
              else if (EmulationIndex < 0)
                EmulationIndex = WpfOptions.DeviceEmulationArray.Length - 1;

              WpfOptions.DeviceEmulation = WpfOptions.DeviceEmulationArray[EmulationIndex];
              Rearrange();
            }
            else if (InvModifier.IsCtrl && (Event.Key == System.Windows.Input.Key.Clear || Event.Key == System.Windows.Input.Key.NumPad5))
            {
              WpfOptions.DeviceEmulation = null;
              Rearrange();
            }
            else if (InvModifier.IsCtrl && (Event.Key == System.Windows.Input.Key.F8))
            {
              var InvSurface = InvApplication.Window.ActiveSurface;
              if (InvSurface != null)
              {
                var SnapshotFilePath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), string.Format("{0}_inv-snapshot.txt", InvApplication.Title));

                var SnapshotFileText = InvModifier.IsShift ? Inv.Reproduction.ReproduceShell(InvSurface) : InvSurface.GetPanelDisplay();

                System.IO.File.WriteAllText(SnapshotFilePath, SnapshotFileText, System.Text.Encoding.UTF8);

                System.Diagnostics.Process.Start(SnapshotFilePath);
              }
            }
          }
        }
      };
      WpfContainer.KeyDown += (Sender, Event) =>
      {
        if (WpfFlyout == null && !InvApplication.Window.InputPrevented)
        {
          var Keystroke = GetKeystroke(Event);
          if (Keystroke != null)
            InvApplication.Keyboard.KeyPress(Keystroke);
        }
      };
      WpfContainer.PreviewKeyUp += (Sender, Event) =>
      {
        CheckModifier();

        if (WpfFlyout == null && !InvApplication.Window.InputPrevented)
        {
          var Keystroke = GetKeystroke(Event);
          if (Keystroke != null)
            InvApplication.Keyboard.KeyRelease(Keystroke);
        }
      };

      Microsoft.Win32.SystemEvents.DisplaySettingsChanged += (Sender, Event) => InvApplication.Platform.WindowPost(() => Rearrange());

      try
      {
        this.SoundPlayer = new WpfSoundPlayer();
      }
      catch (Exception Exception)
      {
        this.SoundException = Exception;
        this.SoundPlayer = null;
      }

      this.ElevationDictionary = [];
      ElevationDictionary.Add(1, new ElevationSpec(5, 1F, 270, Inv.Colour.FromHtmlString("#CCCCCC")));
      ElevationDictionary.Add(2, new ElevationSpec(8, 2.5F, 270, Inv.Colour.FromHtmlString("#BBBBBB")));
      ElevationDictionary.Add(3, new ElevationSpec(14, 4.5F, 270, Inv.Colour.FromHtmlString("#BBBBBB")));
      ElevationDictionary.Add(4, new ElevationSpec(25, 8F, 270, Inv.Colour.FromHtmlString("#BBBBBB")));
      ElevationDictionary.Add(5, new ElevationSpec(35, 13F, 270, Inv.Colour.FromHtmlString("#BBBBBB")));

      this.ControllerList = [];

      this.MirrorTransformArray = new Inv.EnumArray<Mirror, System.Windows.Media.ScaleTransform>()
      {
        { Mirror.Horizontal, new System.Windows.Media.ScaleTransform(-1, +1) },
        { Mirror.Vertical, new System.Windows.Media.ScaleTransform(+1, -1) },
        { Mirror.HorizontalAndVertical, new System.Windows.Media.ScaleTransform(-1, -1) },
      };
      foreach (var MirrorTransform in MirrorTransformArray.GetTuples())
        MirrorTransform.Value.Freeze();

      #region Key mapping.
      this.KeyDictionary = new Dictionary<System.Windows.Input.Key, Inv.Key>()
      {
        { System.Windows.Input.Key.D0, Inv.Key.n0 },
        { System.Windows.Input.Key.D1, Inv.Key.n1 },
        { System.Windows.Input.Key.D2, Inv.Key.n2 },
        { System.Windows.Input.Key.D3, Inv.Key.n3 },
        { System.Windows.Input.Key.D4, Inv.Key.n4 },
        { System.Windows.Input.Key.D5, Inv.Key.n5 },
        { System.Windows.Input.Key.D6, Inv.Key.n6 },
        { System.Windows.Input.Key.D7, Inv.Key.n7 },
        { System.Windows.Input.Key.D8, Inv.Key.n8 },
        { System.Windows.Input.Key.D9, Inv.Key.n9 },
        { System.Windows.Input.Key.Decimal, Inv.Key.Period },
        { System.Windows.Input.Key.NumPad0, Inv.Key.n0 },
        { System.Windows.Input.Key.NumPad1, Inv.Key.n1 },
        { System.Windows.Input.Key.NumPad2, Inv.Key.n2 },
        { System.Windows.Input.Key.NumPad3, Inv.Key.n3 },
        { System.Windows.Input.Key.NumPad4, Inv.Key.n4 },
        { System.Windows.Input.Key.NumPad5, Inv.Key.n5 },
        { System.Windows.Input.Key.NumPad6, Inv.Key.n6 },
        { System.Windows.Input.Key.NumPad7, Inv.Key.n7 },
        { System.Windows.Input.Key.NumPad8, Inv.Key.n8 },
        { System.Windows.Input.Key.NumPad9, Inv.Key.n9 },
        { System.Windows.Input.Key.A, Inv.Key.A },
        { System.Windows.Input.Key.B, Inv.Key.B },
        { System.Windows.Input.Key.C, Inv.Key.C },
        { System.Windows.Input.Key.D, Inv.Key.D },
        { System.Windows.Input.Key.E, Inv.Key.E },
        { System.Windows.Input.Key.F, Inv.Key.F },
        { System.Windows.Input.Key.G, Inv.Key.G },
        { System.Windows.Input.Key.H, Inv.Key.H },
        { System.Windows.Input.Key.I, Inv.Key.I },
        { System.Windows.Input.Key.J, Inv.Key.J },
        { System.Windows.Input.Key.K, Inv.Key.K },
        { System.Windows.Input.Key.L, Inv.Key.L },
        { System.Windows.Input.Key.M, Inv.Key.M },
        { System.Windows.Input.Key.N, Inv.Key.N },
        { System.Windows.Input.Key.O, Inv.Key.O },
        { System.Windows.Input.Key.P, Inv.Key.P },
        { System.Windows.Input.Key.Q, Inv.Key.Q },
        { System.Windows.Input.Key.R, Inv.Key.R },
        { System.Windows.Input.Key.S, Inv.Key.S },
        { System.Windows.Input.Key.T, Inv.Key.T },
        { System.Windows.Input.Key.U, Inv.Key.U },
        { System.Windows.Input.Key.V, Inv.Key.V },
        { System.Windows.Input.Key.W, Inv.Key.W },
        { System.Windows.Input.Key.X, Inv.Key.X },
        { System.Windows.Input.Key.Y, Inv.Key.Y },
        { System.Windows.Input.Key.Z, Inv.Key.Z },
        { System.Windows.Input.Key.F1, Inv.Key.F1 },
        { System.Windows.Input.Key.F2, Inv.Key.F2 },
        { System.Windows.Input.Key.F3, Inv.Key.F3 },
        { System.Windows.Input.Key.F4, Inv.Key.F4 },
        { System.Windows.Input.Key.F5, Inv.Key.F5 },
        { System.Windows.Input.Key.F6, Inv.Key.F6 },
        { System.Windows.Input.Key.F7, Inv.Key.F7 },
        { System.Windows.Input.Key.F8, Inv.Key.F8 },
        { System.Windows.Input.Key.F9, Inv.Key.F9 },
        { System.Windows.Input.Key.F10, Inv.Key.F10 },
        { System.Windows.Input.Key.F11, Inv.Key.F11 },
        { System.Windows.Input.Key.F12, Inv.Key.F12 },
        { System.Windows.Input.Key.Escape, Inv.Key.Escape },
        { System.Windows.Input.Key.Enter, Inv.Key.Enter },
        { System.Windows.Input.Key.Tab, Inv.Key.Tab },
        { System.Windows.Input.Key.Space, Inv.Key.Space },
        { System.Windows.Input.Key.OemPeriod, Inv.Key.Period },
        { System.Windows.Input.Key.OemComma, Inv.Key.Comma },
        { System.Windows.Input.Key.OemSemicolon, Inv.Key.Colon },
        { System.Windows.Input.Key.OemTilde, Inv.Key.Grave },
        { System.Windows.Input.Key.OemQuotes, Inv.Key.Quote },
        { System.Windows.Input.Key.OemPlus, Inv.Key.Plus },
        { System.Windows.Input.Key.OemMinus, Inv.Key.Minus },
        { System.Windows.Input.Key.Up, Inv.Key.Up },
        { System.Windows.Input.Key.Down, Inv.Key.Down },
        { System.Windows.Input.Key.Left, Inv.Key.Left },
        { System.Windows.Input.Key.Right, Inv.Key.Right },
        { System.Windows.Input.Key.Home, Inv.Key.Home },
        { System.Windows.Input.Key.End, Inv.Key.End },
        { System.Windows.Input.Key.PageUp, Inv.Key.PageUp },
        { System.Windows.Input.Key.PageDown, Inv.Key.PageDown },
        { System.Windows.Input.Key.Clear, Inv.Key.Clear },
        { System.Windows.Input.Key.Insert, Inv.Key.Insert },
        { System.Windows.Input.Key.Delete, Inv.Key.Delete },
        { System.Windows.Input.Key.Oem2, Inv.Key.Slash },
        { System.Windows.Input.Key.Oem5, Inv.Key.Backslash },
        { System.Windows.Input.Key.Add, Inv.Key.Plus },
        { System.Windows.Input.Key.Subtract, Inv.Key.Minus },
        { System.Windows.Input.Key.Divide, Inv.Key.Slash },
        { System.Windows.Input.Key.Multiply, Inv.Key.Asterisk },
        { System.Windows.Input.Key.OemBackslash, Inv.Key.Backslash },
        { System.Windows.Input.Key.Back, Inv.Key.Backspace },
        { System.Windows.Input.Key.LeftShift, Inv.Key.LeftShift },
        { System.Windows.Input.Key.RightShift, Inv.Key.RightShift },
        { System.Windows.Input.Key.LeftAlt, Inv.Key.LeftAlt },
        { System.Windows.Input.Key.RightAlt, Inv.Key.RightAlt },
        { System.Windows.Input.Key.LeftCtrl, Inv.Key.LeftCtrl },
        { System.Windows.Input.Key.RightCtrl, Inv.Key.RightCtrl },
        { System.Windows.Input.Key.OemOpenBrackets, Inv.Key.OpenBracket },
        { System.Windows.Input.Key.OemCloseBrackets, Inv.Key.CloseBracket }
      };
#if DEBUG
      var MissingKeySet = Inv.Support.EnumHelper.GetEnumerable<Inv.Key>().Except(KeyDictionary.Values).ToHashSet();
      if (MissingKeySet.Count > 0)
        System.Diagnostics.Debug.WriteLine("Unhandled keys: " + MissingKeySet.Select(K => K.ToString()).AsSeparatedText(", "));
#endif
      #endregion

      this.RouteArray = new Inv.EnumArray<ControlType, Func<Inv.Control, System.Windows.FrameworkElement>>()
      {
        { Inv.ControlType.Block, P => TranslateBlock((Inv.Block)P) },
        { Inv.ControlType.Board, P => TranslateBoard((Inv.Board)P) },
        { Inv.ControlType.Browser, P => TranslateBrowser((Inv.Browser)P) },
        { Inv.ControlType.Button, P => TranslateButton((Inv.Button)P) },
        { Inv.ControlType.Dock, P => TranslateDock((Inv.Dock)P) },
        { Inv.ControlType.Edit, P => TranslateEdit((Inv.Edit)P) },
        { Inv.ControlType.Flow, P => TranslateFlow((Inv.Flow)P) },
        { Inv.ControlType.Frame, P => TranslateFrame((Inv.Frame)P) },
        { Inv.ControlType.Graphic, P => TranslateGraphic((Inv.Graphic)P) },
        { Inv.ControlType.Label, P => TranslateLabel((Inv.Label)P) },
        { Inv.ControlType.Memo, P => TranslateMemo((Inv.Memo)P) },
        { Inv.ControlType.Native, P => TranslateNative((Inv.Native)P) },
        { Inv.ControlType.Overlay, P => TranslateOverlay((Inv.Overlay)P) },
        { Inv.ControlType.Canvas, P => TranslateCanvas((Inv.Canvas)P) },
        { Inv.ControlType.Scroll, P => TranslateScroll((Inv.Scroll)P) },
        { Inv.ControlType.Shape, P => TranslateShape((Inv.Shape)P) },
        { Inv.ControlType.Stack, P => TranslateStack((Inv.Stack)P) },
        { Inv.ControlType.Switch, P => TranslateSwitch((Inv.Switch)P) },
        { Inv.ControlType.Table, P => TranslateTable((Inv.Table)P) },
        { Inv.ControlType.Video, P => TranslateVideo((Inv.Video)P) },
        { Inv.ControlType.Wrap, P => TranslateWrap((Inv.Wrap)P) },
      };
    }

    internal event Action ShutdownEvent;
    internal bool HasSound => SoundException == null;
    internal bool IsTransitioning => TransitionCount > 0;
    internal bool IsInputBlocked => IsTransitioning || (InvApplication?.Window.InputPrevented ?? false);
    internal Inv.Application InvApplication { get; private set; }
    internal Inv.EnumArray<Mirror, System.Windows.Media.ScaleTransform> MirrorTransformArray { get; private set; }
    internal double ScaleFactorX { get; private set; }
    internal double ScaleFactorY { get; private set; }
    internal WpfOptions WpfOptions { get; }
    internal bool IsActive { get; private set; }

    internal string SelectAssetFolderPath()
    {
      return WpfOptions.OverrideAssetFolderPath ?? System.IO.Path.Combine(WpfOptions.MainFolderPath, "Assets");
    }
    internal string SelectAssetPath(Asset Asset)
    {
      return System.IO.Path.Combine(SelectAssetFolderPath(), Asset.Name);
    }
    internal string SelectFilePath(File File)
    {
      return System.IO.Path.Combine(SelectFolderPath(File.Folder), File.Name);
    }
    internal string SelectFolderPath(Folder Folder)
    {
      var Result = System.IO.Path.Combine(WpfOptions.MainFolderPath, Folder.GetRelativePath().AsSeparatedText(System.IO.Path.DirectorySeparatorChar.ToString()));

      System.IO.Directory.CreateDirectory(Result);

      return Result;
    }
    internal KeyModifier GetModifier()
    {
      return new KeyModifier()
      {
        IsLeftShift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift),
        IsRightShift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift),
        IsLeftAlt = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftAlt),
        IsRightAlt = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightAlt),
        IsLeftCtrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl),
        IsRightCtrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl)
      };
    }
    internal void Install(Inv.Application InvApplication)
    {
      this.InvApplication = InvApplication;
      InvApplication.SetPlatform(new WpfPlatform(this));

      try
      {
        InvApplication.Version = Inv.Support.AssemblyHelper.GetVersion(WpfOptions.MainAssembly);
      }
      catch (Exception Exception)
      {
        Debug.WriteLine(Exception.Message);

        InvApplication.Version = "?";
      }

      InvApplication.Directory.Installation = WpfOptions.MainFolderPath;

      InvApplication.Device.Target = DeviceTarget.WindowsDesktop;
      InvApplication.Device.Theme = GetOperatingSystemTheme();
      InvApplication.Device.Name = Inv.Support.EnvironmentHelper.ResolveMachineName();
      InvApplication.Device.Model = "";
      InvApplication.Device.Manufacturer = "";
      InvApplication.Process.Id = System.Diagnostics.Process.GetCurrentProcess().Id;

      try
      {
        using (var mc = new System.Management.ManagementClass("Win32_ComputerSystem"))
        {
          foreach (var mo in mc.GetInstances())
          {
            InvApplication.Device.Manufacturer += mo["Manufacturer"].ToString();
            InvApplication.Device.Model += mo["Model"].ToString();
          }
        }
      }
      catch
      {
        // ignore this failure.
        InvApplication.Device.Model = "Unknown";
        InvApplication.Device.Manufacturer = "Unknown";
      }

      InvApplication.Device.System = Inv.OperatingSystem.Identity;
      InvApplication.Device.Keyboard = true; // TODO: System.Windows.Input.Keyboard;
      InvApplication.Device.Mouse = true; // TODO: System.Windows.Input.Mouse;
      InvApplication.Device.Touch = Inv.OperatingSystem.HasTouchInput();
      InvApplication.Device.ProportionalFontName = WpfOptions.DefaultFontName;
      InvApplication.Device.MonospacedFontName = "Consolas";

      const int MinNumberOfControllers = 0;
      const int MaxNumberOfControllers = 4;

      Debug.Assert(WpfOptions.MaximumNumberOfControllers >= MinNumberOfControllers, "Supported controller count cannot be negative");
      Debug.Assert(WpfOptions.MaximumNumberOfControllers <= MaxNumberOfControllers, "Supported controller count cannot be greater than 4");

      if (WpfOptions.MaximumNumberOfControllers != 0)
      {
        var ControllerCount = Math.Min(Math.Max(WpfOptions.MaximumNumberOfControllers, MinNumberOfControllers), MaxNumberOfControllers);

        foreach (var UserIndex in EnumHelper.GetEnumerable<SharpDX.XInput.UserIndex>().Except(SharpDX.XInput.UserIndex.Any).Take(WpfOptions.MaximumNumberOfControllers))
          ControllerList.Add(new Controller(InvApplication.Device, UserIndex));
      }

      InvApplication.Haptics.IsSupported = false; // TODO: which Windows devices support vibrations?

      InvApplication.Window.NativePanelType = typeof(System.Windows.FrameworkElement);

      InvApplication.Graphics.DrawImageDelegate = GraphicsDrawImage;
      InvApplication.Graphics.CropImageDelegate = GraphicsCropImage;
      InvApplication.Graphics.CanvasDrawDelegate = (Canvas, DrawAction) => GraphicsCanvasDraw(TranslateCanvas(Canvas), DrawAction);

      Resize();

      if (SoundException != null && !WpfOptions.SuppressAudioDriverWarnings)
      {
        System.Windows.MessageBox.Show(
          "There was a problem initialising the sound engine which may be caused by missing assemblies or issues with your sound card driver. Please check the installation and sound card driver." + Environment.NewLine + Environment.NewLine +
          "'" + SoundException.Describe() + "'" + Environment.NewLine + Environment.NewLine +
          "This application will run with all sound effects disabled.");
      }
    }
    internal void Start()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        try
        {
          InvApplication.StartInvoke();
        }
        catch (Exception Exception)
        {
          Inv.SystemExceptionHandler.Route(Exception);

          // NOTE: can't let the application start if the StartEvent failed so have the application crash instead.
          throw Exception.Preserve();
        }

#if DEBUG
        //if (InvApplication.Location.IsRequired) InvApplication.Location.ChangeInvoke(new Inv.Coordinate(-31.953, 115.814, 0.0)); // NOTE: for testing location management only.
#endif

        // ensure first process before the rendering loop kicks on.
        Guard(() => Process());

        System.Windows.Media.CompositionTarget.Rendering += Rendering;
      }
    }
    internal void Stop()
    {
      if (IsActive)
      {
        this.IsActive = false;

        System.Windows.Media.CompositionTarget.Rendering -= Rendering;

        Guard(() =>
        {
          InvApplication.StopInvoke();
        });
      }
    }
    internal void Rearrange()
    {
      var LastLogicalScreenWidthPoints = InvApplication.Window.Width;
      var LastLogicalScreenHeightPoints = InvApplication.Window.Height;

      Resize();

      if (LastLogicalScreenWidthPoints != InvApplication.Window.Width || LastLogicalScreenHeightPoints != InvApplication.Window.Height)
      {
        var InvSurface = InvApplication.Window.ActiveSurface;
        if (InvSurface != null)
          ArrangeSurface(InvSurface, TranslateSurface(InvSurface));
      }
    }
    internal void ShowCalendarPicker(CalendarPicker CalendarPicker)
    {
      this.WpfFlyout = new WpfOverlay();
      WpfApplicationMaster.AddChild(WpfFlyout);

      void HideFlyout()
      {
        if (WpfFlyout != null)
        {
          WpfApplicationMaster.RemoveChild(WpfFlyout);
          this.WpfFlyout = null;
        }
      }

      var WpfShadeBrush = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Black) { Opacity = 0.50 };
      WpfShadeBrush.Freeze();

      var WpfShade = new WpfButton();
      WpfFlyout.AddChild(WpfShade);
      WpfShade.Background = WpfShadeBrush;
      WpfShade.LeftClick += () =>
      {
        CalendarPicker.CancelInvoke();
        HideFlyout();
      };
      WpfShade.RightClick += () =>
      {
        CalendarPicker.CancelInvoke();
        HideFlyout();
      };

      var WpfFrame = new System.Windows.Controls.Border();
      WpfFlyout.AddChild(WpfFrame);
      WpfFrame.Background = System.Windows.Media.Brushes.White;
      WpfFrame.Padding = new System.Windows.Thickness(10);
      WpfFrame.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
      WpfFrame.VerticalAlignment = System.Windows.VerticalAlignment.Center;

      var WpfStack = new System.Windows.Controls.StackPanel();
      WpfFrame.Child = WpfStack;
      WpfStack.Orientation = System.Windows.Controls.Orientation.Vertical;

      var TitleLabel = new WpfLabel();
      WpfStack.Children.Add(TitleLabel);
      TitleLabel.Margin = new System.Windows.Thickness(10);
      TitleLabel.Foreground = System.Windows.Media.Brushes.Black;
      TitleLabel.FontSize = 30;
      TitleLabel.TextAlignment = System.Windows.TextAlignment.Center;
      TitleLabel.Text = "Select" + (CalendarPicker.SetDate ? " Date" : "") + (CalendarPicker.SetTime ? " Time" : "");

      var AcceptButton = new WpfButton();

      var SelectedValue = CalendarPicker.Value.Date + CalendarPicker.Value.TimeOfDay.TruncateSeconds();

      if (CalendarPicker.SetDate)
      {
        var WpfCalendar = new WpfCalendar(this);
        WpfStack.Children.Add(WpfCalendar);
        WpfCalendar.IsHitTestVisible = true;
        WpfCalendar.SelectedDate = SelectedValue;
        WpfCalendar.DisplayDate = SelectedValue;
        WpfCalendar.IsTodayHighlighted = true;
        WpfCalendar.IsDark = Inv.Theme.IsDark;
        WpfCalendar.SelectedDatesChanged += (Sender, Event) =>
        {
          if (WpfCalendar.SelectedDate != null)
            SelectedValue = WpfCalendar.SelectedDate.Value.Date + SelectedValue.TimeOfDay.TruncateSeconds();
        };
        WpfCalendar.Focus();
      }

      if (CalendarPicker.SetTime)
      {
        var WpfTimeEdit = new WpfEdit(false, false);
        WpfStack.Children.Add(WpfTimeEdit);
        WpfTimeEdit.BorderBrush = System.Windows.Media.Brushes.LightGray;
        WpfTimeEdit.BorderThickness = new System.Windows.Thickness(1);
        WpfTimeEdit.Background = System.Windows.Media.Brushes.WhiteSmoke;
        WpfTimeEdit.Padding = new System.Windows.Thickness(10);
        WpfTimeEdit.AsTextBox().FontSize = 20;
        WpfTimeEdit.AsTextBox().TextAlignment = System.Windows.TextAlignment.Center;
        WpfTimeEdit.AsTextBox().Foreground = System.Windows.Media.Brushes.Black;
        WpfTimeEdit.IsReadOnly = false;
        WpfTimeEdit.Text = CalendarPicker.Value.ToString("HH:mm");
        WpfTimeEdit.TextChanged += (Sender, Event) =>
        {
          if (DateTime.TryParseExact(WpfTimeEdit.Text, "H:mm", null, System.Globalization.DateTimeStyles.AssumeLocal, out DateTime Time))
          {
            SelectedValue = SelectedValue.Date + Time.TimeOfDay.TruncateSeconds();

            WpfTimeEdit.Foreground = System.Windows.Media.Brushes.Black;
            AcceptButton.IsEnabled = true;
          }
          else
          {
            WpfTimeEdit.Foreground = System.Windows.Media.Brushes.Red;
            AcceptButton.IsEnabled = false;
          }
        };

        if (!CalendarPicker.SetDate)
          WpfTimeEdit.Focus();
      }

      var ButtonDock = new WpfDock();
      WpfStack.Children.Add(ButtonDock);
      ButtonDock.Margin = new System.Windows.Thickness(0, 10, 0, 0);

      AcceptButton.Focusable = true;
      AcceptButton.Margin = new System.Windows.Thickness(0, 0, 5, 0);
      AcceptButton.AsFlat(System.Windows.Media.Brushes.Green, System.Windows.Media.Brushes.ForestGreen, System.Windows.Media.Brushes.DarkGreen);
      AcceptButton.Padding = new System.Windows.Thickness(10);
      AcceptButton.LeftClick += () =>
      {
        HideFlyout();
        CalendarPicker.Value = SelectedValue;
        CalendarPicker.SelectInvoke();
      };

      var AcceptLabel = new WpfLabel();
      AcceptButton.Content = AcceptLabel;
      AcceptLabel.Foreground = System.Windows.Media.Brushes.White;
      AcceptLabel.FontSize = 20;
      AcceptLabel.TextAlignment = System.Windows.TextAlignment.Center;
      AcceptLabel.Text = "ACCEPT";

      var CancelButton = new WpfButton();
      CancelButton.Focusable = true;
      CancelButton.Margin = new System.Windows.Thickness(5, 0, 0, 0);
      CancelButton.AsFlat(System.Windows.Media.Brushes.LightGray, System.Windows.Media.Brushes.WhiteSmoke, System.Windows.Media.Brushes.DarkGray);
      CancelButton.Padding = new System.Windows.Thickness(10);
      CancelButton.LeftClick += () =>
      {
        CalendarPicker.CancelInvoke();
        HideFlyout();
      };

      var CancelLabel = new WpfLabel();
      CancelButton.Content = CancelLabel;
      CancelLabel.Foreground = System.Windows.Media.Brushes.Black;
      CancelLabel.FontSize = 20;
      CancelLabel.TextAlignment = System.Windows.TextAlignment.Center;
      CancelLabel.Text = "CANCEL";

      ButtonDock.IsHorizontal = true;
      ButtonDock.Compose([], [AcceptButton, CancelButton], []);

      WpfFlyout.PreviewKeyDown += (Sender, Event) =>
      {
        if (Event.Key == System.Windows.Input.Key.Escape)
        {
          CalendarPicker.CancelInvoke();
          HideFlyout();
          Event.Handled = true;
        }
        else if (Event.Key == System.Windows.Input.Key.Return && AcceptButton.IsEnabled)
        {
          HideFlyout();
          CalendarPicker.Value = SelectedValue;
          CalendarPicker.SelectInvoke();
        }
      };
    }
    internal void Post(Action Action)
    {
      WpfContainer.Dispatcher.BeginInvoke(Action);
    }
    internal void Call(Action Action)
    {
      if (System.Windows.Threading.Dispatcher.CurrentDispatcher == WpfContainer.Dispatcher)
        Action();
      else
        WpfContainer.Dispatcher.Invoke(Action);
    }
    internal System.Windows.Media.PenLineJoin TranslateLineJoin(Inv.LineJoin InvLineJoin)
    {
      return InvLineJoin switch
      {
        Inv.LineJoin.Miter => System.Windows.Media.PenLineJoin.Miter,
        Inv.LineJoin.Bevel => System.Windows.Media.PenLineJoin.Bevel,
        Inv.LineJoin.Round => System.Windows.Media.PenLineJoin.Round,
        _ => throw new NotSupportedException("LineJoin not handled: " + InvLineJoin.ToString()),
      };
    }
    internal System.Windows.Media.Stretch TranslateFit(Inv.FitMethod InvFitMethod)
    {
      return InvFitMethod switch
      {
        Inv.FitMethod.Original => System.Windows.Media.Stretch.None,
        Inv.FitMethod.Contain => System.Windows.Media.Stretch.Uniform,
        Inv.FitMethod.Stretch => System.Windows.Media.Stretch.Fill,
        Inv.FitMethod.Cover => System.Windows.Media.Stretch.UniformToFill,
        _ => throw new Exception("FitMethod not handled: " + InvFitMethod),
      };
    }
    internal System.Windows.Media.PenLineCap TranslateLineCap(Inv.LineCap InvLineCap)
    {
      switch (InvLineCap)
      {
        case Inv.LineCap.Butt:
          return System.Windows.Media.PenLineCap.Flat;

        case Inv.LineCap.Round:
          return System.Windows.Media.PenLineCap.Round;

        case Inv.LineCap.Square:
          return System.Windows.Media.PenLineCap.Square;

        default:
          throw new NotSupportedException("LineCap not handled: " + InvLineCap);
      }
    }
    internal System.Windows.Media.Brush TranslateBrush(Inv.Colour InvColour)
    {
      if (InvColour == null)
      {
        return null;
      }
      else if (InvColour.Node == null)
      {
        var Result = new System.Windows.Media.SolidColorBrush(TranslateColour(InvColour));
        Result.Freeze();

        InvColour.Node = Result;

        return Result;
      }
      else
      {
        return (System.Windows.Media.SolidColorBrush)InvColour.Node;
      }
    }
    internal void TranslateRect(Inv.Rect InvRect, ref System.Windows.Rect WpfRect)
    {
      WpfRect.X = InvRect.Left;
      WpfRect.Y = InvRect.Top;
      WpfRect.Width = InvRect.Width;
      WpfRect.Height = InvRect.Height;
    }
    internal System.Windows.Point TranslatePoint(Inv.Point InvPoint)
    {
      return new System.Windows.Point(InvPoint.X, InvPoint.Y);
    }
    internal System.Windows.FontWeight TranslateFontWeight(Inv.FontWeight InvFontWeight)
    {
      return InvFontWeight switch
      {
        Inv.FontWeight.Thin => System.Windows.FontWeights.Thin,
        Inv.FontWeight.Light => System.Windows.FontWeights.Light,
        Inv.FontWeight.Regular => System.Windows.FontWeights.Regular,
        Inv.FontWeight.Medium => System.Windows.FontWeights.Medium,
        Inv.FontWeight.Bold => System.Windows.FontWeights.Bold,
        Inv.FontWeight.Heavy => System.Windows.FontWeights.Black,
        _ => throw Inv.Support.EnumHelper.UnexpectedValueException(InvFontWeight),
      };
    }

    internal System.Windows.FontVariants TranslateFontAxisVariants(Inv.FontAxis InvFontAxis)
    {
      return InvFontAxis switch
      {
        Inv.FontAxis.Baseline => System.Windows.FontVariants.Normal,
        Inv.FontAxis.Superscript => System.Windows.FontVariants.Superscript,
        Inv.FontAxis.Topline => System.Windows.FontVariants.Superscript,
        Inv.FontAxis.Subscript => System.Windows.FontVariants.Subscript,
        Inv.FontAxis.Bottomline => System.Windows.FontVariants.Subscript,
        _ => throw Inv.Support.EnumHelper.UnexpectedValueException(InvFontAxis),
      };
    }
    internal System.Windows.BaselineAlignment TranslateFontAxisAlignment(Inv.FontAxis InvFontAxis)
    {
      return InvFontAxis switch
      {
        Inv.FontAxis.Baseline => System.Windows.BaselineAlignment.Baseline,
        Inv.FontAxis.Superscript => System.Windows.BaselineAlignment.Superscript,
        Inv.FontAxis.Subscript => System.Windows.BaselineAlignment.Subscript,
        Inv.FontAxis.Topline => System.Windows.BaselineAlignment.Top,
        Inv.FontAxis.Bottomline => System.Windows.BaselineAlignment.Bottom,
        _ => throw Inv.Support.EnumHelper.UnexpectedValueException(InvFontAxis),
      };
    }
    internal WpfImage TranslateImage(Inv.Image Image)
    {
      if (Image == null)
      {
        return null;
      }
      else
      {
        var Result = Image.Node as WpfImage;

        if (Result == null)
        {
          using (var MemoryStream = new MemoryStream(Image.GetBuffer()))
            Result = WpfImage.Decode(MemoryStream);

          Debug.Assert(Image.Node == null, "Detected multi-threaded translation of images which can lead to inefficiency.");

          Image.Node = Result;
        }

        return Result;
      }
    }

    internal void Guard(Action Action)
    {
      try
      {
        Action();
      }
      catch (Exception Exception)
      {
        Inv.SystemExceptionHandler.Route(Exception);
      }
    }
    internal void CheckModifier()
    {
      InvApplication.Keyboard.CheckModifier(InvApplication.Window.IsActivated ? GetModifier() : new KeyModifier());
    }
    internal void StopClip(Inv.AudioClip Clip)
    {
      var WpfSound = Clip.Node as WpfSound;
      Clip.Node = null;

      if (WpfSound != null)
        WpfSound.Stop();
    }
    internal void PauseClip(Inv.AudioClip Clip)
    {
      var WpfSound = Clip.Node as WpfSound;
      if (WpfSound != null)
        WpfSound.Pause();
    }
    internal void ResumeClip(Inv.AudioClip Clip)
    {
      var WpfSound = Clip.Node as WpfSound;
      if (WpfSound != null)
        WpfSound.Resume();
    }
    internal void ModulateClip(Inv.AudioClip Clip)
    {
      var WpfSound = Clip.Node as WpfSound;
      if (WpfSound != null)
        WpfSound.Modulate(Clip);
    }
    internal WpfSound PlaySound(Inv.Sound Sound, float Volume, float Rate, float Pan, bool Loop)
    {
      if (HasSound)
        return SoundPlayer.Play(Sound, Volume, Rate, Pan, Loop);
      else
        return null;
    }
    internal TimeSpan GetSoundLength(Inv.Sound Sound)
    {
      if (HasSound)
        return SoundPlayer.GetSoundSource(Sound).GetLength();
      else
        return TimeSpan.Zero;
    }
    internal System.Windows.FrameworkElement GetPanel(Inv.Panel Panel)
    {
      return RoutePanel(Panel);
    }
    internal WpfSurface TranslateSurface(Inv.Surface InvSurface)
    {
      if (InvSurface.Node == null)
      {
        var Result = new WpfSurface();
        Result.Name = "Surface" + SurfaceCount++;
        WpfSurfaceMaster.RegisterName(Result.Name, Result);

        InvSurface.Node = Result;

        return Result;
      }
      else
      {
        return (WpfSurface)InvSurface.Node;
      }
    }
    internal WpfBlock TranslateBlock(Inv.Block InvBlock)
    {
      return TranslatePanel(InvBlock, () => new WpfBlock(), WpfBlock =>
      {
        var WpfTextBlock = WpfBlock.AsTextBlock();

        TranslateLayout(InvBlock, WpfBlock);
        TranslateFont(InvBlock.Font, WpfTextBlock);
        TranslateTooltip(InvBlock.Tooltip, WpfBlock);

        WpfBlock.TextWrapping = InvBlock.LineWrapping ? System.Windows.TextWrapping.Wrap : System.Windows.TextWrapping.NoWrap;
        WpfBlock.TextAlignment = TranslateTextAlignment(InvBlock.Justify);

        if (InvBlock.SpanCollection.Render())
        {
          // TODO: do we have to always recreate the entire textblock?
          WpfTextBlock.Inlines.Clear();

          foreach (var InvSpan in InvBlock.SpanCollection)
          {
            System.Windows.Documents.Inline WpfInline;

            switch (InvSpan.Style)
            {
              case BlockStyle.Run:
                WpfInline = new System.Windows.Documents.Run() { Text = InvSpan.Text };
                break;

              case BlockStyle.Break:
                WpfInline = new System.Windows.Documents.LineBreak();
                break;

              default:
                throw new Exception("Inv.BlockStyle not handled: " + InvSpan.Style);
            }

            InvSpan.Node = WpfInline;

            InvSpan.Background.Render();
            WpfInline.Background = TranslateBrush(InvSpan.Background.Colour);

            TranslateFont(InvSpan.Font, WpfInline);

            WpfTextBlock.Inlines.Add(WpfInline);
          }
        }

        Inv.WpfFoundation.CheckImplicitTooltip(WpfTextBlock);
      });
    }
    internal WpfBoard TranslateBoard(Inv.Board InvBoard)
    {
      return TranslatePanel(InvBoard, () => new WpfBoard(), WpfBoard =>
      {
        TranslateLayout(InvBoard, WpfBoard);

        if (InvBoard.PinCollection.Render())
        {
          foreach (var WpfElement in WpfBoard.GetPins())
            WpfElement.Child = null;

          WpfBoard.RemovePins();
          foreach (var InvPin in InvBoard.PinCollection)
          {
            var WpfElement = WpfBoard.AddPin();
            System.Windows.Controls.Canvas.SetLeft(WpfElement, InvPin.Rect.Left);
            System.Windows.Controls.Canvas.SetTop(WpfElement, InvPin.Rect.Top);
            WpfElement.Width = InvPin.Rect.Width;
            WpfElement.Height = InvPin.Rect.Height;

            var WpfPanel = RoutePanel(InvPin.Panel);
            WpfElement.Child = WpfPanel;
          }
        }
      });
    }
    internal WpfBrowser TranslateBrowser(Inv.Browser InvBrowser)
    {
      return TranslatePanel(InvBrowser, () =>
      {
        var Result = new WpfBrowser();
        Result.FaultEvent += (Exception) => Inv.SystemExceptionHandler.Route(Exception);
        Result.MouseBackEvent += () => InvBrowser.Surface?.GestureBackwardInvoke();
        Result.MouseForwardEvent += () => InvBrowser.Surface?.GestureForwardInvoke();
        Result.BlockQuery += (Uri) =>
        {
          var Fetch = new Inv.BrowserFetch(Uri);
          InvBrowser.FetchInvoke(Fetch);
          return Fetch.IsCancelled;
        };
        Result.ReadyEvent += (Uri) => InvBrowser.ReadyInvoke(new Inv.BrowserReady(Uri));
        return Result;
      }, WpfBrowser =>
      {
        TranslateLayout(InvBrowser, WpfBrowser);

        var Navigate = InvBrowser.UriSingleton.Render();
        if (InvBrowser.HtmlSingleton.Render())
          Navigate = true;

        if (Navigate)
          WpfBrowser.Navigate(InvBrowser.UriSingleton.Data, InvBrowser.HtmlSingleton.Data);
      });
    }
    internal WpfButton TranslateButton(Inv.Button InvButton)
    {
      var IsFlat = InvButton.Style == ButtonStyle.Flat;

      return TranslatePanel(InvButton, () =>
      {
        var Result = new WpfButton();

        Result.GotFocus += (Sender, Event) =>
        {
          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Enter(CallerMemberName: nameof(Result.GotFocus));

          InvButton.Focus.GotInvoke();

          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Leave(CallerMemberName: nameof(Result.GotFocus));
        };
        Result.LostFocus += (Sender, Event) =>
        {
          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Enter(CallerMemberName: nameof(Result.LostFocus));

          InvButton.Focus.LostInvoke();

          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Leave(CallerMemberName: nameof(Result.LostFocus));
        };
        Result.MouseEnter += (Sender, Event) =>
        {
          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Enter(CallerMemberName: nameof(Result.MouseEnter));

          InvButton.OverInvoke();

          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Leave(CallerMemberName: nameof(Result.MouseEnter));
        };
        Result.MouseLeave += (Sender, Event) =>
        {
          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Enter(CallerMemberName: nameof(Result.MouseLeave));

          InvButton.AwayInvoke();

          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Leave(CallerMemberName: nameof(Result.MouseLeave));
        };
        Result.PressClick += () =>
        {
          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Enter(CallerMemberName: nameof(Result.PressClick));

          if (!IsInputBlocked)
            InvButton.PressInvoke();

          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Leave(CallerMemberName: nameof(Result.PressClick));
        };
        Result.ReleaseClick += () =>
        {
          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Enter(CallerMemberName: nameof(Result.ReleaseClick));

          if (!IsInputBlocked)
            InvButton.ReleaseInvoke();

          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Leave(CallerMemberName: nameof(Result.ReleaseClick));
        };
        Result.LeftClick += () =>
        {
          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Enter(CallerMemberName: nameof(Result.LeftClick));

          if (!IsInputBlocked)
          {
            if (Result.IsEnabled) // NOTE: it should not be possible to click a button that is disabled.
            {
              InvButton.SingleTapInvoke();

              // if single tap causes the input to be blocked, we still need to 'release'.
              if (IsInputBlocked)
                InvButton.ReleaseInvoke();
            }
          }

          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Leave(CallerMemberName: nameof(Result.LeftClick));
        };
        Result.RightClick += () =>
        {
          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Enter(CallerMemberName: nameof(Result.RightClick));

          if (!IsInputBlocked)
          {
            if (InvButton.Codepoint.IsSet && (!InvButton.HasContextTap() || System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) || System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl)) && Debugger.IsAttached)
              InvButton.Codepoint.Go();
            else if (Result.IsEnabled) // NOTE: it should not be possible to click a button that is disabled.
              InvButton.ContextTapInvoke();
          }

          if (ButtonTraceClass.IsActive)
            ButtonTraceClass.Leave(CallerMemberName: nameof(Result.RightClick));
        };
        return Result;
      }, WpfButton =>
      {
        TranslateVisibility(InvButton.Visibility, WpfButton);
        TranslateMargin(InvButton.Margin, WpfButton);
        TranslatePadding(InvButton.Padding, WpfButton);
        TranslateOpacity(InvButton.Opacity, WpfButton);
        TranslateAlignment(InvButton.Alignment, WpfButton);
        TranslateSize(InvButton.Size, WpfButton);
        TranslateElevation(InvButton.Elevation, WpfButton);
        TranslateFocus(InvButton.Focus, WpfButton);
        TranslateTooltip(InvButton.Tooltip, WpfButton);
        TranslateBorder(InvButton.Border, WpfButton);

        if (InvButton.Background.Render())
        {
          if (IsFlat)
            WpfButton.AsFlat(TranslateBrush(InvButton.Background.Colour), TranslateHoverBrush(InvButton.Background.Colour), TranslatePressBrush(InvButton.Background.Colour));
          else
            WpfButton.Background = TranslateBrush(InvButton.Background.Colour);
        }

        //if (WpfButton.IsEnabled != InvButton.IsEnabled) // NOTE: checking if IsEnabled has changed is unreliable in WPF - just set it each time the button changes.
        WpfButton.IsEnabled = InvButton.IsEnabled;

        WpfButton.Focusable = InvButton.IsFocusable;

        if (InvButton.Corner.Render())
          WpfButton.CornerRadius = TranslateCorner(InvButton.Corner);

        TranslateHint(WpfButton, InvButton.Hint);

        if (InvButton.ContentSingleton.Render())
        {
          WpfButton.SetContent(null); // TODO: there is a problem in Help > Items if this line is removed.
          WpfButton.SetContent(RoutePanel(InvButton.ContentSingleton.Data));
        }
      });
    }
    internal WpfCanvas TranslateCanvas(Inv.Canvas InvCanvas)
    {
      return TranslatePanel(InvCanvas, () =>
      {
        var Result = new WpfCanvas(this);

        var IsLeftPressed = false;
        var IsRightPressed = false;
        var LeftMouseDownTimestamp = 0;
        var RightMouseDownTimestamp = 0;

        // TODO: IsManipulationEnabled breaks all the mouse events.
        //       which would be fine except I have to reimplement all the useful touch gestures (perhaps learn from Blake.NUI).
        //Result.IsManipulationEnabled = true; 
        /*
        var TouchCount = 0;

        Result.TouchMove += (Sender, Event) =>
        {
          var MovePoint = TranslatePoint(Event.GetTouchPoint(Result).Position);
          P.MoveInvoke(MovePoint);

          Event.Handled = true;
        };
        Result.TouchDown += (Sender, Event) =>
        {
          TouchCount++;
        
          if (TouchCount == 1)
          {
            System.Windows.Input.Mouse.Capture(Result);
            Result.CaptureTouch(Event.TouchDevice);
        
            var PressPoint = TranslatePoint(Event.GetTouchPoint(Result).Position);
            InvCanvas.PressInvoke(PressPoint);
        
            Event.Handled = true;
          }
        };
        Result.TouchUp += (Sender, Event) =>
        {
          TouchCount--;
        
          if (TouchCount == 0)
          {
            Result.ReleaseTouchCapture(Event.TouchDevice);
            Result.ReleaseMouseCapture();
        
            var ReleasePoint = TranslatePoint(Event.GetTouchPoint(Result).Position);
            InvCanvas.ReleaseInvoke(ReleasePoint);
        
            Event.Handled = true;
          }
        };
        */
        Result.MouseMove += (Sender, Event) =>
        {
          if (IsLeftPressed || IsRightPressed || InvCanvas.IsCaptured)
          {
            InvCanvas.MoveInvoke(new CanvasCommand(TranslatePoint(Event.GetPosition(Result)), IsLeftPressed, IsRightPressed));

            Event.Handled = true;
          }
        };
        Result.MouseDown += (Sender, Event) =>
        {
          System.Windows.Input.Mouse.Capture(Result);

          if (Event.ClickCount < 2)
          {
            if (Event.ChangedButton == System.Windows.Input.MouseButton.Left)
            {
              IsLeftPressed = true;
              LeftMouseDownTimestamp = Event.Timestamp;

              InvCanvas.PressInvoke(new CanvasCommand(TranslatePoint(Event.GetPosition(Result)), LeftMouseButton: true, RightMouseButton: false));
            }
            else if (Event.ChangedButton == System.Windows.Input.MouseButton.Right)
            {
              IsRightPressed = true;
              RightMouseDownTimestamp = Event.Timestamp;

              InvCanvas.PressInvoke(new CanvasCommand(TranslatePoint(Event.GetPosition(Result)), LeftMouseButton: false, RightMouseButton: true));
            }
          }

          Event.Handled = true;
        };
        Result.MouseUp += (Sender, Event) =>
        {
          Result.ReleaseMouseCapture();

          if (Event.ClickCount < 2)
          {
            var ReleasePoint = TranslatePoint(Event.GetPosition(Result));

            if (Event.ChangedButton == System.Windows.Input.MouseButton.Left)
            {
              if (IsLeftPressed)
              {
                IsLeftPressed = false;
                InvCanvas.ReleaseInvoke(new CanvasCommand(ReleasePoint, LeftMouseButton: true, RightMouseButton: false));

                if (Event.Timestamp - LeftMouseDownTimestamp <= 250)
                  InvCanvas.SingleTapInvoke(ReleasePoint);
              }
            }
            else if (Event.ChangedButton == System.Windows.Input.MouseButton.Right)
            {
              if (IsRightPressed)
              {
                IsRightPressed = false;
                InvCanvas.ReleaseInvoke(new CanvasCommand(ReleasePoint, LeftMouseButton: false, RightMouseButton: true));

                if (Event.Timestamp - RightMouseDownTimestamp <= 250)
                  InvCanvas.ContextTapInvoke(ReleasePoint);
              }
            }
          }

          Event.Handled = true;
        };
        Result.MouseDoubleClick += (Sender, Event) =>
        {
          if (Event.ChangedButton == System.Windows.Input.MouseButton.Left)
            InvCanvas.DoubleTapInvoke(TranslatePoint(Event.GetPosition(Result)));
        };
        Result.MouseWheel += (Sender, Event) =>
        {
          InvCanvas.ZoomInvoke(new Inv.Zoom(TranslatePoint(Event.GetPosition(Result)), Event.Delta > 0 ? +1 : -1));
        };
        Result.ManipulationDelta += (Sender, Event) =>
        {
          // NOTE: this requires IsManipulationEnabled which is a problem (see comment above).

          if (Event.DeltaManipulation.Scale.X != 1.0 || Event.DeltaManipulation.Scale.Y != 1.0)
            InvCanvas.ZoomInvoke(new Inv.Zoom(TranslatePoint(Event.Manipulators.First().GetPosition(Result)), Event.DeltaManipulation.Scale.X > 1.0 || Event.DeltaManipulation.Scale.Y > 1.0 ? +1 : -1));
        };
        Result.MeasureFunc = (Constraint) =>
        {
          var Measure = new Inv.CanvasMeasure(new Inv.Dimension((int)Constraint.Width, (int)Constraint.Height));

          InvCanvas.MeasureInvoke(Measure);

          if (Measure.Dimension == null)
            return null;

          return new System.Windows.Size(Measure.Dimension.Value.Width, Measure.Dimension.Value.Height);
        };

        return Result;
      }, WpfCanvas =>
      {
        TranslateLayout(InvCanvas, WpfCanvas);

        if (InvCanvas.IsInvalidated)
          GraphicsCanvasDraw(WpfCanvas, InvCanvas.DrawInvoke);
      });
    }
    internal WpfDock TranslateDock(Inv.Dock InvDock)
    {
      return TranslatePanel(InvDock, () =>
      {
        var Result = new WpfDock();
        Result.IsHorizontal = InvDock.IsHorizontal;
        return Result;
      }, WpfDock =>
      {
        TranslateLayout(InvDock, WpfDock);

        if (InvDock.IsHorizontal != WpfDock.IsHorizontal || InvDock.CollectionRender())
        {
          WpfDock.IsHorizontal = InvDock.IsHorizontal;

          WpfDock.Compose(InvDock.HeaderCollection.Select(H => RoutePanel(H)), InvDock.ClientCollection.Select(H => RoutePanel(H)), InvDock.FooterCollection.Select(H => RoutePanel(H)));
        }
      });
    }
    internal WpfEdit TranslateEdit(Inv.Edit InvEdit)
    {
      var IsPassword = InvEdit.Input == EditInput.Password;
      var IsSearch = InvEdit.Input == EditInput.Search;

      return TranslatePanel(InvEdit, () =>
      {
        var Result = new WpfEdit(IsSearch, IsPassword);

        // TODO: Wpf allowed input.
        switch (InvEdit.Input)
        {
          case EditInput.Decimal:
            break;

          case EditInput.Email:
            break;

          case EditInput.Integer:
            break;

          case EditInput.Name:
            break;

          case EditInput.Number:
            break;

          case EditInput.Password:
            break;

          case EditInput.Phone:
            break;

          case EditInput.Search:
            break;

          case EditInput.Text:
            Result.AsTextBox().SpellCheck.IsEnabled = true;
            break;

          case EditInput.Uri:
            break;

          case EditInput.Username:
            break;

          default:
            throw new Exception("EditInput not handled: " + InvEdit.Input);
        }

        Result.GotFocus += (Sender, Event) => InvEdit.Focus.GotInvoke();
        Result.LostFocus += (Sender, Event) => InvEdit.Focus.LostInvoke();
        Result.TextChanged += (Sender, Event) => InvEdit.ChangeText(Result.Text);
        Result.KeyPress += (Sender, Event) =>
        {
          // Don't fire Return key press as this is handled by ReturnEvent.
          if (Event.Key == System.Windows.Input.Key.Return)
            return;

          var Keystroke = GetKeystroke(Event);

          if (Keystroke != null)
            InvEdit.PressKey(Keystroke);
        };
        Result.KeyDown += (Sender, Event) =>
        {
          if (Event.Key == System.Windows.Input.Key.Return)
          {
            InvEdit.Return();
            Event.Handled = true;
          }
        };

        return Result;
      }, WpfEdit =>
      {
        TranslateLayout(InvEdit, WpfEdit);

        if (IsSearch)
        {
          var SearchBox = WpfEdit.AsSearchBox();
          SearchBox.TextAlignment = TranslateTextAlignment(InvEdit.Justify);

          TranslateFont(InvEdit.Font, SearchBox);
          TranslateFocus(InvEdit.Focus, SearchBox);
          TranslateHint(SearchBox, InvEdit.Hint);
        }
        else if (IsPassword)
        {
          var PasswordBox = WpfEdit.AsPasswordBox();
          PasswordBox.HorizontalContentAlignment = TranslateHorizontalAlignment(InvEdit.Justify);

          TranslateFont(InvEdit.Font, PasswordBox);
          TranslateFocus(InvEdit.Focus, PasswordBox);
          TranslateHint(PasswordBox, InvEdit.Hint);
        }
        else
        {
          var TextBox = WpfEdit.AsTextBox();
          TextBox.TextAlignment = TranslateTextAlignment(InvEdit.Justify);

          TranslateFont(InvEdit.Font, TextBox);
          TranslateFocus(InvEdit.Focus, TextBox);
          TranslateHint(TextBox, InvEdit.Hint);
        }

        WpfEdit.IsReadOnly = InvEdit.IsReadOnly;
        WpfEdit.Text = InvEdit.Text;
      });
    }
    internal WpfFlow TranslateFlow(Inv.Flow InvFlow)
    {
      WpfFlowItem TranslateFlowItem(Inv.Panel InvPanel)
      {
        // make sure all changes are processed before returning the panel.
        ProcessChanges();

        return new WpfFlowItem()
        {
          Panel = InvPanel,
          Element = RoutePanel(InvPanel)
        };
      }

      return TranslatePanel(InvFlow, () =>
      {
        var Result = new WpfFlow();
        Result.SectionCountQuery = () => InvFlow.SectionCount;
        Result.HeaderContentQuery = (Section) => TranslateFlowItem(InvFlow.Sections[Section]?.Header);
        Result.FooterContentQuery = (Section) => TranslateFlowItem(InvFlow.Sections[Section]?.Footer);
        Result.ItemCountQuery = (Section) => InvFlow.Sections[Section]?.ItemCount ?? 0;
        Result.ItemContentQuery = (Section, Item) => TranslateFlowItem(InvFlow.Sections[Section]?.ItemInvoke(Item));
        Result.ItemRecycleEvent = (Section, Item, Recyle) => InvFlow.Sections[Section]?.RecycleInvoke(Item, Recyle.Panel);
        return Result;
      }, WpfFlow =>
      {
        TranslateLayout(InvFlow, WpfFlow);

        WpfFlow.Fixture = RoutePanel(InvFlow.Fixture);

        if (InvFlow.IsRefresh)
        {
          InvFlow.IsRefresh = false;

          // TODO: animate refresh.
        }

        if (InvFlow.IsReload)
        {
          InvFlow.IsReload = false;
          WpfFlow.Reload();
        }

        if (InvFlow.ScrollSection != null || InvFlow.ScrollIndex != null)
        {
          if (InvFlow.ScrollSection != null && InvFlow.ScrollIndex != null)
            WpfFlow.ScrollTo(InvFlow.ScrollSection.Value, InvFlow.ScrollIndex.Value);

          InvFlow.ScrollSection = null;
          InvFlow.ScrollIndex = null;
        }
      });
    }
    internal WpfFrame TranslateFrame(Inv.Frame InvFrame)
    {
      return TranslatePanel(InvFrame, () =>
      {
        var Result = new WpfFrame();
        return Result;
      }, WpfFrame =>
      {
        TranslateLayout(InvFrame, WpfFrame);

        if (InvFrame.ContentSingleton.Render())
        {
          var WpfToContent = RoutePanel(InvFrame.ContentSingleton.Data);

          var InvTransition = InvFrame.ActiveTransition;

          if (InvTransition == null)
          {
            WpfFrame.ClearChildren();

            if (WpfToContent != null)
              WpfFrame.AddChild(WpfToContent);
          }
          else
          {
            if (WpfFrame.IsTransitioning)
            {
              InvFrame.ContentSingleton.Change(); // currently transitioning, we need to wait and try again next cycle.
            }
            else
            {
              var WpfFromContent = WpfFrame.ChildCount == 0 ? null : (System.Windows.FrameworkElement)WpfFrame.GetChild(0);

              if (InvFrame.FromPanel != null)
              {
                // NOTE: this gives the previous panel a chance to process before it is animated away.
                RoutePanel(InvFrame.FromPanel);
                InvFrame.FromPanel = null;
              }

              if (WpfFromContent != WpfToContent)
              {
                WpfFrame.IsTransitioning = true;
                ExecuteTransition(InvTransition, WpfFrame, WpfFromContent, WpfToContent, () => WpfFrame.IsTransitioning = false);
              }

              InvFrame.ActiveTransition = null;
            }
          }
        }
      });
    }
    internal WpfGraphic TranslateGraphic(Inv.Graphic InvGraphic)
    {
      return TranslatePanel(InvGraphic, () => new WpfGraphic(), WpfGraphic =>
      {
        TranslateLayout(InvGraphic, WpfGraphic);
        TranslateTooltip(InvGraphic.Tooltip, WpfGraphic);

        if (InvGraphic.Fit.Render())
          WpfGraphic.Stretch = TranslateFit(InvGraphic.Fit.Method);

        if (InvGraphic.ImageSingleton.Render())
          WpfGraphic.Source = TranslateImage(InvGraphic.Image);
      });
    }
    internal WpfLabel TranslateLabel(Inv.Label InvLabel)
    {
      return TranslatePanel(InvLabel, () => new WpfLabel(), WpfLabel =>
      {
        TranslateLayout(InvLabel, WpfLabel);
        TranslateFont(InvLabel.Font, WpfLabel.AsTextBlock());
        TranslateTooltip(InvLabel.Tooltip, WpfLabel);

        WpfLabel.TextWrapping = InvLabel.LineWrapping ? System.Windows.TextWrapping.Wrap : System.Windows.TextWrapping.NoWrap;
        WpfLabel.TextAlignment = TranslateTextAlignment(InvLabel.Justify);
        WpfLabel.Text = InvLabel.Text;
      });
    }
    internal WpfMemo TranslateMemo(Inv.Memo InvMemo)
    {
      return TranslatePanel(InvMemo, () =>
      {
        var Result = new WpfMemo();
        Result.TextChanged += (Sender, Event) => InvMemo.ChangeText(Result.Text);
        Result.GotFocus += (Sender, Event) => InvMemo.Focus.GotInvoke();
        Result.LostFocus += (Sender, Event) => InvMemo.Focus.LostInvoke();
        return Result;
      }, WpfMemo =>
      {
        TranslateLayout(InvMemo, WpfMemo);

        WpfMemo.IsReadOnly = InvMemo.IsReadOnly;

        var RenderText = WpfMemo.Text != InvMemo.Text;
        if (InvMemo.MarkupCollection.Render())
          RenderText = true;

        if (RenderText)
        {
          if (InvMemo.MarkupCollection.Count == 0)
          {
            WpfMemo.SetPlainText(InvMemo.Text);
          }
          else
          {
            WpfMemo.SetRichText(InvMemo.Text);

            var AllRange = WpfMemo.GetRichTextRange(0, InvMemo.Text.Length);
            var AllFamily = InvMemo.Font.Name != null ? new System.Windows.Media.FontFamily(InvMemo.Font.Name) : null;
            var AllBrush = TranslateBrush(InvMemo.Font.Colour);
            var AllSize = InvMemo.Font.Size;
            var AllWeight = InvMemo.Font.Weight != null ? TranslateFontWeight(InvMemo.Font.Weight.Value) : (System.Windows.FontWeight?)null;

            if (AllBrush != null)
              AllRange.ApplyPropertyValue(System.Windows.Documents.TextElement.ForegroundProperty, AllBrush);

            if (AllFamily != null)
              AllRange.ApplyPropertyValue(System.Windows.Documents.TextElement.FontFamilyProperty, AllFamily);

            if (AllSize != null)
              AllRange.ApplyPropertyValue(System.Windows.Documents.TextElement.FontSizeProperty, (double)AllSize.Value);

            if (AllWeight != null)
              AllRange.ApplyPropertyValue(System.Windows.Documents.TextElement.FontWeightProperty, AllWeight.Value);

            foreach (var Markup in InvMemo.MarkupCollection)
            {
              if (Markup.RangeList.Count > 0)
              {
                var MarkupFamily = Markup.Font.Name != null ? new System.Windows.Media.FontFamily(Markup.Font.Name) : null;
                var MarkupBrush = TranslateBrush(Markup.Font.Colour);
                var MarkupSize = Markup.Font.Size;
                var MarkupWeight = Markup.Font.Weight != null ? TranslateFontWeight(Markup.Font.Weight.Value) : (System.Windows.FontWeight?)null;

                foreach (var Range in Markup.RangeList)
                {
                  var WpfTextRange = WpfMemo.GetRichTextRange(Range.Index, Range.Count);

                  if (MarkupBrush != null)
                    WpfTextRange.ApplyPropertyValue(System.Windows.Documents.TextElement.ForegroundProperty, MarkupBrush);

                  if (MarkupFamily != null)
                    WpfTextRange.ApplyPropertyValue(System.Windows.Documents.TextElement.FontFamilyProperty, MarkupFamily);

                  if (MarkupSize != null)
                    WpfTextRange.ApplyPropertyValue(System.Windows.Documents.TextElement.FontSizeProperty, (double)MarkupSize.Value);

                  if (MarkupWeight != null)
                    WpfTextRange.ApplyPropertyValue(System.Windows.Documents.TextElement.FontWeightProperty, MarkupWeight.Value);
                }
              }
            }
          }
        }

        if (WpfMemo.IsPlainText)
        {
          var PlainTextBox = WpfMemo.AsPlainTextBox();

          //PlainTextBox.TextWrapping = InvMemo.LineWrapping ? System.Windows.TextWrapping.Wrap : System.Windows.TextWrapping.NoWrap;

          TranslateFont(InvMemo.Font, PlainTextBox);
          TranslateFocus(InvMemo.Focus, PlainTextBox);
        }
        else if (WpfMemo.IsRichText)
        {
          var RichTextBox = WpfMemo.AsRichTextBox();

          // Wpf RichTextBox does not support no-wrap.

          TranslateFont(InvMemo.Font, RichTextBox);
          TranslateFocus(InvMemo.Focus, RichTextBox);
        }
      });
    }
    internal WpfNative TranslateNative(Inv.Native InvNative)
    {
      return TranslatePanel(InvNative, () => new WpfNative(), WpfNative =>
      {
        TranslateLayout(InvNative, WpfNative);

        if (InvNative.ContentSingleton.Render())
          WpfNative.SetContent((System.Windows.FrameworkElement)InvNative.Content);
      });
    }
    internal WpfOverlay TranslateOverlay(Inv.Overlay InvOverlay)
    {
      return TranslatePanel(InvOverlay, () =>
      {
        var Result = new WpfOverlay();
        return Result;
      }, WpfOverlay =>
      {
        TranslateLayout(InvOverlay, WpfOverlay);

        if (InvOverlay.PanelCollection.Render())
        {
          WpfOverlay.ClearChildren();
          foreach (var InvPanel in InvOverlay.PanelCollection)
          {
            var WpfPanel = RoutePanel(InvPanel);
            WpfOverlay.AddChild(WpfPanel);
          }
        }
      });
    }
    internal WpfScroll TranslateScroll(Inv.Scroll InvScroll)
    {
      return TranslatePanel(InvScroll, () =>
      {
        var Result = new WpfScroll();
        Result.SetOrientation(InvScroll.IsHorizontal);
        return Result;
      }, WpfScroll =>
      {
        TranslateLayout(InvScroll, WpfScroll);

        WpfScroll.IsScrollViewerDark = Inv.Theme.IsDark;

        WpfScroll.SetOrientation(InvScroll.IsHorizontal);

        if (InvScroll.ContentSingleton.Render())
          WpfScroll.Content = RoutePanel(InvScroll.ContentSingleton.Data);

        var Request = InvScroll.HandleRequest();
        if (Request != null)
        {
          switch (Request.Value)
          {
            case ScrollRequest.Start:
              if (InvScroll.IsHorizontal)
                WpfScroll.ScrollToLeftEnd();
              else
                WpfScroll.ScrollToTop();
              break;

            case ScrollRequest.End:
              if (InvScroll.IsHorizontal)
                WpfScroll.ScrollToRightEnd();
              else
                WpfScroll.ScrollToBottom();
              break;

            default:
              throw EnumHelper.UnexpectedValueException(Request.Value);
          }
        }
      });
    }
    internal WpfShape TranslateShape(Inv.Shape InvShape)
    {
      return TranslatePanel(InvShape, () =>
      {
        var Result = new WpfShape();
        return Result;
      }, WpfShape =>
      {
        TranslateLayout(InvShape, WpfShape);

        if (InvShape.Fit.Render())
          WpfShape.Stretch = TranslateFit(InvShape.Fit.Method);

        if (InvShape.Fill.Render())
          WpfShape.Fill = TranslateBrush(InvShape.Fill.Colour);

        if (InvShape.Stroke.Render())
        {
          WpfShape.Stroke = TranslateBrush(InvShape.Stroke.Colour);
          WpfShape.StrokeThickness = InvShape.Stroke.Thickness;
          WpfShape.StrokeLineJoin = TranslateLineJoin(InvShape.Stroke.Join);
          WpfShape.StrokeStartLineCap = TranslateLineCap(InvShape.Stroke.Cap);
          WpfShape.StrokeEndLineCap = WpfShape.StrokeStartLineCap;
          WpfShape.StrokeDashArray = InvShape.Stroke.DashPattern == null ? null : new System.Windows.Media.DoubleCollection(InvShape.Stroke.DashPattern.Select(I => (double)I));
        }

        if (InvShape.FigureCollection.Render())
        {
          var WpfGroup = new System.Windows.Media.GeometryGroup();

          void MergeGeometry(System.Windows.Media.Geometry WpfGeometry)
          {
            WpfGeometry.Freeze();
            WpfGroup.Children.Add(WpfGeometry);
          }

          foreach (var InvFigure in InvShape.FigureCollection)
          {
            if (InvFigure.Polygon != null)
            {
              var PathArray = InvFigure.Polygon.PathArray;
              if (PathArray != null && PathArray.Length > 0)
              {
                var WpfGeometry = new System.Windows.Media.PathGeometry();

                var StartPoint = new System.Windows.Point(PathArray[0].X, PathArray[0].Y);
                var LinePoints = PathArray.Skip(1).Select(P => new System.Windows.Point(P.X, P.Y));

                var WpfSegment = new System.Windows.Media.PolyLineSegment(LinePoints, isStroked: true);
                WpfSegment.Freeze();

                var WpfFigure = new System.Windows.Media.PathFigure(StartPoint, WpfSegment.ToEnumerable(), closed: true);
                WpfFigure.Freeze();

                WpfGeometry.Figures.Add(WpfFigure);

                MergeGeometry(WpfGeometry);
              }
            }
            else if (InvFigure.Ellipse != null)
            {
              var WpfGeometry = new System.Windows.Media.EllipseGeometry();

              WpfGeometry.Center = TranslatePoint(InvFigure.Ellipse.Center);
              WpfGeometry.RadiusX = InvFigure.Ellipse.Radius.X;
              WpfGeometry.RadiusY = InvFigure.Ellipse.Radius.Y;

              MergeGeometry(WpfGeometry);
            }
            else if (InvFigure.Line != null)
            {
              var WpfGeometry = new System.Windows.Media.LineGeometry();

              WpfGeometry.StartPoint = TranslatePoint(InvFigure.Line.Start);
              WpfGeometry.EndPoint = TranslatePoint(InvFigure.Line.End);

              MergeGeometry(WpfGeometry);
            }
          }

          WpfGroup.Freeze();

          var NotionalDimension = InvShape.GetNotionalDimension();
          WpfShape.Set(WpfGroup, NotionalDimension.Width, NotionalDimension.Height);
        }
      });
    }
    internal WpfStack TranslateStack(Inv.Stack InvStack)
    {
      return TranslatePanel(InvStack, () =>
      {
        var Result = new WpfStack();
        Result.Orientation = InvStack.IsHorizontal ? System.Windows.Controls.Orientation.Horizontal : System.Windows.Controls.Orientation.Vertical;
        return Result;
      }, WpfStack =>
      {
        TranslateLayout(InvStack, WpfStack);

        WpfStack.Orientation = InvStack.IsHorizontal ? System.Windows.Controls.Orientation.Horizontal : System.Windows.Controls.Orientation.Vertical;

        if (InvStack.PanelCollection.Render())
          WpfStack.Compose(InvStack.PanelCollection.Select(P => RoutePanel(P)));
      });
    }
    internal WpfSwitch TranslateSwitch(Inv.Switch InvSwitch)
    {
      return TranslatePanel(InvSwitch, () =>
      {
        return new WpfSwitch();
      }, WpfSwitch =>
      {
        TranslateLayout(InvSwitch, WpfSwitch);

        WpfSwitch.PrimaryBrush = TranslateBrush(InvSwitch.PrimaryColour);
        WpfSwitch.SecondaryBrush = TranslateBrush(InvSwitch.SecondaryColour);
        WpfSwitch.IsOn = InvSwitch.IsOn;
        WpfSwitch.IsEnabled = InvSwitch.IsEnabled;
        WpfSwitch.ChangeEvent += (Sender, Event) =>
        {
          InvSwitch.ChangeChecked(WpfSwitch.IsOn);
        };
      });
    }
    internal WpfTable TranslateTable(Inv.Table InvTable)
    {
      return TranslatePanel(InvTable, () =>
      {
        var Result = new WpfTable();
        return Result;
      }, WpfTable =>
      {
        TranslateLayout(InvTable, WpfTable);

        if (InvTable.CollectionRender())
        {
          WpfTable.ClearChildren();
          WpfTable.RowDefinitions.Clear();
          WpfTable.ColumnDefinitions.Clear();

          foreach (var TableColumn in InvTable.ColumnCollection)
          {
            WpfTable.ColumnDefinitions.Add(new System.Windows.Controls.ColumnDefinition() { Width = TranslateTableLength(TableColumn, true) });

            var WpfColumn = RoutePanel(TableColumn.Content);

            if (WpfColumn != null)
            {
              WpfTable.AddChild(WpfColumn);

              System.Windows.Controls.Grid.SetColumn(WpfColumn, TableColumn.Index);
              System.Windows.Controls.Grid.SetRow(WpfColumn, 0);
              System.Windows.Controls.Grid.SetRowSpan(WpfColumn, InvTable.RowCollection.Count);
            }
          }

          foreach (var TableRow in InvTable.RowCollection)
          {
            WpfTable.RowDefinitions.Add(new System.Windows.Controls.RowDefinition() { Height = TranslateTableLength(TableRow, false) });

            var WpfRow = RoutePanel(TableRow.Content);

            if (WpfRow != null)
            {
              WpfTable.AddChild(WpfRow);

              System.Windows.Controls.Grid.SetRow(WpfRow, TableRow.Index);
              System.Windows.Controls.Grid.SetColumn(WpfRow, 0);
              System.Windows.Controls.Grid.SetColumnSpan(WpfRow, InvTable.ColumnCollection.Count);
            }
          }

          foreach (var TableCell in InvTable.CellCollection)
          {
            var WpfCell = RoutePanel(TableCell.Content);

            if (WpfCell != null)
            {
              WpfTable.AddChild(WpfCell);

              System.Windows.Controls.Grid.SetColumn(WpfCell, TableCell.Column.Index);
              System.Windows.Controls.Grid.SetRow(WpfCell, TableCell.Row.Index);
            }
          }
        }
      });
    }
    internal WpfVideo TranslateVideo(Inv.Video InvVideo)
    {
      return TranslatePanel(InvVideo, () =>
      {
        var Result = new WpfVideo();
        return Result;
      }, WpfVideo =>
      {
        TranslateLayout(InvVideo, WpfVideo);

        if (InvVideo.SourceSingleton.Render())
        {
          var InvSource = InvVideo.SourceSingleton.Data;

          if (InvSource?.Asset != null)
            WpfVideo.LoadUri(new Uri(SelectAssetPath(InvSource.Asset)));
          else if (InvSource?.File != null)
            WpfVideo.LoadUri(new Uri(SelectFilePath(InvSource.File)));
          else if (InvSource?.Uri != null)
            WpfVideo.LoadUri(InvSource.Uri);
          else
            WpfVideo.LoadUri(null);
        }

        if (InvVideo.StateSingleton.Render())
        {
          switch (InvVideo.StateSingleton.Data)
          {
            case VideoState.Stop:
              WpfVideo.Stop();
              break;

            case VideoState.Pause:
              WpfVideo.Pause();
              break;

            case VideoState.Play:
              WpfVideo.Play();
              break;

            case VideoState.Restart:
              WpfVideo.Stop();
              WpfVideo.Play();
              break;

            default:
              throw new Exception("VideoState not handled: " + InvVideo.StateSingleton.Data);
          }
        }
      });
    }
    internal WpfWrap TranslateWrap(Inv.Wrap InvWrap)
    {
      return TranslatePanel(InvWrap, () =>
      {
        var Result = new WpfWrap();
        Result.Orientation = InvWrap.IsHorizontal ? System.Windows.Controls.Orientation.Horizontal : System.Windows.Controls.Orientation.Vertical;
        return Result;
      }, WpfWrap =>
      {
        TranslateLayout(InvWrap, WpfWrap);

        WpfWrap.Orientation = InvWrap.IsHorizontal ? System.Windows.Controls.Orientation.Horizontal : System.Windows.Controls.Orientation.Vertical;

        if (InvWrap.PanelCollection.Render())
          WpfWrap.Compose(InvWrap.PanelCollection.Select(P => RoutePanel(P)));
      });
    }
    internal WpfPopup TranslatePopup(Inv.Popup InvPopup)
    {
      var WpfPopup = (Inv.WpfPopup)InvPopup.Node;
      if (WpfPopup == null)
      {
        WpfPopup = new Inv.WpfPopup();
        InvPopup.Node = WpfPopup;
        WpfPopup.FontFamily = new System.Windows.Media.FontFamily(WpfOptions.DefaultFontName);
        WpfPopup.ShowEvent += () => InvPopup.ShowInvoke();
        WpfPopup.HideEvent += () => InvPopup.HideInvoke();

        InvPopup.ChangeAction = () =>
        {
          // NOTE: Checking MenuDropAlignment is necessary because the popup placement modes were designed for menus and tooltips,
          //       and these should pop in the opposite direction for right-handed people so that the menu does not appear under the user's hand.
          //       However, for placement of a popup, it should position as requested instead of sometimes being reversed.

          WpfPopup.Background = TranslateBrush(Inv.Theme.BackgroundColour);
          WpfPopup.PlacementMode = InvPopup.Position.Location switch
          {
            Inv.PopupLocation.Default => System.Windows.Controls.Primitives.PlacementMode.Center,
            Inv.PopupLocation.Above => System.Windows.Controls.Primitives.PlacementMode.Top,
            Inv.PopupLocation.Below => System.Windows.Controls.Primitives.PlacementMode.Bottom,
            Inv.PopupLocation.LeftOf => System.Windows.SystemParameters.MenuDropAlignment ? System.Windows.Controls.Primitives.PlacementMode.Right : System.Windows.Controls.Primitives.PlacementMode.Left,
            Inv.PopupLocation.RightOf => System.Windows.SystemParameters.MenuDropAlignment ? System.Windows.Controls.Primitives.PlacementMode.Left : System.Windows.Controls.Primitives.PlacementMode.Right,
            Inv.PopupLocation.CenterOver => System.Windows.Controls.Primitives.PlacementMode.Center,
            Inv.PopupLocation.Pointer => System.Windows.Controls.Primitives.PlacementMode.Mouse,
            _ => throw Inv.Support.EnumHelper.UnexpectedValueException(InvPopup.Position.Location)
          };
          WpfPopup.PlacementTarget = (InvPopup.Position.Location == PopupLocation.Default ? null : RoutePanel(InvPopup.Position.Target)) ?? WpfContainer;
          WpfPopup.Content = RoutePanel(InvPopup.Content);
        };

        // manually invoke the change action for this first render.
        InvPopup.ChangeInvoke();
      }

      return WpfPopup;
    }
    internal void StartAnimation(Inv.Animation InvAnimation)
    {
      var WpfStoryboard = new System.Windows.Media.Animation.Storyboard();
      InvAnimation.Node = WpfStoryboard;
      WpfStoryboard.Completed += (Sender, Event) => InvAnimation.Complete();

      foreach (var InvTarget in InvAnimation.Targets)
      {
        var WpfPanel = RoutePanel(InvTarget.Panel);
        foreach (var InvTransform in InvTarget.Transforms)
        {
          foreach (var WpfTimeline in TranslateAnimationTransform(InvTarget.Panel, WpfPanel, InvTransform))
          {
            WpfTimeline.Freeze();

            WpfStoryboard.Children.Add(WpfTimeline);
          }
        }
      }

      WpfStoryboard.Freeze();
      WpfStoryboard.Begin();
    }
    internal void StopAnimation(Inv.Animation InvAnimation)
    {
      if (InvAnimation.Node != null)
      {
        var WpfStoryboard = (System.Windows.Media.Animation.Storyboard)InvAnimation.Node;
        InvAnimation.Node = null;

        foreach (var InvTarget in InvAnimation.Targets)
        {
          var InvPanel = InvTarget.Panel;

          var WpfPanel = RoutePanel(InvPanel);
          if (WpfPanel != null)
          {
            var InvControl = InvPanel.Control;

            if (WpfPanel.Opacity != InvControl.Opacity.Get())
            {
              // NOTE: this line ACTUALLY forces the opacity to match the currently animating opacity.
              WpfPanel.Opacity = WpfPanel.Opacity;
              InvControl.Opacity.BypassSet((float)WpfPanel.Opacity);

              // unlocks the opacity property.
              WpfPanel.BeginAnimation(System.Windows.FrameworkElement.OpacityProperty, null);
            }

            // NOTE: we have to replace the animating transform with a non-animated version with the same values.
            var TransformGroup = WpfPanel.RenderTransform as System.Windows.Media.TransformGroup;
            if (TransformGroup != null)
            {
              var RenderTransform = new System.Windows.Media.TransformGroup();

              var ScaleTransform = GetTransform<System.Windows.Media.ScaleTransform>(TransformGroup);
              if (ScaleTransform != null)
                RenderTransform.Children.Add(new System.Windows.Media.ScaleTransform(ScaleTransform.ScaleX, ScaleTransform.ScaleY, ScaleTransform.CenterX, ScaleTransform.CenterY));

              var RotateTransform = GetTransform<System.Windows.Media.RotateTransform>(TransformGroup);
              if (RotateTransform != null)
                RenderTransform.Children.Add(new System.Windows.Media.RotateTransform(RotateTransform.Angle, RotateTransform.CenterX, RotateTransform.CenterY));

              var TranslateTransform = GetTransform<System.Windows.Media.TranslateTransform>(TransformGroup);
              if (TranslateTransform != null)
                RenderTransform.Children.Add(new System.Windows.Media.TranslateTransform(TranslateTransform.X, TranslateTransform.Y));

              WpfPanel.RenderTransform = RenderTransform;
            }

            //WpfStoryboard.Remove(WpfPanel);
          }
        }

        WpfStoryboard.Stop();
        //WpfStoryboard.Remove(); // Remove stuffs things up.
      }
    }
    internal void ShowPopup(Inv.Popup InvPopup)
    {
      TranslatePopup(InvPopup).Show();
    }
    internal void HidePopup(Inv.Popup InvPopup)
    {
      TranslatePopup(InvPopup).Hide();
    }
    internal Inv.Keystroke GetKeystroke(System.Windows.Input.KeyEventArgs EventArgs)
    {
      var WpfKey = EventArgs.Key == System.Windows.Input.Key.System ? EventArgs.SystemKey : EventArgs.Key;

      var InvKey = TranslateKey(WpfKey);
      if (InvKey != null)
        return new Inv.Keystroke(InvKey.Value, GetModifier());

      return null;
    }
    internal System.Windows.Window GetWindow()
    {
      return System.Windows.Window.GetWindow(WpfContainer);
    }
    internal IntPtr GetInteropWindowHandle()
    {
      var OwnerWindow = GetWindow();
      return OwnerWindow != null ? new System.Windows.Interop.WindowInteropHelper(OwnerWindow).Handle : IntPtr.Zero;
    }
    internal void HapticFeedback(HapticFeedback Feedback)
    {
      // TODO: investigate Windows haptic feedback?

      if (!WpfOptions.PreventDeviceEmulation)
      {
        WpfEmulationHapticLabel.Foreground = System.Windows.Media.Brushes.Yellow;
        WpfEmulationHapticLabel.Text = "*" + Feedback.ToString().ExcludeAfter("Impact").ExcludeAfter("Notify").ToUpper() + "*";

        var WpfRotateTransform = new System.Windows.Media.RotateTransform(angle: 0, centerX: WpfEmulationHapticLabel.ActualWidth / 2, centerY: WpfEmulationHapticLabel.ActualHeight / 2);
        WpfEmulationHapticLabel.RenderTransform = WpfRotateTransform;

        var WpfStoryboard = new System.Windows.Media.Animation.Storyboard();
        WpfStoryboard.Completed += (Sender, Event) =>
        {
          WpfStoryboard.Remove();

          WpfEmulationHapticLabel.RenderTransform = null;
          WpfEmulationHapticLabel.Foreground = System.Windows.Media.Brushes.LightGray;
          WpfEmulationHapticLabel.Text = "HAPTICS";
        };

        var WpfAnimation = new System.Windows.Media.Animation.DoubleAnimation();
        WpfAnimation.From = -5;
        WpfAnimation.To = 5;
        WpfAnimation.Duration = TimeSpan.FromSeconds(0.5);
        WpfAnimation.FillBehavior = System.Windows.Media.Animation.FillBehavior.Stop;
        WpfAnimation.Freeze();

        WpfStoryboard.Children.Add(WpfAnimation);

        System.Windows.Media.Animation.Storyboard.SetTarget(WpfStoryboard, WpfRotateTransform);
        System.Windows.Media.Animation.Storyboard.SetTargetProperty(WpfStoryboard, new System.Windows.PropertyPath(System.Windows.Media.RotateTransform.AngleProperty));

        WpfStoryboard.Freeze();
        WpfStoryboard.Begin();

        if (Feedback == Inv.HapticFeedback.Shake)
        {
          if (VibrationSound == null)
          {
            using (var ResourceStream = WpfShell.GetResourceStream("Inv.Resources.Sounds.Vibration.mp3"))
            {
              if (ResourceStream == null)
              {
                Debug.Fail("Vibration resource not found.");
                return;
              }

              var VibrationBuffer = new byte[ResourceStream.Length];

              ResourceStream.ReadByteArray(VibrationBuffer);

              this.VibrationSound = new Inv.Sound(VibrationBuffer, ".mp3");
            }
          }

          PlaySound(VibrationSound, Volume: 1.00F, Rate: 1.0F, Pan: 0.0F, Loop: false);
        }
      }
    }
    internal void ReclaimImage(IReadOnlyList<Inv.Image> ImageList)
    {
      // NOTE: no clean up required, just release the reference for the GC.
      foreach (var Image in ImageList)
        Image.Node = null;
    }
    internal void ReclaimSound(IReadOnlyList<Inv.Sound> SoundList)
    {
      if (HasSound)
      {
        foreach (var Sound in SoundList)
          SoundPlayer.Reclaim(Sound);
      }
    }
    internal void StartSpeechRecognition(SpeechRecognition SpeechRecognition)
    {
      WpfEmulatedSpeechRecognitionList.Add(SpeechRecognition);
    }
    internal void StopSpeechRecognition(SpeechRecognition SpeechRecognition)
    {
      StopEmulatedRecognition();

      WpfEmulatedSpeechRecognitionList.Remove(SpeechRecognition);
    }
    internal System.Windows.Media.FormattedText NewFormattedText(string TextFragment, Inv.DrawFont TextFont, int MaximumTextWidth, int MaximumTextHeight)
    {
#pragma warning disable CS0618 // Type or member is obsolete
      var Result = new System.Windows.Media.FormattedText
      (
        textToFormat: TextFragment ?? string.Empty,
        culture: System.Globalization.CultureInfo.CurrentCulture,
        flowDirection: System.Windows.FlowDirection.LeftToRight,
        typeface: new System.Windows.Media.Typeface(WpfShell.NewFontFamily(TextFont.Name.EmptyAsNull() ?? WpfOptions.DefaultFontName), System.Windows.FontStyles.Normal, TranslateFontWeight(TextFont.Weight), System.Windows.FontStretches.Normal),
        emSize: TextFont.Size,
        foreground: TranslateBrush(TextFont.Colour)
      //, pixelsPerDip: ScaleFactorY // TODO: from dnSpy, this seems like the equivalent value, but should we be using something else for this case?
      );

      if (MaximumTextWidth > 0)
        Result.MaxTextWidth = MaximumTextWidth;

      if (MaximumTextHeight > 0)
        Result.MaxTextHeight = MaximumTextHeight;

      return Result;
#pragma warning restore CS0618 // Type or member is obsolete
    }

    private void Resize()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(GetWindow() != null || !WpfOptions.FullScreenMode, "Must not be fullscreen if there is no owner window,");

      // support DPI greater than 100%
      this.ScaleFactorX = Inv.OperatingSystem.LogicalPixelX / 96.0;
      this.ScaleFactorY = Inv.OperatingSystem.LogicalPixelY / 96.0;

      InvApplication.Device.PixelDensity = (float)ScaleFactorY;
      // NOTE: the actual width/height of the container is already scaled appropriately.
      var ResolutionWidthPixels = (int)WpfContainer.ActualWidth;
      if (ResolutionWidthPixels <= 0 && !double.IsNaN(WpfContainer.Width))
        ResolutionWidthPixels = (int)WpfContainer.Width;

      var ResolutionHeightPixels = (int)WpfContainer.ActualHeight;
      if (ResolutionHeightPixels <= 0 && !double.IsNaN(WpfContainer.Height))
        ResolutionHeightPixels = (int)WpfContainer.Height;

      if (!WpfOptions.FullScreenMode && WpfContainer is System.Windows.Window)
      {
        var HorizontalBorderHeight = (int)System.Windows.SystemParameters.ResizeFrameHorizontalBorderHeight;
        var VerticalBorderWidth = (int)System.Windows.SystemParameters.ResizeFrameVerticalBorderWidth;
        var CaptionHeight = (int)System.Windows.SystemParameters.CaptionHeight;

        ResolutionWidthPixels -= (VerticalBorderWidth * 2) + 8;
        ResolutionHeightPixels -= (HorizontalBorderHeight * 2) + CaptionHeight + 9;
      }

      // NOTE: fallback to resolution pixels, based on 24" monitor.

      if (!WpfOptions.PreventDeviceEmulation)
      {
        var IsActiveEmulation = WpfOptions.DeviceEmulation != null;

        if (IsActiveEmulation)
        {
          if (WpfOptions.DeviceEmulationFramed)
          {
            var ResourceLocation = "Inv.Resources.Frames." + WpfOptions.DeviceEmulation.Name.Replace('/', '_') + "." + (WpfOptions.DeviceEmulationRotated ? "Landscape" : "Portrait") + ".png";

            if (ResourceLocation != WpfEmulationDeviceLocation)
            {
              using (var ResourceStream = WpfShell.GetResourceStream(ResourceLocation))
              {
                if (ResourceStream == null)
                {
                  this.WpfEmulationDeviceImage = null;
                }
                else
                {
                  this.WpfEmulationDeviceImage = WpfImage.Decode(ResourceStream);
                  WpfEmulationDeviceGraphic.Source = WpfEmulationDeviceImage;
                  WpfEmulationDeviceGraphic.Width = WpfEmulationDeviceImage.FirstBitmapSource.PixelWidth / 2;
                  WpfEmulationDeviceGraphic.Height = WpfEmulationDeviceImage.FirstBitmapSource.PixelHeight / 2;
                }
              }

              this.WpfEmulationDeviceLocation = ResourceLocation;
            }
          }

          if (WpfOptions.DeviceEmulationRotated)
          {
            ResolutionWidthPixels = WpfOptions.DeviceEmulation.Height;
            ResolutionHeightPixels = WpfOptions.DeviceEmulation.Width;
          }
          else
          {
            ResolutionWidthPixels = WpfOptions.DeviceEmulation.Width;
            ResolutionHeightPixels = WpfOptions.DeviceEmulation.Height;
          }
        }

        WpfEmulationDeviceLabel.Text = IsActiveEmulation ? WpfOptions.DeviceEmulation.Name + " Emulation" : null;
        WpfEmulationInstructionLabel.Text = IsActiveEmulation ? "Ctrl + Up/Down to change the device" + Environment.NewLine + "Ctrl + Left/Right to rotate this device" + Environment.NewLine + "Ctrl + Plus/Minus to toggle the frame" : null;
        WpfEmulationDeviceGraphic.Source = IsActiveEmulation && WpfOptions.DeviceEmulationFramed ? WpfEmulationDeviceImage : null;
      }

      InvApplication.Window.Width = ResolutionWidthPixels;
      InvApplication.Window.Height = ResolutionHeightPixels;

      if (WpfOptions.PreventDeviceEmulation || WpfOptions.DeviceEmulation == null)
      {
        WpfApplicationMaster.Width = double.NaN;
        WpfApplicationMaster.Height = double.NaN;
        WpfApplicationMaster.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
        WpfApplicationMaster.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
      }
      else
      {
        WpfApplicationMaster.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
        WpfApplicationMaster.VerticalAlignment = System.Windows.VerticalAlignment.Center;
        WpfApplicationMaster.Width = ResolutionWidthPixels;
        WpfApplicationMaster.Height = ResolutionHeightPixels;
      }
    }
    private void Process()
    {
      if (InvApplication.IsExit)
      {
        ShutdownEvent?.Invoke();
      }
      else
      {
        var InvWindow = InvApplication.Window;
        var InvKeyboard = InvApplication.Keyboard;

        InvWindow.ProcessInvoke();

        if (InvWindow.ActiveTimerSet.Count > 0)
        {
          foreach (var InvTimer in InvWindow.ActiveTimerSet)
          {
            var WpfTimer = AccessTimer(InvTimer, S =>
            {
              var Result = new System.Windows.Threading.DispatcherTimer();
              Result.Tick += (Sender, Event) =>
              {
                if (InvTimer.IsEnabled)
                  S.IntervalInvoke();
              };
              return Result;
            });

            if (InvTimer.IsRestarting)
            {
              InvTimer.IsRestarting = false;
              WpfTimer.Stop();
            }

            if (WpfTimer.Interval != InvTimer.IntervalTime)
              WpfTimer.Interval = InvTimer.IntervalTime;

            if (InvTimer.IsEnabled && !WpfTimer.IsEnabled)
              WpfTimer.Start();
            else if (!InvTimer.IsEnabled && WpfTimer.IsEnabled)
              WpfTimer.Stop();
          }

          InvWindow.ActiveTimerSet.RemoveWhere(T => !T.IsEnabled);
        }

        foreach (var Controller in ControllerList)
          Controller.Update();

        var InvSurfaceActive = InvWindow.ActiveSurface;

        if (InvSurfaceActive != null)
        {
          var WpfSurface = TranslateSurface(InvSurfaceActive);

          if (!WpfSurfaceMaster.ContainsChild(WpfSurface))
            ArrangeSurface(InvSurfaceActive, WpfSurface);

          ProcessTransition(WpfSurface);

          InvSurfaceActive.ComposeInvoke();

          UpdateSurface(InvSurfaceActive, WpfSurface);
        }
        else
        {
          WpfSurfaceMaster.ClearChildren();
        }

        ProcessChanges();

        if (InvWindow.Render())
        {
          if (InvWindow.Background.Render())
            WpfApplicationMaster.Background = TranslateBrush(InvWindow.Background.Colour ?? WpfOptions.DefaultWindowColour);

          if (WpfApplicationMaster.IsEnabled == InvWindow.InputPrevented) // check in case setting this boolean has side effects even when it's the same value.
          {
            if (InvWindow.InputPrevented)
              this.LastInputElement = System.Windows.Input.Keyboard.FocusedElement;

            WpfApplicationMaster.IsEnabled = !InvWindow.InputPrevented;

            if (!InvWindow.InputPrevented && LastInputElement != null)
              LastInputElement.Focus();
          }

          foreach (var InvPanel in InvWindow.DisablePanelList)
          {
            var WpfPanel = RoutePanel(InvPanel);
            if (WpfPanel != null)
              WpfPanel.IsEnabled = false;
          }
          InvWindow.DisablePanelList.Clear();

          foreach (var InvPanel in InvWindow.EnablePanelList)
          {
            var WpfPanel = RoutePanel(InvPanel);
            if (WpfPanel != null)
              WpfPanel.IsEnabled = true;
          }
          InvWindow.EnablePanelList.Clear();
        }

        if (InvKeyboard.Focus != null)
        {
          var WpfFocus = RoutePanel(InvKeyboard.Focus.Control);
          var WpfOverrideFocus = WpfFocus as WpfOverrideFocusContract;

          InvKeyboard.Focus = null;

          void CancelFocus()
          {
            // Workaround for the inability to cancel the focused element to the default window unfocused state.
            WpfClearFocusBorder.Visibility = System.Windows.Visibility.Visible;
            WpfClearFocusBorder.Focus();
            WpfClearFocusBorder.Visibility = System.Windows.Visibility.Collapsed;

            // NOTE: below puts the window into a weird focus, where key events are not handled.
            //System.Windows.Input.FocusManager.SetFocusedElement(WpfApplicationMaster, null);
            //System.Windows.Input.Keyboard.ClearFocus();
          }

          // TODO: There is a WPF problem where the dotted border only appears if you are navigating by keyboard (does not show the dotted border when setting focus programmatically).

          if (WpfOverrideFocus != null)
            WpfApplicationMaster.Dispatcher.BeginInvoke((Action)delegate { WpfOverrideFocus.OverrideFocus(); }, System.Windows.Threading.DispatcherPriority.Input);
          else if (WpfFocus != null)
            WpfApplicationMaster.Dispatcher.BeginInvoke((Action)delegate { WpfFocus.Focus(); }, System.Windows.Threading.DispatcherPriority.Input);
          else
            WpfApplicationMaster.Dispatcher.BeginInvoke((Action)delegate { CancelFocus(); }, System.Windows.Threading.DispatcherPriority.Input);
        }

        InvWindow.DisplayRate.Calculate();

        if (!WpfOptions.PreventDeviceEmulation)
        {
          // this string comparison is to avoid calling CheckImplicitTooltip on every frame when there's no active emulation.
          var EmulationText = WpfOptions.DeviceEmulation != null ? $"{InvWindow.Width} x {InvWindow.Height} | {InvWindow.DisplayRate.PerSecond} fps | {InvApplication.Process.GetMemoryUsage()}" : "";
          if (WpfEmulationFrameLabel.Text != EmulationText)
            WpfEmulationFrameLabel.Text = EmulationText;
        }
      }
    }

    private void ArrangeSurface(Surface InvSurface, WpfSurface WpfSurface)
    {
      InvSurface.ArrangeInvoke();

      // apply any insets.
      var WpfInset = WpfOptions.PreventDeviceEmulation ? WpfDeviceInset.Zero : (WpfOptions.DeviceEmulationRotated ? WpfOptions.DeviceEmulation?.LandscapeInset : WpfOptions.DeviceEmulation?.PortraitInset) ?? WpfDeviceInset.Zero;
      WpfSurface.Padding = new System.Windows.Thickness(WpfInset.Left, WpfInset.Top, WpfInset.Right, WpfInset.Bottom);
    }
    private void UpdateSurface(Surface InvSurface, WpfSurface WpfSurface)
    {
      if (InvSurface.Render())
      {
        WpfSurface.Background = TranslateBrush(InvSurface.Background.Colour);
        WpfSurface.Child = RoutePanel(InvSurface.Content);
      }
    }
    private void ProcessChanges()
    {
      InvApplication.Window.ProcessChanges(P => RoutePanel(P));
    }
    private void ProcessTransition(WpfSurface WpfSurface)
    {
      var InvWindow = InvApplication.Window;

      var InvTransition = InvWindow.ActiveTransition;
      if (InvTransition == null)
      {
        //Debug.Assert(WpfMaster.Children.Contains(WpfSurface));
      }
      else if (!IsTransitioning) // don't transition while animating a transition.
      {
        var WpfFromSurface = WpfSurfaceMaster.ChildCount == 0 ? null : (WpfSurface)WpfSurfaceMaster.GetChild(0);
        var WpfToSurface = WpfSurface;

        if (WpfFromSurface == WpfToSurface)
        {
          Debug.WriteLine("Transition to self");
        }
        else
        {
          // give the previous surface a chance to process before it is animated away.
          if (InvWindow.FromSurface != null)
          {
            if (TranslateSurface(InvWindow.FromSurface) == WpfFromSurface)
              UpdateSurface(InvWindow.FromSurface, WpfFromSurface);

            InvWindow.FromSurface = null;
          }

          this.TransitionCount++;
          ExecuteTransition(InvTransition, WpfSurfaceMaster, WpfFromSurface, WpfToSurface, () => TransitionCount--);
        }

        InvWindow.ActiveTransition = null;
      }
    }
    private void ExecuteTransition(Inv.Transition InvTransition, WpfGrid WpfGrid, System.Windows.FrameworkElement WpfFromElement, System.Windows.FrameworkElement WpfToElement, Action CompletedAction)
    {
      void CompleteTransition()
      {
        CompletedAction();
        InvTransition.Complete();
      }

      var AnimateOut = WpfFromElement != null;
      var AnimateIn = WpfToElement != null;

      switch (InvTransition.Type)
      {
        case TransitionType.None:
          WpfGrid.ClearChildren();

          if (AnimateIn)
            WpfGrid.AddChild(WpfToElement);

          CompleteTransition();
          break;

        case TransitionType.Fade:
          if (!AnimateOut && !AnimateIn)
          {
            CompleteTransition();
          }
          else
          {
            var WpfFadeDuration = AnimateOut && AnimateIn ? new System.Windows.Duration(TimeSpan.FromMilliseconds(InvTransition.Duration.TotalMilliseconds / 2)) : new System.Windows.Duration(InvTransition.Duration);

            var WpfFadeInAnimation = AnimateIn ? new System.Windows.Media.Animation.DoubleAnimation()
            {
              From = 0.0,
              To = 1.0,
              Duration = WpfFadeDuration,
              FillBehavior = System.Windows.Media.Animation.FillBehavior.Stop
            } : null;
            var WpfFadeOutAnimation = AnimateOut ? new System.Windows.Media.Animation.DoubleAnimation()
            {
              From = 1.0,
              To = 0.0,
              Duration = WpfFadeDuration,
              FillBehavior = System.Windows.Media.Animation.FillBehavior.Stop
            } : null;

            if (AnimateOut)
            {
              WpfFromElement.IsHitTestVisible = false;

              WpfFadeOutAnimation.Completed += (Sender, Event) =>
              {
                //Debug.WriteLine("FADE OUT - completed");

                WpfFromElement.IsHitTestVisible = true;
                WpfGrid.RemoveChild(WpfFromElement);

                if (!AnimateIn)
                {
                  CompleteTransition();
                }
                else
                {
                  //Debug.WriteLine("FADE IN - started");

                  WpfGrid.AddChild(WpfToElement);
                  WpfToElement.BeginAnimation(System.Windows.UIElement.OpacityProperty, WpfFadeInAnimation);
                }
              };
              WpfFadeOutAnimation.Freeze();

              //Debug.WriteLine("FADE OUT - started");
            }

            if (AnimateIn)
            {
              if (!AnimateOut)
                WpfGrid.AddChild(WpfToElement);

              WpfToElement.IsHitTestVisible = false;
              WpfToElement.Opacity = 0.0;

              WpfFadeInAnimation.Completed += (Sender, Event) =>
              {
                //Debug.WriteLine("FADE IN - completed");

                WpfToElement.IsHitTestVisible = true;
                WpfToElement.Opacity = 1.0;

                CompleteTransition();
              };
              WpfFadeInAnimation.Freeze();
            }

            if (AnimateOut)
              WpfFromElement.BeginAnimation(System.Windows.UIElement.OpacityProperty, WpfFadeOutAnimation);
            else if (AnimateIn)
              WpfToElement.BeginAnimation(System.Windows.UIElement.OpacityProperty, WpfFadeInAnimation);
          }
          break;

        case TransitionType.CarouselPrevious:
        case TransitionType.CarouselNext:
        case TransitionType.CarouselAscend:
        case TransitionType.CarouselDescend:
          if (!AnimateOut && !AnimateIn)
          {
            CompleteTransition();
          }
          else
          {
            var CarouselForward = InvTransition.Type == TransitionType.CarouselNext || InvTransition.Type == TransitionType.CarouselAscend;
            var CarouselHorizontal = InvTransition.Type == TransitionType.CarouselNext || InvTransition.Type == TransitionType.CarouselPrevious;

            var WpfCarouselDuration = new System.Windows.Duration(InvTransition.Duration);

            var WpfCarouselStoryboard = new System.Windows.Media.Animation.Storyboard();

            var WpfActualDimension = CarouselHorizontal ? WpfGrid.ActualWidth : WpfGrid.ActualHeight;

            var WpfTargetProperty = CarouselHorizontal ? new System.Windows.PropertyPath(System.Windows.Media.TranslateTransform.XProperty) : new System.Windows.PropertyPath(System.Windows.Media.TranslateTransform.YProperty);

            if (AnimateOut)
            {
              var FromCarouselTransform = new System.Windows.Media.TranslateTransform() { X = 0, Y = 0 };
              WpfFromElement.RenderTransform = FromCarouselTransform;

              WpfFromElement.IsHitTestVisible = false;

              var WpfFromAnimation = new System.Windows.Media.Animation.DoubleAnimation()
              {
                AccelerationRatio = 0.5,
                DecelerationRatio = 0.5,
                Duration = WpfCarouselDuration,
                From = 0,
                To = CarouselForward ? -WpfActualDimension : WpfActualDimension
              };
              System.Windows.Media.Animation.Storyboard.SetTarget(WpfFromAnimation, FromCarouselTransform);
              System.Windows.Media.Animation.Storyboard.SetTargetProperty(WpfFromAnimation, WpfTargetProperty);
              WpfFromAnimation.Freeze();

              WpfCarouselStoryboard.Children.Add(WpfFromAnimation);
            }

            if (AnimateIn)
            {
              WpfGrid.AddChild(WpfToElement);
              WpfToElement.IsHitTestVisible = false;

              var ToCarouselTransform = new System.Windows.Media.TranslateTransform() { X = 0, Y = 0 };
              WpfToElement.RenderTransform = ToCarouselTransform;

              var WpfToAnimation = new System.Windows.Media.Animation.DoubleAnimation()
              {
                AccelerationRatio = 0.5,
                DecelerationRatio = 0.5,
                Duration = WpfCarouselDuration,
                From = CarouselForward ? WpfActualDimension : -WpfActualDimension,
                To = 0
              };
              System.Windows.Media.Animation.Storyboard.SetTarget(WpfToAnimation, ToCarouselTransform);
              System.Windows.Media.Animation.Storyboard.SetTargetProperty(WpfToAnimation, WpfTargetProperty);
              WpfToAnimation.Freeze();

              WpfCarouselStoryboard.Children.Add(WpfToAnimation);
            }

            WpfCarouselStoryboard.Completed += (Sender, Event) =>
            {
              if (AnimateOut)
              {
                WpfFromElement.IsHitTestVisible = true;
                WpfGrid.RemoveChild(WpfFromElement);
                WpfFromElement.RenderTransform = null;
              }

              if (AnimateIn)
              {
                WpfToElement.IsHitTestVisible = true;
                WpfToElement.RenderTransform = null;
              }

              WpfCarouselStoryboard.Stop(WpfGrid);

              // TODO: this causes the Completed event to fire twice and displays this message: System.Windows.Media.Animation Warning: 6 : Unable to perform action because the specified Storyboard was never applied to this object for interactive control.
              //WpfCarouselStoryboard.Remove(WpfGrid);

              CompleteTransition();
            };
            WpfCarouselStoryboard.Freeze();

            WpfCarouselStoryboard.Begin(WpfGrid, true);
          }
          break;

        default:
          throw new Exception("TransitionType not handled: " + InvTransition.Type);
      }
    }
    private TTransform GetTransform<TTransform>(System.Windows.Media.TransformGroup TransformGroup)
      where TTransform : System.Windows.Media.Transform
    {
      return (TTransform)TransformGroup.Children.Find(C => C.GetType() == typeof(TTransform));
    }
    private TTransform ForceTransform<TTransform>(System.Windows.FrameworkElement WpfPanel)
      where TTransform : System.Windows.Media.Transform
    {
      var TransformGroup = WpfPanel.RenderTransform as System.Windows.Media.TransformGroup;
      if (TransformGroup == null)
      {
        TransformGroup = new System.Windows.Media.TransformGroup();
        WpfPanel.RenderTransform = TransformGroup;
      }

      var Result = GetTransform<TTransform>(TransformGroup);

      if (Result == null)
      {
        Result = Activator.CreateInstance<TTransform>();
        TransformGroup.Children.Add(Result);
      }

      return Result;
    }
    private IEnumerable<System.Windows.Media.Animation.Timeline> TranslateAnimationTransform(Inv.Panel InvPanel, System.Windows.FrameworkElement WpfPanel, AnimationTransform InvTransform)
    {
      return InvTransform.Type switch
      {
        AnimationType.Fade => TranslateAnimationFadeTransform(InvPanel, WpfPanel, (AnimationFadeTransform)InvTransform),
        AnimationType.Rotate => TranslateAnimationRotateTransform(InvPanel, WpfPanel, (AnimationRotateTransform)InvTransform),
        AnimationType.Scale => TranslateAnimationScaleTransform(InvPanel, WpfPanel, (AnimationScaleTransform)InvTransform),
        AnimationType.Translate => TranslateAnimationTranslateTransform(InvPanel, WpfPanel, (AnimationTranslateTransform)InvTransform),
        _ => throw new Exception("Animation Transform not handled: " + InvTransform.Type),
      };
    }
    private IEnumerable<System.Windows.Media.Animation.Timeline> TranslateAnimationFadeTransform(Inv.Panel InvPanel, System.Windows.FrameworkElement WpfPanel, AnimationFadeTransform InvTransform)
    {
      var Result = new System.Windows.Media.Animation.DoubleAnimation()
      {
        From = float.IsNaN(InvTransform.FromOpacity) ? WpfPanel.Opacity : InvTransform.FromOpacity,
        To = InvTransform.ToOpacity,
        Duration = InvTransform.Duration,
        FillBehavior = System.Windows.Media.Animation.FillBehavior.Stop
      };

      // NOTE: if you set BeginTime to null, the animation will not run!
      if (InvTransform.Offset != null && InvTransform.Offset.Value > TimeSpan.Zero)
        Result.BeginTime = InvTransform.Offset;

      System.Windows.Media.Animation.Storyboard.SetTarget(Result, WpfPanel);
      System.Windows.Media.Animation.Storyboard.SetTargetProperty(Result, new System.Windows.PropertyPath(System.Windows.UIElement.OpacityProperty));

      var InvControl = InvPanel.Control;

      if (!float.IsNaN(InvTransform.FromOpacity))
      {
        InvControl.Opacity.BypassSet(InvTransform.FromOpacity);
        WpfPanel.Opacity = InvTransform.FromOpacity;

        //Debug.Assert(WpfPanel.Opacity == InvTransform.FromOpacity, $"Opacity is {WpfPanel.Opacity} but was expected to be {InvTransform.FromOpacity} (WPF property locking is occurring).");
      }

      Result.Completed += (Sender, Event) =>
      {
        // manually hold the end state.
        InvControl.Opacity.BypassSet(InvTransform.ToOpacity);
        WpfPanel.Opacity = InvControl.Opacity.Get();
      };

      yield return Result;
    }
    private IEnumerable<System.Windows.Media.Animation.Timeline> TranslateAnimationRotateTransform(Inv.Panel InvPanel, System.Windows.FrameworkElement WpfPanel, AnimationRotateTransform InvTransform)
    {
      var RotateTransform = ForceTransform<System.Windows.Media.RotateTransform>(WpfPanel);

      var RenderSize = TranslateRenderSize(WpfPanel);
      RotateTransform.CenterX = RenderSize.Width / 2;
      RotateTransform.CenterY = RenderSize.Height / 2;

      var Result = new System.Windows.Media.Animation.DoubleAnimation()
      {
        From = float.IsNaN(InvTransform.FromAngle) ? (double?)null : InvTransform.FromAngle,
        To = InvTransform.ToAngle,
        Duration = InvTransform.Duration,
        FillBehavior = System.Windows.Media.Animation.FillBehavior.Stop
      };

      // NOTE: if you set BeginTime to null, the animation will not run!
      if (InvTransform.Offset != null)
        Result.BeginTime = InvTransform.Offset;

      System.Windows.Media.Animation.Storyboard.SetTarget(Result, RotateTransform);
      System.Windows.Media.Animation.Storyboard.SetTargetProperty(Result, new System.Windows.PropertyPath(System.Windows.Media.RotateTransform.AngleProperty));

      Result.Completed += (Sender, Event) => RotateTransform.Angle = InvTransform.ToAngle;

      yield return Result;
    }
    private IEnumerable<System.Windows.Media.Animation.Timeline> TranslateAnimationScaleTransform(Inv.Panel InvPanel, System.Windows.FrameworkElement WpfPanel, AnimationScaleTransform InvTransform)
    {
      var ScaleTransform = ForceTransform<System.Windows.Media.ScaleTransform>(WpfPanel);

      var RenderSize = TranslateRenderSize(WpfPanel);
      ScaleTransform.CenterX = RenderSize.Width / 2;
      ScaleTransform.CenterY = RenderSize.Height / 2;

      System.Windows.Media.Animation.Timeline ScaleAnimation(float From, float To, System.Windows.DependencyProperty DependencyProperty)
      {
        var Result = new System.Windows.Media.Animation.DoubleAnimation()
        {
          From = float.IsNaN(From) ? (double?)null : From,
          To = To,
          Duration = InvTransform.Duration,
          FillBehavior = System.Windows.Media.Animation.FillBehavior.Stop
        };

        // NOTE: if you set BeginTime to null, the animation will not run!
        if (InvTransform.Offset != null)
          Result.BeginTime = InvTransform.Offset;

        System.Windows.Media.Animation.Storyboard.SetTarget(Result, ScaleTransform);
        System.Windows.Media.Animation.Storyboard.SetTargetProperty(Result, new System.Windows.PropertyPath(DependencyProperty));

        return Result;
      }

      if (!float.IsNaN(InvTransform.ToWidth))
      {
        if (!float.IsNaN(InvTransform.FromWidth))
          ScaleTransform.ScaleX = InvTransform.FromWidth;

        var ResultX = ScaleAnimation(InvTransform.FromWidth, InvTransform.ToWidth, System.Windows.Media.ScaleTransform.ScaleXProperty);
        ResultX.Completed += (Sender, Event) => ScaleTransform.ScaleX = InvTransform.ToWidth;
        yield return ResultX;
      }

      if (!float.IsNaN(InvTransform.ToHeight))
      {
        if (!float.IsNaN(InvTransform.FromHeight))
          ScaleTransform.ScaleY = InvTransform.FromHeight;

        var ResultY = ScaleAnimation(InvTransform.FromHeight, InvTransform.ToHeight, System.Windows.Media.ScaleTransform.ScaleYProperty);
        ResultY.Completed += (Sender, Event) => ScaleTransform.ScaleY = InvTransform.ToHeight;
        yield return ResultY;
      }
    }
    private IEnumerable<System.Windows.Media.Animation.Timeline> TranslateAnimationTranslateTransform(Inv.Panel InvPanel, System.Windows.FrameworkElement WpfPanel, AnimationTranslateTransform InvTransform)
    {
      var TranslateTransform = ForceTransform<System.Windows.Media.TranslateTransform>(WpfPanel);

      System.Windows.Media.Animation.Timeline TranslateAnimation(int? From, int To, System.Windows.DependencyProperty DependencyProperty)
      {
        var Result = new System.Windows.Media.Animation.DoubleAnimation()
        {
          From = From == null ? (double?)null : From.Value,
          To = To,
          Duration = InvTransform.Duration,
          FillBehavior = System.Windows.Media.Animation.FillBehavior.Stop
        };

        // NOTE: if you set BeginTime to null, the animation will not run!
        if (InvTransform.Offset != null)
          Result.BeginTime = InvTransform.Offset;

        System.Windows.Media.Animation.Storyboard.SetTarget(Result, TranslateTransform);
        System.Windows.Media.Animation.Storyboard.SetTargetProperty(Result, new System.Windows.PropertyPath(DependencyProperty));

        return Result;
      }

      if (InvTransform.ToX != null)
      {
        if (InvTransform.FromX != null)
          TranslateTransform.X = InvTransform.FromX.Value;

        var ResultX = TranslateAnimation(InvTransform.FromX, InvTransform.ToX.Value, System.Windows.Media.TranslateTransform.XProperty);
        ResultX.Completed += (Sender, Event) => TranslateTransform.X = InvTransform.ToX.Value;
        yield return ResultX;
      }

      if (InvTransform.ToY != null)
      {
        if (InvTransform.FromY != null)
          TranslateTransform.Y = InvTransform.FromY.Value;

        var ResultY = TranslateAnimation(InvTransform.FromY, InvTransform.ToY.Value, System.Windows.Media.TranslateTransform.YProperty);
        ResultY.Completed += (Sender, Event) => TranslateTransform.Y = InvTransform.ToY.Value;
        yield return ResultY;
      }
    }
    private System.Windows.FrameworkElement RoutePanel(Inv.Panel InvPanel)
    {
      var InvControl = InvPanel?.Control;

      if (InvControl == null)
        return null;
      else
        return RouteArray[InvControl.ControlType](InvControl);
    }
    private Inv.Key? TranslateKey(System.Windows.Input.Key WpfKey)
    {
      if (KeyDictionary.TryGetValue(WpfKey, out Inv.Key Result))
        return Result;
      else
        return null;
    }
    private System.Windows.Size TranslateRenderSize(System.Windows.FrameworkElement WpfPanel)
    {
      var RenderSize = WpfPanel.RenderSize;
      if (RenderSize.Width <= 0 || RenderSize.Height <= 0)
        RenderSize = new System.Windows.Size(WpfPanel.Width, WpfPanel.Height);

      Debug.Assert(RenderSize.Width > 0 && RenderSize.Height > 0);

      return RenderSize;
    }
    private System.Windows.Media.Brush TranslateHoverBrush(Inv.Colour InvColour)
    {
      if (InvColour == null)
        return null;
      else
      {
        if (InvColour == Inv.Colour.White)
          // Special case needs to be different to its pressed colour.
          return TranslateBrush(InvColour.Darken(0.125F));
        else
          return TranslateBrush(InvColour.Lighten(0.25F));
      }
    }
    private System.Windows.Media.Brush TranslatePressBrush(Inv.Colour InvColour)
    {
      if (InvColour == null)
        return null;
      else
      {
        if (InvColour == Inv.Colour.Black)
          // Special case needs to be different to its hover colour.
          return TranslateBrush(InvColour.Lighten(0.125F));
        else
          return TranslateBrush(InvColour.Darken(0.25F));
      }
    }
    private System.Windows.Media.Color TranslateColour(Inv.Colour Colour)
    {
      var ArgbArray = BitConverter.GetBytes(Colour.RawValue);

      return System.Windows.Media.Color.FromArgb(ArgbArray[3], ArgbArray[2], ArgbArray[1], ArgbArray[0]);
    }
    private Inv.Point TranslatePoint(System.Windows.Point WpfPoint)
    {
      return new Inv.Point((int)WpfPoint.X, (int)WpfPoint.Y);
    }
    private void TranslateLayout(Inv.Control InvControl, System.Windows.Controls.Border WpfPanel)
    {
      if (InvControl.Background.Render())
        WpfPanel.Background = TranslateBrush(InvControl.Background.Colour);

      if (InvControl.Corner.Render())
        WpfPanel.CornerRadius = TranslateCorner(InvControl.Corner);

      TranslateMargin(InvControl.Margin, WpfPanel);
      TranslatePadding(InvControl.Padding, WpfPanel);
      TranslateBorder(InvControl.Border, WpfPanel);
      TranslateOpacity(InvControl.Opacity, WpfPanel);
      TranslateAlignment(InvControl.Alignment, WpfPanel);
      TranslateVisibility(InvControl.Visibility, WpfPanel);
      TranslateSize(InvControl.Size, WpfPanel);
      TranslateElevation(InvControl.Elevation, WpfPanel);

      // only connect SizedChanged for tracked panels.
      WpfPanel.SizeChanged -= PanelSizedChanged;
      if (InvControl.HasAdjust)
        WpfPanel.SizeChanged += PanelSizedChanged;
    }
    private void TranslateBorder(Inv.Border InvBorder, System.Windows.Controls.Control WpfControl)
    {
      if (InvBorder.Render())
      {
        WpfControl.BorderBrush = TranslateBrush(InvBorder.Colour);
        WpfControl.BorderThickness = TranslateEdge(InvBorder);
      }
    }
    private void TranslateBorder(Inv.Border InvBorder, System.Windows.Controls.Border WpfBorder)
    {
      if (InvBorder.Render())
      {
        WpfBorder.BorderBrush = TranslateBrush(InvBorder.Colour);
        WpfBorder.BorderThickness = TranslateEdge(InvBorder);
      }
    }
    private void TranslateMargin(Inv.Margin InvMargin, System.Windows.FrameworkElement WpfElement)
    {
      if (InvMargin.Render())
        WpfElement.Margin = TranslateEdge(InvMargin);
    }
    private void TranslatePadding(Inv.Padding InvPadding, System.Windows.Controls.Control WpfControl)
    {
      if (InvPadding.Render())
        WpfControl.Padding = TranslateEdge(InvPadding);
    }
    private void TranslatePadding(Inv.Padding InvPadding, System.Windows.Controls.Border WpfBorder)
    {
      if (InvPadding.Render())
        WpfBorder.Padding = TranslateEdge(InvPadding);
    }
    private void TranslateSize(Inv.Size InvSize, System.Windows.FrameworkElement WpfElement)
    {
      if (InvSize.Render())
      {
        if (InvSize.Width != null)
          WpfElement.Width = InvSize.Width.Value;
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.WidthProperty);

        if (InvSize.Height != null)
          WpfElement.Height = InvSize.Height.Value;
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.HeightProperty);

        if (InvSize.MinimumWidth != null)
          WpfElement.MinWidth = InvSize.MinimumWidth.Value;
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.MinWidthProperty);

        if (InvSize.MinimumHeight != null)
          WpfElement.MinHeight = InvSize.MinimumHeight.Value;
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.MinHeightProperty);

        if (InvSize.MaximumWidth != null)
          WpfElement.MaxWidth = InvSize.MaximumWidth.Value;
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.MaxWidthProperty);

        if (InvSize.MaximumHeight != null)
          WpfElement.MaxHeight = InvSize.MaximumHeight.Value;
        else
          WpfElement.ClearValue(System.Windows.FrameworkElement.MaxHeightProperty);
      }
    }
    private void TranslateVisibility(Inv.Visibility InvVisibility, System.Windows.FrameworkElement WpfElement)
    {
      if (InvVisibility.Render())
        WpfElement.Visibility = InvVisibility.Get() ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
    }
    private void TranslateAlignment(Inv.Alignment InvAlignment, System.Windows.FrameworkElement WpfElement)
    {
      if (InvAlignment.Render())
      {
        switch (InvAlignment.Get())
        {
          case Inv.Placement.Stretch:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchLeft:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchCenter:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.StretchRight:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            break;

          case Inv.Placement.TopStretch:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopLeft:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopCenter:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            break;

          case Inv.Placement.TopRight:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            break;

          case Inv.Placement.CenterStretch:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            break;

          case Inv.Placement.CenterLeft:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            break;

          case Inv.Placement.Center:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            break;

          case Inv.Placement.CenterRight:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            break;

          case Inv.Placement.BottomStretch:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomLeft:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomCenter:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            break;

          case Inv.Placement.BottomRight:
            WpfElement.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            WpfElement.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            break;

          default:
            throw new Exception("Inv.Placement not handled: " + InvAlignment.Get());
        }
      }
    }
    private void TranslateElevation(Inv.Elevation InvElevation, System.Windows.FrameworkElement WpfElement)
    {
      if (InvElevation.Render())
      {
        var Depth = InvElevation.Get();
        if (Depth > 0 && ElevationDictionary.TryGetValue(Math.Min(5, Depth), out var ElevationSpec))
        {
          WpfElement.Effect = new System.Windows.Media.Effects.DropShadowEffect()
          {
            Color = TranslateColour(ElevationSpec.Colour),
            Direction = ElevationSpec.Direction,
            ShadowDepth = ElevationSpec.ShadowDepth,
            Opacity = 1.00,
            RenderingBias = System.Windows.Media.Effects.RenderingBias.Performance,
            BlurRadius = ElevationSpec.BlurRadius
          };
        }
        else
        {
          WpfElement.Effect = null;
        }
      }
    }
    private void TranslateFont(Inv.Font InvFont, System.Windows.Documents.Inline WpfElement)
    {
      InvFont.Render(); // NOTE: we always have to apply the font, because the Inline is recreated each time.
      {
        if (InvFont.Name != null)
          WpfElement.FontFamily = new System.Windows.Media.FontFamily(InvFont.Name);
        else
          WpfElement.ClearValue(System.Windows.Documents.Inline.FontFamilyProperty);

        if (InvFont.Size != null)
          WpfElement.FontSize = InvFont.Size.Value;
        else
          WpfElement.ClearValue(System.Windows.Documents.Inline.FontSizeProperty);

        if (InvFont.Colour != null)
          System.Windows.Documents.Inline.SetForeground(WpfElement, TranslateBrush(InvFont.Colour));
        else
          WpfElement.ClearValue(System.Windows.Documents.Inline.ForegroundProperty);

        if (InvFont.Weight != null)
          WpfElement.FontWeight = TranslateFontWeight(InvFont.Weight.Value);
        else
          WpfElement.ClearValue(System.Windows.Documents.Inline.FontWeightProperty);

        if (InvFont.Axis != null)
          WpfElement.BaselineAlignment = TranslateFontAxisAlignment(InvFont.Axis.Value);
        else
          WpfElement.ClearValue(System.Windows.Documents.Inline.BaselineAlignmentProperty);

        if (InvFont.IsSmallCaps != null)
          WpfElement.Typography.Capitals = InvFont.IsSmallCaps.Value ? System.Windows.FontCapitals.SmallCaps : System.Windows.FontCapitals.Normal;
        else
          WpfElement.ClearValue(System.Windows.Documents.Typography.CapitalsProperty);

        if (InvFont.IsItalics != null)
          WpfElement.FontStyle = InvFont.IsItalics.Value ? System.Windows.FontStyles.Italic : System.Windows.FontStyles.Normal;
        else
          WpfElement.ClearValue(System.Windows.Documents.Inline.FontStyleProperty);

        TranslateFontDecorations(InvFont, WpfElement.TextDecorations);
      }
    }
    private void TranslateFont(Inv.Font InvFont, System.Windows.Controls.TextBlock WpfElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          WpfElement.FontFamily = new System.Windows.Media.FontFamily(InvFont.Name);
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBlock.FontFamilyProperty);

        if (InvFont.Size != null)
          WpfElement.FontSize = InvFont.Size.Value;
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBlock.FontSizeProperty);

        if (InvFont.Colour != null)
          System.Windows.Controls.TextBlock.SetForeground(WpfElement, TranslateBrush(InvFont.Colour));
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBlock.ForegroundProperty);

        if (InvFont.Weight != null)
          WpfElement.FontWeight = TranslateFontWeight(InvFont.Weight.Value);
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBlock.FontWeightProperty);

        if (InvFont.Axis != null)
          WpfElement.Typography.Variants = TranslateFontAxisVariants(InvFont.Axis.Value);
        else
          WpfElement.ClearValue(System.Windows.Documents.Typography.VariantsProperty);

        if (InvFont.IsSmallCaps != null)
          WpfElement.Typography.Capitals = InvFont.IsSmallCaps.Value ? System.Windows.FontCapitals.SmallCaps : System.Windows.FontCapitals.Normal;
        else
          WpfElement.ClearValue(System.Windows.Documents.Typography.CapitalsProperty);

        if (InvFont.IsItalics != null)
          WpfElement.FontStyle = InvFont.IsItalics.Value ? System.Windows.FontStyles.Italic : System.Windows.FontStyles.Normal;
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBlock.FontStyleProperty);

        TranslateFontDecorations(InvFont, WpfElement.TextDecorations);
      }
    }
    private void TranslateFont(Inv.Font InvFont, System.Windows.Controls.TextBox WpfElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          WpfElement.FontFamily = new System.Windows.Media.FontFamily(InvFont.Name);
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBox.FontFamilyProperty);

        if (InvFont.Size != null)
          WpfElement.FontSize = InvFont.Size.Value;
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBox.FontSizeProperty);

        var WpfBrush = TranslateBrush(InvFont.Colour);

        if (WpfBrush != null)
          System.Windows.Documents.TextElement.SetForeground(WpfElement, WpfBrush);
        else
          WpfElement.ClearValue(System.Windows.Documents.TextElement.ForegroundProperty);

        WpfElement.CaretBrush = WpfBrush;

        if (InvFont.Weight != null)
          WpfElement.FontWeight = TranslateFontWeight(InvFont.Weight.Value);
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBox.FontWeightProperty);

        if (InvFont.Axis != null)
          WpfElement.Typography.Variants = TranslateFontAxisVariants(InvFont.Axis.Value);
        else
          WpfElement.ClearValue(System.Windows.Documents.Typography.VariantsProperty);

        if (InvFont.IsSmallCaps != null)
          WpfElement.Typography.Capitals = InvFont.IsSmallCaps.Value ? System.Windows.FontCapitals.SmallCaps : System.Windows.FontCapitals.Normal;
        else
          WpfElement.ClearValue(System.Windows.Documents.Typography.CapitalsProperty);

        if (InvFont.IsItalics != null)
          WpfElement.FontStyle = InvFont.IsItalics.Value ? System.Windows.FontStyles.Italic : System.Windows.FontStyles.Normal;
        else
          WpfElement.ClearValue(System.Windows.Controls.TextBox.FontStyleProperty);

        TranslateFontDecorations(InvFont, WpfElement.TextDecorations);
      }
    }
    private void TranslateFont(Inv.Font InvFont, System.Windows.Controls.RichTextBox WpfElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          WpfElement.FontFamily = new System.Windows.Media.FontFamily(InvFont.Name);
        else
          WpfElement.ClearValue(System.Windows.Controls.RichTextBox.FontFamilyProperty);

        if (InvFont.Size != null)
          WpfElement.FontSize = InvFont.Size.Value;
        else
          WpfElement.ClearValue(System.Windows.Controls.RichTextBox.FontSizeProperty);

        var WpfBrush = TranslateBrush(InvFont.Colour);

        if (WpfBrush != null)
          System.Windows.Documents.TextElement.SetForeground(WpfElement, WpfBrush);
        else
          WpfElement.ClearValue(System.Windows.Documents.TextElement.ForegroundProperty);

        WpfElement.CaretBrush = WpfBrush;

        if (InvFont.Weight != null)
          WpfElement.FontWeight = TranslateFontWeight(InvFont.Weight.Value);
        else
          WpfElement.ClearValue(System.Windows.Controls.RichTextBox.FontWeightProperty);

        if (InvFont.IsItalics != null)
          WpfElement.FontStyle = InvFont.IsItalics.Value ? System.Windows.FontStyles.Italic : System.Windows.FontStyles.Normal;
        else
          WpfElement.ClearValue(System.Windows.Controls.RichTextBox.FontStyleProperty);

        // TODO: RichTextBox Typography and TextDecorations are not supported.
      }
    }
    private void TranslateFont(Inv.Font InvFont, System.Windows.Controls.PasswordBox WpfElement)
    {
      if (InvFont.Render())
      {
        if (InvFont.Name != null)
          WpfElement.FontFamily = new System.Windows.Media.FontFamily(InvFont.Name);
        else
          WpfElement.ClearValue(System.Windows.Controls.PasswordBox.FontFamilyProperty);

        if (InvFont.Size != null)
          WpfElement.FontSize = InvFont.Size.Value;
        else
          WpfElement.ClearValue(System.Windows.Controls.PasswordBox.FontSizeProperty);

        if (InvFont.Colour != null)
          System.Windows.Documents.TextElement.SetForeground(WpfElement, TranslateBrush(InvFont.Colour));
        else
          WpfElement.ClearValue(System.Windows.Controls.PasswordBox.ForegroundProperty);

        if (InvFont.Weight != null)
          WpfElement.FontWeight = TranslateFontWeight(InvFont.Weight.Value);
        else
          WpfElement.ClearValue(System.Windows.Controls.PasswordBox.FontWeightProperty);

        if (InvFont.IsItalics != null)
          WpfElement.FontStyle = InvFont.IsItalics.Value ? System.Windows.FontStyles.Italic : System.Windows.FontStyles.Normal;
        else
          WpfElement.ClearValue(System.Windows.Controls.PasswordBox.FontStyleProperty);

        // NOTE: password box doesn't support Typography or TextDecorations but does it matter if the characters are hidden anyway?
      }
    }
    private void TranslateFontDecorations(Inv.Font InvFont, System.Windows.TextDecorationCollection WpfTextDecorations)
    {
      if (InvFont.IsUnderlined ?? false)
      {
        if (!WpfTextDecorations.ContainsAll(System.Windows.TextDecorations.Underline))
          WpfTextDecorations.AddRange(System.Windows.TextDecorations.Underline);
      }
      else
      {
        if (WpfTextDecorations.ContainsAll(System.Windows.TextDecorations.Underline))
          WpfTextDecorations.RemoveRange(System.Windows.TextDecorations.Underline);
      }

      if (InvFont.IsStrikethrough ?? false)
      {
        if (!WpfTextDecorations.ContainsAll(System.Windows.TextDecorations.Strikethrough))
          WpfTextDecorations.AddRange(System.Windows.TextDecorations.Strikethrough);
      }
      else
      {
        if (WpfTextDecorations.ContainsAll(System.Windows.TextDecorations.Strikethrough))
          WpfTextDecorations.RemoveRange(System.Windows.TextDecorations.Strikethrough);
      }
    }
    private void TranslateFocus(Inv.Focus InvFocus, System.Windows.FrameworkElement WpfElement)
    {
      if (InvFocus.Render())
      {
        // TODO: (dis)connect the GotFocus / LostFocus events from the control?
      }
    }
    private void TranslateTooltip(Inv.Tooltip InvTooltip, System.Windows.FrameworkElement WpfElement)
    {
      if (InvTooltip.Render())
      {
        if (InvTooltip.IsBound)
        {
          var WpfTooltip = WpfElement.ToolTip as WpfTooltip;

          if (WpfTooltip == null)
          {
            WpfTooltip = new WpfTooltip();
            WpfTooltip.Opened += (Sender, Event) => InvTooltip.ShowInvoke();
            WpfTooltip.Closed += (Sender, Event) => InvTooltip.HideInvoke();

            WpfElement.ToolTip = WpfTooltip;
          }

          WpfTooltip.SafeSetContent(RoutePanel(InvTooltip.Content));
        }
        else
        {
          if (WpfElement.ToolTip != null)
            WpfElement.ToolTip = null;
        }
      }
    }
    private void TranslateHint(System.Windows.FrameworkElement WpfElement, string Hint)
    {
      if (Hint != null)
        WpfElement.SetValue(System.Windows.Automation.AutomationProperties.NameProperty, Hint);
      else
        WpfElement.ClearValue(System.Windows.Automation.AutomationProperties.NameProperty);
    }
    private System.Windows.GridLength TranslateTableLength(TableAxis InvTableLength, bool Horizontal)
    {
      switch (InvTableLength.LengthType)
      {
        case TableAxisLength.Auto:
          return System.Windows.GridLength.Auto;

        case TableAxisLength.Fixed:
          return new System.Windows.GridLength(Horizontal ? InvTableLength.LengthValue : InvTableLength.LengthValue, System.Windows.GridUnitType.Pixel);

        case TableAxisLength.Star:
          return new System.Windows.GridLength(InvTableLength.LengthValue, System.Windows.GridUnitType.Star);

        default:
          throw new Exception("Inv.TableLength not handled: " + InvTableLength.LengthType);
      }
    }
    private System.Windows.Thickness TranslateEdge(Inv.Edge InvEdge)
    {
      return new System.Windows.Thickness(InvEdge.Left, InvEdge.Top, InvEdge.Right, InvEdge.Bottom);
    }
    private System.Windows.CornerRadius TranslateCorner(Inv.Corner InvCorner)
    {
      return new System.Windows.CornerRadius(InvCorner.TopLeft, InvCorner.TopRight, InvCorner.BottomRight, InvCorner.BottomLeft);
    }
    private System.Windows.TextAlignment TranslateTextAlignment(Inv.Justify Justify)
    {
      var Justification = Justify.Get();

      return Justification == Justification.Left ? System.Windows.TextAlignment.Left : Justification == Justification.Right ? System.Windows.TextAlignment.Right : System.Windows.TextAlignment.Center;
    }
    private System.Windows.HorizontalAlignment TranslateHorizontalAlignment(Inv.Justify Justify)
    {
      var Justification = Justify.Get();

      return Justification == Justification.Left ? System.Windows.HorizontalAlignment.Left : Justification == Justification.Right ? System.Windows.HorizontalAlignment.Right : System.Windows.HorizontalAlignment.Center;
    }
    private void TranslateOpacity(Opacity Opacity, System.Windows.FrameworkElement WpfElement)
    {
      if (Opacity.Render())
        WpfElement.Opacity = Opacity.Get();
    }
    private Inv.DeviceTheme GetOperatingSystemTheme()
    {
      try
      {
        using (var ThemesKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Themes\Personalize"))
        {
          if (ThemesKey != null)
          {
            var ThemeValue = ThemesKey.GetValue("AppsUseLightTheme");
            if (ThemeValue != null && ThemeValue is int)
              return (int)ThemeValue != 1 ? DeviceTheme.Dark : DeviceTheme.Light;
          }
        }
      }
      catch
      {
        // in case of access errors, default theme is not important enough to prevent startup.
      }

      return DeviceTheme.Light;
    }
    private void StopEmulatedRecognition()
    {
      if (!string.IsNullOrWhiteSpace(WpfEmulationRecognitionEdit.Text))
      {
        var Transcription = new Inv.SpeechTranscription()
        {
          IsFinal = true,
          Text = WpfEmulationRecognitionEdit.Text
        };
        WpfEmulationRecognitionEdit.Text = "";

        foreach (var SpeechRecognition in WpfEmulatedSpeechRecognitionList)
          SpeechRecognition.TranscriptionInvoke(Transcription);
      }
    }
    private void GraphicsCanvasDraw(WpfCanvas WpfCanvas, Action<Inv.DrawContract> DrawAction)
    {
      WpfCanvas.WpfDrawing.Start();
      try
      {
        DrawAction(WpfCanvas.WpfDrawing);
      }
      finally
      {
        WpfCanvas.WpfDrawing.Stop();
      }
    }
    private Inv.Image GraphicsCropImage(Inv.Image Image, Inv.Rect Rect)
    {
      var WpfImage = TranslateImage(Image).FirstBitmapSource;

      var CropImage = WpfImage.Crop(Rect);
      CropImage.Freeze();

      return new WpfImage(WpfImageType.Png, CropImage).Encode();
    }
    private Inv.Image GraphicsDrawImage(Inv.Dimension DrawDimension, Action<Inv.DrawContract> DrawAction)
    {
      var WpfDrawing = new WpfDrawing(this);

      WpfDrawing.Start();
      try
      {
        DrawAction?.Invoke(WpfDrawing);
      }
      finally
      {
        WpfDrawing.Stop();
      }

      var RenderedImage = new System.Windows.Media.Imaging.RenderTargetBitmap(DrawDimension.Width, DrawDimension.Height, 96, 96, System.Windows.Media.PixelFormats.Pbgra32);
      RenderedImage.Render(WpfDrawing.WpfDrawingVisual);
      RenderedImage.Freeze();
      RenderedImage.ReleaseLooseGDIHandles();

      return new WpfImage(WpfImageType.Png, RenderedImage).Encode();
    }
    private System.Windows.Threading.DispatcherTimer AccessTimer(Inv.WindowTimer InvTimer, Func<Inv.WindowTimer, System.Windows.Threading.DispatcherTimer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (System.Windows.Threading.DispatcherTimer)InvTimer.Node;
      }
    }
    private TElement TranslatePanel<TControl, TElement>(TControl InvControl, Func<TElement> BuildFunction, Action<TElement> RenderAction)
      where TControl : Inv.Control
      where TElement : System.Windows.FrameworkElement
    {
      TElement Result;

      // construct the panel if required.
      if (InvControl.Node == null)
      {
        Result = BuildFunction();
        InvControl.Node = Result;
        Result.Tag = InvControl;
      }
      else
      {
        Result = (TElement)InvControl.Node;
      }

      // render the panel if required.
      if (InvControl.Render())
      {
        // trap any exceptions as it will cascade to disrupt other panels that may not have any problems.
        try
        {
          RenderAction(Result);
        }
        catch (Exception Exception)
        {
          Inv.SystemExceptionHandler.Route(Exception);
        }
      }

      return Result;
    }
    private void Rendering(object Sender, EventArgs Args)
    {
      /*
      Calculating the framerate from the UI thread is difficult. WPF decouples the UI thread from the render thread.
      
      The UI thread will render:
       * Whenever something is marked as dirty and we drain down to Render priority.
         This can happen more often than the refresh rate.
       * If an animation is pending (or if someone hooked the CompositionTarget.Rendering event) we will render on the UI thread after every present from the render thread.
         This involves advancing the timing tree so animations calculate their new values.
      
      Because of this, the CompositionTarget.Rendering event can be raised multiple times per 'frame'.
      We report the intended 'frame time' in the RenderingEventArgs, and applications should only do 'per-frame' work when the reported frame time changes.
      */

      var Event = (System.Windows.Media.RenderingEventArgs)Args;
      var NextRenderingTicks = Event.RenderingTime.Ticks;

      if (NextRenderingTicks == LastRenderingTicks)
        return;

      this.LastRenderingTicks = NextRenderingTicks;

      Process();
    }
    private void PanelSizedChanged(object sender, System.Windows.SizeChangedEventArgs e)
    {
      if (e.WidthChanged || e.HeightChanged)
      {
        var InvControl = ((Inv.Panel)(((System.Windows.FrameworkElement)sender).Tag))?.Control;

        // NOTE: WPF will fire SizedChanged twice.
        //       We can't throttle because this happens in subsequent rendering loops.
        //       By the second event the panel has the correct size.
        //       Doesn't seem to be a way to ignore the interim event.
        if (InvControl != null)
          InvControl.AdjustInvoke();
      }
    }

    private readonly System.Windows.Controls.ContentControl WpfContainer;
    private readonly Dictionary<System.Windows.Input.Key, Inv.Key> KeyDictionary;
    private readonly Inv.EnumArray<Inv.ControlType, Func<Inv.Control, System.Windows.FrameworkElement>> RouteArray;
    private readonly WpfMaster WpfApplicationMaster;
    private readonly WpfMaster WpfSurfaceMaster;
    private readonly System.Windows.Controls.Border WpfBorder;
    private readonly System.Windows.Controls.Border WpfClearFocusBorder;
    private readonly WpfLabel WpfEmulationInstructionLabel;
    private readonly WpfLabel WpfEmulationFrameLabel;
    private readonly WpfLabel WpfEmulationDeviceLabel;
    private readonly WpfGraphic WpfEmulationDeviceGraphic;
    private readonly WpfLabel WpfEmulationHapticLabel;
    private readonly WpfEdit WpfEmulationRecognitionEdit;
    private readonly WpfSoundPlayer SoundPlayer;
    private readonly Exception SoundException;
    private readonly Dictionary<int, ElevationSpec> ElevationDictionary;
    private readonly Inv.DistinctList<Controller> ControllerList;
    private readonly Inv.DistinctList<Inv.SpeechRecognition> WpfEmulatedSpeechRecognitionList;
    private long LastRenderingTicks;
    private WpfOverlay WpfFlyout;
    private System.Windows.IInputElement LastInputElement;
    private string WpfEmulationDeviceLocation;
    private WpfImage WpfEmulationDeviceImage;
    private int SurfaceCount;
    private int TransitionCount;
    private Inv.Sound VibrationSound;

    private static readonly Inv.TraceClass ButtonTraceClass = Inv.Trace.NewClass<WpfHost>(Inv.Trace.PlatformFeature);

    private struct ElevationSpec
    {
      public ElevationSpec(int BlurRadius, float ShadowDepth, int Direction, Inv.Colour Colour)
      {
        // Actual Material design specs require two shadows with the following details:
        // Top [colour: 12% black, x-offset: 0, y-offset: 1dp, blur: 1.5dp] Bottom [colour: 24% black, x-offset: 0, y-offset: 1dp, blur: 1dp]
        // Top [colour: 16% black, x-offset: 0, y-offset: 3dp, blur: 3dp] Bottom [colour: 23% black, x-offset: 0, y-offset: 3dp, blur: 3dp]
        // Top [colour: 19% black, x-offset: 0, y-offset: 10dp, blur: 10dp] Bottom [colour: 23% black, x-offset: 0, y-offset: 6dp, blur: 3dp]
        // Top [colour: 25% black, x-offset: 0, y-offset: 14dp, blur: 14dp] Bottom [colour: 22% black, x-offset: 0, y-offset: 10dp, blur: 5dp]
        // Top [colour: 30% black, x-offset: 0, y-offset: 19dp, blur: 19dp] Bottom [colour: 22% black, x-offset: 0, y-offset: 15dp, blur: 6dp]

        this.BlurRadius = BlurRadius;
        this.ShadowDepth = ShadowDepth;
        this.Direction = Direction;
        this.Colour = Colour;
      }

      public int BlurRadius { get; set; }
      public float ShadowDepth { get; set; }
      public int Direction { get; set; }
      public Inv.Colour Colour { get; set; }
    }

    private sealed class Controller : Inv.ControllerContract
    {
      public Controller(Inv.Device Device, SharpDX.XInput.UserIndex UserIndex)
      {
        this.Device = Device;
        this.XInputController = new SharpDX.XInput.Controller(UserIndex);
      }

      internal void Update()
      {
        if (InvController == null && XInputController.IsConnected)
        {
          this.InvController = new Inv.Controller(Device, this);
          InvController.UserId = XInputController.UserIndex.ToString(); // TODO.

          Device.AddController(InvController);
        }
        else if (InvController != null && !XInputController.IsConnected)
        {
          Device.RemoveController(InvController);

          this.InvController = null;
        }

        if (InvController != null)
        {
          var State = XInputController.GetState();

          if (LastState == null || State.PacketNumber != LastState.Value.PacketNumber)
          {
            void UpdateButton(ControllerButton Button, SharpDX.XInput.GamepadButtonFlags Flag)
            {
              var WasPressed = LastState?.Gamepad.Buttons.HasFlag(Flag) ?? false;
              var IsPressed = State.Gamepad.Buttons.HasFlag(Flag);

              if (WasPressed && !IsPressed)
                Button.ReleaseInvoke();
              else if (IsPressed && !WasPressed)
                Button.PressInvoke();
            }

            double MapThumbPosition(short Value, short DeadZone)
            {
              var IsNegative = Value < 0;

              if (IsNegative ? Value > -DeadZone : Value < DeadZone)
                return 0;

              return Math.Min(1, Math.Abs((double)Value) / short.MaxValue) * (IsNegative ? -1 : +1);
            }

            InvController.LeftThumbstick.X = MapThumbPosition(State.Gamepad.LeftThumbX, SharpDX.XInput.Gamepad.LeftThumbDeadZone);
            InvController.LeftThumbstick.Y = -MapThumbPosition(State.Gamepad.LeftThumbY, SharpDX.XInput.Gamepad.LeftThumbDeadZone);
            InvController.RightThumbstick.X = MapThumbPosition(State.Gamepad.RightThumbX, SharpDX.XInput.Gamepad.RightThumbDeadZone);
            InvController.RightThumbstick.Y = -MapThumbPosition(State.Gamepad.RightThumbY, SharpDX.XInput.Gamepad.RightThumbDeadZone);
            InvController.LeftTrigger.Depression = State.Gamepad.LeftTrigger;
            InvController.RightTrigger.Depression = State.Gamepad.RightTrigger;

            UpdateButton(InvController.MenuButton, SharpDX.XInput.GamepadButtonFlags.Start);
            UpdateButton(InvController.ViewButton, SharpDX.XInput.GamepadButtonFlags.Back);
            UpdateButton(InvController.DPadUpButton, SharpDX.XInput.GamepadButtonFlags.DPadUp);
            UpdateButton(InvController.DPadLeftButton, SharpDX.XInput.GamepadButtonFlags.DPadLeft);
            UpdateButton(InvController.DPadDownButton, SharpDX.XInput.GamepadButtonFlags.DPadDown);
            UpdateButton(InvController.DPadRightButton, SharpDX.XInput.GamepadButtonFlags.DPadRight);
            UpdateButton(InvController.AButton, SharpDX.XInput.GamepadButtonFlags.A);
            UpdateButton(InvController.BButton, SharpDX.XInput.GamepadButtonFlags.B);
            UpdateButton(InvController.XButton, SharpDX.XInput.GamepadButtonFlags.X);
            UpdateButton(InvController.YButton, SharpDX.XInput.GamepadButtonFlags.Y);
            UpdateButton(InvController.LeftShoulderButton, SharpDX.XInput.GamepadButtonFlags.LeftShoulder);
            UpdateButton(InvController.RightShoulderButton, SharpDX.XInput.GamepadButtonFlags.RightShoulder);
            UpdateButton(InvController.LeftThumbstick.Button, SharpDX.XInput.GamepadButtonFlags.LeftThumb);
            UpdateButton(InvController.RightThumbstick.Button, SharpDX.XInput.GamepadButtonFlags.RightThumb);
          }

          LastState = State;
        }
      }

      ControllerVibration ControllerContract.GetVibration()
      {
        // Note: querying vibration state not supported by SharpDX.

        return new ControllerVibration()
        {
          LeftMotor = 0,
          RightMotor = 0,
          LeftTrigger = 0,
          RightTrigger = 0,
        };
      }
      void ControllerContract.SetVibration(ControllerVibration Vibration)
      {
        if (InvController == null || !XInputController.IsConnected)
          return;

        // Note: LeftTrigger/RightTrigger vibration not supported by SharpDX.

        ushort Translate(double Value) => (ushort)(Value * MaxMotorSpeed);

        XInputController.SetVibration(new SharpDX.XInput.Vibration()
        {
          LeftMotorSpeed = Translate(Vibration.LeftMotor),
          RightMotorSpeed = Translate(Vibration.RightMotor),
        });
      }
      object ControllerContract.Node => throw new NotImplementedException();

      private readonly Inv.Device Device;
      private readonly SharpDX.XInput.Controller XInputController;
      private Inv.Controller InvController;
      private SharpDX.XInput.State? LastState;

      private const ushort MaxMotorSpeed = ushort.MaxValue;
    }
  }

  internal static class ToolTipBehavior
  {
    public static bool GetIsToolTipEnabled(System.Windows.FrameworkElement obj)
    {
      return (bool)obj.GetValue(ToolTipEnabledProperty);
    }

    public static void SetToolTipEnabled(System.Windows.FrameworkElement obj, bool value)
    {
      obj.SetValue(ToolTipEnabledProperty, value);
    }

    public static readonly System.Windows.DependencyProperty ToolTipEnabledProperty = System.Windows.DependencyProperty.RegisterAttached(
        "IsToolTipEnabled",
        typeof(bool),
        typeof(ToolTipBehavior),
        new System.Windows.FrameworkPropertyMetadata(true, System.Windows.FrameworkPropertyMetadataOptions.Inherits, (sender, e) =>
        {
          var element = sender as System.Windows.FrameworkElement;
          if (element != null)
            element.SetValue(System.Windows.Controls.ToolTipService.IsEnabledProperty, e.NewValue);
        }));
  }
}