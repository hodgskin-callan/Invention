﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  public sealed class SocketHost
  {
    public SocketHost()
    {
      this.CertHash = CertHash;
      this.ChannelList = new Inv.DistinctList<SocketChannel>();
    }

    public bool IsActive { get; private set; }
    public byte[] CertHash { get; set; }
    public int Port { get; set; }
    public event Action StartEvent;
    public event Action StopEvent;
    public event Action<Exception> FaultEvent;
    public event Action<SocketChannel> AcceptEvent;
    public event Action<SocketChannel> RejectEvent;

    public void Start()
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Port != 0, "Port must be specified.");
        //Inv.Assert.Check(Certificate != null, "Certificate must be specified.");
      }

      if (!IsActive)
      {
        if (CertHash != null)
        {
          foreach (var StoreLocation in new[] { System.Security.Cryptography.X509Certificates.StoreLocation.CurrentUser, System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine })
          {
            var Store = new System.Security.Cryptography.X509Certificates.X509Store(System.Security.Cryptography.X509Certificates.StoreName.My, StoreLocation);
            Store.Open(System.Security.Cryptography.X509Certificates.OpenFlags.ReadOnly);
            try
            {
              foreach (var Cert in Store.Certificates)
              {
                if (Cert.GetCertHash().ShallowEqualTo(CertHash))
                {
                  this.Certificate = Cert;
                  break;
                }
              }
            }
            finally
            {
              Store.Close();
            }

            if (this.Certificate != null)
              break;
          }

          if (Inv.Assert.IsEnabled)
            Inv.Assert.Check(Certificate == null || Certificate.GetCertHash().ShallowEqualTo(CertHash), "Certificate not found or CertHash mismatch.");
        }

        // cannot start without SSL certificate, we cannot allow an insecure socket to start listening.
        if (CertHash == null || Certificate != null)
        {
          this.TcpServer = new System.Net.Sockets.TcpListener(System.Net.IPAddress.Any, Port);
          TcpServer.Start();
          TcpServer.BeginAcceptTcpClient(AcceptTcpClient, null);

          this.IsActive = true;

          StartEvent?.Invoke();
        }
      }
    }
    public void Stop()
    {
      if (IsActive)
      {
        this.IsActive = false;

        StopEvent?.Invoke();

        if (TcpServer != null)
        {
          TcpServer.Stop();
          this.TcpServer = null;
        }

        foreach (var Channel in ChannelList)
        {
          try
          {
            Channel.Drop();
          }
          catch
          {
          }
        }

        ChannelList.Clear();
      }
    }
    public bool HasCertificate() => Certificate != null;

    internal System.Security.Cryptography.X509Certificates.X509Certificate2 Certificate { get; private set; }

    internal void CloseChannel(SocketChannel Channel)
    {
      try
      {
        Channel.ClosedInvoke();

        RejectEvent?.Invoke(Channel);
      }
      finally
      {
        Channel.Drop();

        lock (ChannelList)
          ChannelList.Remove(Channel);
      }
    }

    private void HandleFault(Exception Exception)
    {
      try
      {
        FaultEvent?.Invoke(Exception);
      }
      catch
      {
        if (Debugger.IsAttached)
          Debugger.Break();
      }
    }
    private void AcceptTcpClient(IAsyncResult Result)
    {
      try
      {
        if (IsActive)
        {
          // immediately wait for the next client.
          TcpServer.BeginAcceptTcpClient(AcceptTcpClient, null);

          // end the prior accept client.
          var TcpClient = EndAcceptTcpClient(Result);

          if (TcpClient != null)
          {
            TcpClient.NoDelay = true;
            TcpClient.ReceiveBufferSize = Inv.TransportFoundation.ReceiveBufferSize;
            TcpClient.SendBufferSize = Inv.TransportFoundation.SendBufferSize;

            var Channel = new SocketChannel(this, TcpClient);

            lock (ChannelList)
              ChannelList.Add(Channel);

            AcceptEvent?.Invoke(Channel);
          }
        }
      }
      catch (Exception Exception)
      {
        HandleFault(Exception);
      }
    }
    [DebuggerNonUserCode]
    private System.Net.Sockets.TcpClient EndAcceptTcpClient(IAsyncResult Result)
    {
      try
      {
        return TcpServer.EndAcceptTcpClient(Result);
      }
      catch (System.Net.Sockets.SocketException Exception)
      {
        // A blocking operation was interrupted by a call to WSACancelBlockingCall"
        if (Exception.SocketErrorCode == System.Net.Sockets.SocketError.Interrupted && Exception.ErrorCode == 10004)
          return null;
        else
          throw Exception.Preserve();
      }
    }

    private readonly Inv.DistinctList<SocketChannel> ChannelList;
    private System.Net.Sockets.TcpListener TcpServer;
  }

  public sealed class SocketChannel : Inv.TransportConnection
  {
    internal SocketChannel(SocketHost Host, System.Net.Sockets.TcpClient TcpClient)
    {
      this.Host = Host;
      this.TcpClient = TcpClient;

      var NetworkStream = TcpClient.GetStream();

      var ProtocolByte = NetworkStream.ReadByte();

      if (ProtocolByte < 0)
      {
        Drop();
        throw new Exception("Socket channel immediately disconnected.");
      }

      switch (ProtocolByte)
      {
        case Inv.TransportFoundation.ProtocolSecureStream:
          if (Host.Certificate != null)
          {
            var SecureStream = new System.Net.Security.SslStream(NetworkStream, leaveInnerStreamOpen: false);
            try
            {
              SecureStream.AuthenticateAsServer(Host.Certificate, clientCertificateRequired: false, System.Security.Authentication.SslProtocols.Tls12, checkCertificateRevocation: true);
            }
            catch
            {
              SecureStream.Dispose();
              throw;
            }

            this.Flow = new Inv.TransportFlow(SecureStream);

            bool FailedAuthentication;

            using (var TransportSender = new Inv.TransportSender())
            {
              var CompactWriter = TransportSender.Writer;

              try
              {
                using (var TransportReceiver = Flow.ReceivePacket().ToReceiver())
                {
                  var CompactReader = TransportReceiver.Reader;

                  var Domain = CompactReader.ReadString();
                  var Username = CompactReader.ReadString();
                  var Password = CompactReader.ReadString().ToSecureString();

                  var WindowsIdentity = WindowsIdentityHelper.GetWindowsIdentity(Domain, Username, Password, false);
                  if (WindowsIdentity == null)
                  {
                    System.Threading.Thread.Sleep(new Random().Next(1000, 2000)); // NOTE: sleep random 1-2 seconds to slowdown brute force authentication attacks.

                    throw new Exception("Unable to log on with the supplied credentials.");
                  }

                  var UserSID = WindowsIdentity.User.Value;
                  CompactWriter.WriteBoolean(true);
                  CompactWriter.WriteString(UserSID);

                  this.Identity = new TransportIdentity(WindowsIdentity.Name, UserSID);
                }

                FailedAuthentication = false;
              }
              catch (Exception Exception)
              {
                FailedAuthentication = true;

                TransportSender.Reset();

                CompactWriter.WriteBoolean(false);
                CompactWriter.WriteString(Exception.Message);
              }

              try
              {
                Flow.SendPacket(TransportSender.ToPacket());
              }
              catch
              {
                FailedAuthentication = true;
              }
            }

            // disconnect on failed authentication.
            if (FailedAuthentication)
            {
              Drop();
              throw new Exception("Protocol negotiate stream failed on authentication.");
            }
          }
          else
          {
            Drop();
            throw new Exception("Protocol secure stream is not supported without a certificate.");
          }
          break;

        case Inv.TransportFoundation.ProtocolNegotiateStream:
          var NegotiateStream = new System.Net.Security.NegotiateStream(NetworkStream, leaveInnerStreamOpen: false);
          try
          {
            NegotiateStream.AuthenticateAsServer(System.Net.CredentialCache.DefaultNetworkCredentials, System.Net.Security.ProtectionLevel.EncryptAndSign, System.Security.Principal.TokenImpersonationLevel.Identification);

            var WindowsIdentity = (System.Security.Principal.WindowsIdentity)NegotiateStream.RemoteIdentity;
            this.Identity = new Inv.TransportIdentity(WindowsIdentity.Name, WindowsIdentity.User.Value);
          }
          catch
          {
            NegotiateStream.Dispose();
            throw;
          }

          this.Flow = new Inv.TransportFlow(NegotiateStream);
          break;

        default:
          Drop();
          throw new Exception("Protocol stream not recognised: " + ProtocolByte);
      }
    }

    public SocketHost Host { get; }
    public Inv.TransportFlow Flow { get; }
    public Inv.TransportIdentity Identity { get; }
    public System.Net.IPAddress RemoteIPAddress => ((System.Net.IPEndPoint)TcpClient.Client.RemoteEndPoint).Address;

    public event Action ClosedEvent;

    public void Drop()
    {
      Flow.Dispose();

      this.TcpClient.Close();
    }

    internal void ClosedInvoke()
    {
      ClosedEvent?.Invoke();
    }

    private readonly System.Net.Sockets.TcpClient TcpClient;
  }

  public sealed class SocketLink : Inv.Mimic<Inv.TransportLink>, Inv.TransportFactory
  {
    public SocketLink()
    {
      this.Base = new TransportLink(this);
    }

    public string Host { get; set; }
    public int Port { get; set; }
    public System.Net.NetworkCredential Credential { get; set; }
    public bool Established => Base.Established;
    public bool IsOffline => Base.IsOffline;
    public event Action<Exception> FaultBeginEvent
    {
      add => Base.FaultBeginEvent += value;
      remove => Base.FaultBeginEvent -= value;
    }
    public event Action<Exception> FaultContinueEvent
    {
      add => Base.FaultContinueEvent += value;
      remove => Base.FaultContinueEvent -= value;
    }
    public event Action<Exception> FaultEndEvent
    {
      add => Base.FaultEndEvent += value;
      remove => Base.FaultEndEvent -= value;
    }
    public event Action FaultReconnectEvent
    {
      add => Base.FaultReconnectEvent += value;
      remove => Base.FaultReconnectEvent -= value;
    }

    public void Connect() => Base.Connect();
    public void Disconnect() => Base.Disconnect();
    public int ConnectionCount() => Base.ConnectionCount();
    public void Exchange(Action<Inv.TransportConnection> Action) => Base.Exchange(Action);
    public T Exchange<T>(Func<Inv.TransportConnection, T> Func) => Base.Exchange(Func);
    public void SimulateFault(TimeSpan TimeSpan) => Base.SimulateFault(TimeSpan);

    TransportConnection TransportFactory.JoinConnection() => new SocketConnection(this);
  }

  public sealed class SocketConnection : Inv.TransportConnection
  {
    public SocketConnection(SocketLink Link)
    {
      var WindowsIdentity = Link.Credential != null ? WindowsIdentityHelper.GetWindowsIdentity(Link.Credential.Domain, Link.Credential.UserName, Link.Credential.SecurePassword) : System.Security.Principal.WindowsIdentity.GetCurrent();
      this.Identity = new TransportIdentity(WindowsIdentity.Name, WindowsIdentity.User.Value);

      this.TcpClient = new System.Net.Sockets.TcpClient();
      TcpClient.SendBufferSize = Inv.TransportFoundation.SendBufferSize;
      TcpClient.ReceiveBufferSize = Inv.TransportFoundation.ReceiveBufferSize;
      TcpClient.NoDelay = true;

      try
      {
        TcpClient.Connect(Link.Host, Link.Port);

        var NetworkStream = TcpClient.GetStream();
        try
        {
          NetworkStream.WriteByte(Inv.TransportFoundation.ProtocolNegotiateStream);
        }
        catch
        {
          NetworkStream.Dispose();
          throw;
        }

        var NegotiateStream = new System.Net.Security.NegotiateStream(TcpClient.GetStream(), leaveInnerStreamOpen: false);
        try
        {
          NegotiateStream.AuthenticateAsClient(Link.Credential ?? System.Net.CredentialCache.DefaultNetworkCredentials, "", System.Net.Security.ProtectionLevel.EncryptAndSign, System.Security.Principal.TokenImpersonationLevel.Identification);
        }
        catch
        {
          NegotiateStream.Dispose();
          throw;
        }

        this.Flow = new Inv.TransportFlow(NegotiateStream);
      }
      catch
      {
        TcpClient.Close();
        throw;
      }
    }

    public Inv.TransportFlow Flow { get; }
    public Inv.TransportIdentity Identity { get; }

    public void Drop()
    {
      Flow.Dispose();

      try
      {
        TcpClient.Close();
      }
      catch
      {
        // TODO: is this allowed?
      }
    }

    private readonly System.Net.Sockets.TcpClient TcpClient;
  }
}