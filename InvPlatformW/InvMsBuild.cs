﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inv
{
  public static class Dotnet
  {
    public static string GetPath()
    {
      if (System.IO.File.Exists(VisualStudio2022Path))
        return VisualStudio2022Path;
      else if (System.IO.File.Exists(DotnetPath))
        return DotnetPath;
      else
        return string.Empty;
    }

    public const string VisualStudio2022Path = @"C:\Program Files\Microsoft Visual Studio\2022\Community\dotnet\runtime\dotnet.exe";
    public const string DotnetPath = @"C:\Program Files\dotnet\dotnet.exe";
  }

  public static class MsBuild
  {
    public static string GetPath()
    {
      // use Visual Studio msbuild if it is available as it is easier to install, update and validate.
      // otherwise, fallback to the build tools if that is the only thing installed.

      if (System.IO.File.Exists(VisualStudio2022Path))
        return VisualStudio2022Path;
      else if (System.IO.File.Exists(BuildTools2022Path))
        return BuildTools2022Path; 
      else
        return string.Empty;
    }

    public const string VisualStudio2022Path = @"C:\Program Files\Microsoft Visual Studio\2022\Community\MSBuild\Current\Bin\MSBuild.exe";
    public const string BuildTools2022Path = @"C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\MSBuild\Current\Bin\MSBuild.exe";
  }
}