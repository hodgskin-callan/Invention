﻿using System;
using System.Runtime.InteropServices;

namespace Inv
{
  /// <summary>
  /// Provides detailed information about the host operating system.
  /// </summary>
  public static class OperatingSystem
  {
    /// <summary>
    /// Full identity of the installed operating system.
    /// </summary>
    public static string Identity => (Name + " " + Edition + " " + Version + " " + ServicePack).Trim();
    /// <summary>
    /// Gets the name of the operating system running on this computer.
    /// </summary>
    public static string Name
    {
      get
      {
        // NOTE: Applications not manifested for Windows 8.1 or Windows 10 will return the Windows 8 OS version value (6.2).

        if (NameField != null)
          return NameField;

        var name = "unknown";

        var osVersion = Environment.OSVersion;
        var osVersionInfo = new Win32.Kernel32.OSVERSIONINFOEX();
        osVersionInfo.dwOSVersionInfoSize = Marshal.SizeOf(typeof(Win32.Kernel32.OSVERSIONINFOEX));

        if (Win32.Kernel32.GetVersionEx(ref osVersionInfo))
        {
          var majorVersion = osVersion.Version.Major;
          var minorVersion = osVersion.Version.Minor;

          switch (osVersion.Platform)
          {
            case PlatformID.Win32Windows:
              if (majorVersion == 4)
              {
                string csdVersion = osVersionInfo.szCSDVersion;
                switch (minorVersion)
                {
                  case 0:
                    if (csdVersion == "B" || csdVersion == "C")
                      name = "Windows 95 OSR2";
                    else
                      name = "Windows 95";
                    break;
                  case 10:
                    if (csdVersion == "A")
                      name = "Windows 98 Second Edition";
                    else
                      name = "Windows 98";
                    break;
                  case 90:
                    name = "Windows Me";
                    break;
                }
              }
              break;

            case PlatformID.Win32NT:
              switch (majorVersion)
              {
                case 5:
                  switch (minorVersion)
                  {
                    case 0:
                      name = "Windows 2000";
                      break;
                    case 1:
                      name = "Windows XP";
                      break;
                  }
                  break;

                case 6:
                  switch (minorVersion)
                  {
                    case 0:
                      name = "Windows Vista";
                      break;
                    case 1:
                      name = "Windows 7";
                      break;
                    case 2:
                      name = "Windows 8";
                      try
                      {
                        var reg = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion");
                        var productName = (string)reg?.GetValue("ProductName");
                        if (productName != null && productName.Contains("Windows 10"))
                          name = "Windows 10";
                      }
                      catch
                      {
                        // ignore registry exceptions.
                      }
                      break;
                    case 3:
                      // TODO: https://msdn.microsoft.com/en-us/library/windows/desktop/dn481241(v=vs.85).aspx
                      name = "Windows 8.1";
                      break;
                  }
                  break;
                case 10:
                  name = "Windows 10";
                  break;
              }
              break;
          }
        }

        NameField = name;
        return name;
      }
    }
    /// <summary>
    /// Gets the edition of the operating system running on this computer.
    /// </summary>
    public static string Edition
    {
      get
      {
        if (EditionField != null)
          return EditionField;

        var edition = string.Empty;

        var osVersion = Environment.OSVersion;
        var osVersionInfo = new Win32.Kernel32.OSVERSIONINFOEX();
        osVersionInfo.dwOSVersionInfoSize = Marshal.SizeOf(typeof(Win32.Kernel32.OSVERSIONINFOEX));

        if (Win32.Kernel32.GetVersionEx(ref osVersionInfo))
        {
          var majorVersion = osVersion.Version.Major;
          var minorVersion = osVersion.Version.Minor;
          var productType = osVersionInfo.wProductType;
          var suiteMask = osVersionInfo.wSuiteMask;

          #region VERSION 4
          if (majorVersion == 4)
          {
            if (productType == Win32.Kernel32.VER_NT_WORKSTATION)
            {
              // Windows NT 4.0 Workstation
              edition = "Workstation";
            }
            else if (productType == Win32.Kernel32.VER_NT_SERVER)
            {
              if ((suiteMask & Win32.Kernel32.VER_SUITE_ENTERPRISE) != 0)
              {
                // Windows NT 4.0 Server Enterprise
                edition = "Enterprise Server";
              }
              else
              {
                // Windows NT 4.0 Server
                edition = "Standard Server";
              }
            }
          }
          #endregion VERSION 4

          #region VERSION 5
          else if (majorVersion == 5)
          {
            if (productType == Win32.Kernel32.VER_NT_WORKSTATION)
            {
              if ((suiteMask & Win32.Kernel32.VER_SUITE_PERSONAL) != 0)
              {
                // Windows XP Home Edition
                edition = "Home";
              }
              else
              {
                // Windows XP / Windows 2000 Professional
                edition = "Professional";
              }
            }
            else if (productType == Win32.Kernel32.VER_NT_SERVER)
            {
              if (minorVersion == 0)
              {
                if ((suiteMask & Win32.Kernel32.VER_SUITE_DATACENTER) != 0)
                {
                  // Windows 2000 Datacenter Server
                  edition = "Datacenter Server";
                }
                else if ((suiteMask & Win32.Kernel32.VER_SUITE_ENTERPRISE) != 0)
                {
                  // Windows 2000 Advanced Server
                  edition = "Advanced Server";
                }
                else
                {
                  // Windows 2000 Server
                  edition = "Server";
                }
              }
              else
              {
                if ((suiteMask & Win32.Kernel32.VER_SUITE_DATACENTER) != 0)
                {
                  // Windows Server 2003 Datacenter Edition
                  edition = "Datacenter";
                }
                else if ((suiteMask & Win32.Kernel32.VER_SUITE_ENTERPRISE) != 0)
                {
                  // Windows Server 2003 Enterprise Edition
                  edition = "Enterprise";
                }
                else if ((suiteMask & Win32.Kernel32.VER_SUITE_BLADE) != 0)
                {
                  // Windows Server 2003 Web Edition
                  edition = "Web Edition";
                }
                else
                {
                  // Windows Server 2003 Standard Edition
                  edition = "Standard";
                }
              }
            }
          }
          #endregion VERSION 5

          #region VERSION 6 and 10
          else if (majorVersion == 6 || majorVersion == 10)
          {
            if (Win32.Kernel32.GetProductInfo(majorVersion, minorVersion, osVersionInfo.wServicePackMajor, osVersionInfo.wServicePackMinor, out var ed))
            {
              // TODO: more editions here: https://msdn.microsoft.com/en-us/library/windows/desktop/ms724358(v=vs.85).aspx

              switch (ed)
              {
                case Win32.Kernel32.PRODUCT_BUSINESS:
                  edition = "Business";
                  break;
                case Win32.Kernel32.PRODUCT_BUSINESS_N:
                  edition = "Business N";
                  break;
                case Win32.Kernel32.PRODUCT_CLUSTER_SERVER:
                  edition = "HPC Edition";
                  break;
                case Win32.Kernel32.PRODUCT_DATACENTER_SERVER:
                  edition = "Datacenter Server";
                  break;
                case Win32.Kernel32.PRODUCT_DATACENTER_SERVER_CORE:
                  edition = "Datacenter Server (core installation)";
                  break;
                case Win32.Kernel32.PRODUCT_ENTERPRISE:
                  edition = "Enterprise";
                  break;
                case Win32.Kernel32.PRODUCT_ENTERPRISE_N:
                  edition = "Enterprise N";
                  break;
                case Win32.Kernel32.PRODUCT_ENTERPRISE_SERVER:
                  edition = "Enterprise Server";
                  break;
                case Win32.Kernel32.PRODUCT_ENTERPRISE_SERVER_CORE:
                  edition = "Enterprise Server (core installation)";
                  break;
                case Win32.Kernel32.PRODUCT_ENTERPRISE_SERVER_CORE_V:
                  edition = "Enterprise Server without Hyper-V (core installation)";
                  break;
                case Win32.Kernel32.PRODUCT_ENTERPRISE_SERVER_IA64:
                  edition = "Enterprise Server for Itanium-based Systems";
                  break;
                case Win32.Kernel32.PRODUCT_ENTERPRISE_SERVER_V:
                  edition = "Enterprise Server without Hyper-V";
                  break;
                case Win32.Kernel32.PRODUCT_HOME_BASIC:
                  edition = "Home Basic";
                  break;
                case Win32.Kernel32.PRODUCT_HOME_BASIC_N:
                  edition = "Home Basic N";
                  break;
                case Win32.Kernel32.PRODUCT_HOME_PREMIUM:
                  edition = "Home Premium";
                  break;
                case Win32.Kernel32.PRODUCT_HOME_PREMIUM_N:
                  edition = "Home Premium N";
                  break;
                case Win32.Kernel32.PRODUCT_HYPERV:
                  edition = "Microsoft Hyper-V Server";
                  break;
                case Win32.Kernel32.PRODUCT_MEDIUMBUSINESS_SERVER_MANAGEMENT:
                  edition = "Windows Essential Business Management Server";
                  break;
                case Win32.Kernel32.PRODUCT_MEDIUMBUSINESS_SERVER_MESSAGING:
                  edition = "Windows Essential Business Messaging Server";
                  break;
                case Win32.Kernel32.PRODUCT_MEDIUMBUSINESS_SERVER_SECURITY:
                  edition = "Windows Essential Business Security Server";
                  break;
                case Win32.Kernel32.PRODUCT_SERVER_FOR_SMALLBUSINESS:
                  edition = "Windows Essential Server Solutions";
                  break;
                case Win32.Kernel32.PRODUCT_SERVER_FOR_SMALLBUSINESS_V:
                  edition = "Windows Essential Server Solutions without Hyper-V";
                  break;
                case Win32.Kernel32.PRODUCT_SMALLBUSINESS_SERVER:
                  edition = "Windows Small Business Server";
                  break;
                case Win32.Kernel32.PRODUCT_STANDARD_SERVER:
                  edition = "Standard Server";
                  break;
                case Win32.Kernel32.PRODUCT_STANDARD_SERVER_CORE:
                  edition = "Standard Server (core installation)";
                  break;
                case Win32.Kernel32.PRODUCT_STANDARD_SERVER_CORE_V:
                  edition = "Standard Server without Hyper-V (core installation)";
                  break;
                case Win32.Kernel32.PRODUCT_STANDARD_SERVER_V:
                  edition = "Standard Server without Hyper-V";
                  break;
                case Win32.Kernel32.PRODUCT_STARTER:
                  edition = "Starter";
                  break;
                case Win32.Kernel32.PRODUCT_STORAGE_ENTERPRISE_SERVER:
                  edition = "Enterprise Storage Server";
                  break;
                case Win32.Kernel32.PRODUCT_STORAGE_EXPRESS_SERVER:
                  edition = "Express Storage Server";
                  break;
                case Win32.Kernel32.PRODUCT_STORAGE_STANDARD_SERVER:
                  edition = "Standard Storage Server";
                  break;
                case Win32.Kernel32.PRODUCT_STORAGE_WORKGROUP_SERVER:
                  edition = "Workgroup Storage Server";
                  break;
                case Win32.Kernel32.PRODUCT_UNDEFINED:
                  edition = "Unknown product";
                  break;
                case Win32.Kernel32.PRODUCT_ULTIMATE:
                  edition = "Ultimate";
                  break;
                case Win32.Kernel32.PRODUCT_ULTIMATE_N:
                  edition = "Ultimate N";
                  break;
                case Win32.Kernel32.PRODUCT_WEB_SERVER:
                  edition = "Web Server";
                  break;
                case Win32.Kernel32.PRODUCT_WEB_SERVER_CORE:
                  edition = "Web Server (core installation)";
                  break;
                case Win32.Kernel32.PRODUCT_PROFESSIONAL:
                  edition = "Professional";
                  break;

                default:
                  edition = "Unknown Edition";
                  break;
              }
            }
          }
          #endregion VERSION 6
        }

        EditionField = edition;
        return edition;
      }
    }
    /// <summary>
    /// Gets the full version of the operating system running on this computer.
    /// </summary>
    public static System.Version Version
    {
      get
      {
        if (VersionField == null)
          VersionField = Environment.OSVersion.Version;

        return VersionField;
      }
    }
    /// <summary>
    /// Gets the service pack information of the operating system running on this computer.
    /// </summary>
    public static string ServicePack
    {
      get
      {
        var servicePack = String.Empty;
        var osVersionInfo = new Win32.Kernel32.OSVERSIONINFOEX();
        osVersionInfo.dwOSVersionInfoSize = Marshal.SizeOf(typeof(Win32.Kernel32.OSVERSIONINFOEX));

        if (Win32.Kernel32.GetVersionEx(ref osVersionInfo))
          servicePack = osVersionInfo.szCSDVersion;

        return servicePack;
      }
    }
    /// <summary>
    /// Number of logical pixels for the width of the screen.
    /// </summary>
    public static int LogicalPixelX
    {
      get
      {
        if (LogicalPixelXField == 0)
          LoadLogicalDimensions();

        return LogicalPixelXField;
      }
    }
    /// <summary>
    /// Number of logical pixels for the height of the screen.
    /// </summary>
    public static int LogicalPixelY
    {
      get
      {
        if (LogicalPixelYField == 0)
          LoadLogicalDimensions();

        return LogicalPixelYField;
      }
    }
    /// <summary>
    /// Vertical refresh rate of the display device, in cycles per second (Hz).
    /// </summary>
    public static int VerticalRefreshRate
    {
      get
      {
        if (VerticalRefreshRateField == 0)
          LoadLogicalDimensions();

        return VerticalRefreshRateField;
      }
    }

    /// <summary>
    /// Ask if the device and operating system supports touch input.
    /// </summary>
    /// <returns></returns>
    public static bool HasTouchInput()
    {
      // detect device is a touch screen, not how many touches (i.e. single touch vs multi-touch).
      foreach (System.Windows.Input.TabletDevice tabletDevice in System.Windows.Input.Tablet.TabletDevices)
      {
        if (tabletDevice.Type == System.Windows.Input.TabletDeviceType.Touch)
          return true;
      }

      return false;
    }
    /// <summary>
    /// Is the process currently running in a container
    /// </summary>
    public static bool RunningInContainer()
    {
      if (!RunningInContainerField.HasValue)
        RunningInContainerField = string.Equals(Environment.GetEnvironmentVariable("DOTNET_RUNNING_IN_CONTAINER"), "true", StringComparison.InvariantCultureIgnoreCase);

      return RunningInContainerField.Value;
    }

    private static void LoadLogicalDimensions()
    {
      var DesktopHandle = Win32.User32.GetDC(IntPtr.Zero);

      LogicalPixelXField = Win32.Gdi32.GetDeviceCaps(DesktopHandle, (int)Win32.Gdi32.DeviceCap.LOGPIXELSX);
      LogicalPixelYField = Win32.Gdi32.GetDeviceCaps(DesktopHandle, (int)Win32.Gdi32.DeviceCap.LOGPIXELSY);
      VerticalRefreshRateField = Win32.Gdi32.GetDeviceCaps(DesktopHandle, (int)Win32.Gdi32.DeviceCap.VREFRESH);

      Win32.User32.ReleaseDC(IntPtr.Zero, DesktopHandle);
    }

    private static int LogicalPixelXField = 0;
    private static int LogicalPixelYField = 0;
    private static int VerticalRefreshRateField = 0;
    private static System.Version VersionField;
    private static string EditionField;
    private static string NameField;
    private static bool? RunningInContainerField;
  }
}