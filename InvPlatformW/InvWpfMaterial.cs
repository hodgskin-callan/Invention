﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;

namespace Inv
{
  internal sealed class WpfSwitchButton : System.Windows.Controls.Primitives.ToggleButton
  {
    static WpfSwitchButton()
    {
      DefaultStyleKeyProperty.OverrideMetadata(typeof(WpfSwitchButton), new FrameworkPropertyMetadata(typeof(WpfSwitchButton)));

      PrimaryBrushProperty = DependencyProperty.Register("PrimaryBrush", typeof(Brush), typeof(WpfCard), new FrameworkPropertyMetadata(Brushes.Green, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsArrange));
      SecondaryBrushProperty = DependencyProperty.Register("SecondaryBrush", typeof(Brush), typeof(WpfCard), new FrameworkPropertyMetadata(Brushes.Black, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsArrange));
    }

    public WpfSwitchButton()
    {
      var StyleDictionary = WpfShell.LoadResourceDictionary("InvWpfSwitch.xaml");

      if (StyleDictionary != null)
      {
        var DefaultButtonStyle = (Style)StyleDictionary["WpfSwitchButtonStyle"];
        DefaultButtonStyle.Seal();

        this.Style = DefaultButtonStyle;
      }

      PrimaryBrush = Brushes.Aqua;
    }

    public static readonly DependencyProperty PrimaryBrushProperty;
    public static readonly DependencyProperty SecondaryBrushProperty;

    public Brush PrimaryBrush
    {
      get { return (Brush)GetValue(PrimaryBrushProperty); }
      set { SetValue(PrimaryBrushProperty, value); }
    }
    public Brush SecondaryBrush
    {
      get { return (Brush)GetValue(SecondaryBrushProperty); }
      set { SetValue(SecondaryBrushProperty, value); }
    }
  }

  [TemplatePart(Name = ClipBorderPartName, Type = typeof(Border))]
  internal class WpfCard : ContentControl
  {
    static WpfCard()
    {
      DefaultStyleKeyProperty.OverrideMetadata(typeof(WpfCard), new FrameworkPropertyMetadata(typeof(WpfCard)));

      ContentClipPropertyKey = DependencyProperty.RegisterReadOnly("ContentClip", typeof(Geometry), typeof(WpfCard), new PropertyMetadata(default(Geometry)));

      ContentClipProperty = ContentClipPropertyKey.DependencyProperty;
      UniformCornerRadiusProperty = DependencyProperty.Register("UniformCornerRadius", typeof(double), typeof(WpfCard), new FrameworkPropertyMetadata(2.0, FrameworkPropertyMetadataOptions.AffectsMeasure));
    }

    public WpfCard()
    {
      var StyleDictionary = WpfShell.LoadResourceDictionary("InvWpfMaterialCard.xaml");

      if (StyleDictionary != null)
      {
        var DefaultButtonStyle = (Style)StyleDictionary["CardStyle"];
        DefaultButtonStyle.Seal();

        this.Style = DefaultButtonStyle;
      }
    }

    public static readonly DependencyProperty UniformCornerRadiusProperty;
    public static readonly DependencyProperty ContentClipProperty;

    public double UniformCornerRadius
    {
      get { return (double)GetValue(UniformCornerRadiusProperty); }
      set { SetValue(UniformCornerRadiusProperty, value); }
    }
    public Geometry ContentClip
    {
      get { return (Geometry)GetValue(ContentClipProperty); }
      private set { SetValue(ContentClipPropertyKey, value); }
    }

    public override void OnApplyTemplate()
    {
      base.OnApplyTemplate();

      ClipBorder = Template.FindName(ClipBorderPartName, this) as System.Windows.Controls.Border;
    }
    protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
    {
      base.OnRenderSizeChanged(sizeInfo);

      if (ClipBorder == null) 
        return;

      var farPoint = new System.Windows.Point(
          Math.Max(0, ClipBorder.ActualWidth),
          Math.Max(0, ClipBorder.ActualHeight));

      var clipRect = new System.Windows.Rect(
          new System.Windows.Point(),
          new System.Windows.Point(farPoint.X, farPoint.Y));

      ContentClip = new RectangleGeometry(clipRect, UniformCornerRadius, UniformCornerRadius);
    }

    private static readonly DependencyPropertyKey ContentClipPropertyKey;

    private System.Windows.Controls.Border ClipBorder;

    public const string ClipBorderPartName = "PART_ClipBorder";
  }

  [TemplateVisualState(GroupName = "CommonStates", Name = TemplateStateNormal)]
  [TemplateVisualState(GroupName = "CommonStates", Name = TemplateStateMousePressed)]
  [TemplateVisualState(GroupName = "CommonStates", Name = TemplateStateMouseOut)]
  internal class WpfRipple : ContentControl
  {
    static WpfRipple()
    {
      DefaultStyleKeyProperty.OverrideMetadata(typeof(WpfRipple), new FrameworkPropertyMetadata(typeof(WpfRipple)));

      EventManager.RegisterClassHandler(typeof(ContentControl), Mouse.PreviewMouseUpEvent, new MouseButtonEventHandler(MouseButtonEventHandler), true);
      EventManager.RegisterClassHandler(typeof(ContentControl), Mouse.MouseMoveEvent, new MouseEventHandler(MouseMoveEventHandler), true);
      EventManager.RegisterClassHandler(typeof(System.Windows.Controls.Primitives.Popup), Mouse.PreviewMouseUpEvent, new MouseButtonEventHandler(MouseButtonEventHandler), true);
      EventManager.RegisterClassHandler(typeof(System.Windows.Controls.Primitives.Popup), Mouse.MouseMoveEvent, new MouseEventHandler(MouseMoveEventHandler), true);

      FeedbackProperty = DependencyProperty.Register("Feedback", typeof(Brush), typeof(WpfRipple), new PropertyMetadata(default(Brush)));
      RippleSizePropertyKey = DependencyProperty.RegisterReadOnly("RippleSize", typeof(double), typeof(WpfRipple), new PropertyMetadata(default(double)));
      RippleSizeProperty = RippleSizePropertyKey.DependencyProperty;
      RippleXPropertyKey = DependencyProperty.RegisterReadOnly("RippleX", typeof(double), typeof(WpfRipple), new PropertyMetadata(default(double)));
      RippleXProperty = RippleXPropertyKey.DependencyProperty;
      RippleYPropertyKey = DependencyProperty.RegisterReadOnly("RippleY", typeof(double), typeof(WpfRipple), new PropertyMetadata(default(double)));
      RippleYProperty = RippleYPropertyKey.DependencyProperty;
      RecognizesAccessKeyProperty = DependencyProperty.Register("RecognizesAccessKey", typeof(bool), typeof(WpfRipple), new PropertyMetadata(default(bool)));

      PressedInstances = new HashSet<WpfRipple>();
    }

    public WpfRipple()
    {
      var StyleDictionary = WpfShell.LoadResourceDictionary("InvWpfMaterialRipple.xaml");

      if (StyleDictionary != null)
      {
        var DefaultButtonStyle = (Style)StyleDictionary["DefaultRipple"];
        DefaultButtonStyle.Seal();

        this.Style = DefaultButtonStyle;
      }

      this.SizeChanged += OnSizeChanged;
    }

    public static readonly DependencyProperty FeedbackProperty;
    public static readonly DependencyProperty RippleSizeProperty;
    public static readonly DependencyProperty RippleXProperty;
    public static readonly DependencyProperty RippleYProperty;
    public static readonly DependencyProperty RecognizesAccessKeyProperty;

    public Brush Feedback
    {
      get { return (Brush)GetValue(FeedbackProperty); }
      set { SetValue(FeedbackProperty, value); }
    }
    public double RippleSize
    {
      get { return (double)GetValue(RippleSizeProperty); }
      private set { SetValue(RippleSizePropertyKey, value); }
    }
    public double RippleX
    {
      get { return (double)GetValue(RippleXProperty); }
      private set { SetValue(RippleXPropertyKey, value); }
    }
    public double RippleY
    {
      get { return (double)GetValue(RippleYProperty); }
      private set { SetValue(RippleYPropertyKey, value); }
    }
    public bool RecognizesAccessKey
    {
      get { return (bool)GetValue(RecognizesAccessKeyProperty); }
      set { SetValue(RecognizesAccessKeyProperty, value); }
    }

    protected override void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs e)
    {
      var point = e.GetPosition(this);

      if (RippleAttachedHelper.GetIsCentered(this))
      {
        var innerContent = (Content as FrameworkElement);

        if (innerContent != null)
        {
          var position = innerContent.TransformToAncestor(this).Transform(new System.Windows.Point(0, 0));

          RippleX = position.X + innerContent.ActualWidth / 2 - RippleSize / 2;
          RippleY = position.Y + innerContent.ActualHeight / 2 - RippleSize / 2;
        }
        else
        {
          RippleX = ActualWidth / 2 - RippleSize / 2;
          RippleY = ActualHeight / 2 - RippleSize / 2;
        }
      }
      else
      {
        RippleX = point.X - RippleSize / 2;
        RippleY = point.Y - RippleSize / 2;
      }

      VisualStateManager.GoToState(this, TemplateStateNormal, false);
      VisualStateManager.GoToState(this, TemplateStateMousePressed, true);
      PressedInstances.Add(this);

      base.OnPreviewMouseLeftButtonDown(e);
    }

    public override void OnApplyTemplate()
    {
      base.OnApplyTemplate();

      VisualStateManager.GoToState(this, TemplateStateNormal, false);
    }

    private static void MouseButtonEventHandler(object sender, MouseButtonEventArgs e)
    {
      foreach (var ripple in PressedInstances)
      {
        // adjust the transition scale time according to the current animated scale
        var scaleTrans = ripple.Template.FindName("ScaleTransform", ripple) as ScaleTransform;
        if (scaleTrans != null)
        {
          double currentScale = scaleTrans.ScaleX;
          var newTime = TimeSpan.FromMilliseconds(300 * (1.0 - currentScale));

          // change the scale animation according to the current scale
          var scaleXKeyFrame = ripple.Template.FindName("MousePressedToNormalScaleXKeyFrame", ripple) as EasingDoubleKeyFrame;
          if (scaleXKeyFrame != null)
          {
            scaleXKeyFrame.KeyTime = KeyTime.FromTimeSpan(newTime);
          }
          var scaleYKeyFrame = ripple.Template.FindName("MousePressedToNormalScaleYKeyFrame", ripple) as EasingDoubleKeyFrame;
          if (scaleYKeyFrame != null)
          {
            scaleYKeyFrame.KeyTime = KeyTime.FromTimeSpan(newTime);
          }
        }

        VisualStateManager.GoToState(ripple, TemplateStateNormal, true);
      }
      PressedInstances.Clear();
    }
    private static void MouseMoveEventHandler(object sender, MouseEventArgs e)
    {
      foreach (var ripple in PressedInstances.ToList())
      {
        var relativePosition = Mouse.GetPosition(ripple);
        if (relativePosition.X < 0
            || relativePosition.Y < 0
            || relativePosition.X >= ripple.ActualWidth
            || relativePosition.Y >= ripple.ActualHeight)
        {
          VisualStateManager.GoToState(ripple, TemplateStateMouseOut, true);
          PressedInstances.Remove(ripple);
        }
      }
    }

    private void OnSizeChanged(object sender, SizeChangedEventArgs sizeChangedEventArgs)
    {
      var innerContent = (Content as FrameworkElement);

      double width, height;

      if (RippleAttachedHelper.GetIsCentered(this) && innerContent != null)
      {
        width = innerContent.ActualWidth;
        height = innerContent.ActualHeight;
      }
      else
      {
        width = sizeChangedEventArgs.NewSize.Width;
        height = sizeChangedEventArgs.NewSize.Height;
      }

      var radius = Math.Sqrt(Math.Pow(width, 2) + Math.Pow(height, 2));

      RippleSize = 2 * radius * RippleAttachedHelper.GetRippleSizeMultiplier(this);
    }

    private static readonly DependencyPropertyKey RippleSizePropertyKey;
    private static readonly DependencyPropertyKey RippleXPropertyKey;
    private static readonly DependencyPropertyKey RippleYPropertyKey;
    private static readonly HashSet<WpfRipple> PressedInstances;

    private const string TemplateStateNormal = "Normal";
    private const string TemplateStateMousePressed = "MousePressed";
    private const string TemplateStateMouseOut = "MouseOut";
  }

  internal static class RippleAttachedHelper
  {
    static RippleAttachedHelper()
    {
      ClipToBoundsProperty = DependencyProperty.RegisterAttached("ClipToBounds", typeof(bool), typeof(RippleAttachedHelper), new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.Inherits));
      IsCenteredProperty = DependencyProperty.RegisterAttached("IsCentered", typeof(bool), typeof(RippleAttachedHelper), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.Inherits));
      RippleSizeMultiplierProperty = DependencyProperty.RegisterAttached("RippleSizeMultiplier", typeof(double), typeof(RippleAttachedHelper), new FrameworkPropertyMetadata(1.0, FrameworkPropertyMetadataOptions.Inherits));
      FeedbackProperty = DependencyProperty.RegisterAttached("Feedback", typeof(Brush), typeof(RippleAttachedHelper), new FrameworkPropertyMetadata(default(Brush), FrameworkPropertyMetadataOptions.Inherits | FrameworkPropertyMetadataOptions.AffectsRender));
    }

    public static bool GetClipToBounds(DependencyObject element)
    {
      return (bool)element.GetValue(ClipToBoundsProperty);
    }
    public static void SetClipToBounds(DependencyObject element, bool value)
    {
      element.SetValue(ClipToBoundsProperty, value);
    }    
    public static void SetIsCentered(DependencyObject element, bool value)
    {
      element.SetValue(IsCenteredProperty, value);
    }
    public static bool GetIsCentered(DependencyObject element)
    {
      return (bool)element.GetValue(IsCenteredProperty);
    }
    public static double GetRippleSizeMultiplier(DependencyObject element)
    {
      return (double)element.GetValue(RippleSizeMultiplierProperty);
    }
    public static void SetRippleSizeMultiplier(DependencyObject element, double value)
    {
      element.SetValue(RippleSizeMultiplierProperty, value);
    }
    public static Brush GetFeedback(DependencyObject element)
    {
      return (Brush)element.GetValue(FeedbackProperty);
    }
    public static void SetFeedback(DependencyObject element, Brush value)
    {
      element.SetValue(FeedbackProperty, value);
    }  
        
    public static readonly DependencyProperty RippleSizeMultiplierProperty;
    public static readonly DependencyProperty ClipToBoundsProperty;
    public static readonly DependencyProperty IsCenteredProperty;
    public static readonly DependencyProperty FeedbackProperty;  
  }

  internal class ShadowConverter : IValueConverter
  {
    static ShadowConverter()
    {
      Instance = new ShadowConverter();

      ShadowEffectDictionary = new Dictionary<int, DropShadowEffect>
      {
        { 0, null },
        { 1, NewShadow(5, 1) },
        { 2, NewShadow(8, 1.5) },
        { 3, NewShadow(14, 4.5) },
        { 4, NewShadow(25, 8) },
        { 5, NewShadow(35, 13) }
      };
    }

    public static ShadowConverter Instance { get; private set; }

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (!(value is int))
        return null;

      return ShadowEffectDictionary[(int)value];
    }
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    private static DropShadowEffect NewShadow(double BlurRadius, double Depth)
    {
      var Result = new DropShadowEffect()
      {
        BlurRadius = BlurRadius,
        Color = Color.FromArgb(0xAA, 0x0, 0x0, 0x0),
        Direction = 270,
        Opacity = 0.42,
        RenderingBias = RenderingBias.Performance,
        ShadowDepth = Depth
      };
      Result.Freeze();

      return Result;
    }

    private static readonly IDictionary<int, DropShadowEffect> ShadowEffectDictionary;
  }

  internal static class ShadowAttachedHelper
  {
    static ShadowAttachedHelper()
    {
      DepthProperty = DependencyProperty.RegisterAttached("Depth", typeof(int), typeof(ShadowAttachedHelper), new FrameworkPropertyMetadata(0, FrameworkPropertyMetadataOptions.AffectsRender));
    }

    public static readonly DependencyProperty DepthProperty;

    public static int GetDepth(DependencyObject Element)
    {
      return (int)Element.GetValue(DepthProperty);
    }
    public static void SetDepth(DependencyObject Element, int Value)
    {
      Element.SetValue(DepthProperty, Value);
    }
  }

  internal class CalendarHeaderDisplay : ContentControl
  {
    static CalendarHeaderDisplay()
    {
      DisplayDateProperty = DependencyProperty.Register("DisplayDate", typeof(DateTime), typeof(CalendarHeaderDisplay), new PropertyMetadata(default(DateTime), DisplayDatePropertyChangedCallback));
    }

    public CalendarHeaderDisplay()
    {
      var LayoutStack = new System.Windows.Controls.StackPanel();
      this.Content = LayoutStack;
      LayoutStack.Orientation = System.Windows.Controls.Orientation.Vertical;

      this.YearBlock = new System.Windows.Controls.TextBlock();
      LayoutStack.Children.Add(YearBlock);
      YearBlock.FontSize = 12;

      this.DisplayBlock = new System.Windows.Controls.TextBlock();
      LayoutStack.Children.Add(DisplayBlock);
      DisplayBlock.FontSize = 18;

      SetCurrentValue(DisplayDateProperty, DateTime.Now.Date);
    }

    public DateTime DisplayDate
    {
      get { return (DateTime)GetValue(DisplayDateProperty); }
      set { SetValue(DisplayDateProperty, value); }
    }

    private void Update()
    {
      var CurrentDate = DisplayDate;

      YearBlock.Text = CurrentDate.Year.ToString();
      DisplayBlock.Text = CurrentDate.ToString("dddd, dd MMMM");
    }

    private static void DisplayDatePropertyChangedCallback(DependencyObject DependencyObject, DependencyPropertyChangedEventArgs Args)
    {
      ((CalendarHeaderDisplay)DependencyObject).Update();
    }

    private System.Windows.Controls.TextBlock YearBlock;
    private System.Windows.Controls.TextBlock DisplayBlock;

    public static readonly DependencyProperty DisplayDateProperty;
  }

  internal class CardClipConverter : IMultiValueConverter
  {
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      if (values.Length != 2 || !(values[0] is Size) || !(values[1] is Thickness))
        return Binding.DoNothing;

      var size = (System.Windows.Size)values[0];
      var farPoint = new System.Windows.Point(
          Math.Max(0, size.Width),
          Math.Max(0, size.Height));
      var padding = (System.Windows.Thickness)values[1];
      farPoint.Offset(padding.Left + padding.Right, padding.Top + padding.Bottom);

      return new System.Windows.Rect(
          new System.Windows.Point(),
          new System.Windows.Point(farPoint.X, farPoint.Y));
    }
    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      return null;
    }
  }

  internal sealed class CalendarDateCoalesceConverter : IMultiValueConverter
  {
    public object Convert(object[] ValueArray, Type TargetType, object Parameter, CultureInfo Culture)
    {
      if (ValueArray.Length != 2)
        throw new ArgumentException("Unexpected", "ValueArray");

      if (!(ValueArray[0] is DateTime))
        throw new ArgumentException("Unexpected", "ValueArray");

      if (ValueArray[1] != null && !(ValueArray[1] is DateTime?))
        throw new ArgumentException("Unexpected", "ValueArray");

      var SingleSelectedDate = (DateTime?)ValueArray[1];

      return SingleSelectedDate ?? ValueArray[0];
    }
    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      return null;
    }
  }

  public static class ShadowAssist
  {
    public static readonly DependencyProperty ShadowDepthProperty = DependencyProperty.RegisterAttached(
        "ShadowDepth", typeof(ShadowDepth), typeof(ShadowAssist), new FrameworkPropertyMetadata(default(ShadowDepth), FrameworkPropertyMetadataOptions.AffectsRender));

    public static void SetShadowDepth(DependencyObject element, ShadowDepth value)
    {
      element.SetValue(ShadowDepthProperty, value);
    }

    public static ShadowDepth GetShadowDepth(DependencyObject element)
    {
      return (ShadowDepth)element.GetValue(ShadowDepthProperty);
    }

    private static readonly DependencyPropertyKey LocalInfoPropertyKey = DependencyProperty.RegisterAttachedReadOnly(
        "LocalInfo", typeof(ShadowLocalInfo), typeof(ShadowAssist), new PropertyMetadata(default(ShadowLocalInfo)));

    private static void SetLocalInfo(DependencyObject element, ShadowLocalInfo value)
    {
      element.SetValue(LocalInfoPropertyKey, value);
    }

    private static ShadowLocalInfo GetLocalInfo(DependencyObject element)
    {
      return (ShadowLocalInfo)element.GetValue(LocalInfoPropertyKey.DependencyProperty);
    }

    public static readonly DependencyProperty DarkenProperty = DependencyProperty.RegisterAttached(
        "Darken", typeof(bool), typeof(ShadowAssist), new FrameworkPropertyMetadata(default(bool), FrameworkPropertyMetadataOptions.AffectsRender, DarkenPropertyChangedCallback));

    private static void DarkenPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
    {
      var uiElement = dependencyObject as UIElement;
      var dropShadowEffect = uiElement?.Effect as DropShadowEffect;

      if (dropShadowEffect == null) return;

      if ((bool)dependencyPropertyChangedEventArgs.NewValue)
      {
        SetLocalInfo(dependencyObject, new ShadowLocalInfo(dropShadowEffect.Opacity));

        var doubleAnimation = new DoubleAnimation(1, new Duration(TimeSpan.FromMilliseconds(350)))
        {
          FillBehavior = FillBehavior.HoldEnd
        };
        dropShadowEffect.BeginAnimation(DropShadowEffect.OpacityProperty, doubleAnimation);
      }
      else
      {
        var shadowLocalInfo = GetLocalInfo(dependencyObject);
        if (shadowLocalInfo == null) return;

        var doubleAnimation = new DoubleAnimation(shadowLocalInfo.StandardOpacity, new Duration(TimeSpan.FromMilliseconds(350)))
        {
          FillBehavior = FillBehavior.HoldEnd
        };
        dropShadowEffect.BeginAnimation(DropShadowEffect.OpacityProperty, doubleAnimation);
      }
    }

    public static void SetDarken(DependencyObject element, bool value)
    {
      element.SetValue(DarkenProperty, value);
    }

    public static bool GetDarken(DependencyObject element)
    {
      return (bool)element.GetValue(DarkenProperty);
    }

    public static readonly DependencyProperty ShadowEdgesProperty = DependencyProperty.RegisterAttached(
        "ShadowEdges", typeof(ShadowEdges), typeof(ShadowAssist), new PropertyMetadata(ShadowEdges.All));

    public static void SetShadowEdges(DependencyObject element, ShadowEdges value)
    {
      element.SetValue(ShadowEdgesProperty, value);
    }

    public static ShadowEdges GetShadowEdges(DependencyObject element)
    {
      return (ShadowEdges)element.GetValue(ShadowEdgesProperty);
    }
  }

  public enum ShadowDepth
  {
    Depth0,
    Depth1,
    Depth2,
    Depth3,
    Depth4,
    Depth5
  }

  [Flags]
  public enum ShadowEdges
  {
    None = 0,
    Left = 1,
    Top = 2,
    Right = 4,
    Bottom = 8,
    All = Left | Top | Right | Bottom
  }

  internal class ShadowLocalInfo
  {
    public ShadowLocalInfo(double standardOpacity)
    {
      StandardOpacity = standardOpacity;
    }

    public double StandardOpacity { get; }
  }

  public static class ToggleButtonAssist
  {
    private static readonly DependencyPropertyKey HasOnContentPropertyKey =
        DependencyProperty.RegisterAttachedReadOnly(
            "HasOnContent", typeof(bool), typeof(ToggleButtonAssist),
            new PropertyMetadata(false));

    public static readonly DependencyProperty HasOnContentProperty = HasOnContentPropertyKey.DependencyProperty;

    private static void SetHasOnContent(DependencyObject element, object value)
    {
      element.SetValue(HasOnContentPropertyKey, value);
    }

    /// <summary>
    /// Framework use only.
    /// </summary>
    /// <param name="element"></param>
    /// <returns></returns>
    public static bool GetHasOnContent(DependencyObject element)
    {
      return (bool)element.GetValue(HasOnContentProperty);
    }

    /// <summary>
    /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
    /// </summary>
    public static readonly DependencyProperty OnContentProperty = DependencyProperty.RegisterAttached(
        "OnContent", typeof(object), typeof(ToggleButtonAssist), new PropertyMetadata(default, OnContentPropertyChangedCallback));

    private static void OnContentPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
    {
      SetHasOnContent(dependencyObject, dependencyPropertyChangedEventArgs.NewValue != null);
    }

    /// <summary>
    /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
    /// </summary>
    /// <param name="element"></param>
    /// <param name="value"></param>
    public static void SetOnContent(DependencyObject element, object value)
    {
      element.SetValue(OnContentProperty, value);
    }

    /// <summary>
    /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
    /// </summary>
    public static object GetOnContent(DependencyObject element)
    {
      return (object)element.GetValue(OnContentProperty);
    }

    /// <summary>
    /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
    /// </summary>
    public static readonly DependencyProperty OnContentTemplateProperty = DependencyProperty.RegisterAttached(
        "OnContentTemplate", typeof(DataTemplate), typeof(ToggleButtonAssist), new PropertyMetadata(default(DataTemplate)));

    /// <summary>
    /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
    /// </summary>
    public static void SetOnContentTemplate(DependencyObject element, DataTemplate value)
    {
      element.SetValue(OnContentTemplateProperty, value);
    }

    /// <summary>
    /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
    /// </summary>
    public static DataTemplate GetOnContentTemplate(DependencyObject element)
    {
      return (DataTemplate)element.GetValue(OnContentTemplateProperty);
    }
  }
}
