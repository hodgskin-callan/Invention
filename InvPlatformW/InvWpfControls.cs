﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Reflection;
using Inv.Support;

namespace Inv
{
  internal sealed class WpfChrome
  {
    internal WpfChrome(WpfWindow Window)
    {
      this.Window = Window;
      this.ClipElementSet = [];
      this.Chrome = new System.Windows.Shell.WindowChrome()
      {
        ResizeBorderThickness = new System.Windows.Thickness(5),
        CaptionHeight = 32,
        CornerRadius = new System.Windows.CornerRadius(0),
        GlassFrameThickness = new System.Windows.Thickness(0)
      };

      Toggle(true);
    }

    public bool IsMouseOver { get; internal set; }
    public event Action MouseEnterEvent;
    public event Action MouseLeaveEvent;
    public int CaptionHeight
    {
      get => (int)Chrome.CaptionHeight;
      set => Chrome.CaptionHeight = value;
    }

    public void ClipCaption(System.Windows.FrameworkElement Element)
    {
      ClipElementSet.Add(Element);

      System.Windows.Shell.WindowChrome.SetIsHitTestVisibleInChrome(Element, true);
    }
    public void BindCaptionHeight(System.Windows.FrameworkElement Element)
    {
      Element.SizeChanged += (sender, e) =>
      {
        if (e.HeightChanged)
          Chrome.CaptionHeight = e.NewSize.Height;
      };
    }

    internal void Toggle(bool Value)
    {
      if (Value)
        System.Windows.Shell.WindowChrome.SetWindowChrome(Window, Chrome);
      else
        System.Windows.Shell.WindowChrome.SetWindowChrome(Window, null);
    }
    internal void SetMouseOver(bool Value)
    {
      if (IsMouseOver != Value)
      {
        if (Value || (!Value && !ClipElementSet.Any(E => E.IsMouseOver)))
        {
          this.IsMouseOver = Value;

          //Debug.WriteLine("Chrome mouse over: " + IsMouseOver);

          if (IsMouseOver)
            MouseEnterEvent?.Invoke();
          else
            MouseLeaveEvent?.Invoke();
        }
      }
    }

    private readonly WpfWindow Window;
    private readonly System.Windows.Shell.WindowChrome Chrome;
    private readonly HashSet<System.Windows.FrameworkElement> ClipElementSet;
  }

  internal sealed class WpfSound
  {
    internal WpfSound(dynamic IrrKlangSound)
    {
      this.IrrKlangSound = IrrKlangSound;
    }

    public void Stop() => IrrKlangSound.Stop();
    public void Pause() => IrrKlangSound.Paused = true;
    public void Resume() => IrrKlangSound.Paused = false;
    public void Modulate(Inv.AudioClip Clip)
    {
      IrrKlangSound.Volume = Clip.Volume;
      IrrKlangSound.Pan = -Clip.Pan; // irrKlang has an opposite convention?
      IrrKlangSound.PlaybackSpeed = Clip.Rate;
      IrrKlangSound.Looped = Clip.Looped;
    }

    private readonly dynamic IrrKlangSound; // IrrKlang.ISound
  }

  internal sealed class WpfSoundSource
  {
    internal WpfSoundSource(dynamic IrrKlangSoundSource)
    {
      this.IrrKlangSoundSource = IrrKlangSoundSource;
    }

    public string Name => IrrKlangSoundSource.Name;

    public void Dispose() => IrrKlangSoundSource.Dispose();
    public TimeSpan GetLength()
    {
      return TimeSpan.FromMilliseconds(IrrKlangSoundSource?.PlayLength ?? 0);
    }

    internal dynamic GetHandle() => IrrKlangSoundSource;

    private readonly dynamic IrrKlangSoundSource; // IrrKlang.ISoundSource
  }

  internal sealed class WpfSoundPlayer
  {
    private static readonly object WpfSoundLock = new object();
    private static System.Reflection.Assembly IrrKlangAssembly;

    internal WpfSoundPlayer()
    {
      lock (WpfSoundLock)
      {
        if (IrrKlangAssembly == null)
        {
          var ProcessIs32Bit = IntPtr.Size == 4;
          var AssemblyFolderPath = System.IO.Path.GetDirectoryName(typeof(WpfSoundPlayer).Assembly.Location);
          var IrrKlangFolderPath = System.IO.Path.Combine(AssemblyFolderPath, $"irrKlang-{(ProcessIs32Bit ? "x86" : "x64")}");

          // copy platform specific files to the assembly folder.
          var DotNetAssemblyName = "irrKlang.NET4.dll";
          var MissingFileNameList = new Inv.DistinctList<string>();

          foreach (var IrrKlangFileName in new[] { DotNetAssemblyName, "ikpFlac.dll", "ikpMP3.dll" })
          {
            var IrrKlangFilePath = System.IO.Path.Combine(IrrKlangFolderPath, IrrKlangFileName);
            var AssemblyFilePath = System.IO.Path.Combine(AssemblyFolderPath, IrrKlangFileName);

            if (System.IO.File.Exists(IrrKlangFilePath))
            {
              try
              {
                System.IO.File.Copy(IrrKlangFilePath, AssemblyFilePath, overwrite: true);
              }
              catch (System.IO.IOException)
              {
                // eg. file in use by another instance of the app process; as long as the expected file exists in the check below, continue.
              }
            }

            if (!System.IO.File.Exists(AssemblyFilePath))
              MissingFileNameList.Add(IrrKlangFilePath);
          }

          if (MissingFileNameList.Count > 0)
            throw new Exception("The following assembly files were not located:" + Environment.NewLine + MissingFileNameList.AsLineSeparatedText());

          IrrKlangAssembly = System.Reflection.Assembly.LoadFrom(System.IO.Path.Combine(AssemblyFolderPath, DotNetAssemblyName));
        }
      }

      this.IrrKlangSoundEngine = IrrKlangAssembly.CreateInstance("IrrKlang.ISoundEngine");
    }

    public WpfSound Play(Inv.Sound InvSound, float VolumeScale, float RateScale, float PanScale, bool Looped)
    {
      var IrrKlangSoundSource = GetSoundSource(InvSound).GetHandle();
      var IrrKlangSound = IrrKlangSoundEngine.Play2D(IrrKlangSoundSource, playLooped: false, startPaused: false, enableSoundEffects: true);

      if (IrrKlangSound == null)
        return null;

      IrrKlangSound.Volume = VolumeScale;
      IrrKlangSound.Pan = -PanScale; // irrKlang has an opposite convention?
      IrrKlangSound.PlaybackSpeed = RateScale;
      IrrKlangSound.Looped = Looped;

      return new WpfSound(IrrKlangSound);
    }
    public void Reclaim(Inv.Sound InvSound)
    {
      var WpfSoundSource = InvSound.Node as WpfSoundSource;

      if (WpfSoundSource == null || !IrrKlangSoundEngine.IsCurrentlyPlaying(WpfSoundSource.Name))
      {
        InvSound.Node = null;

        if (WpfSoundSource != null)
        {
          IrrKlangSoundEngine.RemoveSoundSource(WpfSoundSource.Name);
          WpfSoundSource.Dispose();
        }
      }
    }
    public WpfSoundSource GetSoundSource(Inv.Sound InvSound)
    {
      if (InvSound == null)
        return null;

      var WpfSoundSource = InvSound.Node as WpfSoundSource;

      if (WpfSoundSource == null)
      {
        var IrrKlangSoundSource = IrrKlangSoundEngine.AddSoundSourceFromMemory(InvSound.GetBuffer(), (SoundCount++).ToString());

        if (IrrKlangSoundSource != null)
        {
          WpfSoundSource = new WpfSoundSource(IrrKlangSoundSource);
          InvSound.Node = WpfSoundSource;
        }
      }

      return WpfSoundSource;
    }

    private readonly dynamic IrrKlangSoundEngine; // IrrKlang.ISoundEngine
    private int SoundCount; // never reset this because duplicates in AddSoundSourceFromMemory.
  }

  internal interface WpfOverrideFocusContract
  {
    void OverrideFocus();
  }

  public sealed class WpfSurface : System.Windows.Controls.Border
  {
    internal WpfSurface()
    {
      this.ClipToBounds = true;
    }
  }

  public sealed class WpfNative : WpfClipper
  {
    internal WpfNative()
    {
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }

    public void SetContent(System.Windows.FrameworkElement Element)
    {
      this.SafeSetContent(Element);
    }
  }

  public sealed class WpfButton : System.Windows.Controls.Button
  {
    internal WpfButton()
    {
      this.Background = null;
      this.Margin = new System.Windows.Thickness(0);
      this.Padding = new System.Windows.Thickness(0);
      this.BorderThickness = new System.Windows.Thickness(0); // while this seems redundant (when viewing in the debugger), it actually removes a 1dp border.
      this.Focusable = false;
      this.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Stretch;
      this.VerticalContentAlignment = System.Windows.VerticalAlignment.Stretch;
      this.Style = DefaultStyle;
      this.IsEnabledChanged += (Sender, Event) => RefreshInvoke();
    }

    public System.Windows.CornerRadius CornerRadius
    {
      get => (System.Windows.CornerRadius)GetValue(CornerRadiusProperty);
      set => SetValue(CornerRadiusProperty, value);
    }
    public event Action LeftClick;
    public event Action RightClick;
    public event Action PressClick;
    public event Action ReleaseClick;

    public void SetContent(System.Windows.FrameworkElement Element)
    {
      this.SafeSetContent(Element);
    }
    public void AsFlat(System.Windows.Media.Brush Normal, System.Windows.Media.Brush Hovered, System.Windows.Media.Brush Pushed)
    {
      void RefreshAsFlat()
      {
        if (IsPressed)
          this.Background = Pushed;
        else if (IsMouseOver || IsFocused)
          this.Background = Hovered;
        else
          this.Background = Normal;

        this.Opacity = IsEnabled ? 1.00F : 0.50F;
      }

      RefreshAsFlat();

      this.RefreshDelegate = RefreshAsFlat;
    }

    protected override void OnGotFocus(System.Windows.RoutedEventArgs e)
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      base.OnGotFocus(e);

      RefreshInvoke();

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    protected override void OnLostFocus(System.Windows.RoutedEventArgs e)
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      base.OnLostFocus(e);

      RefreshInvoke();

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    protected override void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      base.OnMouseEnter(e);

      RefreshInvoke();

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    protected override void OnMouseLeave(System.Windows.Input.MouseEventArgs e)
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      base.OnMouseLeave(e);

      RefreshInvoke();

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    protected override void OnMouseMove(System.Windows.Input.MouseEventArgs e)
    {
      base.OnMouseMove(e);

      // If the button has captured the mouse, and the pointer is outside the button, we want to set IsPressed to false.
      // NOTE: the SPACEBAR key will invoke mousemouse when it depresses the button, so we are looking for any mouse button to be pressed.
      if (this.IsMouseCaptured && (e.LeftButton == System.Windows.Input.MouseButtonState.Pressed || e.RightButton == System.Windows.Input.MouseButtonState.Pressed || e.XButton1 == System.Windows.Input.MouseButtonState.Pressed || e.XButton2 == System.Windows.Input.MouseButtonState.Pressed))
      {
        var HitIsInside = false;

        System.Windows.Media.VisualTreeHelper.HitTest(
            this,
            d =>
            {
              if (d == this)
                HitIsInside = true;

              return System.Windows.Media.HitTestFilterBehavior.Stop;
            },
            ht => System.Windows.Media.HitTestResultBehavior.Stop, new System.Windows.Media.PointHitTestParameters(e.GetPosition(this)));

        base.IsPressed = HitIsInside;
      }
    }
    protected override void OnMouseLeftButtonDown(System.Windows.Input.MouseButtonEventArgs e)
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      base.OnMouseLeftButtonDown(e);

      base.IsPressed = true;

      PressInvoke();
      RefreshInvoke();

      CaptureMouse();
      this.IsLeftClicked = true;

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    protected override void OnMouseLeftButtonUp(System.Windows.Input.MouseButtonEventArgs e)
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      base.OnMouseLeftButtonUp(e);

      base.IsPressed = false;

      if (IsLeftClicked)
      {
        if (this.IsMouseOver)
          LeftClick?.Invoke();

        this.IsLeftClicked = false;
      }

      base.IsPressed = false;

      ReleaseInvoke();
      RefreshInvoke();

      e.Handled = true;

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    protected override void OnMouseRightButtonDown(System.Windows.Input.MouseButtonEventArgs e)
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      base.OnMouseRightButtonDown(e);

      base.IsPressed = true;

      PressInvoke();
      RefreshInvoke();

      CaptureMouse();
      this.IsRightClicked = true;

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    protected override void OnMouseRightButtonUp(System.Windows.Input.MouseButtonEventArgs e)
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      base.OnMouseRightButtonUp(e);

      ReleaseMouseCapture();

      if (IsRightClicked)
      {
        if (this.IsMouseOver)
          RightClick?.Invoke();

        this.IsRightClicked = false;
      }

      base.IsPressed = false;

      ReleaseInvoke();
      RefreshInvoke();

      e.Handled = true;

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }
    protected override void OnClick()
    {
      if (TraceClass.IsActive)
        TraceClass.Enter();

      base.OnClick();

      // NOTE: this handles non-mouse clicks such as the ENTER key.
      if (!IsLeftClicked)
      {
        PressInvoke();

        LeftClick?.Invoke();

        ReleaseInvoke();
      }

      if (TraceClass.IsActive)
        TraceClass.Leave();
    }

    private void PressInvoke()
    {
      PressClick?.Invoke();
    }
    private void ReleaseInvoke()
    {
      ReleaseClick?.Invoke();
    }
    private void RefreshInvoke()
    {
      RefreshDelegate?.Invoke();
    }

    private Action RefreshDelegate;
    private bool IsLeftClicked;
    private bool IsRightClicked;

    static WpfButton()
    {
      CornerRadiusProperty = System.Windows.DependencyProperty.Register("CornerRadius", typeof(System.Windows.CornerRadius), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(new System.Windows.CornerRadius(0), System.Windows.FrameworkPropertyMetadataOptions.AffectsArrange | System.Windows.FrameworkPropertyMetadataOptions.AffectsMeasure));

      var ResourceDictionary = WpfShell.LoadResourceDictionary("InvWpfButton.xaml");

      DefaultStyle = (System.Windows.Style)ResourceDictionary["InvDefaultButton"];
      DefaultStyle.Seal();
    }

    public static readonly System.Windows.DependencyProperty CornerRadiusProperty;

    private static readonly Inv.TraceClass TraceClass = Inv.Trace.NewClass<WpfButton>(Inv.Trace.PlatformFeature);
    private static readonly System.Windows.Style DefaultStyle;
  }

  [System.Windows.Data.ValueConversion(typeof(System.Windows.CornerRadius), typeof(System.Windows.CornerRadius))]
  internal sealed class InnerCornerRadiusConverter : System.Windows.Data.IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      var InputRadius = (System.Windows.CornerRadius)value;

      return new System.Windows.CornerRadius(Math.Max(0, InputRadius.TopLeft - 1), Math.Max(0, InputRadius.TopRight - 1), Math.Max(0, InputRadius.BottomRight - 1), Math.Max(0, InputRadius.BottomLeft - 1));
    }
    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      return null;
    }
  }

  public sealed class WpfScroll : WpfClipper
  {
    internal WpfScroll()
    {
      this.Inner = new WpfScrollViewer();
      Inner.KeyboardScrollingDisabled = true;
      Inner.PanningMode = System.Windows.Controls.PanningMode.Both;
      Inner.CanContentScroll = false;
      Inner.Focusable = false;

      base.Child = Inner;

      Inner.PreviewMouseWheel += (Sender, Event) =>
      {
        if (IsHorizontalField == true)
        {
          if (Event.Delta < 0)
            Inner.LineRight();
          else
            Inner.LineLeft();
        }
      };
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }

    public System.Windows.FrameworkElement Content
    {
      get => (System.Windows.FrameworkElement)Inner.Content;
      set => Inner.SafeSetContent(value);
    }
    public bool IsScrollViewerDark
    {
      get => Inner.IsDark;
      set => Inner.IsDark = value;
    }
    public System.Windows.Controls.ScrollBarVisibility VerticalScrollBarVisibility
    {
      get => Inner.VerticalScrollBarVisibility;
      set => Inner.VerticalScrollBarVisibility = value;
    }
    public System.Windows.Controls.ScrollBarVisibility HorizontalScrollBarVisibility
    {
      get => Inner.HorizontalScrollBarVisibility;
      set => Inner.HorizontalScrollBarVisibility = value;
    }
    public bool MousePanningEnabled
    {
      set => Inner.MousePanningEnabled = value;
    }
    public double ViewportWidth => Inner.ViewportWidth;
    public double ViewportHeight => Inner.ViewportHeight;
    public double HorizontalOffset => Inner.HorizontalOffset;
    public double VerticalOffset => Inner.VerticalOffset;
    public event System.Windows.Controls.ScrollChangedEventHandler ScrollChangedEvent
    {
      add => Inner.ScrollChanged += value;
      remove => Inner.ScrollChanged -= value;
    }
    public System.Windows.Controls.PanningMode PanningMode
    {
      get => Inner.PanningMode;
      set => Inner.PanningMode = value;
    }
    public System.Windows.Controls.Primitives.ScrollBar VerticalScrollBar => Inner.VerticalScrollBar;
    public System.Windows.Controls.Primitives.ScrollBar HorizontalScrollBar => Inner.HorizontalScrollBar;

    public void LineLeft() => Inner.LineLeft();
    public void LineRight() => Inner.LineRight();
    public void ScrollToTop() => Inner.ScrollToTop();
    public void ScrollToBottom() => Inner.ScrollToBottom();
    public void ScrollToLeftEnd() => Inner.ScrollToLeftEnd();
    public void ScrollToRightEnd() => Inner.ScrollToRightEnd();
    public void ScrollToVerticalOffset(double Offset) => Inner.ScrollToVerticalOffset(Offset);
    public void ScrollToHorizontalOffset(double Offset) => Inner.ScrollToHorizontalOffset(Offset);

    public void SetOrientation(bool IsHorizontal)
    {
      if (IsHorizontal != IsHorizontalField)
      {
        this.IsHorizontalField = IsHorizontal;

        if (IsHorizontal)
        {
          Inner.VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Disabled;
          Inner.HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto;
          Inner.PanningMode = System.Windows.Controls.PanningMode.HorizontalOnly;
        }
        else
        {
          Inner.VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto;
          Inner.HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Disabled;
          Inner.PanningMode = System.Windows.Controls.PanningMode.VerticalOnly;
        }
      }
    }

    public void ScrollViewerApplyTemplate() => Inner.ApplyTemplate();

    private readonly WpfScrollViewer Inner;
    private bool? IsHorizontalField;
  }

  public sealed class WpfScrollViewer : System.Windows.Controls.ScrollViewer
  {
    public WpfScrollViewer()
    {
      this.Style = FlatScrollViewerStyle;
      this.PreviewMouseWheel += PreviewMouseWheelHandler;
    }

    public static readonly System.Windows.Style FlatScrollBarStyle;

    public bool KeyboardScrollingDisabled { get; set; }
    public bool MousePanningEnabled { get; set; }
    public bool IsDark
    {
      get => (bool)GetValue(IsDarkProperty);
      set => SetValue(IsDarkProperty, value);
    }

    public override void OnApplyTemplate()
    {
      base.OnApplyTemplate();

      if (Template != null)
      {
        VerticalScrollBar = Template.FindName("PART_VerticalScrollBar", this) as System.Windows.Controls.Primitives.ScrollBar;
        HorizontalScrollBar = Template.FindName("PART_HorizontalScrollBar", this) as System.Windows.Controls.Primitives.ScrollBar;
      }
    }

    protected override void OnMouseMove(System.Windows.Input.MouseEventArgs e)
    {
      base.OnMouseMove(e);

      if (MousePanningEnabled && IsMouseCaptured)
      {
        var CurrentPosition = e.GetPosition(this);

        var Delta = new System.Windows.Point();
        Delta.X = (CurrentPosition.X > PanStartPosition.X) ? -(CurrentPosition.X - PanStartPosition.X) : (PanStartPosition.X - CurrentPosition.X);
        Delta.Y = (CurrentPosition.Y > PanStartPosition.Y) ? -(CurrentPosition.Y - PanStartPosition.Y) : (PanStartPosition.Y - CurrentPosition.Y);

        ScrollToHorizontalOffset(PanStartOffset.X + Delta.X);
        ScrollToVerticalOffset(PanStartOffset.Y + Delta.Y);
      }
    }
    protected override void OnPreviewMouseDown(System.Windows.Input.MouseButtonEventArgs e)
    {
      base.OnPreviewMouseDown(e);

      if (MousePanningEnabled && IsMouseOver)
      {
        PanStartPosition = e.GetPosition(this);
        PanStartOffset.X = HorizontalOffset;
        PanStartOffset.Y = VerticalOffset;

        Cursor = System.Windows.Input.Cursors.ScrollAll;
        CaptureMouse();
      }
    }
    protected override void OnPreviewMouseUp(System.Windows.Input.MouseButtonEventArgs e)
    {
      base.OnPreviewMouseUp(e);

      if (MousePanningEnabled && IsMouseCaptured)
      {
        Cursor = System.Windows.Input.Cursors.Arrow;
        ReleaseMouseCapture();
      }
    }
    protected override void OnManipulationBoundaryFeedback(System.Windows.Input.ManipulationBoundaryFeedbackEventArgs e)
    {
      e.Handled = true; // prevent weird scrolling 'window bounce' when using touch input.
    }
    protected override void OnKeyDown(System.Windows.Input.KeyEventArgs e)
    {
      if (KeyboardScrollingDisabled)
      {
        // workaround because the scrollviewer will steal up/down/left/right to scroll the content, rather than changing focus on the content inside the scrollviewer.
        var Element = e.OriginalSource as System.Windows.FrameworkElement;
        var Direction = GetDirection(e.Key);

        if (Element != null && Direction != null)
          e.Handled = Element.MoveFocus(new System.Windows.Input.TraversalRequest(Direction.Value));
      }

      base.OnKeyDown(e);
    }
    protected override void OnPreviewKeyDown(System.Windows.Input.KeyEventArgs e)
    {
      base.OnPreviewKeyDown(e);
    }

    internal System.Windows.Controls.Primitives.ScrollBar VerticalScrollBar { get; private set; }
    internal System.Windows.Controls.Primitives.ScrollBar HorizontalScrollBar { get; private set; }

    private System.Windows.Input.FocusNavigationDirection? GetDirection(System.Windows.Input.Key Key)
    {
      var Result = (System.Windows.Input.FocusNavigationDirection?)null;

      if (Key == System.Windows.Input.Key.Up)
        Result = System.Windows.Input.FocusNavigationDirection.Up;
      else if (Key == System.Windows.Input.Key.Down)
        Result = System.Windows.Input.FocusNavigationDirection.Down;
      else if (Key == System.Windows.Input.Key.Left)
        Result = System.Windows.Input.FocusNavigationDirection.Left;
      else if (Key == System.Windows.Input.Key.Right)
        Result = System.Windows.Input.FocusNavigationDirection.Right;
      // these don't seem to do anything for MoveFocus, but it stops the scrollviewer from 'eating' the keys.
      else if (Key == System.Windows.Input.Key.Home)
        Result = System.Windows.Input.FocusNavigationDirection.First;
      else if (Key == System.Windows.Input.Key.End)
        Result = System.Windows.Input.FocusNavigationDirection.Last;
      else if (Key == System.Windows.Input.Key.PageUp)
        Result = System.Windows.Input.FocusNavigationDirection.Previous;
      else if (Key == System.Windows.Input.Key.PageDown)
        Result = System.Windows.Input.FocusNavigationDirection.Next;

      return Result;
    }

    private System.Windows.Point PanStartPosition;
    private System.Windows.Point PanStartOffset;

    static WpfScrollViewer()
    {
      IsDarkProperty = System.Windows.DependencyProperty.Register("IsDark", typeof(bool), typeof(WpfScrollViewer), new System.Windows.FrameworkPropertyMetadata(false));

      var ResourceDictionary = WpfShell.LoadResourceDictionary("InvWpfScrollViewer.xaml");

      if (ResourceDictionary != null)
      {
        FlatScrollViewerStyle = (System.Windows.Style)ResourceDictionary["FlatScrollViewerStyle"];
        FlatScrollViewerStyle.Seal();

        FlatScrollBarStyle = (System.Windows.Style)ResourceDictionary["FlatScrollBarStyle"];
        FlatScrollBarStyle.Seal();
      }
    }

    internal static readonly System.Windows.DependencyProperty IsDarkProperty;
    internal static readonly System.Windows.Style FlatScrollViewerStyle;

    private static void PreviewMouseWheelHandler(object Sender, System.Windows.Input.MouseWheelEventArgs e)
    {
      var ScrollViewer = Sender as System.Windows.Controls.ScrollViewer;

      Debug.Assert(ScrollViewer != null, "Sender is not assigned or is not a ScrollViewer");

      if (!e.Handled && !Inv.WpfFoundation.MouseWheelEventArgsList.Contains(e))
      {
        var PreviewEventArg = new System.Windows.Input.MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta)
        {
          RoutedEvent = System.Windows.UIElement.PreviewMouseWheelEvent,
          Source = Sender
        };

        var OriginalSource = e.OriginalSource as System.Windows.UIElement;

        if (OriginalSource != null)
        {
          Inv.WpfFoundation.MouseWheelEventArgsList.Add(PreviewEventArg);
          OriginalSource.RaiseEvent(PreviewEventArg);
          Inv.WpfFoundation.MouseWheelEventArgsList.Remove(PreviewEventArg);
        }

        e.Handled = PreviewEventArg.Handled;

        if (!e.Handled)
        {
          var Delta = e.Delta;

          if ((Delta > 0 && ScrollViewer.VerticalOffset == 0) || (Delta <= 0 && ScrollViewer.VerticalOffset >= ScrollViewer.ExtentHeight - ScrollViewer.ViewportHeight))
          {
            var LogicalParent = (System.Windows.UIElement)((System.Windows.FrameworkElement)Sender).Parent;
            if (LogicalParent != null)
            {
              var EventArgs = new System.Windows.Input.MouseWheelEventArgs(e.MouseDevice, e.Timestamp, Delta)
              {
                RoutedEvent = System.Windows.UIElement.MouseWheelEvent,
                Source = Sender
              };

              LogicalParent.RaiseEvent(EventArgs);

              e.Handled = EventArgs.Handled;
            }
          }
        }
      }
    }
  }

  internal sealed class WpfDrawing : Inv.DrawContract
  {
    internal WpfDrawing(WpfHost WpfHost)
    {
      this.WpfHost = WpfHost;

      // reused for memory management performance.
      this.WpfRectangleRect = new System.Windows.Rect();
      this.WpfImageRect = new System.Windows.Rect();

      this.WpfDrawingVisual = new System.Windows.Media.DrawingVisual();

      this.PenDictionary = [];
      this.LookupPenKey = new PenKey();
    }

    public void Start()
    {
      this.WpfDrawingContext = WpfDrawingVisual.RenderOpen();
      this.WpfDrawingTickCount = Environment.TickCount;
    }
    public void Stop()
    {
      WpfDrawingContext.Close();
    }

    internal System.Windows.Media.DrawingContext WpfDrawingContext { get; private set; }

    void DrawContract.Mask(Inv.Rect ViewportRect, Action<Inv.DrawMask> MaskAction)
    {
      var WpfDrawMask = new WpfDrawMask(this, ViewportRect);
      MaskAction?.Invoke(WpfDrawMask);
      WpfDrawingContext.PushOpacityMask(WpfDrawMask.CompleteBrush());
    }
    void DrawContract.Unmask()
    {
      WpfDrawingContext.Pop();
    }
    void DrawContract.DrawText(string TextFragment, Inv.DrawFont TextFont, Point TextPoint, HorizontalPosition TextHorizontal, VerticalPosition TextVertical, int MaximumTextWidth, int MaximumTextHeight)
    {
      var WpfFormattedText = WpfHost.NewFormattedText(TextFragment, TextFont, MaximumTextWidth, MaximumTextHeight);

      var WpfTextPoint = WpfHost.TranslatePoint(TextPoint);

      if (TextHorizontal != HorizontalPosition.Left)
      {
        var TextWidth = (int)WpfFormattedText.Width;

        if (TextHorizontal == HorizontalPosition.Right)
          WpfTextPoint.X -= TextWidth;
        else if (TextHorizontal == HorizontalPosition.Center)
          WpfTextPoint.X -= TextWidth / 2;
      }

      if (TextVertical != VerticalPosition.Top)
      {
        var TextHeight = (int)WpfFormattedText.Height;

        if (TextVertical == VerticalPosition.Bottom)
          WpfTextPoint.Y -= TextHeight;
        else if (TextVertical == VerticalPosition.Center)
          WpfTextPoint.Y -= TextHeight / 2;
      }

      WpfDrawingContext.DrawText(WpfFormattedText, WpfTextPoint);
    }
    void DrawContract.DrawLine(Inv.Colour LineStrokeColour, int LineStrokeThickness, Inv.LineJoin LineJoin, Inv.LineCap LineCap, Inv.Point LineSourcePoint, Inv.Point LineTargetPoint, params Inv.Point[] LineExtraPointArray)
    {
      if (LineStrokeThickness <= 0 || LineStrokeColour == Inv.Colour.Transparent)
        return;

      var PointPen = TranslatePen(LineStrokeColour, LineStrokeThickness, WpfHost.TranslateLineJoin(LineJoin), WpfHost.TranslateLineCap(LineCap));

      // In WPF, when you draw a line, the line is centered on the coordinates you specify. 
      var StrokeWidth = 0.5; // LineStrokeThickness / 2.0;

      var SourcePoint = WpfHost.TranslatePoint(LineSourcePoint);
      SourcePoint.X += StrokeWidth;
      SourcePoint.Y += StrokeWidth;

      var CurrentPoint = WpfHost.TranslatePoint(LineTargetPoint);
      CurrentPoint.X += StrokeWidth;
      CurrentPoint.Y += StrokeWidth;
      WpfDrawingContext.DrawLine(PointPen, SourcePoint, CurrentPoint);

      for (var Index = 0; Index < LineExtraPointArray.Length; Index++)
      {
        var TargetPoint = WpfHost.TranslatePoint(LineExtraPointArray[Index]);
        TargetPoint.X += StrokeWidth;
        TargetPoint.Y += StrokeWidth;
        WpfDrawingContext.DrawLine(PointPen, CurrentPoint, TargetPoint);

        CurrentPoint = TargetPoint;
      }
    }
    void DrawContract.DrawRectangle(Colour RectangleFillColour, Colour RectangleStrokeColour, int RectangleStrokeThickness, Rect RectangleRect)
    {
      WpfHost.TranslateRect(RectangleRect, ref WpfRectangleRect);

      if (RectangleStrokeThickness > 0)
      {
        var StrokeWidth = RectangleStrokeThickness / 2.0;
        WpfRectangleRect.X += StrokeWidth;
        WpfRectangleRect.Y += StrokeWidth;

        if (WpfRectangleRect.Width >= RectangleStrokeThickness)
          WpfRectangleRect.Width -= RectangleStrokeThickness;

        if (WpfRectangleRect.Height >= RectangleStrokeThickness)
          WpfRectangleRect.Height -= RectangleStrokeThickness;
      }
      /*
      if (StrokeWidth > 0)
      {
        var Guidelines = new System.Windows.Media.GuidelineSet();
        Guidelines.GuidelinesX.Add(WpfRectangleRect.Left + StrokeWidth);
        Guidelines.GuidelinesX.Add(WpfRectangleRect.Right + StrokeWidth);
        Guidelines.GuidelinesY.Add(WpfRectangleRect.Top + StrokeWidth);
        Guidelines.GuidelinesY.Add(WpfRectangleRect.Bottom + StrokeWidth);
        WpfContext.PushGuidelineSet(Guidelines);
      }*/

      WpfDrawingContext.DrawRectangle(
        WpfHost.TranslateBrush(RectangleFillColour),
        TranslatePen(RectangleStrokeColour, RectangleStrokeThickness),
        WpfRectangleRect);

      //if (StrokeWidth > 0)
      //  WpfContext.Pop();
    }
    void DrawContract.DrawEllipse(Colour EllipseFillColour, Colour EllipseStrokeColour, int EllipseStrokeThickness, Point EllipseCenter, Point EllipseRadius)
    {
      WpfDrawingContext.DrawEllipse(
        WpfHost.TranslateBrush(EllipseFillColour),
        TranslatePen(EllipseStrokeColour, EllipseStrokeThickness),
        WpfHost.TranslatePoint(EllipseCenter), EllipseRadius.X, EllipseRadius.Y);
    }
    void DrawContract.DrawArc(Inv.Colour ArcFillColour, Inv.Colour ArcStrokeColour, int ArcStrokeThickness, Inv.Point ArcCenter, Inv.Point ArcRadius, float StartAngle, float SweepAngle)
    {
      var MaxWidth = Math.Max(0.0, (ArcRadius.X * 2) - ArcStrokeThickness);
      var MaxHeight = Math.Max(0.0, (ArcRadius.Y * 2) - ArcStrokeThickness);

      var StartX = ArcRadius.X * Math.Cos(StartAngle * Math.PI / 180.0);
      var StartY = ArcRadius.Y * Math.Sin(StartAngle * Math.PI / 180.0);

      var EndX = ArcRadius.X * Math.Cos(SweepAngle * Math.PI / 180.0);
      var EndY = ArcRadius.Y * Math.Sin(SweepAngle * Math.PI / 180.0);

      var WpfGeometry = new System.Windows.Media.PathGeometry();

      var WpfFigure = new System.Windows.Media.PathFigure
      {
        StartPoint = new System.Windows.Point(ArcCenter.X + StartX, ArcCenter.Y - StartY),
        IsFilled = true,
        IsClosed = true
      };
      WpfFigure.Segments.Add(new System.Windows.Media.ArcSegment
      {
        Point = new System.Windows.Point(ArcCenter.X + EndX, ArcCenter.Y - EndY),
        Size = new System.Windows.Size(MaxWidth / 2.0, MaxHeight / 2),
        RotationAngle = 0.0,
        IsLargeArc = (SweepAngle - StartAngle) > 180,
        SweepDirection = System.Windows.Media.SweepDirection.Counterclockwise,
        IsStroked = true,
        IsSmoothJoin = false
      });
      WpfFigure.Segments.Add(new System.Windows.Media.LineSegment
      {
        Point = new System.Windows.Point(ArcCenter.X, ArcCenter.Y),
        IsStroked = true,
        IsSmoothJoin = false
      });
      WpfFigure.Freeze();

      WpfGeometry.Figures.Add(WpfFigure);
      WpfGeometry.Freeze();

      WpfDrawingContext.DrawGeometry(WpfHost.TranslateBrush(ArcFillColour), TranslatePen(ArcStrokeColour, ArcStrokeThickness), WpfGeometry);
    }
    void DrawContract.DrawImage(Inv.Image ImageSource, Inv.Rect ImageRect, float ImageOpacity, Inv.Colour ImageTintColour, Inv.Mirror? ImageMirror, Inv.Rotation? ImageRotation)
    {
      WpfHost.TranslateRect(ImageRect, ref WpfImageRect);

      if (ImageOpacity != 1.0F)
      {
        // NOTE: WpfContext.PushOpacity performs terribly if you push/pop in quick succession (this is a workaround).
        var WpfTintBrush = WpfHost.TranslateBrush(Inv.Colour.White.Opacity(ImageOpacity));
        WpfDrawingContext.PushOpacityMask(WpfTintBrush);
      }

      if (ImageRotation != null)
      {
        var ImageAngle = ImageRotation.Value.Angle;
        var ImagePivot = ImageRotation.Value.LocatePoint(ImageRect);

        // NOTE: cannot reuse these transforms with different values in the one draw phase.
        var WpfRotateTransform = new System.Windows.Media.RotateTransform(ImageAngle, ImagePivot.X, ImagePivot.Y);
        WpfRotateTransform.Freeze();
        WpfDrawingContext.PushTransform(WpfRotateTransform);
      }

      if (ImageMirror != null)
      {
        WpfDrawingContext.PushTransform(WpfHost.MirrorTransformArray[ImageMirror.Value]);

        if (ImageMirror.Value == Mirror.Horizontal || ImageMirror.Value == Mirror.HorizontalAndVertical)
          WpfImageRect.X = -WpfImageRect.X - WpfImageRect.Width;

        if (ImageMirror.Value == Mirror.Vertical || ImageMirror.Value == Mirror.HorizontalAndVertical)
          WpfImageRect.Y = -WpfImageRect.Y - WpfImageRect.Height;
      }

      var WpfImage = WpfHost.TranslateImage(ImageSource);

      var WpfBitmapSource = WpfImage.FirstBitmapSource;

      if (WpfImage.IndividualFrames != null)
      {
        var TargetAnimationDelayMS = WpfDrawingTickCount % WpfImage.TotalAnimationDelayMS;
        foreach (var IndividualFrame in WpfImage.IndividualFrames)
        {
          if (IndividualFrame.AnimationKeytimeMS >= TargetAnimationDelayMS)
          {
            WpfBitmapSource = IndividualFrame.BitmapSource;
            break;
          }
        }
      }

      WpfDrawingContext.DrawImage(WpfBitmapSource, WpfImageRect);

      if (ImageTintColour != null)
      {
        var WpfTintBrush = WpfHost.TranslateBrush(ImageTintColour);

        var WpfImageBrush = new System.Windows.Media.ImageBrush(WpfBitmapSource);
        WpfImageBrush.Freeze();

        WpfDrawingContext.PushOpacityMask(WpfImageBrush);
        WpfDrawingContext.DrawRectangle(WpfTintBrush, null, WpfImageRect);
        WpfDrawingContext.Pop();
      }

      if (ImageMirror != null)
        WpfDrawingContext.Pop();

      if (ImageRotation != null)
        WpfDrawingContext.Pop();

      if (ImageOpacity != 1.0F)
        WpfDrawingContext.Pop();
    }
    void DrawContract.DrawPolygon(Colour FillColour, Inv.Colour StrokeColour, int StrokeThickness, Inv.LineJoin LineJoin, bool IsClosed, Inv.Point StartPoint, params Inv.Point[] PointArray)
    {
      var PathGeometry = new System.Windows.Media.PathGeometry();
      var PathFigure = new System.Windows.Media.PathFigure()
      {
        StartPoint = WpfHost.TranslatePoint(StartPoint),
        IsClosed = IsClosed
      };
      PathFigure.Segments.Add(new System.Windows.Media.PolyLineSegment(PointArray.Select(P => WpfHost.TranslatePoint(P)).ToList(), true));
      PathFigure.Freeze();
      PathGeometry.Figures.Add(PathFigure);
      PathGeometry.Freeze();

      var PathBrush = WpfHost.TranslateBrush(FillColour);
      var PathPen = TranslatePen(StrokeColour, StrokeThickness, WpfHost.TranslateLineJoin(LineJoin));
      WpfDrawingContext.DrawGeometry(PathBrush, PathPen, PathGeometry);
    }

    internal readonly WpfHost WpfHost;
    internal readonly System.Windows.Media.DrawingVisual WpfDrawingVisual;

    internal System.Windows.Media.Pen TranslatePen(Inv.Colour Colour, int Thickness, System.Windows.Media.PenLineJoin LineJoin = System.Windows.Media.PenLineJoin.Miter, System.Windows.Media.PenLineCap LineCap = System.Windows.Media.PenLineCap.Round)
    {
      if (Colour == null || Thickness <= 0)
        return null;

      LookupPenKey.Colour = Colour;
      LookupPenKey.Thickness = Thickness;
      LookupPenKey.LineJoin = LineJoin;
      LookupPenKey.LineCap = LineCap;

      return PenDictionary.GetOrAdd(LookupPenKey, C =>
      {
        var Result = new System.Windows.Media.Pen(WpfHost.TranslateBrush(Colour), Thickness)
        {
          LineJoin = LineJoin,
          StartLineCap = LineCap,
          EndLineCap = LineCap
        };
        Result.Freeze();
        return Result;
      });
    }

    private readonly Dictionary<PenKey, System.Windows.Media.Pen> PenDictionary;
    private PenKey LookupPenKey;
    private System.Windows.Rect WpfRectangleRect;
    private System.Windows.Rect WpfImageRect;
    private int WpfDrawingTickCount;

    private struct PenKey
    {
      public Inv.Colour Colour;
      public int Thickness;
      public System.Windows.Media.PenLineJoin LineJoin;
      public System.Windows.Media.PenLineCap LineCap;

      public override int GetHashCode()
      {
        return Colour.GetHashCode() ^ Thickness.GetHashCode() ^ LineJoin.GetHashCode() ^ LineCap.GetHashCode();
      }
      public override bool Equals(object obj)
      {
        var Key = (PenKey)obj;

        return
          Key.Thickness == Thickness &&
          Key.Colour == Colour &&
          Key.LineJoin == LineJoin &&
          Key.LineCap == LineCap;
      }
    }
  }

  internal sealed class WpfDrawMask : Inv.DrawMask
  {
    internal WpfDrawMask(WpfDrawing WpfDrawing, Inv.Rect ViewportRect)
    {
      this.WpfDrawing = WpfDrawing;
      this.WpfViewportRect = new System.Windows.Rect(ViewportRect.Left, ViewportRect.Top, ViewportRect.Width, ViewportRect.Height);
      this.WpfDrawingGroup = new System.Windows.Media.DrawingGroup();

      // anchor shape for the viewport is necessary if a rectangle isn't applied to the whole viewport.
      var WpfRectangleGeometry = new System.Windows.Media.RectangleGeometry();
      WpfRectangleGeometry.Rect = WpfViewportRect;
      WpfRectangleGeometry.Freeze();
      
      var WpfGeometryDrawing = new System.Windows.Media.GeometryDrawing(System.Windows.Media.Brushes.Transparent, null, WpfRectangleGeometry);
      WpfGeometryDrawing.Freeze();
      WpfDrawingGroup.Children.Add(WpfGeometryDrawing);

      // clip to the viewport to prevent weirdness.
      WpfDrawingGroup.ClipGeometry = WpfRectangleGeometry;
    }

    void DrawMask.DrawImage(Inv.Rect Rect, Inv.Image Image, float Opacity, Inv.Mirror? Mirror)
    {
      var WpfImage = WpfDrawing.WpfHost.TranslateImage(Image);
      var WpfBitmapSource = WpfImage.FirstBitmapSource;

      var ImageLeft = Rect.Left;
      var ImageTop = Rect.Top;

      if (Mirror != null)
      {
        if (Mirror.Value == Inv.Mirror.Horizontal || Mirror.Value == Inv.Mirror.HorizontalAndVertical)
          ImageLeft = -Rect.Left - Rect.Width;

        if (Mirror.Value == Inv.Mirror.Vertical || Mirror.Value == Inv.Mirror.HorizontalAndVertical)
          ImageTop = -Rect.Top - Rect.Height;
      }

      var WpfImageDrawing = new System.Windows.Media.ImageDrawing(WpfBitmapSource, new System.Windows.Rect(ImageLeft, ImageTop, Rect.Width, Rect.Height));
      WpfImageDrawing.Freeze();

      if (Opacity == 1.0F && Mirror == null)
      {
        WpfDrawingGroup.Children.Add(WpfImageDrawing);
      }
      else
      {
        var WpfExtraGroup = new System.Windows.Media.DrawingGroup();
        WpfDrawingGroup.Children.Add(WpfExtraGroup);

        if (Opacity != 1.0F)
          WpfExtraGroup.Opacity = Opacity;

        if (Mirror != null)
          WpfExtraGroup.Transform = WpfDrawing.WpfHost.MirrorTransformArray[Mirror.Value];

        WpfExtraGroup.Children.Add(WpfImageDrawing);
      }
    }
    void DrawMask.DrawRectangle(Inv.Rect Rect, Inv.Colour FillColour, int StrokeThickness, Inv.Colour StrokeColour)
    {
      var WpfRectangleGeometry = new System.Windows.Media.RectangleGeometry();
      WpfRectangleGeometry.Rect = new System.Windows.Rect(Rect.Left, Rect.Top, Rect.Width, Rect.Height);
      WpfRectangleGeometry.Freeze();

      var WpfGeometryDrawing = new System.Windows.Media.GeometryDrawing(WpfDrawing.WpfHost.TranslateBrush(FillColour), WpfDrawing.TranslatePen(StrokeColour, StrokeThickness), WpfRectangleGeometry);
      WpfGeometryDrawing.Freeze();

      WpfDrawingGroup.Children.Add(WpfGeometryDrawing);
    }

    internal System.Windows.Media.DrawingBrush CompleteBrush()
    {
      WpfDrawingGroup.Freeze();

      var WpfDrawingBrush = new System.Windows.Media.DrawingBrush(WpfDrawingGroup);
      //WpfDrawingBrush.TileMode = System.Windows.Media.TileMode.None;
      //WpfDrawingBrush.Stretch = System.Windows.Media.Stretch.None;
      WpfDrawingBrush.Viewport = WpfViewportRect;
      WpfDrawingBrush.ViewportUnits = System.Windows.Media.BrushMappingMode.Absolute;
      WpfDrawingBrush.Freeze();

      return WpfDrawingBrush;
    }

    private readonly WpfDrawing WpfDrawing;
    private readonly System.Windows.Rect WpfViewportRect;
    private readonly System.Windows.Media.DrawingGroup WpfDrawingGroup;
  }

  public sealed class WpfCanvas : WpfClipper
  {
    internal WpfCanvas(WpfHost WpfHost)
    {
      this.ClipToBounds = true; // prevents drawing outside of the control.

      this.WpfDrawing = new WpfDrawing(WpfHost);
      AddVisualChild(WpfDrawing.WpfDrawingVisual);

      // NOTE: uncertain how CacheMode affects custom drawn performance. However, it does prevent the RenderOptions from being used and end up with poorly scaled DrawImages.
      //this.CacheMode = new System.Windows.Media.BitmapCache();
      System.Windows.Media.RenderOptions.SetBitmapScalingMode(this, System.Windows.Media.BitmapScalingMode.Fant);

      // By setting it to Aliased, we are turning off anti-aliasing for that element - this is to prevent fuzzy line drawing.
      //this.SetValue(System.Windows.Media.RenderOptions.EdgeModeProperty, System.Windows.Media.EdgeMode.Aliased);
    }

    public static readonly System.Windows.RoutedEvent MouseDoubleClickEvent = System.Windows.Controls.Control.MouseDoubleClickEvent.AddOwner(typeof(WpfCanvas));

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }
    public event System.Windows.Input.MouseButtonEventHandler MouseDoubleClick
    {
      add => base.AddHandler(WpfCanvas.MouseDoubleClickEvent, value);
      remove => base.RemoveHandler(WpfCanvas.MouseDoubleClickEvent, value);
    }
    public Func<System.Windows.Size, System.Windows.Size?> MeasureFunc;

    internal readonly WpfDrawing WpfDrawing;

    protected override System.Windows.Size MeasureOverride(System.Windows.Size constraint)
    {
      var Result = MeasureFunc(constraint);

      if (Result == null)
        return base.MeasureOverride(constraint);

      return Result.Value;
    }
    //protected override System.Windows.Size ArrangeOverride(System.Windows.Size arrangeBounds)
    //{
    //  return arrangeBounds;
    //}
    protected override void OnMouseDown(System.Windows.Input.MouseButtonEventArgs e)
    {
      if (e.ClickCount == 2)
      {
        RaiseEvent(new System.Windows.Input.MouseButtonEventArgs(System.Windows.Input.Mouse.PrimaryDevice, 0, e.ChangedButton)
        {
          RoutedEvent = WpfCanvas.MouseDoubleClickEvent,
          Source = this,
        });

        e.Handled = true;
      }
      else
      {
        base.OnMouseDown(e);
      }
    }
    protected override int VisualChildrenCount => 1;
    protected override System.Windows.Media.Visual GetVisualChild(int index) => WpfDrawing.WpfDrawingVisual;
  }

  internal sealed class WpfWindow : System.Windows.Window
  {
  }

  public abstract class WpfClipper : System.Windows.Controls.Border
  {
    protected override void OnRender(System.Windows.Media.DrawingContext dc)
    {
      OnApplyChildClip();
      base.OnRender(dc);
    }

    public override System.Windows.UIElement Child
    {
      get => base.Child;
      set
      {
        if (this.Child != value)
        {
          if (this.Child != null)
            this.Child.SetValue(System.Windows.UIElement.ClipProperty, OldClip); // Restore original clipping

          if (value != null)
            OldClip = value.ReadLocalValue(System.Windows.UIElement.ClipProperty);
          else
            OldClip = null; // If we dont set it to null we could leak a Geometry object

          base.Child = value;
        }
      }
    }

    protected virtual void OnApplyChildClip()
    {
      var ApplyChild = this.Child;

      if (ApplyChild != null)
      {
        if (this.CornerRadius.TopLeft != 0 || this.CornerRadius.TopRight != 0 || this.CornerRadius.BottomLeft != 0 || this.CornerRadius.BottomRight != 0)
        {
          /*
          if (ClipPath == null)
            this.ClipPath = new PathGeometry();
          else
            ClipPath.Clear();

          var WpfFigure = new PathFigure
          {
            StartPoint = new System.Windows.Point(0, 0),
            IsFilled = true,
            IsClosed = true
          };
          WpfFigure.Segments.Add(new ArcSegment
          {
            Point = new System.Windows.Point(ArcCenter.X + EndX, ArcCenter.Y - EndY),
            Size = new System.Windows.Size(MaxWidth / 2.0, MaxHeight / 2),
            RotationAngle = 0.0,
            IsLargeArc = (SweepAngle - StartAngle) > 180,
            SweepDirection = System.Windows.Media.SweepDirection.Counterclockwise,
            IsStroked = true,
            IsSmoothJoin = false
          });
          WpfFigure.Segments.Add(new LineSegment
          {
            Point = new System.Windows.Point(ArcCenter.X, ArcCenter.Y),
            IsStroked = true,
            IsSmoothJoin = false
          });
          WpfFigure.Freeze();

          ClipPath.Figures.Add(WpfFigure);
          ClipPath.Freeze();
          */
          var Radius = Math.Max(0.0, this.CornerRadius.TopLeft - (this.BorderThickness.Left * 0.5));

          ClipRect ??= new System.Windows.Media.RectangleGeometry();
          ClipRect.RadiusX = Radius;
          ClipRect.RadiusY = Radius;
          ClipRect.Rect = new System.Windows.Rect(Child.RenderSize);

          ApplyChild.Clip = ClipRect;
        }
        else
        {
          this.ClipRect = null;
          //this.ClipPath = null;

          ApplyChild.Clip = null;
        }
      }
    }

    private System.Windows.Media.RectangleGeometry ClipRect;
    //private PathGeometry ClipPath;
    private object OldClip;
  }

  public sealed class WpfFrame : WpfGrid
  {
    internal WpfFrame()
      : base()
    {
    }

    internal bool IsTransitioning
    {
      get => ClipToBounds;
      set => ClipToBounds = value; // NOTE: required for carousel transitions.
    }
  }

  public sealed class WpfBrowser : WpfClipper
  {
    internal WpfBrowser()
    {
      this.Inner = new System.Windows.Controls.WebBrowser();
      base.Child = Inner;
      Inner.Margin = new System.Windows.Thickness(0);

      // so we can see the background of control instead of stark white, before the page is loaded.
      Inner.Visibility = System.Windows.Visibility.Hidden;
      Inner.Loaded += (Sender, Event) =>
      {
        // NOTE: active mouse hooks cause the debugger to significantly lag when a breakpoint is hit (for a few seconds).
        if (!Debugger.IsAttached)
        {
          if (MouseHook == null)
          {
            this.MouseHook = new Inv.SystemMouseHook();
            MouseHook.FaultEvent += (Exception) => FaultEvent?.Invoke(Exception);
            MouseHook.HandleEvent += (Mouse) =>
            {
              if (Inner.IsLoaded && Inner.IsVisible)
              {
                var MousePosition = Win32.User32.GetMousePosition();
                var ControlPosition = Inner.PointFromScreen(MousePosition);

                if (ControlPosition.X >= 0 && ControlPosition.Y >= 0 && ControlPosition.X < Inner.ActualWidth && ControlPosition.Y < Inner.ActualHeight)
                {
                  // NOTE: the WebBrowser will always prevent bubbling of the mouse X events UNLESS we remove the WebBrowser from the visual tree in the Mouse Back/Forward event handlers.
                  //       so we are posting the event handler into the next message loop frame to avoid this situation from occurring.
                  Inner.Dispatcher.BeginInvoke(new Action(() =>
                  {
                    if (Mouse == Inv.SystemMouseType.XButton1Down)
                      MouseBackEvent?.Invoke();
                    else if (Mouse == Inv.SystemMouseType.XButton2Down)
                      MouseForwardEvent?.Invoke();
                  }));
                }
              }
            };
          }
        }
      };
      Inner.Unloaded += (Sender, Event) =>
      {
        if (MouseHook != null)
        {
          MouseHook.Dispose();
          this.MouseHook = null;
        }
      };
      Inner.LoadCompleted += (Sender, Event) =>
      {
        Inner.Visibility = System.Windows.Visibility.Visible;

        // TODO: autosize to content and hide scrollbars?
        //dynamic doc = Inner.Document;
        //Inner.Width = doc.body.scrollWidth;
        //Inner.Height = doc.body.scrollHeight;

        Inner.Dispatcher.BeginInvoke(new Action(() =>
        {
          ReadyEvent?.Invoke(Event.Uri);
        }));
      };
      Inner.Navigating += (Sender, Event) =>
      {
        if (BlockQuery != null && Event.Uri != null)
          Event.Cancel = BlockQuery(Event.Uri);
      };

      HideScriptErrors(true);

      void HideScriptErrors(bool hide)
      {
        var fiComWebBrowser = typeof(System.Windows.Controls.WebBrowser).GetField("_axIWebBrowser2", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
        if (fiComWebBrowser == null) return;
        var objComWebBrowser = fiComWebBrowser.GetValue(Inner);
        if (objComWebBrowser == null)
        {
          Inner.Loaded += (o, s) => HideScriptErrors(hide); //In case we are to early
          return;
        }
        objComWebBrowser.GetType().InvokeMember("Silent", System.Reflection.BindingFlags.SetProperty, null, objComWebBrowser, [hide]);
      }

      // TODO: does not seem to be any way to prevent the WebBrowser from displaying over the window when using a carousel navigation.
      //this.ClipToBounds = true;
      //Inner.ClipToBounds = true;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }
    public event Action<Exception> FaultEvent;
    public event Action MouseBackEvent;
    public event Action MouseForwardEvent;
    public event Func<Uri, bool> BlockQuery;
    public event Action<Uri> ReadyEvent;

    public void Navigate(Uri Uri, string Html)
    {
      if (Html != null)
        Inner.NavigateToString(Html);
      else if (Uri != null)
        Inner.Navigate(Uri);
      else
        Inner.Navigate("about:blank");
    }

    private readonly System.Windows.Controls.WebBrowser Inner;
    private Inv.SystemMouseHook MouseHook;
  }

  public sealed class WpfStack : WpfClipper
  {
    internal WpfStack()
    {
      this.Inner = new System.Windows.Controls.StackPanel();
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }
    public System.Windows.Controls.Orientation Orientation
    {
      get => Inner.Orientation;
      set
      {
        if (Inner.Orientation != value)
          Inner.Orientation = value;
      }
    }

    internal void Compose(IEnumerable<System.Windows.FrameworkElement> WpfElements)
    {
      // TODO: delta algorithm?

      Inner.Children.Clear();
      foreach (var WpfElement in WpfElements)
        Inner.SafeAddChild(WpfElement);
    }

    public System.Windows.Controls.StackPanel AsStackPanel()
    {
      return Inner;
    }

    private readonly System.Windows.Controls.StackPanel Inner;
  }

  public sealed class WpfSwitch : WpfClipper
  {
    public WpfSwitch()
    {
      this.Inner = new WpfSwitchButton();
      Inner.IsThreeState = false;
      Inner.PrimaryBrush = System.Windows.Media.Brushes.DimGray;
      Inner.SecondaryBrush = System.Windows.Media.Brushes.LightGray;
      Inner.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;

      base.Child = Inner;
    }

    public bool IsOn
    {
      get => Inner.IsChecked ?? false;
      set => Inner.IsChecked = value;
    }
    public System.Windows.Media.Brush PrimaryBrush
    {
      get => Inner.PrimaryBrush;
      set => Inner.PrimaryBrush = value;
    }
    public System.Windows.Media.Brush SecondaryBrush
    {
      get => Inner.SecondaryBrush;
      set => Inner.SecondaryBrush = value;
    }
    public event System.Windows.RoutedEventHandler ChangeEvent
    {
      add
      {
        Inner.Checked += value;
        Inner.Unchecked += value;
      }
      remove
      {
        Inner.Checked -= value;
        Inner.Unchecked -= value;
      }
    }

    private readonly WpfSwitchButton Inner;
  }

  public sealed class WpfWrap : WpfClipper
  {
    internal WpfWrap()
    {
      this.Inner = new System.Windows.Controls.WrapPanel();
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }
    public System.Windows.Controls.Orientation Orientation
    {
      get => Inner.Orientation;
      set
      {
        if (Inner.Orientation != value)
          Inner.Orientation = value;
      }
    }

    internal void Compose(IEnumerable<System.Windows.FrameworkElement> WpfElements)
    {
      // TODO: delta algorithm?

      Inner.Children.Clear();
      foreach (var WpfElement in WpfElements)
        Inner.SafeAddChild(WpfElement);
    }

    public System.Windows.Controls.WrapPanel AsWrapPanel()
    {
      return Inner;
    }

    private readonly System.Windows.Controls.WrapPanel Inner;
  }

  public struct WpfFlowItem
  {
    public Inv.Panel Panel;
    public System.Windows.FrameworkElement Element;
  }

  public sealed class WpfFlow : WpfClipper
  {
    internal WpfFlow()
    {
      this.FlowItemsControl = new WpfFlowItemsControl();
      base.Child = FlowItemsControl;

      System.Windows.Controls.ScrollViewer.SetPanningMode(FlowItemsControl, System.Windows.Controls.PanningMode.VerticalOnly);
      //System.Windows.Controls.ScrollViewer.SetPanningRatio(FlowItemsControl, 1); // The ratio of scrolling offset to translate manipulation offset in device-independent units (1/96th inch per unit).
      //System.Windows.Controls.ScrollViewer.SetPanningDeceleration(FlowItemsControl, 0.001); // device-independent units (1/96th inch per unit) per squared millisecond when in inertia.

      //this.ScrollTargetIndex = -1;

      FlowItemsControl.ScrollChangeEvent += (E) => QueryItemRange((int)E.VerticalOffset, (int)(E.VerticalOffset + E.ViewportHeight));

      this.IndexContentDictionary = [];

      this.PlaceholderCollection = [];
      FlowItemsControl.ItemsSource = PlaceholderCollection;
    }

    public object Fixture
    {
      get => FlowItemsControl.Header;
      set => FlowItemsControl.Header = value;
    }
    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }
    public Func<int, WpfFlowItem> HeaderContentQuery;
    public Func<int, WpfFlowItem> FooterContentQuery;
    public Func<int> SectionCountQuery;
    public Func<int, int> ItemCountQuery;
    public Func<int, int, WpfFlowItem> ItemContentQuery;
    public Action<int, int, WpfFlowItem> ItemRecycleEvent;

    public void Reload()
    {
      // everything we thought we knew is gone.
      if (ItemRecycleEvent != null)
      {
        foreach (var IndexContent in IndexContentDictionary.Values)
        {
          if (IndexContent != null)
            ItemRecycleEvent(IndexContent.SectionIndex, IndexContent.ItemIndex, IndexContent.Item);
        }
      }
      IndexContentDictionary.Clear();

      var SectionCount = SectionCountQuery();

      var PlaceholderIndex = 0;

      for (var SectionIndex = 0; SectionIndex < SectionCount; SectionIndex++)
      {
        var HeaderContent = HeaderContentQuery(SectionIndex);
        if (HeaderContent.Element != null)
          PlaceholderIndex++;

        PlaceholderIndex += ItemCountQuery(SectionIndex);

        var FooterContent = FooterContentQuery(SectionIndex);
        if (FooterContent.Element != null)
          PlaceholderIndex++;
      }

      if (PlaceholderIndex != PlaceholderCollection.Count)
      {
        // KJV 2022-10-28
        // Note: BeginInit() and EndInit() are not used here as they appear to force a time-consuming layout pass on the items even when nothing in the visible portion of the visual tree has changed.
        //  VirtualizingStackPanel appears to be intelligent enough to know that nothing in the visible portion of the visual tree has changed, and avoids the layout pass.

        // CH  2022-11-10
        // Note: For the initial load and adding of the placeholders, a BeginInit()/EndInit() is required to prevent the list from scrolling to the last item.

        PlaceholderCollection.Truncate(PlaceholderIndex);

        var IsInitial = PlaceholderCollection.Count == 0;

        if (IsInitial)
          FlowItemsControl.BeginInit();
        try
        {
          while (PlaceholderIndex > PlaceholderCollection.Count)
          {
            PlaceholderCollection.Add(new System.Windows.Controls.ContentPresenter()
            {
              Height = 50, // default row size, prior to row-specific sizing.
              Content = null
            });
          }
        }
        finally
        {
          if (IsInitial)
            FlowItemsControl.EndInit();
        }
      }

      if (/*FlowItemsControl.IsLoaded && */FlowItemsControl.VirtualizingStackPanel != null)
        QueryItemRange((int)FlowItemsControl.VirtualizingStackPanel.VerticalOffset, (int)(FlowItemsControl.VirtualizingStackPanel.VerticalOffset + FlowItemsControl.VirtualizingStackPanel.ViewportHeight));
    }
    public void ScrollTo(int Section, int Index)
    {
      var TargetIndex = GetAbsoluteIndex(Section, Index);

      if (FlowItemsControl.VirtualizingStackPanel != null && TargetIndex >= 0 && TargetIndex < PlaceholderCollection.Count)
        FlowItemsControl.VirtualizingStackPanel.BringIndexIntoViewPublic(TargetIndex);
      //else
      //  this.ScrollTargetIndex = TargetIndex;
    }

    private readonly WpfFlowItemsControl FlowItemsControl;
    private readonly System.Collections.ObjectModel.ObservableCollection<System.Windows.FrameworkElement> PlaceholderCollection;
    //private int ScrollTargetIndex;

    private void QueryItemRange(int FromIndex, int UntilIndex)
    {
      //Debug.WriteLine($"QueryItemRange: {FromIndex} {UntilIndex}");

      foreach (var IndexContentEntry in IndexContentDictionary.ToArray())
      {
        var Index = IndexContentEntry.Key;

        if (Index < FromIndex || Index > UntilIndex)
        {
          var OldContent = IndexContentEntry.Value;

          if (OldContent != null)
          {
            ItemRecycleEvent?.Invoke(OldContent.SectionIndex, OldContent.ItemIndex, OldContent.Item);

            IndexContentDictionary.Remove(Index);
          }
        }
      }

      var SectionCount = SectionCountQuery();
      var AbsoluteIndex = 0;

      for (var SectionIndex = 0; SectionIndex < SectionCount; SectionIndex++)
      {
        var ItemCount = ItemCountQuery(SectionIndex);

        var HeaderContent = HeaderContentQuery(SectionIndex);
        if (HeaderContent.Element != null)
        {
          if (!HasContent(AbsoluteIndex) && AbsoluteIndex.Between(FromIndex, UntilIndex))
            IndexContentDictionary[AbsoluteIndex] = new WpfFlowContentInfo() { Item = HeaderContent, SectionIndex = SectionIndex, ItemIndex = -1 };

          AbsoluteIndex++;
        }

        for (var ItemIndex = 0; ItemIndex < ItemCount; ItemIndex++)
        {
          if (!HasContent(AbsoluteIndex) && AbsoluteIndex.Between(FromIndex, UntilIndex))
            IndexContentDictionary[AbsoluteIndex] = new WpfFlowContentInfo() { Item = ItemContentQuery(SectionIndex, ItemIndex), SectionIndex = SectionIndex, ItemIndex = ItemIndex };

          AbsoluteIndex++;
        }

        var FooterContent = FooterContentQuery(SectionIndex);
        if (FooterContent.Element != null)
        {
          if (!HasContent(AbsoluteIndex) && AbsoluteIndex.Between(FromIndex, UntilIndex))
            IndexContentDictionary[AbsoluteIndex] = new WpfFlowContentInfo() { Item = FooterContent, SectionIndex = SectionIndex, ItemIndex = -1 };

          AbsoluteIndex++;
        }
      }

      foreach (var Pair in IndexContentDictionary)
        AddOrUpdatePlaceholder(Pair.Key, Pair.Value?.Item.Element);
    }
    private void AddOrUpdatePlaceholder(int AbsoluteIndex, System.Windows.FrameworkElement Element)
    {
      if (PlaceholderCollection.Count <= AbsoluteIndex)
      {
        PlaceholderCollection.Add(new System.Windows.Controls.ContentPresenter() { Height = double.NaN, Content = Element });
      }
      else
      {
        var ExistingContentPresenter = PlaceholderCollection.ElementAt(AbsoluteIndex) as System.Windows.Controls.ContentPresenter;
        if (ExistingContentPresenter != null)
        {
          if (ExistingContentPresenter.Content != Element)
            ExistingContentPresenter.Content = Element;

          if (ExistingContentPresenter.Height != double.NaN)
            ExistingContentPresenter.Height = double.NaN;
        }
      }
    }
    private int GetAbsoluteIndex(int Section, int Index)
    {
      var Position = 0;

      for (var SectionIndex = 0; SectionIndex <= Section; SectionIndex++)
      {
        if (HeaderContentQuery(SectionIndex).Element != null)
          Position++;

        if (SectionIndex == Section)
        {
          Position += Index;
          break;
        }

        Position += ItemCountQuery(SectionIndex);

        if (FooterContentQuery(SectionIndex).Element != null)
          Position++;
      }

      return Position;
    }
    private bool HasContent(int Index)
    {
      return IndexContentDictionary.TryGetValue(Index, out var Content) && Content != null;
    }

    private readonly Dictionary<int, WpfFlowContentInfo> IndexContentDictionary;

    private sealed class WpfFlowContentInfo
    {
      public int SectionIndex;
      public int ItemIndex;
      public WpfFlowItem Item;
    }

    private class WpfFlowItemContainer : System.Windows.Controls.ContentControl
    {
      public WpfFlowItemContainer()
      {
        this.Focusable = false;
      }
    }

    private class WpfFlowItemsControl : System.Windows.Controls.HeaderedItemsControl
    {
      public WpfFlowItemsControl()
      {
        this.Style = DefaultStyle;
        this.Focusable = false;

        WpfFlowVirtualizingStackPanel.AddScrollChangeHandler(this, (Sender, E) =>
        {
          // NOTE: This just exists now to get access to the underlying virtualised stack panel.

          this.VirtualizingStackPanel = E.Panel;
        });
      }

      public WpfFlowVirtualizingStackPanel VirtualizingStackPanel { get; private set; }
      public event Action<System.Windows.Controls.ScrollChangedEventArgs> ScrollChangeEvent;

      public override void OnApplyTemplate()
      {
        if (ContentScrollViewer != null)
          ContentScrollViewer.ScrollChanged -= HandleScrollChanged;

        base.OnApplyTemplate();

        this.ContentScrollViewer = Template.FindName("ItemsControl_ContentScrollViewer", this) as WpfScrollViewer;
        this.HeaderScrollViewer = Template.FindName("ItemsControl_FixtureScrollViewer", this) as WpfScrollViewer;

        HeaderScrollViewer.KeyboardScrollingDisabled = true;

        ContentScrollViewer.KeyboardScrollingDisabled = true;
        ContentScrollViewer.ScrollChanged += HandleScrollChanged;
      }

      protected override bool IsItemItsOwnContainerOverride(object Item)
      {
        return Item is WpfFlowItemContainer;
      }
      protected override System.Windows.DependencyObject GetContainerForItemOverride()
      {
        return new WpfFlowItemContainer();
      }

      private void HandleScrollChanged(object sender, System.Windows.Controls.ScrollChangedEventArgs e)
      {
        var IsDark = Inv.Theme.IsDark;
        var Offset = e.HorizontalOffset;

        ContentScrollViewer.IsDark = IsDark;

        if (HeaderScrollViewer != null)
        {
          HeaderScrollViewer.IsDark = IsDark;
          HeaderScrollViewer.ScrollToHorizontalOffset(Offset);
        }

        ScrollChangeEvent?.Invoke(e);
      }

      private WpfScrollViewer ContentScrollViewer;
      private WpfScrollViewer HeaderScrollViewer;

      static WpfFlowItemsControl()
      {
        var ResourceDictionary = WpfShell.LoadResourceDictionary("InvWpfVirtualisedItemsControl.xaml");

        if (ResourceDictionary != null)
        {
          DefaultStyle = (System.Windows.Style)ResourceDictionary["BaseStyle"];
          DefaultStyle.Seal();
        }
      }

      private static readonly System.Windows.Style DefaultStyle;
    }
  }

  internal class WpfFlowOffsetChangedEventArgs : System.Windows.RoutedEventArgs
  {
    internal WpfFlowOffsetChangedEventArgs(System.Windows.RoutedEvent routedEvent, object source)
    {
      this.RoutedEvent = routedEvent;
      this.Source = source;
    }

    public WpfFlowVirtualizingStackPanel Panel { get; set; }
    public double ViewportHeight { get; set; }
    public double VerticalOffset { get; set; }

    public string Name { get; set; }
  }

  internal delegate void WpfFlowOffsetChangedEventHandler(object sender, WpfFlowOffsetChangedEventArgs e);

  internal class WpfFlowVirtualizingStackPanel : System.Windows.Controls.VirtualizingStackPanel
  {
    static WpfFlowVirtualizingStackPanel()
    {
      ScrollChangeEvent = System.Windows.EventManager.RegisterRoutedEvent("OffsetChangedEvent", System.Windows.RoutingStrategy.Bubble, typeof(WpfFlowOffsetChangedEventHandler), typeof(WpfFlowVirtualizingStackPanel));
    }

    public static readonly System.Windows.RoutedEvent ScrollChangeEvent;

    public static void AddScrollChangeHandler(System.Windows.DependencyObject o, WpfFlowOffsetChangedEventHandler Event)
    {
      ((System.Windows.UIElement)o).AddHandler(WpfFlowVirtualizingStackPanel.ScrollChangeEvent, Event);
    }
    public static void RemoveScrollChangeHandler(System.Windows.DependencyObject o, WpfFlowOffsetChangedEventHandler Event)
    {
      ((System.Windows.UIElement)o).RemoveHandler(WpfFlowVirtualizingStackPanel.ScrollChangeEvent, Event);
    }

    protected override void OnViewportOffsetChanged(System.Windows.Vector OldViewportOffset, System.Windows.Vector NewViewportOffset)
    {
      base.OnViewportOffsetChanged(OldViewportOffset, NewViewportOffset);

      OffsetChangedInvoke("OnViewportOffsetChanged");
    }
    protected override void OnViewportSizeChanged(System.Windows.Size OldViewportSize, System.Windows.Size NewViewportSize)
    {
      base.OnViewportSizeChanged(OldViewportSize, NewViewportSize);

      OffsetChangedInvoke("OnViewportSizeChanged");
    }

    private void OffsetChangedInvoke(string Name)
    {
      this.RaiseEvent(new WpfFlowOffsetChangedEventArgs(WpfFlowVirtualizingStackPanel.ScrollChangeEvent, this) { Name = Name, Panel = this, ViewportHeight = this.ViewportHeight, VerticalOffset = this.VerticalOffset });
    }
  }

  public abstract class WpfGrid : WpfClipper
  {
    internal WpfGrid()
    {
      this.Inner = new System.Windows.Controls.Grid();
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }

    public int ChildCount => Inner.Children.Count;
    public System.Windows.Controls.ColumnDefinitionCollection ColumnDefinitions => Inner.ColumnDefinitions;
    public System.Windows.Controls.RowDefinitionCollection RowDefinitions => Inner.RowDefinitions;

    public void ClearChildren()
    {
      Inner.Children.Clear();
    }
    public bool ContainsChild(System.Windows.UIElement Element)
    {
      return Inner.Children.Contains(Element);
    }
    public void AddChild(System.Windows.UIElement Element)
    {
      Inner.SafeAddChild(Element);
    }
    public void RemoveChild(System.Windows.UIElement Element)
    {
      Inner.Children.Remove(Element);
    }
    public System.Windows.UIElement GetChild(int Index)
    {
      return Inner.Children[Index];
    }

    private readonly System.Windows.Controls.Grid Inner;
  }

  internal sealed class WpfMaster : WpfGrid
  {
    internal WpfMaster()
      : base()
    {
    }
  }

  public sealed class WpfDock : WpfClipper
  {
    internal WpfDock()
      : base()
    {
      this.Panel = new WpfDockPanel();
      base.Child = Panel;
      Panel.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      Panel.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }

    public bool IsHorizontal
    {
      get => Panel.IsHorizontal;
      set => Panel.IsHorizontal = value;
    }

    public void Compose(IEnumerable<System.Windows.FrameworkElement> WpfHeaders, IEnumerable<System.Windows.FrameworkElement> WpfClients, IEnumerable<System.Windows.FrameworkElement> WpfFooters)
    {
      Panel.Compose(WpfHeaders, WpfClients, WpfFooters);
    }

    private readonly WpfDockPanel Panel;

    private sealed class WpfDockPanel : System.Windows.Controls.Panel
    {
      internal WpfDockPanel()
        : base()
      {
        this.WpfHeaderList = [];
        this.WpfClientList = [];
        this.WpfFooterList = [];
        this.WpfActiveList = [];
      }

      [Obsolete("Do not use", true)]
      public new System.Windows.Controls.UIElementCollection Children => base.Children;
      public bool IsHorizontal
      {
        get => IsHorizontalField;
        set
        {
          if (IsHorizontalField != value)
          {
            this.IsHorizontalField = value;

            // this is necessary to toggle horizontal/vertical if none of the child panels change as well.
            InvalidateArrange();
            InvalidateMeasure();
          }
        }
      }

      protected override System.Windows.Size MeasureOverride(System.Windows.Size constraint)
      {
        var Result = new System.Windows.Size();

        var FitSize = constraint;

        foreach (var WpfPanelList in new[] { WpfHeaderList, WpfFooterList })
        {
          foreach (var WpfElement in WpfPanelList)
          {
            if (WpfElement.Visibility != System.Windows.Visibility.Collapsed)
            {
              WpfElement.Measure(FitSize);

              var WpfDesiredSize = WpfElement.DesiredSize;

              if (IsHorizontal)
              {
                if (Result.Height < WpfDesiredSize.Height)
                  Result.Height = WpfDesiredSize.Height;

                if (WpfDesiredSize.Width > 0)
                {
                  Result.Width += WpfDesiredSize.Width;

                  if (FitSize.Width > 0)
                    FitSize.Width -= Math.Min(FitSize.Width, WpfDesiredSize.Width); // FitSize can be 5.599999 when DesiredWidth is 5.6 (which causes a measure error on set width).
                }
              }
              else
              {
                if (Result.Width < WpfDesiredSize.Width)
                  Result.Width = WpfDesiredSize.Width;

                if (WpfDesiredSize.Height > 0)
                {
                  Result.Height += WpfDesiredSize.Height;

                  if (FitSize.Height > 0)
                    FitSize.Height -= Math.Min(FitSize.Height, WpfDesiredSize.Height);
                }
              }
            }
          }
        }

        var WpfClientArray = WpfClientList.Where(C => C.Visibility != System.Windows.Visibility.Collapsed).ToArray();

        if (WpfClientArray.Length > 0)
        {
          var SharedSize = new System.Windows.Size(IsHorizontal ? FitSize.Width / WpfClientArray.Length : FitSize.Width, IsHorizontal ? FitSize.Height : FitSize.Height / WpfClientArray.Length);

          foreach (var WpfElement in WpfClientArray)
          {
            WpfElement.Measure(SharedSize);

            var WpfDesiredSize = WpfElement.DesiredSize;

            if (IsHorizontal)
            {
              if (Result.Height < WpfDesiredSize.Height)
                Result.Height = WpfDesiredSize.Height;

              if (WpfDesiredSize.Width > 0)
                Result.Width += WpfDesiredSize.Width;
            }
            else
            {
              if (Result.Width < WpfDesiredSize.Width)
                Result.Width = WpfDesiredSize.Width;

              if (WpfDesiredSize.Height > 0)
                Result.Height += WpfDesiredSize.Height;
            }
          }
        }

        return Result;
      }
      protected override System.Windows.Size ArrangeOverride(System.Windows.Size arrangeBounds)
      {
        var DockWidth = arrangeBounds.Width;
        var DockHeight = arrangeBounds.Height;

        var WpfClientArray = WpfClientList.Where(C => C.Visibility != System.Windows.Visibility.Collapsed).ToArray();

        if (IsHorizontal)
        {
          var PanelLeft = 0.0;

          var HeaderWidth = 0.0;

          foreach (var WpfElement in WpfHeaderList)
          {
            if (WpfElement.Visibility != System.Windows.Visibility.Collapsed)
            {
              var HeaderSize = WpfElement.DesiredSize;
              WpfElement.Arrange(new System.Windows.Rect(PanelLeft, 0, HeaderSize.Width, DockHeight));
              PanelLeft += HeaderSize.Width;
              HeaderWidth += HeaderSize.Width;
            }
          }

          var FooterTotal = WpfFooterList.Sum(F => F.DesiredSize.Width);
          var ClientRemainder = DockWidth - HeaderWidth - FooterTotal;

          if (WpfClientArray.Length > 0)
          {
            var SharedWidth = ClientRemainder < 0 ? 0 : ClientRemainder / WpfClientArray.Length;

            var LastClient = WpfClientArray.Last();

            foreach (var Client in WpfClientArray)
            {
              var FrameWidth = SharedWidth;
              if (ClientRemainder > 0 && Client == LastClient)
                FrameWidth += ClientRemainder - (SharedWidth * WpfClientArray.Length);

              Client.Arrange(new System.Windows.Rect(PanelLeft, 0, FrameWidth, DockHeight));
              PanelLeft += FrameWidth;
            }
          }

          var PanelRight = (ClientRemainder < 0 ? DockWidth - ClientRemainder : DockWidth);

          foreach (var WpfElement in WpfFooterList)
          {
            if (WpfElement.Visibility != System.Windows.Visibility.Collapsed)
            {
              var FooterWidth = WpfElement.DesiredSize.Width;
              PanelRight -= FooterWidth;
              WpfElement.Arrange(new System.Windows.Rect(PanelRight, 0, FooterWidth, DockHeight));
            }
          }
        }
        else
        {
          var HeaderHeight = 0.0;

          foreach (var WpfElement in WpfHeaderList)
          {
            if (WpfElement.Visibility != System.Windows.Visibility.Collapsed)
            {
              var CurrentHeight = WpfElement.DesiredSize.Height;
              WpfElement.Arrange(new System.Windows.Rect(0, HeaderHeight, DockWidth, CurrentHeight));
              HeaderHeight += CurrentHeight;
            }
          }

          var FooterTotal = WpfFooterList.Sum(F => F.DesiredSize.Height);
          var ClientRemainder = DockHeight - HeaderHeight - FooterTotal;

          if (WpfClientArray.Length > 0)
          {
            var SharedHeight = ClientRemainder < 0 ? 0 : ClientRemainder / WpfClientArray.Length;
            var LastClient = WpfClientArray.Last();

            var ClientTop = HeaderHeight;

            foreach (var WpfElement in WpfClientArray)
            {
              var FrameHeight = SharedHeight;
              if (ClientRemainder > 0 && WpfElement == LastClient)
                FrameHeight += ClientRemainder - (SharedHeight * WpfClientArray.Length);

              WpfElement.Arrange(new System.Windows.Rect(0, ClientTop, DockWidth, FrameHeight));
              ClientTop += FrameHeight;
            }
          }

          var FooterBottom = (ClientRemainder < 0 ? DockHeight - ClientRemainder : DockHeight);

          foreach (var WpfElement in WpfFooterList)
          {
            if (WpfElement.Visibility != System.Windows.Visibility.Collapsed)
            {
              var FooterHeight = WpfElement.DesiredSize.Height;
              FooterBottom -= FooterHeight;
              WpfElement.Arrange(new System.Windows.Rect(0, FooterBottom, DockWidth, FooterHeight));
            }
          }
        }

        return arrangeBounds;
      }

      public void Compose(IEnumerable<System.Windows.FrameworkElement> WpfHeaders, IEnumerable<System.Windows.FrameworkElement> WpfClients, IEnumerable<System.Windows.FrameworkElement> WpfFooters)
      {
        WpfHeaderList.Clear();
        WpfHeaderList.AddRange(WpfHeaders);

        WpfClientList.Clear();
        WpfClientList.AddRange(WpfClients);

        WpfFooterList.Clear();
        WpfFooterList.AddRange(WpfFooters.Reverse()); // will be arranged in reverse order.

        var WpfPreviousList = WpfActiveList;
        this.WpfActiveList = new DistinctList<System.Windows.FrameworkElement>(WpfHeaderList.Count + WpfClientList.Count + WpfFooterList.Count);
        WpfActiveList.AddRange(WpfHeaderList);
        WpfActiveList.AddRange(WpfClientList);
        WpfActiveList.AddRange(WpfFooterList);

        foreach (var Container in WpfActiveList.Except(WpfPreviousList))
          this.SafeAddChild(InternalChildren, Container);

        foreach (var Container in WpfPreviousList.Except(WpfActiveList))
          InternalChildren.Remove(Container);
      }

      private readonly Inv.DistinctList<System.Windows.FrameworkElement> WpfHeaderList;
      private readonly Inv.DistinctList<System.Windows.FrameworkElement> WpfClientList;
      private readonly Inv.DistinctList<System.Windows.FrameworkElement> WpfFooterList;
      private Inv.DistinctList<System.Windows.FrameworkElement> WpfActiveList;
      private bool IsHorizontalField;
    }
  }

  public sealed class WpfOverlay : WpfGrid
  {
    internal WpfOverlay()
      : base()
    {
    }
  }

  public sealed class WpfTable : WpfGrid
  {
    internal WpfTable()
      : base()
    {
    }
  }

  public sealed class WpfBoard : WpfClipper
  {
    internal WpfBoard()
    {
      this.Inner = new System.Windows.Controls.Canvas();
      base.Child = Inner;

      this.UseLayoutRounding = true;
      this.SnapsToDevicePixels = true;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }

    public IEnumerable<WpfBoardPin> GetPins()
    {
      return Inner.Children.Cast<WpfBoardPin>();
    }
    public void RemovePins()
    {
      Inner.Children.Clear();
    }
    public WpfBoardPin AddPin()
    {
      var Result = new WpfBoardPin();

      Inner.Children.Add(Result);

      return Result;
    }

    private readonly System.Windows.Controls.Canvas Inner;
  }

  public sealed class WpfBoardPin : System.Windows.Controls.Border
  {
    internal WpfBoardPin()
    {
    }
  }

  public sealed class WpfGraphic : WpfClipper
  {
    internal WpfGraphic()
    {
      this.Inner = new System.Windows.Controls.Image()
      {
        Stretch = System.Windows.Media.Stretch.Uniform,   // default stretch.
        HorizontalAlignment = System.Windows.HorizontalAlignment.Center, // so we centre the image when stretching.
        VerticalAlignment = System.Windows.VerticalAlignment.Center      // so we centre the image when stretching.
      };
      System.Windows.Media.RenderOptions.SetBitmapScalingMode(Inner, System.Windows.Media.BitmapScalingMode.Fant);

      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }
    public System.Windows.Media.Stretch Stretch
    {
      get => Inner.Stretch;
      set => Inner.Stretch = value;
    }
    public WpfImage Source
    {
      set
      {
        if (SourceField != value)
        {
          // end any animations.
          if (SourceField?.IndividualFrames != null)
            Inner.BeginAnimation(System.Windows.Controls.Image.SourceProperty, null);

          this.SourceField = value;

          if (value == null || value.FirstBitmapSource == null)
          {
            Inner.Source = null;
          }
          else
          {
            if (value.IndividualFrames == null || value.TotalAnimationDelayMS == 0)
            {
              // single image files and multi-page files only show the first page.
              Inner.Source = value.FirstBitmapSource;
            }
            else
            {
              // animated gifs.
              var Animation = new System.Windows.Media.Animation.ObjectAnimationUsingKeyFrames();
              foreach (var IndividualFrame in value.IndividualFrames)
                Animation.KeyFrames.Add(new System.Windows.Media.Animation.DiscreteObjectKeyFrame(IndividualFrame.BitmapSource, TimeSpan.FromMilliseconds(IndividualFrame.AnimationKeytimeMS)));
              Animation.Duration = TimeSpan.FromMilliseconds(value.TotalAnimationDelayMS);
              Animation.RepeatBehavior = System.Windows.Media.Animation.RepeatBehavior.Forever;

              Inner.Source = Animation.KeyFrames.Count > 0 ? (System.Windows.Media.ImageSource)Animation.KeyFrames[0].Value : value.FirstBitmapSource;
              Inner.BeginAnimation(System.Windows.Controls.Image.SourceProperty, Animation);
            }
          }
        }
      }
    }

    private readonly System.Windows.Controls.Image Inner;
    private WpfImage SourceField;
  }

  public enum WpfImageType
  {
    Unknown, Png, Jpg, Tiff, Gif, Bmp, Ico, Wmp
  }

  public sealed class WpfImage
  {
    internal WpfImage(WpfImageType Type, System.Windows.Media.Imaging.BitmapSource Source)
    {
      Debug.Assert(Source.IsFrozen, "Source must be frozen.");

      this.Type = Type;
      this.FirstBitmapSource = Source;
    }
    private WpfImage(WpfImageType Type, IReadOnlyList<System.Windows.Media.Imaging.BitmapSource> SourceList)
    {
      this.Type = Type;

      if (SourceList.Count == 1)
      {
        // NOTE: WriteableBitmap is required to avoid thread ownership exceptions, when the frozen frame is used from a different thread.
        var WriteableBitmap = new System.Windows.Media.Imaging.WriteableBitmap(SourceList[0]);
        WriteableBitmap.Freeze();

        this.FirstBitmapSource = WriteableBitmap;
      }
      else
      {
        var IndividualArray = new WpfImageFrame[SourceList.Count];

        if (Type == WpfImageType.Gif)
        {
          // multi-frame animated gif.
          var FirstSource = SourceList[0];
          var DpiX = FirstSource.DpiX;
          var DpiY = FirstSource.DpiY;
          var FullWidth = FirstSource.PixelWidth;
          var FullHeight = FirstSource.PixelHeight;
          var FullRect = new System.Windows.Rect(0, 0, FullWidth, FullHeight);

          var SourceIndex = 0;
          System.Windows.Media.Imaging.BitmapSource BackingBitmapSource = null;
          foreach (var Source in SourceList)
          {
            var SourceDisposalMethod = WpfImageFrameDisposalMethod.None;
            var SourceAnimationDelayMS = 100;
            var SourceLeft = 0;
            var SourceTop = 0;
            var SourceWidth = Source.PixelWidth;
            var SourceHeight = Source.PixelHeight;
            var SourceMetadata = Source.Metadata as System.Windows.Media.Imaging.BitmapMetadata;
            if (SourceMetadata != null)
            {
              try
              {
                T? QueryResult<T>(string query)
                  where T : struct
                {
                  if (SourceMetadata.ContainsQuery(query))
                  {
                    object value = SourceMetadata.GetQuery(query);
                    if (value != null)
                      return (T)value;
                  }
                  return null;
                }

                var DelayResult = QueryResult<ushort>(GifDelayQuery);
                if (DelayResult != null)
                  SourceAnimationDelayMS = DelayResult.Value * 10;

                var DisposalResult = QueryResult<byte>(GifDisposalQuery);
                if (DisposalResult != null)
                  SourceDisposalMethod = (WpfImageFrameDisposalMethod)DisposalResult.Value;

                var LeftResult = QueryResult<ushort>(GifLeftQuery);
                if (LeftResult != null)
                  SourceLeft = LeftResult.Value;

                var TopResult = QueryResult<ushort>(GifTopQuery);
                if (TopResult.HasValue)
                  SourceTop = TopResult.Value;

                var WidthResult = QueryResult<ushort>(GifWidthQuery);
                if (WidthResult.HasValue)
                  SourceWidth = WidthResult.Value;

                var HeightResult = QueryResult<ushort>(GifHeightQuery);
                if (HeightResult.HasValue)
                  SourceHeight = HeightResult.Value;
              }
              catch (NotSupportedException)
              {
              }
            }

            var RenderInFresh = BackingBitmapSource == null;
            var RenderInFull = SourceLeft == 0 && SourceTop == 0 && SourceWidth == FullWidth && SourceHeight == FullHeight;

            System.Windows.Media.Imaging.BitmapSource RenderSource;

            if (RenderInFresh && RenderInFull)
            {
              // No previous image to combine with and we are rendering the full rect, so just use the frame.
              RenderSource = Source;
            }
            else
            {
              var RenderRect = new System.Windows.Rect(SourceLeft, SourceTop, SourceWidth, SourceHeight);
              var DrawingVisual = new System.Windows.Media.DrawingVisual();

              using (var DrawingContext = DrawingVisual.RenderOpen())
              {
                if (!RenderInFresh)
                  DrawingContext.DrawImage(BackingBitmapSource, FullRect);

                DrawingContext.DrawImage(Source, RenderRect);
              }

              var RenderBitmap = new System.Windows.Media.Imaging.RenderTargetBitmap(FullWidth, FullHeight, DpiX, DpiY, System.Windows.Media.PixelFormats.Pbgra32);
              RenderBitmap.Render(DrawingVisual);
              RenderBitmap.Freeze();
              RenderBitmap.ReleaseLooseGDIHandles();

              RenderSource = RenderBitmap;
            }

            // NOTE: WriteableBitmap is required to avoid thread ownership exceptions, when the frozen frame is used from a different thread.
            var WriteableBitmap = new System.Windows.Media.Imaging.WriteableBitmap(RenderSource);
            WriteableBitmap.Freeze();

            if (FirstBitmapSource == null)
              this.FirstBitmapSource = WriteableBitmap;

#if DEBUG
            //System.IO.File.WriteAllBytes(@$"C:\Hole\Frame{FrameIndex}.png", RenderFrame.ConvertToInvImagePng().GetBuffer());
#endif

            IndividualArray[SourceIndex++] = new WpfImageFrame(WriteableBitmap, AnimationKeytimeMS: TotalAnimationDelayMS);
            this.TotalAnimationDelayMS += SourceAnimationDelayMS;

            switch (SourceDisposalMethod)
            {
              case WpfImageFrameDisposalMethod.None:
              case WpfImageFrameDisposalMethod.DoNotDispose:
                // use the new frame.
                BackingBitmapSource = WriteableBitmap;
                break;

              case WpfImageFrameDisposalMethod.RestoreBackground:
                // we need to clear the background, when not rendering the full rect.
                BackingBitmapSource = RenderInFull ? null : ClearFrame(WriteableBitmap, FullRect, new System.Windows.Rect(SourceLeft, SourceTop, SourceWidth, SourceHeight));
                break;

              case WpfImageFrameDisposalMethod.RestorePrevious:
                // Reuse the same backing frame.
                break;
            }
          }
        }
        else
        {
          // multi-page images such as tiff.
          for (var SourceIndex = 0; SourceIndex < SourceList.Count; SourceIndex++)
          {
            var Source = SourceList[SourceIndex];

            var WriteableBitmap = new System.Windows.Media.Imaging.WriteableBitmap(Source);
            WriteableBitmap.Freeze();

            IndividualArray[SourceIndex] = new WpfImageFrame(WriteableBitmap, AnimationKeytimeMS: 0);

            if (FirstBitmapSource == null)
              this.FirstBitmapSource = WriteableBitmap;
          }
        }

        this.IndividualFrames = IndividualArray;
      }
    }

    private static System.Windows.Media.Imaging.WriteableBitmap ClearFrame(System.Windows.Media.Imaging.WriteableBitmap SourceBitmap, System.Windows.Rect FullRect, System.Windows.Rect ClearRect)
    {
      var DrawingVisual = new System.Windows.Media.DrawingVisual();

      using (var DrawingContext = DrawingVisual.RenderOpen())
      {
        var DrawingClip = System.Windows.Media.Geometry.Combine(new System.Windows.Media.RectangleGeometry(FullRect), new System.Windows.Media.RectangleGeometry(ClearRect), System.Windows.Media.GeometryCombineMode.Exclude, null);
        DrawingContext.PushClip(DrawingClip);
        DrawingContext.DrawImage(SourceBitmap, FullRect);
      }

      var RenderBitmap = new System.Windows.Media.Imaging.RenderTargetBitmap(SourceBitmap.PixelWidth, SourceBitmap.PixelHeight, SourceBitmap.DpiX, SourceBitmap.DpiY, System.Windows.Media.PixelFormats.Pbgra32);
      RenderBitmap.Render(DrawingVisual);
      RenderBitmap.Freeze();
      RenderBitmap.ReleaseLooseGDIHandles();

      var Result = new System.Windows.Media.Imaging.WriteableBitmap(RenderBitmap);
      Result.Freeze();
      return Result;
    }

    public WpfImageType Type { get; }
    public System.Windows.Media.Imaging.BitmapSource FirstBitmapSource { get; }
    public IReadOnlyList<WpfImageFrame> IndividualFrames { get; }
    public int TotalAnimationDelayMS { get; }

    public static WpfImage Decode(System.IO.Stream Stream)
    {
      var Decoder = System.Windows.Media.Imaging.BitmapDecoder.Create(Stream, System.Windows.Media.Imaging.BitmapCreateOptions.None, System.Windows.Media.Imaging.BitmapCacheOption.OnLoad);

      WpfImage New(WpfImageType Type) => new WpfImage(Type, Decoder.Frames);

      // checked in order of likelihood.
      if (Decoder is System.Windows.Media.Imaging.PngBitmapDecoder)
        return New(WpfImageType.Png);
      else if (Decoder is System.Windows.Media.Imaging.JpegBitmapDecoder)
        return New(WpfImageType.Jpg);
      else if (Decoder is System.Windows.Media.Imaging.TiffBitmapDecoder)
        return New(WpfImageType.Tiff);
      else if (Decoder is System.Windows.Media.Imaging.GifBitmapDecoder)
        return New(WpfImageType.Gif);
      else if (Decoder is System.Windows.Media.Imaging.BmpBitmapDecoder)
        return New(WpfImageType.Bmp);
      else if (Decoder is System.Windows.Media.Imaging.IconBitmapDecoder)
        return New(WpfImageType.Ico);
      else if (Decoder is System.Windows.Media.Imaging.WmpBitmapDecoder)
        return New(WpfImageType.Wmp);
      else
        return New(WpfImageType.Unknown);
    }

    public Inv.Image Encode()
    {
      using (var MemoryStream = new System.IO.MemoryStream())
      {
        System.Windows.Media.Imaging.BitmapEncoder Encoder = Type switch
        {
          WpfImageType.Png => new System.Windows.Media.Imaging.PngBitmapEncoder(),
          WpfImageType.Jpg => new System.Windows.Media.Imaging.JpegBitmapEncoder(),
          WpfImageType.Tiff => new System.Windows.Media.Imaging.TiffBitmapEncoder(),
          WpfImageType.Gif => NewFixedGifBitmapEncoder(),
          WpfImageType.Bmp => new System.Windows.Media.Imaging.BmpBitmapEncoder(),
          //WpfImageType.Ico => new System.Windows.Media.Imaging.IconBitmapEncoder(), // TODO: does not exist in WPF?
          WpfImageType.Wmp => new System.Windows.Media.Imaging.WmpBitmapEncoder(),
          _ => throw EnumHelper.UnexpectedValueException(Type)
        };

        if (IndividualFrames == null)
        {
          Encoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(FirstBitmapSource));
        }
        else
        {
          var PreviousKeytimeMS = 0;

          foreach (var Frame in IndividualFrames)
          {
            System.Windows.Media.Imaging.BitmapMetadata FrameMetadata;
            if (TotalAnimationDelayMS > 0)
            {
              var FrameDelay = (ushort)((Frame.AnimationKeytimeMS - PreviousKeytimeMS) / 10);
              PreviousKeytimeMS = Frame.AnimationKeytimeMS;

              FrameMetadata = new System.Windows.Media.Imaging.BitmapMetadata(Type.ToString().ToLowerInvariant());
              FrameMetadata.SetQuery(GifDelayQuery, FrameDelay);
              //FrameMetadata.SetQuery(GifDisposalQuery, (byte)WpfImageFrameDisposalMethod.RestoreBackground);
            }
            else
            {
              FrameMetadata = null;
            }

            Encoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(Frame.BitmapSource, thumbnail: null, metadata: FrameMetadata, colorContexts: null));
          }
        }

        Encoder.Save(MemoryStream);

        var Result = new Inv.Image(MemoryStream.ToArray(), "." + Type.ToString().ToLowerInvariant());

        Result.Node = this;

        return Result;
      }
    }
    public WpfImage Convert(Func<System.Windows.Media.Imaging.BitmapSource, System.Windows.Media.Imaging.BitmapSource> FrameFunction)
    {
      var WpfConvertArray = new System.Windows.Media.Imaging.BitmapFrame[IndividualFrames == null ? 1 : IndividualFrames.Count];

      for (var FrameIndex = 0; FrameIndex < WpfConvertArray.Length; FrameIndex++)
      {
        var WpfSource = IndividualFrames == null ? FirstBitmapSource : IndividualFrames[FrameIndex].BitmapSource;
        var WpfMetadata = WpfSource.Metadata as System.Windows.Media.Imaging.BitmapMetadata;

        var WpfConvert = System.Windows.Media.Imaging.BitmapFrame.Create(FrameFunction(WpfSource), thumbnail: null, WpfMetadata, colorContexts: null);
        WpfConvert.Freeze();

        WpfConvertArray[FrameIndex] = WpfConvert;
      }

      return new WpfImage(Type, WpfConvertArray);
    }

    private System.Windows.Media.Imaging.GifBitmapEncoder NewFixedGifBitmapEncoder()
    {
      var Encoder = new System.Windows.Media.Imaging.GifBitmapEncoder();

      var EncoderType = typeof(System.Windows.Media.Imaging.GifBitmapEncoder);

      EncoderType.GetField("_supportsFrameMetadata", BindingFlags.NonPublic | BindingFlags.Instance)?.SetValue(Encoder, true);
      EncoderType.GetField("_supportsGlobalMetadata", BindingFlags.NonPublic | BindingFlags.Instance)?.SetValue(Encoder, true);

      return Encoder;
    }

    private const string GifDelayQuery = "/grctlext/Delay";
    private const string GifDisposalQuery = "/grctlext/Disposal";
    private const string GifLeftQuery = "/imgdesc/Left";
    private const string GifTopQuery = "/imgdesc/Top";
    private const string GifWidthQuery = "/imgdesc/Width";
    private const string GifHeightQuery = "/imgdesc/Height";
  }

  public sealed class WpfImageFrame
  {
    internal WpfImageFrame(System.Windows.Media.Imaging.BitmapSource BitmapSource, int AnimationKeytimeMS)
    {
      this.BitmapSource = BitmapSource;
      this.AnimationKeytimeMS = AnimationKeytimeMS;
    }

    public System.Windows.Media.Imaging.BitmapSource BitmapSource { get; }
    public int AnimationKeytimeMS { get; }
  }

  public enum WpfImageFrameDisposalMethod
  {
    None = 0,
    DoNotDispose = 1,
    RestoreBackground = 2,
    RestorePrevious = 3
  }

  public sealed class WpfBlock : WpfClipper
  {
    internal WpfBlock()
    {
      this.Inner = new System.Windows.Controls.TextBlock()
      {
        VerticalAlignment = System.Windows.VerticalAlignment.Center,
        TextTrimming = System.Windows.TextTrimming.CharacterEllipsis,
      };
      Inner.SizeChanged += (Sender, Event) => Inv.WpfFoundation.CheckImplicitTooltip(Inner);
      Inner.Loaded += (Sender, Event) => Inv.WpfFoundation.CheckImplicitTooltip(Inner);

      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }
    public System.Windows.TextAlignment TextAlignment
    {
      get => Inner.TextAlignment;
      set => Inner.TextAlignment = value;
    }
    public System.Windows.TextWrapping TextWrapping
    {
      get => Inner.TextWrapping;
      set => Inner.TextWrapping = value;
    }
    public System.Windows.TextTrimming TextTrimming
    {
      get => Inner.TextTrimming;
      set => Inner.TextTrimming = value;
    }
    public System.Windows.Media.Brush Foreground
    {
      get => Inner.Foreground;
      set => Inner.Foreground = value;
    }
    public double FontSize
    {
      get => Inner.FontSize;
      set => Inner.FontSize = value;
    }

    public System.Windows.Documents.Inline GetInline(Inv.BlockSpan Span)
    {
      return Span.Node as System.Windows.Documents.Inline;
    }

    public System.Windows.Controls.TextBlock AsTextBlock()
    {
      return Inner;
    }

    private readonly System.Windows.Controls.TextBlock Inner;
  }

  public sealed class WpfLabel : WpfClipper
  {
    internal WpfLabel()
    {
      this.Inner = new System.Windows.Controls.TextBlock()
      {
        VerticalAlignment = System.Windows.VerticalAlignment.Center,
        TextTrimming = System.Windows.TextTrimming.CharacterEllipsis,
      };
      Inner.SizeChanged += (Sender, Event) => Inv.WpfFoundation.CheckImplicitTooltip(Inner);
      Inner.Loaded += (Sender, Event) => Inv.WpfFoundation.CheckImplicitTooltip(Inner);

      //Debug.Assert(Inner.FontSize == 12, "FontSize is expected to be set to 12."); // TODO: this actually fails on some resolutions!

      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }
    public System.Windows.TextAlignment TextAlignment
    {
      get => Inner.TextAlignment;
      set => Inner.TextAlignment = value;
    }
    public System.Windows.TextWrapping TextWrapping
    {
      get => Inner.TextWrapping;
      set => Inner.TextWrapping = value;
    }
    public System.Windows.TextTrimming TextTrimming
    {
      get => Inner.TextTrimming;
      set => Inner.TextTrimming = value;
    }
    public System.Windows.Media.Brush Foreground
    {
      get => Inner.Foreground;
      set => Inner.Foreground = value;
    }
    public double FontSize
    {
      get => Inner.FontSize;
      set => Inner.FontSize = value;
    }
    public string Text
    {
      get => Inner.Text;
      set
      {
        Inner.Text = value;
        Inv.WpfFoundation.CheckImplicitTooltip(Inner);
      }
    }

    public System.Windows.Controls.TextBlock AsTextBlock()
    {
      return Inner;
    }

    private readonly System.Windows.Controls.TextBlock Inner;
  }

  public sealed class WpfEdit : WpfClipper, WpfOverrideFocusContract
  {
    internal WpfEdit(bool SearchControl, bool PasswordMask)
    {
      if (SearchControl)
      {
        var LayoutDock = new System.Windows.Controls.DockPanel();
        base.Child = LayoutDock;
        LayoutDock.LastChildFill = true;

        // TODO: left search image watermark.

        var ClearButton = new WpfButton();
        LayoutDock.Children.Add(ClearButton);
        System.Windows.Controls.DockPanel.SetDock(ClearButton, System.Windows.Controls.Dock.Right);
        ClearButton.Focusable = false;
        ClearButton.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
        ClearButton.VerticalContentAlignment = System.Windows.VerticalAlignment.Center;
        ClearButton.Visibility = System.Windows.Visibility.Collapsed;
        ClearButton.Padding = new System.Windows.Thickness(1);

        var ClearPath = new System.Windows.Shapes.Path();
        ClearPath.Data = System.Windows.Media.Geometry.Parse("M0,0 L1,1 M0,1 L1,0");
        ClearPath.Margin = new System.Windows.Thickness(1);
        ClearPath.Stretch = System.Windows.Media.Stretch.UniformToFill;
        ClearPath.StrokeThickness = 2;
        ClearPath.StrokeStartLineCap = System.Windows.Media.PenLineCap.Round;
        ClearPath.StrokeEndLineCap = System.Windows.Media.PenLineCap.Round;
        ClearButton.Content = ClearPath;

        ClearButton.Click += (Sender, Event) =>
        {
          InnerSearchBox.Text = ""; // fires change event.
        };

        this.InnerSearchBox = new System.Windows.Controls.TextBox()
        {
          IsReadOnlyCaretVisible = true,
          AcceptsReturn = false,
          AcceptsTab = false,
          TextWrapping = System.Windows.TextWrapping.NoWrap,
          Background = System.Windows.Media.Brushes.Transparent,
          BorderBrush = System.Windows.Media.Brushes.Transparent,
          BorderThickness = new System.Windows.Thickness(0),
          Padding = new System.Windows.Thickness(-2, 0, 0, 0) // fixes leading indent.
        };
        InnerSearchBox.GotFocus += (Sender, Event) => GotFocusInvoke(Sender, Event);
        InnerSearchBox.LostFocus += (Sender, Event) => LostFocusInvoke(Sender, Event);

        var CurrentForegroundColor = (System.Windows.Media.Color?)null;

        InnerSearchBox.TextChanged += (Sender, Event) =>
        {
          var IsEmpty = string.IsNullOrEmpty(InnerSearchBox.Text);
          ClearButton.Visibility = IsEmpty ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;

          if (!IsEmpty)
          {
            ClearButton.Width = InnerSearchBox.FontSize;
            ClearButton.Height = ClearButton.Width;
            ClearButton.CornerRadius = new System.Windows.CornerRadius(InnerSearchBox.FontSize / 2);

            var ForegroundColor = (InnerSearchBox.Foreground as System.Windows.Media.SolidColorBrush)?.Color ?? System.Windows.Media.Colors.Black;

            if (CurrentForegroundColor != ForegroundColor)
            {
              CurrentForegroundColor = ForegroundColor;

              var HoverBrush = new System.Windows.Media.SolidColorBrush(ForegroundColor) { Opacity = 0.20F };
              HoverBrush.Freeze();

              var PressedBrush = new System.Windows.Media.SolidColorBrush(ForegroundColor) { Opacity = 0.40F };
              PressedBrush.Freeze();

              ClearButton.AsFlat(InnerSearchBox.Background, HoverBrush, PressedBrush);
            }

            ClearPath.Stroke = InnerSearchBox.Foreground;
          }

          TextChangedInvoke(Sender, Event);
        };
        InnerSearchBox.PreviewKeyDown += (Sender, Event) => KeyPressInvoke(Sender, Event);
        InnerSearchBox.KeyDown += (Sender, Event) =>
        {
          if (Event.Key == System.Windows.Input.Key.Escape)
          {
            if (!string.IsNullOrEmpty(InnerSearchBox.Text))
            {
              InnerSearchBox.Text = ""; // fires change event.

              Event.Handled = true;
            }
          }
        };
        LayoutDock.Children.Add(InnerSearchBox);
      }
      else if (PasswordMask)
      {
        this.InnerPasswordBox = new System.Windows.Controls.PasswordBox()
        {
          Background = System.Windows.Media.Brushes.Transparent,
          BorderBrush = System.Windows.Media.Brushes.Transparent,
          BorderThickness = new System.Windows.Thickness(0),
          Padding = new System.Windows.Thickness(-2, 0, 0, 0),
        };
        InnerPasswordBox.PasswordChanged += (Sender, Event) => TextChangedInvoke(Sender, Event);
        InnerPasswordBox.GotFocus += (Sender, Event) => GotFocusInvoke(Sender, Event);
        InnerPasswordBox.LostFocus += (Sender, Event) => LostFocusInvoke(Sender, Event);
        InnerPasswordBox.PreviewKeyDown += (Sender, Event) => KeyPressInvoke(Sender, Event);
        base.Child = InnerPasswordBox;
      }
      else
      {
        this.InnerTextBox = new System.Windows.Controls.TextBox()
        {
          IsReadOnlyCaretVisible = true,
          AcceptsReturn = false,
          AcceptsTab = false,
          TextWrapping = System.Windows.TextWrapping.NoWrap,
          Background = System.Windows.Media.Brushes.Transparent,
          BorderBrush = System.Windows.Media.Brushes.Transparent,
          Margin = new System.Windows.Thickness(0),
          Padding = new System.Windows.Thickness(-2, 0, 0, 0), // fixes leading indent.
          BorderThickness = new System.Windows.Thickness(0),
        };
        InnerTextBox.TextChanged += (Sender, Event) => TextChangedInvoke(Sender, Event);
        InnerTextBox.GotFocus += (Sender, Event) => GotFocusInvoke(Sender, Event);
        InnerTextBox.LostFocus += (Sender, Event) => LostFocusInvoke(Sender, Event);
        InnerTextBox.PreviewKeyDown += (Sender, Event) => KeyPressInvoke(Sender, Event);
        base.Child = InnerTextBox;
      }
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }
    public bool IsReadOnly
    {
      get
      {
        if (InnerTextBox != null)
          return InnerTextBox.IsReadOnly;
        else if (InnerPasswordBox != null)
          return !InnerPasswordBox.IsEnabled;
        else if (InnerSearchBox != null)
          return InnerSearchBox.IsReadOnly;
        else
          return false;
      }
      set
      {
        if (InnerTextBox != null)
          InnerTextBox.IsReadOnly = value;
        else if (InnerPasswordBox != null)
          InnerPasswordBox.IsEnabled = !value;
        else if (InnerSearchBox != null)
          InnerSearchBox.IsReadOnly = value;
      }
    }
    public string Text
    {
      get
      {
        if (InnerTextBox != null)
          return InnerTextBox.Text;
        else if (InnerPasswordBox != null)
          return InnerPasswordBox.Password;
        else if (InnerSearchBox != null)
          return InnerSearchBox.Text;
        else
          return null;
      }
      set
      {
        if (InnerTextBox != null)
          InnerTextBox.Text = value;
        else if (InnerPasswordBox != null)
          InnerPasswordBox.Password = value;
        else if (InnerSearchBox != null)
          InnerSearchBox.Text = value;
      }
    }
    public new event System.Windows.RoutedEventHandler GotFocus;
    public new event System.Windows.RoutedEventHandler LostFocus;
    public event System.Windows.RoutedEventHandler TextChanged;
    public event System.Windows.Input.KeyEventHandler KeyPress;
    public System.Windows.Media.Brush Foreground
    {
      get => AsControl()?.Foreground;
      set
      {
        var Control = AsControl();
        if (Control != null)
          Control.Foreground = value;
      }
    }

    public System.Windows.Controls.TextBox AsTextBox() => InnerTextBox;
    public System.Windows.Controls.PasswordBox AsPasswordBox() => InnerPasswordBox;
    public System.Windows.Controls.TextBox AsSearchBox() => InnerSearchBox;
    public new void Focus() => AsControl()?.Focus();

    private System.Windows.Controls.Control AsControl()
    {
      if (InnerTextBox != null)
        return InnerTextBox;

      if (InnerPasswordBox != null)
        return InnerPasswordBox;

      if (InnerSearchBox != null)
        return InnerSearchBox;

      return null;
    }
    private void TextChangedInvoke(object Sender, System.Windows.RoutedEventArgs EventArgs) => TextChanged?.Invoke(Sender, EventArgs);
    private void GotFocusInvoke(object Sender, System.Windows.RoutedEventArgs EventArgs) => GotFocus?.Invoke(Sender, EventArgs);
    private void LostFocusInvoke(object Sender, System.Windows.RoutedEventArgs EventArgs) => LostFocus?.Invoke(Sender, EventArgs);
    private void KeyPressInvoke(object Sender, System.Windows.Input.KeyEventArgs EventArgs) => KeyPress?.Invoke(Sender, EventArgs);

    void WpfOverrideFocusContract.OverrideFocus() => Focus();

    private readonly System.Windows.Controls.TextBox InnerTextBox;
    private readonly System.Windows.Controls.PasswordBox InnerPasswordBox;
    private readonly System.Windows.Controls.TextBox InnerSearchBox;
  }

  public sealed class WpfMemo : WpfClipper, WpfOverrideFocusContract
  {
    internal WpfMemo()
    {
      SetPlainText(null);
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }
    public bool IsReadOnly
    {
      get => IsReadOnlyField;
      set
      {
        this.IsReadOnlyField = value;

        if (RichTextBox != null)
        {
          RichTextBox.IsReadOnly = IsReadOnlyField;
          RichTextBox.SpellCheck.IsEnabled = !IsReadOnlyField;
        }
        else if (PlainTextBox != null)
        {
          PlainTextBox.IsReadOnly = IsReadOnlyField;
          PlainTextBox.SpellCheck.IsEnabled = !IsReadOnlyField;
        }
      }
    }
    public string Text { get; private set; }
    public bool IsPlainText => PlainTextBox != null;
    public bool IsRichText => RichTextBox != null;
    public event System.Windows.Controls.TextChangedEventHandler TextChanged;

    public void SetPlainText(string Text)
    {
      this.Text = Text;

      if (PlainTextBox == null)
      {
        this.PlainTextBox = new System.Windows.Controls.TextBox()
        {
          AcceptsReturn = true,
          AcceptsTab = false, // NOTE: this is false because of keyboard navigation (tab, alt+tab), it could be an option if ever required.
          TextWrapping = System.Windows.TextWrapping.Wrap,
          IsReadOnlyCaretVisible = true,
          VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto,
          HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto,
          Background = System.Windows.Media.Brushes.Transparent,
          BorderBrush = System.Windows.Media.Brushes.Transparent,
          BorderThickness = new System.Windows.Thickness(0),
          Padding = new System.Windows.Thickness(-2, 0, 0, 0), // fixes leading indent.
          IsReadOnly = IsReadOnlyField
        };
        base.Child = PlainTextBox;
        PlainTextBox.SpellCheck.IsEnabled = !IsReadOnlyField;
        PlainTextBox.TextChanged += TextChangedInvoke;
      }

      if (RichTextBox != null)
      {
        PlainTextBox.FontFamily = RichTextBox.FontFamily;
        PlainTextBox.FontSize = RichTextBox.FontSize;
        PlainTextBox.FontWeight = RichTextBox.FontWeight;
        PlainTextBox.Foreground = RichTextBox.Foreground;

        this.RichTextBox = null;
      }

      PlainTextBox.Text = Text;
    }
    public void SetRichText(string Text)
    {
      this.Text = Text;

      if (RichTextBox == null)
      {
        this.RichTextBox = new System.Windows.Controls.RichTextBox()
        {
          AcceptsReturn = true,
          AcceptsTab = false, // NOTE: this is false because of keyboard navigation (tab, alt+tab), it could be an option if ever required.
          //TextWrapping = System.Windows.TextWrapping.Wrap,
          IsReadOnlyCaretVisible = true,
          VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto,
          HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto,
          Background = System.Windows.Media.Brushes.Transparent,
          BorderBrush = System.Windows.Media.Brushes.Transparent,
          BorderThickness = new System.Windows.Thickness(0),
          Margin = new System.Windows.Thickness(0),
          Padding = new System.Windows.Thickness(-4, 0, 0, 0), // fixes leading padding.
          HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch,
          VerticalAlignment = System.Windows.VerticalAlignment.Stretch,
          IsReadOnly = IsReadOnlyField
        };
        base.Child = RichTextBox;
        RichTextBox.SpellCheck.IsEnabled = !IsReadOnlyField;
        RichTextBox.TextChanged += TextChangedInvoke;

        // <FlowDocument Name="rtbFlowDoc" PageWidth="{Binding ElementName=rtb, Path=ActualWidth}" />
        var x = new System.Windows.Data.Binding("ActualWidth");
        x.Source = RichTextBox;
        RichTextBox.Document.SetBinding(System.Windows.Documents.FlowDocument.PageWidthProperty, x);

        var Style = new System.Windows.Style(typeof(System.Windows.Documents.Paragraph));
        Style.Setters.Add(new System.Windows.Setter(System.Windows.Documents.Paragraph.MarginProperty, new System.Windows.Thickness(0)));
        RichTextBox.Resources.Add(typeof(System.Windows.Documents.Paragraph), Style);

        RichTextBox.Document.PagePadding = new System.Windows.Thickness(0);
      }

      if (PlainTextBox != null)
      {
        RichTextBox.FontFamily = PlainTextBox.FontFamily;
        RichTextBox.FontSize = PlainTextBox.FontSize;
        RichTextBox.FontWeight = PlainTextBox.FontWeight;
        RichTextBox.Foreground = PlainTextBox.Foreground;

        this.PlainTextBox = null;
      }

      new System.Windows.Documents.TextRange(RichTextBox.Document.ContentStart, RichTextBox.Document.ContentEnd).Text = Text;
    }
    public System.Windows.Controls.TextBox AsPlainTextBox()
    {
      return PlainTextBox;
    }
    public System.Windows.Controls.RichTextBox AsRichTextBox()
    {
      return RichTextBox;
    }

    internal System.Windows.Documents.TextRange GetRichTextRange(int Index, int Count)
    {
      // NOTE: this is slow but no obvious way to optimise and still get the correct behaviour.
      var WpfStartPointer = GetTextPoint(RichTextBox.Document.ContentStart, Index);
      var WpfEndPointer = GetTextPoint(RichTextBox.Document.ContentStart, Index + Count);

      return new System.Windows.Documents.TextRange(WpfStartPointer, WpfEndPointer);
    }

    protected override void OnManipulationBoundaryFeedback(System.Windows.Input.ManipulationBoundaryFeedbackEventArgs e)
    {
      e.Handled = true; // prevent weird scrolling 'window bounce' when using touch input.
    }

    private void TextChangedInvoke(object Sender, System.Windows.Controls.TextChangedEventArgs Event)
    {
      if (RichTextBox != null)
        this.Text = new System.Windows.Documents.TextRange(RichTextBox.Document.ContentStart, RichTextBox.Document.ContentEnd).Text;
      else if (PlainTextBox != null)
        this.Text = PlainTextBox.Text;

      TextChanged?.Invoke(Sender, Event);
    }

    private static System.Windows.Documents.TextPointer GetTextPoint(System.Windows.Documents.TextPointer start, int x)
    {
      var ret = start;
      var i = 0;
      while (ret != null)
      {
        string stringSoFar = new System.Windows.Documents.TextRange(ret, ret.GetPositionAtOffset(i, System.Windows.Documents.LogicalDirection.Forward)).Text;
        if (stringSoFar.Length == x)
          break;
        i++;
        if (ret.GetPositionAtOffset(i, System.Windows.Documents.LogicalDirection.Forward) == null)
          return ret.GetPositionAtOffset(i - 1, System.Windows.Documents.LogicalDirection.Forward);
      }
      ret = ret.GetPositionAtOffset(i, System.Windows.Documents.LogicalDirection.Forward);
      return ret;
    }

    void WpfOverrideFocusContract.OverrideFocus()
    {
      if (RichTextBox != null)
        RichTextBox.Focus();
      else if (PlainTextBox != null)
        PlainTextBox.Focus();
    }

    private System.Windows.Controls.TextBox PlainTextBox;
    private System.Windows.Controls.RichTextBox RichTextBox;
    private bool IsReadOnlyField;
  }

  public sealed class WpfShape : WpfClipper
  {
    internal WpfShape()
    {
      this.Inner = new System.Windows.Shapes.Path();
      base.Child = Inner;

      Debug.Assert(Inner.Stretch == System.Windows.Media.Stretch.None, "Required default stretch value to match Invention default.");
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }
    public System.Windows.Media.Stretch Stretch
    {
      get => Inner.Stretch;
      set => Inner.Stretch = value;
    }
    public System.Windows.Media.Brush Fill
    {
      get => Inner.Fill;
      set => Inner.Fill = value;
    }
    public System.Windows.Media.Brush Stroke
    {
      get => Inner.Stroke;
      set => Inner.Stroke = value;
    }
    public double StrokeThickness
    {
      get => Inner.StrokeThickness;
      set => Inner.StrokeThickness = value;
    }
    public System.Windows.Media.PenLineCap StrokeStartLineCap
    {
      get => Inner.StrokeStartLineCap;
      set => Inner.StrokeStartLineCap = value;
    }
    public System.Windows.Media.PenLineCap StrokeEndLineCap
    {
      get => Inner.StrokeEndLineCap;
      set => Inner.StrokeEndLineCap = value;
    }
    public System.Windows.Media.PenLineJoin StrokeLineJoin
    {
      get => Inner.StrokeLineJoin;
      set => Inner.StrokeLineJoin = value;
    }
    public System.Windows.Media.DoubleCollection StrokeDashArray
    {
      get => Inner.StrokeDashArray;
      set => Inner.StrokeDashArray = value;
    }

    public void Set(System.Windows.Media.Geometry Geometry, int NotionalWidth, int NotionalHeight)
    {
      Inner.Data = Geometry;
      this.NotionalWidth = NotionalWidth;
      this.NotionalHeight = NotionalHeight;
    }

    protected override System.Windows.Size MeasureOverride(System.Windows.Size constraint)
    {
      base.MeasureOverride(constraint); // there must be private state that need to be initialised, even though we ignore the result size.

      var MeasureWidth = (double)NotionalWidth;
      var MeasureHeight = (double)NotionalHeight;

      if (MeasureWidth > constraint.Width)
        MeasureWidth = constraint.Width;

      if (MeasureHeight > constraint.Height)
        MeasureHeight = constraint.Height;

      return new System.Windows.Size(MeasureWidth, MeasureHeight);
    }

    private readonly System.Windows.Shapes.Path Inner;
    private int NotionalWidth;
    private int NotionalHeight;
  }

  public sealed class WpfCalendar : System.Windows.Controls.Calendar
  {
    internal WpfCalendar(WpfHost Host)
    {
      this.Host = Host;

      Margin = new System.Windows.Thickness(0);
      Padding = new System.Windows.Thickness(0);
      BorderThickness = new System.Windows.Thickness(0);
      SelectionMode = System.Windows.Controls.CalendarSelectionMode.SingleDate;
      LayoutTransform = new System.Windows.Media.ScaleTransform(1.25, 1.25);
      DisplayDateStart = new System.DateTime(1800, 1, 1);
      DisplayDateEnd = new System.DateTime(2199, 12, 31);

      this.DisplayDateStart = new System.DateTime(1800, 1, 1);
      this.DisplayDateEnd = new System.DateTime(2199, 12, 31);
      this.LayoutTransform = new System.Windows.Media.ScaleTransform(1.25, 1.25);

      var ResourceDictionary = WpfShell.LoadResourceDictionary("InvWpfMaterialCalendar.xaml");

      if (ResourceDictionary != null)
      {
        var OverrideStyle = (System.Windows.Style)ResourceDictionary["MaterialDesignCalendarPortrait"];
        OverrideStyle.Seal();

        this.Style = OverrideStyle;
      }

      IsDark = false;
    }

    public bool IsDark
    {
      get => (bool)GetValue(IsDarkProperty);
      set
      {
        SetValue(IsDarkProperty, value);

        SetBackgroundBrush(value);
        SetFontBrush(value);
      }
    }

    protected override void OnPreviewMouseUp(System.Windows.Input.MouseButtonEventArgs e)
    {
      base.OnPreviewMouseUp(e);

      // NOTE: workaround for a strange focus issue, where you have to click twice to 'get out' of the calendar.
      if (System.Windows.Input.Mouse.Captured is System.Windows.Controls.Primitives.CalendarItem)
        System.Windows.Input.Mouse.Capture(null);
    }

    private void SetBackgroundBrush(bool IsDark)
    {
      Background = IsDark ? Host.TranslateBrush(Inv.Colour.GraySmoke) : Host.TranslateBrush(Inv.Colour.WhiteSmoke);
    }
    private void SetFontBrush(bool IsDarkTheme)
    {
      System.Windows.Documents.TextElement.SetForeground(this, IsDarkTheme ? Host.TranslateBrush(Inv.Colour.White) : Host.TranslateBrush(Inv.Colour.Black));
    }

    private readonly WpfHost Host;

    static WpfCalendar()
    {
      IsDarkProperty = System.Windows.DependencyProperty.Register("IsDark", typeof(bool), typeof(System.Windows.Controls.Calendar), new System.Windows.FrameworkPropertyMetadata(false));
    }

    public static readonly System.Windows.DependencyProperty IsDarkProperty;
  }

  public sealed class WpfVideo : WpfClipper
  {
    internal WpfVideo()
    {
      this.Inner = new System.Windows.Controls.MediaElement();
      base.Child = Inner;
      Inner.LoadedBehavior = System.Windows.Controls.MediaState.Manual;
      Inner.UnloadedBehavior = System.Windows.Controls.MediaState.Manual;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }
    public void LoadUri(Uri Source)
    {
      this.SourceField = Source;
      Inner.Source = null; // have to 'play' after you set the uri.
    }

    public void Play()
    {
      if (Inner.Source != SourceField)
        Inner.Source = SourceField;

      Inner.Play();
    }
    public void Pause()
    {
      Inner.Pause();
    }
    public void Stop()
    {
      Inner.Stop();
      Inner.Close();
      Inner.Source = null; // This will close the stream attached to the media element and blank the control in the visual tree.
    }

    private readonly System.Windows.Controls.MediaElement Inner;
    private Uri SourceField;
  }

  public sealed class WpfTooltip : System.Windows.Controls.ToolTip
  {
    public WpfTooltip()
    {
      this.Background = GetDefaultBackgroundBrush();
      this.BorderBrush = GetDefaultBorderBrush();
      this.Foreground = GetDefaultForegroundBrush();
      this.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
      this.Padding = new System.Windows.Thickness(0);
      this.VerticalOffset = 5;
      this.HorizontalOffset = 5;
    }

    public void SetBackgroundBrush(System.Windows.Media.Brush Brush) => this.Background = Brush ?? GetDefaultBackgroundBrush();
    public void SetBorderBrush(System.Windows.Media.Brush Brush) => this.BorderBrush = Brush ?? GetDefaultBorderBrush();

    public static System.Windows.Media.Brush GetDefaultBackgroundBrush() => Inv.Theme.IsDark ? System.Windows.Media.Brushes.Black : System.Windows.Media.Brushes.White;
    public static System.Windows.Media.Brush GetDefaultBorderBrush() => Inv.Theme.IsDark ? System.Windows.Media.Brushes.DimGray : System.Windows.Media.Brushes.DarkGray;
    public static System.Windows.Media.Brush GetDefaultForegroundBrush() => Inv.Theme.IsDark ? System.Windows.Media.Brushes.White : System.Windows.Media.Brushes.Black;

    static WpfTooltip()
    {
      // NOTE: this globally sets all tooltips to last an infinite duration.
      System.Windows.Controls.ToolTipService.ShowDurationProperty.OverrideMetadata(typeof(System.Windows.DependencyObject), new System.Windows.FrameworkPropertyMetadata(int.MaxValue));
    }
  }

  public sealed class WpfPopup
  {
    public WpfPopup()
    {
      this.Container = new System.Windows.Controls.ContentControl()
      {
        Focusable = false,
        Background = System.Windows.Media.Brushes.Transparent
      };

      // NOTE: this border is necessary because the ContentControl doesn't render its own Background.
      this.Border = new System.Windows.Controls.Border()
      {
        Child = Container
      };

      // 2019-08-07 KV
      // WPF Popups with AllowsTransparency enabled do not function correctly when gdiScaling is on in the application manifest.
      this.Base = new System.Windows.Controls.Primitives.Popup()
      {
        AllowsTransparency = false,
        UseLayoutRounding = true,
        SnapsToDevicePixels = true,
        StaysOpen = false,
        PopupAnimation = System.Windows.Controls.Primitives.PopupAnimation.None,
        Child = Border
      };

      Base.Opened += (Sender, Event) =>
      {
        ShowEvent?.Invoke();
      };
      Base.Closed += (Sender, Event) =>
      {
        HideEvent?.Invoke();
      };
      Base.KeyDown += (Sender, Event) =>
      {
        if (Event.Key == System.Windows.Input.Key.Escape)
        {
          Hide();

          Event.Handled = true;
        }
      };
    }

    public bool IsOpen => Base.IsOpen;
    public bool IsMouseOver => Base.IsMouseOver;
    public System.Windows.Media.Brush Background
    {
      set => Border.Background = value;
    }
    public System.Windows.Media.FontFamily FontFamily
    {
      set => Container.FontFamily = value;
    }
    public System.Windows.Controls.Primitives.PlacementMode PlacementMode
    {
      set => Base.Placement = value;
    }
    public System.Windows.FrameworkElement PlacementTarget
    {
      set => Base.PlacementTarget = value;
    }
    public System.Windows.FrameworkElement Content
    {
      set => Container.Content = value;
    }
    public event System.Action ShowEvent;
    public event System.Action HideEvent;

    public void Show()
    {
      if (!Base.IsOpen)
        Base.IsOpen = true;
    }
    public void Hide()
    {
      if (Base.IsOpen)
        Base.IsOpen = false;
    }

    private readonly System.Windows.Controls.Primitives.Popup Base;
    private readonly System.Windows.Controls.ContentControl Container;
    private readonly System.Windows.Controls.Border Border;
  }

  public static class WpfFoundation
  {
    static WpfFoundation()
    {
      TextBlockCheckFlagsMethod = typeof(System.Windows.Controls.TextBlock)?.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Find(M => M.IsPrivate && M.Name == "CheckFlags");
    }

    public static readonly HashSet<System.Windows.Input.MouseWheelEventArgs> MouseWheelEventArgsList = [];

    public static void CheckImplicitTooltip(System.Windows.Controls.TextBlock WpfTextBlock)
    {
      // TODO: this check doesn't seem to protect against any problems but does cause some update issues with a fixed width label getting updated text [CH 2023-02-13].
      //if (!WpfTextBlock.IsMeasureValid)
      //  return;

      var ShowTooltip = true;

      var CheckElement = WpfTextBlock.Parent as System.Windows.FrameworkElement;
      while (CheckElement != null)
      {
        if (CheckElement.ToolTip != null && !((CheckElement.ToolTip as WpfTooltip)?.Content is ImplicitTooltipText))
        {
          ShowTooltip = false; // if a parent has an explicit tooltip, we don't want to measure.
          break;
        }

        CheckElement = CheckElement.Parent as System.Windows.FrameworkElement;
      }

      var IsWrapping = WpfTextBlock.TextWrapping != System.Windows.TextWrapping.NoWrap;

      if (ShowTooltip)
      {
        var HasFlagReflection = false;

        if (TextBlockCheckFlagsMethod != null)
        {
          var HasParagraphEllipsesFlag = TextBlockCheckFlagsMethod.Invoke(WpfTextBlock, [TextBlockFlags.HasParagraphEllipses]);
          if (HasParagraphEllipsesFlag != null && HasParagraphEllipsesFlag is bool && (bool)HasParagraphEllipsesFlag)
          {
            // The TextBlock control has the 'HasParagraphEllipses' flag in it's private _flags list
            // when the text is wrapping over multiple lines and also truncating to ellipsis.

            ShowTooltip = true;
            HasFlagReflection = true;
          }
        }

        if (!HasFlagReflection)
        {
          if (IsWrapping)
          {
            // For wrapping text blocks, DesiredSize already reflects the size required to render all the text.

            ShowTooltip = WpfTextBlock.ActualHeight < WpfTextBlock.DesiredSize.Height;
          }
          else
          {
            // For single line text blocks, if there is insufficient width and the text is trimmed,
            //  DesiredSize won't reflect the actual required width, so remeasure with infinite bounds
            //  to determine the required width.

            WpfTextBlock.Measure(new System.Windows.Size(double.PositiveInfinity, double.PositiveInfinity));
            ShowTooltip = WpfTextBlock.ActualWidth < WpfTextBlock.DesiredSize.Width;
          }
        }
      }

      if (ShowTooltip)
      {
        var WpfTooltip = WpfTextBlock.ToolTip as WpfTooltip;

        // Only create the tooltip the first time because this code is called in the size changed event.
        if (WpfTooltip == null)
        {
          var TooltipOffset = -(TooltipPadding + TooltipBorder);

          WpfTooltip = new WpfTooltip()
          {
            UseLayoutRounding = true,
            SnapsToDevicePixels = true,
            //Padding = new Thickness(TooltipPadding), // don't apply padding here, as it has to only be applied on the 'topmost' control to ensure correct backgrounds.
            BorderThickness = new System.Windows.Thickness(TooltipBorder),
            Placement = System.Windows.Controls.Primitives.PlacementMode.Custom,
            VerticalOffset = 0,
            HorizontalOffset = 0,
            CustomPopupPlacementCallback = (PopupSize, TargetSize, Offset) =>
            {
              // NOTE: display the tooltip directly over the textblock; could not get any other placement mode to work; the -6 offset is due to the padding and border.
              return [new System.Windows.Controls.Primitives.CustomPopupPlacement(new System.Windows.Point(TooltipOffset, TooltipOffset), System.Windows.Controls.Primitives.PopupPrimaryAxis.Horizontal)];
            },
            Content = new ImplicitTooltipContent() // required to ensure that the tooltip is recognised as an implicit tooltip.
          };

          // NOTE: the below doesn't seem to work - see the global workaround in the WpfTooltip static constructor.
          //System.Windows.Controls.ToolTipService.SetShowDuration(WpfTooltip, int.MaxValue);

          WpfTextBlock.ToolTip = WpfTooltip;

          WpfTooltip.Opened += (sender, args) =>
          {
            var BackgroundBrushList = new List<System.Windows.Media.Brush>();
            var LastBrush = null as System.Windows.Media.Brush;
            var BorderBrush = null as System.Windows.Media.Brush;
            var BackgroundCandidate = WpfTextBlock as System.Windows.DependencyObject;
            while (BackgroundCandidate != null)
            {
              var Background = null as System.Windows.Media.Brush;

              bool CheckValueSource(System.Windows.Media.Brush CheckBrush, System.Windows.ValueSource ValueSource)
              {
                // This is to handle the case where a child control declared in a XAML template has its background set by a dependency property on the owning control.
                // In this case, both the child and the parent have a valid Background, which causes an additional (incorrect) background border in the tooltip.
                // Note: this only affects cases where the background has a sub-255 alpha channel (i.e. is partially transparent) as the downstream code will drop out otherwise.

                if (ValueSource.BaseValueSource == System.Windows.BaseValueSource.ParentTemplate)
                {
                  LastBrush = CheckBrush;
                  return true;
                }

                var Result = ValueSource.BaseValueSource != System.Windows.BaseValueSource.Local || LastBrush != CheckBrush;
                LastBrush = null;
                return Result;
              }

              if (BackgroundCandidate is System.Windows.Controls.Border BackgroundBorder)
              {
                if (CheckValueSource(BackgroundBorder.Background, System.Windows.DependencyPropertyHelper.GetValueSource(BackgroundCandidate, System.Windows.Controls.Border.BackgroundProperty)))
                  Background = BackgroundBorder.Background;
              }
              else if (BackgroundCandidate is System.Windows.Controls.Control BackgroundControl)
              {
                if (CheckValueSource(BackgroundControl.Background, System.Windows.DependencyPropertyHelper.GetValueSource(BackgroundCandidate, System.Windows.Controls.Control.BackgroundProperty)))
                  Background = BackgroundControl.Background;
              }

              if (Background != null)
              {
                if (Background is System.Windows.Media.SolidColorBrush SolidBrush)
                {
                  if (!SolidBrush.Color.IsTransparent())
                  {
                    LastBrush = SolidBrush;
                    BackgroundBrushList.Add(SolidBrush);

                    if (SolidBrush.Color.A == 255)
                    {
                      // Set the border colour based on the opaque background.

                      var BackgroundColor = SolidBrush.Color.ConvertToInvColour();
                      var BorderColor = (Inv.Theme.IsDark ? BackgroundColor.Lighten(0.25F) : BackgroundColor.Darken(0.25F)).ConvertToMediaColor();

                      BorderBrush = BorderColor != null ? new System.Windows.Media.SolidColorBrush(BorderColor.Value) : null;
                      BorderBrush?.Freeze();

                      // Once an opaque colour is found, we can drop out.
                      break;
                    }
                  }
                }
                else
                {
                  // If we encounter a background that is not a solid color, then abandon the attempt to be clever as the tooltip text may end up unreadable.

                  break;
                }
              }

              BackgroundCandidate = System.Windows.Media.VisualTreeHelper.GetParent(BackgroundCandidate);
            }
            WpfTooltip.SetBackgroundBrush(BackgroundBrushList.LastOrDefault());
            WpfTooltip.SetBorderBrush(BorderBrush);

            if (WpfTooltip.Content == null || WpfTooltip.Content is ImplicitTooltipContent)
            {
              var ImplicitTooltipContent = WpfTooltip.Content as ImplicitTooltipContent;
              ImplicitTooltipContent.SetBackgroundBrushes(BackgroundBrushList.ReverseX().Skip(1));

              var ImplicitTooltipText = ImplicitTooltipContent.Text;

              bool GetHasDecoration(System.Windows.Documents.Inline Inline, System.Windows.TextDecorationCollection TextDecoration) => Inline.TextDecorations.ContainsAll(TextDecoration);
              void SetHasDecoration(System.Windows.Documents.Inline Inline, System.Windows.TextDecorationCollection TextDecoration) => Inline.TextDecorations.Add(TextDecoration);

              bool IsItalic(System.Windows.Documents.Inline Inline) => Inline.FontStyle == System.Windows.FontStyles.Italic;
              bool IsUnderline(System.Windows.Documents.Inline Inline) => GetHasDecoration(Inline, System.Windows.TextDecorations.Underline);
              bool IsStrikethrough(System.Windows.Documents.Inline Inline) => GetHasDecoration(Inline, System.Windows.TextDecorations.Strikethrough);

              bool IsContentEqualTo(System.Windows.Documents.Inline[] A, System.Windows.Documents.Inline[] B)
              {
                if (A.Length != B.Length)
                  return false;

                for (var InlineIndex = 0; InlineIndex < A.Length; InlineIndex++)
                {
                  var InlineA = A[InlineIndex];
                  var InlineB = B[InlineIndex];

                  if (InlineA.GetType() != InlineB.GetType())
                    return false;

                  if (InlineA is System.Windows.Documents.Run RunA && RunA.Text != ((System.Windows.Documents.Run)InlineB).Text)
                    return false;

                  if (InlineA.Foreground != InlineB.Foreground ||
                      InlineA.FontFamily != InlineB.FontFamily ||
                      InlineA.FontSize != InlineB.FontSize ||
                      InlineA.FontWeight != InlineB.FontWeight ||
                      InlineA.Typography.Capitals != InlineB.Typography.Capitals ||
                      IsItalic(InlineA) != IsItalic(InlineB) ||
                      IsUnderline(InlineA) != IsUnderline(InlineB) ||
                      IsStrikethrough(InlineA) != IsStrikethrough(InlineB)
                      )
                  {
                    return false;
                  }

                  // Only check the text colour if we have determined the background colour of the text block.
                  // Otherwise, we are going to apply the default text colour anyway.
                  if (BackgroundBrushList.Any() && InlineA.Foreground != InlineB.Foreground)
                    return false;
                }

                return true;
              }

              var ImplicitRunArray = ImplicitTooltipText?.Inlines.ToArray() ?? [];
              var WpfTextRunArray = WpfTextBlock.Inlines.ToArray();

              // NOTE: must change the tooltip content with the text changes to make it update the UI.
              if (ImplicitTooltipText == null || !IsContentEqualTo(ImplicitRunArray, WpfTextRunArray))
              {
                ImplicitTooltipText = new ImplicitTooltipText();
                ImplicitTooltipContent.Text = ImplicitTooltipText;

                foreach (var WpfTextInline in WpfTextBlock.Inlines)
                {
                  System.Windows.Documents.Inline ImplicitInline;

                  if (WpfTextInline is System.Windows.Documents.Run)
                  {
                    var WpfTextRun = (System.Windows.Documents.Run)WpfTextInline;

                    var ImplicitRun = new System.Windows.Documents.Run();
                    ImplicitRun.Text = WpfTextRun.Text;
                    ImplicitInline = ImplicitRun;
                  }
                  else if (WpfTextInline is System.Windows.Documents.LineBreak)
                  {
                    ImplicitInline = new System.Windows.Documents.LineBreak();
                  }
                  else
                  {
                    continue; // unsupported inline type, ignore.
                  }

                  ImplicitTooltipText.Inlines.Add(ImplicitInline);

                  ImplicitInline.Foreground = BackgroundBrushList.Any() ? WpfTextInline.Foreground : WpfTooltip.GetDefaultForegroundBrush();
                  ImplicitInline.FontFamily = WpfTextInline.FontFamily;
                  ImplicitInline.FontSize = WpfTextInline.FontSize;
                  ImplicitInline.FontWeight = WpfTextInline.FontWeight;
                  ImplicitInline.Typography.Capitals = WpfTextInline.Typography.Capitals;
                  ImplicitInline.FontStyle = WpfTextInline.FontStyle == System.Windows.FontStyles.Italic ? System.Windows.FontStyles.Italic : System.Windows.FontStyles.Normal;

                  if (IsUnderline(WpfTextInline))
                    SetHasDecoration(ImplicitInline, System.Windows.TextDecorations.Underline);

                  if (IsStrikethrough(WpfTextInline))
                    SetHasDecoration(ImplicitInline, System.Windows.TextDecorations.Strikethrough);
                }
              }

              // KJV 2022-02-24
              // The +1 here resolves an issue where the text wrapping differs between the underlying text block and
              //  the text block within the tooltip, despite the blocks having identical settings and width. This may indicate
              //  an offset issue in WPF rendering or a currently unknown difference between how the two controls are set up.
              // The observed behaviour is that the tooltip text block can wrap prematurely in comparison to the 
              //  underlying text block.
              if (IsWrapping)
                ImplicitTooltipText.Width = Math.Max(WpfTextBlock.ActualWidth + 1, 128);
              else
                ImplicitTooltipText.ClearValue(System.Windows.FrameworkElement.WidthProperty);

              ImplicitTooltipText.TextTrimming = System.Windows.TextTrimming.CharacterEllipsis;
              ImplicitTooltipText.TextWrapping = IsWrapping ? System.Windows.TextWrapping.Wrap : System.Windows.TextWrapping.NoWrap;
              ImplicitTooltipText.FontFamily = WpfTextBlock.FontFamily;
              ImplicitTooltipText.FontSize = WpfTextBlock.FontSize;
              ImplicitTooltipText.FontWeight = WpfTextBlock.FontWeight;
            }
          };
        }
      }
      else if ((WpfTextBlock.ToolTip as WpfTooltip)?.Content is ImplicitTooltipContent)
      {
        WpfTextBlock.ToolTip = null;
      }
    }
    public static string GetRunText(System.Windows.Controls.TextBlock WpfTextBlock)
    {
      var Result = new StringBuilder();

      var PreviousRun = false;

      foreach (var Inline in WpfTextBlock.Inlines)
      {
        var Run = Inline as System.Windows.Documents.Run;

        if (Run != null)
        {
          if (PreviousRun)
            Result.Append(" ");

          Result.Append(Run.Text);
          PreviousRun = true;
        }
        else
        {
          var LineBreak = Inline as System.Windows.Documents.LineBreak;

          if (LineBreak != null)
          {
            Result.AppendLine();
            PreviousRun = false;
          }
        }
      }

      return Result.ToString();
    }

    internal static void SafeSetContent(this System.Windows.Controls.Border Parent, System.Windows.UIElement Child)
    {
      if (Parent.Child != Child)
      {
        if (Child == null)
          Parent.Child = null;
        else
          SafePlaceChild(Parent, Child, () => Parent.Child = Child);
      }
    }
    internal static void SafeSetContent(this System.Windows.Controls.ContentControl Parent, System.Windows.UIElement Child)
    {
      if (Parent.Content != Child)
      {
        if (Child == null)
          Parent.Content = null;
        else
          SafePlaceChild(Parent, Child, () => Parent.Content = Child);
      }
    }
    internal static void SafeAddChild(this System.Windows.Controls.Panel Parent, System.Windows.UIElement Child)
    {
      SafePlaceChild(Parent, Child, () => Parent.Children.Add(Child));
    }
    internal static void SafeAddChild(this System.Windows.FrameworkElement Parent, System.Windows.Controls.UIElementCollection Collection, System.Windows.UIElement Child)
    {
      SafePlaceChild(Parent, Child, () => Collection.Add(Child));
    }

    private static void SafePlaceChild(this System.Windows.FrameworkElement Parent, System.Windows.UIElement Child, Action PlaceAction)
    {
      var Owner = System.Windows.Media.VisualTreeHelper.GetParent(Child);

      if (Owner == null)
      {
        PlaceAction();
      }
      else if (Owner != Parent)
      {
        var OwnerAsPanel = Owner as System.Windows.Controls.Panel;
        if (OwnerAsPanel != null)
        {
          OwnerAsPanel.Children.Remove(Child);
        }
        else
        {
          var OwnerAsDecorator = Owner as System.Windows.Controls.Decorator;
          if (OwnerAsDecorator != null)
          {
            if (OwnerAsDecorator.Child == Child)
              OwnerAsDecorator.Child = null;
          }
          else
          {
            var OwnerAsContentControl = Owner as System.Windows.Controls.ContentControl;
            if (OwnerAsContentControl != null)
            {
              if (OwnerAsContentControl.Content == Child)
                OwnerAsContentControl.Content = null;
            }
            else
            {
              var OwnerAsScrollContentPresenter = Owner as System.Windows.Controls.ScrollContentPresenter;
              if (OwnerAsScrollContentPresenter != null)
              {
                if (OwnerAsScrollContentPresenter.ScrollOwner.Content == Child)
                  OwnerAsScrollContentPresenter.ScrollOwner.Content = null;
              }
              else
              {
                var OwnerAsContentPresenter = Owner as System.Windows.Controls.ContentPresenter;
                if (OwnerAsContentPresenter != null)
                {
                  if (OwnerAsContentPresenter.Content == Child)
                    OwnerAsContentPresenter.Content = null;
                }
                else
                {
                  Debug.Fail("Unhandled owner.");
                }
              }
            }
          }
        }

        PlaceAction();
      }
    }

    private const int TooltipPadding = 5;
    private const int TooltipBorder = 1;

    private sealed class ImplicitTooltipContent : System.Windows.Controls.Border
    {
      public ImplicitTooltipContent()
      {
        this.TextField = new ImplicitTooltipText();
        this.TextParentBorder = this;
        this.BorderList = [];
      }

      public ImplicitTooltipText Text
      {
        get => TextField;
        set
        {
          TextParentBorder.Child = value;
          TextField = value;
        }
      }

      public void SetBackgroundBrushes(IEnumerable<System.Windows.Media.Brush> Brushes)
      {
        void ClearBorder(System.Windows.Controls.Border Border)
        {
          Border.Padding = new System.Windows.Thickness(0);
          Border.Background = null;
          Border.Child = null;
        }

        ClearBorder(this);
        foreach (var Border in BorderList)
          ClearBorder(Border);

        var AvailableBorderList = BorderList.ToList();

        System.Windows.Controls.Border GetBorder()
        {
          var Result = AvailableBorderList.RemoveFirstOrDefault();

          if (Result == null)
          {
            Result = new System.Windows.Controls.Border();
            BorderList.Add(Result);
          }

          return Result;
        }

        var LastBorder = null as System.Windows.Controls.Border;
        foreach (var Brush in Brushes)
        {
          if (LastBorder == null)
          {
            LastBorder = this;
          }
          else
          {
            var NextBorder = GetBorder();
            LastBorder.Child = NextBorder;
            LastBorder = NextBorder;
          }

          LastBorder.Background = Brush;
        }

        this.TextParentBorder = LastBorder ?? this;
        TextParentBorder.Padding = new System.Windows.Thickness(TooltipPadding);
        TextParentBorder.Child = Text;
      }

      private readonly Inv.DistinctList<System.Windows.Controls.Border> BorderList;
      private System.Windows.Controls.Border TextParentBorder;
      private ImplicitTooltipText TextField;
    }

    private sealed class ImplicitTooltipText : System.Windows.Controls.TextBlock
    {
      public ImplicitTooltipText()
      {
        UseLayoutRounding = true;
        SnapsToDevicePixels = true;
      }
    }

    private static readonly MethodInfo TextBlockCheckFlagsMethod;

    // Copied from System.Windows.Controls.TextBlock source code.
    private enum TextBlockFlags : int
    {
      // Token: 0x040045CA RID: 17866
      FormattedOnce = 1,
      // Token: 0x040045CB RID: 17867
      MeasureInProgress = 2,
      // Token: 0x040045CC RID: 17868
      TreeInReadOnlyMode = 4,
      // Token: 0x040045CD RID: 17869
      RequiresAlignment = 8,
      // Token: 0x040045CE RID: 17870
      ContentChangeInProgress = 16,
      // Token: 0x040045CF RID: 17871
      IsContentPresenterContainer = 32,
      // Token: 0x040045D0 RID: 17872
      HasParagraphEllipses = 64,
      // Token: 0x040045D1 RID: 17873
      PendingTextContainerEventInit = 128,
      // Token: 0x040045D2 RID: 17874
      ArrangeInProgress = 256,
      // Token: 0x040045D3 RID: 17875
      IsTypographySet = 512,
      // Token: 0x040045D4 RID: 17876
      TextContentChanging = 1024,
      // Token: 0x040045D5 RID: 17877
      IsHyphenatorSet = 2048,
      // Token: 0x040045D6 RID: 17878
      HasFirstLine = 4096
    }
  }
}