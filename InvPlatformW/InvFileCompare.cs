﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  public sealed class FileCompare
  {
    public FileCompare()
    {
      this.ParameterFormat = "\"$S\" \"$T\"";

      try
      {
        if (System.IO.File.Exists(BeyondComparePath))
          this.ExecutablePath = BeyondComparePath;
        else if (System.IO.File.Exists(WinMergePath))
          this.ExecutablePath = WinMergePath;
        else if (System.IO.File.Exists(WinMerge86Path))
          this.ExecutablePath = WinMerge86Path;
      }
      catch
      {
        // ignore file access issues.
      }
    }

    public event Action ComparisonClosedEvent;
    public event Action ComparisonExecutableMissingEvent;

    public FileCompareProcess Compare(string SourceFilePath, string TargetFilePath)
    {
      if (string.IsNullOrWhiteSpace(ExecutablePath) || !System.IO.File.Exists(ExecutablePath))
      {
        ComparisonClosedEvent?.Invoke();

        if (ComparisonExecutableMissingEvent != null)
          ComparisonExecutableMissingEvent();
        else
          throw new Exception("No comparison tool could be detected");

        return new FileCompareProcess(null);
      }
      else
      {
        var Execution = System.Diagnostics.Process.Start(ExecutablePath, ProduceParameters(SourceFilePath, TargetFilePath));
        Execution.EnableRaisingEvents = true;
        Execution.Exited += (Sender, E) =>
        {
          ComparisonClosedEvent?.Invoke();
        };
        return new FileCompareProcess(Execution);
      }
    }

    private string ProduceParameters(string SourceFilePath, string TargetFilePath)
    {
      if (ParameterFormat.NullAsEmpty().IsWhitespace())
        throw new Exception("ParameterFormat is empty");

      return ParameterFormat.Replace("$S", SourceFilePath).Replace("$T", TargetFilePath);
    }

    private readonly string ExecutablePath;
    private readonly string ParameterFormat;

    private const string WinMergePath = @"C:\Program Files\WinMerge\WinMergeU.exe";
    private const string WinMerge86Path = @"C:\Program Files (x86)\WinMerge\WinMergeU.exe";
    private const string BeyondComparePath = @"C:\Program Files\Beyond Compare 4\BComp.exe";
  }

  public sealed class FileCompareProcess
  {
    public FileCompareProcess(System.Diagnostics.Process Process)
    {
      this.Process = Process;
    }

    public void Wait()
    {
      Process?.WaitForExit();
    }

    private readonly System.Diagnostics.Process Process;
  }

  public sealed class StringCompare
  {
    public StringCompare()
    {
      this.FileCompare = new FileCompare();
    }

    public event Action ComparisonExecutableMissingEvent
    {
      add => FileCompare.ComparisonExecutableMissingEvent += value;
      remove => FileCompare.ComparisonExecutableMissingEvent -= value;
    }

    public void Compare(string SourceString, string TargetString, string SourceDescription = null, string TargetDescription = null)
    {
      var SourceTempPath = System.IO.Path.GetTempFileName();
      var TargetTempPath = System.IO.Path.GetTempFileName();

      var Directory = System.IO.Path.GetDirectoryName(SourceTempPath);
      var Filename = System.IO.Path.GetFileNameWithoutExtension(SourceTempPath);
      var Extra = SourceDescription == null ? "" : "-" + SourceDescription;

      var SourceFilePath = System.IO.Path.Combine(Directory, Filename + Extra + System.IO.Path.GetExtension(SourceTempPath));

      Directory = System.IO.Path.GetDirectoryName(TargetTempPath);
      Filename = System.IO.Path.GetFileNameWithoutExtension(TargetTempPath);
      Extra = TargetDescription == null ? "" : "-" + TargetDescription;

      var TargetFilePath = System.IO.Path.Combine(Directory, Filename + Extra + System.IO.Path.GetExtension(TargetTempPath));

      System.IO.File.Delete(SourceTempPath);
      System.IO.File.Delete(TargetTempPath);

      System.IO.File.WriteAllText(SourceFilePath, SourceString);
      System.IO.File.WriteAllText(TargetFilePath, TargetString);

      FileCompare.ComparisonClosedEvent += () =>
      {
        System.IO.File.Delete(SourceFilePath);
        System.IO.File.Delete(TargetFilePath);
      };

      FileCompare.Compare(SourceFilePath, TargetFilePath);
    }

    private readonly FileCompare FileCompare;
  }
}