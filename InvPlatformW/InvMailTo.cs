﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  public sealed class MailTo
  {
    public MailTo()
    {
    }

    public string Subject { get; set; }
    public string Body { get; set; }

    public void To(IEnumerable<string> Addresses)
    {
      this.ToAddressArray = Addresses.ToArray();
    }
    public void To(params string[] AddressArray)
    {
      this.ToAddressArray = AddressArray;
    }
    public void Cc(IEnumerable<string> Addresses)
    {
      this.CcAddressArray = Addresses.ToArray();
    }
    public void Cc(params string[] AddressArray)
    {
      this.CcAddressArray = AddressArray;
    }
    public void Bcc(IEnumerable<string> Addresses)
    {
      this.BccAddressArray = Addresses.ToArray();
    }
    public void Bcc(params string[] AddressArray)
    {
      this.BccAddressArray = AddressArray;
    }
    public void Send()
    {
      // TODO: what is the string maximum length for the mailto command?

      var ParameterList = new List<string>();

      var MailToAddress = ToAddressArray != null && ToAddressArray.Length > 0 ? ToAddressArray.AsSeparatedText(",") : string.Empty;

      var MailCcAddress = CcAddressArray != null && CcAddressArray.Length > 0 ? CcAddressArray.AsSeparatedText(",") : string.Empty;
      if (MailCcAddress != "")
        ParameterList.Add($"cc={MailCcAddress}");

      var MailBccAddress = BccAddressArray != null && BccAddressArray.Length > 0 ? BccAddressArray.AsSeparatedText(",") : string.Empty;
      if (MailBccAddress != "")
        ParameterList.Add($"bcc={MailBccAddress}");

      if (!string.IsNullOrWhiteSpace(Subject))
        ParameterList.Add($"subject={Uri.EscapeDataString(Subject)}");

      if (!string.IsNullOrWhiteSpace(Body))
        ParameterList.Add($"body={Uri.EscapeDataString(Body)}");

      System.Diagnostics.Process.Start($"mailto:{MailToAddress}{(ParameterList.Count > 0 ? $"?{ParameterList.AsSeparatedText("&")}" : string.Empty)}");
    }

    private string[] ToAddressArray;
    private string[] CcAddressArray;
    private string[] BccAddressArray;
  }
}