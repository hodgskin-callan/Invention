﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Inv
{
  public enum SystemMouseType
  {
    XButton1Down,
    XButton2Down
  }

  /// Low-level mouse intercept hook.
  public sealed class SystemMouseHook : IDisposable
  {
    public SystemMouseHook()
    {
      this.HookDelegate = HookCallback;

      using (var CurrentProcess = System.Diagnostics.Process.GetCurrentProcess())
      using (var CurrentModule = CurrentProcess.MainModule)
      {
        this.HookHandle = Win32.User32.SetWindowsHookEx(Win32.User32.WH_MOUSE_LL, HookDelegate, Win32.User32.GetModuleHandle(CurrentModule.ModuleName), 0);
        if (HookHandle == IntPtr.Zero)
          throw new Win32Exception();
      }
    }
    ~SystemMouseHook()
    {
      if (HookHandle != IntPtr.Zero)
        Win32.User32.UnhookWindowsHookEx(HookHandle);
    }
    public void Dispose()
    {
      if (HookHandle != IntPtr.Zero)
      {
        if (!Win32.User32.UnhookWindowsHookEx(HookHandle))
          throw new Win32Exception();

        this.HookHandle = IntPtr.Zero;
      }
    }

    public event Action<SystemMouseType> HandleEvent;
    public event Action<Exception> FaultEvent;

    private IntPtr HookCallback(int nCode, IntPtr wParam, ref Win32.User32.MSLLHOOKSTRUCT lParam)
    {
      if (nCode == 0)
      {
        try
        {
          var wParamValue = wParam.ToInt32();

          if (Win32.User32.WM_XBUTTONDOWN == wParamValue)
          {
            var HiOrder = lParam.mouseData >> 16;

            if (HiOrder == 1)
              HandleInvoke(SystemMouseType.XButton1Down);
            else
              HandleInvoke(SystemMouseType.XButton2Down);
          }
        }
        catch
        {
          if (Debugger.IsAttached)
            Debugger.Break();

          // NOTE: an unhandled exception in the callback will terminate the process.
        }
      }

      return Win32.User32.CallNextHookEx(HookHandle, nCode, wParam, ref lParam);
    }
    private void HandleInvoke(SystemMouseType MouseType)
    {
      try
      {
        if (HandleEvent != null)
          HandleEvent(MouseType);
      }
      catch (Exception Exception)
      {
        FaultEvent?.Invoke(Exception);
      }
    }

    private IntPtr HookHandle;
    private Win32.User32.MouseHookHandlerDelegate HookDelegate;
  }
}
