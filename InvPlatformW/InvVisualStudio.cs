﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using Inv.Support;

namespace Inv
{
  public interface VisualStudioEditor
  {
    void OpenFile(string FilePath, long? LineNumber = null);
  }

  public sealed class VisualStudioAutomation : VisualStudioEditor
  {
    public VisualStudioAutomation()
    {
      var ProgIDArray = new string[] { "VisualStudio.DTE.17.0", "VisualStudio.DTE.16.0", "VisualStudio.DTE.15.0", "VisualStudio.DTE.14.0", "VisualStudio.DTE.13.0", "VisualStudio.DTE.12.0", "VisualStudio.DTE.11.0", "VisualStudio.DTE.10.0" };

      using (var ClassesRootKey = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.ClassesRoot, Microsoft.Win32.RegistryView.Default))
      {
        for (var Index = 0; Index < ProgIDArray.Length && VisualStudioProgID == null; Index++)
        {
          var DTEKey = ClassesRootKey.OpenSubKey(ProgIDArray[Index]);
          if (DTEKey != null)
          {
            this.VisualStudioProgID = ProgIDArray[Index];
            DTEKey.Close();
          }
        }
      }

      if (VisualStudioProgID != null)
      {
        var Version = VisualStudioProgID.Substring(17);

        var PathArray = new[] 
        {
          @"C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE\devenv.exe",
          @"C:\Program Files\Microsoft Visual Studio " + Version + @"\Common7\IDE\devenv.exe", 
          @"C:\Program Files (x86)\Microsoft Visual Studio " + Version + @"\Common7\IDE\devenv.exe"
        };

        foreach (var Path in PathArray)
        {
          if (System.IO.File.Exists(Path))
          {
            this.VisualStudioPath = Path;
            break;
          }
        }
      }
    }

    public bool IsInstalled => VisualStudioPath != null && VisualStudioProgID != null;

    public void OpenFile(string FilePath, long? LineNumber = null)
    {
      // NOTE: the dynamic is to remove the hard dependency which is only available when Visual Studio is installed (EnvDTE80.DTE2)
      var DTE = GetVisualStudioDTE();

      if (DTE == null)
      {
        var StartInfo = new ProcessStartInfo();

        if (VisualStudioPath != null)
        {
          StartInfo.Verb = "Open";
          StartInfo.FileName = "\"" + VisualStudioPath + "\"";
          StartInfo.Arguments = "/edit \"" + FilePath + "\"";

          if (LineNumber != null)
          {
            StartInfo.Arguments += " /command \"edit.goto " + LineNumber.Value.ToString() + "\"";
            StartInfo.UseShellExecute = true;
          }
        }
        else
        {
          StartInfo.Verb = "Open";
          StartInfo.FileName = FilePath;
        }

        System.Diagnostics.Process.Start(StartInfo);
      }
      else
      {
        // seems more stable if we activate Visual Studio, before opening the file and going to the line number.
        GuardActivate(DTE);

        DTE.ExecuteCommand("File.OpenFile", FilePath);

        // TODO: do we need a sleep here to wait for the file to be opened? 
        //       sometimes the Edit.Goto fails the first time it is attempted.

        if (LineNumber != null)
          GuardGoto(DTE, LineNumber.Value);
      }
    }
    public void AttachDebugger(int ProcessID)
    {
      try
      {
        var DTE = GetVisualStudioDTE();

        if (DTE != null)
        {
          foreach (var LocalProcess in DTE.Debugger.LocalProcesses)
          {
            if (LocalProcess.ProcessID == ProcessID)
            {
              LocalProcess.Attach();
              return;
            }
          }
        }
      }
      catch (Exception Exception)
      {
        Debug.WriteLine(Exception.Describe());

        if (Debugger.IsAttached)
          Debugger.Break();
      }

      // Otherwise prompt the user.
      Debugger.Launch();
    }

    private dynamic GetVisualStudioDTE()
    {
      try
      {
        return GetActiveObject(VisualStudioProgID);// System.Runtime.InteropServices.Marshal.GetActiveObject(VisualStudioProgID);
      }
      catch
      {
        return null;
      }
    }

    private void GuardGoto(dynamic DTE, long LineNumber)
    {
      try
      {
        // TODO: there is no way to prevent Visual Studio from breaking on an exception in specific code (you used to be able to do it with an attribute).
        //       all you can do is use Exception Settings to ignore the COMException, which is a bad idea.
        DTE.ExecuteCommand("Edit.Goto", LineNumber.ToString());
      }
      catch (Exception Exception)
      {
        Debug.WriteLine("Edit.Goto: " + Exception.Message);
      }
    }
    private void GuardActivate(dynamic DTE)
    {
      try
      {
        // TODO: there is no way to prevent Visual Studio from breaking on an exception in specific code (you used to be able to do it with an attribute).
        //       all you can do is use Exception Settings to ignore the COMException, which is a bad idea.
        DTE.MainWindow.Activate();
      }
      catch (Exception Exception)
      {
        Debug.WriteLine("MainWindow.Activate: " + Exception.Message);
      }
    }

    private readonly string VisualStudioProgID;
    private readonly string VisualStudioPath;

    private static object GetActiveObject(string progId, bool throwOnError = false)
    {
      if (progId == null)
        throw new ArgumentNullException(nameof(progId));

      var hr = Win32.Ole32.CLSIDFromProgIDEx(progId, out var clsid);
      if (hr < 0)
      {
        if (throwOnError)
          Marshal.ThrowExceptionForHR(hr);

        return null;
      }

      hr = Win32.Oleaut32.GetActiveObject(clsid, IntPtr.Zero, out var obj);
      if (hr < 0)
      {
        if (throwOnError)
          Marshal.ThrowExceptionForHR(hr);

        return null;
      }
      return obj;
    }
  }

  public sealed class VisualStudioCodeAutomation : VisualStudioEditor
  {
    public VisualStudioCodeAutomation()
    {
      const string AdminVSCodePath = @"C:\Program Files\Microsoft VS Code\Code.exe";
      var UserVSCodePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"Programs\Microsoft VS Code\Code.exe");

      var PathArray = new[] { AdminVSCodePath, UserVSCodePath };

      foreach (var Path in PathArray)
      {
        if (System.IO.File.Exists(Path))
        {
          this.VisualStudioCodePath = Path;
          break;
        }
      }
    }

    public readonly string VisualStudioCodePath;
    public bool IsInstalled => VisualStudioCodePath != null;

    public void OpenFile(string FilePath, long? LineNumber = null)
    {
      var Arguments = $"-g {FilePath}:{LineNumber ?? 1}:1";
      var StartInfo = new System.Diagnostics.ProcessStartInfo(VisualStudioCodePath, Arguments);

      System.Diagnostics.Process.Start(StartInfo);
    }
  }
}