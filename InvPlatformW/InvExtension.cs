﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using Microsoft.Win32;

// Namespace containing extension methods for framework classes. 

namespace Inv.Support
{
  public static class Extension
  {
    /// <summary>
    /// Return the timestamp of when the executable was built.
    /// </summary>
    /// <returns></returns>
    public static DateTime RetrieveLinkerTimestamp(this System.Reflection.Assembly Assembly)
    {
      const int c_PeHeaderOffset = 60;
      const int c_LinkerTimestampOffset = 8;

      var Buffer = new byte[2048];
      using (var Stream = new System.IO.FileStream(Assembly.Location, System.IO.FileMode.Open, System.IO.FileAccess.Read))
        Stream.ReadByteArray(Buffer);

      var Index = BitConverter.ToInt32(Buffer, c_PeHeaderOffset);
      var SecondsSince1970 = BitConverter.ToInt32(Buffer, Index + c_LinkerTimestampOffset);
      var Result = new DateTime(1970, 1, 1, 0, 0, 0);
      Result = Result.AddSeconds(SecondsSince1970);
      Result = Result.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(Result).Hours);
      return Result;
    }
    /// <summary>
    /// Convert every element of this <see cref="IEnumerable{T}"/> using the specified <see cref="Converter{T,TOutput}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <typeparam name="TOutput">The desired output type of the conversion.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Converter">A <see cref="Converter{T,TOutput}"/> to convert elements.</param>
    /// <returns>An <see cref="IEnumerable{TOutput}"/> of converted elements.</returns>
    public static IEnumerable<TOutput> ConvertAll<T, TOutput>(this IEnumerable<T> Source, Converter<T, TOutput> Converter)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Converter != null, "Converter must be specified.");

      return Source.Select(Item => Converter(Item));
    }
    public static bool ManualRepairExeConfigSupportedFramework(string ExeConfigFilePath, string SupportedFrameworkVersion, bool UseServerGC)
    {
      if (System.IO.File.Exists(ExeConfigFilePath))
      {
        var ExeConfigText = System.IO.File.ReadAllText(ExeConfigFilePath);
        var ExeConfigRegex = new System.Text.RegularExpressions.Regex(@"\<supportedRuntime\s+version\s*=\s*\""v4\.0\""\s+sku\s*=\s*\""(\.NETFramework,Version\=v4\.[0-9])+\""\s*\/\>");

        var ExeConfigResult = ExeConfigRegex.Match(ExeConfigText);

        if (ExeConfigResult.Success && ExeConfigResult.Groups[1].Value != SupportedFrameworkVersion)
        {
          var NewConfigText = ExeConfigRegex.Replace(ExeConfigText, Match =>
          {
            var NewConfigGroup = Match.Groups[1];
            return $"{Match.Value.Substring(0, NewConfigGroup.Index - Match.Index)}{SupportedFrameworkVersion}{Match.Value.Substring(NewConfigGroup.Index - Match.Index + NewConfigGroup.Length)}";
          });

          System.IO.File.WriteAllText(ExeConfigFilePath, NewConfigText);

          return true;
        }

        return false;
      }
      else
      {
        var ServerGCText = UseServerGC ? @"
  <runtime>
    <gcServer enabled=""true""/>
  </runtime>" : "";

        System.IO.File.WriteAllText(ExeConfigFilePath,
$@"<?xml version=""1.0"" encoding=""utf-8""?>
<configuration>
  <startup>
    <supportedRuntime version=""v4.0"" sku=""{SupportedFrameworkVersion}"" />
  </startup>
  <system.net>
    <defaultProxy useDefaultCredentials=""true"">
      <proxy usesystemdefault=""true"" />
    </defaultProxy>
  </system.net>" + ServerGCText + @"
</configuration>");

        return true;
      }
    }
  }

  public static class StreamHelper
  {
    public static bool EqualTo(this Stream LeftStream, Stream RightStream, int BufferSize)
    {
      Debug.Assert(LeftStream.Position == 0);
      Debug.Assert(RightStream.Position == 0);

      var LeftLength = LeftStream.Length;
      var RightLength = RightStream.Length;

      if (LeftLength != RightLength)
        return false;

      var LeftBuffer = new byte[BufferSize];
      var RightBuffer = new byte[BufferSize];

      var RemainderLength = LeftLength;

      while (RemainderLength > 0)
      {
        var ReadLength = LeftBuffer.Length;

        if (ReadLength > RemainderLength)
          ReadLength = (int)RemainderLength;

        RemainderLength -= ReadLength;

        LeftStream.ReadByteArray(LeftBuffer);
        RightStream.ReadByteArray(RightBuffer);

        for (var BufferIndex = 0; BufferIndex < ReadLength; BufferIndex++)
        {
          if (LeftBuffer[BufferIndex] != RightBuffer[BufferIndex])
            return false;
        }
      }

      return true;
    }

    public static bool FileEqualTo(string LeftFilePath, string RightFilePath)
    {
      using (var LeftStream = new FileStream(LeftFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 65536))
      using (var RightStream = new FileStream(RightFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 65536))
      {
        return LeftStream.EqualTo(RightStream, 65536);
      }
    }
  }

  public static class GuidHelper
  {
    public static bool IsCOMRegistered(this Guid ClassGUID)
    {
      var Result = Win32.Ole32.ProgIDFromCLSID(ref ClassGUID, out var ProgID);
      return Result == 0;
    }
  }

  public static class DoubleHelper
  {
    public static double RoundToSignificantDigits(this double Value, int Digits)
    {
      if (Value == 0.0)
        return Value;

      var Scale = Math.Pow(10, Math.Floor(Math.Log10(Math.Abs(Value))) + 1);
      return Scale * Math.Round(Value / Scale, Digits);
    }
    public static double TruncateToSignificantDigits(this double Value, int Digits)
    {
      if (Value == 0)
        return 0;

      var Scale = Math.Pow(10, Math.Floor(Math.Log10(Math.Abs(Value))) + 1 - Digits);
      return Scale * (Value / Scale).Truncate();
    }
  }

  public static class ShellHelper
  {
    public static string GetFileTypeDescription(string Extension)
    {
      var ShInfo = new Win32.Shell32.SHFILEINFO();

      var FileInfo = Win32.Shell32.SHGetFileInfo(Extension, Win32.Shell32.FILE_ATTRIBUTE.NORMAL, ref ShInfo, Marshal.SizeOf(ShInfo), Win32.Shell32.SHGFI.TYPENAME | Win32.Shell32.SHGFI.USEFILEATTRIBUTES);
      if (FileInfo == IntPtr.Zero)
        return null;
      else
        return ShInfo.szTypeName.Trim();
    }
  }

  public static class RegistryHelper
  {
    public static ContentType AccessFileContentType(string Extension)
    {
      Debug.Assert(!string.IsNullOrWhiteSpace(Extension), "Extension must not be null or blank.");

      using (var ContentRegistryKey = Registry.ClassesRoot.OpenSubKey(Extension))
      {
        if (ContentRegistryKey != null)
        {
          var DefaultValue = ContentRegistryKey.GetValue("");
          if (DefaultValue != null)
          {
            var DefaultString = DefaultValue.ToString();
            if (DefaultString.Equals("txtfile", StringComparison.CurrentCultureIgnoreCase) || DefaultString.Equals("VBSFile", StringComparison.CurrentCultureIgnoreCase) || DefaultString.StartsWith("Microsoft.PowerShellScript", StringComparison.CurrentCultureIgnoreCase))
              return ContentType.Text;
            else if (DefaultString.StartsWith("Word", StringComparison.CurrentCultureIgnoreCase))
              return ContentType.Word;
            else if (DefaultString.StartsWith("Excel", StringComparison.CurrentCultureIgnoreCase))
              return ContentType.Excel;
          }

          var ContentTypeValue = (ContentRegistryKey.GetValue("Content Type") ?? "").ToString();

          if (!string.IsNullOrWhiteSpace(ContentTypeValue))
          {
            if (ContentTypeValue.ToLower().StartsWith("text/") || ContentTypeValue.ToLower().EndsWith("/xml") || ContentTypeValue.ToLower().EndsWith("/javascript"))
              return ContentType.Text;
            else if (ContentTypeValue.ToLower().StartsWith("video/") || ContentTypeValue.ToLower().StartsWith("audio/"))
              return ContentType.Media;
            else if (ContentTypeValue.ToLower().StartsWith("image/"))
              return ContentType.Image;
          }

          var PerceivedType = (ContentRegistryKey.GetValue("PerceivedType") ?? "").ToString();

          if (PerceivedType.Equals("text", StringComparison.CurrentCultureIgnoreCase))
            return ContentType.Text;
          else if (PerceivedType.Equals("video", StringComparison.CurrentCultureIgnoreCase) || PerceivedType.Equals("audio", StringComparison.CurrentCultureIgnoreCase))
            return ContentType.Media;
          else if (PerceivedType.Equals("image", StringComparison.CurrentCultureIgnoreCase))
            return ContentType.Image;
        }

        return ContentType.Other;
      }
    }

    public enum ContentType { Text, Media, Image, Word, Excel, Other }
  }

  public static class NameValueCollectionHelper
  {
    public static T GetValue<T>(this System.Collections.Specialized.NameValueCollection NameValueCollection, string Key, T Default = default)
      where T : IConvertible
    {
      T Result;
      var Value = NameValueCollection[Key];

      if (Value == null)
        Result = Default;
      else
      {
        try
        {
          Result = (T)Convert.ChangeType(Value, typeof(T));
        }
        catch
        {
          Result = Default;
        }
      }

      return Result;
    }
    public static bool TryGetValue<T>(this System.Collections.Specialized.NameValueCollection NameValueCollection, string Key, out T Result)
      where T : IConvertible
    {
      Result = default;
      var Value = NameValueCollection[Key];

      if (Value == null)
        return false;
      else
      {
        try
        {
          Result = (T)Convert.ChangeType(Value, typeof(T));
          return true;
        }
        catch
        {
          return false;
        }
      }
    }
  }

  public static class DirectoryInfoHelper
  {
    public static Inv.DataSize Size(this DirectoryInfo Source, System.IO.SearchOption SearchOption = SearchOption.AllDirectories)
    {
      long FolderSize = 0;
      try
      {
        if (Source.Exists)
        {
          try
          {
            foreach (var File in Source.EnumerateFiles("*", SearchOption))
            {
              if (File.Exists)
                FolderSize += File.Length;
            }
          }
          catch (NotSupportedException e)
          {
            Debug.WriteLine("Unable to calculate folder size: {0}", e.Message);
          }
        }
      }
      catch (UnauthorizedAccessException e)
      {
        Debug.WriteLine("Unable to calculate folder size: {0}", e.Message);
      }

      return Inv.DataSize.FromBytes(FolderSize);
    }
  }

  public static class FileInfoHelper
  {
    public static byte[] ReadAllBytes(this FileInfo Source)
    {
      byte[] Result;

      using (var FileStream = new FileStream(Source.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 65536))
      {
        var FileLength = FileStream.Length;

        if (FileLength > Int32.MaxValue)
          throw new IOException("File too long");

        var Count = (int)FileLength;

        Result = new byte[Count];

        FileStream.ReadByteArray(Result);
      }

      return Result;
    }
    public static Inv.Dimension ReadImageDimension(this FileInfo Source)
    {
      using (var FileStream = new FileStream(Source.FullName, FileMode.Open, FileAccess.Read, FileShare.Read, 65536))
      {
        var Result = BitmapFrame.Create(FileStream, BitmapCreateOptions.DelayCreation, BitmapCacheOption.None);

        return new Inv.Dimension(Result.PixelWidth, Result.PixelHeight);
      }
    }
    public static TimeSpan ReadSoundLength(this FileInfo Source)
    {
      var lengthBuf = new StringBuilder(32);

      var Res = Win32.WinMM.mciSendString(string.Format("status \"{0}\" length", Source.FullName), lengthBuf, lengthBuf.Capacity, IntPtr.Zero);
      //Debug.Assert(Res == 0, Source.FullName + " sound length not found [" + Res + "].");

      if (!int.TryParse(lengthBuf.ToString(), out int length))
        length = 0;

      return TimeSpan.FromMilliseconds(length);
    }
    public static Inv.DataSize Size(this FileInfo Source)
    {
      return Inv.DataSize.FromBytes(Source.Length);
    }
  }

  /// <summary>
  /// A static class with extension methods related to paths.
  /// </summary>
  public static class PathHelper
  {
    /// <summary>
    /// Get the path to the x86 Program Files directory.
    /// </summary>
    /// <returns>The path to the x86 Program Files directory.</returns>
    public static string ProgramFilesx86()
    {
      var ProgramFilePath = new System.Text.StringBuilder(255);

      if (Win32.Shell32.SHGetSpecialFolderPath(IntPtr.Zero, ProgramFilePath, Win32.Shell32.CSIDL_PROGRAM_FILESX86, 0))
        return ProgramFilePath.ToString();
      else
        return null;
    }

    /// <summary>
    /// Get the relative path between two absolute paths. The two paths must share a common prefix and refer to files or directories which
    /// actually exist at the time of calling GetRelativePath.
    /// </summary>
    /// <param name="fromPath">The path to use as the source of the relative path.</param>
    /// <param name="toPath">The destination path.</param>
    /// <returns>A string expressing the relative path required to get to toPath from fromPath.</returns>
    public static string GetRelativePath(string fromPath, string toPath)
    {
      var fromAttr = GetPathAttribute(fromPath);
      var toAttr = GetPathAttribute(toPath);

      var path = new StringBuilder(260); // MAX_PATH
      if (PathRelativePathTo(
          path,
          fromPath,
          fromAttr,
          toPath,
          toAttr) == 0)
      {
        throw new ArgumentException("Paths must have a common prefix");
      }
      return path.ToString();
    }

    public static string GetApplicationPath()
    {
      return System.AppDomain.CurrentDomain.BaseDirectory;
    }

    public static string GetValidFilePath(string FilePath) => FilePath.Replace(Path.GetInvalidFileNameChars(), ' ');

    [DebuggerNonUserCode]
    private static int GetPathAttribute(string path)
    {
      var di = new DirectoryInfo(path);
      if (di.Exists)
      {
        return FILE_ATTRIBUTE_DIRECTORY;
      }

      var fi = new FileInfo(path);
      if (fi.Exists)
      {
        return FILE_ATTRIBUTE_NORMAL;
      }

      throw new FileNotFoundException();
    }

    private const int FILE_ATTRIBUTE_DIRECTORY = 0x10;
    private const int FILE_ATTRIBUTE_NORMAL = 0x80;

    [DllImport("shlwapi.dll", SetLastError = true)]
    private static extern int PathRelativePathTo(StringBuilder pszPath, string pszFrom, int dwAttrFrom, string pszTo, int dwAttrTo);
  }

  public static class ConcurrentDictionaryHelper
  {
    public static bool Remove<TKey, TValue>(this System.Collections.Concurrent.ConcurrentDictionary<TKey, TValue> Dictionary, TKey Key)
    {
      return Dictionary.TryRemove(Key, out var Value);
    }
    public static void Add<TKey, TValue>(this System.Collections.Concurrent.ConcurrentDictionary<TKey, TValue> Dictionary, TKey Key, TValue Value)
    {
      if (!Dictionary.TryAdd(Key, Value))
        throw new Exception("Concurrent dictionary already contains an item for this key.");
    }
    public static TValue GetValueOrDefault<TKey, TValue>(this System.Collections.Concurrent.ConcurrentDictionary<TKey, TValue> Dictionary, TKey Key, TValue DefaultValue = default)
    {
      if (Dictionary.TryGetValue(Key, out var Result))
        return Result;
      else
        return DefaultValue;
    }
  }

  public static class StringConverterHelper
  {
    public static TimeSpan? TimeSpanFromString(string Text)
    {
      var TimeSpan = new TimeSpan?();
      var ParseError = false;

      if (Text.ToLower().Contains("from") || Text.ToLower().Contains("to") || Text.ToLower().Contains("till"))
      {
        var TimeSplit = Text.ToLower().Split(new string[] { "from", "to", "till" }, StringSplitOptions.RemoveEmptyEntries);

        if (TimeSplit.Length != 2)
        {
          TimeSpan = new TimeSpan(0);
        }
        else
        {
          var StartTime = TimeSplit[0];
          StartTime = StartTime.Trim();

          var StartTimeSplit = StartTime.Split(':');

          var EndTime = TimeSplit[1];
          EndTime = EndTime.Trim();

          var EndTimeSplit = EndTime.Split(':');

          var StartTimeHours = 0;
          var StartTimeMinutes = 0;

          if (StartTimeSplit.Length == 1)
          {
            if (!int.TryParse(StartTimeSplit[0], out StartTimeHours))
              ParseError = true;
          }
          else if (StartTimeSplit.Length == 2)
          {
            if (!int.TryParse(StartTimeSplit[0], out StartTimeHours))
              ParseError = true;

            if (!int.TryParse(StartTimeSplit[1], out StartTimeMinutes))
              ParseError = true;
          }
          else
          {
            ParseError = true;
          }

          var EndTimeHours = 0;
          var EndTimeMinutes = 0;

          if (EndTimeSplit.Length == 1)
          {
            if (!int.TryParse(EndTimeSplit[0], out EndTimeHours))
              ParseError = true;
          }
          else if (EndTimeSplit.Length == 2)
          {
            if (!int.TryParse(EndTimeSplit[0], out EndTimeHours))
              ParseError = true;

            if (!int.TryParse(EndTimeSplit[1], out EndTimeMinutes))
              ParseError = true;
          }
          else
          {
            ParseError = true;
          }

          if (ParseError)
          {
            TimeSpan = new TimeSpan(0);
          }
          else
          {
            if (EndTimeHours < StartTimeHours)
              EndTimeHours += 12;

            if (EndTimeMinutes < StartTimeMinutes)
            {
              EndTimeMinutes += 60;
              EndTimeHours -= 1;
            }

            var TimeSpanHours = EndTimeHours - StartTimeHours;
            var TimeSpanMinutes = EndTimeMinutes - StartTimeMinutes;

            TimeSpan = new TimeSpan(TimeSpanHours, TimeSpanMinutes, 0);
          }

        }
      }
      else if (Text.ToLower().Contains("hour") || Text.ToLower().Contains("hours") || Text.ToLower().Contains("hrs") || Text.ToLower().Contains("hr") ||
        Text.ToLower().Contains("minute") || Text.ToLower().Contains("minutes") || Text.ToLower().Contains("mins") || Text.ToLower().Contains("min"))
      {
        var HourSplit = Text.ToLower().Split(new string[] { "hours", "hour", "hrs", "hr" }, StringSplitOptions.None);

        var TimeSpanHours = 0;
        var TimeSpanMinutes = 0;

        if (HourSplit.Length == 2)
        {
          if (!int.TryParse(HourSplit[0], out TimeSpanHours))
            ParseError = true;

          if (HourSplit[1] != "")
          {
            var MinuteString = HourSplit[1].Trim();
            MinuteString = MinuteString.Replace("minutes", "");
            MinuteString = MinuteString.Replace("minute", "");
            MinuteString = MinuteString.Replace("mins", "");
            MinuteString = MinuteString.Replace("min", "");

            if (!int.TryParse(MinuteString, out TimeSpanMinutes))
              ParseError = true;
          }
        }
        else if (HourSplit.Length == 1)
        {
          var MinuteString = HourSplit[0].Trim();
          MinuteString = MinuteString.Replace("minutes", "");
          MinuteString = MinuteString.Replace("minute", "");
          MinuteString = MinuteString.Replace("mins", "");
          MinuteString = MinuteString.Replace("min", "");

          if (!int.TryParse(MinuteString, out TimeSpanMinutes))
            ParseError = true;
        }
        else
        {
          ParseError = true;
        }

        if (ParseError)
          TimeSpan = new TimeSpan(0);
        else
          TimeSpan = new TimeSpan(TimeSpanHours, TimeSpanMinutes, 0);

      }
      else if (System.Text.RegularExpressions.Regex.IsMatch(Text, "[0-9]"))
      {
        if (int.TryParse(Text.Trim(), out int TimeSpanMinutes))
          TimeSpan = new TimeSpan(0, TimeSpanMinutes, 0);
        else
          ParseError = true;
      }
      else
      {
        ParseError = true;
      }

      return TimeSpan;
    }
    public static double? MathematicalEvaluateFromString(string Text)
    {
      var Queue = Organise(Tokenise(Text));

      var DynamicMethod = new DynamicMethod("Execution", typeof(double), null, typeof(StringConverterHelper));
      var ILGenerator = DynamicMethod.GetILGenerator();

      while (Queue.Count > 0)
      {
        var Token = Queue.Dequeue();

        switch (Token)
        {
          case "+":
            ILGenerator.Emit(OpCodes.Add);
            break;

          case "-":
            ILGenerator.Emit(OpCodes.Sub);
            break;

          case "*":
            ILGenerator.Emit(OpCodes.Mul);
            break;

          case "/":
            ILGenerator.Emit(OpCodes.Div);
            break;

          default:
            ILGenerator.Emit(OpCodes.Ldc_R8, double.Parse(Token));
            break;
        }
      }

      ILGenerator.Emit(OpCodes.Ret);

      return NonUserExecutionInvoke(DynamicMethod);
    }

    [System.Diagnostics.DebuggerNonUserCode]
    private static double? NonUserExecutionInvoke(DynamicMethod Method)
    {
      double? Result = null;
      try
      {
        Result = (double)Method.Invoke(null, null);
      }
      catch (Exception)
      {
      }

      return Result;
    }
    private static Queue<string> Tokenise(string Text)
    {
      var Position = 0;
      var TokenQueue = new Queue<string>();
      var StringBuilder = new StringBuilder();

      while (Position < Text.Length)
      {
        var Symbol = Text[Position];

        if (SymbolMap.Contains(Symbol))
        {
          if (StringBuilder.Length > 0)
          {
            TokenQueue.Enqueue(StringBuilder.ToString());
            StringBuilder.Clear();
          }

          TokenQueue.Enqueue(Symbol.ToString());
        }
        else if (char.IsDigit(Symbol) || Symbol == '.')
        {
          StringBuilder.Append(Symbol);
        }
        else if (!char.IsWhiteSpace(Symbol))
        {
          throw new InvalidDataException(string.Format("Invalid expression symbol '{0}'", Symbol));
        }

        Position++;
      }

      if (StringBuilder.Length > 0)
        TokenQueue.Enqueue(StringBuilder.ToString());

      return TokenQueue;
    }
    private static Queue<string> Organise(Queue<string> Text)
    {
      var PrecedenceStack = new Stack<string>();

      var Result = new Queue<String>();

      while (Text.Count > 0)
      {
        var Token = Text.Dequeue();

        if (OperatorMap.Contains(Token))
        {
          while (PrecedenceStack.Count > 0 && PrecedenceStack.Peek() != "(")
          {
            if (PrecedenceLevel(PrecedenceStack.Peek()) >= PrecedenceLevel(Token))
              Result.Enqueue(PrecedenceStack.Pop());
            else
              break;
          }

          PrecedenceStack.Push(Token);
        }
        else if (Token == "(")
        {
          PrecedenceStack.Push("(");
        }
        else if (Token == ")")
        {
          while (PrecedenceStack.Count > 0 && PrecedenceStack.Peek() != "(")
            Result.Enqueue(PrecedenceStack.Pop());

          if (PrecedenceStack.Count > 0)
            PrecedenceStack.Pop();
        }
        else
        {
          Result.Enqueue(Token);
        }
      }
      while (PrecedenceStack.Count > 0)
      {
        Result.Enqueue(PrecedenceStack.Pop());
      }

      return Result;
    }
    private static int PrecedenceLevel(string Value)
    {
      int Result;

      switch (Value)
      {
        case "*":
        case "/":
          Result = 10;
          break;

        case "+":
        case "-":
          Result = 9;
          break;

        default:
          Result = 0;
          break;
      }

      return Result;
    }

    private const string OperatorMap = "+-/*";
    private const string SymbolMap = OperatorMap + "()";
  }

  public static class EffectHelper
  {
    public static Effect FreezeIt(this Effect Effect)
    {
      Effect.Freeze();

      return Effect;
    }
  }

  public static class PenHelper
  {
    public static Pen FreezeIt(this Pen Pen)
    {
      Pen.Freeze();

      return Pen;
    }
  }

  public static class StyleHelper
  {
    public static Style SealIt(this Style Style)
    {
      Style.Seal();

      return Style;
    }
  }

  public static class WindowsIdentityHelper
  {
    public static WindowsIdentity GetWindowsIdentity(string Domain, string Username, System.Security.SecureString Password, bool IsInteractive = true)
    {
      var PasswordPtr = IntPtr.Zero;
      try
      {
        // marshal the SecureString to unmanaged memory.
        PasswordPtr = Marshal.SecureStringToGlobalAllocUnicode(Password);

        if (Win32.advapi32.LogonUser(Username, Domain, PasswordPtr, IsInteractive ? Win32.advapi32.LOGON32_LOGON_INTERACTIVE : Win32.advapi32.LOGON32_LOGON_NETWORK, Win32.advapi32.LOGON32_PROVIDER_DEFAULT, out Win32.Kernel32.SafeTokenHandle LogonHandle))
          return new WindowsIdentity(LogonHandle.DangerousGetHandle());
        else
          return null;
      }
      finally
      {
        // zero-out and free the unmanaged string reference.
        Marshal.ZeroFreeGlobalAllocUnicode(PasswordPtr);
      }
    }
    public static bool IsElevated()
    {
      return WindowsIdentity.GetCurrent().IsElevated();
    }
    public static bool IsElevated(this WindowsIdentity WindowsIdentity)
    {
      return new WindowsPrincipal(WindowsIdentity).IsInRole(WindowsBuiltInRole.Administrator);
    }
  }

  public static class ProcessHelper
  {
    [DebuggerNonUserCode]
    public static string MainFilePath(this System.Diagnostics.Process Process)
    {
      try
      {
        return Process.MainModule.FileName;
      }
      catch
      {
        return Process.ProcessName;
      }
    }
  }

  public static class NetworkInterfaceHelper
  {
    public static NetworkInterface GetInterfaceByAddress(string Address)
    {
      using (var UdpClient = new UdpClient(Address, 1))
      {
        var LocalAddress = ((IPEndPoint)UdpClient.Client.LocalEndPoint).Address;

        NetworkInterface ActiveInterface = null;

        foreach (var Interface in NetworkInterface.GetAllNetworkInterfaces())
        {
          if (Interface.GetIPProperties().UnicastAddresses.Exists(A => A.Address.Equals(LocalAddress)))
            ActiveInterface = Interface;

          if (ActiveInterface != null)
            break;
        }

        return ActiveInterface;
      }
    }
    public static bool IsWireless(this NetworkInterface Interface)
    {
      return Interface.NetworkInterfaceType == NetworkInterfaceType.Wireless80211;
    }
    public static bool IsEthernet(this NetworkInterface Interface)
    {
      return Interface.NetworkInterfaceType.ToString().Contains("Ethernet", StringComparison.CurrentCultureIgnoreCase);
    }
    public static bool IsLocalHost(this IPAddress Address)
    {
      var HostEntry = Dns.GetHostEntry(Address);
      var LocalHostEntry = Dns.GetHostEntry(Dns.GetHostName());

      foreach (IPAddress HostAddress in HostEntry.AddressList)
      {
        if (IPAddress.IsLoopback(HostAddress) || Array.IndexOf(LocalHostEntry.AddressList, HostAddress) != -1)
          return true;
      }

      return false;
    }
    public static IPAddress AsIPAddress(this string Source)
    {
      if (IPAddress.TryParse(Source, out IPAddress Address))
        return Address;

      var AddressList = Dns.GetHostEntry(Source).AddressList;
      var V4Addresses = AddressList.Where(A => !A.ToString().Contains(":") && !A.Equals(IPAddress.Loopback) && !A.Equals(IPAddress.IPv6Loopback)).ToList();
      var V4Address = V4Addresses.Where(A => !A.ToString().StartsWith("169")).FirstOrDefault();

      return V4Address;
    }
  }

  public static class ScreenHelper
  {
    [DllImport("gdi32.dll")]
    static extern int GetDeviceCaps(IntPtr hdc, int nIndex);
    public enum DeviceCap
    {
      HORZRES = 8,
      VERTRES = 10,
      DESKTOPVERTRES = 117,
      DESKTOPHORZRES = 118,

      // http://pinvoke.net/default.aspx/gdi32/GetDeviceCaps.html
    }

    public static float GetScalingFactorForDeviceContext(IntPtr DeviceContext, out int PhysicalScreenWidth, out int PhysicalScreenHeight)
    {
      var LogicalScreenHeight = GetDeviceCaps(DeviceContext, (int)DeviceCap.VERTRES);
      PhysicalScreenHeight = GetDeviceCaps(DeviceContext, (int)DeviceCap.DESKTOPVERTRES);
      PhysicalScreenWidth = GetDeviceCaps(DeviceContext, (int)DeviceCap.DESKTOPHORZRES);

      var ScreenScalingFactor = (float)PhysicalScreenHeight / (float)LogicalScreenHeight;

      return ScreenScalingFactor;
    }
  }

  public sealed class Monitor
  {
    public IntPtr Handle;
    public string Name;
    public Rect Location;
    public bool IsPrimary;
    public float ScalingFactor;
    public int PhysicalScreenWidth;
    public int PhysicalScreenHeight;
  }

  public static class MonitorHelper
  {
    public static IntPtr GetWindowHandle(System.Windows.Window Window)
    {
      return new WindowInteropHelper(Window).Handle;
    }

    public static Monitor[] GetMonitorInformation()
    {
      var InformationList = new List<Monitor>();
      Win32.User32.EnumDisplayMonitors(IntPtr.Zero, IntPtr.Zero, MonitorDelegate, IntPtr.Zero);
      return InformationList.ToArray();

      bool MonitorDelegate(IntPtr MonitorHandle, IntPtr DeviceContext, ref Rect Rect, IntPtr Data)
      {
        var MonitorInfo = new Win32.User32.MonitorInfoEx();

        if (Win32.User32.GetMonitorInfo(MonitorHandle, MonitorInfo))
        {
          InformationList.Add(MonitorInfo.AsMonitor(MonitorHandle));
        }

        return true;
      };
    }
    public static bool WindowIsOnPrimaryMonitor(IntPtr WindowHandle)
    {
      var MonitorHandle = Win32.User32.MonitorFromWindow(WindowHandle, Win32.User32.MonitorConstants.MONITOR_DEFAULTTONEAREST);
      return IsPrimaryMonitor(MonitorHandle);
    }
    public static bool WindowIsOnPrimaryMonitor(System.Windows.Window Window)
    {
      var MonitorHandle = GetCurrentMonitorHandle(Window);
      return IsPrimaryMonitor(MonitorHandle);
    }

    public static Monitor GetCurrentMonitorInformation(IntPtr WindowHandle)
    {
      return GetMonitorInformation(Win32.User32.MonitorFromWindow(WindowHandle, Win32.User32.MonitorConstants.MONITOR_DEFAULTTONEAREST));
    }
    public static Monitor GetCurrentMonitorInformation(System.Windows.Window Window)
    {
      return GetMonitorInformation(GetCurrentMonitorHandle(Window));
    }
    public static Monitor GetPrimaryMonitorInformation()
    {
      return GetMonitorInformation(GetPrimaryMonitorHandle());
    }

    private static Monitor GetMonitorInformation(IntPtr MonitorHandle)
    {
      var MonitorInfoEx = new Win32.User32.MonitorInfoEx();
      Win32.User32.GetMonitorInfo(MonitorHandle, MonitorInfoEx);

      return MonitorInfoEx.AsMonitor(MonitorHandle);
    }

    public static Rect GetWindowLocation(IntPtr WindowHandle)
    {
      var Rect = new Win32.User32.RectStruct();
      Win32.User32.GetWindowRect(WindowHandle, ref Rect);

      return new Rect(Rect.Left, Rect.Top, Rect.Right - Rect.Left, Rect.Bottom - Rect.Top);
    }

    private static IntPtr GetCurrentMonitorHandle(System.Windows.Window Window)
    {
      //var Point = new System.Drawing.Point((int)Window.Left, (int)Window.Top);
      // Use middle of window as the point for determining which screen the window is primarily on
      var Point = new System.Drawing.Point((int)Window.Left + ((int)Window.Width / 2), (int)Window.Top + ((int)Window.Height / 2));

      return Win32.User32.MonitorFromPoint(Point, Win32.User32.MonitorConstants.MONITOR_DEFAULTTONEAREST);
    }
    private static IntPtr GetPrimaryMonitorHandle()
    {
      var Point = new System.Drawing.Point(0, 0);

      return Win32.User32.MonitorFromPoint(Point, Win32.User32.MonitorConstants.MONITOR_DEFAULTTOPRIMARY);
    }
    private static bool IsPrimaryMonitor(IntPtr MonitorHandle)
    {
      var MonitorInfo = new Win32.User32.MonitorInfoEx();

      Win32.User32.GetMonitorInfo(MonitorHandle, MonitorInfo);

      return MonitorInfo.IsPrimaryMonitor();
    }
  }

  internal static class MonitorInfoExHelper
  {
    public static bool IsPrimaryMonitor(this Win32.User32.MonitorInfoEx MonitorInfoEx)
    {
      return (MonitorInfoEx.dwFlags & Win32.User32.MonitorConstants.MONITOR_DEFAULTTOPRIMARY) == Win32.User32.MonitorConstants.MONITOR_DEFAULTTOPRIMARY;
    }
    public static float ScalingFactor(this Win32.User32.MonitorInfoEx MonitorInfoEx, out int PhysicalScreenWidth, out int PhysicalScreenHeight)
    {
      PhysicalScreenWidth = 0;
      PhysicalScreenHeight = 0;

      IntPtr DC = IntPtr.Zero;
      try
      {
        var DeviceName = new string(MonitorInfoEx.szDevice);
        DC = Win32.Gdi32.CreateDC(null, DeviceName, null, IntPtr.Zero);

        if (DC != IntPtr.Zero)
          return ScreenHelper.GetScalingFactorForDeviceContext(DC, out PhysicalScreenWidth, out PhysicalScreenHeight);
        else
          return 1;
      }
      finally
      {
        Win32.Gdi32.DeleteDC(DC);
      }
    }

    public static Monitor AsMonitor(this Win32.User32.MonitorInfoEx MonitorInfoEx, IntPtr MonitorHandle)
    {
      return new Monitor
      {
        Handle = MonitorHandle,
        Name = new string(MonitorInfoEx.szDevice).Trim('\0'),
        Location = new Rect(
          Left: MonitorInfoEx.rcMonitor.Left,
          Top: MonitorInfoEx.rcMonitor.Top,
          Height: MonitorInfoEx.rcMonitor.Bottom - MonitorInfoEx.rcMonitor.Top,
          Width: MonitorInfoEx.rcMonitor.Right - MonitorInfoEx.rcMonitor.Left
        ),
        IsPrimary = MonitorInfoEx.IsPrimaryMonitor(),
        ScalingFactor = MonitorInfoEx.ScalingFactor(out var PhysicalScreenWidth, out var PhysicalScreenHeight),
        PhysicalScreenWidth = PhysicalScreenWidth,
        PhysicalScreenHeight = PhysicalScreenHeight
      };
    }
  }

  public static class CsvTableHelper
  {
    public static CsvReadFile ReadFile(this CsvTable CsvTable, string Path)
    {
      return CsvTable.ReadFile(new StreamReader(Path));
    }
    public static CsvWriteFile WriteFile(this CsvTable CsvTable, string Path)
    {
      return CsvTable.WriteFile(new StreamWriter(Path));
    }
  }

  public static class XmlDocumentHelper
  {
    public static void WriteToFile(this XmlDocument XmlDocument, string FilePath, System.Text.Encoding Encoding = null, bool OmitXMLDeclaration = false)
    {
      using (var FileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read))
        XmlDocument.WriteToStream(FileStream, Encoding, OmitXMLDeclaration);
    }
    public static void ReadFromFile(this XmlDocument XmlDocument, string FilePath)
    {
      using (var FileStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read, 65536))
        XmlDocument.ReadFromStream(FileStream);
    }
  }

  public static class XmlElementHelper
  {
    public static void WriteToFile(this XmlElement XMLElement, string FilePath, Encoding Encoding, bool OmitXMLDeclaration)
    {
      using (var FileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read))
      {
        XmlElementStreamWriteEngine.Execute
        (
          XmlElement: XMLElement,
          Stream: FileStream,
          Encoding: Encoding,
          OmitXMLDeclaration: OmitXMLDeclaration
        );
      }
    }
  }

  public static class DataObjectHelper
  {
    public static bool TryGetData<T>(this IDataObject DataObject, out T Result)
    {
      if (DataObject.GetDataPresent(typeof(T)))
      {
        Result = (T)DataObject.GetData(typeof(T));
        return true;
      }

      Result = default;
      return false;
    }
  }

  public static class ResourceFileFormatHelper
  {
    public static string GetInsightText(this Inv.Resource.FileFormat FileFormat, string FilePath)
    {
      var FileInfo = new FileInfo(FilePath);

      switch (FileFormat)
      {
        case Inv.Resource.FileFormat.Image:
          var ImageDimension = FileInfo.ReadImageDimension();
          return ImageDimension.Width + " x " + ImageDimension.Height;

        case Inv.Resource.FileFormat.Sound:
          return "~"; // TODO: this is too slow: FileInfo.ReadSoundLength().TotalSeconds.ToString("0.000");

        case Inv.Resource.FileFormat.Text:
          using (var FileStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
          using (var StreamReader = new StreamReader(FileStream))
            return StreamReader.ReadLine().Truncate(128);

        case Inv.Resource.FileFormat.Binary:
          return "~";

        default:
          throw new Exception("FileFormat not handled: " + FileFormat);
      }
    }

    public static string GetMDA5Hash(this string FilePath)
    {
      using (var FileStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
      using (var MD5 = System.Security.Cryptography.MD5.Create())
        return BitConverter.ToString(MD5.ComputeHash(FileStream)).Replace("-", "").ToLowerInvariant();
    }
  }

  public static class MediaColorHelper
  {
    public static bool IsTransparent(this System.Windows.Media.Color MediaColor)
    {
      return MediaColor.A == 0;
    }
    public static Inv.Colour ConvertToInvColour(this System.Windows.Media.Color? MediaColor)
    {
      return MediaColor != null ? MediaColor.Value.ConvertToInvColour() : (Inv.Colour)null;
    }
    public static Inv.Colour ConvertToInvColour(this System.Windows.Media.Color MediaColor)
    {
      var ArgbArray = new byte[4] { MediaColor.B, MediaColor.G, MediaColor.R, MediaColor.A };

      return Inv.Colour.FromArgb(BitConverter.ToInt32(ArgbArray, 0));
    }
    public static System.Windows.Media.Color? ConvertToMediaColor(this Inv.Colour InvColour)
    {
      if (InvColour == null)
      {
        return null;
      }
      else
      {
        var ArgbArray = BitConverter.GetBytes(InvColour.RawValue);
        return System.Windows.Media.Color.FromArgb(ArgbArray[3], ArgbArray[2], ArgbArray[1], ArgbArray[0]);
      }
    }
  }

  public static class ImageHelper
  {
    /// <summary>
    /// Indicates whether the image's buffer contains a bitmap with multiple frames, e.g. a multi-page TIFF.
    /// </summary>
    /// <param name="Image">The <see cref="Inv.Image"/> to test for the presence of multiple frames.</param>
    /// <returns>Whether the contained bitmap has multiple frames.</returns>
    public static bool IsMultiFrameBitmap(this Inv.Image Image)
    {
      using (var FileStream = new System.IO.MemoryStream(Image.GetBuffer()))
      {
        var Decoder = BitmapDecoder.Create(FileStream, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.None);
        return Decoder.Frames.Count > 1;
      }
    }
    public static BitmapSource Rotate(this BitmapSource Source, int Angle)
    {
      var Result = new TransformedBitmap(Source, new RotateTransform(Angle)).AsBitmapSource();
      Result.Freeze();
      return Result;
    }
    public static BitmapImage Rotate(this Inv.Image InvImage, int Angle)
    {
      var TransformedBitmapSource = new TransformedBitmap(InvImage.ConvertToMediaBitmap(), new RotateTransform(Angle));
      var Result = new BitmapImage();
      using (var Stream = new System.IO.MemoryStream())
      {
        TransformedBitmapSource.SaveToStreamAsBmp(Stream);
        Stream.Position = 0;

        Result.BeginInit();
        Result.CacheOption = BitmapCacheOption.OnLoad;
        Result.StreamSource = Stream;
        Result.EndInit();
        Result.Freeze();
      }

      return Result;
    }
    public static Inv.Image ConvertToInvImagePng(this ImageSource ImageSource)
    {
      if (ImageSource == null)
      {
        return null;
      }
      else
      {
        using (var MemoryStream = new System.IO.MemoryStream())
        {
          ImageSource.SaveToStreamAsPng(MemoryStream);

          return new Inv.Image(MemoryStream.ToArray(), ".png");
        }
      }
    }
    public static Inv.Image ConvertToInvImageJpeg(this ImageSource ImageSource, int Quality = 100)
    {
      if (ImageSource == null)
      {
        return null;
      }
      else
      {
        using (var MemoryStream = new System.IO.MemoryStream())
        {
          ImageSource.SaveToStreamAsJpg(MemoryStream, Quality);

          return new Inv.Image(MemoryStream.ToArray(), ".jpg");
        }
      }
    }
    public static bool StreamEquals(this BitmapSource LeftBitmapSource, BitmapSource RightBitmapSource)
    {
      using (var LeftMemoryStream = new System.IO.MemoryStream())
      {
        using (var RightMemoryStream = new System.IO.MemoryStream())
        {
          var LeftEncoder = new System.Windows.Media.Imaging.PngBitmapEncoder();
          LeftEncoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(LeftBitmapSource));
          LeftEncoder.Save(LeftMemoryStream);

          var RightEncoder = new System.Windows.Media.Imaging.PngBitmapEncoder();
          RightEncoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(RightBitmapSource));
          RightEncoder.Save(RightMemoryStream);

          return LeftMemoryStream.GetBuffer().CompareTo(RightMemoryStream.GetBuffer()) == 0;
        }
      }
    }
    public static BitmapSource Scale(this BitmapSource BitmapSource, double Scale)
    {
      return BitmapSource.Scale(Scale, Scale);
    }
    public static BitmapSource Scale(this BitmapSource BitmapSource, double Width, double Height)
    {
      var TransformedBitmapSource = new TransformedBitmap(BitmapSource, new ScaleTransform(Width, Height));

      using (var Stream = new System.IO.MemoryStream())
      {
        TransformedBitmapSource.SaveToStreamAsBmp(Stream);
        Stream.Position = 0;

        var Result = new BitmapImage();
        Result.BeginInit();
        Result.CacheOption = BitmapCacheOption.OnLoad;
        Result.StreamSource = Stream;
        Result.EndInit();
        Result.Freeze();
        return Result;
      }
    }
    /// <summary>
    /// Make the image transparent using a reference colour. Similar to Gimp 'Color To Alpha' function.
    /// </summary>
    /// <param name="BitmapSource"></param>
    /// <param name="ReferenceColour"></param>
    /// <returns></returns>
    public static BitmapSource ColourToTransparent(this BitmapSource BitmapSource, Inv.Colour ReferenceColour)
    {
      var ReferenceRecord = ReferenceColour.GetARGBRecord();

      // Source: https://stackoverflow.com/questions/9280902/what-algorithm-is-behind-the-gimps-color-to-alpha-feature

      const double mX = 255; // Maximum channel value.
      const double mA = 255; // Maximum alpha value.

      double r1 = ReferenceRecord.R;
      double r2 = ReferenceRecord.G;
      double r3 = ReferenceRecord.B;
      double aA, a1, a2, a3;
      double pA, p1, p2, p3;

      var Pixels = ReadPixels(BitmapSource);

      for (var PixelY = 0; PixelY < Pixels.Height; PixelY++)
      {
        for (var PixelX = 0; PixelX < Pixels.Width; PixelX++)
        {
          var PixelRecord = Pixels[PixelX, PixelY].GetARGBRecord();

          pA = PixelRecord.A;
          p1 = PixelRecord.R;
          p2 = PixelRecord.G;
          p3 = PixelRecord.B;

          // a1 calculation: minimal alpha giving r1 from p1
          if (p1 > r1) a1 = mA * (p1 - r1) / (mX - r1);
          else if (p1 < r1) a1 = mA * (r1 - p1) / r1;
          else a1 = 0.0;
          // a2 calculation: minimal alpha giving r2 from p2
          if (p2 > r2) a2 = mA * (p2 - r2) / (mX - r2);
          else if (p2 < r2) a2 = mA * (r2 - p2) / r2;
          else a2 = 0.0;
          // a3 calculation: minimal alpha giving r3 from p3
          if (p3 > r3) a3 = mA * (p3 - r3) / (mX - r3);
          else if (p3 < r3) a3 = mA * (r3 - p3) / r3;
          else a3 = 0.0;
          // aA calculation: max(a1, a2, a3)
          aA = a1;
          if (a2 > aA) aA = a2;
          if (a3 > aA) aA = a3;
          // apply aA to pixel:
          if (aA >= mA / mX)
          {
            pA = aA * pA / mA;
            p1 = mA * (p1 - r1) / aA + r1;
            p2 = mA * (p2 - r2) / aA + r2;
            p3 = mA * (p3 - r3) / aA + r3;
          }
          else
          {
            pA = 0;
            p1 = 0;
            p2 = 0;
            p3 = 0;
          }

          PixelRecord.A = (byte)pA;
          PixelRecord.R = (byte)p1;
          PixelRecord.G = (byte)p2;
          PixelRecord.B = (byte)p3;

          Pixels[PixelX, PixelY] = Inv.Colour.FromArgbRecord(PixelRecord);
        }
      }

      return WriteImage(Pixels);
    }
    /// <summary>
    /// Maintain the aspect ratio to produce a thumbnail version of the original image.
    /// </summary>
    /// <param name="Image"></param>
    /// <param name="ThumbnailSize"></param>
    /// <returns></returns>
    public static ImageSource Thumbnail(this ImageSource Image, int ThumbnailSize)
    {
      if (Image == null || Image.Width <= ThumbnailSize || Image.Height <= ThumbnailSize)
        return Image;

      var ThumbWidth = ThumbnailSize;
      var ThumbHeight = ThumbnailSize;

      if (Image.Width != Image.Height)
      {
        var AspectRatio = Image.Width / Image.Height;

        if (AspectRatio >= 1)
          ThumbWidth = (int)(ThumbWidth * AspectRatio);
        else if (AspectRatio > 0)
          ThumbHeight = (int)(ThumbHeight / AspectRatio);
      }

      var Thumbnail = Image.Resize(ThumbWidth, ThumbHeight);
      Thumbnail.Freeze();

      return Thumbnail;
    }
    /// <summary>
    /// Crop to produce a square image and optionally scaled to a specified size.
    /// </summary>
    /// <param name="InputSource"></param>
    /// <param name="ImageSize"></param>
    /// <returns></returns>
    public static BitmapSource Clamped(this BitmapSource InputSource, int? ImageSize = null)
    {
      if (InputSource == null)
        return null;

      CroppedBitmap CroppedSource;
      if (InputSource.PixelWidth > InputSource.PixelHeight)
        CroppedSource = new CroppedBitmap(InputSource, new System.Windows.Int32Rect((InputSource.PixelWidth - InputSource.PixelHeight) / 2, 0, InputSource.PixelHeight, InputSource.PixelHeight));
      else
        CroppedSource = new CroppedBitmap(InputSource, new System.Windows.Int32Rect(0, (InputSource.PixelHeight - InputSource.PixelWidth) / 2, InputSource.PixelWidth, InputSource.PixelWidth));

      //CroppedSource.Freeze();

      if (ImageSize == null)
        return CroppedSource;

      var NewSize = (double)ImageSize.Value;

      var Result = new TransformedBitmap(CroppedSource, new ScaleTransform(NewSize / CroppedSource.PixelWidth, NewSize / CroppedSource.PixelHeight, 0, 0));

      //Result.Freeze();

      return Result;
    }
    public static BitmapSource WriteImage(this Inv.BGRAByteArrayPixels Pixels)
    {
      var Bitmap = new WriteableBitmap(Pixels.Width, Pixels.Height, 96, 96, PixelFormats.Bgra32, null);
      Bitmap.WritePixels(new System.Windows.Int32Rect(0, 0, Pixels.Width, Pixels.Height), Pixels.PixelArray, Pixels.PixelStride, 0);
      Bitmap.Freeze();
      return Bitmap;
    }
    public static Inv.BGRAByteArrayPixels ReadPixels(this BitmapSource Image)
    {
      if (Image == null)
        return null;

      var PixelWidth = Image.PixelWidth;
      var PixelHeight = Image.PixelHeight;
      var PixelStride = PixelWidth * Inv.BGRAByteArrayPixels.PixelSize;
      var PixelArray = new byte[PixelHeight * PixelStride];

      Image.CopyPixels(new System.Windows.Int32Rect(0, 0, PixelWidth, PixelHeight), PixelArray, PixelStride, 0);

      return new Inv.BGRAByteArrayPixels(PixelWidth, PixelHeight, PixelArray);
    }
    public static BitmapSource Crop(this BitmapSource Image, Inv.Rect Rect)
    {
      return new System.Windows.Media.Imaging.CroppedBitmap(Image, new System.Windows.Int32Rect(Rect.Left, Rect.Top, Rect.Width, Rect.Height));
    }
    public static BitmapSource Resize(this ImageSource Image, int ResizeWidth, int ResizeHeight, BitmapScalingMode ScalingMode = BitmapScalingMode.Fant)
    {
      var Rect = new System.Windows.Rect(0, 0, ResizeWidth, ResizeHeight);

      var Group = new DrawingGroup();
      RenderOptions.SetBitmapScalingMode(Group, ScalingMode);
      Group.Children.Add(new ImageDrawing(Image, Rect));

      var DrawingVisual = new DrawingVisual();
      using (var DrawingContext = DrawingVisual.RenderOpen())
        DrawingContext.DrawDrawing(Group);

      var ResizedImage = new RenderTargetBitmap(ResizeWidth, ResizeHeight, 96, 96, PixelFormats.Default);
      ResizedImage.Render(DrawingVisual);
      ResizedImage.Freeze();
      ResizedImage.ReleaseLooseGDIHandles();

      return ResizedImage;
    }
    public static BitmapImage ConvertToMediaBitmapAndFreeze(this Inv.Image InvImage)
    {
      using (var MemoryStream = new System.IO.MemoryStream(InvImage.GetBuffer()))
      {
        var Result = new BitmapImage();
        Result.LoadFromStream(MemoryStream);
        Result.Freeze();
        return Result;
      }
    }
    public static BitmapImage ConvertToMediaBitmapAndFreeze(this Inv.Image InvImage, int Width, int Height)
    {
      using (var MemoryStream = new System.IO.MemoryStream(InvImage.GetBuffer()))
      {
        var Result = new BitmapImage();
        Result.LoadFromStream(MemoryStream, Width, Height);
        Result.Freeze();
        return Result;
      }
    }
    public static void LoadFromStream(this BitmapImage BitmapImage, System.IO.Stream Stream, bool IgnoreColorProfile = false, int? Width = null, int? Height = null)
    {
      Debug.Assert(Stream != null, "Stream must not be null.");

      LoadFromStreamWithImageReadFailureHandling(() =>
      {
        BitmapImage.BeginInit();

        if (IgnoreColorProfile)
          BitmapImage.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;

        BitmapImage.CacheOption = BitmapCacheOption.OnLoad;

        if (Width != null)
          BitmapImage.DecodePixelWidth = Width.Value;

        if (Height != null)
          BitmapImage.DecodePixelHeight = Height.Value;

        BitmapImage.StreamSource = Stream;
        BitmapImage.EndInit();
      });
    }
    public static void LoadFromStream(this BitmapImage BitmapImage, System.IO.Stream Stream, int Width, int Height)
    {
      Debug.Assert(Stream != null, "Stream must not be null.");

      LoadFromStreamWithImageReadFailureHandling(() =>
      {
        var SourceWidth = 0;
        var SourceHeight = 0;

        if (Stream.CanSeek)
        {
          // Create a temporary bitmap to determine the original aspect ratio of the image.
          var SourceBitmap = new BitmapImage();
          SourceBitmap.BeginInit();
          SourceBitmap.StreamSource = Stream;
          SourceBitmap.EndInit();

          SourceWidth = SourceBitmap.PixelWidth;
          SourceHeight = SourceBitmap.PixelHeight;

          Stream.Seek(0, System.IO.SeekOrigin.Begin);
        }

        BitmapImage.BeginInit();
        BitmapImage.CacheOption = BitmapCacheOption.OnLoad;
        BitmapImage.StreamSource = Stream;

        if (Stream.CanSeek)
        {
          if (SourceWidth / (double)SourceHeight > Width / (double)Height)
            BitmapImage.DecodePixelHeight = Height;
          else
            BitmapImage.DecodePixelWidth = Width;
        }
        else
        {
          BitmapImage.DecodePixelWidth = Width;
          BitmapImage.DecodePixelHeight = Height;
        }

        BitmapImage.EndInit();
      });
    }
    public static void SaveToStreamAsBmp(this BitmapSource BitmapSource, System.IO.Stream Stream)
    {
      Debug.Assert(Stream != null, "Stream must not be null.");

      var Encoder = new BmpBitmapEncoder();
      Encoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(BitmapSource));
      Encoder.Save(Stream);
    }
    public static void SaveToStreamAsBmp(this ImageSource ImageSource, System.IO.Stream Stream)
    {
      Debug.Assert(ImageSource is BitmapSource ||
                   ImageSource is DrawingImage, "ImageSource is of unknown type (possibly D3DImage?)");

      if (ImageSource is BitmapSource)
        SaveToStreamAsBmp((BitmapSource)ImageSource, Stream);
      else if (ImageSource is DrawingImage)
        SaveToStreamAsBmp(ConvertDrawingImageToBitmapSource((DrawingImage)ImageSource), Stream);
    }
    public static BitmapSource AsBitmapSource(this ImageSource ImageSource)
    {
      Debug.Assert(ImageSource is BitmapSource ||
                  ImageSource is DrawingImage, "ImageSource is of unknown type (possibly D3DImage?)");

      if (ImageSource is BitmapSource)
        return (BitmapSource)ImageSource;
      else if (ImageSource is DrawingImage)
        return ConvertDrawingImageToBitmapSource((DrawingImage)ImageSource);

      return null;
    }
    public static void SaveToStreamAsPng(this BitmapSource BitmapSource, System.IO.Stream Stream)
    {
      Debug.Assert(Stream != null, "Stream must not be null.");

      var Encoder = new PngBitmapEncoder();
      Encoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(BitmapSource));
      Encoder.Save(Stream);
    }
    public static void SaveToStreamAsPng(this ImageSource ImageSource, System.IO.Stream Stream)
    {
      Debug.Assert(ImageSource is BitmapSource || ImageSource is DrawingImage, "ImageSource is of unknown type (possibly D3DImage?)");

      if (ImageSource is BitmapSource)
        SaveToStreamAsPng((BitmapSource)ImageSource, Stream);
      else if (ImageSource is DrawingImage)
        SaveToStreamAsPng(ConvertDrawingImageToBitmapSource((DrawingImage)ImageSource), Stream);

    }
    public static void SaveToStreamAsJpg(this BitmapSource BitmapSource, System.IO.Stream Stream, int QualityLevel)
    {
      Debug.Assert(Stream != null, "Stream must not be null.");

      var Encoder = new JpegBitmapEncoder();
      if (QualityLevel > 0)
        Encoder.QualityLevel = QualityLevel;

      Encoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(BitmapSource));
      Encoder.Save(Stream);
    }
    public static void SaveToStreamAsJpg(this ImageSource ImageSource, System.IO.Stream Stream, int QualityLevel)
    {
      Debug.Assert(ImageSource is BitmapSource || ImageSource is DrawingImage, "ImageSource is of unknown type (possibly D3DImage?)");

      if (ImageSource is BitmapSource)
        SaveToStreamAsJpg((BitmapSource)ImageSource, Stream, QualityLevel);
      else if (ImageSource is DrawingImage)
        SaveToStreamAsJpg(ConvertDrawingImageToBitmapSource((DrawingImage)ImageSource), Stream, QualityLevel);
    }
    public static BitmapImage ConvertToMediaBitmap(this Inv.Image InvImage, bool IgnoreColorProfile = false, int? Width = null, int? Height = null)
    {
      using (var MemoryStream = new System.IO.MemoryStream(InvImage.GetBuffer()))
      {
        var Result = new BitmapImage();
        Result.LoadFromStream(MemoryStream, IgnoreColorProfile, Width, Height);
        return Result;
      }
    }
    public static BitmapSource AsTint(this BitmapSource BitmapSource, Brush TintBrush)
    {
      var DrawingVisual = new System.Windows.Media.DrawingVisual();

      using (var WpfDrawingContext = DrawingVisual.RenderOpen())
      {
        var WpfImageRect = new System.Windows.Rect(0, 0, BitmapSource.Width, BitmapSource.Height);
        WpfDrawingContext.DrawImage(BitmapSource, WpfImageRect);

        var WpfImageBrush = new System.Windows.Media.ImageBrush(BitmapSource);
        WpfImageBrush.Freeze();

        WpfDrawingContext.PushOpacityMask(WpfImageBrush);
        WpfDrawingContext.DrawRectangle(TintBrush, null, WpfImageRect);
        WpfDrawingContext.Pop();
      }

      var WpfBitmap = new RenderTargetBitmap(BitmapSource.PixelWidth, BitmapSource.PixelHeight, BitmapSource.DpiX, BitmapSource.DpiY, System.Windows.Media.PixelFormats.Default);
      WpfBitmap.Render(DrawingVisual);
      WpfBitmap.Freeze();
      WpfBitmap.ReleaseLooseGDIHandles();

      return WpfBitmap;
    }
    public static Inv.Image AsGrayscale(this Inv.Image Image)
    {
      return Image.ConvertToMediaBitmapAndFreeze().AsGrayscale().ConvertToInvImagePng();
    }
    public static BitmapSource AsGrayscale(this BitmapSource BitmapSource)
    {
      var Result = UnsafeGrayscaleConversion(BitmapSource);
      Result.Freeze();
      return Result;
    }
    /// <summary>
    /// Workaround without forcing the GC is to manually release the handle in the RenderTargetBitmap using reflection.
    /// https://github.com/dotnet/wpf/issues/3067
    /// </summary>
    /// <param name="RenderedImage"></param>
    public static void ReleaseLooseGDIHandles(this RenderTargetBitmap RenderedImage)
    {
      var RenderedField = RenderedImage.GetType().GetField("_renderTargetBitmap", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
      var RenderedDisposable = RenderedField?.GetValue(RenderedImage) as IDisposable;
      RenderedDisposable?.Dispose();
    }

    internal static BitmapSource ConvertDrawingImageToBitmapSource(DrawingImage image)
    {
      var wrapperImage = new System.Windows.Controls.Image();
      wrapperImage.Source = image;
      wrapperImage.Measure(new System.Windows.Size(image.Width, image.Height));
      wrapperImage.Arrange(new System.Windows.Rect(0, 0, image.Width, image.Height));

      var render = new RenderTargetBitmap((int)(image.Width * 96), (int)(image.Height * 96), 96, 96, PixelFormats.Default);
      render.Render(wrapperImage);
      return render;
    }

    private static unsafe BitmapSource UnsafeGrayscaleConversion(BitmapSource Source)
    {
      try
      {
        var Result = new WriteableBitmap(Source);
        Result.Lock();
        try
        {
          var buff = Result.BackBuffer;
          var stride = Result.BackBufferStride;

          if (Result.Format == PixelFormats.Bgra32)
          {
            var nWidth = Result.PixelWidth;
            var nHeight = Result.PixelHeight;

            for (var y = 0; y < nHeight; y++)
            {
              var p = (byte*)buff;

              for (var x = 0; x < nWidth; x++)
              {
                var i = y * stride + x * 4;
                var Avg = (byte)((p[i] + p[i + 1] + p[i + 2]) / 3);

                p[i] = Avg;
                p[i + 1] = Avg;
                p[i + 2] = Avg;
                //p[i + 3] = unchanged alpha.
              }
            }

            return Result;
          }
          else
          {
            Debug.WriteLine("PixelFormat not supported for correct grayscale conversion: " + Result.Format);
          }
        }
        finally
        {
          Result.Unlock();
        }
      }
      catch (Exception Exception)
      {
        // failed to convert to grayscale.
        Debug.WriteLine(Exception.Describe());
      }

      // couldn't use the pixel manipulation (stride was not 4 or there was an exception).
      var ConvertedBitmap = new FormatConvertedBitmap();
      ConvertedBitmap.BeginInit();
      ConvertedBitmap.Source = Source;
      ConvertedBitmap.DestinationFormat = PixelFormats.Gray32Float;
      ConvertedBitmap.AlphaThreshold = 100;
      ConvertedBitmap.EndInit();

      // Create Opacity Mask for greyscale image as FormatConvertedBitmap does not keep transparency info
      return ConvertedBitmap;
    }
    private static void LoadFromStreamWithImageReadFailureHandling(Action LoadFromStreamEvent)
    {
      Debug.Assert(LoadFromStreamEvent != null, "LoadFromStreamEvent must not be null.");

      try
      {
        LoadFromStreamEvent();
      }
      catch (System.NotSupportedException e) when (e.InnerException is System.Runtime.InteropServices.COMException COMException && COMException.HResult == unchecked((int)0x88982F50))
      {
        throw new ImageByteArrayReadFailException("Failed to read image byte array.", e);
      }
    }

    public class ImageByteArrayReadFailException : Exception
    {
      public ImageByteArrayReadFailException() : base() { }
      public ImageByteArrayReadFailException(string message) : base(message) { }
      public ImageByteArrayReadFailException(string message, Exception inner) : base(message, inner) { }
    }
  }

  public static class FrameworkElementHelper
  {
    public static BitmapSource RenderToBitmapSource(this System.Windows.FrameworkElement Element, double DPI = 96.0)
    {
      Element.Measure(new System.Windows.Size(double.PositiveInfinity, double.PositiveInfinity));
      Element.Arrange(new System.Windows.Rect(Element.DesiredSize));

      var Ratio = DPI / 96.0;

      var Result = new RenderTargetBitmap((int)(Element.ActualWidth * Ratio), (int)(Element.ActualHeight * Ratio), DPI, DPI, PixelFormats.Pbgra32);
      Result.Render(Element);
      Result.Freeze();
      Result.ReleaseLooseGDIHandles();

      return Result;
    }
    public static BitmapSource ScaledRenderToBitmapSource(this System.Windows.FrameworkElement Element, int Width, int Height, double DPI = 96.0)
    {
      var ScaleTransform = new ScaleTransform()
      {
        ScaleX = double.IsNaN(Element.Width) ? 1 : Width / Element.Width,
        ScaleY = double.IsNaN(Element.Height) ? 1 : Height / Element.Height
      };
      var ContentControl = new System.Windows.Controls.ContentControl()
      {
        Content = Element,
        RenderTransform = ScaleTransform
      };
      ContentControl.Measure(new System.Windows.Size(double.PositiveInfinity, double.PositiveInfinity));
      ContentControl.Arrange(new System.Windows.Rect(ContentControl.DesiredSize));

      var Ratio = DPI / 96.0;

      var Result = new RenderTargetBitmap((int)(Width * Ratio), (int)(Height * Ratio), DPI, DPI, PixelFormats.Pbgra32);
      Result.Render(ContentControl);
      Result.Freeze();
      Result.ReleaseLooseGDIHandles();

      return Result;
    }
    public static DrawingVisual WrapWithDrawingVisual(this System.Windows.FrameworkElement Element, System.Windows.Size ElementSize, System.Windows.Media.Brush BackgroundBrush = null)
    {
      var DrawingVisual = new DrawingVisual();
      using (var DrawingContext = DrawingVisual.RenderOpen())
      {
        var VisualBrush = new VisualBrush(Element);

        if (BackgroundBrush != null)
          DrawingContext.DrawRectangle(BackgroundBrush, null, new System.Windows.Rect(0, 0, ElementSize.Width, ElementSize.Height));

        DrawingContext.DrawRectangle(VisualBrush, null, new System.Windows.Rect(0, 0, ElementSize.Width, ElementSize.Height));
      }

      return DrawingVisual;
    }
  }

  public static class DrawingVisualHelper
  {
    public static BitmapSource RenderToBitmapSource(this DrawingVisual Visual, System.Windows.Size Size, double DPI = 96.0)
    {
      var Ratio = DPI / 96.0;

      var Result = new RenderTargetBitmap((int)(Size.Width * Ratio), (int)(Size.Height * Ratio), DPI, DPI, PixelFormats.Pbgra32);
      Result.Render(Visual);
      Result.Freeze();
      Result.ReleaseLooseGDIHandles();

      return Result;
    }
  }

  public static class DrawingBitmapHelper
  {
    /// <summary>
    /// Convert this <see cref="BitmapSource"/> to a <see cref="System.Drawing.Bitmap"/>.
    /// </summary>
    /// <param name="BitmapSource">This <see cref="BitmapSource"/>.</param>
    /// <returns>A <see cref="System.Drawing.Bitmap"/> with the image contents of this <see cref="BitmapSource"/>.</returns>
    public static System.Drawing.Bitmap ConvertToDrawingBitmap(this BitmapSource BitmapSource)
    {
      if (BitmapSource == null)
      {
        return null;
      }
      else
      {
        using (var MemoryStream = new System.IO.MemoryStream())
        {
          var ImageEncoder = new PngBitmapEncoder();
          ImageEncoder.Frames.Add(BitmapFrame.Create(BitmapSource));
          ImageEncoder.Save(MemoryStream);

          return new System.Drawing.Bitmap(MemoryStream);
        }
      }
    }
    public static System.Drawing.Bitmap ConvertToDrawingBitmap(this ImageSource ImageSource)
    {
      Debug.Assert(ImageSource is BitmapSource || ImageSource is DrawingImage, "ImageSource is of unknown type (possibly D3DImage?)");

      if (ImageSource is BitmapSource)
        return ConvertToDrawingBitmap((BitmapSource)ImageSource);
      else if (ImageSource is DrawingImage)
        return ConvertToDrawingBitmap(ImageHelper.ConvertDrawingImageToBitmapSource((DrawingImage)ImageSource));

      return null;
    }
    public static System.Drawing.Bitmap DrawingBitmapAsGrayscale(this System.Drawing.Bitmap original)
    {
      //create a blank bitmap the same size as original
      var newBitmap = new System.Drawing.Bitmap(original.Width, original.Height);

      //get a graphics object from the new image
      var g = System.Drawing.Graphics.FromImage(newBitmap);

      //create the grayscale ColorMatrix
      var colorMatrix = new System.Drawing.Imaging.ColorMatrix(new float[][]
      {
        new float[] { .3f, .3f, .3f, 0, 0 },
        new float[] { .59f, .59f, .59f, 0, 0 },
        new float[] { .11f, .11f, .11f, 0, 0 },
        new float[] { 0, 0, 0, 1, 0 },
        new float[] { 0, 0, 0, 0, 1 }
      });

      //create some image attributes
      var attributes = new System.Drawing.Imaging.ImageAttributes();

      //set the color matrix attribute
      attributes.SetColorMatrix(colorMatrix);

      //draw the original image on the new image
      //using the grayscale color matrix
      g.DrawImage(original, new System.Drawing.Rectangle(0, 0, original.Width, original.Height),
         0, 0, original.Width, original.Height, System.Drawing.GraphicsUnit.Pixel, attributes);

      //dispose the Graphics object
      g.Dispose();
      return newBitmap;
    }
  }

  public sealed class EnvironmentAuthority
  {
    internal EnvironmentAuthority(string Name, string Host, string Root)
    {
      this.Name = Name;
      this.Host = Host;
      this.Root = Root;
    }

    public string Name { get; }
    public string Host { get; }
    public string Root { get; }
  }

  public static class EnvironmentHelper
  {
    public static EnvironmentAuthority GetMachineAuthority()
    {
      return new EnvironmentAuthority(Environment.MachineName, string.Empty, string.Empty);
    }
    public static EnvironmentAuthority GetDomainAuthority()
    {
      var UserDNSDomain = Environment.GetEnvironmentVariable("USERDNSDOMAIN").NullAsEmpty().ToLower();

      return new EnvironmentAuthority
      (
        Name: !string.IsNullOrEmpty(UserDNSDomain) ? Environment.UserDomainName : Environment.MachineName,
        Host: !string.IsNullOrEmpty(UserDNSDomain) ? UserDNSDomain : string.Empty,
        Root: !string.IsNullOrEmpty(UserDNSDomain) ? UserDNSDomain.Split('.').Select(Part => "DC=" + Part).AsSeparatedText(",") : string.Empty
      );
    }
    public static string DotNetFramework()
    {
      return GetDotNetFrameworkFromRegistry() + " (" + Environment.Version.ToString() + ")";
    }

    /// <summary>
    /// Resolve the machine name including support for remote access services.<br />
    /// <br />
    /// Tested for the following deployment scenarios:<br />
    /// 1. VMWare Horizon<br />
    /// 2. VDI Infrastructure<br />
    /// 3. Standard RDP<br />
    /// 4. Running the client locally<br />
    /// </summary>
    /// <returns></returns>
    public static string ResolveMachineName()
    {
      RegistryKey GetKeys(string keyName)
      {
        // Read a key and catch all the known exceptions when reading from the registry.
        RegistryKey returnValue = null;

        try
        {
          returnValue = Registry.CurrentUser.OpenSubKey(keyName);
        }
        catch (System.Security.SecurityException)
        {
          // The user does not have the permissions required to read from the registry key.
        }
        catch (System.IO.IOException)
        {
          // The RegistryKey that contains the specified value has been marked for deletion.
        }
        catch (System.ArgumentException)
        {
          // keyName does not begin with a valid registry root.
        }

        return returnValue;
      }
      string GetStringValueFromRegistry(RegistryKey Key, string KeyName)
      {
        // Ensure the key type is a string (REG_SZ) and catch all the known exceptions when reading from the registry.
        var Result = "";
        try
        {
          // Check key exists before we check the key type.
          if (Key.GetValue(KeyName) != null && Key.GetValueKind(KeyName) == RegistryValueKind.String)
            Result = Key.GetValue(KeyName) as string;
        }
        catch (System.Security.SecurityException)
        {
          // The user does not have the permissions required to read from the registry key.
        }
        catch (System.IO.IOException)
        {
          // The RegistryKey that contains the specified value has been marked for deletion.
        }
        catch (System.ArgumentException)
        {
          // KeyName does not begin with a valid registry root.
        }

        return Result;
      }

      try
      {
        // Remote Access Services use the special 'Volatile Environment' registry hive for in-memory only keys (we can inspect these for keys from known service providers).
        var VolatileEnvironment = GetKeys("Volatile Environment");
        if (VolatileEnvironment != null)
        {
          // grab the current session ID this process is running in.
          var SessionID = System.Diagnostics.Process.GetCurrentProcess().SessionId;
          var SessionKeys = VolatileEnvironment.OpenSubKey(SessionID.ToString());

          if (SessionKeys != null)
          {
            // Check to see if this session is being accessed through a VMWare Horizon session.
            var VMWareHorizon_MachineName = GetStringValueFromRegistry(SessionKeys, "ViewClient_Machine_Name");
            if (!string.IsNullOrWhiteSpace(VMWareHorizon_MachineName))
              return VMWareHorizon_MachineName;

            // Check to see if this session is being accessed through a RDP (or compatible) session.
            var RDP_MachineName = GetStringValueFromRegistry(SessionKeys, "CLIENTNAME");
            if (!string.IsNullOrWhiteSpace(RDP_MachineName))
              return RDP_MachineName;
          }
        }
      }
      catch (Exception Exception)
      {
        Debug.WriteLine(Exception.Message);
        // suppress all exceptions at this point.
      }

      // Fallback to the Environment.MachineName.
      // NOTE: Environment.MachineName can throw an InvalidOperationException when the name of the computer cannot be obtained: https://docs.microsoft.com/en-us/dotnet/api/system.environment.machinename
      return Environment.MachineName;
    }

    private static string GetDotNetFrameworkFromRegistry()
    {
      try
      {
        using (var BaseKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\\"))
        {
          var ReleaseKey = Convert.ToInt32(BaseKey.GetValue("Release"));

          // Checking the version using >= will enable forward compatibility,  
          // however you should always compile your code on newer versions of 
          // the framework to ensure your app works the same. 

          if (ReleaseKey == 528040)
            return "4.8";
          else if (ReleaseKey > 528040)
            return "4.8 + patches";

          if (ReleaseKey == 461808/* Windows 10 */ || ReleaseKey == 461814/* Other Windows */)
            return "4.7.2";
          else if (ReleaseKey > 461808)
            return "4.7.2 + patches";

          if (ReleaseKey == 461308/* Windows 10 */ || ReleaseKey == 461310/* Other Windows */)
            return "4.7.1";
          else if (ReleaseKey > 461308)
            return "4.7.1 + patches";

          if (ReleaseKey == 460798/* Windows 10 */ || ReleaseKey == 460805/* Other Windows */)
            return "4.7.0";
          else if (ReleaseKey > 460798)
            return "4.7.0 + patches";

          if (ReleaseKey == 394802/* Windows 10 */ || ReleaseKey == 394806/* Other Windows */)
            return "4.6.2";
          else if (ReleaseKey > 394802)
            return "4.6.2 + patches";

          if (ReleaseKey == 394254/* Windows 10 */ || ReleaseKey == 394271/* Other Windows */)
            return "4.6.1";
          else if (ReleaseKey > 394254)
            return "4.6.1 + patches";

          if (ReleaseKey == 393295/* Windows 10 */ || ReleaseKey == 393297/* Other Windows */)
            return "4.6.0";
          else if (ReleaseKey > 393295)
            return "4.6.0 + patches";

          if (ReleaseKey == 379893)
            return "4.5.2";
          else if (ReleaseKey > 379893)
            return "4.5.2 + patches";

          if (ReleaseKey == 378675/*Windows 8.1*/ || ReleaseKey == 378758/*Windows 8, Windows 7, etc*/)
            return "4.5.1";
          else if (ReleaseKey > 378675)
            return "4.5.1 + patches";

          if (ReleaseKey == 378389)
            return "4.5.0";
          else if (ReleaseKey > 378389)
            return "4.5.0 + patches";

          // This line should never execute. A non-null release key should mean that 4.5 or later is installed. 
          return "?.?.?";
        }
      }
      catch
      {
        return "4.5.?";
      }
    }
  }

  public static class CodepointHelper
  {
    /// <summary>
    /// Automate Visual Studio to open at the set file:line.
    /// </summary>
    [Conditional("DEBUG")]
    public static void Go(this Inv.Codepoint Codepoint)
    {
      if (Codepoint.IsSet)
      {
        var VisualStudio = new Inv.VisualStudioAutomation();
        VisualStudio.OpenFile(Codepoint.LabelFilePath, Codepoint.LabelLineNumber);
      }
    }
  }
}