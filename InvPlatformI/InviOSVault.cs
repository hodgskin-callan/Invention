﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Inv.Support;

namespace Inv
{
  internal class iOSVault
  {
    public iOSVault()
    {
      this.ServiceId = Foundation.NSBundle.MainBundle.BundleIdentifier ?? "Invention";
    }

    public IEnumerable<Secret> LoadSecrets(Inv.Vault Vault)
    {
      var Query = new Security.SecRecord(Security.SecKind.GenericPassword);
      Query.Service = ServiceId;

      var RecordArray = Security.SecKeyChain.QueryAsRecord(Query, 1000, out var QueryCode);

      return RecordArray != null ? RecordArray.Select(R =>
      {
        var Result = new Secret(Vault, R.Account);

        TranslateSecRecord(Result, R);

        return Result;
      }).ToList() : new List<Secret>();
    }
    public void Load(Secret Secret)
    {
      var SecRecord = GetSecRecord(Secret.Name);

      if (SecRecord != null)
        TranslateSecRecord(Secret, SecRecord);
      else
        Secret.Properties.Clear();
    }
    public void Save(Secret Secret)
    {
      var SerializedSecret = Secret.Serialize();
      var Data = Foundation.NSData.FromString(SerializedSecret, Foundation.NSStringEncoding.UTF8);

      // Remove any existing record.
      var ExistingRecord = GetSecRecord(Secret.Name);
      if (ExistingRecord != null)
      {
        var Query = new Security.SecRecord(Security.SecKind.GenericPassword);
        Query.Service = ServiceId;
        Query.Account = Secret.Name;

        var RemoveCode = Security.SecKeyChain.Remove(Query);
        if (RemoveCode != Security.SecStatusCode.Success)
          throw new Exception("Could not replace secret in keychain: " + RemoveCode);
      }

      // Add this record.
      var Record = new Security.SecRecord(Security.SecKind.GenericPassword);
      Record.Service = ServiceId;
      Record.Account = Secret.Name;
      Record.Generic = Data; // TODO: use .ValueData instead?
      Record.Accessible = Security.SecAccessible.Always;

      var AddCode = Security.SecKeyChain.Add(Record);
      if (AddCode != Security.SecStatusCode.Success)
        Debug.WriteLine("Could not save secret to keychain: " + AddCode); // NOTE: this is failing in the iOS simulator, not sure why.
    }
    public void Delete(Secret Secret)
    {
      var Query = new Security.SecRecord(Security.SecKind.GenericPassword);
      Query.Service = ServiceId;
      Query.Account = Secret.Name;

      var statusCode = Security.SecKeyChain.Remove(Query);

      if (statusCode != Security.SecStatusCode.Success)
        Debug.WriteLine("Could not delete secret from keychain: " + statusCode);
    }

    private Security.SecRecord GetSecRecord(string Name)
    {
      var Query = new Security.SecRecord(Security.SecKind.GenericPassword);
      Query.Service = ServiceId;
      Query.Account = Name;

      return Security.SecKeyChain.QueryAsRecord(Query, out var QueryCode);
    }
    private void TranslateSecRecord(Secret Secret, Security.SecRecord r)
    {
      var Data = Foundation.NSString.FromData(r.Generic, Foundation.NSStringEncoding.UTF8);

      Secret.Deserialize(Data);
    }

    private readonly string ServiceId;
  }
}
