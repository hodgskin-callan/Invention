﻿#if DEBUG
//#define FLOW_DEQUEUE         // TODO: the DequeueReusableCell techniques leads to missing cells when scrolling rapidly.
//#define PERFORMANCE
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using UIKit;
using CoreGraphics;
using Inv.Support;

namespace Inv
{
  public interface iOSElement
  {
    CGSize MeasureSize(CGSize MaxSize);
    UIView View { get; }
    iOSBorder Border { get; }
  }

  internal interface ITouchable
  {
  }

  public sealed class iOSBorder
  {
    internal iOSBorder(UIView View)
    {
      this.View = View;
    }

    public int TopLeftCornerRadius
    {
      get => CurrentState.TopLeftCornerRadius;
      set => CurrentState.TopLeftCornerRadius = value;
    }
    public int TopRightCornerRadius
    {
      get => CurrentState.TopRightCornerRadius;
      set => CurrentState.TopRightCornerRadius = value;
    }
    public int BottomRightCornerRadius
    {
      get => CurrentState.BottomRightCornerRadius;
      set => CurrentState.BottomRightCornerRadius = value;
    }
    public int BottomLeftCornerRadius
    {
      get => CurrentState.BottomLeftCornerRadius;
      set => CurrentState.BottomLeftCornerRadius = value;
    }

    public int LeftBorderThickness
    {
      get => CurrentState.LeftBorderThickness;
      set => CurrentState.LeftBorderThickness = value;
    }
    public int TopBorderThickness
    {
      get => CurrentState.TopBorderThickness;
      set => CurrentState.TopBorderThickness = value;
    }
    public int RightBorderThickness
    {
      get => CurrentState.RightBorderThickness;
      set => CurrentState.RightBorderThickness = value;
    }
    public int BottomBorderThickness
    {
      get => CurrentState.BottomBorderThickness;
      set => CurrentState.BottomBorderThickness = value;
    }

    public CGColor BorderColour
    {
      get => CurrentState.BorderColour;
      set => CurrentState.BorderColour = value;
    }

    public void Layout()
    {
      CurrentState.Width = View.Frame.Width;
      CurrentState.Height = View.Frame.Height;

      if ((ClipLayer == null && BorderLayer == null && (!CurrentState.IsCornerRadiusUniform || !CurrentState.IsBorderThicknessUniform)) || !CurrentState.Equals(PreviousState))
      {
        if (ClipLayer != null)
        {
          View.Layer.Mask = null;
          ClipLayer = null;
        }

        if (BorderLayer != null)
        {
          BorderLayer.RemoveFromSuperLayer();
          BorderLayer = null;
        }

        if (!CurrentState.IsCornerRadiusUniform || !CurrentState.IsBorderThicknessUniform)
        {
          PathSize.Width = (float)View.Frame.Size.Width;
          PathSize.Height = (float)View.Frame.Size.Height;

          PathCornerRadius.TopLeft = CurrentState.TopLeftCornerRadius;
          PathCornerRadius.TopRight = CurrentState.TopRightCornerRadius;
          PathCornerRadius.BottomRight = CurrentState.BottomRightCornerRadius;
          PathCornerRadius.BottomLeft = CurrentState.BottomLeftCornerRadius;

          PathBorderThickness.Left = CurrentState.LeftBorderThickness;
          PathBorderThickness.Top = CurrentState.TopBorderThickness;
          PathBorderThickness.Right = CurrentState.RightBorderThickness;
          PathBorderThickness.Bottom = CurrentState.BottomBorderThickness;

          RectPath.RemoveAllPoints();
          RectBuilder.AddRoundedRect(PathSize, PathCornerRadius, PathBorderThickness);

          // Clip the view to the shape of the outer border
          var ViewMaskShape = new CoreAnimation.CAShapeLayer();
          ViewMaskShape.Path = RectPath.CGPath;
          View.Layer.Mask = ViewMaskShape;

          this.ClipLayer = ViewMaskShape;

          if (CurrentState.IsBorderThicknessSet)
          {
            BorderPath.RemoveAllPoints();
            BorderBuilder.AddRoundedRectBorder(PathSize, PathCornerRadius, PathBorderThickness);

            var BorderShape = new CoreAnimation.CAShapeLayer();
            BorderShape.Path = BorderPath.CGPath;
            BorderShape.FillColor = BorderColour;

            View.Layer.AddSublayer(BorderShape);
            this.BorderLayer = BorderShape;
          }
        }

        PreviousState = CurrentState;
      }
    }

    private readonly UIView View;
    private CoreAnimation.CALayer BorderLayer;
    private CoreAnimation.CALayer ClipLayer;
    private State CurrentState;
    private State PreviousState;

    private struct State : IEquatable<State>
    {
      public int TopLeftCornerRadius { get; set; }
      public int TopRightCornerRadius { get; set; }
      public int BottomRightCornerRadius { get; set; }
      public int BottomLeftCornerRadius { get; set; }

      public int LeftBorderThickness { get; set; }
      public int TopBorderThickness { get; set; }
      public int RightBorderThickness { get; set; }
      public int BottomBorderThickness { get; set; }

      public CGColor BorderColour { get; set; }

      public nfloat Width { get; set; }
      public nfloat Height { get; set; }

      public readonly bool IsCornerRadiusSet => TopLeftCornerRadius != 0 || TopRightCornerRadius != 0 || BottomRightCornerRadius != 0 || BottomLeftCornerRadius != 0;
      public readonly bool IsBorderThicknessSet => LeftBorderThickness != 0 || TopBorderThickness != 0 || RightBorderThickness != 0 || BottomBorderThickness != 0;
      public readonly bool IsCornerRadiusUniform => TopLeftCornerRadius == TopRightCornerRadius && TopRightCornerRadius == BottomRightCornerRadius && BottomRightCornerRadius == BottomLeftCornerRadius;
      public readonly bool IsBorderThicknessUniform => LeftBorderThickness == TopBorderThickness && TopBorderThickness == RightBorderThickness && RightBorderThickness == BottomBorderThickness;

      public readonly bool Equals(State Other)
      {
        return
          TopLeftCornerRadius == Other.TopLeftCornerRadius && TopRightCornerRadius == Other.TopRightCornerRadius &&
          BottomRightCornerRadius == Other.BottomRightCornerRadius && BottomLeftCornerRadius == Other.BottomLeftCornerRadius &&
          LeftBorderThickness == Other.LeftBorderThickness && TopBorderThickness == Other.TopBorderThickness &&
          RightBorderThickness == Other.RightBorderThickness && BottomBorderThickness == Other.BottomBorderThickness &&
          BorderColour == Other.BorderColour && Width == Other.Width && Height == Other.Height;
      }
      public override bool Equals(object Other)
      {
        return Other is State State && Equals(State);
      }
      public override readonly int GetHashCode()
      {
        return Tuple.Create(
          Tuple.Create(TopLeftCornerRadius, TopRightCornerRadius, BottomRightCornerRadius, BottomLeftCornerRadius),
          Tuple.Create(LeftBorderThickness, TopBorderThickness, RightBorderThickness, BottomBorderThickness),
          BorderColour,
          Width,
          Height).GetHashCode();
      }
    }

    private static readonly UIKit.UIBezierPath BorderPath = new();
    private static readonly UIKit.UIBezierPath RectPath = new();
    private static Inv.BezierSize PathSize = new();
    private static Inv.BezierCornerRadius PathCornerRadius = new();
    private static Inv.BezierBorderThickness PathBorderThickness = new();
    private static readonly iOSBezierPathBuilder BorderBuilder = new(BorderPath);
    private static readonly iOSBezierPathBuilder RectBuilder = new(RectPath);
  }

  internal sealed class iOSBezierPathBuilder : Inv.BezierPathBuilder<UIBezierPath>
  {
    internal iOSBezierPathBuilder(UIBezierPath Path)
    {
      this.Path = Path;
      Path.RemoveAllPoints();
    }

    public override void MoveTo(Inv.BezierPoint Point)
    {
      Path.MoveTo(new CGPoint(Point.X, Point.Y));
    }
    public override void AddLineTo(Inv.BezierPoint Point)
    {
      Path.AddLineTo(new CGPoint(Point.X, Point.Y));
    }
    public override void AddCurveTo(Inv.BezierPoint EndPoint, Inv.BezierPoint ControlPoint1, Inv.BezierPoint ControlPoint2)
    {
      Path.AddCurveToPoint(new CGPoint(EndPoint.X, EndPoint.Y), new CGPoint(ControlPoint1.X, ControlPoint1.Y), new CGPoint(ControlPoint2.X, ControlPoint2.Y));
    }
    public override void Close()
    {
      Path.ClosePath();
    }
    public override UIBezierPath Render()
    {
      return Path;
    }

    private readonly UIBezierPath Path;
  }

  public sealed class iOSSurface : iOSView
  {
    internal iOSSurface()
      : base()
    {
      this.Border = new iOSBorder(this);

      // NOTE: don't need ClipsToBounds = true because the surface covers the full window.
    }

    public iOSBorder Border { get; }
    public iOSContainer Content { get; private set; }

    public void SetContent(iOSContainer Content)
    {
      if (this.Content != Content)
      {
        if (this.Content != null)
          this.SafeRemoveView(this.Content);

        this.Content = Content;

        if (this.Content != null)
          this.SafeAddView(this.Content);
      }
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      if (Content != null)
      {
        var iOSBounds = Bounds;

        // NOTE: iOS 13+ workaround where Bounds are corrupt after using a file picker controller.
        if (Bounds.Width <= 0 || Bounds.Height <= 0)
          iOSBounds = UIKit.UIScreen.MainScreen.Bounds;

        if (iOSFoundation.iOS11_0)
        {
          var iOSInsets = SafeAreaInsets;

          // NOTE: iOS 13+ workaround where SafeAreaInsets are corrupt after using a file picker controller.
          if (iOSInsets.Top > 0 && iOSInsets.Bottom <= 0)
            iOSInsets = UIKit.UIApplication.SharedApplication.KeyWindow.SafeAreaInsets;

          // NOTE: this code forces the surface content to be within the safe area (avoiding the notch and home button for iPhone X).
          iOSBounds = iOSInsets.InsetRect(iOSBounds);
        }

        Content.Frame = iOSBounds;
      }

      Border.Layout();

      SW.StopLayout();
    }

    private readonly iOSStopwatch SW = iOSInstrumentation.SurfacePerformance.NewStopwatch();
  }

  public abstract class iOSView : UIView
  {
    internal iOSView()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.DisableSafeAreaLayoutMargins();
    }

    /*public override void Dispose()
    {
      // Brute force, remove everything
      foreach (var view in Subviews)
        view.RemoveFromSuperview();
      base.Dispose();
    }*/

    public override UIView HitTest(CGPoint Point, UIEvent Event)
    {
      var Result = base.HitTest(Point, Event);

      if (Result == this && (BackgroundColor ?? UIColor.Clear) == UIColor.Clear)
        return this.HitTestFindNothing(Result);
      else
        return this.HitTestFindTouchable(Result);
    }
  }

  public sealed class iOSBrowser : iOSView, iOSElement
  {
    internal iOSBrowser()
      : base()
    {
      this.Border = new iOSBorder(this);

      // NOTE: we have to embed the webviews inside a parentview, because you cannot set the background color.
      //       the webviews are marked as transparent and the parentview background color is shown.

      this.BrowserView = new iOSWebkitBrowser();
      AddSubview(BrowserView);
      BrowserView.BlockQuery += (Url) => BlockQuery(Url);
      BrowserView.ReadyEvent += (Url) => ReadyEvent(Url);
    }

    public iOSBorder Border { get; }
    public Func<string, bool> BlockQuery;
    public event Action<string> ReadyEvent;

    public void Navigate(Uri Uri, string Html)
    {
      BrowserView.Navigate(Uri, Html);
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      BrowserView.Frame = LayoutMargins.InsetRect(Bounds);

      Border.Layout();

      SW.StopLayout();
    }

    internal bool IsTransitioning { get; set; }

    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var Result = BrowserView.MeasureSize(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      SW.StopMeasure();

      return Result;
    }
    UIView iOSElement.View => this;

    private readonly iOSWebkitBrowser BrowserView;
    private readonly iOSStopwatch SW = iOSInstrumentation.BrowserPerformance.NewStopwatch();
  }

  public sealed class iOSWebkitBrowser : WebKit.WKWebView
  {
    internal iOSWebkitBrowser()
      : base(CGRect.Empty, new WebKit.WKWebViewConfiguration())
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;

      this.NavigationDelegate = new iOSWebkitDelegate(this);

      // NOTE: this makes the browser truly transparent.
      this.Opaque = false;
      this.BackgroundColor = UIColor.Clear;
      this.ScrollView.BackgroundColor = UIColor.Clear;

      // TODO: causes SIGSEVs?
      //AddObserver("scrollView.contentSize", NSKeyValueObservingOptions.New, A =>
      //{
      //  var Size = (A.NewValue as NSValue).CGSizeValue;
      //  Debug.WriteLine("CS: " + Size.Width + " x " + Size.Height);
      //});
    }

    public event Func<string, bool> BlockQuery;
    public event Action<string> ReadyEvent;

    internal void Navigate(Uri Uri, string Html)
    {
      this.CalculatedHeight = 0;

      if (Html != null)
      {
        if (iOSFoundation.iOS9_0)
          LoadData(Foundation.NSData.FromString(Html, Foundation.NSStringEncoding.UTF8), "text/html", "UTF-8", new Foundation.NSUrl("")); // LoadData introduced iOS 9.0
        else
          LoadHtmlString(Html, new Foundation.NSUrl(""));
      }
      else if (Uri != null)
      {
        LoadRequest(new Foundation.NSUrlRequest(new Foundation.NSUrl(Uri.AbsoluteUri)));
      }
      else
      {
        LoadRequest(new Foundation.NSUrlRequest(new Foundation.NSUrl("about:blank")));
      }
    }
    internal CGSize MeasureSize(CGSize MaxSize)
    {
      var Result = SizeThatFits(this.FitToSize(MaxSize));

      if (CalculatedHeight > 0)
        Result.Height = CalculatedHeight;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private bool Block(Foundation.NSUrl Url)
    {
      return BlockQuery != null && BlockQuery(Url.ToString());
    }
    private void FinishNavigation(Foundation.NSUrl Url)
    {
      //var ContentSize = ScrollView.ContentSize;
      //Debug.WriteLine("SV: " + ContentSize.Width + " x " + ContentSize.Height);

      this.EvaluateJavaScript("document.body.offsetHeight;", (R, E) =>
      {
        // Check 'R' to make sure it's not null because this was found in the crash reports:
        //   System.NullReferenceException: Object reference not set to an instance of an object - [iOSWebkitBrowser <FinishNavigation>b__10_0(Foundation.NSObject R, Foundation.NSError E)]

        if (R == null || !int.TryParse(R.ToString(), out CalculatedHeight))
          CalculatedHeight = 0;
        Debug.WriteLine("JS: " + CalculatedHeight);

        this.Arrange();
      });

      if (Url != null)
        ReadyEvent?.Invoke(Url.ToString());
    }

    private int CalculatedHeight;

    private sealed class iOSWebkitDelegate : WebKit.WKNavigationDelegate
    {
      public iOSWebkitDelegate(iOSWebkitBrowser Browser)
      {
        this.Browser = Browser;
      }

      public override void DecidePolicy(WebKit.WKWebView webView, WebKit.WKNavigationAction navigationAction, Action<WebKit.WKNavigationActionPolicy> decisionHandler)
      {
        if (!string.Equals(navigationAction.Request.Url.AbsoluteString, "about:blank", StringComparison.InvariantCultureIgnoreCase) && Browser.Block(navigationAction.Request.Url))
          decisionHandler(WebKit.WKNavigationActionPolicy.Cancel);
        else
          decisionHandler(WebKit.WKNavigationActionPolicy.Allow);
      }
      public override void DidFinishNavigation(WebKit.WKWebView webView, WebKit.WKNavigation navigation)
      {
        Browser.FinishNavigation(webView.Url);
      }

      private readonly iOSWebkitBrowser Browser;
    }
  }

  public sealed class iOSFlow : UIScrollView, iOSElement
  {
    internal iOSFlow(iOSFlowConfiguration Configuration)
      : base()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.DisableSafeAreaLayoutMargins();
      this.Border = new iOSBorder(this);
      this.DirectionalLockEnabled = true;

      // removes indent on the left edge.
      if (iOSFoundation.iOS11_0)
        this.ContentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.Never;
      else
        this.ContentInset = UIEdgeInsets.Zero;

      this.TableView = new iOSTableView(Configuration);
      this.AddSubview(TableView);

      TableView.UpdateWidthEvent += () =>
      {
        if (TableView.MaxContentWidth > DefaultTableWidth)
          SetNeedsLayout();
      };
    }

    public iOSBorder Border { get; }
    public iOSContainer Fixture
    {
      get => FixtureField;
      set
      {
        if (FixtureField == value)
          return;

        if (FixtureField != null && Subviews.Contains(FixtureField))
          FixtureField.RemoveFromSuperview();

        this.FixtureField = value;

        if (FixtureField != null)
          AddSubview(FixtureField);

        SetNeedsLayout();
      }
    }

    internal iOSTableView TableView { get; }
    internal bool IsRefreshable
    {
      get => RefreshControlField != null;
      set
      {
        if (value != IsRefreshable)
        {
          var SupportsRefreshControl = iOSFoundation.iOS10_0;

          if (value)
          {
            this.RefreshControlField = new UIRefreshControl();
            RefreshControlField.ValueChanged += WeakHelpers.WeakEventHandlerWrapper(this, (Flow, Sender, Event) => Flow.TableView.RefreshAction());

            if (SupportsRefreshControl)
              RefreshControl = RefreshControlField;
            else
              AddSubview(RefreshControlField);

            ApplyShade();
          }
          else
          {
            if (SupportsRefreshControl)
              RefreshControl = null;
            else
              RefreshControlField.RemoveFromSuperview();

            this.RefreshControlField = null;
          }
        }
      }
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      var LayoutSize = TableView.MeasureSize(this.FitToSize(Bounds.Size));

      this.DefaultTableWidth = LayoutSize.Width;
      var TableWidth = DefaultTableWidth;

      if (TableView.MaxContentWidth > TableWidth)
        TableWidth = TableView.MaxContentWidth;

      var FixtureHeight = (nfloat)0;

      if (Fixture != null)
      {
        var FixtureSize = Fixture.GetContentSize(new CGSize(Bounds.Size.Width, Bounds.Size.Height - LayoutSize.Height));
        FixtureHeight = FixtureSize.Height;

        var YOffset = ContentOffset.Y;
        if (YOffset < 0)
          YOffset = 0;

        Fixture.Frame = new CGRect(LayoutMargins.Left, LayoutMargins.Top + YOffset, FixtureSize.Width, FixtureSize.Height);
      }

      TableView.Frame = new CGRect(LayoutMargins.Left, LayoutMargins.Top + FixtureHeight, TableWidth, Bounds.Height - LayoutMargins.Top - LayoutMargins.Bottom - FixtureHeight);

      this.ContentSize = new CGSize(TableWidth + LayoutMargins.Left + LayoutMargins.Right, FixtureHeight + LayoutSize.Height + LayoutMargins.Top + LayoutMargins.Bottom);

      Border.Layout();

      SW.StopLayout();
    }
    public override void MovedToSuperview()
    {
      base.MovedToSuperview();

      ApplyShade();
    }

    internal void NewRefresh()
    {
      if (RefreshControlField != null)
      {
        if (ContentOffset.Y <= 0f)
        {
          var X = ContentOffset.X;
          var StartY = ContentOffset.Y;
          var Y = -RefreshControlField.Frame.Size.Height;

          // HACK: This somehow fixes an issue with the refresh control not applying its TintColor on the first call of BeginRefreshing()
          ContentOffset = new CGPoint(X, Y);
          ContentOffset = new CGPoint(X, StartY);

          // Force the refresh control to be visible
          UIView.Animate(0.25, () => ContentOffset = new CGPoint(X, Y));
        }

        RefreshControlField.BeginRefreshing();
        TableView.RefreshAction();
      }
    }
    internal void CompleteRefresh()
    {
      RefreshControlField?.EndRefreshing();
    }
    internal void ScrollToRow(Foundation.NSIndexPath IndexPath, bool Animated)
    {
      var RowRect = TableView.RectForRowAtIndexPath(IndexPath);

      ScrollRectToVisible(RowRect, Animated);
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var Result = TableView.MeasureSize(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      SW.StopMeasure();

      return Result;
    }

    private void ApplyShade()
    {
      var OptimalShade = this.GetPreferredContentLightness();

      if (OptimalShade == 1f)
      {
        this.IndicatorStyle = UIScrollViewIndicatorStyle.White;
        if (RefreshControlField != null)
          RefreshControlField.TintColor = UIColor.White;
      }
      else
      {
        if (OptimalShade == 0f)
          this.IndicatorStyle = UIScrollViewIndicatorStyle.Black;
        else
          this.IndicatorStyle = UIScrollViewIndicatorStyle.Default;

        if (RefreshControlField != null)
          RefreshControlField.TintColor = null;
      }
    }

    private nfloat DefaultTableWidth;
    private UIRefreshControl RefreshControlField;
    private iOSContainer FixtureField;
    private readonly iOSStopwatch SW = iOSInstrumentation.FlowPerformance.NewStopwatch();
  }

  internal sealed class iOSTableView : UITableView
  {
    internal iOSTableView(iOSFlowConfiguration Configuration)
      : base(CGRect.Empty, UITableViewStyle.Grouped)
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.DisableSafeAreaLayoutMargins();
      this.BackgroundColor = UIKit.UIColor.Clear;
      this.ConfigurationField = Configuration;
      this.KnownHeightDictionary = new Dictionary<Foundation.NSIndexPath, nfloat>();
      this.KnownItemDictionary = new Dictionary<Foundation.NSIndexPath, iOSFlowItem>();
      this.AllowsSelection = false;
      this.DataSource = new iOSFlowDataSource(this);
      this.Delegate = new iOSFlowDelegate(this);
      this.SeparatorStyle = UITableViewCellSeparatorStyle.None;
      this.EstimatedRowHeight = UITableView.AutomaticDimension;
      this.EstimatedSectionHeaderHeight = UITableView.AutomaticDimension;
      this.EstimatedSectionFooterHeight = UITableView.AutomaticDimension;
      //this.ScrollEnabled = false;

      this.TableHeaderView = new UIView(new CGRect(0, 0, 0, 0.001));// HACK: Without a header view, there will be ~35 pixels of padding at the top of the table
      this.TableFooterView = new UIView(new CGRect(0, 0, 0, 0.001));// HACK: Without a footer view, there will be ~35 pixels of padding at the bottom of the table

      if (iOSFoundation.iOS9_0)
        this.CellLayoutMarginsFollowReadableWidth = false;

      RegisterClassForCellReuse(typeof(iOSFlowTile), iOSFlowTile.CellReuseIdentifier);
    }

    internal Action RefreshAction;
    internal event Action UpdateWidthEvent;

    public override void ReloadData()
    {
      KnownItemDictionary.Clear();
      KnownHeightDictionary.Clear();
      MaxContentWidth = 0;

      base.ReloadData();
    }

    internal Dictionary<Foundation.NSIndexPath, nfloat> KnownHeightDictionary { get; }
    internal Dictionary<Foundation.NSIndexPath, iOSFlowItem> KnownItemDictionary { get; }
    internal nfloat MaxContentWidth
    {
      get => MaxContentWidthField;
      set
      {
        MaxContentWidthField = value;
        UpdateWidthEvent?.Invoke();
      }
    }

    internal CGSize MeasureSize(CGSize MaxSize)
    {
      var Result = SizeThatFits(this.FitToSize(MaxSize));

      // TODO: why was this here, and the Height increment commented out?
      //if (MaxSize.Height == UIView.NoIntrinsicMetric)
      //  Result.Height = Window.Bounds.Height;
      //else
      //  Result.Height = MaxSize.Height;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private nfloat MaxContentWidthField;
    private readonly iOSFlowConfiguration ConfigurationField;

    private sealed class iOSFlowTile : UITableViewCell
    {
      internal iOSFlowTile()
      {
        Prepare();
      }
      internal iOSFlowTile(IntPtr Handle)
        : base(Handle)
      {
        Prepare();
      }

      internal Foundation.NSIndexPath IndexPath { get; private set; }

      internal void SetContent(iOSTableView Flow, Foundation.NSIndexPath IndexPath, iOSFlowItem ContentItem)
      {
        this.Flow = Flow;
        this.IndexPath = IndexPath;

        var OldContainer = this.ContentItem.Container;
        var NewContainer = ContentItem.Container;

        if (OldContainer != NewContainer)
        {
          if (OldContainer != null)
            ContentView.SafeRemoveView(OldContainer);

          this.ContentItem = ContentItem;

          if (NewContainer != null)
            ContentView.SafeAddView(NewContainer);

          this.Arrange();
        }
      }

      public override void LayoutSubviews()
      {
        base.LayoutSubviews();

        if (ContentItem.Container != null)
          ContentItem.Container.Frame = Bounds;
      }

      public override void PrepareForReuse()
      {
        base.PrepareForReuse();
#if FLOW_DEQUEUE
        Recycle();
#endif
      }
      public override void MovedToWindow()
      {
        base.MovedToWindow();

#if !FLOW_DEQUEUE
        if (Window == null) Recycle();
#endif
      }

      internal iOSFlowItem ContentItem { get; private set; }

      private void Recycle()
      {
        if (ContentItem.Container != null)
        {
          Flow.ConfigurationField.RecycleViewEvent(IndexPath.Section, IndexPath.Row, ContentItem);

          Flow.KnownItemDictionary.Remove(IndexPath);
        }
      }
      private void Prepare()
      {
        // NOTE: commented out because of this warning in the Debug window:
        //       Changing the translatesAutoresizingMaskIntoConstraints property of a UITableViewCell that is managed by a UITableView is not supported, and will result in incorrect self-sizing.
        //this.TranslatesAutoresizingMaskIntoConstraints = false;

        this.LayoutMargins = UIEdgeInsets.Zero;
        this.BackgroundColor = UIColor.Clear;
      }

      public const string CellReuseIdentifier = "tileGridCell";

      private iOSTableView Flow;
    }

    private sealed class iOSFlowDataSource : UITableViewDataSource
    {
      public iOSFlowDataSource(iOSTableView Flow)
      {
        this.Flow = Flow;
      }

      public override UITableViewCell GetCell(UITableView TableView, Foundation.NSIndexPath IndexPath)
      {
#if FLOW_DEQUEUE
        var Tile = TableView.DequeueReusableCell(iOSFlowTile.CellReuseIdentifier, IndexPath) as iOSFlowTile;
#else
        var Tile = new iOSFlowTile();
#endif

        var ContentItem = Flow.KnownItemDictionary.GetValueOrDefault(IndexPath);

        if (ContentItem.Container == null)
        {
          //Debug.WriteLine("QUERY - GetCell");
          ContentItem = Flow.ConfigurationField.GetCellContentQuery(IndexPath.Section, IndexPath.Row);
        }

        Tile.SetContent(Flow, IndexPath, ContentItem);

        if (ContentItem.Container != null)
        {
          var FittingSize = ContentItem.Container.GetContentSize(new CGSize(UILayoutFittingExpandedSize.Width, UILayoutFittingExpandedSize.Height));

          Flow.KnownHeightDictionary[IndexPath] = FittingSize.Height;

          Flow.KnownItemDictionary[IndexPath] = ContentItem;

          if (FittingSize.Width > Flow.MaxContentWidth)
            Flow.MaxContentWidth = FittingSize.Width;
        }
        else
        {
          Debug.WriteLine("Missing item");
        }

        return Tile;
      }
      public override nint NumberOfSections(UITableView TableView)
      {
        return Flow.ConfigurationField.NumberOfSectionsQuery();
      }
      public override nint RowsInSection(UITableView TableView, nint Section)
      {
        return Flow.ConfigurationField.RowsInSectionQuery((int)Section);
      }

      private readonly iOSTableView Flow;
    }

    private sealed class iOSFlowDelegate : UITableViewDelegate
    {
      public iOSFlowDelegate(iOSTableView Flow)
      {
        this.Flow = Flow;
      }

      public override nfloat EstimatedHeight(UITableView TableView, Foundation.NSIndexPath IndexPath)
      {
        if (Flow.KnownHeightDictionary.TryGetValue(IndexPath, out var Result))
          return Result;

        return UITableView.AutomaticDimension;
      }
      public override nfloat GetHeightForRow(UITableView TableView, Foundation.NSIndexPath IndexPath)
      {
        if (Flow.KnownHeightDictionary.TryGetValue(IndexPath, out var Result))
          return Result;

        var ContentItem = Flow.KnownItemDictionary.GetValueOrDefault(IndexPath);

        if (ContentItem.Container == null)
        {
          //Debug.WriteLine("QUERY - GetHeightForRow");
          ContentItem = Flow.ConfigurationField.GetCellContentQuery(IndexPath.Section, IndexPath.Row);
          Flow.KnownItemDictionary[IndexPath] = ContentItem;
        }

        if (ContentItem.Container == null)
          return 0.0F;

        var FittingSize = ContentItem.Container.GetContentSize(new CGSize(UILayoutFittingExpandedSize.Width, UILayoutFittingExpandedSize.Height));

        return FittingSize.Height;
      }
      public override UIView GetViewForHeader(UITableView TableView, nint Section)
      {
        return Flow.ConfigurationField.GetViewForHeaderQuery((int)Section).Container;
      }
      public override nfloat GetHeightForHeader(UITableView TableView, nint Section)
      {
        var ContentItem = Flow.ConfigurationField.GetViewForHeaderQuery((int)Section);
        if (ContentItem.Container == null)
          return 0.001f; // HACK: to prevent leading header gaps.

        var FittingSize = ContentItem.Container.GetContentSize(new CGSize(UILayoutFittingExpandedSize.Width, UILayoutFittingExpandedSize.Height));
        return FittingSize.Height;
      }
      public override UIView GetViewForFooter(UITableView TableView, nint Section)
      {
        return Flow.ConfigurationField.GetViewForFooterQuery((int)Section).Container;
      }
      public override nfloat GetHeightForFooter(UITableView TableView, nint Section)
      {
        var ContentItem = Flow.ConfigurationField.GetViewForFooterQuery((int)Section);
        if (ContentItem.Container == null)
          return 0.001f; // HACK: to prevent trailing footer gaps.

        var FittingSize = ContentItem.Container.GetContentSize(new CGSize(UILayoutFittingExpandedSize.Width, UILayoutFittingExpandedSize.Height));
        return FittingSize.Height;
      }

      private readonly iOSTableView Flow;
    }
  }

  internal sealed class iOSFlowConfiguration
  {
    public Func<int, int, iOSFlowItem> GetCellContentQuery;
    public Func<int> NumberOfSectionsQuery;
    public Func<int, int> RowsInSectionQuery;
    public Func<int, iOSFlowItem> GetViewForHeaderQuery;
    public Func<int, iOSFlowItem> GetViewForFooterQuery;
    public Action<int, int, iOSFlowItem> RecycleViewEvent;
  }

  internal struct iOSFlowItem
  {
    public Inv.Panel Panel;
    public iOSContainer Container;
  }

  public sealed class iOSButton : UIButton, iOSElement, ITouchable
  {
    internal iOSButton()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.DisableSafeAreaLayoutMargins();
      this.Border = new iOSBorder(this);
      this.ExclusiveTouch = true; // if this view starts tracking a touch, no other views in the window will receive these events. This prevents the user from pressing two buttons at the same time with multi-touch.

      this.SingleTapGestureRecognizer = new UIKit.UITapGestureRecognizer(R =>
      {
        iOSInstrumentation.Print();
        iOSInstrumentation.Clear();

        if (R.State == UIKit.UIGestureRecognizerState.Ended)
          SingleTapEvent?.Invoke();
      });
      AddGestureRecognizer(SingleTapGestureRecognizer);

      this.LongPressGestureRecognizer = new UIKit.UILongPressGestureRecognizer(R =>
      {
        if (R.State == UIKit.UIGestureRecognizerState.Began)
          LongPressEvent?.Invoke();
      });
      AddGestureRecognizer(LongPressGestureRecognizer);
    }

    public iOSBorder Border { get; }
    internal event Action PreviewTouchesBeganEvent;
    internal event Action PreviewTouchesEndedEvent;
    internal event Action PreviewTouchesCancelledEvent;
    internal event Action SingleTapEvent;
    internal event Action LongPressEvent;
    internal event Func<bool> IsAccessibleQuery;

    public override UIAccessibilityTrait AccessibilityTraits
    {
      get
      {
        return base.AccessibilityTraits;
      }
      set => base.AccessibilityTraits = value;
    }
    public override bool IsAccessibilityElement
    {
      get
      {
        // TODO: (iosbutton.AccessibilityLabel != null || iosbutton.Content != null) ?

        var Result = base.IsAccessibilityElement;

        if (Result && IsAccessibleQuery != null)
          Result = IsAccessibleQuery();

        return Result;
      }
      set => base.IsAccessibilityElement = value;
    }
    public override string AccessibilityLabel
    {
      get
      {
        var Result = base.AccessibilityLabel;

        if (Result == null && Content != null)
        {
          Result = "";

          void Recurse(UIView View)
          {
            foreach (var Subview in View.Subviews)
            {
              if (Subview is iOSLabel)
                Result += ((iOSLabel)Subview).Text + " ";
              else if (Subview.Subviews.Length > 0)
                Recurse(Subview);
            }
          }

          Recurse(Content);

          Result = Result.Trim();
        }

        return Result;
      }
      set => base.AccessibilityLabel = value;
    }

    public void SetContent(iOSContainer Content)
    {
      if (this.Content != Content)
      {
        if (this.Content != null)
          (this).SafeRemoveView(this.Content);

        this.Content = Content;

        if (this.Content != null)
          (this).SafeAddView(this.Content);

        (this).Arrange();
      }
    }

    public override void TouchesBegan(Foundation.NSSet touches, UIEvent evt)
    {
      base.TouchesBegan(touches, evt);

      PreviewTouchesBeganEvent?.Invoke();
    }
    public override void TouchesCancelled(Foundation.NSSet touches, UIEvent evt)
    {
      base.TouchesCancelled(touches, evt);

      PreviewTouchesCancelledEvent?.Invoke();
    }
    public override void TouchesEnded(Foundation.NSSet touches, UIEvent evt)
    {
      base.TouchesEnded(touches, evt);

      PreviewTouchesEndedEvent?.Invoke();
    }
    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      if (Content != null)
        Content.Frame = LayoutMargins.InsetRect(Bounds);

      Border.Layout();

      SW.StopLayout();
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var Result = Content != null ? Content.GetContentSize(this.FitToSize(MaxSize)) : CGSize.Empty;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      SW.StopMeasure();

      return Result;
    }

    private iOSContainer Content;
    private readonly UIKit.UITapGestureRecognizer SingleTapGestureRecognizer;
    private readonly UIKit.UILongPressGestureRecognizer LongPressGestureRecognizer;
    private readonly iOSStopwatch SW = iOSInstrumentation.ButtonPerformance.NewStopwatch();
  }

  public sealed class iOSSwitch : iOSView, iOSElement
  {
    internal iOSSwitch()
      : base()
    {
      this.Border = new iOSBorder(this);

      this.SwitchView = new UISwitch();
      AddSubview(SwitchView);
      SwitchView.TranslatesAutoresizingMaskIntoConstraints = false;
      SwitchView.LayoutMargins = UIEdgeInsets.Zero;
    }

    // TODO: OffTintColor is not properly supported. The Internet suggests this hack:
    //
    // SwitchView.TintColor = OffTintColor;
    // SwitchView.Layer.CornerRadius = SwitchView.Frame.Height / 2;
    // SwitchView.BackgroundColor = OffTintColor;
    //
    //public UIColor OffTintColor
    //{
    //  get => SwitchView.TintColor;
    //  set => SwitchView.TintColor = value;
    //}
    public UIColor OnTintColor
    {
      get => SwitchView.OnTintColor;
      set => SwitchView.OnTintColor = value;
    }
    public bool IsOn
    {
      get => SwitchView.On;
      set
      {
        if (SwitchView.On != value)
          SwitchView.On = value;
      }
    }
    public bool IsEnabled
    {
      get => SwitchView.Enabled;
      set => SwitchView.Enabled = value;
    }
    public iOSBorder Border { get; }
    public event EventHandler ValueChanged
    {
      add => SwitchView.ValueChanged += value;
      remove => SwitchView.ValueChanged -= value;
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      var SwitchWidth = SwitchView.Bounds.Size.Width; // 71 pt
      var SwitchHeight = SwitchView.Bounds.Size.Height; // 51 pt
      var FrameRect = LayoutMargins.InsetRect(Bounds);

      // Right-justifying for consistency with Android.
      var ContentWidth = FrameRect.Width - SwitchWidth;
      if (ContentWidth < 0)
        ContentWidth = 0;

      var ContentHeight = (FrameRect.Height - SwitchHeight) / 2;
      if (ContentHeight < 0)
        ContentHeight = 0;

      if (ContentWidth > 0 || ContentHeight > 0)
        SwitchView.Frame = new CGRect(ContentWidth, ContentHeight, SwitchWidth, SwitchHeight);
      else
        SwitchView.Frame = FrameRect;

      Border.Layout();

      SW.StopLayout();
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var Result = SwitchView.SizeThatFits(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      Result = new CGSize(Math.Ceiling(Result.Width), Math.Ceiling(Result.Height));

      SW.StopMeasure();

      return Result;
    }

    private readonly UISwitch SwitchView;
    private readonly iOSStopwatch SW = iOSInstrumentation.SwitchPerformance.NewStopwatch();
  }

  public sealed class iOSLabel : iOSView, iOSElement
  {
    internal iOSLabel()
      : base()
    {
      // NOTE: using a nested label because you can't have both a CornerRadius and a Shadow (CornerRadius requires ClipsToBounds = true or the background is drawn as a rectangle; but it clips off the shadow).
      this.LabelView = new UILabel();
      AddSubview(LabelView);
      LabelView.TranslatesAutoresizingMaskIntoConstraints = false;
      LabelView.LayoutMargins = UIEdgeInsets.Zero;
      LabelView.Font = iOSFoundation.DefaultSystemFont;
      LabelView.Text = "";

      this.Border = new iOSBorder(this);

      this.TextField = "";
    }

    public iOSBorder Border { get; }
    public bool Underline
    {
      get => UnderlineField;
      set
      {
        if (UnderlineField != value)
        {
          this.UnderlineField = value;
          UpdateText();
        }
      }
    }
    public bool Strikethrough
    {
      get => StrikethroughField;
      set
      {
        if (StrikethroughField != value)
        {
          this.StrikethroughField = value;
          UpdateText();
        }
      }
    }
    public string Text
    {
      get => TextField;
      set
      {
        var TextValue = value ?? "";

        if (TextField != TextValue)
        {
          this.TextField = TextValue;
          UpdateText();
        }
      }
    }
    public UIFont Font
    {
      get => LabelView.Font;
      set => LabelView.Font = value;
    }
    public UIColor TextColor
    {
      get => LabelView.TextColor;
      set => LabelView.TextColor = value;
    }
    public UITextAlignment TextAlignment
    {
      get => LabelView.TextAlignment;
      set => LabelView.TextAlignment = value;
    }
    public nint Lines
    {
      get => LabelView.Lines;
      set => LabelView.Lines = value;
    }
    public UILineBreakMode LineBreakMode
    {
      get => LabelView.LineBreakMode;
      set => LabelView.LineBreakMode = value;
    }
    public Func<bool> IsAccessibleQuery;
    public override bool IsAccessibilityElement
    {
      get
      {
        var Result = base.IsAccessibilityElement && (base.AccessibilityLabel != null || !string.IsNullOrEmpty(TextField));

        if (Result && IsAccessibleQuery != null)
          Result = IsAccessibleQuery();

        return Result;
      }
      set => base.IsAccessibilityElement = value;
    }
    public override UIEdgeInsets LayoutMargins
    {
      get => base.LayoutMargins;
      set => base.LayoutMargins = value;
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      LabelView.Frame = LayoutMargins.InsetRect(Bounds);

      Border.Layout();

      SW.StopLayout();
    }

    private void UpdateText()
    {
      if (Underline || Strikethrough)
        LabelView.AttributedText = new Foundation.NSAttributedString(TextField, underlineStyle: Underline ? Foundation.NSUnderlineStyle.Single : Foundation.NSUnderlineStyle.None, strikethroughStyle: Strikethrough ? Foundation.NSUnderlineStyle.Single : Foundation.NSUnderlineStyle.None);
      else
        LabelView.Text = TextField;

      this.Arrange();
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      CGSize Result;

      // NOTE: ensure 'empty' labels have a height of one line of text.
      if (string.IsNullOrEmpty(TextField))
        Result = new CGSize(0, RepresentativeNSString.StringSize(LabelView.Font).Height);
      else
        Result = LabelView.SizeThatFits(this.FitToSize(MaxSize));

      Result.Width = (nfloat)Math.Ceiling(Result.Width + LayoutMargins.Left + LayoutMargins.Right);
      Result.Height = (nfloat)Math.Ceiling(Result.Height + LayoutMargins.Top + LayoutMargins.Bottom);

      SW.StopMeasure();

      return Result;
    }

    private readonly UILabel LabelView;
    private string TextField;
    private bool UnderlineField;
    private bool StrikethroughField;
    private readonly iOSStopwatch SW = iOSInstrumentation.LabelPerformance.NewStopwatch();
    private static readonly Foundation.NSString RepresentativeNSString = new("Pp");
  }

  public sealed class iOSBlock : iOSView, iOSElement
  {
    internal iOSBlock()
      : base()
    {
      // NOTE: using a nested label because you can't have both a CornerRadius and a Shadow (CornerRadius requires ClipsToBounds = true or the background is drawn as a rectangle; but it clips off the shadow).
      this.LabelView = new UILabel();
      AddSubview(LabelView);
      LabelView.TranslatesAutoresizingMaskIntoConstraints = false;
      LabelView.LayoutMargins = UIEdgeInsets.Zero;
      LabelView.Font = iOSFoundation.DefaultSystemFont;

      this.Border = new iOSBorder(this);
    }

    public iOSBorder Border { get; }
    public UITextAlignment TextAlignment
    {
      get => LabelView.TextAlignment;
      set => LabelView.TextAlignment = value;
    }
    public nint Lines
    {
      get => LabelView.Lines;
      set => LabelView.Lines = value;
    }
    public UILineBreakMode LineBreakMode
    {
      get => LabelView.LineBreakMode;
      set => LabelView.LineBreakMode = value;
    }
    public Foundation.NSAttributedString AttributedText
    {
      get => LabelView.AttributedText;
      set
      {
        if (LabelView.AttributedText != value)
        {
          LabelView.AttributedText = value;
          Update();
        }
      }
    }
    public Func<bool> IsAccessibleQuery;
    public override bool IsAccessibilityElement
    {
      get
      {
        var Result = base.IsAccessibilityElement && (base.AccessibilityLabel != null || AttributedText != null);

        if (Result && IsAccessibleQuery != null)
          Result = IsAccessibleQuery();

        return Result;
      }
      set => base.IsAccessibilityElement = value;
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      LabelView.Frame = LayoutMargins.InsetRect(Bounds);

      Border.Layout();

      SW.StopLayout();
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var Result = LabelView.SizeThatFits(this.FitToSize(MaxSize));

      // HACK: ensure 'empty' labels have a height of one line of text.
      if (Result.Height == 0 && string.IsNullOrEmpty(LabelView.Text))
      {
        Result.Height = RepresentativeNSString.StringSize(LabelView.Font).Height;
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      Result = new CGSize(Math.Ceiling(Result.Width), Math.Ceiling(Result.Height));

      SW.StopMeasure();

      return Result;
    }

    private void Update()
    {
      this.Arrange();
    }

    private readonly UILabel LabelView;
    private static readonly Foundation.NSString RepresentativeNSString = new("Pp");
    private readonly iOSStopwatch SW = iOSInstrumentation.BlockPerformance.NewStopwatch();
  }

  public sealed class iOSGraphic : iOSView, iOSElement
  {
    internal iOSGraphic()
      : base()
    {
      //this.ClipsToBounds = true; // NOTE: this fixes the corner radius, but clips the elevation.

      this.ImageView = new UIImageView();
      AddSubview(ImageView);
      ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
      //ImageView.ClipsToBounds = true; // TODO: clip the image to the owning border shape.
      ImageView.TranslatesAutoresizingMaskIntoConstraints = false;
      ImageView.LayoutMargins = UIEdgeInsets.Zero;
      ImageView.DisableSafeAreaLayoutMargins();

      this.Border = new iOSBorder(this);
    }

    public UIViewContentMode ImageContentMode
    {
      get => ImageView.ContentMode;
      set
      {
        ImageView.ContentMode = value;
        this.ClipsToBounds = value == UIViewContentMode.Center;
      }
    }
    public UIImage Image
    {
      get => ImageView.Image;
      set => ImageView.Image = value;
    }
    public iOSBorder Border { get; }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      var LayoutFrame = LayoutMargins.InsetRect(Bounds);

      if (ImageContentMode == UIViewContentMode.Center)
        ImageView.Frame = new CGRect(new CGPoint((LayoutFrame.Size.Width - Image.Size.Width) / 2, (LayoutFrame.Size.Height - Image.Size.Height) / 2), Image.Size);
      else
        ImageView.Frame = LayoutFrame;

      Border.Layout();

      SW.StopLayout();
    }

    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var MaxResult = this.FitToSize(MaxSize);
      var Result = ImageView.SizeThatFits(MaxResult);

      var Image = ImageView.Image;

      if (ImageContentMode == UIViewContentMode.Center)
      {
        Result = Image.Size;
      }
      else if (Image != null && Image.Size.Width > 0 && Image.Size.Height > 0 && (MaxResult.Width != UIView.NoIntrinsicMetric || (MaxResult.Height != UIView.NoIntrinsicMetric)))
      {
        // NOTE: this scales the image to fit the available max size and maintains aspect ratio.
        //       this may not be expected when simply centering and image in the screen but is consistent with Wpf/Uwp.

        if (ImageContentMode == UIViewContentMode.ScaleAspectFill || ImageContentMode == UIViewContentMode.ScaleAspectFit)
        {
          var ImageAspect = Image.Size.Width / Image.Size.Height;

          if (ImageAspect > 0)
          {
            if (MaxResult.Width > 0 && MaxResult.Height > 0)
            {
              if (ImageAspect > MaxResult.Width / MaxResult.Height)
                Result = new CGSize(MaxResult.Width, MaxResult.Width / ImageAspect);
              else
                Result = new CGSize(MaxResult.Height * ImageAspect, MaxResult.Height);
            }
            else
            {
              if (MaxResult.Width > 0)
                Result = new CGSize(MaxResult.Width, MaxResult.Width / ImageAspect);
              else if (MaxResult.Height > 0)
                Result = new CGSize(MaxResult.Height * ImageAspect, MaxResult.Height);
            }
          }
        }
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      Result = new CGSize(Math.Ceiling(Result.Width), Math.Ceiling(Result.Height));

      SW.StopMeasure();

      return Result;
    }
    UIView iOSElement.View => this;

    private readonly UIImageView ImageView;
    private readonly iOSStopwatch SW = iOSInstrumentation.GraphicPerformance.NewStopwatch();
  }

  public sealed class iOSSearch : UISearchBar, iOSElement
  {
    internal iOSSearch()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.DisableSafeAreaLayoutMargins();
      this.SearchBarStyle = UISearchBarStyle.Prominent;
      this.Translucent = true; // needs to be true or the clear background colour is drawn as black. Previously set to false with a comment *hides weird semi-transparent rounded gray background*
      this.ShowsCancelButton = false;
      this.TextColor = UIColor.Black;
      this.TextFont = iOSFoundation.DefaultSystemFont;
      this.EnablesReturnKeyAutomatically = false; // search button is always enabled, even if the search text is empty.
      this.BackgroundColor = UIColor.Clear;
      this.BackgroundImage = new UIImage(); // fixes top and bottom thin black lines.
      this.Border = new iOSBorder(this);
      this.TintColor = UIColor.Clear;
      this.BarTintColor = UIColor.Clear;
      this.ShouldBeginEditing = (t) => !IsReadOnly;
      this.SearchButtonClicked += (Sender, Event) => (Sender as UIResponder)?.ResignFirstResponder();

      this.InputAccessoryView = new iOSDoneToolbar(this);
    }

    public bool IsReadOnly { get; set; }
    public UIFont TextFont { get; set; }
    public UIColor TextColor { get; set; }
    public iOSBorder Border { get; }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      FindFields();

      UpdateFields();

      if (TextField != null)
      {
        TextField.Frame = new CGRect(LayoutMargins.Left, LayoutMargins.Top, Frame.Size.Width - (LayoutMargins.Left + LayoutMargins.Right), Frame.Size.Height - (LayoutMargins.Top + LayoutMargins.Bottom));
        TextField.LeftViewMode = ContainerView != null ? UITextFieldViewMode.Always : UITextFieldViewMode.Never; // hide the magnifying glass.

        Border.Layout();
      }
    }
    public override void Draw(CGRect rect)
    {
      this.TintColor = BackgroundColor;
      this.BarTintColor = BackgroundColor;

      FindFields();

      UpdateFields();

      base.Draw(rect);
    }

    // TODO: how to handle the hardware escape key to clear the text?
    //public override UIKeyCommand[] KeyCommands
    //{
    //  get => new UIKeyCommand[] { UIKit.UIKeyCommand.Create(UIKit.UIKeyCommand.Escape, (UIKit.UIKeyModifierFlags)0, new ObjCRuntime.Selector("SearchKeyDown:")) };
    //}
    //[Foundation.Export("SearchKeyDown:")]
    //private void SearchKeyDown(UIKeyCommand Command)
    //{
    //  if (Command.Input == UIKit.UIKeyCommand.Escape)
    //    Text = "";
    //}

    private void UpdateFields()
    {
      if (TextField != null)
      {
        // NOTE: commented because they don't appear to do anything.
        //TextField.TranslatesAutoresizingMaskIntoConstraints = false;
        //TextField.DisableSafeAreaLayoutMargins();
        //TextField.LayoutMargins = UIEdgeInsets.Zero;

        TextField.Font = TextFont;
        TextField.TextColor = TextColor;
        TextField.BackgroundColor = BackgroundColor;
        TextField.TintColor = TextColor; // cursor colour.
      }
    }
    private void FindFields()
    {
      this.TextField = null;
      this.ImageView = null;
      this.ContainerView = null;

      if (Subviews.Length == 1)
      {
        var HolderView = Subviews[0];

        if (HolderView.Subviews.Length == 2)
        {
          this.ImageView = HolderView.Subviews[0] as UIImageView;
          this.TextField = HolderView.Subviews[1] as UITextField; // <iOS13

          if (TextField == null)
          {
            this.ContainerView = HolderView.Subviews[1];

            if (ImageView != null && ContainerView != null && ContainerView.Subviews.Length == 1)
              this.TextField = ContainerView.Subviews[0] as UITextField; // iOS13+
          }
        }
      }
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      FindFields();

      var Result = TextField != null ? TextField.SizeThatFits(this.FitToSize(MaxSize)) : SizeThatFits(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }

    private UITextField TextField;
    private UIImageView ImageView;
    private UIView ContainerView;
  }

  public sealed class iOSEdit : UITextField, iOSElement
  {
    internal iOSEdit()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.DisableSafeAreaLayoutMargins();
      this.Font = iOSFoundation.DefaultSystemFont;
      this.ShouldBeginEditing = (t) => !IsReadOnly;
      this.ShouldReturn = (t) =>
      {
        t.EndEditing(true);

        ReturnEvent?.Invoke();

        return true;
      };
      this.ShouldChangeCharacters = (t, r, s) =>
      {
        var View = Superview;
        while (View.Superview != null && !(View is UIScrollView))
          View = View.Superview;

        if (View is UIScrollView)
          (View as UIScrollView).ScrollRectToVisible(ConvertRectToView(Bounds, View), true);

        return true;
      };
      this.Border = new iOSBorder(this);
      this.InputAccessoryView = new iOSDoneToolbar(this);
    }

    public bool IsReadOnly { get; set; }
    public iOSBorder Border { get; }
    public bool Underline
    {
      get => UnderlineField;
      set
      {
        if (UnderlineField != value)
        {
          this.UnderlineField = value;
          Update(Text);
        }
      }
    }
    public bool Strikethrough
    {
      get => StrikethroughField;
      set
      {
        if (StrikethroughField != value)
        {
          this.StrikethroughField = value;
          Update(Text);
        }
      }
    }
    public override string Text
    {
      get => base.Text ?? "";
      set
      {
        if (base.Text != value)
          Update(value);
      }
    }
    public event Action ReturnEvent;

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      Border.Layout();

      SW.StopLayout();
    }
    // TextRect and EditingRect observe the left and right padding.
    public override CGRect TextRect(CGRect forBounds)
    {
      forBounds.X += LayoutMargins.Left;
      forBounds.Width -= LayoutMargins.Right;
      return forBounds;
    }
    public override CGRect EditingRect(CGRect forBounds)
    {
      forBounds.X += LayoutMargins.Left;
      forBounds.Width -= LayoutMargins.Right;
      return forBounds;
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var Result = SizeThatFits(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      SW.StopMeasure();

      return Result;
    }

    private void Update(string Text)
    {
      if (Underline || Strikethrough)
        base.AttributedText = new Foundation.NSAttributedString(Text, underlineStyle: Underline ? Foundation.NSUnderlineStyle.Single : Foundation.NSUnderlineStyle.None, strikethroughStyle: Strikethrough ? Foundation.NSUnderlineStyle.Single : Foundation.NSUnderlineStyle.None);
      else
        base.Text = Text;
      this.Arrange();
    }

    private bool UnderlineField;
    private bool StrikethroughField;
    private readonly iOSStopwatch SW = iOSInstrumentation.EditPerformance.NewStopwatch();
  }

  internal sealed class iOSDoneToolbar : UIKit.UIToolbar
  {
    internal iOSDoneToolbar(UIView View)
      : base(new CoreGraphics.CGRect(0, 0, 320, 50))
    {
      this.BarStyle = UIKit.UIBarStyle.Default;
      this.Items = new UIKit.UIBarButtonItem[]
      {
         new UIBarButtonItem(UIKit.UIBarButtonSystemItem.FlexibleSpace),
         new UIBarButtonItem("Done", UIKit.UIBarButtonItemStyle.Done, (Sender, Event) => View.ResignFirstResponder())
       };
      this.SizeToFit();
    }
  }

  public sealed class iOSMemo : UITextView, iOSElement
  {
    internal iOSMemo()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.DisableSafeAreaLayoutMargins();
      this.BackgroundColor = UIColor.Clear;
      this.ContentInset = UIEdgeInsets.Zero;
      this.TextContainer.LineFragmentPadding = 0;
      this.TextContainerInset = UIEdgeInsets.Zero;
      this.Font = iOSFoundation.DefaultSystemFont;
      this.Changed += WeakHelpers.WeakEventHandlerWrapper(this, (layoutMemo, Sender, Event) => layoutMemo.Arrange());
      this.ShouldBeginEditing = (t) => !IsReadOnly;
      this.Border = new iOSBorder(this);
      this.InputAccessoryView = new iOSDoneToolbar(this);
      //this.AlwaysBounceVertical = false;
      //this.AlwaysBounceHorizontal = false;
      //this.Bounces = false;
    }

    public bool Underline
    {
      get => UnderlineField;
      set
      {
        if (UnderlineField != value)
        {
          this.UnderlineField = value;
          Update(Text);
        }
      }
    }
    public bool Strikethrough
    {
      get => StrikethroughField;
      set
      {
        if (StrikethroughField != value)
        {
          this.StrikethroughField = value;
          Update(Text);
        }
      }
    }
    public override string Text
    {
      get => base.Text ?? "";
      set
      {
        if (base.Text != value)
          Update(value);
      }
    }
    public override Foundation.NSAttributedString AttributedText
    {
      get
      {
        return base.AttributedText;
      }
      set
      {
        base.AttributedText = value;
        this.Arrange();
      }
    }
    public override UIEdgeInsets LayoutMargins
    {
      get => base.TextContainerInset;
      set => base.TextContainerInset = value;
    }
    public iOSBorder Border { get; }

    internal bool IsReadOnly { get; set; }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      Border.Layout();

      SW.StopLayout();
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var Result = SizeThatFits(this.FitToSize(MaxSize));

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      Result = new CGSize(Math.Ceiling(Result.Width), Math.Ceiling(Result.Height));

      SW.StopMeasure();

      return Result;
    }

    private void Update(string Text)
    {
      if (Underline || Strikethrough)
        base.AttributedText = new Foundation.NSAttributedString(Text, underlineStyle: Underline ? Foundation.NSUnderlineStyle.Single : Foundation.NSUnderlineStyle.None, strikethroughStyle: Strikethrough ? Foundation.NSUnderlineStyle.Single : Foundation.NSUnderlineStyle.None);
      else
        base.Text = Text;
      this.Arrange();
    }

    private bool UnderlineField;
    private bool StrikethroughField;
    private readonly iOSStopwatch SW = iOSInstrumentation.MemoPerformance.NewStopwatch();
  }

  public sealed class iOSCanvas : UIView, IUIAccessibilityContainer, iOSElement, Inv.DrawContract
  {
    internal iOSCanvas(iOSEngine iOSEngine)
    {
      // accessibility containers cannot be accessibility elements.
      if (IsAccessibilityElement)
        IsAccessibilityElement = false;

      this.iOSEngine = iOSEngine;
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.DisableSafeAreaLayoutMargins();
      this.BackgroundColor = UIKit.UIColor.Clear;
      this.Border = new iOSBorder(this);

      //this.Layer.ShouldRasterize = true; // NOTE: does not seem to help achieve 60fps.
      this.Layer.DrawsAsynchronously = true; // NOTE: very much seems to help achieve 60fps!

      this.SingleTapGestureRecognizer = new UIKit.UITapGestureRecognizer(R =>
      {
        if (R.State == UIKit.UIGestureRecognizerState.Ended)
        {
          var SinglePoint = R.LocationInView(this);

          SingleTapAction(SinglePoint);

          TouchUpAction(SinglePoint);
        }
      });
      AddGestureRecognizer(SingleTapGestureRecognizer);

      this.DoubleTapGestureRecognizer = new UIKit.UITapGestureRecognizer(R =>
      {
        if (R.State == UIKit.UIGestureRecognizerState.Ended)
        {
          var DoublePoint = R.LocationInView(this);

          DoubleTapAction(DoublePoint);

          TouchUpAction(DoublePoint);
        }
      })
      { NumberOfTapsRequired = 2 };
      AddGestureRecognizer(DoubleTapGestureRecognizer);

      this.LongPressGestureRecognizer = new UIKit.UILongPressGestureRecognizer(R =>
      {
        if (R.State == UIKit.UIGestureRecognizerState.Ended)
        {
          var LongPoint = R.LocationInView(this);

          LongPressAction(LongPoint);

          TouchUpAction(LongPoint);
        }
      });
      AddGestureRecognizer(LongPressGestureRecognizer);

      this.PinchGestureRecognizer = new UIKit.UIPinchGestureRecognizer(R =>
      {
        // NOTE: R.Scale is percentage increase/decrease eg 120% (1.20) or 20% (0.20)
        if (R.NumberOfTouches < 2)
        {
          R.Enabled = false;
          R.Enabled = true;

          TouchUpAction(R.LocationInView(this));
        }
        else
        {
          if (R.State == UIKit.UIGestureRecognizerState.Changed || R.State == UIKit.UIGestureRecognizerState.Began)
            ZoomAction(R.LocationInView(this), R.Scale > 1.0F ? +1 : -1);

          if (R.State == UIKit.UIGestureRecognizerState.Ended || R.State == UIKit.UIGestureRecognizerState.Cancelled)
            TouchUpAction(R.LocationInView(this));
        }
      });
      AddGestureRecognizer(PinchGestureRecognizer);

      this.iOSEllipseRect = new CoreGraphics.CGRect();
      this.iOSRectangleRect = new CoreGraphics.CGRect();
      this.iOSTextPoint = new CoreGraphics.CGPoint();
      this.iOSImageRect = new CoreGraphics.CGRect();
    }

    public iOSBorder Border { get; }
    internal Action DrawAction;
    internal Func<CGSize, CGSize?> MeasureFunc;
    internal Action<CoreGraphics.CGPoint> TouchDownAction;
    internal Action<CoreGraphics.CGPoint> TouchUpAction;
    internal Action<CoreGraphics.CGPoint> TouchMoveAction;
    internal Action<CoreGraphics.CGPoint> SingleTapAction;
    internal Action<CoreGraphics.CGPoint> DoubleTapAction;
    internal Action<CoreGraphics.CGPoint> LongPressAction;
    internal Action<CoreGraphics.CGPoint, int> ZoomAction;
    internal Func<UIAccessibilityElement[]> AccessibilityQuery;

    public override void SetNeedsDisplay()
    {
      this.iOSLastQueryArray = null; // must re-query for accessibility elements.

      base.SetNeedsDisplay();
    }
    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      Border.Layout();

      SW.StopLayout();
    }
    public override void Draw(CoreGraphics.CGRect rect)
    {
      base.Draw(rect);

      this.iOSContext = UIGraphics.GetCurrentContext();
      DrawAction();
    }

    public override void TouchesBegan(Foundation.NSSet touches, UIEvent evt)
    {
      base.TouchesBegan(touches, evt);

      var Touch = touches.AnyObject as UITouch;

      if (Touch != null)
        TouchDownAction(Touch.LocationInView(this));
    }
    public override void TouchesMoved(Foundation.NSSet touches, UIEvent evt)
    {
      base.TouchesMoved(touches, evt);

      var Touch = touches.AnyObject as UITouch;

      if (Touch != null)
        TouchMoveAction(Touch.LocationInView(this));
    }
    public override void TouchesEnded(Foundation.NSSet touches, UIEvent evt)
    {
      base.TouchesEnded(touches, evt);

      var Touch = touches.AnyObject as UITouch;

      if (Touch != null)
        TouchUpAction(Touch.LocationInView(this));
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var Constraint = this.FitToSize(MaxSize);

      var Dimension = MeasureFunc(Constraint);

      var Result = Dimension == null ? SizeThatFits(Constraint) : new CGSize(Dimension.Value.Width, Dimension.Value.Height);

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      SW.StopMeasure();

      return Result;
    }

    void DrawContract.Mask(Inv.Rect ViewportRect, Action<Inv.DrawMask> MaskAction)
    {
      // TODO: implement.
    }
    void DrawContract.Unmask()
    {
      // TODO: implement.
    }
    void DrawContract.DrawText(string TextFragment, Inv.DrawFont TextFont, Inv.Point TextPoint, Inv.HorizontalPosition TextHorizontal, Inv.VerticalPosition TextVertical, int MaximumTextWidth, int MaximumTextHeight)
    {
      using (var AttributedString = iOSEngine.NewAttributedString(TextFragment, TextFont))
      {
        var TextRect = AttributedString.GetBoundingRect(new CoreGraphics.CGSize(MaximumTextWidth, MaximumTextHeight), Foundation.NSStringDrawingOptions.OneShot, null);
        var TextPointX = TextPoint.X;
        var TextPointY = TextPoint.Y;

        if (TextHorizontal != HorizontalPosition.Left)
        {
          var TextWidth = (int)TextRect.Width;

          if (TextHorizontal == HorizontalPosition.Right)
            TextPointX -= TextWidth;
          else if (TextHorizontal == HorizontalPosition.Center)
            TextPointX -= TextWidth / 2;
        }

        if (TextVertical != VerticalPosition.Top)
        {
          var TextHeight = (int)TextRect.Height;

          if (TextVertical == VerticalPosition.Bottom)
            TextPointY -= TextHeight;
          else if (TextVertical == VerticalPosition.Center)
            TextPointY -= TextHeight / 2;
        }

        iOSTextPoint.X = TextPointX;
        iOSTextPoint.Y = TextPointY;

        AttributedString.DrawString(iOSTextPoint);
      }
    }
    void DrawContract.DrawLine(Inv.Colour LineStrokeColour, int LineStrokeThickness, Inv.LineJoin LineJoin, Inv.LineCap LineCap, Inv.Point LineSourcePoint, Inv.Point LineTargetPoint, params Inv.Point[] LineExtraPointArray)
    {
      if (LineStrokeThickness <= 0 || LineStrokeColour == Inv.Colour.Transparent)
        return;

      var iOSPointArray = new CGPoint[(LineExtraPointArray.Length * 2) + 2];
      iOSPointArray[0] = iOSEngine.TranslateCGPoint(LineSourcePoint);
      iOSPointArray[1] = iOSEngine.TranslateCGPoint(LineTargetPoint);

      var CurrentPoint = iOSPointArray[1];
      var iOSPointIndex = 2;
      for (var Index = 0; Index < LineExtraPointArray.Length; Index++)
      {
        var TargetPoint = iOSEngine.TranslateCGPoint(LineExtraPointArray[Index]);

        iOSPointArray[iOSPointIndex++] = CurrentPoint;
        iOSPointArray[iOSPointIndex++] = TargetPoint;

        CurrentPoint = TargetPoint;
      }

      iOSContext.SetStrokeColor(iOSEngine.TranslateCGColor(LineStrokeColour));
      iOSContext.SetLineWidth(LineStrokeThickness);
      iOSContext.SetLineCap(TranslateCGLineCap(LineCap));
      iOSContext.SetLineJoin(TranslateCGLineJoin(LineJoin));
      iOSContext.StrokeLineSegments(iOSPointArray);
    }
    void DrawContract.DrawRectangle(Inv.Colour RectangleFillColour, Inv.Colour RectangleStrokeColour, int RectangleStrokeThickness, Inv.Rect RectangleRect)
    {
      iOSRectangleRect.X = RectangleRect.Left;
      iOSRectangleRect.Y = RectangleRect.Top;
      iOSRectangleRect.Width = RectangleRect.Width;
      iOSRectangleRect.Height = RectangleRect.Height;

      if (RectangleFillColour != null)
      {
        iOSContext.SetFillColor(iOSEngine.TranslateCGColor(RectangleFillColour));
        iOSContext.FillRect(iOSRectangleRect);
      }

      if (RectangleStrokeColour != null && RectangleStrokeThickness > 0)
      {
        var HalfThickness = RectangleStrokeThickness / 2.0F;
        iOSRectangleRect.X += HalfThickness;
        iOSRectangleRect.Y += HalfThickness;

        if (iOSRectangleRect.Width > RectangleStrokeThickness)
          iOSRectangleRect.Width -= RectangleStrokeThickness;

        if (iOSRectangleRect.Height > RectangleStrokeThickness)
          iOSRectangleRect.Height -= RectangleStrokeThickness;

        iOSContext.SetStrokeColor(iOSEngine.TranslateCGColor(RectangleStrokeColour));
        iOSContext.SetLineWidth(RectangleStrokeThickness);
        iOSContext.StrokeRect(iOSRectangleRect);
      }
    }
    void DrawContract.DrawArc(Inv.Colour ArcFillColour, Inv.Colour ArcStrokeColour, int ArcStrokeThickness, Inv.Point ArcCenter, Inv.Point ArcRadius, float StartAngle, float SweepAngle)
    {
      var MaxWidth = Math.Max(0.0F, (ArcRadius.X * 2) - ArcStrokeThickness);
      var MaxHeight = Math.Max(0.0F, (ArcRadius.Y * 2) - ArcStrokeThickness);

      var StartAngleRad = SanitisedDegreesToRadians(360.0F - StartAngle);
      var SweepAngleRad = SanitisedDegreesToRadians(360.0F - SweepAngle);

      iOSContext.BeginPath();

      iOSContext.MoveTo(ArcCenter.X, ArcCenter.Y);
      iOSContext.AddArc(ArcCenter.X, ArcCenter.Y, Math.Max(MaxWidth / 2, MaxHeight / 2), StartAngleRad, SweepAngleRad, clockwise: true);

      iOSContext.ClosePath();

      iOSContext.SetFillColor(iOSEngine.TranslateCGColor(ArcFillColour));
      iOSContext.SetStrokeColor(iOSEngine.TranslateCGColor(ArcStrokeColour));
      iOSContext.SetLineWidth(ArcStrokeThickness);

      iOSContext.DrawPath(CGPathDrawingMode.FillStroke);
    }
    void DrawContract.DrawEllipse(Inv.Colour EllipseFillColour, Inv.Colour EllipseStrokeColour, int EllipseStrokeThickness, Inv.Point EllipseCenter, Inv.Point EllipseRadius)
    {
      iOSEllipseRect.X = EllipseCenter.X - EllipseRadius.X;
      iOSEllipseRect.Y = EllipseCenter.Y - EllipseRadius.Y;
      iOSEllipseRect.Width = EllipseRadius.X * 2;
      iOSEllipseRect.Height = EllipseRadius.Y * 2;

      if (EllipseFillColour != null)
      {
        iOSContext.SetFillColor(iOSEngine.TranslateCGColor(EllipseFillColour));
        iOSContext.FillEllipseInRect(iOSEllipseRect);
      }

      if (EllipseStrokeColour != null && EllipseStrokeThickness > 0)
      {
        iOSContext.SetStrokeColor(iOSEngine.TranslateCGColor(EllipseStrokeColour));
        iOSContext.SetLineWidth(EllipseStrokeThickness);
        iOSContext.StrokeEllipseInRect(iOSEllipseRect);
      }
    }
    void DrawContract.DrawImage(Inv.Image ImageSource, Inv.Rect ImageRect, float ImageOpacity, Colour ImageTint, Mirror? ImageMirror, Inv.Rotation? ImageRotation)
    {
      var iOSImageSource = iOSEngine.TranslateCGImage(ImageSource);

      iOSContext.SaveState();
      try
      {
        if (ImageRotation != null)
        {
          var ImageAngle = ImageRotation.Value.Angle;
          var ImagePivot = ImageRotation.Value.LocatePoint(ImageRect);

          iOSImageRect.X = 0;
          iOSImageRect.Y = 0;
          iOSImageRect.Width = ImageRect.Width;
          iOSImageRect.Height = ImageRect.Height;

          iOSContext.TranslateCTM(ImagePivot.X, ImagePivot.Y);
          iOSContext.RotateCTM((float)(Math.PI * ImageAngle / 180.0));

          if (ImageMirror == Inv.Mirror.Horizontal)
            iOSContext.ScaleCTM(-1.0F, -1.0F);
          else if (ImageMirror == Inv.Mirror.HorizontalAndVertical)
            iOSContext.ScaleCTM(-1.0F, +1.0F);
          else if (ImageMirror != Inv.Mirror.Vertical)
            iOSContext.ScaleCTM(+1.0F, -1.0F);

          iOSContext.TranslateCTM(ImageRect.Width * -0.5F, ImageRect.Height * -0.5F);
        }
        else if (ImageMirror == null)
        {
          var BoundsHeight = Bounds.Height;

          iOSImageRect.X = ImageRect.Left;
          iOSImageRect.Y = BoundsHeight - ImageRect.Top - ImageRect.Height;
          iOSImageRect.Width = ImageRect.Width;
          iOSImageRect.Height = ImageRect.Height;

          iOSContext.TranslateCTM(0, BoundsHeight);
          iOSContext.ScaleCTM(+1.0F, -1.0F);
        }
        else if (ImageMirror.Value == Mirror.Horizontal)
        {
          var BoundsWidth = Bounds.Width;
          var BoundsHeight = Bounds.Height;

          iOSImageRect.X = BoundsWidth - ImageRect.Left - ImageRect.Width;
          iOSImageRect.Y = BoundsHeight - ImageRect.Top - ImageRect.Height;
          iOSImageRect.Width = ImageRect.Width;
          iOSImageRect.Height = ImageRect.Height;

          iOSContext.TranslateCTM(BoundsWidth, BoundsHeight);
          iOSContext.ScaleCTM(-1.0F, -1.0F);
        }
        else if (ImageMirror.Value == Mirror.Vertical)
        {
          iOSImageRect.X = ImageRect.Left;
          iOSImageRect.Y = ImageRect.Top;
          iOSImageRect.Width = ImageRect.Width;
          iOSImageRect.Height = ImageRect.Height;
        }

        if (ImageOpacity != 1.0F)
          iOSContext.SetAlpha(ImageOpacity);

        iOSContext.DrawImage(iOSImageRect, iOSImageSource);

        // TODO: use of image tint severely impacts on FPS.
        if (ImageTint != null)
        {
          iOSContext.ClipToMask(iOSImageRect, iOSImageSource);
          iOSContext.SetFillColor(iOSEngine.TranslateCGColor(ImageTint));
          iOSContext.FillRect(iOSImageRect);
        }
      }
      finally
      {
        iOSContext.RestoreState();
      }
    }
    void DrawContract.DrawPolygon(Inv.Colour FillColour, Inv.Colour StrokeColour, int StrokeThickness, Inv.LineJoin LineJoin, bool IsClosed, Inv.Point StartPoint, params Inv.Point[] PointArray)
    {
      iOSContext.BeginPath();

      iOSContext.MoveTo(StartPoint.X, StartPoint.Y);
      foreach (var Point in PointArray)
        iOSContext.AddLineToPoint(Point.X, Point.Y);

      if (IsClosed)
        iOSContext.ClosePath();

      iOSContext.SetFillColor(iOSEngine.TranslateCGColor(FillColour));
      iOSContext.SetStrokeColor(iOSEngine.TranslateCGColor(StrokeColour));
      iOSContext.SetLineWidth(StrokeThickness);
      iOSContext.SetLineJoin(TranslateCGLineJoin(LineJoin));

      iOSContext.DrawPath(CGPathDrawingMode.FillStroke);
    }

    [Foundation.Export("accessibilityElementCount")]
    private nint AccessibilityElementCount()
    {
      return Query().Length;
    }
    [Foundation.Export("accessibilityElementAtIndex:")]
    private Foundation.NSObject GetAccessibilityElementAt(nint Index)
    {
      var Array = Query();

      if (Index >= 0 && Index < Array.Length)
        return Array[(int)Index];
      else
        return null;
    }
    [Foundation.Export("indexOfAccessibilityElement:")]
    private nint GetIndexOfAccessibilityElement(Foundation.NSObject Element)
    {
      var Array = Query();

      var FindElement = Element as UIAccessibilityElement;

      if (FindElement != null)
      {
        for (var Index = 0; Index < Array.Length; Index++)
        {
          var ScanElement = Array[Index];

          if (ScanElement == FindElement || (ScanElement.AccessibilityFrame == FindElement.AccessibilityFrame && ScanElement.AccessibilityLabel == FindElement.AccessibilityLabel))
            return Index;
        }

        return 0;
      }

      return -1;
    }
    [Foundation.Export("accessibilityElements")]
    private Foundation.NSObject GetAccessibilityElements()
    {
      return Foundation.NSObject.FromObject(Query());
    }
    [Foundation.Export("setAccessibilityElements:")]
    private void SetAccessibilityElements(Foundation.NSObject Elements)
    {
      this.iOSLastQueryArray = null;
    }
    [Foundation.Export("accessibilityContainerType")]
    private UIAccessibilityContainerType AccessibilityContainerType => UIAccessibilityContainerType.None;

    private UIAccessibilityElement[] Query()
    {
      if (iOSLastQueryArray == null)
        this.iOSLastQueryArray = AccessibilityQuery?.Invoke() ?? Array.Empty<UIAccessibilityElement>();

      return iOSLastQueryArray;
    }
    private float SanitisedDegreesToRadians(float Angle)
    {
      if (Angle < 0)
        Angle = ((Angle % 360) + 360) % 360;
      else if (Angle >= 360)
        Angle %= 360;

      return iOSFoundation.DegreesToRadians(Angle);
    }
    private CGLineJoin TranslateCGLineJoin(Inv.LineJoin InvLineJoin)
    {
      switch (InvLineJoin)
      {
        case Inv.LineJoin.Miter:
          return CGLineJoin.Miter;

        case Inv.LineJoin.Bevel:
          return CGLineJoin.Bevel;

        case Inv.LineJoin.Round:
          return CGLineJoin.Round;

        default:
          throw EnumHelper.UnexpectedValueException(InvLineJoin);
      }
    }
    private CGLineCap TranslateCGLineCap(Inv.LineCap InvLineCap)
    {
      switch (InvLineCap)
      {
        case Inv.LineCap.Butt:
          return CGLineCap.Butt;

        case Inv.LineCap.Round:
          return CGLineCap.Round;

        case Inv.LineCap.Square:
          return CGLineCap.Square;

        default:
          throw EnumHelper.UnexpectedValueException(InvLineCap);
      }
    }
    private readonly UIKit.UIPinchGestureRecognizer PinchGestureRecognizer;
    private readonly UIKit.UILongPressGestureRecognizer LongPressGestureRecognizer;
    private readonly UIKit.UITapGestureRecognizer SingleTapGestureRecognizer;
    private readonly UIKit.UITapGestureRecognizer DoubleTapGestureRecognizer;
    private CGContext iOSContext;
    private CGRect iOSEllipseRect;
    private CGRect iOSRectangleRect;
    private CGPoint iOSTextPoint;
    private CGRect iOSImageRect;
    private readonly iOSEngine iOSEngine;
    private UIAccessibilityElement[] iOSLastQueryArray;
    private readonly iOSStopwatch SW = iOSInstrumentation.CanvasPerformance.NewStopwatch();
  }

  public enum iOSHorizontal
  {
    Stretch,
    Center,
    Left,
    Right
  }

  public enum iOSVertical
  {
    Stretch,
    Center,
    Top,
    Bottom
  }

  public sealed class iOSContainer : UIView
  {
    public iOSContainer(Inv.Control ReferenceControl)
    {
      this.ReferenceControl = ReferenceControl;
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.DisableSafeAreaLayoutMargins();
      this.ContentWidth = ContentNaNValue;
      this.ContentHeight = ContentNaNValue;
      this.ContentMinimumWidth = ContentNaNValue;
      this.ContentMinimumHeight = ContentNaNValue;
      this.ContentMaximumWidth = ContentNaNValue;
      this.ContentMaximumHeight = ContentNaNValue;
      this.ContentVisible = true;
      this.Border = new iOSBorder(this);
    }

    public iOSBorder Border { get; }
    public iOSElement ContentElement { get; private set; }
    public event Action<Inv.Control> SizeChanged;

    public void SetContentVisibility(bool Visible)
    {
      if (Visible != this.ContentVisible)
      {
        this.ContentVisible = Visible;

        if (Hidden == Visible)
        {
          this.Hidden = !Visible;

          void RecurseElements(UIKit.UIView View)
          {
            if (Visible)
            {
              // NOTE: this is necessary so the iOSSearch will redraw properly (there's a problem if the search edit is created as hidden and then shown).
              if (View.GetType() == typeof(iOSSearch))
                View.SetNeedsDisplay();
            }
            else
            {
              // NOTE: hide the soft-keyboard when the view is hidden.
              if (View.IsFirstResponder)
                View.ResignFirstResponder();
            }

            foreach (var Child in View.Subviews)
              RecurseElements(Child);
          }

          if (ContentElement != null)
            RecurseElements(ContentElement.View);
        }

        this.Arrange();
      }
    }
    public void SetContentAlignment(iOSVertical Vertical, iOSHorizontal Horizontal)
    {
      if (Vertical != this.ContentVertical || Horizontal != this.ContentHorizontal)
      {
        this.ContentVertical = Vertical;
        this.ContentHorizontal = Horizontal;

        this.Arrange();
      }
    }
    public void SetContentElement(iOSElement Content)
    {
      if (Content != this.ContentElement)
      {
        if (this.ContentElement != null)
          this.SafeRemoveView(this.ContentElement.View);

        this.ContentElement = Content;

        if (this.ContentElement != null)
          this.SafeAddView(this.ContentElement.View);

        this.Arrange();
      }
    }
    public void SetContentWidth(int? Width)
    {
      var WidthValue = Width ?? ContentNaNValue;
      if (WidthValue != this.ContentWidth)
      {
        this.ContentWidth = WidthValue;

        this.Arrange();
      }
    }
    public void SetContentHeight(int? Height)
    {
      var HeightValue = Height ?? ContentNaNValue;
      if (HeightValue != this.ContentHeight)
      {
        this.ContentHeight = HeightValue;

        this.Arrange();
      }
    }
    public void SetContentMinimumWidth(int? MinimumWidth)
    {
      var MinimumWidthValue = MinimumWidth ?? ContentNaNValue;
      if (MinimumWidthValue != this.ContentMinimumWidth)
      {
        this.ContentMinimumWidth = MinimumWidthValue;

        this.Arrange();
      }
    }
    public void SetContentMaximumWidth(int? MaximumWidth)
    {
      var MaximumWidthValue = MaximumWidth ?? ContentNaNValue;
      if (MaximumWidthValue != this.ContentMaximumWidth)
      {
        this.ContentMaximumWidth = MaximumWidthValue;

        this.Arrange();
      }
    }
    public void SetContentMinimumHeight(int? MinimumHeight)
    {
      var MinimumHeightValue = MinimumHeight ?? ContentNaNValue;
      if (MinimumHeightValue != this.ContentMinimumHeight)
      {
        this.ContentMinimumHeight = MinimumHeightValue;

        this.Arrange();
      }
    }
    public void SetContentMaximumHeight(int? MaximumHeight)
    {
      var MaximumHeightValue = MaximumHeight ?? ContentNaNValue;
      if (MaximumHeightValue != this.ContentMaximumHeight)
      {
        this.ContentMaximumHeight = MaximumHeightValue;

        this.Arrange();
      }
    }

    public override void SetNeedsLayout()
    {
      base.SetNeedsLayout();
      this.LastContentCalculated = false;
    }
    public override UIView HitTest(CGPoint Point, UIEvent Event)
    {
      return this.HitTestFindNothing(base.HitTest(Point, Event));
    }
    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      if (ContentElement != null)
      {
        var Element = ContentElement.View;

        if (!ContentVisible)
        {
          Element.Frame = CGRect.Empty;
        }
        else
        {
          var OuterSize = Bounds.Size;
          OuterSize.Width -= LayoutMargins.Left + LayoutMargins.Right;
          OuterSize.Height -= LayoutMargins.Top + LayoutMargins.Bottom;

          var InternalSize = RequestSize(OuterSize);

          var OuterWidth = OuterSize.Width;
          var OuterHeight = OuterSize.Height;
          var InnerWidth = InternalSize.Width;
          var InnerHeight = InternalSize.Height;

          nfloat ElementWidth;
          nfloat ElementX;

          // TODO: this check differs the behaviour from Android/Wpf, but otherwise it will draw over other views.
          if (InternalSize.Width > OuterWidth)
          {
            ElementWidth = OuterWidth;
            ElementX = 0;
          }
          else
          {
            switch (ContentHorizontal)
            {
              case iOSHorizontal.Stretch:
                if (!IsContentNaN(this.ContentWidth) && this.ContentWidth < OuterWidth)
                {
                  ElementWidth = ContentWidth;
                  ElementX = (OuterWidth - ContentWidth) / 2;
                }
                else if (!IsContentNaN(ContentMaximumWidth) && ContentMaximumWidth < OuterWidth)
                {
                  ElementWidth = ContentMaximumWidth;
                  ElementX = (OuterWidth - ContentMaximumWidth) / 2;
                }
                else
                {
                  ElementWidth = OuterWidth;
                  ElementX = 0;
                }
                break;

              case iOSHorizontal.Center:
                ElementWidth = InnerWidth;
                ElementX = (OuterWidth - ElementWidth) / 2;
                break;

              case iOSHorizontal.Left:
                ElementWidth = InnerWidth;
                ElementX = 0;
                break;

              case iOSHorizontal.Right:
                ElementWidth = InnerWidth;
                ElementX = OuterWidth - ElementWidth;
                break;

              default:
                throw EnumHelper.UnexpectedValueException(ContentHorizontal);
            }
          }

          nfloat ElementHeight;
          nfloat ElementY;

          // TODO: this check differs the behaviour from Android/Wpf, but otherwise it will draw over other views.
          if (InternalSize.Height > OuterSize.Height)
          {
            ElementHeight = OuterHeight;
            ElementY = 0;
          }
          else
          {
            switch (ContentVertical)
            {
              case iOSVertical.Stretch:
                if (!IsContentNaN(this.ContentHeight) && this.ContentHeight < OuterSize.Height)
                {
                  ElementHeight = ContentHeight;
                  ElementY = (OuterHeight - ContentHeight) / 2;
                }
                else if (!IsContentNaN(this.ContentMaximumHeight) && this.ContentMaximumHeight < OuterSize.Height)
                {
                  ElementHeight = ContentMaximumHeight;
                  ElementY = (OuterHeight - ContentMaximumHeight) / 2;
                }
                else
                {
                  ElementHeight = OuterHeight;
                  ElementY = 0;
                }
                break;

              case iOSVertical.Center:
                ElementHeight = InnerHeight;
                ElementY = (OuterHeight - ElementHeight) / 2;
                break;

              case iOSVertical.Top:
                ElementHeight = InnerHeight;
                ElementY = 0;
                break;

              case iOSVertical.Bottom:
                ElementHeight = InnerHeight;
                ElementY = OuterHeight - ElementHeight;
                break;

              default:
                throw EnumHelper.UnexpectedValueException(ContentVertical);
            }
          }

          var OldWidth = Element.Frame.Width;
          var OldHeight = Element.Frame.Height;

          Element.Frame = new CGRect(LayoutMargins.Left + ElementX, LayoutMargins.Top + ElementY, ElementWidth, ElementHeight); // 3ms

          if (OldWidth != ElementWidth || OldHeight != ElementHeight)
            SizeChanged?.Invoke(ReferenceControl);
        }
      }

      Border.Layout();

      SW.StopLayout();
    }

    internal readonly Inv.Control ReferenceControl;
    internal bool ContentVisible { get; private set; }

    internal CGSize GetContentSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var Result = CGSize.Empty;

      if (ContentVisible)
      {
        Result = RequestSize(this.FitToSize(MaxSize));

        Result.Width += LayoutMargins.Left + LayoutMargins.Right;
        Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;
      }

      SW.StopMeasure();

      return Result;
    }

    private CGSize RequestSize(CGSize MaxSize)
    {
      if (!IsContentNaN(ContentWidth) && !IsContentNaN(ContentHeight))
      {
        return new CGSize(ContentWidth, ContentHeight);
      }
      else
      {
        var ContentSize = MaxSize;
        if (!IsContentNaN(ContentWidth))
          ContentSize.Width = ContentWidth;
        else if (!IsContentNaN(ContentMaximumWidth) && ContentMaximumWidth < ContentSize.Width)
          ContentSize.Width = ContentMaximumWidth;

        if (!IsContentNaN(ContentHeight))
          ContentSize.Height = ContentHeight;
        else if (!IsContentNaN(ContentMaximumHeight) && ContentMaximumHeight < ContentSize.Height)
          ContentSize.Height = ContentMaximumHeight;

        var Result = ContentElement != null ? ContentElement.View.Transform.TransformSize(GetMeasureSize(ContentSize)) : CGSize.Empty;

        if (!IsContentNaN(ContentWidth))
        {
          Result.Width = ContentWidth;
        }
        else
        {
          if (!IsContentNaN(ContentMinimumWidth) && ContentMinimumWidth > Result.Width)
            Result.Width = ContentMinimumWidth;

          if (!IsContentNaN(ContentMaximumWidth) && ContentMaximumWidth < Result.Width)
            Result.Width = ContentMaximumWidth;
        }

        if (!IsContentNaN(ContentHeight))
        {
          Result.Height = ContentHeight;
        }
        else
        {
          if (!IsContentNaN(ContentMinimumHeight) && ContentMinimumHeight > Result.Height)
            Result.Height = ContentMinimumHeight;

          if (!IsContentNaN(ContentMaximumHeight) && ContentMaximumHeight < Result.Height)
            Result.Height = ContentMaximumHeight;
        }

        return Result;
      }
    }
    private CGSize GetMeasureSize(CGSize ContentSize)
    {
      if (!LastContentCalculated || LastLimitSize != ContentSize)
      {
        this.LastContentSize = ContentElement.MeasureSize(ContentSize);
        this.LastLimitSize = ContentSize;
        this.LastContentCalculated = true;
      }

      return LastContentSize;
    }

    // Content size fields are considered to be undefined if they are this value.
    private const int ContentNaNValue = int.MaxValue;
    private static bool IsContentNaN(int Value) => Value == ContentNaNValue;

    private iOSVertical ContentVertical;
    private iOSHorizontal ContentHorizontal;
    private int ContentWidth;
    private int ContentHeight;
    private int ContentMinimumWidth;
    private int ContentMinimumHeight;
    private int ContentMaximumWidth;
    private int ContentMaximumHeight;
    private CGSize LastLimitSize;
    private CGSize LastContentSize;
    private bool LastContentCalculated;
    private readonly iOSStopwatch SW = iOSInstrumentation.ContainerPerformance.NewStopwatch();
  }

  public enum iOSOrientation
  {
    Horizontal,
    Vertical
  }

  public sealed class iOSFrame : iOSView, iOSElement
  {
    internal iOSFrame()
      : base()
    {
      this.Border = new iOSBorder(this);
      this.ClipsToBounds = false;
    }

    public iOSBorder Border { get; }
    public iOSContainer Content { get; private set; }

    public void SetContent(iOSContainer Content)
    {
      this.Content = Content;
      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      if (Content != null)
        Content.Frame = LayoutMargins.InsetRect(Bounds);

      Border.Layout();

      SW.StopLayout();
    }

    internal bool IsTransitioning
    {
      get => ClipsToBounds;
      set => this.ClipsToBounds = value; // clipping is required while transitioning (carousel animation can exit the bounds of the frame).
    }

    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var Result = Content != null ? Content.GetContentSize(this.FitToSize(MaxSize)) : CGSize.Empty;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      SW.StopMeasure();

      return Result;
    }
    UIView iOSElement.View => this;

    private readonly iOSStopwatch SW = iOSInstrumentation.FramePerformance.NewStopwatch();
  }

  public sealed class iOSNative : iOSView, iOSElement
  {
    internal iOSNative()
      : base()
    {
      this.Border = new iOSBorder(this);
    }

    public iOSBorder Border { get; }
    public UIView Content { get; private set; }

    public void SetContent(UIView Content)
    {
      this.Content = Content;
      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      if (Content != null)
        Content.Frame = LayoutMargins.InsetRect(Bounds);

      Border.Layout();

      SW.StopLayout();
    }

    internal bool IsTransitioning { get; set; }

    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var Result = Content != null ? Content.SizeThatFits(this.FitToSize(MaxSize)) : CGSize.Empty;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      SW.StopMeasure();

      return Result;
    }
    UIView iOSElement.View => this;

    private readonly iOSStopwatch SW = iOSInstrumentation.NativePerformance.NewStopwatch();
  }

  public sealed class iOSStack : iOSView, iOSElement
  {
    internal iOSStack()
      : base()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.ContainerArray = Array.Empty<iOSContainer>();
      this.Border = new iOSBorder(this);
    }

    public iOSBorder Border { get; }

    public void SetOrientation(iOSOrientation Orientation)
    {
      if (Orientation != this.Orientation)
      {
        this.Orientation = Orientation;

        this.Arrange();
      }
    }
    public void Compose(IEnumerable<iOSContainer> Containers)
    {
      var PreviousArray = ContainerArray;
      this.ContainerArray = Containers.ToArray();

      foreach (var Container in ContainerArray.Except(PreviousArray))
        this.SafeAddView(Container);

      foreach (var Container in PreviousArray.Except(ContainerArray))
        this.SafeRemoveView(Container);

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      switch (Orientation)
      {
        case iOSOrientation.Horizontal:
          var StackHeight = Bounds.Height - LayoutMargins.Top - LayoutMargins.Bottom;
          var ContainerLeft = (nfloat)LayoutMargins.Left;

          var ColumnSize = new CGSize(UIView.NoIntrinsicMetric, StackHeight);

          foreach (var Container in ContainerArray)
          {
            var ContainerSize = Container.GetContentSize(ColumnSize);

            Container.Frame = new CGRect(ContainerLeft, LayoutMargins.Top, ContainerSize.Width, StackHeight);

            ContainerLeft += ContainerSize.Width;
          }
          break;

        case iOSOrientation.Vertical:
          var StackWidth = Bounds.Width - LayoutMargins.Left - LayoutMargins.Right;
          var ContainerTop = (nfloat)LayoutMargins.Top;

          var RowSize = new CGSize(StackWidth, UIView.NoIntrinsicMetric);

          foreach (var Container in ContainerArray)
          {
            var ContainerSize = Container.GetContentSize(RowSize);

            Container.Frame = new CGRect(LayoutMargins.Left, ContainerTop, StackWidth, ContainerSize.Height);

            ContainerTop += ContainerSize.Height;
          }
          break;

        default:
          throw EnumHelper.UnexpectedValueException(Orientation);
      }

      Border.Layout();

      SW.StopLayout();
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var FitSize = this.FitToSize(MaxSize);

      var Result = new CGSize();

      switch (Orientation)
      {
        case iOSOrientation.Vertical:
          foreach (var Container in ContainerArray)
          {
            var ContainerSize = Container.GetContentSize(FitSize);

            if (ContainerSize.Width > Result.Width)
              Result.Width = ContainerSize.Width;

            Result.Height += ContainerSize.Height;
          }
          break;

        case iOSOrientation.Horizontal:
          foreach (var Container in ContainerArray)
          {
            var ContainerSize = Container.GetContentSize(FitSize);

            if (ContainerSize.Height > Result.Height)
              Result.Height = ContainerSize.Height;

            Result.Width += ContainerSize.Width;
          }
          break;

        default:
          throw EnumHelper.UnexpectedValueException(Orientation);
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      SW.StopMeasure();

      return Result;
    }

    private iOSContainer[] ContainerArray;
    private iOSOrientation Orientation;
    private readonly iOSStopwatch SW = iOSInstrumentation.StackPerformance.NewStopwatch();
  }

  public sealed class iOSDock : iOSView, iOSElement
  {
    internal iOSDock()
      : base()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.ActiveContainerList = new DistinctList<iOSContainer>();
      this.HeaderList = new DistinctList<iOSContainer>();
      this.ClientList = new DistinctList<iOSContainer>();
      this.FooterList = new DistinctList<iOSContainer>();
      this.FixedListArray = new[] { HeaderList, FooterList };
      this.Border = new iOSBorder(this);
    }

    public iOSBorder Border { get; }

    public void SetOrientation(iOSOrientation Orientation)
    {
      if (Orientation != this.Orientation)
      {
        this.Orientation = Orientation;
        this.Arrange();
      }
    }
    public void Compose(IEnumerable<iOSContainer> Headers, IEnumerable<iOSContainer> Clients, IEnumerable<iOSContainer> Footers)
    {
      this.HeaderList.Clear();
      HeaderList.AddRange(Headers);

      this.ClientList.Clear();
      ClientList.AddRange(Clients);

      this.FooterList.Clear();
      FooterList.AddRange(Footers);

      var PreviousContainerList = ActiveContainerList;
      this.ActiveContainerList = new DistinctList<iOSContainer>(HeaderList.Count + ClientList.Count + FooterList.Count);
      ActiveContainerList.AddRange(HeaderList);
      ActiveContainerList.AddRange(ClientList);
      ActiveContainerList.AddRange(FooterList);

      foreach (var Container in ActiveContainerList.Except(PreviousContainerList))
        this.SafeAddView(Container);

      foreach (var Container in PreviousContainerList.Except(ActiveContainerList))
        this.SafeRemoveView(Container);

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      var DockWidth = Bounds.Width - LayoutMargins.Left - LayoutMargins.Right;
      var DockHeight = Bounds.Height - LayoutMargins.Top - LayoutMargins.Bottom;
      var ClientArray = ClientList.Where(C => C.ContentVisible).ToArray();

      switch (Orientation)
      {
        case iOSOrientation.Horizontal:
          var PanelLeft = (nfloat)LayoutMargins.Left;

          var ColumnSize = new CGSize(UIView.NoIntrinsicMetric, DockHeight);
          var HeaderWidth = (nfloat)0;

          foreach (var Header in HeaderList)
          {
            var HeaderSize = Header.GetContentSize(ColumnSize);
            Header.Frame = new CGRect(PanelLeft, LayoutMargins.Top, HeaderSize.Width, DockHeight);
            PanelLeft += HeaderSize.Width;
            HeaderWidth += HeaderSize.Width;
          }

          var FooterWidth = (nfloat)FooterList.Sum(F => F.GetContentSize(ColumnSize).Width);
          var RemainderWidth = DockWidth - HeaderWidth - FooterWidth;

          if (ClientArray.Length > 0)
          {
            var SharedWidth = RemainderWidth < 0 ? 0 : RemainderWidth / ClientArray.Length;

            var LastClient = ClientArray.Last();

            foreach (var Client in ClientArray)
            {
              var FrameWidth = SharedWidth;
              if (RemainderWidth > 0 && Client == LastClient)
                FrameWidth += RemainderWidth - (SharedWidth * ClientArray.Length);

              Client.Frame = new CGRect(PanelLeft, LayoutMargins.Top, FrameWidth, DockHeight);
              PanelLeft += FrameWidth;
            }
          }

          var PanelRight = LayoutMargins.Left + (RemainderWidth < 0 ? DockWidth - RemainderWidth : DockWidth);

          foreach (var Footer in FooterList)
          {
            var FooterSize = Footer.GetContentSize(ColumnSize);
            PanelRight -= FooterSize.Width;
            Footer.Frame = new CGRect(PanelRight, LayoutMargins.Top, FooterSize.Width, DockHeight);
          }
          break;

        case iOSOrientation.Vertical:
          var PanelTop = (nfloat)LayoutMargins.Top;

          var RowSize = new CGSize(DockWidth, UIView.NoIntrinsicMetric);
          var HeaderHeight = (nfloat)0;

          foreach (var Header in HeaderList)
          {
            var HeaderSize = Header.GetContentSize(RowSize);
            Header.Frame = new CGRect(LayoutMargins.Left, PanelTop, DockWidth, HeaderSize.Height);
            PanelTop += HeaderSize.Height;
            HeaderHeight += HeaderSize.Height;
          }

          var FooterHeight = (nfloat)FooterList.Sum(F => F.GetContentSize(RowSize).Height);
          var RemainderHeight = DockHeight - HeaderHeight - FooterHeight;

          if (ClientArray.Length > 0)
          {
            var SharedHeight = RemainderHeight < 0 ? 0 : RemainderHeight / ClientArray.Length;
            var LastClient = ClientArray.Last();

            foreach (var Client in ClientArray)
            {
              var FrameHeight = SharedHeight;
              if (RemainderHeight > 0 && Client == LastClient)
                FrameHeight += RemainderHeight - (SharedHeight * ClientArray.Length);

              Client.Frame = new CGRect(LayoutMargins.Left, PanelTop, DockWidth, FrameHeight);
              PanelTop += FrameHeight;
            }
          }

          var PanelBottom = LayoutMargins.Top + (RemainderHeight < 0 ? DockHeight - RemainderHeight : DockHeight);

          foreach (var Footer in FooterList)
          {
            var FooterSize = Footer.GetContentSize(RowSize);
            PanelBottom -= FooterSize.Height;
            Footer.Frame = new CGRect(LayoutMargins.Left, PanelBottom, DockWidth, FooterSize.Height);
          }
          break;

        default:
          throw EnumHelper.UnexpectedValueException(Orientation);
      }

      Border.Layout();

      SW.StopLayout();
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var FitSize = this.FitToSize(MaxSize);

      var Result = new CGSize();

      switch (Orientation)
      {
        case iOSOrientation.Vertical:
          foreach (var FixedList in FixedListArray)
          {
            foreach (var Fixed in FixedList)
            {
              var FixedSize = Fixed.GetContentSize(FitSize);

              if (FixedSize.Width > Result.Width)
                Result.Width = FixedSize.Width;

              if (FitSize.Height != UIView.NoIntrinsicMetric && FixedSize.Height > 0)
              {
                FitSize.Height -= FixedSize.Height;
                if (FitSize.Height < 0)
                  FitSize.Height = 0;
              }

              Result.Height += FixedSize.Height;
            }
          }

          if (ClientList.Count > 0)
          {
            var ClientSize = new CGSize(FitSize.Width, FitSize.Height / ClientList.Count);

            foreach (var Client in ClientList)
            {
              var AdjustedSize = Client.GetContentSize(ClientSize);

              if (AdjustedSize.Width > Result.Width)
                Result.Width = AdjustedSize.Width;

              if (FitSize.Height != UIView.NoIntrinsicMetric && AdjustedSize.Height > 0)
              {
                FitSize.Height -= AdjustedSize.Height;
                if (FitSize.Height < 0)
                  FitSize.Height = 0;
              }

              Result.Height += AdjustedSize.Height;
            }
          }
          break;

        case iOSOrientation.Horizontal:
          foreach (var FixedList in FixedListArray)
          {
            foreach (var Fixed in FixedList)
            {
              var FixedSize = Fixed.GetContentSize(FitSize);

              if (FixedSize.Height > Result.Height)
                Result.Height = FixedSize.Height;

              if (FitSize.Width != UIView.NoIntrinsicMetric && FixedSize.Width > 0)
              {
                FitSize.Width -= FixedSize.Width;
                if (FitSize.Width < 0)
                  FitSize.Width = 0;
              }

              Result.Width += FixedSize.Width;
            }
          }

          if (ClientList.Count > 0)
          {
            var ClientSize = new CGSize(FitSize.Width / ClientList.Count, FitSize.Height);

            foreach (var Client in ClientList)
            {
              var AdjustedSize = Client.GetContentSize(ClientSize);

              if (AdjustedSize.Height > Result.Height)
                Result.Height = AdjustedSize.Height;

              if (FitSize.Width != UIView.NoIntrinsicMetric && AdjustedSize.Width > 0)
              {
                FitSize.Width -= AdjustedSize.Width;
                if (FitSize.Width < 0)
                  FitSize.Width = 0;
              }

              Result.Width += AdjustedSize.Width;
            }
          }
          break;

        default:
          throw EnumHelper.UnexpectedValueException(Orientation);
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      SW.StopMeasure();

      return Result;
    }

    private readonly Inv.DistinctList<iOSContainer>[] FixedListArray;
    private Inv.DistinctList<iOSContainer> ActiveContainerList;
    private readonly Inv.DistinctList<iOSContainer> HeaderList;
    private readonly Inv.DistinctList<iOSContainer> ClientList;
    private readonly Inv.DistinctList<iOSContainer> FooterList;
    private iOSOrientation Orientation;
    private readonly iOSStopwatch SW = iOSInstrumentation.DockPerformance.NewStopwatch();
  }

  public sealed class iOSWrap : iOSView, iOSElement
  {
    internal iOSWrap()
      : base()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.ContainerArray = Array.Empty<iOSContainer>();
      this.Border = new iOSBorder(this);
      this.ElementLineList = new Inv.DistinctList<ElementLine>();
    }

    public iOSBorder Border { get; }

    public void SetOrientation(iOSOrientation Orientation)
    {
      if (Orientation != this.Orientation)
      {
        this.Orientation = Orientation;

        this.Arrange();
      }
    }
    public void Compose(IEnumerable<iOSContainer> Containers)
    {
      var PreviousArray = ContainerArray;
      this.ContainerArray = Containers.ToArray();

      foreach (var Container in ContainerArray.Except(PreviousArray))
        this.SafeAddView(Container);

      foreach (var Container in PreviousArray.Except(ContainerArray))
        this.SafeRemoveView(Container);

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      var MinX = LayoutMargins.Left;
      var MinY = LayoutMargins.Top;

      var CurrentX = MinX;
      var CurrentY = MinY;

      switch (Orientation)
      {
        case iOSOrientation.Horizontal:
          foreach (var Line in ElementLineList)
          {
            CurrentX = MinX;

            foreach (var Element in Line.MeasuredContainerArray)
            {
              Element.Container.Frame = new CGRect(CurrentX, CurrentY, Element.Size.Width, Line.LineSize);
              CurrentX += (float)Element.Size.Width;
            }

            CurrentY += Line.LineSize;
          }
          break;

        case iOSOrientation.Vertical:
          foreach (var Line in ElementLineList)
          {
            CurrentY = MinY;

            foreach (var Element in Line.MeasuredContainerArray)
            {
              Element.Container.Frame = new CGRect(CurrentX, CurrentY, Line.LineSize, Element.Size.Height);
              CurrentY += (float)Element.Size.Height;
            }

            CurrentX += Line.LineSize;
          }
          break;

        default:
          throw EnumHelper.UnexpectedValueException(Orientation);
      }

      Border.Layout();

      SW.StopLayout();
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var FitSize = this.FitToSize(MaxSize);

      var Result = new CGSize();

      var CurrentX = 0f;
      var CurrentY = 0f;
      var LineSize = 0f;
      var CurrentLineElementList = new Inv.DistinctList<MeasuredContainer>();
      ElementLineList.Clear();

      switch (Orientation)
      {
        case iOSOrientation.Horizontal:
          var MaximumWidth = 0f;

          foreach (var Container in ContainerArray)
          {
            var ContainerSize = Container.GetContentSize(new CGSize(FitSize.Width, Math.Max(0f, FitSize.Height - CurrentY)));

            if (CurrentX > 0 && CurrentX + ContainerSize.Width > FitSize.Width)
            {
              ContainerSize = Container.GetContentSize(new CGSize(FitSize.Width, Math.Max(0f, FitSize.Height - CurrentY - LineSize)));

              ElementLineList.Add(new ElementLine(CurrentLineElementList, LineSize));
              CurrentLineElementList.Clear();
              CurrentX = 0;
              CurrentY += LineSize;
              LineSize = 0;
            }

            if (ContainerSize.Height > LineSize)
              LineSize = (float)ContainerSize.Height;

            CurrentX += (float)ContainerSize.Width;
            CurrentLineElementList.Add(new MeasuredContainer(Container, ContainerSize));

            if (CurrentX > MaximumWidth)
              MaximumWidth = CurrentX;
          }

          if (CurrentLineElementList.Any())
            ElementLineList.Add(new ElementLine(CurrentLineElementList, LineSize));

          Result.Width = MaximumWidth;
          Result.Height = CurrentY + LineSize;
          break;

        case iOSOrientation.Vertical:
          var MaximumHeight = 0f;

          foreach (var Container in ContainerArray)
          {
            var ContainerSize = Container.GetContentSize(new CGSize(Math.Max(0f, FitSize.Width - CurrentX), FitSize.Height));

            if (CurrentY > 0 && CurrentY + ContainerSize.Height > FitSize.Height)
            {
              ContainerSize = Container.GetContentSize(new CGSize(Math.Max(0f, FitSize.Width - CurrentX - LineSize), FitSize.Height));

              ElementLineList.Add(new ElementLine(CurrentLineElementList, LineSize));
              CurrentLineElementList.Clear();
              CurrentX += LineSize;
              CurrentY = 0;
              LineSize = 0;
            }

            if (ContainerSize.Width > LineSize)
              LineSize = (float)ContainerSize.Width;

            CurrentY += (float)ContainerSize.Height;
            CurrentLineElementList.Add(new MeasuredContainer(Container, ContainerSize));

            if (CurrentY > MaximumHeight)
              MaximumHeight = CurrentY;
          }

          if (CurrentLineElementList.Any())
            ElementLineList.Add(new ElementLine(CurrentLineElementList, LineSize));

          Result.Width = CurrentX + LineSize;
          Result.Height = MaximumHeight;
          break;

        default:
          throw EnumHelper.UnexpectedValueException(Orientation);
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      SW.StopMeasure();

      return Result;
    }

    private readonly Inv.DistinctList<ElementLine> ElementLineList;
    private iOSContainer[] ContainerArray;
    private iOSOrientation Orientation;
    private readonly iOSStopwatch SW = iOSInstrumentation.WrapPerformance.NewStopwatch();

    private sealed class ElementLine
    {
      public ElementLine(IEnumerable<MeasuredContainer> MeasuredContainers, float LineSize)
      {
        this.MeasuredContainerArray = MeasuredContainers.ToArray();
        this.LineSize = LineSize;
      }

      public MeasuredContainer[] MeasuredContainerArray { get; }
      public float LineSize { get; }
    }

    private sealed class MeasuredContainer
    {
      public MeasuredContainer(iOSContainer Container, CGSize Size)
      {
        this.Container = Container;
        this.Size = Size;
      }

      public iOSContainer Container { get; }
      public CGSize Size { get; }
    }
  }

  public sealed class iOSOverlay : iOSView, iOSElement
  {
    internal iOSOverlay()
      : base()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.ContainerList = new DistinctList<iOSContainer>();
      this.Border = new iOSBorder(this);
    }

    public iOSBorder Border { get; }

    public void Compose(IEnumerable<iOSContainer> Containers)
    {
      var PreviousList = ContainerList;
      this.ContainerList = Containers.ToDistinctList();

      foreach (var Container in ContainerList.Except(PreviousList))
        this.SafeAddView(Container);

      foreach (var Container in PreviousList.Except(ContainerList))
        this.SafeRemoveView(Container);

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      var ContainerFrame = LayoutMargins.InsetRect(Bounds);
      foreach (var Container in ContainerList)
        Container.Frame = ContainerFrame;

      Border.Layout();

      SW.StopLayout();
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var FitSize = this.FitToSize(MaxSize);

      var Result = new CGSize();

      foreach (var Panel in ContainerList)
      {
        var PanelSize = Panel.GetContentSize(FitSize);

        if (PanelSize.Width > Result.Width)
          Result.Width = PanelSize.Width;

        if (PanelSize.Height > Result.Height)
          Result.Height = PanelSize.Height;
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      SW.StopMeasure();

      return Result;
    }

    private Inv.DistinctList<iOSContainer> ContainerList;
    private readonly iOSStopwatch SW = iOSInstrumentation.OverlayPerformance.NewStopwatch();
  }

  public sealed class iOSBoard : iOSView, iOSElement
  {
    internal iOSBoard()
      : base()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.RecordList = new DistinctList<iOSBoardPin>();
      this.Border = new iOSBorder(this);
    }

    public iOSBorder Border { get; }

    public void RemovePins()
    {
      if (RecordList.Count > 0)
      {
        foreach (var Record in RecordList)
          this.SafeRemoveView(Record.Container);
        RecordList.Clear();

        this.Arrange();
      }
    }
    public void AddPin(iOSContainer Container, CGRect Rect)
    {
      RecordList.Add(new iOSBoardPin()
      {
        Container = Container,
        Rect = Rect
      });
      this.SafeAddView(Container);

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      foreach (var Record in RecordList)
        Record.Container.Frame = LayoutMargins.InsetRect(Record.Rect);

      Border.Layout();

      SW.StopLayout();
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var Result = new CGSize();

      foreach (var Record in RecordList)
      {
        var RecordSize = Record.Rect.Size;

        if (RecordSize.Width > Result.Width)
          Result.Width = RecordSize.Width;

        if (RecordSize.Height > Result.Height)
          Result.Height = RecordSize.Height;
      }

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      SW.StopMeasure();

      return Result;
    }

    private readonly Inv.DistinctList<iOSBoardPin> RecordList;
    private readonly iOSStopwatch SW = iOSInstrumentation.BoardPerformance.NewStopwatch();

    private sealed class iOSBoardPin
    {
      public iOSContainer Container;
      public CGRect Rect;
    }
  }

  public sealed class iOSTable : iOSView, iOSElement
  {
    internal iOSTable()
      : base()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.RowList = new DistinctList<iOSTableRow>();
      this.ColumnList = new DistinctList<iOSTableColumn>();
      this.CellList = new DistinctList<iOSTableCell>();
      this.ContainerList = new DistinctList<iOSContainer>();
      this.Border = new iOSBorder(this);
    }

    public iOSBorder Border { get; }

    internal void Compose(IEnumerable<iOSTableRow> Rows, IEnumerable<iOSTableColumn> Columns, IEnumerable<iOSTableCell> Cells)
    {
      this.RowList.Clear();
      RowList.AddRange(Rows);

      this.ColumnList.Clear();
      ColumnList.AddRange(Columns);

      this.CellList.Clear();
      CellList.AddRange(Cells);

      var PreviousContainerList = ContainerList;
      this.ContainerList = new DistinctList<iOSContainer>(RowList.Count + ColumnList.Count + CellList.Count);
      ContainerList.AddRange(RowList.Select(R => R.Container).ExceptNull());
      ContainerList.AddRange(ColumnList.Select(R => R.Container).ExceptNull());
      ContainerList.AddRange(CellList.Select(R => R.Container).ExceptNull());

      foreach (var Container in ContainerList.Except(PreviousContainerList))
        this.SafeAddView(Container);

      foreach (var Container in PreviousContainerList.Except(ContainerList))
        this.SafeRemoveView(Container);

      this.Arrange();
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      var AvailableWidth = (int)Math.Ceiling(Bounds.Width - LayoutMargins.Left - LayoutMargins.Right);
      var AvailableHeight = (int)Math.Ceiling(Bounds.Height - LayoutMargins.Top - LayoutMargins.Bottom);

      var WidestRowWidth = AvailableWidth;
      var TallestColumnHeight = AvailableHeight;

      var StarRowArray = RowList.Where(R => R.Length.IsStar).ToArray();
      var StarColumnArray = ColumnList.Where(C => C.Length.IsStar).ToArray();

      var StarRowWeight = StarRowArray.Sum(R => R.Length.Size ?? 1);
      var StarColumnWeight = StarColumnArray.Sum(C => C.Length.Size ?? 1);

      var ContainerSizeCache = new Dictionary<iOSContainer, Tuple<int, int, CGSize>>();

      CGSize CacheSize(iOSContainer Container, double MaxWidth, double MaxHeight)
      {
        if (ContainerSizeCache.TryGetValue(Container, out var ExistingSize))
        {
          if (ExistingSize.Item1 == (int)MaxWidth && ExistingSize.Item2 == (int)MaxHeight)
            return ExistingSize.Item3;
        }

        var Result = Container.GetContentSize(new CGSize(MaxWidth, MaxHeight));
        ContainerSizeCache[Container] = new Tuple<int, int, CGSize>((int)MaxWidth, (int)MaxHeight, Result);
        return Result;
      };

      // fixed rows.
      var FixedHeight = 0;
      foreach (var Row in RowList)
      {
        Row.TempHeight = -1;

        if (Row.Length.IsFixed)
        {
          Row.TempHeight = Row.Length.Size ?? 0;
          FixedHeight += Row.TempHeight;

          if (Row.Container != null)
          {
            var MeasuredSize = CacheSize(Row.Container, WidestRowWidth, Row.TempHeight);
            if (MeasuredSize.Width > WidestRowWidth)
              WidestRowWidth = (int)Math.Ceiling(MeasuredSize.Width);
          }
        }
      }

      // fixed columns.
      var FixedWidth = 0;
      foreach (var Column in ColumnList)
      {
        Column.TempWidth = -1;

        if (Column.Length.IsFixed)
        {
          Column.TempWidth = Column.Length.Size ?? 0;
          FixedWidth += Column.TempWidth;

          if (Column.Container != null)
          {
            var MeasuredSize = CacheSize(Column.Container, Column.TempWidth, TallestColumnHeight);
            if (MeasuredSize.Height > TallestColumnHeight)
              TallestColumnHeight = (int)Math.Ceiling(MeasuredSize.Height);
          }
        }
      }

      var RemainingWidth = AvailableWidth - FixedWidth;
      var RemainingHeight = AvailableHeight - FixedHeight;

      // auto rows.
      foreach (var Row in RowList.Where(R => R.Length.IsAuto))
      {
        var RowWidth = 0;
        var RowHeight = 0;

        if (Row.Container != null)
        {
          var MeasuredSize = CacheSize(Row.Container, WidestRowWidth, RemainingHeight);
          RowWidth = (int)Math.Ceiling(MeasuredSize.Width);
          RowHeight = (int)Math.Ceiling(MeasuredSize.Height);
        }

        var UsedWidth = 0;
        foreach (var Cell in CellList.Where(C => C.Row == Row && C.Container != null))
        {
          var MeasuredSize = CacheSize(Cell.Container,
            MaxWidth: Cell.Column.TempWidth == -1 ? RemainingWidth - UsedWidth : Cell.Column.TempWidth,
            MaxHeight: Math.Max(RemainingHeight, 0));

          UsedWidth += (int)Math.Ceiling(MeasuredSize.Width);
          if (MeasuredSize.Height > RowHeight)
            RowHeight = (int)Math.Ceiling(MeasuredSize.Height);
        }

        RowWidth = Math.Max(RowWidth, UsedWidth);
        if (RowWidth > WidestRowWidth)
          WidestRowWidth = RowWidth;

        Row.TempHeight = RowHeight;
        RemainingHeight -= RowHeight;
      }

      // auto columns.
      foreach (var Column in ColumnList.Where(C => C.Length.IsAuto))
      {
        var ColumnWidth = 0;
        var ColumnHeight = 0;

        if (Column.Container != null)
        {
          var MeasuredSize = CacheSize(Column.Container, RemainingWidth, TallestColumnHeight);
          ColumnWidth = (int)Math.Ceiling(MeasuredSize.Width);
          ColumnHeight = (int)Math.Ceiling(MeasuredSize.Height);
        }

        var UsedHeight = 0;
        foreach (var Cell in CellList.Where(C => C.Column == Column && C.Container != null))
        {
          var MeasuredSize = CacheSize(Cell.Container,
            MaxWidth: RemainingWidth,
            MaxHeight: Cell.Row.TempHeight == -1 ? Math.Max(RemainingHeight - UsedHeight, 0) : Cell.Row.TempHeight);

          UsedHeight += (int)Math.Ceiling(MeasuredSize.Height);
          if (MeasuredSize.Width > ColumnWidth)
            ColumnWidth = (int)Math.Ceiling(MeasuredSize.Width);
        }

        ColumnHeight = Math.Max(ColumnHeight, UsedHeight);
        if (ColumnHeight > TallestColumnHeight)
          TallestColumnHeight = ColumnHeight;

        Column.TempWidth = ColumnWidth;
        RemainingWidth -= ColumnWidth;
      }

      var StarRowFactor = StarRowWeight == 0 ? 0f : RemainingHeight / (float)StarRowWeight;
      var StarColumnFactor = StarColumnWeight == 0 ? 0f : RemainingWidth / (float)StarColumnWeight;

      // star rows.
      foreach (var Row in StarRowArray)
      {
        var TargetRowHeight = (int)(StarRowFactor * (Row.Length.Size ?? 1));

        var RowWidth = 0;
        var RowHeight = (int)(StarRowFactor * (Row.Length.Size ?? 1));

        if (Row.Container != null)
        {
          var MeasuredSize = CacheSize(Row.Container, WidestRowWidth, TargetRowHeight);
          RowWidth = (int)Math.Ceiling(MeasuredSize.Width);
          RowHeight = (int)Math.Ceiling(MeasuredSize.Height);
        }

        var UsedWidth = 0;
        foreach (var Cell in CellList.Where(C => C.Row == Row && C.Container != null))
        {
          var MeasuredSize = CacheSize(Cell.Container,
            MaxWidth: Cell.Column.TempWidth == -1 ? Math.Max(RemainingWidth - UsedWidth, 0) : Cell.Column.TempWidth,
            MaxHeight: Math.Max(RowHeight, TargetRowHeight));

          UsedWidth += (int)Math.Ceiling(MeasuredSize.Width);

          var TargetHeight = Math.Max((int)Math.Ceiling(MeasuredSize.Height), TargetRowHeight);
          if (TargetHeight > RowHeight)
            RowHeight = TargetHeight;
        }

        RowWidth = Math.Max(RowWidth, UsedWidth);
        if (RowWidth > WidestRowWidth)
          WidestRowWidth = RowWidth;

        Row.TempHeight = RowHeight;
        RemainingHeight -= RowHeight;
      }

      // star columns.
      foreach (var Column in StarColumnArray)
      {
        var TargetColumnWidth = (int)(StarColumnFactor * (Column.Length.Size ?? 1));

        var ColumnWidth = 0;
        var ColumnHeight = 0;

        if (Column.Container != null)
        {
          var MeasuredSize = CacheSize(Column.Container, TargetColumnWidth, TallestColumnHeight);
          ColumnWidth = (int)Math.Ceiling(MeasuredSize.Width);
          ColumnHeight = (int)Math.Ceiling(MeasuredSize.Height);
        }

        var UsedHeight = 0;
        foreach (var Cell in CellList.Where(C => C.Column == Column && C.Container != null))
        {
          var MeasuredSize = CacheSize(Cell.Container,
            MaxWidth: Math.Max(ColumnWidth, TargetColumnWidth),
            MaxHeight: Cell.Row.TempHeight);

          UsedHeight += (int)Math.Ceiling(MeasuredSize.Height);

          var TargetWidth = Math.Max((int)Math.Ceiling(MeasuredSize.Width), TargetColumnWidth);
          if (TargetWidth > ColumnWidth)
            ColumnWidth = TargetWidth;
        }

        ColumnHeight = Math.Max(ColumnHeight, UsedHeight);
        if (ColumnHeight > TallestColumnHeight)
          TallestColumnHeight = ColumnHeight;

        Column.TempWidth = ColumnWidth;
        RemainingWidth -= ColumnWidth;
      }

      var FinalWidth = Math.Max(WidestRowWidth, ColumnList.Sum(C => C.TempWidth));
      var FinalHeight = Math.Max(TallestColumnHeight, RowList.Sum(R => R.TempHeight));

      // row elements.
      var RowOffset = (int)LayoutMargins.Top;
      foreach (var Row in RowList)
      {
        Row.TempTop = RowOffset;
        RowOffset += Row.TempHeight;

        if (Row.Container != null)
          Row.Container.Frame = new CGRect(LayoutMargins.Left, Row.TempTop, FinalWidth, Row.TempHeight);
      }

      // column elements.
      var ColumnOffset = (int)LayoutMargins.Left;
      foreach (var Column in ColumnList)
      {
        Column.TempLeft = ColumnOffset;
        ColumnOffset += Column.TempWidth;

        if (Column.Container != null)
          Column.Container.Frame = new CGRect(Column.TempLeft, LayoutMargins.Top, Column.TempWidth, FinalHeight);
      }

      // cell elements.
      foreach (var Cell in CellList)
      {
        if (Cell.Container != null)
          Cell.Container.Frame = new CGRect(Cell.Column.TempLeft, Cell.Row.TempTop, Cell.Column.TempWidth, Cell.Row.TempHeight);
      }

      Border.Layout();

      SW.StopLayout();
    }

    internal Inv.DistinctList<iOSTableRow> RowList { get; }
    internal Inv.DistinctList<iOSTableColumn> ColumnList { get; }
    internal Inv.DistinctList<iOSTableCell> CellList { get; }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var FitSize = this.FitToSize(MaxSize);

      var Result = new CGSize();

      var RowHeight = (nfloat)0;

      foreach (var Row in RowList)
      {
        var RowSize = Row.CalculateSize(FitSize);

        if (RowSize.Width > Result.Width)
          Result.Width = RowSize.Width;

        RowHeight += RowSize.Height;
      }

      var ColumnWidth = (nfloat)0;

      foreach (var Column in ColumnList)
      {
        var ColumnSize = Column.CalculateSize(FitSize);

        if (ColumnSize.Height > Result.Height)
          Result.Height = ColumnSize.Height;

        ColumnWidth += ColumnSize.Width;
      }

      if (ColumnWidth > Result.Width)
        Result.Width = ColumnWidth;

      if (RowHeight > Result.Height)
        Result.Height = RowHeight;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      SW.StopMeasure();

      return Result;
    }

    private Inv.DistinctList<iOSContainer> ContainerList;
    private readonly iOSStopwatch SW = iOSInstrumentation.TablePerformance.NewStopwatch();
  }

  internal sealed class iOSTableLength
  {
    internal iOSTableLength(bool Fill, int? Value)
    {
      this.Fill = Fill;
      this.Size = Value;
    }

    public bool Fill { get; }
    public int? Size { get; }
    public bool IsStar => Fill;
    public bool IsAuto => !Fill && Size == null;
    public bool IsFixed => !Fill && Size != null;
  }

  internal sealed class iOSTableColumn
  {
    internal iOSTableColumn(iOSTable Table, iOSContainer Container, bool Fill, int? Value)
    {
      this.Table = Table;
      this.Container = Container;
      this.Length = new iOSTableLength(Fill, Value);
    }

    public iOSContainer Container { get; }
    public iOSTableLength Length { get; }

    internal int TempLeft;
    internal int TempWidth;

    internal CGSize CalculateSize(CGSize MaxSize)
    {
      var Result = Container != null ? Container.GetContentSize(MaxSize) : CGSize.Empty;

      if (Length.IsFixed)
        Result.Width = Length.Size.Value;

      foreach (var Cell in Table.CellList.Where(C => C.Column == this))
      {
        if (Cell.Container != null)
        {
          var CellSize = Cell.Container.GetContentSize(MaxSize);

          if (!Length.IsFixed && CellSize.Width > Result.Width)
            Result.Width = CellSize.Width;

          if (Cell.Row.Length.IsFixed)
            Result.Height += Cell.Row.Length.Size.Value;
          else
            Result.Height += CellSize.Height;
        }
      }

      return Result;
    }

    private readonly iOSTable Table;
  }

  internal sealed class iOSTableRow
  {
    internal iOSTableRow(iOSTable Table, iOSContainer Container, bool Fill, int? Value)
    {
      this.Table = Table;
      this.Container = Container;
      this.Length = new iOSTableLength(Fill, Value);
    }

    public iOSContainer Container { get; }
    public iOSTableLength Length { get; }

    internal int TempTop;
    internal int TempHeight;

    internal CGSize CalculateSize(CGSize MaxSize)
    {
      var Result = Container != null ? Container.GetContentSize(MaxSize) : CGSize.Empty;

      if (Length.IsFixed)
        Result.Height = Length.Size.Value;

      foreach (var Cell in Table.CellList.Where(C => C.Row == this))
      {
        if (Cell.Container != null)
        {
          var AvailableSize = new CGSize(Math.Max(MaxSize.Width - Result.Width, 0), MaxSize.Height);

          var CellSize = Cell.Container.GetContentSize(AvailableSize);

          if (!Length.IsFixed && CellSize.Height > Result.Height)
            Result.Height = CellSize.Height;

          if (Cell.Column.Length.IsFixed)
            Result.Width += Cell.Column.Length.Size.Value;
          else
            Result.Width += CellSize.Width;
        }
      }

      return Result;
    }

    private readonly iOSTable Table;
  }

  internal sealed class iOSTableCell
  {
    internal iOSTableCell(iOSTableColumn Column, iOSTableRow Row, iOSContainer Container)
    {
      this.Column = Column;
      this.Row = Row;
      this.Container = Container;
    }

    public iOSTableColumn Column { get; }
    public iOSTableRow Row { get; }
    public iOSContainer Container { get; }
  }

  public sealed class iOSScroll : UIScrollView, iOSElement
  {
    internal iOSScroll()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.DisableSafeAreaLayoutMargins();
      this.Border = new iOSBorder(this);

      // removes indent on the left edge.
      if (iOSFoundation.iOS11_0)
        this.ContentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.Never;
      else
        this.ContentInset = UIEdgeInsets.Zero;

      //Debug.Assert(this.ContentInset == UIEdgeInsets.Zero);
      //Debug.Assert(this.ScrollIndicatorInsets == UIEdgeInsets.Zero);

      this.KeyboardNotificationObserverList = new List<Foundation.NSObject>();

      foreach (var Name in new[] { UIKeyboard.WillShowNotification, UIKeyboard.WillHideNotification, UIKeyboard.WillChangeFrameNotification, UIKeyboard.DidShowNotification, UIKeyboard.DidHideNotification, UIKeyboard.DidChangeFrameNotification })
      {
        KeyboardNotificationObserverList.Add(Foundation.NSNotificationCenter.DefaultCenter.AddObserver(Name, (Notification) =>
        {
          var AnimationCurveValue = Notification.UserInfo[UIKeyboard.AnimationCurveUserInfoKey] as Foundation.NSNumber;
          var AnimationDurationValue = Notification.UserInfo[UIKeyboard.AnimationDurationUserInfoKey] as Foundation.NSNumber;
          var KeyboardEndValue = Notification.UserInfo[UIKeyboard.FrameEndUserInfoKey] as Foundation.NSValue;
          var IsLocalValue = Notification.UserInfo[UIKeyboard.IsLocalUserInfoKey] as Foundation.NSNumber;

          var IsLocal = IsLocalValue == null || IsLocalValue.BoolValue;

          if (AnimationCurveValue != null && AnimationDurationValue != null && KeyboardEndValue != null && IsLocal)
          {
            var AnimationCurve = (UIViewAnimationCurve)(ulong)AnimationCurveValue.UnsignedLongValue;
            var AnimationDuration = AnimationDurationValue.DoubleValue;
            var KeyboardEndRect = ConvertRectFromView(KeyboardEndValue.CGRectValue, null);

            var MaxY = Bounds.GetMaxY();
            var NewInsets = UIEdgeInsets.Zero;

            // If keyboard overlaps, but doesn't completely obscure scroll view...
            if (KeyboardEndRect.Y < MaxY && MaxY <= KeyboardEndRect.GetMaxY() && MaxY - KeyboardEndRect.Y < Bounds.Height)
            {
              NewInsets = new UIEdgeInsets(0, 0, MaxY - KeyboardEndRect.Y, 0);
            }
            else if (KeyboardEndRect.Y <= Bounds.Y && Bounds.Y < KeyboardEndRect.GetMaxY() && KeyboardEndRect.GetMaxY() - Bounds.Y < Bounds.Height)
            {
              NewInsets = new UIEdgeInsets(KeyboardEndRect.GetMaxY() - Bounds.Y, 0, 0, 0);
            }

            UIView.BeginAnimations("au.com.invention.iOSscroll-keyboard");
            UIView.SetAnimationDuration(AnimationDuration);
            UIView.SetAnimationCurve(AnimationCurve);
            UIView.SetAnimationBeginsFromCurrentState(true);

            ContentInset = NewInsets;
            ScrollIndicatorInsets = NewInsets;

            UIView.CommitAnimations();
          }
        }));
      }
    }
    ~iOSScroll()
    {
      foreach (var Observer in KeyboardNotificationObserverList)
        Foundation.NSNotificationCenter.DefaultCenter.RemoveObserver(Observer);
    }

    public iOSBorder Border { get; }

    public void SetOrientation(iOSOrientation Orientation)
    {
      if (Orientation != this.Orientation)
      {
        this.Orientation = Orientation;

        this.Arrange();
      }
    }
    public void SetContent(iOSContainer Content)
    {
      if (Content != this.Content)
      {
        if (this.Content != null)
          this.SafeRemoveView(this.Content);

        this.Content = Content;

        if (this.Content != null)
          this.SafeAddView(this.Content);

        this.Arrange();
      }
    }
    public void ScrollToStart()
    {
      SetContentOffset(new CGPoint(x: 0, y: 0), animated: false);
    }
    public void ScrollToEnd()
    {
      if (Orientation == iOSOrientation.Vertical)
        SetContentOffset(new CGPoint(x: 0, y: ContentSize.Height - Bounds.Height + ContentInset.Bottom), animated: false);
      else
        SetContentOffset(new CGPoint(x: ContentSize.Width - Bounds.Width + ContentInset.Right, y: 0), animated: false);
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      if (Content != null)
      {
        var LayoutSize = Content.GetContentSize(this.FitToSize(Bounds.Size));

        if (Orientation == iOSOrientation.Vertical)
        {
          LayoutSize.Width = Bounds.Width;

          var LayoutHeight = Bounds.Height - LayoutMargins.Top - LayoutMargins.Bottom;
          if (LayoutSize.Height < LayoutHeight)
            LayoutSize.Height = LayoutHeight;

          Content.Frame = new CGRect(LayoutMargins.Left, LayoutMargins.Top, Bounds.Width - LayoutMargins.Left - LayoutMargins.Right, LayoutSize.Height);
        }
        else if (Orientation == iOSOrientation.Horizontal)
        {
          LayoutSize.Height = Bounds.Height;

          var LayoutWidth = Bounds.Width - LayoutMargins.Left - LayoutMargins.Right;
          if (LayoutSize.Width < LayoutWidth)
            LayoutSize.Width = LayoutWidth;

          Content.Frame = new CGRect(LayoutMargins.Left, LayoutMargins.Top, LayoutSize.Width, LayoutSize.Height - LayoutMargins.Top - LayoutMargins.Bottom);
        }

        this.ContentSize = new CGSize(Content.Frame.Size.Width + LayoutMargins.Left + LayoutMargins.Right, Content.Frame.Size.Height + LayoutMargins.Top + LayoutMargins.Bottom);
      }
      else
      {
        this.ContentSize = CGSize.Empty;
      }

      Border.Layout();

      SW.StopLayout();
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var Result = Content != null ? Content.GetContentSize(this.FitToSize(MaxSize)) : CGSize.Empty;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      SW.StopMeasure();

      return Result;
    }

    private iOSContainer Content;
    private iOSOrientation Orientation;
    private readonly List<Foundation.NSObject> KeyboardNotificationObserverList;
    private readonly iOSStopwatch SW = iOSInstrumentation.ScrollPerformance.NewStopwatch();
  }

  public sealed class iOSShape : iOSView, iOSElement
  {
    internal iOSShape()
      : base()
    {
      this.Border = new iOSBorder(this);

      this.ShapeLayer = new CoreAnimation.CAShapeLayer();
      Layer.AddSublayer(ShapeLayer);
    }

    public iOSBorder Border { get; }
    public CGColor FillColor
    {
      get => ShapeLayer.FillColor;
      set => ShapeLayer.FillColor = value;
    }
    public CGColor StrokeColor
    {
      get => ShapeLayer.StrokeColor;
      set => ShapeLayer.StrokeColor = value;
    }
    public nfloat StrokeWidth
    {
      get => ShapeLayer.LineWidth;
      set => ShapeLayer.LineWidth = value;
    }
    public Foundation.NSString LineCap
    {
      get => ShapeLayer.LineCap;
      set => ShapeLayer.LineCap = value;
    }
    public Foundation.NSString LineJoin
    {
      get => ShapeLayer.LineJoin;
      set => ShapeLayer.LineJoin = value;
    }
    public Foundation.NSNumber[] LineDashPattern
    {
      get => ShapeLayer.LineDashPattern;
      set => ShapeLayer.LineDashPattern = value;
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      // stretch implementation for iOS.
      var PathRect = ShapeLayer.Path.PathBoundingBox;
      var ViewWidth = Bounds.Width - LayoutMargins.Left - LayoutMargins.Right;
      var ViewHeight = Bounds.Height - LayoutMargins.Top - LayoutMargins.Bottom;

      var boundingBoxAspectRatio = PathRect.Width / PathRect.Height;
      var viewAspectRatio = (nfloat)NotionalWidth / (nfloat)NotionalHeight;

      nfloat scaleFactor;
      if (boundingBoxAspectRatio > viewAspectRatio)
        scaleFactor = ViewWidth / PathRect.Width; // Width is limiting factor
      else
        scaleFactor = ViewHeight / PathRect.Height; // Height is limiting factor

      ShapeLayer.Position = new CGPoint(LayoutMargins.Left, LayoutMargins.Top);
      ShapeLayer.Transform = CoreAnimation.CATransform3D.MakeScale(scaleFactor, scaleFactor, 1.0F);

      Border.Layout();

      SW.StopLayout();
    }

    public void SetPath(CGPath iOSPath, int NotionalWidth, int NotionalHeight)
    {
      this.NotionalWidth = NotionalWidth;
      this.NotionalHeight = NotionalHeight;

      ShapeLayer.Path = iOSPath;

      this.Arrange();
    }

    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var MaxResult = this.FitToSize(MaxSize);

      var Result = new CGSize(NotionalWidth, NotionalHeight);
      if (Result.Width > MaxResult.Width)
        Result.Width = MaxResult.Width;

      if (Result.Height > MaxResult.Height)
        Result.Height = MaxResult.Height;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      Result = new CGSize(Math.Ceiling(Result.Width), Math.Ceiling(Result.Height));

      SW.StopMeasure();

      return Result;
    }
    UIView iOSElement.View => this;

    private readonly CoreAnimation.CAShapeLayer ShapeLayer;
    private int NotionalWidth;
    private int NotionalHeight;
    private readonly iOSStopwatch SW = iOSInstrumentation.ShapePerformance.NewStopwatch();
  }

  public sealed class iOSVideo : iOSView, iOSElement
  {
    internal iOSVideo()
      : base()
    {
      this.VideoController = new AVKit.AVPlayerViewController();
      AddSubview(VideoController.View);
      VideoController.ShowsPlaybackControls = false;
      //VideoController.ShouldAutoplay = false;

      this.ViewPlayer = new AVFoundation.AVPlayer();
      VideoController.Player = ViewPlayer;

      this.Border = new iOSBorder(this);
    }

    public iOSBorder Border { get; }

    public void SetSource(Uri Uri)
    {
      if (Uri != null)
        ViewPlayer.ReplaceCurrentItemWithPlayerItem(new AVFoundation.AVPlayerItem(Uri));
      else
        ViewPlayer.ReplaceCurrentItemWithPlayerItem(null);
    }

    public void Play()
    {
      ViewPlayer.Play();
    }
    public void Pause()
    {
      ViewPlayer.Pause();
    }
    public void Stop()
    {
      ViewPlayer.Pause();
      ViewPlayer.Seek(new CoreMedia.CMTime(0, 1));
    }

    public override void LayoutSubviews()
    {
      SW.StartLayout();

      base.LayoutSubviews();

      VideoController.View.Frame = LayoutMargins.InsetRect(Bounds);

      Border.Layout();

      SW.StopLayout();
    }

    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      SW.StartMeasure();

      var MaxResult = this.FitToSize(MaxSize);
      var Result = VideoController.View.SizeThatFits(MaxResult);

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      Result = new CGSize(Math.Ceiling(Result.Width), Math.Ceiling(Result.Height));

      SW.StopMeasure();

      return Result;
    }
    UIView iOSElement.View => this;

    private readonly AVKit.AVPlayerViewController VideoController;
    private readonly AVFoundation.AVPlayer ViewPlayer;
    private readonly iOSStopwatch SW = iOSInstrumentation.VideoPerformance.NewStopwatch();
  }

  internal sealed class iOSDatePicker : UIDatePicker, iOSElement
  {
    internal iOSDatePicker()
    {
      this.TranslatesAutoresizingMaskIntoConstraints = false;
      this.LayoutMargins = UIEdgeInsets.Zero;
      this.Border = new iOSBorder(this);
    }

    public iOSBorder Border { get; }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      Border.Layout();
    }

    UIView iOSElement.View => this;
    CGSize iOSElement.MeasureSize(CGSize MaxSize)
    {
      var Result = IntrinsicContentSize;

      Result.Width += LayoutMargins.Left + LayoutMargins.Right;
      Result.Height += LayoutMargins.Top + LayoutMargins.Bottom;

      return Result;
    }
  }

  internal sealed class iOSPopoverViewController : UIViewController
  {
    internal iOSPopoverViewController()
    {
      this.ModalPresentationStyle = UIKit.UIModalPresentationStyle.Popover;
    }

    public event Action LoadEvent;

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();

      LoadEvent?.Invoke();
    }
    public override void ViewDidLayoutSubviews()
    {
      base.ViewDidLayoutSubviews();

      if (Content != null)
        Content.Frame = View.Bounds;
    }

    public void SetContent(iOSContainer Content)
    {
      if (this.Content != Content)
      {
        if (this.Content != null)
          View.SafeRemoveView(this.Content);

        this.Content = Content;

        if (this.Content != null)
          View.SafeAddView(this.Content);
      }
    }

    private iOSContainer Content;
  }

  internal sealed class iOSController : UIKit.UIViewController
  {
    internal iOSController()
      : base()
    {
      this.LeftEdgeRecognizer = new UIKit.UIScreenEdgePanGestureRecognizer(() =>
      {
        if (LeftEdgeRecognizer.State == UIGestureRecognizerState.Recognized)
          LeftEdgeSwipeEvent?.Invoke();
      }) { Edges = UIKit.UIRectEdge.Left, DelaysTouchesBegan = false };

      this.RightEdgeRecognizer = new UIKit.UIScreenEdgePanGestureRecognizer(() =>
      {
        if (RightEdgeRecognizer.State == UIGestureRecognizerState.Recognized)
          RightEdgeSwipeEvent?.Invoke();
      }) { Edges = UIKit.UIRectEdge.Right, DelaysTouchesBegan = false };

      // NOTE: don't do the following two commented out lines as it will break the surface transition animations.
      //View.TranslatesAutoresizingMaskIntoConstraints = false;
      //View.LayoutMargins = UIEdgeInsets.Zero;

      View.DisableSafeAreaLayoutMargins();

      if (iOSFoundation.iOS11_0)
        this.ViewRespectsSystemMinimumLayoutMargins = false;
    }

    public iOSSurface Surface { get; private set; }
    public event Action LoadEvent;
    public bool IsLeftEdgeSwipe
    {
      get => IsLeftEdgeSwipeField;
      set
      {
        if (value != IsLeftEdgeSwipeField)
        {
          this.IsLeftEdgeSwipeField = value;

          if (value)
            View.AddGestureRecognizer(LeftEdgeRecognizer);
          else
            View.RemoveGestureRecognizer(LeftEdgeRecognizer);
        }
      }
    }
    public event Action LeftEdgeSwipeEvent;
    public bool IsRightEdgeSwipe
    {
      get => IsRightEdgeSwipeField;
      set
      {
        if (value != IsRightEdgeSwipeField)
        {
          this.IsRightEdgeSwipeField = value;

          if (value)
            View.AddGestureRecognizer(RightEdgeRecognizer);
          else
            View.RemoveGestureRecognizer(RightEdgeRecognizer);
        }
      }
    }
    public event Action RightEdgeSwipeEvent;
    public event Action MemoryWarningEvent;
    public event Action<CoreGraphics.CGSize> ResizeEvent;
    public event Func<UIKeyCommand[]> KeyQuery;
    public event Action<UIKit.UIKeyModifierFlags, string> KeyEvent;

    public override UIStatusBarStyle PreferredStatusBarStyle()
    {
      if (!PrefersStatusBarHidden())
      {
        var iOSWindow = AccessWindow();

        if (iOSWindow.BackgroundColor != null)
        {
          iOSWindow.BackgroundColor.GetHSBA(out var _, out var _, out var Brightness, out var _);

          // dark background colours need light content.
          if (Brightness < 0.5F)
            return UIStatusBarStyle.LightContent;
        }
      }

      return base.PreferredStatusBarStyle();
    }
    // TODO: will be useful when supporting the full screen of a notched/virtual home button.
    //public override bool PrefersHomeIndicatorAutoHidden => !HasNotch();

    public override bool PrefersStatusBarHidden()
    {
      return !iOSShell.ShowStatusBar && !HasNotch();
    }
    public override bool CanBecomeFirstResponder => true;
    public override UIKeyCommand[] KeyCommands
    {
      get
      {
        if (FindFirstResponder(View) == null && KeyQuery != null)
          return KeyQuery();
        else
          return base.KeyCommands;
      }
    }
    public void SetSurface(iOSSurface Surface)
    {
      this.Surface = Surface;
      View.Arrange();
    }
    public void Reload()
    {
      if (IsLoaded)
        LoadInvoke();
    }

    public override void ViewDidLayoutSubviews()
    {
      base.ViewDidLayoutSubviews();

      if (Surface != null)
        Surface.Frame = View.Bounds;
    }
    public override void DidReceiveMemoryWarning()
    {
      // Release any cached data, images, etc that aren't in use.
      MemoryWarningEvent?.Invoke();

      base.DidReceiveMemoryWarning();
    }
    public override void ViewDidLoad()
    {
      base.ViewDidLoad();

      //View.TranslatesAutoresizingMaskIntoConstraints = false; // NOTE: causes the background colour to be lost.
      View.LayoutMargins = UIEdgeInsets.Zero; // NOTE: this is a workaround to avoid the default leading/trailing 16pt margin.

      this.IsLoaded = true;

      LoadInvoke();
    }
    public override void ViewWillAppear(bool animated)
    {
      base.ViewWillAppear(animated);
    }
    public override void ViewDidAppear(bool animated)
    {
      base.ViewDidAppear(animated);

      BecomeFirstResponder();
    }
    public override void ViewWillDisappear(bool animated)
    {
      base.ViewWillDisappear(animated);
    }
    public override void ViewDidDisappear(bool animated)
    {
      base.ViewDidDisappear(animated);
    }
    public override void ViewWillTransitionToSize(CoreGraphics.CGSize toSize, IUIViewControllerTransitionCoordinator coordinator)
    {
      base.ViewWillTransitionToSize(toSize, coordinator);

      ResizeEvent?.Invoke(toSize);
    }
    public override bool ShouldAutorotateToInterfaceOrientation(UIInterfaceOrientation toInterfaceOrientation)
    {
      return true;
    }
    public override void WillAnimateRotation(UIInterfaceOrientation toInterfaceOrientation, double duration)
    {
      base.WillAnimateRotation(toInterfaceOrientation, duration);
    }

    [Foundation.Export("KeyDown:")]
    private void KeyDown(UIKeyCommand Command)
    {
      KeyEvent?.Invoke(Command.ModifierFlags, Command.Input);
    }
    /* // TODO: track both key up/down.
    [Foundation.Export("KeyUp:")]
    private void KeyDown(UIKeyCommand Command)
    {
      if (KeyEvent != null)
        KeyEvent(Command.ModifierFlags, Command.Input);
    }
    */
    private UIView FindFirstResponder(UIView CurrentView)
    {
      if (CurrentView.IsFirstResponder)
        return CurrentView;

      foreach (var Subview in CurrentView.Subviews)
      {
        var ResultView = FindFirstResponder(Subview);

        if (ResultView != null)
          return ResultView;
      }

      return null;
    }

    private bool HasNotch()
    {
      if (!iOSFoundation.iOS11_0)
        return false;

      return AccessWindow().SafeAreaInsets.Top > 0; // iPhoneX Notch
    }
    private void LoadInvoke()
    {
      LoadEvent?.Invoke();
    }
    private UIKit.UIWindow AccessWindow() => Surface?.Window ?? UIKit.UIApplication.SharedApplication.KeyWindow;

    private bool IsLoaded;
    private readonly UIScreenEdgePanGestureRecognizer LeftEdgeRecognizer;
    private bool IsLeftEdgeSwipeField;
    private readonly UIScreenEdgePanGestureRecognizer RightEdgeRecognizer;
    private bool IsRightEdgeSwipeField;
  }

  [Foundation.Register("iOSAppDelegate")]
  internal sealed class iOSAppDelegate : UIKit.UIApplicationDelegate
  {
    public override UIKit.UIWindow Window { get; set; }

    public override void OnActivated(UIApplication application)
    {
    }
    public override void OnResignActivation(UIApplication application)
    {
      iOSEngine.Pausing();
    }
    public override void DidEnterBackground(UIApplication application)
    {
    }
    public override void WillEnterForeground(UIApplication application)
    {
      iOSEngine.Resuming();
    }
    public override void WillTerminate(UIApplication application)
    {
      iOSEngine.Terminating();
    }
    public override bool FinishedLaunching(UIKit.UIApplication app, Foundation.NSDictionary options)
    {
      this.Window = iOSEngine.Launching(iOSInstall);

      return true;
    }
    public override void ReceiveMemoryWarning(UIApplication application)
    {
      // TODO: what to do?
    }

    internal static Action iOSInstall;
    internal static iOSEngine iOSEngine;
  }

  public static class iOSInstrumentation
  {
    [Conditional("DEBUG")]
    public static void Clear()
    {
#if PERFORMANCE
      foreach (var Performance in PerformanceArray)
        Performance.Clear();
#endif
    }
    [Conditional("DEBUG")]
    public static void Print()
    {
#if PERFORMANCE
      Debug.WriteLine("**PERFORMANCE REPORT**");

      var PerformanceNameMax = PerformanceArray.Max(P => P.Name.Length);

      foreach (var Performance in PerformanceArray.OrderBy(P => P.Name))
      {
        if (Performance.MeasureCount > 0)
        {
          var MeasureElapsedMilliseconds = TimeSpan.FromTicks(Performance.MeasureElapsedTicks).TotalMilliseconds;

          Debug.WriteLine($"**{Performance.Name.ToUpper().PadRight(PerformanceNameMax)} MEASURE x{Performance.MeasureCount} = {(int)MeasureElapsedMilliseconds} ~{(int)(MeasureElapsedMilliseconds / Performance.MeasureCount)}");
        }

        if (Performance.LayoutCount > 0)
        {
          var LayoutElapsedMilliseconds = TimeSpan.FromTicks(Performance.LayoutElapsedTicks).TotalMilliseconds;

          Debug.WriteLine($"**{Performance.Name.ToUpper().PadRight(PerformanceNameMax)} LAYOUT  x{Performance.LayoutCount} = {(int)LayoutElapsedMilliseconds} ~{(int)(LayoutElapsedMilliseconds / Performance.LayoutCount)}");
        }
      }

      Debug.WriteLine("**********************");
#endif
    }

    internal static iOSStopwatch NewStopwatch(this iOSPerformance Performance)
    {
#if PERFORMANCE
      return new iOSStopwatch(Performance);
#else
      return null;
#endif
    }
    [Conditional("DEBUG")]
    internal static void StartMeasure(this iOSStopwatch SW)
    {
#if PERFORMANCE
      AccumulateStack.Push(new iOSAccumulate());

      SW.Start();
#endif
    }
    [Conditional("DEBUG")]
    internal static void StopMeasure(this iOSStopwatch SW)
    {
#if PERFORMANCE
      SW.Stop();

      var Result = AccumulateStack.Pop();
      if (AccumulateStack.Count > 0)
        AccumulateStack.Peek().MeasureElapsedTicks += SW.ElapsedTicks;

      SW.Performance.MeasureCount++;
      SW.Performance.MeasureElapsedTicks += SW.ElapsedTicks - Result.MeasureElapsedTicks;
#endif
    }
    [Conditional("DEBUG")]
    internal static void StartLayout(this iOSStopwatch SW)
    {
#if PERFORMANCE
      AccumulateStack.Push(new iOSAccumulate());

      SW.Start();
#endif
    }
    [Conditional("DEBUG")]
    internal static void StopLayout(this iOSStopwatch SW)
    {
#if PERFORMANCE
      SW.Stop();

      var Result = AccumulateStack.Pop();
      if (AccumulateStack.Count > 0)
        AccumulateStack.Peek().LayoutElapsedTicks += SW.ElapsedTicks;

      SW.Performance.LayoutCount++;
      SW.Performance.LayoutElapsedTicks += SW.ElapsedTicks - Result.LayoutElapsedTicks;
#endif
    }

    internal static readonly iOSPerformance BlockPerformance = NewPerformance("block");
    internal static readonly iOSPerformance BoardPerformance = NewPerformance("board");
    internal static readonly iOSPerformance BrowserPerformance = NewPerformance("browser");
    internal static readonly iOSPerformance ButtonPerformance = NewPerformance("button");
    internal static readonly iOSPerformance CanvasPerformance = NewPerformance("canvas");
    internal static readonly iOSPerformance ContainerPerformance = NewPerformance("container");
    internal static readonly iOSPerformance DockPerformance = NewPerformance("dock");
    internal static readonly iOSPerformance EditPerformance = NewPerformance("edit");
    internal static readonly iOSPerformance FlowPerformance = NewPerformance("flow");
    internal static readonly iOSPerformance FramePerformance = NewPerformance("frame");
    internal static readonly iOSPerformance LabelPerformance = NewPerformance("label");
    internal static readonly iOSPerformance GraphicPerformance = NewPerformance("graphic");
    internal static readonly iOSPerformance MemoPerformance = NewPerformance("memo");
    internal static readonly iOSPerformance NativePerformance = NewPerformance("native");
    internal static readonly iOSPerformance OverlayPerformance = NewPerformance("overlay");
    internal static readonly iOSPerformance SearchPerformance = NewPerformance("search");
    internal static readonly iOSPerformance ScrollPerformance = NewPerformance("scroll");
    internal static readonly iOSPerformance ShapePerformance = NewPerformance("shape");
    internal static readonly iOSPerformance StackPerformance = NewPerformance("stack");
    internal static readonly iOSPerformance SwitchPerformance = NewPerformance("switch");
    internal static readonly iOSPerformance SurfacePerformance = NewPerformance("surface");
    internal static readonly iOSPerformance TablePerformance = NewPerformance("table");
    internal static readonly iOSPerformance VideoPerformance = NewPerformance("video");
    internal static readonly iOSPerformance WrapPerformance = NewPerformance("wrap");

#if PERFORMANCE
    private static readonly Stack<iOSAccumulate> AccumulateStack = new Stack<iOSAccumulate>();

    private static readonly iOSPerformance[] PerformanceArray = new iOSPerformance[]
    {
      BoardPerformance,
      BrowserPerformance,
      ButtonPerformance,
      CanvasPerformance,
      ContainerPerformance,
      DockPerformance,
      EditPerformance,
      FlowPerformance,
      FramePerformance,
      GraphicPerformance,
      LabelPerformance,
      MemoPerformance,
      NativePerformance,
      OverlayPerformance,
      SearchPerformance,
      ScrollPerformance,
      ShapePerformance,
      StackPerformance,
      SwitchPerformance,
      SurfacePerformance,
      TablePerformance,
      VideoPerformance,
      WrapPerformance
    };
#endif

    private static iOSPerformance NewPerformance(string Name)
    {
#if PERFORMANCE
      return new iOSPerformance(Name);
#else
      return null;
#endif
    }
  }

#if PERFORMANCE
  internal sealed class iOSAccumulate
  {
    public long MeasureElapsedTicks;
    public long LayoutElapsedTicks;
  }
#endif

  internal sealed class iOSPerformance
  {
    public iOSPerformance(string Name)
    {
      this.Name = Name;
    }

    public string Name { get; }
    public int MeasureCount;
    public long MeasureElapsedTicks;
    public int LayoutCount;
    public long LayoutElapsedTicks;

    public void Clear()
    {
      this.MeasureCount = 0;
      this.MeasureElapsedTicks = 0;
      this.LayoutCount = 0;
      this.LayoutElapsedTicks = 0;
    }
  }

  internal sealed class iOSStopwatch
  {
    public iOSStopwatch(iOSPerformance Performance)
    {
      this.Base = new System.Diagnostics.Stopwatch();
      this.Performance = Performance;
    }

    public iOSPerformance Performance { get; }
    public long ElapsedTicks => Base.ElapsedTicks;

    public void Start()
    {
      Debug.Assert(!Base.IsRunning);

      Base.Restart();
    }
    public void Stop()
    {
      Debug.Assert(Base.IsRunning);

      Base.Stop();
    }

    private readonly System.Diagnostics.Stopwatch Base;
  }

  internal static class iOSFoundation
  {
    static iOSFoundation()
    {
      DefaultSystemFont = UIKit.UIFont.SystemFontOfSize(14);

      var CurrentDevice = UIKit.UIDevice.CurrentDevice;

      iOS15_0 = CurrentDevice.CheckSystemVersion(15, 0);
      iOS14_0 = CurrentDevice.CheckSystemVersion(14, 0);
      iOS13_3 = CurrentDevice.CheckSystemVersion(13, 3);
      iOS13_2 = CurrentDevice.CheckSystemVersion(13, 2);
      iOS13_0 = CurrentDevice.CheckSystemVersion(13, 0);
      iOS12_0 = CurrentDevice.CheckSystemVersion(12, 0);
      iOS11_0 = CurrentDevice.CheckSystemVersion(11, 0);
      iOS10_0 = CurrentDevice.CheckSystemVersion(10, 0);
      iOS9_0 = CurrentDevice.CheckSystemVersion(9, 0);
      iOS8_0 = CurrentDevice.CheckSystemVersion(8, 0);
    }

    public static readonly UIKit.UIFont DefaultSystemFont;
    public static readonly bool iOS15_0;
    public static readonly bool iOS14_0;
    public static readonly bool iOS13_3;
    public static readonly bool iOS13_2;
    public static readonly bool iOS13_0;
    public static readonly bool iOS12_0;
    public static readonly bool iOS11_0;
    public static readonly bool iOS10_0;
    public static readonly bool iOS9_0;
    public static readonly bool iOS8_0;

    public static void SafeAddView(this UIView ParentView, UIView ChildView)
    {
      if (ChildView.Superview != ParentView)
        ParentView.AddSubview(ChildView);
    }
    public static void SafeRemoveView(this UIView ParentView, UIView ChildView)
    {
      if (ChildView.Superview == ParentView)
        ChildView.RemoveFromSuperview();
    }

    public static CGSize FitToSize(this UIView iOSView, CGSize MaxSize)
    {
      var Result = MaxSize;
      if (Result.Width != UIView.NoIntrinsicMetric)
        Result.Width -= iOSView.LayoutMargins.Left + iOSView.LayoutMargins.Right;

      if (Result.Height != UIView.NoIntrinsicMetric)
        Result.Height -= iOSView.LayoutMargins.Top + iOSView.LayoutMargins.Bottom;

      return Result;
    }
    public static void Arrange(this UIView iOSView)
    {
      // NOTE: setting needs layout on all parents is necessary when a panel is hidden/removed from the visual tree.

      var iOSCurrent = iOSView;
      while (iOSCurrent != null)
      {
        iOSCurrent.SetNeedsLayout();

        iOSCurrent = iOSCurrent.Superview;
      }
    }
    public static void FadeIn(this UIView iOSView, double Duration, double Delay, Action CompleteAction, bool UserInteractions)
    {
      Fade(iOSView, MinAlpha, MaxAlpha, Duration, Delay, CompleteAction, UserInteractions);
    }
    public static void FadeOut(this UIView iOSView, double Duration, double Delay, Action CompleteAction, bool UserInteractions)
    {
      Fade(iOSView, MaxAlpha, MinAlpha, Duration, Delay, CompleteAction, UserInteractions);
    }
    public static void Fade(this UIView iOSView, nfloat FromAlpha, nfloat ToAlpha, double Duration, double Delay, Action CompleteAction, bool UserInteractions)
    {
      iOSView.Alpha = FromAlpha;
      UIView.Animate(Duration, Delay, UserInteractions ? UIViewAnimationOptions.CurveEaseInOut | UIViewAnimationOptions.AllowUserInteraction : UIViewAnimationOptions.CurveEaseInOut, WeakHelpers.WeakWrapper(iOSView, (iosView) =>
      {
        iosView.Alpha = ToAlpha;
      }), CompleteAction);
    }
    public static void Rotate(this UIView iOSView, nfloat FromAngle, nfloat ToAngle, double Duration, double Delay, Action CompleteAction, bool UserInteractions)
    {
      iOSView.Transform = CGAffineTransform.MakeRotation(FromAngle);

      var Options = UserInteractions ? UIViewAnimationOptions.CurveEaseInOut | UIViewAnimationOptions.AllowUserInteraction : UIViewAnimationOptions.CurveEaseInOut;

      if ((Math.Abs(FromAngle) < 0.000001F) && (Math.Abs(Math.Abs(ToAngle) - (Math.PI * 2)) < 0.000001F))
      {
        // With UIView animations, Core Animation computes the shortest path between the initial transform and the final transform.
        // The obvious 360° rotation doesn't work because the final transform is the same as the initial transform.
        UIView.Animate(Duration / 2, Delay, Options, WeakHelpers.WeakWrapper(iOSView, (iosView) =>
        {
          iosView.Transform = CGAffineTransform.MakeRotation(ToAngle / 2);
        }), () =>
        {
          UIView.Animate(Duration / 2, 0, Options, WeakHelpers.WeakWrapper(iOSView, (iosView) =>
          {
            iosView.Transform = CGAffineTransform.MakeRotation(ToAngle);
          }), CompleteAction);
        });
      }
      else
      {
        UIView.Animate(Duration, Delay, Options, WeakHelpers.WeakWrapper(iOSView, (iosView) =>
        {
          iosView.Transform = CGAffineTransform.MakeRotation(ToAngle);
        }), CompleteAction);
      }
    }
    public static void Scale(this UIView iOSView, nfloat FromX, nfloat ToX, nfloat FromY, nfloat ToY, double Duration, double Delay, Action CompleteAction, bool UserInteractions)
    {
      iOSView.Transform = CGAffineTransform.MakeScale(FromX, FromY);

      var Options = UserInteractions ? UIViewAnimationOptions.CurveEaseInOut | UIViewAnimationOptions.AllowUserInteraction : UIViewAnimationOptions.CurveEaseInOut;

      UIView.Animate(Duration, Delay, Options, WeakHelpers.WeakWrapper(iOSView, (iosView) =>
      {
        iosView.Transform = CGAffineTransform.MakeScale(ToX, ToY);
      }), CompleteAction);
    }
    public static void Translate(this UIView iOSView, nfloat FromX, nfloat ToX, nfloat FromY, nfloat ToY, double Duration, double Delay, Action CompleteAction, bool UserInteractions)
    {
      iOSView.Transform = CGAffineTransform.MakeTranslation(FromX, FromY);

      var Options = UserInteractions ? UIViewAnimationOptions.CurveEaseInOut | UIViewAnimationOptions.AllowUserInteraction : UIViewAnimationOptions.CurveEaseInOut;

      UIView.Animate(Duration, Delay, Options, WeakHelpers.WeakWrapper(iOSView, (iosView) =>
      {
        iosView.Transform = CGAffineTransform.MakeTranslation(ToX, ToY);
      }), CompleteAction);
    }
    public static float GetPreferredContentLightness(this UIView iOSView)
    {
      var ShadeStack = new Stack<Tuple<float, float>>();

      var Parent = iOSView;

      while (Parent != null)
      {
        if (Parent.BackgroundColor != null)
        {
          Parent.BackgroundColor.GetWhite(out nfloat White, out nfloat Alpha);

          ShadeStack.Push(new Tuple<float, float>((float)White, (float)Alpha));

          if (Alpha == 1f)
            break;
        }

        Parent = Parent.Superview;
      }

      var ResultShade = 0f;

      while (ShadeStack.Any())
      {
        var NextShade = ShadeStack.Pop();
        ResultShade = (ResultShade * (1 - NextShade.Item2)) + (NextShade.Item1 * NextShade.Item2);
      }

      if (ResultShade == 1f)
        return 0f;
      else if (ResultShade < 0.5f)
        return 1f;
      else
        return 0.5f;
    }
    public static string ChildHierarchyToString(this UIView RootView)
    {
      var Viewer = new HierarchyWriter<UIView>(V => V.Subviews.Length, V => V.Subviews);
      return Viewer.WriteToString(RootView);
    }
    public static UIView HitTestFindTouchable(this UIView View, UIView SuperHitTestResult)
    {
      if (SuperHitTestResult == View)
      {
        while (SuperHitTestResult != null)
        {
          SuperHitTestResult = SuperHitTestResult.Superview;

          if (SuperHitTestResult is ITouchable)
            return SuperHitTestResult;
        }

        return View;
      }

      return SuperHitTestResult;
    }
    public static UIView HitTestFindNothing(this UIView View, UIView SuperHitTestResult)
    {
      // ignore any user interaction on the layout view itself.
      return SuperHitTestResult == View ? null : SuperHitTestResult;
    }
    public static void DisableSafeAreaLayoutMargins(this UIView View)
    {
      if (iOSFoundation.iOS11_0)
        View.InsetsLayoutMarginsFromSafeArea = false;
    }

    public static float DegreesToRadians(float Angle)
    {
      return (float)(Angle * Math.PI / 180.0);
    }

    private static readonly nfloat MinAlpha = 0.0f;
    private static readonly nfloat MaxAlpha = 1.0f;
  }
}