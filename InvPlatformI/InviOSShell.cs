﻿#if DEBUG
//#define AUDIO_FILE // WIP.
#endif

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// The iOS implementation of the portable Invention application.
  /// </summary>
  public static class iOSShell
  {
    /// <summary>
    /// Show the iOS status bar.
    /// </summary>
    public static bool ShowStatusBar { get; set; }

    /// <summary>
    /// Run the portable application.
    /// </summary>
    /// <param name="ApplicationAction"></param>
    public static void RunBridge(Action<iOSBridge> ApplicationAction)
    {
      var Bridge = new iOSBridge();

      RunningApplication = Bridge.Application;
      AppDomain.CurrentDomain.UnhandledException += UnhandledException;
      System.Threading.Tasks.TaskScheduler.UnobservedTaskException += UnobservedTaskException;
      try
      {
        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

        Bridge.Engine.Run(() => ApplicationAction(Bridge));
      }
      finally
      {
        AppDomain.CurrentDomain.UnhandledException -= UnhandledException;
        System.Threading.Tasks.TaskScheduler.UnobservedTaskException -= UnobservedTaskException;
        RunningApplication = null;
      }
    }
    /// <summary>
    /// Run the portable application.
    /// </summary>
    /// <param name="ApplicationAction"></param>
    public static void Run(Action<Inv.Application> ApplicationAction)
    {
      RunBridge(B => ApplicationAction(B.Application));
    }
    /// <summary>
    /// Read an asset file into a string.
    /// </summary>
    /// <param name="AssetName"></param>
    /// <returns></returns>
    public static string ReadAssetString(string AssetName)
    {
      var AssetPath = Foundation.NSBundle.MainBundle.PathForResource(System.IO.Path.GetFileNameWithoutExtension(AssetName), System.IO.Path.GetExtension(AssetName));

      using (var AssetStream = new System.IO.FileStream(AssetPath, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read, 1024))
      using (var AssetReader = new System.IO.StreamReader(AssetStream))
        return AssetReader.ReadToEnd().Trim();
    }

    internal static string AppFolderPath() => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "..", "Library");

    private static void UnobservedTaskException(object Sender, System.Threading.Tasks.UnobservedTaskExceptionEventArgs Event)
    {
      var Application = RunningApplication;
      var Exception = Event.Exception;
      if (Application != null && Exception != null)
      {
        // A Task's exception(s) were not observed either by Waiting on the Task or accessing its Exception property. As a result, the unobserved exception was rethrown by the finalizer thread.
        // System.Net.WebException
        // The request was aborted: The request was canceled.
        //   at System.Net.HttpWebRequest+<MyGetResponseAsync>d__243.MoveNext () [0x003d5] in <38a52d63ed804d168e02e1c7dc43c55c>:0

        bool ShouldHandleException;

        if (Exception.InnerExceptions != null && Exception.InnerExceptions.Count == 0)
        {
          ShouldHandleException = true;
        }
        else
        {
          ShouldHandleException = false;

          foreach (var IndividualException in Exception.InnerExceptions)
          {
            var WebException = IndividualException as System.Net.WebException;

            if (WebException == null || WebException.Status != System.Net.WebExceptionStatus.RequestCanceled)
              ShouldHandleException = true;
          }
        }

        if (ShouldHandleException)
          Application.HandleExceptionInvoke(Exception);
      }

      Event.SetObserved();
    }
    private static void UnhandledException(object Sender, UnhandledExceptionEventArgs Event)
    {
      var Application = RunningApplication;
      var Exception = Event.ExceptionObject as Exception;
      if (Application != null && Exception != null)
        Application.HandleExceptionInvoke(Exception);
    }

    private static Application RunningApplication;
  }

  /// <summary>
  /// The bridge can be used to break the abstraction and reach into the implementation controls.
  /// This is for workarounds where Invention does not adequately handle a native scenario.
  /// </summary>
  public sealed class iOSBridge
  {
    internal iOSBridge()
    {
      this.Application = new Inv.Application();
      this.Engine = new iOSEngine(Application);
    }

    /// <summary>
    /// The portable application.
    /// </summary>
    public Inv.Application Application { get; private set; }

    /// <summary>
    /// Retrieve the <see cref="iOSSurface"/>
    /// </summary>
    /// <param name="InvSurface"></param>
    /// <returns></returns>
    public iOSSurface GetSurface(Inv.Surface InvSurface)
    {
      return Engine.TranslateSurface(InvSurface);
    }
    /// <summary>
    /// Retrieve the <see cref="iOSBlock"/>
    /// </summary>
    /// <param name="InvBlock"></param>
    /// <returns></returns>
    public iOSBlock GetBlock(Inv.Block InvBlock)
    {
      return Engine.TranslateBlock(InvBlock).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSBoard"/>
    /// </summary>
    /// <param name="InvBoard"></param>
    /// <returns></returns>
    public iOSBoard GetBoard(Inv.Board InvBoard)
    {
      return Engine.TranslateBoard(InvBoard).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSWebkitBrowser"/>
    /// </summary>
    /// <param name="InvBrowser"></param>
    /// <returns></returns>
    public iOSBrowser GetBrowser(Inv.Browser InvBrowser)
    {
      return Engine.TranslateBrowser(InvBrowser).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSButton"/>
    /// </summary>
    /// <param name="InvButton"></param>
    /// <returns></returns>
    public iOSButton GetButton(Inv.Button InvButton)
    {
      return Engine.TranslateButton(InvButton).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSCanvas"/>
    /// </summary>
    /// <param name="InvCanvas"></param>
    /// <returns></returns>
    public iOSCanvas GetCanvas(Inv.Canvas InvCanvas)
    {
      return Engine.TranslateCanvas(InvCanvas).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSDock"/>
    /// </summary>
    /// <param name="InvDock"></param>
    /// <returns></returns>
    public iOSDock GetDock(Inv.Dock InvDock)
    {
      return Engine.TranslateDock(InvDock).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSEdit"/>
    /// </summary>
    /// <param name="InvEdit"></param>
    /// <returns></returns>
    public iOSElement GetEdit(Inv.Edit InvEdit)
    {
      return Engine.TranslateEdit(InvEdit).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSFlow"/>
    /// </summary>
    /// <param name="InvFlow"></param>
    /// <returns></returns>
    public iOSFlow GetFlow(Inv.Flow InvFlow)
    {
      return Engine.TranslateFlow(InvFlow).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSFrame"/>
    /// </summary>
    /// <param name="InvFrame"></param>
    /// <returns></returns>
    public iOSFrame GetFrame(Inv.Frame InvFrame)
    {
      return Engine.TranslateFrame(InvFrame).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSGraphic"/>
    /// </summary>
    /// <param name="InvGraphic"></param>
    /// <returns></returns>
    public iOSGraphic GetGraphic(Inv.Graphic InvGraphic)
    {
      return Engine.TranslateGraphic(InvGraphic).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSLabel"/>
    /// </summary>
    /// <param name="InvLabel"></param>
    /// <returns></returns>
    public iOSLabel GetLabel(Inv.Label InvLabel)
    {
      return Engine.TranslateLabel(InvLabel).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSMemo"/>
    /// </summary>
    /// <param name="InvMemo"></param>
    /// <returns></returns>
    public iOSMemo GetMemo(Inv.Memo InvMemo)
    {
      return Engine.TranslateMemo(InvMemo).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSOverlay"/>
    /// </summary>
    /// <param name="InvOverlay"></param>
    /// <returns></returns>
    public iOSOverlay GetOverlay(Inv.Overlay InvOverlay)
    {
      return Engine.TranslateOverlay(InvOverlay).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSScroll"/>
    /// </summary>
    /// <param name="InvScroll"></param>
    /// <returns></returns>
    public iOSScroll GetScroll(Inv.Scroll InvScroll)
    {
      return Engine.TranslateScroll(InvScroll).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSShape"/>
    /// </summary>
    /// <param name="InvShape"></param>
    /// <returns></returns>
    public iOSShape GetShape(Inv.Shape InvShape)
    {
      return Engine.TranslateShape(InvShape).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSStack"/>
    /// </summary>
    /// <param name="InvStack"></param>
    /// <returns></returns>
    public iOSStack GetStack(Inv.Stack InvStack)
    {
      return Engine.TranslateStack(InvStack).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSTable"/>
    /// </summary>
    /// <param name="InvTable"></param>
    /// <returns></returns>
    public iOSTable GetTable(Inv.Table InvTable)
    {
      return Engine.TranslateTable(InvTable).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSVideo"/>
    /// </summary>
    /// <param name="InvVideo"></param>
    /// <returns></returns>
    public iOSVideo GetVideo(Inv.Video InvVideo)
    {
      return Engine.TranslateVideo(InvVideo).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="iOSWrap"/>
    /// </summary>
    /// <param name="InvWrap"></param>
    /// <returns></returns>
    public iOSWrap GetWrap(Inv.Wrap InvWrap)
    {
      return Engine.TranslateWrap(InvWrap).Element;
    }

    internal iOSEngine Engine { get; private set; }
  }

  internal sealed class iOSPlatform : Inv.Platform
  {
    public iOSPlatform(iOSEngine Engine)
    {
      this.Engine = Engine;
      this.Vault = new iOSVault();
      this.CurrentProcess = System.Diagnostics.Process.GetCurrentProcess();

      // NOTE: required to workaround AVAudioPlayer problems.
      ObjCRuntime.Class.ThrowOnInitFailure = false;

      // This allows music from other apps to continue playing.
      AVFoundation.AVAudioSession.SharedInstance().SetCategory(AVFoundation.AVAudioSession.CategoryAmbient);
    }

    int Platform.ThreadAffinity()
    {
      return System.Threading.Thread.CurrentThread.ManagedThreadId;
    }
    string Platform.CalendarTimeZoneName()
    {
      return Foundation.NSTimeZone.LocalTimeZone.Name;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      Engine.ShowCalendarPicker(CalendarPicker);
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      var Result = MessageUI.MFMailComposeViewController.CanSendMail;

      if (Result)
      {
        this.iOSMailController = new MessageUI.MFMailComposeViewController();
        iOSMailController.SetToRecipients(EmailMessage.GetTos().Select(T => T.Address).ToArray());
        iOSMailController.SetSubject(EmailMessage.Subject ?? "");
        iOSMailController.SetMessageBody(EmailMessage.Body ?? "", false);

        foreach (var Attachment in EmailMessage.GetAttachments())
        {
          var FileData = Foundation.NSData.FromFile(Engine.SelectFilePath(Attachment.File));

          // TODO: better mime type analysis.
          string MimeType;

          switch (Attachment.File.Extension.ToLower())
          {
            case ".log":
            case ".txt":
              MimeType = "text/plain";
              break;

            case ".png":
              MimeType = "image/png";
              break;

            case ".jpg":
            case ".jpeg":
              MimeType = "image/jpeg";
              break;

            default:
              MimeType = "application/octect-stream";
              break;
          }

          iOSMailController.AddAttachmentData(FileData, MimeType, Attachment.File.Name);
        }

        iOSMailController.Finished += (Sender, Event) =>
        {
          Console.WriteLine(Event.Result.ToString());
          Event.Controller.DismissViewController(animated: true, completionHandler: null);

          if (iOSMailController != null)
          {
            iOSMailController.Dispose();
            this.iOSMailController = null;
          }
        };
        Engine.iOSRootController.PresentViewController(iOSMailController, animated: true, completionHandler: null);
      }

      return Result;
    }
    bool Platform.PhoneIsSupported
    {
      get => UIKit.UIApplication.SharedApplication.CanOpenUrl(new Foundation.NSUrl("tel:"));
    }
    void Platform.PhoneDial(string PhoneNumber)
    {
      if (!Engine.OpenUri(new Uri("tel:" + PhoneNumber.Strip(' '))))
        Engine.Toast(null, "Unable to dial " + PhoneNumber, "OK");
    }
    void Platform.PhoneSMS(string PhoneNumber)
    {
      if (!Engine.OpenUri(new Uri("sms:" + PhoneNumber.Strip(' '))))
        Engine.Toast(null, "Unable to SMS " + PhoneNumber, "OK");
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      return new System.IO.FileInfo(Engine.SelectFilePath(File)).Length;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      return new System.IO.FileInfo(Engine.SelectFilePath(File)).LastWriteTimeUtc;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      System.IO.File.SetLastWriteTimeUtc(Engine.SelectFilePath(File), Timestamp);
    }
    Stream Platform.DirectoryCreateFile(File File)
    {
      return new System.IO.FileStream(Engine.SelectFilePath(File), FileMode.Create, FileAccess.Write, FileShare.Read, 65536);
    }
    Stream Platform.DirectoryAppendFile(File File)
    {
      return new System.IO.FileStream(Engine.SelectFilePath(File), FileMode.Append, FileAccess.Write, FileShare.Read, 65536);
    }
    Stream Platform.DirectoryOpenFile(File File)
    {
      return new System.IO.FileStream(Engine.SelectFilePath(File), FileMode.Open, FileAccess.Read, FileShare.Read, 65536);
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      return System.IO.File.Exists(Engine.SelectFilePath(File));
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      System.IO.File.Delete(Engine.SelectFilePath(File));
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Copy(Engine.SelectFilePath(SourceFile), Engine.SelectFilePath(TargetFile));
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Move(Engine.SelectFilePath(SourceFile), Engine.SelectFilePath(TargetFile));
    }
    void Platform.DirectoryReplaceFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Replace(Engine.SelectFilePath(SourceFile), Engine.SelectFilePath(TargetFile), destinationBackupFileName: null);
    }
    IEnumerable<Folder> Platform.DirectoryGetFolderSubfolders(Folder Folder, string FolderMask)
    {
      return new DirectoryInfo(Engine.SelectFolderPath(Folder)).GetDirectories(FolderMask).Select(F => Folder.NewFolder(F.Name));
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      return new DirectoryInfo(Engine.SelectFolderPath(Folder)).GetFiles(FileMask).Select(F => Folder.NewFile(F.Name));
    }
    IEnumerable<Asset> Platform.DirectoryGetAssets(string AssetMask)
    {
      return new DirectoryInfo(Foundation.NSBundle.MainBundle.ResourcePath).GetFiles(AssetMask).Select(F => new Asset(Engine.InvApplication.Directory, F.Name));
    }
    long Platform.DirectoryGetLengthAsset(Asset Asset)
    {
      return new System.IO.FileInfo(Engine.SelectAssetPath(Asset)).Length;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcAsset(Asset Asset)
    {
      return new System.IO.FileInfo(Engine.SelectAssetPath(Asset)).LastWriteTimeUtc;
    }
    Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      return new System.IO.FileStream(Engine.ResolveAssetPath(Asset), System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read, 65536);
    }
    bool Platform.DirectoryExistsAsset(Asset Asset)
    {
      var Result = Engine.SelectAssetPath(Asset);

      return Result != null && System.IO.File.Exists(Result);
    }
    void Platform.DirectoryShowFilePicker(DirectoryFilePicker FilePicker)
    {
      switch (FilePicker.PickType)
      {
        case PickType.Sound:
        case PickType.Any:
          var AllowedUTIs = new string[] { MobileCoreServices.UTType.Item }; // this is the root for all types.
          if (FilePicker.PickType == PickType.Sound)
            AllowedUTIs = new string[] { MobileCoreServices.UTType.Audio };

          // TODO: iOS 8.4 seems to have trouble just creating the UIDocumentPickerViewController.
          // Objective-C exception thrown.  Name: NSInternalInconsistencyException Reason: Application initializing document picker is missing the iCloud entitlement. Is com.apple.developer.icloud-container-identifiers set?

          var AnyFilePicker = new UIKit.UIDocumentPickerViewController(AllowedUTIs, UIKit.UIDocumentPickerMode.Import);
          if (iOSFoundation.iOS11_0)
          {
            //AnyFilePicker.AllowsMultipleSelection = false; // assumed to be false by default (also introduced in iOS 11.0+).
            AnyFilePicker.DidPickDocumentAtUrls += WeakHelpers.WeakEventHandlerWrapper<iOSEngine, Inv.DirectoryFilePicker, UIKit.UIDocumentPickedAtUrlsEventArgs>(Engine, FilePicker, (engine, filePicker, DocumentSender, DocumentEvent) =>
            {
              if (DocumentEvent.Urls.Length == 1)
                SelectFile(engine, filePicker, DocumentEvent.Urls[0]);
              else
                throw new NotImplementedException();
            });
          }
          else
          {
            AnyFilePicker.DidPickDocument += WeakHelpers.WeakEventHandlerWrapper<iOSEngine, Inv.DirectoryFilePicker, UIKit.UIDocumentPickedEventArgs>(Engine, FilePicker, (engine, filePicker, DocumentSender, DocumentEvent) =>
            {
              SelectFile(engine, filePicker, DocumentEvent.Url);
            });
          }
          AnyFilePicker.WasCancelled += WeakHelpers.WeakEventHandlerWrapper(Engine, FilePicker, (engine, filePicker, DocumentSender, DocumentEvent) =>
          {
            engine.Guard(() => filePicker.CancelInvoke());
          });

          Engine.iOSRootController.PresentViewController(AnyFilePicker, animated: true, completionHandler: null);
          break;

        case PickType.Image:
          var Permitted = Foundation.NSBundle.MainBundle.ObjectForInfoDictionary("NSPhotoLibraryUsageDescription");

          if (Permitted == null)
          {
            Engine.Toast(null, "The picture library cannot be accessed without the required permission. " +
#if DEBUG
              "Please install this key into your Info.plist: " +
              "<key>NSPhotoLibraryUsageDescription</key> " +
              "<string>$(PRODUCT_NAME) needs access to use your photo library</string>"
#else
              "Please contact the developer for assistance."
#endif
            , "OK");
            return;
          }

          var ImagePicker = new UIKit.UIImagePickerController();
          ImagePicker.SourceType = UIKit.UIImagePickerControllerSourceType.PhotoLibrary;
          //ImagePicker.MediaTypes = UIKit.UIImagePickerController.AvailableMediaTypes(UIKit.UIImagePickerControllerSourceType.PhotoLibrary); // NOTE: default is a 'still image' only.
          ImagePicker.FinishedPickingMedia += WeakHelpers.WeakEventHandlerWrapper<iOSEngine, DirectoryFilePicker, UIKit.UIImagePickerController, UIKit.UIImagePickerMediaPickedEventArgs>(Engine, FilePicker, ImagePicker, (engine, filePicker, imagePicker, Sender, Event) =>
          {
            try
            {
              var ReferenceUrl = Event.Info[UIKit.UIImagePickerController.ReferenceUrl] as Foundation.NSUrl;
              var iOSImage = Event.Info[UIKit.UIImagePickerController.OriginalImage] as UIKit.UIImage;
              if (iOSImage != null)
              {
                var PickName = ReferenceUrl != null ? System.IO.Path.GetFileName(ReferenceUrl.Path) : "";
                var PickPNG = iOSImage.AsPNG(); // will return null if there was an error, rather than an exception.
                if (PickPNG != null)
                  engine.Guard(() => filePicker.SelectInvoke(new Pick(PickName, () => new MemoryStream(PickPNG.ToArray()))));
              }

              imagePicker.DismissModalViewController(animated: true);
            }
            catch (Exception Exception)
            {
              engine.Guard(() =>
              {
                engine.InvApplication.HandleExceptionInvoke(Exception);

                filePicker.CancelInvoke(); // the select failed.
              });
            }
          });
          ImagePicker.Canceled += WeakHelpers.WeakEventHandlerWrapper<iOSEngine, DirectoryFilePicker, UIKit.UIImagePickerController>(Engine, FilePicker, ImagePicker, (engine, filePicker, imagePicker, Sender, Event) =>
          {
            try
            {
              Engine.Guard(FilePicker.CancelInvoke);

              imagePicker.DismissModalViewController(animated: true);
            }
            catch (Exception Exception)
            {
              Engine.Guard(() =>
              {
                Engine.InvApplication.HandleExceptionInvoke(Exception);
              });
            }
          });

          Engine.iOSRootController.PresentViewController(ImagePicker, animated: true, completionHandler: null);
          break;

        default:
          throw EnumHelper.UnexpectedValueException(FilePicker.PickType);
      }
    }
    string Platform.DirectoryGetFolderPath(Inv.Folder Folder)
    {
      return Engine.SelectFolderPath(Folder);
    }
    string Platform.DirectoryGetFilePath(Inv.File File)
    {
      return Engine.SelectFilePath(File);
    }
    bool Platform.LocationIsSupported
    {
      get => CoreLocation.CLLocationManager.LocationServicesEnabled;
    }
    void Platform.LocationShowMap(string Location)
    {
      if (!Engine.OpenUri(new Uri(@"http://maps.apple.com/?q=" + Location.Replace(' ', '+'))))
        Engine.Toast(null, "Unable to launch maps for '" + Location + "'", "OK");
    }
    void Platform.LocationLookup(LocationResult LocationLookup)
    {
      using (var iOSGeocoder = new CoreLocation.CLGeocoder())
      {
        iOSGeocoder.ReverseGeocodeLocation(new CoreLocation.CLLocation(LocationLookup.Coordinate.Latitude, LocationLookup.Coordinate.Longitude), (PlacemarkArray, Error) =>
        {
          Engine.Guard(() => LocationLookup.SetPlacemarks(PlacemarkArray.Select(P => new Inv.LocationPlacemark
          (
            Name: P.Name,
            Locality: P.Locality,
            SubLocality: P.SubLocality,
            PostalCode: P.PostalCode,
            AdministrativeArea: P.AdministrativeArea,
            SubAdministrativeArea: P.SubAdministrativeArea,
            CountryName: P.Country,
            CountryCode: P.IsoCountryCode,
            Latitude: P.Location.Coordinate.Latitude,
            Longitude: P.Location.Coordinate.Longitude
          ))));
        });
      }
    }
    void Platform.AudioPlaySound(Inv.Sound Sound, float Volume, float Rate, float Pan)
    {
      Engine.PlaySound(Sound, Volume, Rate, Pan, Loop: false, OneShot: true);
    }
    TimeSpan Platform.AudioGetSoundLength(Inv.Sound Sound)
    {
      return Engine.GetSoundLength(Sound);
    }
    void Platform.AudioReclaim(IReadOnlyList<Inv.Sound> SoundList)
    {
      Engine.ReclaimSound(SoundList);
    }
    void Platform.AudioPlayClip(AudioClip Clip)
    {
      Clip.Node = Engine.PlaySound(Clip.Sound, Clip.Volume, Clip.Rate, Clip.Pan, Clip.Looped, OneShot: false);
    }
    void Platform.AudioPauseClip(AudioClip Clip)
    {
      var iOSSound = Clip.Node as AVFoundation.AVAudioPlayer;
      if (iOSSound != null)
        iOSSound.Pause();
    }
    void Platform.AudioResumeClip(AudioClip Clip)
    {
      var iOSSound = Clip.Node as AVFoundation.AVAudioPlayer;
      if (iOSSound != null)
        iOSSound.Play();
    }
    void Platform.AudioStopClip(AudioClip Clip)
    {
      var iOSSound = Clip.Node as AVFoundation.AVAudioPlayer;
      Clip.Node = null;

      if (iOSSound != null)
        iOSSound.Stop();
    }
    void Platform.AudioModulateClip(AudioClip Clip)
    {
      var iOSSound = Clip.Node as AVFoundation.AVAudioPlayer;
      if (iOSSound != null)
        Engine.AdjustSound(iOSSound, Clip.Volume, Clip.Rate, Clip.Pan, Clip.Looped);
    }
    void Platform.AudioStartSpeechRecognition(Inv.SpeechRecognition SpeechRecognition)
    {
      if (iOSSpeechRecognition.IsSupported)
      {
        var iOSSpeechRecognition = SpeechRecognition.Node as iOSSpeechRecognition;
        if (iOSSpeechRecognition == null)
        {
          iOSSpeechRecognition = new iOSSpeechRecognition();
          SpeechRecognition.Node = iOSSpeechRecognition;

          iOSSpeechRecognition.TranscriptionEvent += (iOSTranscription, IsFinal) =>
          {
            var InvTranscription = new SpeechTranscription()
            {
              IsFinal = IsFinal,
              Text = iOSTranscription.FormattedString
            };
            SpeechRecognition.TranscriptionInvoke(InvTranscription);
          };
        }

#if AUDIO_FILE
        var iOSAudioUrl = new Foundation.NSUrl(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "test.caf"));
        if (System.IO.File.Exists(iOSAudioUrl.ToString()))
        {
          Debug.WriteLine("FileLength: " + new System.IO.FileInfo(iOSAudioUrl.ToString()).Length);

          var iOSDocumentInteractionController = new UIKit.UIDocumentInteractionController();
          iOSDocumentInteractionController.ViewControllerForPreview = (Controller) => Engine.iOSRootController; // this line fixes this error when you use Quick Look: *** Terminating app due to uncaught exception 'NSInternalInconsistencyException', reason: 'documentInteractionControllerViewControllerForPreview: must return a view controller'
          iOSDocumentInteractionController.DidDismissOptionsMenu += (s, a) => iOSDocumentInteractionController = null;

          iOSDocumentInteractionController.Url = new Uri("file://" + iOSAudioUrl.ToString());
          iOSDocumentInteractionController.PresentOptionsMenu(new CoreGraphics.CGRect(0, Engine.iOSWindow.Bounds.Height - 300, Engine.iOSWindow.Bounds.Width, 300), Engine.iOSRootController.View, animated: true);

          return;
        }
#endif

        iOSSpeechRecognition.Start();
      }
    }
    void Platform.AudioStopSpeechRecognition(Inv.SpeechRecognition SpeechRecognition)
    {
      var iOSSpeechRecognition = SpeechRecognition.Node as iOSSpeechRecognition;
      if (iOSSpeechRecognition != null)
        iOSSpeechRecognition.Stop();
    }
    bool Platform.AudioSpeechRecognitionIsSupported
    {
      get => iOSSpeechRecognition.IsSupported;
    }
    void Platform.WindowBrowse(Inv.File File)
    {
      Engine.InteractDocument(File, TemporaryCopy: false);
    }
    void Platform.WindowShare(Inv.File File)
    {
      Engine.InteractDocument(File, TemporaryCopy: true);
    }
    void Platform.WindowPost(Action Action)
    {
      Engine.Post(Action);
    }
    void Platform.WindowCall(Action Action)
    {
      Engine.Call(Action);
    }
    Inv.Dimension Platform.WindowGetDimension(Inv.Panel Panel)
    {
      var Result = Engine.GetContainer(Panel);

      return Result == null || Result.ContentElement == null ? Inv.Dimension.Zero : new Dimension((int)Result.ContentElement.View.Frame.Width, (int)Result.ContentElement.View.Frame.Height);
    }
    void Platform.WindowStartAnimation(Inv.Animation Animation)
    {
      Engine.StartAnimation(Animation);
    }
    void Platform.WindowStopAnimation(Inv.Animation Animation)
    {
      Engine.StopAnimation(Animation);
    }
    void Platform.WindowShowPopup(Inv.Popup Popup)
    {
      Engine.ShowPopup(Popup);
    }
    void Platform.WindowHidePopup(Inv.Popup Popup)
    {
      Engine.HidePopup(Popup);
    }
    long Platform.ProcessMemoryUsedBytes()
    {
      // NOTE: CurrentProcess.Refresh() doesn't seem to be required here on iOS.
      return 0L;// TODO: CurrentProcess.PrivateMemorySize64; // Operation is not supported on this platform.
    }
    string Platform.ProcessAncillaryInformation()
    {
      return Engine.IsRunningOnMac ? "(Running on Mac)" : "";
    }
    void Platform.WebClientConnect(WebClient WebClient)
    {
      var TcpClient = new Inv.Tcp.Client(WebClient.Host, WebClient.Port);
      TcpClient.Connect();

      WebClient.Node = TcpClient;
      WebClient.SetTransportStream(TcpClient.Stream);
    }
    void Platform.WebClientDisconnect(WebClient WebClient)
    {
      var TcpClient = (Inv.Tcp.Client)WebClient.Node;
      if (TcpClient != null)
      {
        WebClient.Node = null;
        WebClient.SetTransportStream(null);

        TcpClient.Disconnect();
      }
    }
    void Platform.WebServerConnect(WebServer WebServer)
    {
      var TcpServer = new Inv.Tcp.Server(WebServer.Host, WebServer.Port);
      TcpServer.AcceptEvent += (TcpChannel) => WebServer.AcceptChannel(TcpChannel, TcpChannel.Stream);
      TcpServer.RejectEvent += (TcpChannel) => WebServer.RejectChannel(TcpChannel);

      WebServer.Node = TcpServer;
      WebServer.DropDelegate = (Node) => ((Inv.Tcp.Channel)Node).Drop();

      TcpServer.Connect();
    }
    void Platform.WebServerDisconnect(WebServer WebServer)
    {
      var TcpServer = (Inv.Tcp.Server)WebServer.Node;
      if (TcpServer != null)
      {
        TcpServer.Disconnect();

        WebServer.Node = null;
        WebServer.DropDelegate = null;
      }
    }
    void Platform.WebBroadcastConnect(WebBroadcast WebBroadcast)
    {
      var UdpBroadcast = new Inv.Udp.Broadcast();
      WebBroadcast.Node = UdpBroadcast;
      WebBroadcast.SetBroadcast(UdpBroadcast);
    }
    void Platform.WebBroadcastDisconnect(WebBroadcast WebBroadcast)
    {
      var UdpBroadcast = (Inv.Udp.Broadcast)WebBroadcast.Node;
      if (UdpBroadcast != null)
      {
        UdpBroadcast.Close();

        WebBroadcast.Node = null;
        WebBroadcast.SetBroadcast(null);
      }
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      Engine.OpenUri(Uri);
    }
    void Platform.WebInstallUri(Uri Uri)
    {
      // prompts the user with an AlertView to CANCEL/INSTALL the .ipa.
      if (Engine.OpenUri(new Uri("itms-services://?action=download-manifest&url=" + Uri.AbsoluteUri)))
      {
        // TODO: iOS13 doesn't seem to be ending the app and installing the new one automatically (the user has to force-quit to work around the issue?).

        //Engine.Terminate(); // TODO: this might also work but assumes we are reinstalling ourself... and cancel doesn't do anything.
      }
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      Engine.OpenUri(new Uri("itms-apps://itunes.apple.com/app/id" + AppleiTunesID));
    }
    void Platform.VaultLoadSecret(Secret Secret)
    {
      Vault.Load(Secret);
    }
    void Platform.VaultSaveSecret(Secret Secret)
    {
      Vault.Save(Secret);
    }
    void Platform.VaultDeleteSecret(Secret Secret)
    {
      Vault.Delete(Secret);
    }
    bool Platform.ClipboardIsTextSupported
    {
      get => true;
    }
    string Platform.ClipboardGetText()
    {
      return UIKit.UIPasteboard.General.String;
    }
    void Platform.ClipboardSetText(string Text)
    {
      if (Text == null)
        UIKit.UIPasteboard.General.Items = Array.Empty<Foundation.NSDictionary>();
      else
        UIKit.UIPasteboard.General.String = Text; // does not accept null strings.
    }
    bool Platform.ClipboardIsImageSupported
    {
      get => true;
    }
    Inv.Image Platform.ClipboardGetImage()
    {
      return ConvertToPngInvImage(UIKit.UIPasteboard.General.Image);
    }
    void Platform.ClipboardSetImage(Inv.Image Image)
    {
      if (Image == null)
        UIKit.UIPasteboard.General.Items = Array.Empty<Foundation.NSDictionary>();
      else
        UIKit.UIPasteboard.General.Image = Engine.TranslateUIImage(Image);
    }
    void Platform.HapticFeedback(HapticFeedback Feedback)
    {
      Engine.HapticFeedback(Feedback);
    }
    Inv.Dimension Platform.GraphicsGetDimension(Inv.Image Image)
    {
      var Result = Engine.TranslateUIImage(Image);

      return Result == null ? 
        Inv.Dimension.Zero : 
        new Inv.Dimension((int)Result.Size.Width, (int)Result.Size.Height);
    }
    Inv.Image Platform.GraphicsGrayscale(Inv.Image Image)
    {
      var iOSImage = Engine.TranslateUIImage(Image);

      var iOSImageWidth = iOSImage.CGImage.Width;
      var iOSImageHeight = iOSImage.CGImage.Height;
      var iOSImageRect = new CoreGraphics.CGRect(0, 0, iOSImageWidth, iOSImageHeight);

      using (var iOSColorSpace = CoreGraphics.CGColorSpace.CreateDeviceGray())
      using (var iOSColorContext = new CoreGraphics.CGBitmapContext(IntPtr.Zero, iOSImageWidth, iOSImageHeight, 8, 0, iOSColorSpace, CoreGraphics.CGImageAlphaInfo.None))
      using (var iOSAlphaContext = new CoreGraphics.CGBitmapContext(IntPtr.Zero, iOSImageWidth, iOSImageHeight, 8, 0, null, CoreGraphics.CGImageAlphaInfo.Only))
      {
        iOSColorContext.DrawImage(iOSImageRect, iOSImage.CGImage);
        iOSAlphaContext.DrawImage(iOSImageRect, iOSImage.CGImage);

        using (var iOSColorRef = iOSColorContext.ToImage())
        using (var iOSAlphaRef = iOSAlphaContext.ToImage())
        using (var iOSCombineImage = iOSColorRef.WithMask(iOSAlphaRef))
        using (var iOSResultImage = new UIKit.UIImage(iOSCombineImage))
          return ConvertToPngInvImage(iOSResultImage);
      }
    }
    Inv.Image Platform.GraphicsTint(Inv.Image Image, Inv.Colour Colour)
    {
      var iOSImage = Engine.TranslateUIImage(Image);
      var iOSTintColour = Engine.TranslateCGColor(Colour);
      var iOSImageRect = new CoreGraphics.CGRect(CoreGraphics.CGPoint.Empty, iOSImage.Size);

      UIKit.UIGraphics.BeginImageContext(iOSImage.Size);
      using (var iOSContext = UIKit.UIGraphics.GetCurrentContext())
      {
        iOSContext.TranslateCTM(0, iOSImage.Size.Height);
        iOSContext.ScaleCTM(1.0f, -1.0f);

        iOSContext.DrawImage(iOSImageRect, iOSImage.CGImage);

        iOSContext.ClipToMask(iOSImageRect, iOSImage.CGImage);
        iOSContext.SetFillColor(iOSTintColour);
        iOSContext.FillRect(iOSImageRect);

        var iOSResult = UIKit.UIGraphics.GetImageFromCurrentImageContext();
        UIKit.UIGraphics.EndImageContext();

        return ConvertToPngInvImage(iOSResult);
      }
    }
    Inv.Image Platform.GraphicsResize(Inv.Image Image, Inv.Dimension Dimension)
    {
      var iOSImage = Engine.TranslateUIImage(Image);
      var iOSDimension = new CoreGraphics.CGSize(Dimension.Width, Dimension.Height);

      UIKit.UIGraphics.BeginImageContextWithOptions(iOSDimension, false, 0.0F);
      iOSImage.Draw(new CoreGraphics.CGRect(CoreGraphics.CGPoint.Empty, iOSDimension));
      var iOSResult = UIKit.UIGraphics.GetImageFromCurrentImageContext();
      UIKit.UIGraphics.EndImageContext();

      return ConvertToPngInvImage(iOSResult);
    }
    Inv.Pixels Platform.GraphicsReadPixels(Inv.Image Image)
    {
      var iOSImage = Engine.TranslateUIImage(Image);

      var iOSPixelWidth = (int)iOSImage.CGImage.Width;
      var iOSPixelHeight = (int)iOSImage.CGImage.Height;
      var iOSPixelStride = iOSPixelWidth * Inv.RGBAByteArrayPixels.PixelSize;
      var iOSPixelArray = new byte[iOSPixelWidth * iOSPixelStride];
      var iOSImageRect = new CoreGraphics.CGRect(CoreGraphics.CGPoint.Empty, new CoreGraphics.CGSize(iOSPixelWidth, iOSPixelHeight));

      using (var iOSPixelSpace = CoreGraphics.CGColorSpace.CreateDeviceRGB())
      using (var iOSColorContext = new CoreGraphics.CGBitmapContext(iOSPixelArray, iOSPixelWidth, iOSPixelHeight, bitsPerComponent: 8, bytesPerRow: iOSPixelStride, iOSPixelSpace, CoreGraphics.CGImageAlphaInfo.PremultipliedLast))
        iOSColorContext.DrawImage(iOSImageRect, iOSImage.CGImage);

      return new Inv.RGBAByteArrayPixels(iOSPixelWidth, iOSPixelHeight, iOSPixelArray);
    }
    Inv.Image Platform.GraphicsWritePixels(Inv.Pixels Pixels)
    {
      var RGBAByteArrayPixels = (Pixels as Inv.RGBAByteArrayPixels) ?? new RGBAByteArrayPixels(Pixels);

      using (var iOSPixelSpace = CoreGraphics.CGColorSpace.CreateDeviceRGB())
      using (var iOSColorContext = new CoreGraphics.CGBitmapContext(RGBAByteArrayPixels.PixelArray, RGBAByteArrayPixels.Width, RGBAByteArrayPixels.Height, bitsPerComponent: 8, bytesPerRow: RGBAByteArrayPixels.PixelStride, iOSPixelSpace, CoreGraphics.CGImageAlphaInfo.PremultipliedLast))
      using (var iOSImage = new UIKit.UIImage(iOSColorContext.ToImage()))
        return ConvertToPngInvImage(iOSImage);
    }
    void Platform.GraphicsRender(Inv.Panel Panel, Action<Inv.Image> ReturnAction)
    {
      var iOSPanel = Engine.GetPanel(Panel);

      var Result = ConvertToPngInvImage(RenderViewImage(iOSPanel));
      try
      {
        ReturnAction(Result);
      }
      finally
      {
        Engine.ReclaimImage(new[] { Result });
      }
    }
    void Platform.GraphicsReclaim(IReadOnlyList<Inv.Image> ImageList)
    {
      Engine.ReclaimImage(ImageList);
    }
    Inv.Dimension Platform.CanvasCalculateText(string TextFragment, Inv.DrawFont TextFont, int MaximumTextWidth, int MaximumTextHeight)
    {
      using (var AttributedString = Engine.NewAttributedString(TextFragment, TextFont))
      {
        var TextRect = AttributedString.GetBoundingRect(new CoreGraphics.CGSize(MaximumTextWidth, MaximumTextHeight), Foundation.NSStringDrawingOptions.OneShot, null);

        return new Inv.Dimension((int)TextRect.Width, (int)TextRect.Height);
      }
    }

    private UIKit.UIImage RenderViewImage(UIKit.UIView iOSView)
    {
      UIKit.UIGraphics.BeginImageContextWithOptions(iOSView.Bounds.Size, iOSView.Opaque, scale: 1);
      iOSView.DrawViewHierarchy(iOSView.Frame, afterScreenUpdates: true);
      var iOSImage = UIKit.UIGraphics.GetImageFromCurrentImageContext();
      UIKit.UIGraphics.EndImageContext();

      return iOSImage;
    }
    private Inv.Image ConvertToPngInvImage(UIKit.UIImage iOSResult)
    {
      if (iOSResult == null)
        return null;

      using (var iOSData = iOSResult.AsPNG())
      {
        var InvBuffer = new byte[iOSData.Length];
        System.Runtime.InteropServices.Marshal.Copy(iOSData.Bytes, InvBuffer, 0, Convert.ToInt32(iOSData.Length));

        var InvResult = new Inv.Image(InvBuffer, ".png");

        // NOTE: don't keep the native image as it causes iOS 12.x to crash when rendering a grayscale tile with a tint colour on the canvas.
        //       below is the assertion that crashes the app:
        // Assertion failed: (0), function img_colormask_stage, file /BuildRoot/Library/Caches/com.apple.xbs/Sources/CoreGraphics/CoreGraphics-1245.8/CoreGraphics/Images/CGSImage.c, line 3273.
        // InvResult.Node = iOSResult;

        return InvResult;
      }
    }
    private void SelectFile(iOSEngine engine, DirectoryFilePicker filePicker, Foundation.NSUrl DocumentUrl)
    {
      try
      {
        // defensive in case the passed url is null.
        if (DocumentUrl == null)
        {
          engine.Guard(() => filePicker.CancelInvoke()); // the select failed.
          return;
        }

        try
        {
          DocumentUrl.StartAccessingSecurityScopedResource();

          var Data = Foundation.NSData.FromUrl(DocumentUrl);

          var Buffer = Data?.ToArray() ?? Array.Empty<byte>(); // defensive against possible null op.

          engine.Guard(() => filePicker.SelectInvoke(new Pick(DocumentUrl.LastPathComponent, () => new MemoryStream(Buffer))));
        }
        catch (Exception Exception)
        {
          engine.Guard(() =>
          {
            engine.InvApplication.HandleExceptionInvoke(Exception);

            filePicker.CancelInvoke(); // the select failed.
          });
        }
        finally
        {
          DocumentUrl.StopAccessingSecurityScopedResource();
        }
      }
      catch (Exception Exception)
      {
        engine.Guard(() => engine.InvApplication.HandleExceptionInvoke(Exception));
      }
    }

    private readonly iOSEngine Engine;
    private readonly System.Diagnostics.Process CurrentProcess;
    private readonly iOSVault Vault;
    private MessageUI.MFMailComposeViewController iOSMailController;
  }

  internal static class iOSKeyboard
  {
    static iOSKeyboard()
    {
      KeyDictionary = new Dictionary<string, Inv.Key>()
      {
        { "0", Inv.Key.n0 },
        { "1", Inv.Key.n1 },
        { "2", Inv.Key.n2 },
        { "3", Inv.Key.n3 },
        { "4", Inv.Key.n4 },
        { "5", Inv.Key.n5 },
        { "6", Inv.Key.n6 },
        { "7", Inv.Key.n7 },
        { "8", Inv.Key.n8 },
        { "9", Inv.Key.n9 },

        { "A", Inv.Key.A },
        { "B", Inv.Key.B },
        { "C", Inv.Key.C },
        { "D", Inv.Key.D },
        { "E", Inv.Key.E },
        { "F", Inv.Key.F },
        { "G", Inv.Key.G },
        { "H", Inv.Key.H },
        { "I", Inv.Key.I },
        { "J", Inv.Key.J },
        { "K", Inv.Key.K },
        { "L", Inv.Key.L },
        { "M", Inv.Key.M },
        { "N", Inv.Key.N },
        { "O", Inv.Key.O },
        { "P", Inv.Key.P },
        { "Q", Inv.Key.Q },
        { "R", Inv.Key.R },
        { "S", Inv.Key.S },
        { "T", Inv.Key.T },
        { "U", Inv.Key.U },
        { "V", Inv.Key.V },
        { "W", Inv.Key.W },
        { "X", Inv.Key.X },
        { "Y", Inv.Key.Y },
        { "Z", Inv.Key.Z },

        { " ", Inv.Key.Space },
        { ".", Inv.Key.Period },
        { ">", Inv.Key.Period },
        { ",", Inv.Key.Comma },
        { "<", Inv.Key.Comma },
        { "~", Inv.Key.Grave },
        { "`", Inv.Key.Grave },
        { "'", Inv.Key.Quote },
        { "\"", Inv.Key.Quote },
        { ":", Inv.Key.Colon },
        { ";", Inv.Key.Colon },
        { "\\", Inv.Key.Backslash },
        { "|", Inv.Key.Backslash },
        { "/", Inv.Key.Slash },
        { "?", Inv.Key.Slash },
        { "[", Inv.Key.OpenBracket },
        { "{", Inv.Key.OpenBracket },
        { "]", Inv.Key.CloseBracket },
        { "}", Inv.Key.CloseBracket },
        { "=", Inv.Key.Plus },
        { "+", Inv.Key.Plus },
        { "-", Inv.Key.Minus },
        { "_", Inv.Key.Minus },
        { "*", Inv.Key.Asterisk },
        { "\r", Inv.Key.Enter },
        { "\t", Inv.Key.Tab },
        { UIKit.UIKeyCommand.Escape, Inv.Key.Escape },
        { UIKit.UIKeyCommand.UpArrow, Inv.Key.Up },
        { UIKit.UIKeyCommand.DownArrow, Inv.Key.Down },
        { UIKit.UIKeyCommand.LeftArrow, Inv.Key.Left },
        { UIKit.UIKeyCommand.RightArrow, Inv.Key.Right },

        // UIKit.UIKeyCommand.F? was introduced iOS 13.4.
        { "F1", Inv.Key.F1 },
        { "F2", Inv.Key.F2 },
        { "F3", Inv.Key.F3 },
        { "F4", Inv.Key.F4 },
        { "F5", Inv.Key.F5 },
        { "F6", Inv.Key.F6 },
        { "F7", Inv.Key.F7 },
        { "F8", Inv.Key.F8 },
        { "F9", Inv.Key.F9 },
        { "F10", Inv.Key.F10 },
        { "F11", Inv.Key.F11 },
        { "F12", Inv.Key.F12 },

        // introduced iOS 13.4.
        { "UIKeyInputHome", Inv.Key.Home },
        { "UIKeyInputEnd", Inv.Key.End },
        { "UIKeyInputPageUp", Inv.Key.PageUp },
        { "UIKeyInputPageDown", Inv.Key.PageDown },

        // UIKit.UIKeyCommand.Delete was introduced iOS 15.0.
        { "\b", Inv.Key.Delete }
        //{ "UIKeyInputBackspace", Inv.Key.Backspace } // TODO: on MacOS: fn+Delete = Backspace... but what is the string?
      };
#if DEBUG
      var MissingKeySet = Inv.Support.EnumHelper.GetEnumerable<Inv.Key>().Except(KeyDictionary.Values).ToHashSet();
      if (MissingKeySet.Count > 0)
        System.Diagnostics.Debug.WriteLine("Unhandled keys: " + MissingKeySet.Select(K => K.ToString()).AsSeparatedText(", "));
#endif

      // TODO: DiscoverabilityTitle in iOS 9?

      UIKit.UIKeyCommand NewKeyCommand(string Key, UIKit.UIKeyModifierFlags Flags)
      {
        var Result = UIKit.UIKeyCommand.Create(new Foundation.NSString(Key), Flags, new ObjCRuntime.Selector("KeyDown:"));

        if (iOSFoundation.iOS15_0)
          Result.WantsPriorityOverSystemBehavior = true;

        return Result;
      }

      // NOTE: the "KeyDown:" selector is actually a private method on the iOSSurfaceController.
      var KeyList =
        KeyDictionary.Select(E => NewKeyCommand(E.Key, (UIKit.UIKeyModifierFlags)0)).Union(
        KeyDictionary.Select(E => NewKeyCommand(E.Key, UIKit.UIKeyModifierFlags.Shift)).Union(
        KeyDictionary.Select(E => NewKeyCommand(E.Key, UIKit.UIKeyModifierFlags.Control)).Union(
        KeyDictionary.Select(E => NewKeyCommand(E.Key, UIKit.UIKeyModifierFlags.Alternate))))).ToDistinctList();

      // TODO: page up/down, home/end keys - requires function modifier flag?
      //KeyList.Add(UIKit.UIKeyCommand.Create(new Foundation.NSString(UIKit.UIKeyCommand.UpArrow), UIKit.UIKeyModifierFlags.Fn, new ObjCRuntime.Selector("KeyDown:")));

      KeyCommandArray = KeyList.ToArray();
      BlankCommandArray = Array.Empty<UIKit.UIKeyCommand>();
    }

    public static Inv.Key TranslateKey(string iOSInput)
    {
      return KeyDictionary[iOSInput];
    }
    public static Inv.KeyModifier TranslateModifier(UIKit.UIKeyModifierFlags iOSModifierFlags)
    {
      var Result = new Inv.KeyModifier();

      if ((iOSModifierFlags & UIKit.UIKeyModifierFlags.Shift) != 0)
      {
        Result.IsLeftShift = true;
        Result.IsRightShift = true;
      }

      if ((iOSModifierFlags & UIKit.UIKeyModifierFlags.Control) != 0)
      {
        Result.IsLeftCtrl = true;
        Result.IsRightCtrl = true;
      }

      if ((iOSModifierFlags & UIKit.UIKeyModifierFlags.Alternate) != 0)
      {
        Result.IsLeftAlt = true;
        Result.IsRightAlt = true;
      }

      return Result;
    }

    public static readonly UIKit.UIKeyCommand[] KeyCommandArray;
    public static readonly UIKit.UIKeyCommand[] BlankCommandArray;

    private static readonly Dictionary<string, Inv.Key> KeyDictionary;
  }

  internal sealed class iOSEngine
  {
    internal iOSEngine(Inv.Application InvApplication)
    {
      this.InvApplication = InvApplication;
      //this.ColourList = new Inv.DistinctList<Inv.Colour>(16384); // 64KB memory.
      this.RouteArray = new Inv.EnumArray<Inv.ControlType, Func<Inv.Control, iOSNode>>()
      {
        { Inv.ControlType.Block, P => TranslateBlock((Inv.Block)P) },
        { Inv.ControlType.Board, P => TranslateBoard((Inv.Board)P) },
        { Inv.ControlType.Browser, P => TranslateBrowser((Inv.Browser)P) },
        { Inv.ControlType.Button, P => TranslateButton((Inv.Button)P) },
        { Inv.ControlType.Dock, P => TranslateDock((Inv.Dock)P) },
        { Inv.ControlType.Edit, P => TranslateEdit((Inv.Edit)P) },
        { Inv.ControlType.Flow, P => TranslateFlow((Inv.Flow)P) },
        { Inv.ControlType.Frame, P => TranslateFrame((Inv.Frame)P) },
        { Inv.ControlType.Graphic, P => TranslateGraphic((Inv.Graphic)P) },
        { Inv.ControlType.Label, P => TranslateLabel((Inv.Label)P) },
        { Inv.ControlType.Memo, P => TranslateMemo((Inv.Memo)P) },
        { Inv.ControlType.Overlay, P => TranslateOverlay((Inv.Overlay)P) },
        { Inv.ControlType.Canvas, P => TranslateCanvas((Inv.Canvas)P) },
        { Inv.ControlType.Scroll, P => TranslateScroll((Inv.Scroll)P) },
        { Inv.ControlType.Shape, P => TranslateShape((Inv.Shape)P) },
        { Inv.ControlType.Stack, P => TranslateStack((Inv.Stack)P) },
        { Inv.ControlType.Switch, P => TranslateSwitch((Inv.Switch)P) },
        { Inv.ControlType.Table, P => TranslateTable((Inv.Table)P) },
        { Inv.ControlType.Video, P => TranslateVideo((Inv.Video)P) },
        { Inv.ControlType.Wrap, P => TranslateWrap((Inv.Wrap)P) },
      };

      this.iOSFontFamilyArray = new Inv.EnumArray<Inv.FontWeight, string>()
      {
        { FontWeight.Thin, "-Thin" },
        { FontWeight.Light, "-Light" },
        { FontWeight.Regular, "" },
        { FontWeight.Medium, "-Medium" },
        { FontWeight.Bold, "-Bold" },
        { FontWeight.Heavy, "-Black" },
      };

      // NOTE: UltraLight, SemiBold and Heavy are not supported in all platforms.
      this.iOSFontWeightArray = new Inv.EnumArray<Inv.FontWeight, UIKit.UIFontWeight>()
      {
        { FontWeight.Thin, UIKit.UIFontWeight.Thin },
        { FontWeight.Light, UIKit.UIFontWeight.Light },
        { FontWeight.Regular, UIKit.UIFontWeight.Regular },
        { FontWeight.Medium, UIKit.UIFontWeight.Medium },
        { FontWeight.Bold, UIKit.UIFontWeight.Bold },
        { FontWeight.Heavy, UIKit.UIFontWeight.Black },
      };

      this.iOSImpactFeedbackArray = new Inv.EnumArray<UIKit.UIImpactFeedbackStyle, UIKit.UIImpactFeedbackGenerator>();

      //foreach (var Family in UIKit.UIFont.FamilyNames) Debug.WriteLine(Family);

      InvApplication.SetPlatform(new iOSPlatform(this));

      try
      {
        InvApplication.Version = Foundation.NSBundle.MainBundle.ObjectForInfoDictionary("CFBundleShortVersionString") + "." + Foundation.NSBundle.MainBundle.ObjectForInfoDictionary("CFBundleVersion");
      }
      catch (Exception Exception)
      {
        Debug.WriteLine(Exception.Message);

        InvApplication.Version = "?";
      }

      InvApplication.Directory.Installation = Foundation.NSBundle.MainBundle.BundleIdentifier;

      var CurrentDevice = UIKit.UIDevice.CurrentDevice;
      InvApplication.Device.Target = DeviceTarget.IOS;
      InvApplication.Device.Name = CurrentDevice.Name;
      InvApplication.Device.Model = iOSHardware.DeviceModel;
      InvApplication.Device.Manufacturer = "Apple";
      InvApplication.Device.System = "iOS " + CurrentDevice.SystemVersion;
      InvApplication.Device.Keyboard = false; // TODO: doesn't seem to have a reliable way to ask if there is a hardware keyboard connected.
      InvApplication.Device.Mouse = false; // TODO: is this a thing?  you can use mouse in the simulator.
      InvApplication.Device.Touch = true;
      InvApplication.Device.ProportionalFontName = "SanFrancisoText"; // NOTE: this UIKit.UIFont.SystemFontOfSize(10).Name; returns ".SFUIText"
      InvApplication.Device.MonospacedFontName = "Menlo";
      InvApplication.Device.PixelDensity = (float)UIKit.UIScreen.MainScreen.Scale;

      InvApplication.Process.Id = Foundation.NSProcessInfo.ProcessInfo.ProcessIdentifier;

      if (iOSFoundation.iOS14_0)
        this.IsRunningOnMac = Foundation.NSProcessInfo.ProcessInfo.IsiOSApplicationOnMac;
      else
        this.IsRunningOnMac = false;

      InvApplication.Window.NativePanelType = typeof(UIKit.UIView);

      InvApplication.Haptics.IsSupported = iOSFoundation.iOS10_0; // NOTE: UIFeedbackGenerator API was first introduced in iOS 10.

      if (InvApplication.Haptics.IsSupported)
      {
        // NOTE: This is a workaround because the iOS API does not have a supported way to ask if the device supports haptic feedback.
        try
        {
          using (var TestFeedback = new UIKit.UIImpactFeedbackGenerator(UIKit.UIImpactFeedbackStyle.Heavy))
          {
            TestFeedback.Prepare();
            var TestNumber = TestFeedback.DebugDescription ?? "";

            if (TestNumber.StartsWith("<UIImpactFeedbackGenerator:") && TestNumber.EndsWith("prepared=0>"))
              InvApplication.Haptics.IsSupported = false;
          }
        }
        catch
        {
          // ignore haptics engine failures.
        }
      }
    }

    public void Run(Action iOSInstall)
    {
      iOSAppDelegate.iOSEngine = this;
      iOSAppDelegate.iOSInstall = iOSInstall;
      try
      {
        UIKit.UIApplication.Main(Array.Empty<string>(), null, typeof(iOSAppDelegate));
      }
      finally
      {
        iOSAppDelegate.iOSInstall = null;
        iOSAppDelegate.iOSEngine = null;
      }
    }

#pragma warning disable IDE1006 // naming rule violation messages.
    internal iOSController iOSRootController { get; private set; }
    internal UIKit.UIWindow iOSWindow { get; private set; }
#pragma warning restore IDE1006
    internal Inv.Application InvApplication { get; private set; }
    internal bool IsRunningOnMac { get; private set; }
    internal bool IsTransitioning => TransitionCount > 0;
    internal bool IsInputBlocked => IsTransitioning || (InvApplication?.Window.InputPrevented ?? false);

    // NOTE: called from the AppDelegate.
    internal UIKit.UIWindow Launching(Action iOSInstall)
    {
      Guard(() =>
      {
        // NOTE: using iOSWindow.Bounds will work in 9.x but not work in 8.x (it will cause the view to be offset from the middle of the screen).
        this.iOSWindow = new UIKit.UIWindow();

        RefreshWindowSize();

        iOSWindow.BackgroundColor = UIKit.UIColor.Black;
        iOSWindow.MakeKeyAndVisible();

        this.iOSRootController = new iOSController();
        iOSWindow.RootViewController = iOSRootController;

        InvApplication.Device.Theme = GetOperatingSystemTheme();

        iOSInstall();

        try
        {
          InvApplication.StartInvoke();
        }
        catch (Exception Exception)
        {
          InvApplication.HandleExceptionInvoke(Exception);

          // NOTE: can't let the application start if the StartEvent failed so have the application crash instead.
          throw Exception.Preserve();
        }

        var InvWindow = InvApplication.Window;
        var InvKeyboard = InvApplication.Keyboard;
        var InvAccessibility = InvWindow.Accessibility;

        iOSRootController.KeyQuery += () =>
        {
          if (IsInputBlocked)
            return iOSKeyboard.BlankCommandArray;
          else
            return iOSKeyboard.KeyCommandArray;
        };
        iOSRootController.KeyEvent += (Modifier, Key) =>
        {
          // TODO: track key up/down so we can track ctrl/alt/shift state with Window.KeyModifier.

          InvApplication.Keyboard.KeyPress(new Inv.Keystroke(iOSKeyboard.TranslateKey(Key), iOSKeyboard.TranslateModifier(Modifier)));
        };
        iOSRootController.MemoryWarningEvent += () =>
        {
          // TODO: Process.WarningEvent?
        };
        iOSRootController.ResizeEvent += (Size) =>
        {
          //if (IsRunningOnMac) return;

          Post(() =>
          {
            InvWindow.Width = (int)Size.Width;
            InvWindow.Height = (int)Size.Height;

            InvWindow.ActiveSurface?.ArrangeInvoke();
          });
        };
        iOSRootController.LeftEdgeSwipeEvent += () =>
        {
          Guard(() =>
          {
            InvWindow.ActiveSurface?.GestureBackwardInvoke();
          });
        };
        iOSRootController.RightEdgeSwipeEvent += () =>
        {
          Guard(() =>
          {
            InvWindow.ActiveSurface?.GestureForwardInvoke();
          });
        };

        iOSRootController.LoadEvent += () =>
        {
          Guard(() =>
          {
            var InvSurface = InvWindow.ActiveSurface;
            if (InvSurface != null)
            {
              var iOSSurface = TranslateSurface(InvSurface);

              if ((iOSRootController.Surface != iOSSurface && InvWindow.ActiveTransition != null)/* || (IsRunningOnMac && RefreshWindowSize())*/)
                InvSurface.ArrangeInvoke();

              ProcessTransition(iOSSurface);

              InvSurface.ComposeInvoke();

              UpdateSurface(InvSurface, iOSSurface);
            }

            ProcessChanges();

            if (InvKeyboard.Focus != null)
            {
              var iOSFocus = InvKeyboard.Focus.Control?.Node as iOSNode;

              InvKeyboard.Focus = null;

              if (iOSFocus != null)
                Post(() => iOSFocus.Element.View.BecomeFirstResponder());

              // TODO: clear focus?
            }

            if (InvAccessibility.AnnounceList.Count > 0)
            {
              var AnnouncementText = InvAccessibility.AnnounceList.AsLineSeparatedText();
              InvAccessibility.AnnounceList.Clear();

              UIKit.UIAccessibility.PostNotification(UIKit.UIAccessibilityPostNotification.Announcement, new Foundation.NSString(AnnouncementText));
            }

            if (InvAccessibility.FocusPanel != null)
            {
              var iOSElement = TranslatePanel(InvAccessibility.FocusPanel);
              InvAccessibility.FocusPanel = null;

              UIKit.UIAccessibility.PostNotification(UIKit.UIAccessibilityPostNotification.LayoutChanged, iOSElement);

              // TODO: GetFocus
              //iOSElement = UIKit.UIAccessibility.FocusedElement(UIKit.UIView.NotificationVoiceOverIdentifier) as iOSContainer;
              //if (iOSElement != null)
              //  InvAccessibility.FocusPanel = iOSElement.PanelTag;
            }
          });
        };

        this.iOSDisplayLink = CoreAnimation.CADisplayLink.Create(Process);

        // TODO: iOSFoundation.iOS15_0: iOSDisplayLink.PreferredFrameRateRange
        if (iOSFoundation.iOS10_0)
          iOSDisplayLink.PreferredFramesPerSecond = 60;

        iOSDisplayLink.AddToRunLoop(Foundation.NSRunLoop.Current, Foundation.NSRunLoopMode.Common);

        // TODO: is this right? what if Location API is used in the Application.StartEvent?
        if (InvApplication.Location.IsRequired && InvApplication.Location.IsSupported)
        {
          this.iOSLocationManager = new CoreLocation.CLLocationManager();

          // iOS 8 requires you to manually request authorisation now.
          if (iOSFoundation.iOS8_0)
          {
            iOSLocationManager.RequestAlwaysAuthorization(); // works in background and foreground
            //iOSLocationManager.RequestWhenInUseAuthorization(); // works only in foreground
          }

          iOSLocationManager.DesiredAccuracy = CoreLocation.CLLocation.AccuracyNearestTenMeters;
          iOSLocationManager.DistanceFilter = 10.0F;
          iOSLocationManager.LocationsUpdated += (Sender, Event) =>
          {
            if (Event.Locations.Length > 0)
            {
              var Location = Event.Locations[Event.Locations.Length - 1];

              Guard(() => InvApplication.Location.ChangeInvoke(new Inv.Coordinate(Location.Coordinate.Latitude, Location.Coordinate.Longitude, Location.Altitude)));
            }
          };

          iOSLocationManager.StartUpdatingLocation();
        }
      });

      Process();

      /*
      var Checker = new UIKit.UITextChecker();

      var Word = "yacht";

      var Result = Checker.RangeOfMisspelledWordInString(Word, new Foundation.NSRange(0, Word.Length), 0, false, Foundation.NSLocale.CurrentLocale.LanguageCode);

      if (Result.Location != Foundation.NSRange.NotFound)
      {
        // TODO: invalid word.
      }

      var Ref = new UIKit.UIReferenceLibraryViewController(Word);
      iOSRootController.PresentViewController(Ref, true, null);
      */
      return iOSWindow;
    }
    internal void Terminating()
    {
      Guard(() =>
      {
        InvApplication.StopInvoke();

        if (iOSNotificationFeedbackGenerator != null)
        {
          iOSNotificationFeedbackGenerator.Dispose();
          this.iOSNotificationFeedbackGenerator = null;
        }

        foreach (var iOSImpactFeedbackGenerator in iOSImpactFeedbackArray)
          iOSImpactFeedbackGenerator?.Dispose();
        iOSImpactFeedbackArray.Fill(S => null);

        if (iOSLocationManager != null)
        {
          iOSLocationManager.StopUpdatingLocation();
          this.iOSLocationManager = null;
        }
      });
    }
    internal void Pausing()
    {
      Guard(() => InvApplication.SuspendInvoke());
    }
    internal void Resuming()
    {
      Guard(() => InvApplication.ResumeInvoke());
    }
    internal iOSContainer GetPanel(Inv.Panel InvPanel)
    {
      return TranslatePanel(InvPanel);
    }
    internal AVFoundation.AVAudioPlayer PlaySound(Inv.Sound InvSound, float Volume, float Rate, float Pan, bool Loop, bool OneShot)
    {
      var iOSSound = GetSound(InvSound);
      if (iOSSound == null)
        return null;

      /*
      // this is necessary to overlap the playing of the same SFX.
      if (OneShot && iOSSound.Playing)
      {
        // NOTE: This code has been demonstrated to cause a SIGSEGV - if you spam the same SFX multiple times in succession.

        // option 1.
        //var iOSOneShot = LoadSound(InvSound);
        //if (iOSOneShot != null)
        //{
        //  iOSOneShot.FinishedPlaying += (Sender, Event) => iOSOneShot.Dispose();
        //  iOSSound = iOSOneShot;
        //}

        // option 2.
        // iOSSound = AVFoundation.AVAudioPlayer.FromData(iOSSound.Data);

        // option 3.
        // AVAudioPlayer Pool?
      }
      */

      iOSSound.CurrentTime = 0; // repeated playing of the same sound effect or music track will restart from the beginning.
      iOSSound.PrepareToPlay();

      AdjustSound(iOSSound, Volume, Rate, Pan, Loop);

      iOSSound.Play();

      return iOSSound;
    }
    internal void AdjustSound(AVFoundation.AVAudioPlayer iOSSound, float Volume, float Rate, float Pan, bool Loop)
    {
      // ensure volume is within a valid range.
      if (Volume > 1.0F)
        Volume = 1.0F;
      else if (Volume < 0.0F)
        Volume = 0.0F;

      // NOTE: iOS docs say that 0.5 to 2.0 is the support range for playback rate (https://developer.apple.com/reference/avfoundation/avaudioplayer/1386118-rate).
      if (Rate > 2.0F)
        Rate = 2.0F;
      else if (Rate < 0.5F)
        Rate = 0.5F;

      iOSSound.Volume = Volume;
      iOSSound.Pan = Pan;
      iOSSound.EnableRate = true;
      iOSSound.NumberOfLoops = Loop ? -1 : 0;
      iOSSound.Rate = Rate;
    }
    internal TimeSpan GetSoundLength(Inv.Sound InvSound)
    {
      return TimeSpan.FromSeconds(GetSound(InvSound)?.Duration ?? 0.0);
    }
    internal void InteractDocument(Inv.File File, bool TemporaryCopy)
    {
      if (iOSDocumentInteractionController != null)
      {
        iOSDocumentInteractionController.DismissMenu(animated: false);
        this.iOSDocumentInteractionController = null;
      }

      // NOTE: clean up the last temporary file that was used.
      //       this is obviously not perfect, but the UIDocumentInteractionController events do not fire in a correct or useful way, so this is the best we can do.
      if (iOSInteractTemporaryPath != null)
      {
        try
        {
          if (System.IO.File.Exists(iOSInteractTemporaryPath))
            System.IO.File.Delete(iOSInteractTemporaryPath);
        }
        catch (Exception Exception)
        {
          Debug.WriteLine(Exception.Message);
        }
      }

      // NOTE: if you only retain a reference to the controller for the life of the method that is triggered by the button press, the popup will disappear.
      this.iOSDocumentInteractionController = new UIKit.UIDocumentInteractionController();
      iOSDocumentInteractionController.ViewControllerForPreview = (Controller) => iOSRootController; // this line fixes this error when you use Quick Look: *** Terminating app due to uncaught exception 'NSInternalInconsistencyException', reason: 'documentInteractionControllerViewControllerForPreview: must return a view controller'

      // NOTE: none of these events fire in a useful way.
      /*
      iOSDocumentInteractionController.WillBeginPreview += (s, a) =>
      {
        Debug.WriteLine("WillBeginPreview");
      };
      iOSDocumentInteractionController.DidEndPreview += (s, a) =>
      {
        Debug.WriteLine("DidEndPreview");
      };
      iOSDocumentInteractionController.WillPresentOpenInMenu += (s, a) =>
      {
        Debug.WriteLine("WillPresentOpenInMenu");
      };
      iOSDocumentInteractionController.DidDismissOpenInMenu += (s, a) =>
      {
        Debug.WriteLine("DidDismissOpenInMenu");
      };
      iOSDocumentInteractionController.WillBeginSendingToApplication += (s, a) =>
      {
        Debug.WriteLine("WillBeginSendingToApplication");
      };
      iOSDocumentInteractionController.DidEndSendingToApplication += (s, a) =>
      {
        Debug.WriteLine("DidEndSendingToApplication");
      };
      iOSDocumentInteractionController.WillPresentOptionsMenu += (s, a) =>
      {
        Debug.WriteLine("WillPresentOptionsMenu");
      };
      */
      iOSDocumentInteractionController.DidDismissOptionsMenu += (s, a) =>
      {
        //Debug.WriteLine("DidDismissOptionsMenu");
        this.iOSDocumentInteractionController = null; // allow garbage collection.
      };

      var InteractPath = SelectFilePath(File);

      if (TemporaryCopy)
      {
        try
        {
          // NOTE: iOS document sharing is async, so we must take a temporary copy of the file, in case the shared file is deleted immediately after the controller is presented.
          this.iOSInteractTemporaryPath = Path.Combine(Path.GetTempPath(), File.Name);

          System.IO.File.Copy(InteractPath, iOSInteractTemporaryPath, overwrite: true);

          InteractPath = iOSInteractTemporaryPath;
        }
        catch (Exception Exception)
        {
          Debug.WriteLine(Exception.Message);
        }
      }

      iOSDocumentInteractionController.Url = new Uri("file://" + InteractPath);
      iOSDocumentInteractionController.PresentOptionsMenu(new CoreGraphics.CGRect(0, iOSWindow.Bounds.Height - 300, iOSWindow.Bounds.Width, 300), iOSRootController.View, animated: true);
    }
    internal void Post(Action Action)
    {
      iOSWindow.BeginInvokeOnMainThread(() => Guard(Action));
    }
    internal void Call(Action Action)
    {
      iOSWindow.InvokeOnMainThread(Action);
    }
    internal void Guard(Action Action)
    {
      try
      {
        Action();
      }
      catch (Exception Exception)
      {
        InvApplication.HandleExceptionInvoke(Exception.Preserve());
      }
    }
    internal T Guard<T>(Func<T> Func, T Default = default)
    {
      try
      {
        return Func();
      }
      catch (Exception Exception)
      {
        InvApplication.HandleExceptionInvoke(Exception.Preserve());

        return Default;
      }
    }
    internal void ReclaimImage(IReadOnlyList<Inv.Image> ImageList)
    {
      foreach (var Image in ImageList)
      {
        var iOSImage = Image.Node as UIKit.UIImage;
        Image.Node = null;
        iOSImage?.Dispose();
      }
    }
    internal void ReclaimSound(IReadOnlyList<Inv.Sound> SoundList)
    {
      foreach (var Sound in SoundList)
      {
        var iOSSound = Sound.Node as AVFoundation.AVAudioPlayer;

        if (iOSSound == null || !iOSSound.Playing)
        {
          Sound.Node = null;
          iOSSound?.Dispose();
        }
      }
    }
    internal string ResolveAssetPath(Asset Asset)
    {
      var Result = SelectAssetPath(Asset);

      if (Result == null)
        throw new Exception("Asset was not found: " + Asset.Name);

      return Result;

    }
    internal string SelectAssetPath(Asset Asset)
    {
      return Foundation.NSBundle.MainBundle.PathForResource(System.IO.Path.GetFileNameWithoutExtension(Asset.Name), System.IO.Path.GetExtension(Asset.Name));
    }
    internal string SelectFilePath(File File)
    {
      return Path.Combine(SelectFolderPath(File.Folder), File.Name);
    }
    internal string SelectFolderPath(Folder Folder)
    {
      // /Library/ is the top-level directory for any files that are not user data files - ideal for application data files.
      var Result = Path.Combine(iOSShell.AppFolderPath(), Folder.GetRelativePath().AsSeparatedText(System.IO.Path.DirectorySeparatorChar.ToString()));

      System.IO.Directory.CreateDirectory(Result);

      return Result;
    }
    internal void ShowCalendarPicker(CalendarPicker CalendarPicker)
    {
      if (iOSDateTimePicker == null)
        this.iOSDateTimePicker = new iOSDateTimePicker(iOSRootController);
      iOSDateTimePicker.Show(CalendarPicker);
    }
    internal UIKit.UIFont TranslateUIFont(string FontName, int FontSize, Inv.FontWeight InvFontWeight)
    {
      // TODO: custom font name.

      if (FontName == null || FontName == InvApplication.Device.ProportionalFontName)
      {
        return UIKit.UIFont.SystemFontOfSize(FontSize, iOSFontWeightArray[InvFontWeight]);
      }
      /*else if (FontName == InvApplication.Device.MonospacedFontName)
      {
        var Result = UIKit.UIFont.MonospacedDigitSystemFontOfSize(FontSize, UIFontWeightArray[InvFontWeight]);

        Debug.Assert(Result != null);

        return Result;
      }*/
      else
      {
        var CheckFontWeight = InvFontWeight;

        var Result = UIKit.UIFont.FromName(FontName + iOSFontFamilyArray[CheckFontWeight], FontSize);

        while (Result == null)
        {
          switch (CheckFontWeight)
          {
            case Inv.FontWeight.Thin:
              CheckFontWeight = Inv.FontWeight.Light;
              break;

            case Inv.FontWeight.Light:
              CheckFontWeight = Inv.FontWeight.Regular;
              break;

            case Inv.FontWeight.Regular:
              return UIKit.UIFont.SystemFontOfSize(FontSize, iOSFontWeightArray[InvFontWeight]); // give up.

            case Inv.FontWeight.Medium:
              // just in case bold is possible, we'd prefer that over regular.
              Result = UIKit.UIFont.FromName(FontName + iOSFontFamilyArray[Inv.FontWeight.Bold], FontSize);
              if (Result != null)
                return Result;
              CheckFontWeight = Inv.FontWeight.Regular;
              break;

            case Inv.FontWeight.Bold:
              CheckFontWeight = Inv.FontWeight.Medium;
              break;

            case Inv.FontWeight.Heavy:
              CheckFontWeight = Inv.FontWeight.Bold;
              break;

            default:
              throw EnumHelper.UnexpectedValueException(CheckFontWeight);
          }

          Result = UIKit.UIFont.FromName(FontName + iOSFontFamilyArray[CheckFontWeight], FontSize);
        }

        return Result;
      }
    }
    internal CoreGraphics.CGImage TranslateCGImage(Inv.Image Image)
    {
      if (Image == null)
        return null;
      else
        return TranslateUIImage(Image).CGImage;
    }
    internal UIKit.UIColor TranslateUIColor(Inv.Colour InvColour)
    {
      if (InvColour == null)
      {
        return UIKit.UIColor.Clear;
      }
      else if (InvColour.Node == null)
      {
        var Record = InvColour.GetARGBRecord();
        var Result = new UIKit.UIColor((float)Record.R / 255F, (float)Record.G / 255F, (float)Record.B / 255F, (float)Record.A / 255F);

        InvColour.Node = Result;
        //ColourList.Add(InvColour);

        return Result;
      }
      else
      {
        return (UIKit.UIColor)InvColour.Node;
      }
    }
    internal Foundation.NSString TranslateCALineJoin(Inv.LineJoin InvLineJoin)
    {
      switch (InvLineJoin)
      {
        case Inv.LineJoin.Miter:
          return CoreAnimation.CAShapeLayer.JoinMiter;

        case Inv.LineJoin.Bevel:
          return CoreAnimation.CAShapeLayer.JoinBevel;

        case Inv.LineJoin.Round:
          return CoreAnimation.CAShapeLayer.JoinRound;

        default:
          throw EnumHelper.UnexpectedValueException(InvLineJoin);
      }
    }
    internal Foundation.NSString TranslateCALineCap(Inv.LineCap InvLineCap)
    {
      switch (InvLineCap)
      {
        case Inv.LineCap.Butt:
          return CoreAnimation.CAShapeLayer.CapButt;

        case Inv.LineCap.Round:
          return CoreAnimation.CAShapeLayer.CapRound;

        case Inv.LineCap.Square:
          return CoreAnimation.CAShapeLayer.CapSquare;

        default:
          throw EnumHelper.UnexpectedValueException(InvLineCap);
      }
    }
    internal CoreGraphics.CGColor TranslateCGColor(Inv.Colour InvColour)
    {
      return TranslateUIColor(InvColour).CGColor;
    }
    internal CoreGraphics.CGPoint TranslateCGPoint(Inv.Point InvPoint)
    {
      return new CoreGraphics.CGPoint(InvPoint.X, InvPoint.Y);
    }
    internal UIKit.UIImage TranslateUIImage(Inv.Image InvImage)
    {
      if (InvImage == null)
        return null;

      var Result = InvImage.Node as UIKit.UIImage;

      if (Result == null)
      {
        var Buffer = InvImage.GetBuffer();

        Result = UIKit.UIImage.LoadFromData(Foundation.NSData.FromArray(Buffer), 3.0F);

        InvImage.Node = Result;
      }

      return Result;
    }
    internal iOSContainer GetContainer(Inv.Panel InvPanel)
    {
      return TranslatePanel(InvPanel);
    }
    internal iOSSurface TranslateSurface(Inv.Surface InvSurface)
    {
      if (InvSurface.Node == null)
      {
        var Result = new iOSSurface();

        InvSurface.Node = Result;

        return Result;
      }
      else
      {
        return (iOSSurface)InvSurface.Node;
      }
    }
    internal iOSNode<iOSBlock> TranslateBlock(Inv.Block InvBlock)
    {
      var iOSNode = AccessPanel(InvBlock, P =>
      {
        var Result = new iOSBlock();
        Result.IsAccessibleQuery += WeakHelpers.WeakWrapper(this, P, (engine, button) =>
        {
          return engine.InvApplication.Window.IsActiveSurface(button.Surface);
        });
        return Result;
      });

      RenderPanel(InvBlock, iOSNode, (iOSContainer, iOSBlock) =>
      {
        TranslateLayout(InvBlock, iOSContainer, iOSBlock);
        TranslateTooltip(InvBlock.Tooltip, iOSBlock);

        iOSBlock.TextAlignment = TranslateJustify(InvBlock.Justify);
        iOSBlock.Lines = InvBlock.LineWrapping ? 0 : 1;
        iOSBlock.LineBreakMode = InvBlock.LineWrapping ? UIKit.UILineBreakMode.WordWrap : UIKit.UILineBreakMode.TailTruncation;

        var SpanChanged = InvBlock.Font.Render();
        if (InvBlock.SpanCollection.Render())
          SpanChanged = true;

        if (SpanChanged)
        {
          var iOSBlockAttributedText = new Foundation.NSMutableAttributedString();

          foreach (var InvSpan in InvBlock.SpanCollection)
          {
            InvSpan.Font.Render();
            TranslateFont(InvBlock.Font, InvSpan.Font, out var iOSFont, out var iOSColor);

            InvSpan.Background.Render();
            var iOSBackgroundColor = InvSpan.Background.Colour != null ? TranslateUIColor(InvSpan.Background.Colour) : null;

            var iOSSpanAttributeText = new Foundation.NSAttributedString
            (
              str: InvSpan.Text ?? Environment.NewLine,
              font: iOSFont,
              foregroundColor: iOSColor,
              backgroundColor: iOSBackgroundColor,
              underlineStyle: InvSpan.Font.IsUnderlined ?? InvBlock.Font.IsUnderlined ?? false ? Foundation.NSUnderlineStyle.Single : Foundation.NSUnderlineStyle.None,
              strikethroughStyle: InvSpan.Font.IsStrikethrough ?? InvBlock.Font.IsStrikethrough ?? false ? Foundation.NSUnderlineStyle.Single : Foundation.NSUnderlineStyle.None
            );
            iOSBlockAttributedText.Append(iOSSpanAttributeText);
          }

          iOSBlock.AttributedText = iOSBlockAttributedText;
        }
      });

      return iOSNode;
    }
    internal iOSNode<iOSBoard> TranslateBoard(Inv.Board InvBoard)
    {
      var iOSNode = AccessPanel(InvBoard, P =>
      {
        var Result = new iOSBoard();
        return Result;
      });

      RenderPanel(InvBoard, iOSNode, (iOSContainer, iOSBoard) =>
      {
        TranslateLayout(InvBoard, iOSContainer, iOSBoard);

        if (InvBoard.PinCollection.Render())
        {
          iOSBoard.RemovePins();
          foreach (var InvPin in InvBoard.PinCollection)
          {
            var InvRect = InvPin.Rect;

            var iOSElement = TranslatePanel(InvPin.Panel);
            iOSBoard.AddPin(iOSElement, TranslateRect(InvRect));
          }
        }
      });

      return iOSNode;
    }
    internal iOSNode<iOSBrowser> TranslateBrowser(Inv.Browser InvBrowser)
    {
      var iOSNode = AccessPanel(InvBrowser, P =>
      {
        var Result = new iOSBrowser();

        // TODO: use WeakHelpers to prevent memory leaks?

        Result.BlockQuery += (Url) =>
        {
          return Guard(() =>
          {
            var Fetch = new Inv.BrowserFetch(new Uri(Url));
            P.FetchInvoke(Fetch);
            return Fetch.IsCancelled;
          });
        };
        Result.ReadyEvent += (Url) =>
        {
          Guard(() => P.ReadyInvoke(new Inv.BrowserReady(new Uri(Url))));
        };

        return Result;
      });

      RenderPanel(InvBrowser, iOSNode, (iOSContainer, iOSBrowser) =>
      {
        TranslateLayout(InvBrowser, iOSContainer, iOSBrowser);

        var Navigate = InvBrowser.UriSingleton.Render();
        if (InvBrowser.HtmlSingleton.Render())
          Navigate = true;

        if (Navigate)
          iOSBrowser.Navigate(InvBrowser.UriSingleton.Data, InvBrowser.HtmlSingleton.Data);
      });

      return iOSNode;
    }
    internal iOSNode<iOSButton> TranslateButton(Inv.Button InvButton)
    {
      var iOSNode = AccessPanel(InvButton, P =>
      {
        var Result = new iOSButton();

        Result.PreviewTouchesBeganEvent += WeakHelpers.WeakWrapper(this, P, Result, (engine, button, iosButton) =>
        {
          engine.Guard(() =>
          {
            button.PressInvoke();

            if (button.Style == Inv.ButtonStyle.Flat)
              engine.TranslateBackground(button.Background.Colour?.Darken(0.25F), iosButton);
          });
        });
        Result.PreviewTouchesEndedEvent += WeakHelpers.WeakWrapper(this, P, Result, (engine, button, iosButton) =>
        {
          engine.Guard(() =>
          {
            // return to the background colour.
            if (button.Style == Inv.ButtonStyle.Flat)
              engine.TranslateBackground(button.Background.Colour, iosButton);
          });
        });
        Result.PreviewTouchesCancelledEvent += WeakHelpers.WeakWrapper(this, P, Result, (engine, button, iosButton) =>
        {
          engine.Guard(() =>
          {
            button.ReleaseInvoke();

            // return to the background colour.
            if (button.Style == Inv.ButtonStyle.Flat)
              engine.TranslateBackground(button.Background.Colour, iosButton);
          });
        });
        Result.SingleTapEvent += WeakHelpers.WeakWrapper(this, P, Result, (engine, button, iosbutton) =>
        {
          if (!engine.IsInputBlocked)
            engine.Guard(button.SingleTapInvoke);
        });
        Result.LongPressEvent += WeakHelpers.WeakWrapper(this, P, Result, (engine, button, iosbutton) =>
        {
          if (!engine.IsInputBlocked)
            engine.Guard(button.ContextTapInvoke);
        });
        Result.IsAccessibleQuery += WeakHelpers.WeakWrapper(this, P, (engine, button) =>
        {
          return engine.InvApplication.Window.IsActiveSurface(button.Surface);
        });

        return Result;
      });

      RenderPanel(InvButton, iOSNode, (iOSContainer, iOSButton) =>
      {
        TranslateLayout(InvButton, iOSContainer, iOSButton);
        TranslateFocus(InvButton.Focus, iOSButton);
        TranslateTooltip(InvButton.Tooltip, iOSButton);

        if (InvButton.Style == Inv.ButtonStyle.Flat)
          iOSContainer.Alpha = InvButton.IsEnabled ? 1.0F : 0.5F;

        iOSButton.Enabled = InvButton.IsEnabled;
        // TODO: InvButton.IsFocused

        iOSButton.AccessibilityLabel = InvButton.Hint;

        if (InvButton.ContentSingleton.Render())
          iOSButton.SetContent(TranslatePanel(InvButton.ContentSingleton.Data));
      });

      return iOSNode;
    }
    internal iOSNode<iOSCanvas> TranslateCanvas(Inv.Canvas InvCanvas)
    {
      var iOSNode = AccessPanel(InvCanvas, P =>
      {
        var Result = new iOSCanvas(this);
        Result.TouchDownAction = WeakHelpers.WeakWrapper<iOSEngine, Inv.Canvas, CoreGraphics.CGPoint>(this, P, (engine, invCanvas, Point) => engine.Guard(() => invCanvas.PressInvoke(new Inv.CanvasCommand(engine.TranslatePoint(Point), LeftMouseButton: false, RightMouseButton: false))));
        Result.TouchMoveAction = WeakHelpers.WeakWrapper<iOSEngine, Inv.Canvas, CoreGraphics.CGPoint>(this, P, (engine, invCanvas, Point) => engine.Guard(() => invCanvas.MoveInvoke(new Inv.CanvasCommand(engine.TranslatePoint(Point), LeftMouseButton: false, RightMouseButton: false))));
        Result.TouchUpAction = WeakHelpers.WeakWrapper<iOSEngine, Inv.Canvas, CoreGraphics.CGPoint>(this, P, (engine, invCanvas, Point) => engine.Guard(() => invCanvas.ReleaseInvoke(new Inv.CanvasCommand(engine.TranslatePoint(Point), LeftMouseButton: false, RightMouseButton: false))));
        Result.SingleTapAction = WeakHelpers.WeakWrapper<iOSEngine, Inv.Canvas, CoreGraphics.CGPoint>(this, P, (engine, invCanvas, Point) => engine.Guard(() => invCanvas.SingleTapInvoke(engine.TranslatePoint(Point))));
        Result.DoubleTapAction = WeakHelpers.WeakWrapper<iOSEngine, Inv.Canvas, CoreGraphics.CGPoint>(this, P, (engine, invCanvas, Point) => engine.Guard(() => invCanvas.DoubleTapInvoke(engine.TranslatePoint(Point))));
        Result.LongPressAction = WeakHelpers.WeakWrapper<iOSEngine, Inv.Canvas, CoreGraphics.CGPoint>(this, P, (engine, invCanvas, Point) => engine.Guard(() => invCanvas.ContextTapInvoke(engine.TranslatePoint(Point))));
        Result.ZoomAction = WeakHelpers.WeakWrapper<iOSEngine, Inv.Canvas, CoreGraphics.CGPoint, int>(this, P, (engine, invCanvas, Point, Delta) => engine.Guard(() => invCanvas.ZoomInvoke(new Inv.Zoom(engine.TranslatePoint(Point), Delta))));
        Result.DrawAction = WeakHelpers.WeakWrapper(this, P, Result, (engine, invCanvas, iosCanvas) => engine.Guard(() => invCanvas.DrawInvoke(iosCanvas)));
        Result.MeasureFunc = WeakHelpers.WeakWrapper<iOSEngine, Inv.Canvas, iOSCanvas, CoreGraphics.CGSize, CoreGraphics.CGSize?>(this, P, Result, (engine, invCanvas, iosCanvas, Constraint) => engine.Guard(() =>
        {
          var CanvasWidth = (int)Constraint.Width;
          var CanvasHeight = (int)Constraint.Height;

          if (CanvasWidth == 0 || CanvasHeight == 0)
            return (CoreGraphics.CGSize?)null;

          var Measure = new Inv.CanvasMeasure(new Inv.Dimension(CanvasWidth, CanvasHeight));

          invCanvas.MeasureInvoke(Measure);

          if (Measure.Dimension == null)
            return (CoreGraphics.CGSize?)null;

          return new CoreGraphics.CGSize(Measure.Dimension.Value.Width, Measure.Dimension.Value.Height);
        }));
        Result.AccessibilityQuery = WeakHelpers.WeakWrapper(this, P, Result, (engine, invCanvas, iosCanvas) => engine.Guard(() =>
        {
          var Query = new CanvasQuery();
          invCanvas.QueryInvoke(Query);
          return Query.Regions.Select(R => new UIKit.UIAccessibilityElement(Result) { AccessibilityFrame = iosCanvas.ConvertRectToView(engine.TranslateRect(R.Rect), iOSWindow), AccessibilityLabel = R.Hint }).ToArray();
        }));
        return Result;
      });

      RenderPanel(InvCanvas, iOSNode, (iOSContainer, iOSCanvas) =>
      {
        TranslateLayout(InvCanvas, iOSContainer, iOSCanvas);

        if (InvCanvas.IsInvalidated)
          iOSCanvas.SetNeedsDisplay();
      });

      return iOSNode;
    }
    internal iOSNode<iOSDock> TranslateDock(Inv.Dock InvDock)
    {
      var iOSNode = AccessPanel(InvDock, P =>
      {
        var Result = new iOSDock();
        Result.SetOrientation(InvDock.IsHorizontal ? iOSOrientation.Horizontal : iOSOrientation.Vertical);
        return Result;
      });

      RenderPanel(InvDock, iOSNode, (iOSContainer, iOSDock) =>
      {
        TranslateLayout(InvDock, iOSContainer, iOSDock);

        iOSDock.SetOrientation(InvDock.IsHorizontal ? iOSOrientation.Horizontal : iOSOrientation.Vertical);

        if (InvDock.CollectionRender())
          iOSDock.Compose(InvDock.HeaderCollection.Select(H => TranslatePanel(H)), InvDock.ClientCollection.Select(C => TranslatePanel(C)), InvDock.FooterCollection.Reverse().Select(F => TranslatePanel(F)));
      });

      return iOSNode;
    }
    internal iOSNode<iOSElement> TranslateEdit(Inv.Edit InvEdit)
    {
      var IsSearch = InvEdit.Input == EditInput.Search;

      var iOSNode = AccessPanel(InvEdit, P =>
      {
        if (IsSearch)
        {
          var Result = new iOSSearch();

          Result.AutocapitalizationType = UIKit.UITextAutocapitalizationType.None;
          Result.AutocorrectionType = UIKit.UITextAutocorrectionType.No;
          Result.TextChanged += WeakHelpers.WeakEventHandlerWrapper<iOSEngine, Inv.Edit, iOSSearch, UIKit.UISearchBarTextChangedEventArgs>(this, P, Result, (engine, invEdit, iosNode, Sender, Event) => engine.Guard(() => invEdit.ChangeText(iosNode.Text)));
          Result.SearchButtonClicked += WeakHelpers.WeakEventHandlerWrapper(this, P, (engine, invEdit, Sender, Event) => engine.Guard(invEdit.Return));
          Result.OnEditingStarted += WeakHelpers.WeakEventHandlerWrapper(this, P, Result, (engine, invEdit, iosNode, Sender, Event) => engine.Guard(() => invEdit.Focus.GotInvoke()));
          Result.OnEditingStopped += WeakHelpers.WeakEventHandlerWrapper(this, P, Result, (engine, invEdit, iosNode, Sender, Event) => engine.Guard(() => invEdit.Focus.LostInvoke()));

          return (iOSElement)Result;
        }
        else
        {
          var Result = new iOSEdit();

          switch (P.Input)
          {
            case EditInput.Decimal:
              Result.KeyboardType = UIKit.UIKeyboardType.DecimalPad;
              break;

            case EditInput.Email:
              Result.KeyboardType = UIKit.UIKeyboardType.EmailAddress;
              break;

            case EditInput.Integer:
              Result.KeyboardType = UIKit.UIKeyboardType.NumberPad;
              break;

            case EditInput.Name:
              Result.KeyboardType = UIKit.UIKeyboardType.Default;
              Result.AutocapitalizationType = UIKit.UITextAutocapitalizationType.Words;
              Result.AutocorrectionType = UIKit.UITextAutocorrectionType.No;
              break;

            case EditInput.Number:
              Result.KeyboardType = UIKit.UIKeyboardType.Default;
              break;

            case EditInput.Password:
              Result.KeyboardType = UIKit.UIKeyboardType.Default;
              Result.SecureTextEntry = true;
              break;

            case EditInput.Phone:
              Result.KeyboardType = UIKit.UIKeyboardType.PhonePad;
              break;

            case EditInput.Text:
              Result.KeyboardType = UIKit.UIKeyboardType.Default;
              Result.AutocapitalizationType = UIKit.UITextAutocapitalizationType.None;
              break;

            case EditInput.Uri:
              Result.KeyboardType = UIKit.UIKeyboardType.Url;
              break;

            case EditInput.Username:
              Result.KeyboardType = UIKit.UIKeyboardType.Default;
              Result.AutocapitalizationType = UIKit.UITextAutocapitalizationType.None;
              Result.AutocorrectionType = UIKit.UITextAutocorrectionType.No;
              break;

            default:
              throw EnumHelper.UnexpectedValueException(P.Input);
          }

          Result.AccessibilityLabel = P.Hint;

          Result.EditingDidBegin += WeakHelpers.WeakEventHandlerWrapper(this, P, Result, (engine, invEdit, iosNode, Sender, Event) => engine.Guard(() => invEdit.Focus.GotInvoke()));
          Result.EditingChanged += WeakHelpers.WeakEventHandlerWrapper(this, P, Result, (engine, invEdit, iosNode, Sender, Event) => engine.Guard(() => invEdit.ChangeText(iosNode.Text)));
          Result.EditingDidEnd += WeakHelpers.WeakEventHandlerWrapper(this, P, Result, (engine, invEdit, iosNode, Sender, Event) => engine.Guard(() => invEdit.Focus.LostInvoke()));
          Result.ReturnEvent += WeakHelpers.WeakWrapper(this, P, (engine, invEdit) => engine.Guard(invEdit.Return));

          return (iOSElement)Result;
        }
      });

      RenderPanel(InvEdit, iOSNode, (iOSContainer, iOSElement) =>
      {
        if (IsSearch)
        {
          var iOSSearch = (iOSSearch)iOSElement;

          TranslateLayout(InvEdit, iOSContainer, iOSSearch);
          TranslateFont(InvEdit.Font, (iOSFont, iOSColor) =>
          {
            iOSSearch.TextFont = iOSFont;
            iOSSearch.TextColor = iOSColor;
          });
          TranslateFocus(InvEdit.Focus, iOSSearch);

          iOSSearch.IsReadOnly = InvEdit.IsReadOnly;
          iOSSearch.Text = InvEdit.Text;
        }
        else
        {
          var iOSEdit = (iOSEdit)iOSElement;

          TranslateLayout(InvEdit, iOSContainer, iOSEdit);
          TranslateFont(InvEdit.Font, (iOSFont, iOSColor) =>
          {
            iOSEdit.Font = iOSFont;
            iOSEdit.TextColor = iOSColor;
            iOSEdit.Underline = InvEdit.Font.IsUnderlined ?? false;
            iOSEdit.Strikethrough = InvEdit.Font.IsStrikethrough ?? false;
          });
          TranslateFocus(InvEdit.Focus, iOSEdit);

          iOSEdit.IsReadOnly = InvEdit.IsReadOnly;
          iOSEdit.Text = InvEdit.Text;
        }
      });

      return iOSNode;
    }
    internal iOSNode<iOSFlow> TranslateFlow(Inv.Flow InvFlow)
    {
      iOSFlowItem TranslateFlowItem(Inv.Panel InvContent)
      {
        // make sure all changes are processed before returning the panel.
        ProcessChanges();

        return new iOSFlowItem()
        {
          Panel = InvContent,
          Container = TranslatePanel(InvContent)
        };
      }

      var iOSNode = AccessPanel(InvFlow, P =>
      {
        var Result = new iOSFlow(new iOSFlowConfiguration
        {
          NumberOfSectionsQuery = () => P.SectionCount,
          RowsInSectionQuery = (Section) => Guard(() => P.Sections[Section]?.ItemCount ?? 0),
          GetViewForHeaderQuery = (Section) => Guard(() => TranslateFlowItem(P.Sections[Section]?.Header)),
          GetViewForFooterQuery = (Section) => Guard(() => TranslateFlowItem(P.Sections[Section]?.Footer)),
          GetCellContentQuery = (Section, Item) => Guard(() => TranslateFlowItem(P.Sections[Section]?.ItemInvoke(Item))),
          RecycleViewEvent = (Section, Item, Recycle) => Guard(() => P.Sections[Section]?.RecycleInvoke(Item, Recycle.Panel))
        });
        Result.TableView.RefreshAction = () => Guard(() => P.RefreshInvoke(new FlowRefresh(P, Result.CompleteRefresh)));
        return Result;
      });

      RenderPanel(InvFlow, iOSNode, (iOSContainer, iOSFlow) =>
      {
        TranslateLayout(InvFlow, iOSContainer, iOSFlow);

        iOSFlow.IsRefreshable = InvFlow.IsRefreshable;
        iOSFlow.Fixture = TranslatePanel(InvFlow.Fixture);

        if (InvFlow.IsRefresh)
        {
          InvFlow.IsRefresh = false;
          iOSFlow.NewRefresh();
        }

        if (InvFlow.IsReload)
        {
          InvFlow.IsReload = false;
          iOSFlow.TableView.ReloadData();
        }

        if (InvFlow.ScrollSection != null || InvFlow.ScrollIndex != null)
        {
          if (InvFlow.ScrollSection != null && InvFlow.ScrollIndex != null)
            iOSFlow.ScrollToRow(Foundation.NSIndexPath.FromItemSection(InvFlow.ScrollIndex.Value, InvFlow.ScrollSection.Value), true);

          InvFlow.ScrollSection = null;
          InvFlow.ScrollIndex = null;
        }
      });

      return iOSNode;
    }
    internal iOSNode<iOSFrame> TranslateFrame(Inv.Frame InvFrame)
    {
      var iOSNode = AccessPanel(InvFrame, P =>
      {
        var Result = new iOSFrame();
        return Result;
      });

      RenderPanel(InvFrame, iOSNode, (iOSContainer, iOSFrame) =>
      {
        TranslateLayout(InvFrame, iOSContainer, iOSFrame);

        if (InvFrame.ContentSingleton.Render())
        {
          var iOSFromContent = iOSFrame.Content;
          var iOSToContent = TranslatePanel(InvFrame.ContentSingleton.Data);

          var InvTransition = InvFrame.ActiveTransition;

          if (InvTransition == null)
          {
            if (iOSFromContent != iOSToContent)
            {
              if (iOSFromContent != null)
                iOSFrame.SafeRemoveView(iOSFromContent);

              if (iOSToContent != null)
                iOSFrame.SafeAddView(iOSToContent);

              iOSFrame.SetContent(iOSToContent);
            }
          }
          else if (iOSFrame.IsTransitioning)
          {
            InvFrame.ContentSingleton.Change(); // retry next cycle.
          }
          else
          {
            // NOTE: give the previous panel a chance to process before it is animated away.
            if (InvFrame.FromPanel != null)
            {
              TranslatePanel(InvFrame.FromPanel);
              InvFrame.FromPanel = null;
            }

            if (iOSFromContent != iOSToContent)
            {
              iOSFrame.IsTransitioning = true;
              ExecuteTransition(InvTransition, iOSFrame, iOSFromContent, iOSToContent, () => iOSFrame.IsTransitioning = false);

              iOSFrame.SetContent(iOSToContent);
            }

            InvFrame.ActiveTransition = null;
          }
        }
      });

      return iOSNode;
    }
    internal iOSNode<iOSGraphic> TranslateGraphic(Inv.Graphic InvGraphic)
    {
      var iOSNode = AccessPanel(InvGraphic, P =>
      {
        var Result = new iOSGraphic();
        return Result;
      });

      RenderPanel(InvGraphic, iOSNode, (iOSContainer, iOSGraphic) =>
      {
        TranslateLayout(InvGraphic, iOSContainer, iOSGraphic);
        TranslateTooltip(InvGraphic.Tooltip, iOSGraphic);

        if (InvGraphic.Fit.Render())
          iOSGraphic.ImageContentMode = TranslateFit(InvGraphic.Fit.Method);

        if (InvGraphic.ImageSingleton.Render())
        {
          var iOSImage = TranslateUIImage(InvGraphic.Image);

          if (iOSGraphic.Image != iOSImage)
            iOSGraphic.Image = iOSImage;
        }
      });

      return iOSNode;
    }
    internal iOSNode<iOSLabel> TranslateLabel(Inv.Label InvLabel)
    {
      var iOSNode = AccessPanel(InvLabel, P =>
      {
        var Result = new iOSLabel();
        Result.IsAccessibleQuery += WeakHelpers.WeakWrapper(this, P, (engine, button) =>
        {
          return engine.InvApplication.Window.IsActiveSurface(button.Surface);
        });
        return Result;
      });

      RenderPanel(InvLabel, iOSNode, (iOSContainer, iOSLabel) =>
      {
        TranslateLayout(InvLabel, iOSContainer, iOSLabel);
        TranslateTooltip(InvLabel.Tooltip, iOSLabel);

        TranslateFont(InvLabel.Font, (iOSFont, iOSColor) =>
        {
          iOSLabel.Font = iOSFont;
          iOSLabel.TextColor = iOSColor;
          iOSLabel.Underline = InvLabel.Font.IsUnderlined ?? false;
          iOSLabel.Strikethrough = InvLabel.Font.IsStrikethrough ?? false;
        });

        iOSLabel.TextAlignment = TranslateJustify(InvLabel.Justify);
        iOSLabel.Lines = InvLabel.LineWrapping ? 0 : 1;
        iOSLabel.LineBreakMode = InvLabel.LineWrapping ? UIKit.UILineBreakMode.WordWrap : UIKit.UILineBreakMode.TailTruncation;

        iOSLabel.Text = InvLabel.Text;
      });

      return iOSNode;
    }
    internal iOSNode<iOSMemo> TranslateMemo(Inv.Memo InvMemo)
    {
      var iOSNode = AccessPanel(InvMemo, P =>
      {
        var Result = new iOSMemo();
        Result.Changed += WeakHelpers.WeakEventHandlerWrapper(this, P, Result, (engine, invMemo, iosNode, Sender, Event) => engine.Guard(() => invMemo.ChangeText(iosNode.Text)));
        Result.Started += WeakHelpers.WeakEventHandlerWrapper(this, P, Result, (engine, invMemo, iosNode, Sender, Event) => engine.Guard(() => invMemo.Focus.GotInvoke()));
        Result.Ended += WeakHelpers.WeakEventHandlerWrapper(this, P, Result, (engine, invMemo, iosNode, Sender, Event) => engine.Guard(() => invMemo.Focus.LostInvoke()));
        return Result;
      });

      RenderPanel(InvMemo, iOSNode, (iOSContainer, iOSMemo) =>
      {
        TranslateLayout(InvMemo, iOSContainer, iOSMemo);
        TranslateFont(InvMemo.Font, (iOSFont, iOSColor) =>
        {
          iOSMemo.Font = iOSFont;
          iOSMemo.TextColor = iOSColor;
          iOSMemo.Underline = InvMemo.Font.IsUnderlined ?? false;
          iOSMemo.Strikethrough = InvMemo.Font.IsStrikethrough ?? false;
        });
        TranslateFocus(InvMemo.Focus, iOSMemo);

        iOSMemo.Editable = !InvMemo.IsReadOnly;

        var RenderText = iOSMemo.Text != InvMemo.Text;
        if (InvMemo.MarkupCollection.Render())
          RenderText = true;

        if (RenderText)
        {
          if (InvMemo.MarkupCollection.Count == 0)
          {
            iOSMemo.Text = InvMemo.Text;
          }
          else
          {
            var iOSAttributedString = new Foundation.NSMutableAttributedString(InvMemo.Text);

            var BaseAttributes = new UIKit.UIStringAttributes();
            BaseAttributes.Font = iOSMemo.Font;
            BaseAttributes.ForegroundColor = iOSMemo.TextColor;
            BaseAttributes.UnderlineStyle = InvMemo.Font.IsUnderlined ?? false ? Foundation.NSUnderlineStyle.Single : Foundation.NSUnderlineStyle.None;
            BaseAttributes.StrikethroughStyle = InvMemo.Font.IsStrikethrough ?? false ? Foundation.NSUnderlineStyle.Single : Foundation.NSUnderlineStyle.None;
            iOSAttributedString.SetAttributes(BaseAttributes, new Foundation.NSRange(0, InvMemo.Text.Length));

            foreach (var InvMarkup in InvMemo.MarkupCollection.Where(M => M.RangeList.Count > 0))
            {
              TranslateFont(InvMemo.Font, InvMarkup.Font, out var iOSMarkupFont, out var iOSMarkupColor);

              var MarkupAttributes = new UIKit.UIStringAttributes();
              MarkupAttributes.Font = iOSMarkupFont;
              MarkupAttributes.ForegroundColor = iOSMarkupColor;
              MarkupAttributes.UnderlineStyle = InvMarkup.Font.IsUnderlined ?? InvMemo.Font.IsUnderlined ?? false ? Foundation.NSUnderlineStyle.Single : Foundation.NSUnderlineStyle.None;
              MarkupAttributes.StrikethroughStyle = InvMarkup.Font.IsStrikethrough ?? InvMemo.Font.IsStrikethrough ?? false ? Foundation.NSUnderlineStyle.Single : Foundation.NSUnderlineStyle.None;

              foreach (var Range in InvMarkup.RangeList)
                iOSAttributedString.SetAttributes(MarkupAttributes, new Foundation.NSRange(Range.Index, Range.Count));
            }

            iOSMemo.AttributedText = iOSAttributedString;
          }
        }
      });

      return iOSNode;
    }
    internal iOSNode<iOSNative> TranslateNative(Inv.Native InvNative)
    {
      var iOSNode = AccessPanel(InvNative, P =>
      {
        var Result = new iOSNative();
        return Result;
      });

      RenderPanel(InvNative, iOSNode, (iOSContainer, iOSNative) =>
      {
        TranslateLayout(InvNative, iOSContainer, iOSNative);

        if (InvNative.ContentSingleton.Render())
          iOSNative.SetContent((UIKit.UIView)InvNative.ContentSingleton.Data);
      });

      return iOSNode;
    }
    internal iOSNode<iOSOverlay> TranslateOverlay(Inv.Overlay InvOverlay)
    {
      var iOSNode = AccessPanel(InvOverlay, P =>
      {
        var Result = new iOSOverlay();
        return Result;
      });

      RenderPanel(InvOverlay, iOSNode, (iOSContainer, iOSOverlay) =>
      {
        TranslateLayout(InvOverlay, iOSContainer, iOSOverlay);

        if (InvOverlay.PanelCollection.Render())
          iOSOverlay.Compose(InvOverlay.PanelCollection.Select(E => TranslatePanel(E)));
      });

      return iOSNode;
    }
    internal iOSNode<iOSScroll> TranslateScroll(Inv.Scroll InvScroll)
    {
      var iOSNode = AccessPanel(InvScroll, P =>
      {
        var Result = new iOSScroll();
        Result.SetOrientation(InvScroll.IsHorizontal ? iOSOrientation.Horizontal : iOSOrientation.Vertical);
        return Result;
      });

      RenderPanel(InvScroll, iOSNode, (iOSContainer, iOSScroll) =>
      {
        TranslateLayout(InvScroll, iOSContainer, iOSScroll);

        iOSScroll.SetOrientation(InvScroll.IsHorizontal ? iOSOrientation.Horizontal : iOSOrientation.Vertical);

        if (InvScroll.ContentSingleton.Render())
          iOSScroll.SetContent(TranslatePanel(InvScroll.Content));

        var Request = InvScroll.HandleRequest();
        if (Request != null)
        {
          switch (Request.Value)
          {
            case ScrollRequest.Start:
              iOSScroll.ScrollToStart();
              break;

            case ScrollRequest.End:
              iOSScroll.ScrollToEnd();
              break;

            default:
              throw EnumHelper.UnexpectedValueException(Request.Value);
          }
        }
      });

      return iOSNode;
    }
    internal iOSNode<iOSShape> TranslateShape(Inv.Shape InvShape)
    {
      var iOSNode = AccessPanel(InvShape, P =>
      {
        var Result = new iOSShape();
        return Result;
      });

      RenderPanel(InvShape, iOSNode, (iOSContainer, iOSShape) =>
      {
        TranslateLayout(InvShape, iOSContainer, iOSShape);

        if (InvShape.Fit.Render())
        {
          // TODO: fit.
          //iOSShape.ShapeContentMode = TranslateFit(InvShape.Fit.Method);
        }

        if (InvShape.Fill.Render())
        {
          iOSShape.FillColor = TranslateUIColor(InvShape.Fill.Colour).CGColor;
        }

        if (InvShape.Stroke.Render())
        {
          iOSShape.StrokeWidth = InvShape.Stroke.Thickness;
          iOSShape.StrokeColor = TranslateUIColor(InvShape.Stroke.Colour).CGColor;
          iOSShape.LineCap = TranslateCALineCap(InvShape.Stroke.Cap);
          iOSShape.LineJoin = TranslateCALineJoin(InvShape.Stroke.Join);

          // TODO: LineDashPattern does not seem to be implemented on the CAShapeLayer.
          iOSShape.LineDashPattern = InvShape.Stroke.DashPattern?.Select(P => new Foundation.NSNumber(P)).ToArray();
        }

        if (InvShape.FigureCollection.Render())
        {
          var iOSBezierPath = new UIKit.UIBezierPath();

          foreach (var InvFigure in InvShape.FigureCollection)
          {
            if (InvFigure.Polygon != null)
            {
              var PathArray = InvFigure.Polygon.PathArray;
              if (PathArray != null && PathArray.Length > 0)
              {
                iOSBezierPath.MoveTo(TranslateCGPoint(PathArray[0]));
                foreach (var Path in PathArray.Skip(1))
                  iOSBezierPath.AddLineTo(TranslateCGPoint(Path));
              }
            }
            else if (InvFigure.Ellipse != null)
            {
              var Left = InvFigure.Ellipse.Center.X - InvFigure.Ellipse.Radius.X;
              var Top = InvFigure.Ellipse.Center.Y - InvFigure.Ellipse.Radius.Y;
              var Width = InvFigure.Ellipse.Radius.X * 2;
              var Height = InvFigure.Ellipse.Radius.Y * 2;

              iOSBezierPath.AppendPath(UIKit.UIBezierPath.FromOval(new CoreGraphics.CGRect(Left, Top, Width, Height)));
            }
            else if (InvFigure.Line != null)
            {
              iOSBezierPath.MoveTo(TranslateCGPoint(InvFigure.Line.Start));
              iOSBezierPath.AddLineTo(TranslateCGPoint(InvFigure.Line.End));
            }
          }

          var NotionalDimension = InvShape.GetNotionalDimension();
          iOSShape.SetPath(iOSBezierPath.CGPath, NotionalDimension.Width, NotionalDimension.Height);
        }
      });

      return iOSNode;
    }
    internal iOSNode<iOSStack> TranslateStack(Inv.Stack InvStack)
    {
      var iOSNode = AccessPanel(InvStack, P =>
      {
        var Result = new iOSStack();
        Result.SetOrientation(InvStack.IsHorizontal ? iOSOrientation.Horizontal : iOSOrientation.Vertical);
        return Result;
      });

      RenderPanel(InvStack, iOSNode, (iOSContainer, iOSStack) =>
      {
        TranslateLayout(InvStack, iOSContainer, iOSStack);

        iOSStack.SetOrientation(InvStack.IsHorizontal ? iOSOrientation.Horizontal : iOSOrientation.Vertical);

        if (InvStack.PanelCollection.Render())
          iOSStack.Compose(InvStack.PanelCollection.Select(E => TranslatePanel(E)));
      });

      return iOSNode;
    }
    internal iOSNode<iOSSwitch> TranslateSwitch(Inv.Switch InvSwitch)
    {
      var iOSNode = AccessPanel(InvSwitch, P =>
      {
        var Result = new iOSSwitch();
        Result.ValueChanged += WeakHelpers.WeakEventHandlerWrapper(this, P, Result, (engine, @switch, iosswitch, sender, e) =>
        {
          engine.Guard(() => @switch.ChangeChecked(iosswitch.IsOn));
        });
        return Result;
      });

      RenderPanel(InvSwitch, iOSNode, (iOSContainer, iOSSwitch) =>
      {
        iOSSwitch.IsOn = InvSwitch.IsOn;
        iOSSwitch.IsEnabled = InvSwitch.IsEnabled;
        iOSSwitch.OnTintColor = TranslateUIColor(InvSwitch.PrimaryColour);
        //iOSSwitch.OffTintColor = TranslateUIColor(InvSwitch.SecondaryColour);

        TranslateLayout(InvSwitch, iOSContainer, iOSSwitch);
      });

      return iOSNode;
    }
    internal iOSNode<iOSTable> TranslateTable(Inv.Table InvTable)
    {
      var iOSNode = AccessPanel(InvTable, P =>
      {
        var Result = new iOSTable();
        return Result;
      });

      RenderPanel(InvTable, iOSNode, (iOSContainer, iOSTable) =>
      {
        TranslateLayout(InvTable, iOSContainer, iOSTable);

        if (InvTable.CollectionRender())
        {
          var iOSRowList = new Inv.DistinctList<iOSTableRow>(InvTable.RowCollection.Count);

          foreach (var InvRow in InvTable.RowCollection)
          {
            var iOSRow = new iOSTableRow(iOSTable, TranslatePanel(InvRow.Content), InvRow.LengthType == TableAxisLength.Star, InvRow.LengthType == TableAxisLength.Fixed || InvRow.LengthType == TableAxisLength.Star ? InvRow.LengthValue : (int?)null);
            iOSRowList.Add(iOSRow);
          }

          var iOSColumnList = new Inv.DistinctList<iOSTableColumn>(InvTable.ColumnCollection.Count);

          foreach (var InvColumn in InvTable.ColumnCollection)
          {
            var iOSColumn = new iOSTableColumn(iOSTable, TranslatePanel(InvColumn.Content), InvColumn.LengthType == TableAxisLength.Star, InvColumn.LengthType == TableAxisLength.Fixed || InvColumn.LengthType == TableAxisLength.Star ? InvColumn.LengthValue : (int?)null);
            iOSColumnList.Add(iOSColumn);
          }

          var iOSCellList = new Inv.DistinctList<iOSTableCell>(InvTable.CellCollection.Count);

          foreach (var InvCell in InvTable.CellCollection)
          {
            var iOSCell = new iOSTableCell(iOSColumnList[InvCell.Column.Index], iOSRowList[InvCell.Row.Index], TranslatePanel(InvCell.Content));
            iOSCellList.Add(iOSCell);
          }

          iOSTable.Compose(iOSRowList, iOSColumnList, iOSCellList);
        }
      });

      return iOSNode;
    }
    internal iOSNode<iOSVideo> TranslateVideo(Inv.Video InvVideo)
    {
      var iOSNode = AccessPanel(InvVideo, P =>
      {
        var Result = new iOSVideo();
        return Result;
      });

      RenderPanel(InvVideo, iOSNode, (iOSContainer, iOSVideo) =>
      {
        TranslateLayout(InvVideo, iOSContainer, iOSVideo);

        if (InvVideo.SourceSingleton.Render())
        {
          var InvSource = InvVideo.SourceSingleton.Data;

          if (InvSource?.Asset != null)
            iOSVideo.SetSource(new Uri(ResolveAssetPath(InvSource.Asset)));
          else if (InvSource?.File != null)
            iOSVideo.SetSource(new Uri(SelectFilePath(InvSource.File)));
          else if (InvSource?.Uri != null)
            iOSVideo.SetSource(InvSource.Uri);
          else
            iOSVideo.SetSource(null);
        }

        if (InvVideo.StateSingleton.Render())
        {
          switch (InvVideo.StateSingleton.Data)
          {
            case VideoState.Stop:
              iOSVideo.Stop();
              break;

            case VideoState.Pause:
              iOSVideo.Pause();
              break;

            case VideoState.Play:
              iOSVideo.Play();
              break;

            case VideoState.Restart:
              iOSVideo.Stop();
              iOSVideo.Play();
              break;

            default:
              throw EnumHelper.UnexpectedValueException(InvVideo.StateSingleton.Data);
          }
        }
      });

      return iOSNode;
    }
    internal iOSNode<iOSWrap> TranslateWrap(Inv.Wrap InvWrap)
    {
      var iOSNode = AccessPanel(InvWrap, P =>
      {
        var Result = new iOSWrap();
        Result.SetOrientation(InvWrap.IsHorizontal ? iOSOrientation.Horizontal : iOSOrientation.Vertical);
        return Result;
      });

      RenderPanel(InvWrap, iOSNode, (iOSContainer, iOSWrap) =>
      {
        TranslateLayout(InvWrap, iOSContainer, iOSWrap);

        iOSWrap.SetOrientation(InvWrap.IsHorizontal ? iOSOrientation.Horizontal : iOSOrientation.Vertical);

        if (InvWrap.PanelCollection.Render())
          iOSWrap.Compose(InvWrap.PanelCollection.Select(E => TranslatePanel(E)));
      });

      return iOSNode;
    }
    internal void StartAnimation(Inv.Animation InvAnimation)
    {
      var iOSAnimationSet = new iOSAnimationSet();
      InvAnimation.Node = iOSAnimationSet;

      var InvLastTarget = InvAnimation.Targets.LastOrDefault();

      foreach (var InvTarget in InvAnimation.Targets)
      {
        var InvLastTransform = InvTarget.Transforms.LastOrDefault();

        var InvPanel = InvTarget.Panel;
        var iOSContainer = TranslatePanel(InvPanel);
        var iOSPanel = iOSContainer.ContentElement?.View ?? iOSContainer;

        var IsFirstExecute = true;

        void Execute()
        {
          if (iOSContainer.Window == null && IsFirstExecute)
          {
            IsFirstExecute = false;

            if (InvAnimation.IsActive)
              Post(() => Execute());
          }
          else
          {
            var AnimationCount = 0;

            void AnimationComplete()
            {
              if (--AnimationCount == 0)
                InvAnimation.Complete();
            }

            foreach (var InvTransform in InvTarget.Transforms)
              TranslateAnimationTransform(InvAnimation, InvPanel, iOSPanel, InvTransform, iOSAnimationSet, AnimationComplete);

            AnimationCount = iOSAnimationSet.Count;
          }
        }

        Execute();
      }
    }
    internal void StopAnimation(Inv.Animation InvAnimation)
    {
      if (InvAnimation.Node != null)
      {
        var iOSAnimationSet = (iOSAnimationSet)InvAnimation.Node;
        InvAnimation.Node = null;

        iOSAnimationSet.IsCancelled = true;

        foreach (var iOSAnimationItem in iOSAnimationSet.Get())
        {
          var InvPanel = iOSAnimationItem.InvPanel;
          var iOSPanel = iOSAnimationItem.iOSPanel;

          iOSAnimationItem.StopInvoke();

          iOSPanel.Layer.RemoveAnimation(iOSAnimationItem.iOSAnimationKey);
        }
      }
    }
    internal void ShowPopup(Inv.Popup InvPopup)
    {
      throw new NotImplementedException();
    }
    internal void HidePopup(Inv.Popup InvPopup)
    {
      throw new NotImplementedException();
    }
    internal void Terminate()
    {
      UIKit.UIApplication.SharedApplication.PerformSelector(new ObjCRuntime.Selector("terminateWithSuccess"), null, 0f);
      // ALTERNATIVE? [DllImport("__Internal", EntryPoint = "exit")] static extern void exit(int status); exit(0);
    }
    internal bool OpenUri(Uri Uri)
    {
      // TODO: OpenUrl is deprecated in iOS 10.
      return UIKit.UIApplication.SharedApplication.OpenUrl(new Foundation.NSUrl(Uri.AbsoluteUri));
    }
    internal void Toast(string Title, string Message, string Button)
    {
      var AlertView = new UIKit.UIAlertView(Title, Message, (UIKit.IUIAlertViewDelegate)null, Button, null);
      //AlertView.Dismissed += (Sender, Event) => AlertView.Dispose(); // TODO: test this behaviour.
      AlertView.Show();
    }
    internal void HapticFeedback(Inv.HapticFeedback Feedback)
    {
      if (Feedback == Inv.HapticFeedback.Shake)
      {
        AudioToolbox.SystemSound.Vibrate.PlaySystemSound();
      }
      else if (iOSFoundation.iOS10_0)
      {
        UIKit.UINotificationFeedbackType? iOSNotificationFeedback = null;
        UIKit.UIImpactFeedbackStyle? iOSImpactFeedback = null;

        switch (Feedback)
        {
          case Inv.HapticFeedback.SuccessNotify: iOSNotificationFeedback = UIKit.UINotificationFeedbackType.Success; break;
          case Inv.HapticFeedback.WarningNotify: iOSNotificationFeedback = UIKit.UINotificationFeedbackType.Warning; break;
          case Inv.HapticFeedback.ErrorNotify: iOSNotificationFeedback = UIKit.UINotificationFeedbackType.Error; break;
          case Inv.HapticFeedback.LightImpact: iOSImpactFeedback = UIKit.UIImpactFeedbackStyle.Light; break;
          case Inv.HapticFeedback.MediumImpact: iOSImpactFeedback = UIKit.UIImpactFeedbackStyle.Medium; break;
          case Inv.HapticFeedback.HeavyImpact: iOSImpactFeedback = UIKit.UIImpactFeedbackStyle.Heavy; break;
          default:
            throw EnumHelper.UnexpectedValueException(Feedback);
        }

        if (iOSImpactFeedback != null)
        {
          var ImpactFeedbackGenerator = iOSImpactFeedbackArray[iOSImpactFeedback.Value];
          if (ImpactFeedbackGenerator == null)
          {
            ImpactFeedbackGenerator = new UIKit.UIImpactFeedbackGenerator(iOSImpactFeedback.Value);
            ImpactFeedbackGenerator.Prepare();
            iOSImpactFeedbackArray[iOSImpactFeedback.Value] = ImpactFeedbackGenerator;
          }

          ImpactFeedbackGenerator.ImpactOccurred();
        }

        if (iOSNotificationFeedback != null)
        {
          if (iOSNotificationFeedbackGenerator == null)
          {
            this.iOSNotificationFeedbackGenerator = new UIKit.UINotificationFeedbackGenerator();
            iOSNotificationFeedbackGenerator.Prepare();
          }

          iOSNotificationFeedbackGenerator.NotificationOccurred(iOSNotificationFeedback.Value);
        }
      }
      else
      {
        // TODO what?
      }
    }
    internal Foundation.NSAttributedString NewAttributedString(string TextFragment, Inv.DrawFont TextFont)
    {
      var AttributedDictionary = new Foundation.NSMutableDictionary()
      {
        { new Foundation.NSString("NSColor"), TranslateUIColor(TextFont.Colour) },
        { new Foundation.NSString("NSFont"), TranslateUIFont(TextFont.Name.EmptyAsNull(), TextFont.Size, TextFont.Weight) }
      };

      return new Foundation.NSAttributedString(TextFragment, new CoreText.CTStringAttributes(AttributedDictionary));
    }

    private void Process()
    {
      if (InvApplication.IsExit)
      {
        Terminate();
      }
      else
      {
        Guard(() =>
        {
          var InvWindow = InvApplication.Window;
          InvWindow.ProcessInvoke();

          if (InvWindow.ActiveTimerSet.Count > 0)
          {
            foreach (var InvTimer in InvWindow.ActiveTimerSet)
            {
              var iOSTimer = AccessTimer(InvTimer, S =>
              {
                var Result = new System.Timers.Timer();
                Result.Elapsed += (Sender, Event) =>
                {
                  if (InvTimer.IsEnabled)
                  {
                    Post(() =>
                    {
                      if (InvTimer.IsEnabled)
                        InvTimer.IntervalInvoke();
                    });
                  }
                };
                return Result;
              });

              if (InvTimer.IsRestarting)
              {
                InvTimer.IsRestarting = false;
                iOSTimer.Stop();
              }

              if (iOSTimer.Interval != InvTimer.IntervalTime.TotalMilliseconds)
                iOSTimer.Interval = InvTimer.IntervalTime.TotalMilliseconds;

              if (InvTimer.IsEnabled && !iOSTimer.Enabled)
                iOSTimer.Start();
              else if (!InvTimer.IsEnabled && iOSTimer.Enabled)
                iOSTimer.Stop();
            }

            InvWindow.ActiveTimerSet.RemoveWhere(T => !T.IsEnabled);
          }

          var InvSurface = InvWindow.ActiveSurface;

          if (InvSurface != null)
          {
            var iOSSurface = TranslateSurface(InvSurface);

            iOSRootController.Reload();
          }

          if (InvWindow.Render())
          {
            if (iOSWindow.UserInteractionEnabled == InvWindow.InputPrevented) // check just in case setting this boolean when it's already set, costs something.
              iOSWindow.UserInteractionEnabled = !InvWindow.InputPrevented;

            // TODO: InvApplication.Title;
            if (InvWindow.Background.Render())
              iOSWindow.BackgroundColor = TranslateUIColor(InvWindow.Background.Colour ?? Inv.Colour.Black);

            // enable/disable interaction on panels (and their subpanels).
            foreach (var InvPanel in InvWindow.DisablePanelList)
            {
              var iOSContainer = TranslatePanel(InvPanel);
              if (iOSContainer != null)
                iOSContainer.UserInteractionEnabled = false;
            }
            InvWindow.DisablePanelList.Clear();

            foreach (var InvPanel in InvWindow.EnablePanelList)
            {
              var iOSContainer = TranslatePanel(InvPanel);
              if (iOSContainer != null)
                iOSContainer.UserInteractionEnabled = true;
            }
            InvWindow.EnablePanelList.Clear();
          }

          InvWindow.DisplayRate.Calculate();
        });
      }
    }
    private void UpdateSurface(Surface InvSurface, iOSSurface iOSSurface)
    {
      if (InvSurface.Render())
      {
        iOSRootController.IsLeftEdgeSwipe = InvSurface.HasGestureBackward;
        iOSRootController.IsRightEdgeSwipe = InvSurface.HasGestureForward;

        TranslateBackground(InvSurface.Background.Colour ?? InvSurface.Window.Background.Colour ?? Inv.Colour.Black, iOSSurface);

        iOSSurface.SetContent(TranslatePanel(InvSurface.Content));
      }
    }
    private void ProcessChanges()
    {
      InvApplication.Window.ProcessChanges(P => TranslatePanel(P));
    }
    private void ProcessTransition(iOSSurface iOSSurface)
    {
      var InvWindow = InvApplication.Window;
      var InvTransition = InvWindow.ActiveTransition;

      if (InvTransition == null)
      {
        // no transition.
      }
      else if (!IsTransitioning)
      {
        var iOSFromSurface = iOSRootController.Surface != null ? (iOSSurface)iOSRootController.Surface : null;

        if (iOSSurface == iOSFromSurface)
        {
          Debug.WriteLine("Transition to self");
        }
        else
        {
          // give the previous surface a chance to process before it is animated away.
          if (InvWindow.FromSurface != null)
          {
            if (TranslateSurface(InvWindow.FromSurface) == iOSFromSurface)
              UpdateSurface(InvWindow.FromSurface, iOSFromSurface);

            InvWindow.FromSurface = null;
          }

          ExecuteTransition(InvTransition, iOSRootController.View, iOSFromSurface, iOSSurface, () => { });
          iOSRootController.SetSurface(iOSSurface);
        }

        InvWindow.ActiveTransition = null;
      }
    }
    private void ExecuteTransition(Inv.Transition InvTransition, UIKit.UIView iOSMaster, UIKit.UIView iOSFromView, UIKit.UIView iOSToView, Action CompletedAction)
    {
      void CompleteTransition()
      {
        CompletedAction();
        InvTransition.Complete();
      }

      var AnimateDuration = InvTransition.Duration.TotalSeconds;
#if DEBUG
      //AnimateDuration *= 10; // slow down animations for testing.
#endif
      var AnimateOut = iOSFromView != null;
      var AnimateIn = iOSToView != null;

      switch (InvTransition.Type)
      {
        case TransitionType.None:
          if (AnimateOut)
            iOSMaster.SafeRemoveView(iOSFromView);

          if (AnimateIn)
            iOSMaster.SafeAddView(iOSToView);

          CompleteTransition();
          break;

        case TransitionType.Fade:
          if (!AnimateOut && !AnimateIn)
          {
            CompleteTransition();
          }
          else
          {
            this.TransitionCount++;

            var iOSFadeDuration = (AnimateOut && AnimateIn ? AnimateDuration / 2.0 : AnimateDuration);

            // NOTE: Apple have broken UIView.Animate in iOS 14 so we have to change how animations work.

            if (AnimateOut)
            {
              iOSFromView.FadeOut(iOSFadeDuration, 0.0, () =>
              {
                iOSMaster.SafeRemoveView(iOSFromView);
                iOSFromView.Alpha = 1.0F;

                if (!AnimateIn)
                {
                  this.TransitionCount--;
                  CompleteTransition();
                }
              }, false);
            }

            if (AnimateIn)
            {
              iOSMaster.SafeAddView(iOSToView);
              iOSToView.FadeIn(iOSFadeDuration, AnimateDuration - iOSFadeDuration, () =>
              {
                this.TransitionCount--;
                CompleteTransition();
              }, false);
            }
          }
          break;

        case TransitionType.CarouselNext:
        case TransitionType.CarouselPrevious:
        case TransitionType.CarouselAscend:
        case TransitionType.CarouselDescend:
          if (!AnimateOut && !AnimateIn)
          {
            CompleteTransition();
          }
          else
          {
            this.TransitionCount++;

            var CarouselForward = InvTransition.Type == TransitionType.CarouselNext || InvTransition.Type == TransitionType.CarouselAscend;
            var CarouselHorizontal = InvTransition.Type == TransitionType.CarouselNext || InvTransition.Type == TransitionType.CarouselPrevious;

            var iOSMasterWidth = iOSMaster.Frame.Width;
            var iOSMasterHeight = iOSMaster.Frame.Height;
            var iOSActualWidth = CarouselHorizontal ? iOSMasterWidth : 0.0F;
            var iOSActualHeight = !CarouselHorizontal ? iOSMasterHeight : 0.0F;

            if (AnimateOut)
            {
              UIKit.UIView.Animate(AnimateDuration, () => iOSFromView.Frame = new CoreGraphics.CGRect(CarouselForward ? -iOSActualWidth : iOSActualWidth, CarouselForward ? -iOSActualHeight : iOSActualHeight, iOSMasterWidth, iOSMasterHeight), () =>
              {
                iOSMaster.SafeRemoveView(iOSFromView);

                if (!AnimateIn)
                {
                  this.TransitionCount--;
                  CompleteTransition();
                }
              });
            }

            if (AnimateIn)
            {
              iOSMaster.SafeAddView(iOSToView);

              iOSToView.Frame = new CoreGraphics.CGRect(CarouselForward ? iOSActualWidth : -iOSActualWidth, CarouselForward ? iOSActualHeight : -iOSActualHeight, iOSMasterWidth, iOSMasterHeight);
              UIKit.UIView.Animate(AnimateDuration, () => iOSToView.Frame = new CoreGraphics.CGRect(0, 0, iOSMasterWidth, iOSMasterHeight), () =>
              {
                this.TransitionCount--;
                CompleteTransition();
              });
            }
          }
          break;

        default:
          throw EnumHelper.UnexpectedValueException(InvTransition.Type);
      }
    }
    private void TranslateAnimationTransform(Inv.Animation InvAnimation, Inv.Panel InvPanel, UIKit.UIView iOSPanel, AnimationTransform InvTransform, iOSAnimationSet iOSAnimationSet, Action CompleteAction)
    {
      switch (InvTransform.Type)
      {
        case AnimationType.Fade:
          TranslateAnimationFadeTransform(InvAnimation, InvPanel, iOSPanel, (AnimationFadeTransform)InvTransform, iOSAnimationSet, CompleteAction);
          break;

        case AnimationType.Rotate:
          TranslateAnimationRotateTransform(InvAnimation, InvPanel, iOSPanel, (AnimationRotateTransform)InvTransform, iOSAnimationSet, CompleteAction);
          break;

        case AnimationType.Scale:
          TranslateAnimationScaleTransform(InvAnimation, InvPanel, iOSPanel, (AnimationScaleTransform)InvTransform, iOSAnimationSet, CompleteAction);
          break;

        case AnimationType.Translate:
          TranslateAnimationTranslateTransform(InvAnimation, InvPanel, iOSPanel, (AnimationTranslateTransform)InvTransform, iOSAnimationSet, CompleteAction);
          break;

        default:
          throw EnumHelper.UnexpectedValueException(InvTransform.Type);
      }
    }
    private void TranslateAnimationFadeTransform(Inv.Animation InvAnimation, Inv.Panel InvPanel, UIKit.UIView iOSPanel, AnimationFadeTransform InvTransform, iOSAnimationSet iOSAnimationSet, Action CompleteAction)
    {
      var InvControl = InvPanel.Control;
      var FromOpacity = float.IsNaN(InvTransform.FromOpacity) ? iOSPanel.Layer.Opacity : InvTransform.FromOpacity;
      var ToOpacity = InvTransform.ToOpacity;

      var Result = CoreAnimation.CABasicAnimation.FromKeyPath("opacity");
      Result.From = new Foundation.NSNumber(FromOpacity);
      Result.To = new Foundation.NSNumber(ToOpacity);
      Result.Duration = InvTransform.Duration.TotalSeconds;
      Result.TimingFunction = CoreAnimation.CAMediaTimingFunction.FromName(CoreAnimation.CAMediaTimingFunction.EaseInEaseOut);
      Result.FillMode = CoreAnimation.CAFillMode.Forwards;

      if (InvTransform.Offset != null && InvTransform.Offset.Value > TimeSpan.Zero)
        Result.BeginTime = CoreAnimation.CAAnimation.CurrentMediaTime() + InvTransform.Offset.Value.TotalSeconds;

      iOSPanel.Layer.Opacity = FromOpacity;
      InvControl.Opacity.BypassSet(ToOpacity);

      Result.AnimationStarted += (Sender, Event) => //WeakHelpers.WeakEventHandlerWrapper(InvAnimation, iOSPanel, InvPanel, InvTransform, (invAnimation, iosPanel, invPanel, invTransform, Sender, Event) =>
      {
        if (!iOSAnimationSet.IsCancelled)
          iOSPanel.Layer.Opacity = Math.Max(0.02F, ToOpacity); // if fading completely to zero, the view will not be touchable unless we do this 0.02 hack. The layer opacity will be set to 0.00 in the AnimationStopped event.
      };
      Result.AnimationStopped += (Sender, Event) => //WeakHelpers.WeakEventHandlerWrapper<Inv.Animation, AnimationFadeOpacityTransform, Action, CoreAnimation.CAAnimationStateEventArgs>(InvAnimation, InvTransform, CompleteAction, (invAnimation, invTransform, completeAction, Sender, Event) =>
      {
        if (!iOSAnimationSet.IsCancelled)
        {
          iOSPanel.Layer.Opacity = ToOpacity;
          CompleteAction();
        }
      };

      iOSAnimationSet.Add(InvPanel, iOSPanel, Result).StopEvent += () =>
      {
        var PresentationOpacity = iOSPanel.Layer.PresentationLayer?.Opacity ?? iOSPanel.Layer.Opacity;
        iOSPanel.Layer.Opacity = PresentationOpacity;
        InvControl.Opacity.BypassSet(PresentationOpacity);
      };
    }
    private void TranslateAnimationRotateTransform(Inv.Animation InvAnimation, Inv.Panel InvPanel, UIKit.UIView iOSPanel, AnimationRotateTransform InvTransform, iOSAnimationSet iOSAnimationSet, Action CompleteAction)
    {
      var CurrentAngle = ((Foundation.NSNumber)iOSPanel.ValueForKeyPath(new Foundation.NSString("layer.transform.rotation.z"))).NFloatValue;
      var FromAngle = float.IsNaN(InvTransform.FromAngle) ? CurrentAngle : iOSFoundation.DegreesToRadians(InvTransform.FromAngle);
      var ToAngle = float.IsNaN(InvTransform.ToAngle) ? CurrentAngle : iOSFoundation.DegreesToRadians(InvTransform.ToAngle);
      var Duration = InvTransform.Duration.TotalSeconds;

      Debug.WriteLine("ROTATE: " + FromAngle + ", " + ToAngle);

      /*
      var AnimationKey = iOSAnimationSet.NewAnimationKey();

      UIKit.UIView.BeginAnimations(AnimationKey);

      iOSPanel.Rotate(FromAngle, ToAngle, Duration, InvTransform.Offset?.TotalSeconds ?? 0, () => // WeakHelpers.WeakWrapper(InvAnimation, InvTransform, CompleteAction, (invAnimation, invTransform, completeAction) =>
      {
        if (!iOSAnimationSet.IsCancelled)
          CompleteAction();
      }, true);

      UIKit.UIView.CommitAnimations();

      iOSAnimationSet.Add(InvPanel, iOSPanel, AnimationKey).StopEvent += () =>
      {
      };
      */

      var Result = CoreAnimation.CABasicAnimation.FromKeyPath("transform.rotation");
      Result.From = new Foundation.NSNumber(FromAngle);
      Result.To = new Foundation.NSNumber(ToAngle);
      Result.Duration = InvTransform.Duration.TotalSeconds;
      Result.TimingFunction = CoreAnimation.CAMediaTimingFunction.FromName(CoreAnimation.CAMediaTimingFunction.EaseInEaseOut);
      Result.FillMode = CoreAnimation.CAFillMode.Forwards;

      if (InvTransform.Offset != null)
        Result.BeginTime = CoreAnimation.CAAnimation.CurrentMediaTime() + InvTransform.Offset.Value.TotalSeconds;

      iOSPanel.Transform = CoreGraphics.CGAffineTransform.MakeRotation(FromAngle);

      Result.AnimationStarted += (Sender, Event) => //WeakHelpers.WeakEventHandlerWrapper(InvAnimation, iOSPanel, InvPanel, InvTransform, (invAnimation, iosPanel, invPanel, invTransform, Sender, Event) =>
      {
        if (!iOSAnimationSet.IsCancelled)
          iOSPanel.Transform = CoreGraphics.CGAffineTransform.MakeRotation(ToAngle);
      };
      Result.AnimationStopped += (Sender, Event) => //WeakHelpers.WeakEventHandlerWrapper<Inv.Animation, AnimationFadeOpacityTransform, Action, CoreAnimation.CAAnimationStateEventArgs>(InvAnimation, InvTransform, CompleteAction, (invAnimation, invTransform, completeAction, Sender, Event) =>
      {
        if (!iOSAnimationSet.IsCancelled)
          CompleteAction();
      };

      iOSAnimationSet.Add(InvPanel, iOSPanel, Result).StopEvent += () =>
      {
        iOSPanel.Transform = CoreGraphics.CGAffineTransform.MakeRotation(-((Foundation.NSNumber)iOSPanel.ValueForKeyPath(new Foundation.NSString("layer.transform.rotation.z"))).NFloatValue);
      };
    }
    private void TranslateAnimationScaleTransform(Inv.Animation InvAnimation, Inv.Panel InvPanel, UIKit.UIView iOSPanel, AnimationScaleTransform InvTransform, iOSAnimationSet iOSAnimationSet, Action CompleteAction)
    {
      var FromX = float.IsNaN(InvTransform.FromWidth) ? iOSPanel.Transform.xx : InvTransform.FromWidth;
      var FromY = float.IsNaN(InvTransform.FromHeight) ? iOSPanel.Transform.yy : InvTransform.FromHeight;
      var ToX = float.IsNaN(InvTransform.ToWidth) ? iOSPanel.Transform.xx : InvTransform.ToWidth;
      var ToY = float.IsNaN(InvTransform.ToHeight) ? iOSPanel.Transform.yy : InvTransform.ToHeight;
      var Duration = InvTransform.Duration.TotalSeconds;

      //Debug.WriteLine("SCALE FROM: " + FromX + ", " + FromY);
      //Debug.WriteLine("SCALE TO: " + ToX + ", " + ToY);

      var AnimationKey = iOSAnimationSet.NewAnimationKey();

      UIKit.UIView.BeginAnimations(AnimationKey);

      iOSPanel.Scale(FromX, ToX, FromY, ToY, Duration, InvTransform.Offset?.TotalSeconds ?? 0, () => // WeakHelpers.WeakWrapper(InvAnimation, InvTransform, CompleteAction, (invAnimation, invTransform, completeAction) =>
      {
        if (!iOSAnimationSet.IsCancelled)
          CompleteAction();
      }, true);

      UIKit.UIView.CommitAnimations();

      iOSAnimationSet.Add(InvPanel, iOSPanel, AnimationKey).StopEvent += () =>
      {
      };

      /*
      var ScalingX = CoreAnimation.CABasicAnimation.FromKeyPath("transform.scale.x");
      ScalingX.From = new Foundation.NSNumber(FromX);
      ScalingX.To = new Foundation.NSNumber(ToX);
      ScalingX.Duration = Duration;
      ScalingX.TimingFunction = CoreAnimation.CAMediaTimingFunction.FromName(CoreAnimation.CAMediaTimingFunction.EaseInEaseOut);
      ScalingX.FillMode = CoreAnimation.CAFillMode.Forwards;
      //ScalingX.RemovedOnCompletion = false;

      var ScalingY = CoreAnimation.CABasicAnimation.FromKeyPath("transform.scale.y");
      ScalingY.From = new Foundation.NSNumber(FromY);
      ScalingY.To = new Foundation.NSNumber(ToY);
      ScalingY.Duration = Duration;
      ScalingY.TimingFunction = CoreAnimation.CAMediaTimingFunction.FromName(CoreAnimation.CAMediaTimingFunction.EaseInEaseOut);
      ScalingY.FillMode = CoreAnimation.CAFillMode.Forwards;
      //ScalingY.RemovedOnCompletion = false;

      iOSPanel.Transform = CoreGraphics.CGAffineTransform.MakeScale(FromX, FromY);

      //ScalingX.AnimationStarted += WeakHelpers.WeakEventHandlerWrapper(InvAnimation, iOSPanel, InvPanel, InvTransform, (invAnimation, iosPanel, invPanel, invTransform, Sender, Event) =>
      //{
      //});
      ScalingY.AnimationStopped += WeakHelpers.WeakEventHandlerWrapper<UIKit.UIView, Inv.Animation, AnimationScaleSizeTransform, Action, CoreAnimation.CAAnimationStateEventArgs>(iOSPanel, InvAnimation, InvTransform, CompleteAction, (iosPanel, invAnimation, invTransform, completeAction, Sender, Event) =>
      {
        if (!iOSAnimationSet.IsCancelled)
        {
          iosPanel.Transform = CoreGraphics.CGAffineTransform.MakeScale(ToX, ToY);

          invTransform.Complete();

          // two animations are returned.
          completeAction();
          completeAction();
        }
      });

      if (InvTransform.Offset != null)
      {
        var BeginTime = CoreAnimation.CAAnimation.CurrentMediaTime() + InvTransform.Offset.Value.TotalSeconds;

        ScalingX.BeginTime = BeginTime;
        ScalingY.BeginTime = BeginTime;
      }

      yield return ScalingX;
      yield return ScalingY;
      */
    }
    private void TranslateAnimationTranslateTransform(Inv.Animation InvAnimation, Inv.Panel InvPanel, UIKit.UIView iOSPanel, AnimationTranslateTransform InvTransform, iOSAnimationSet iOSAnimationSet, Action CompleteAction)
    {
      var FromX = InvTransform.FromX == null ? iOSPanel.Transform.x0 : -InvTransform.FromX.Value;
      var FromY = InvTransform.FromY == null ? iOSPanel.Transform.y0 : -InvTransform.FromY.Value;
      var ToX = InvTransform.ToX == null ? iOSPanel.Transform.x0 : -InvTransform.ToX.Value;
      var ToY = InvTransform.ToY == null ? iOSPanel.Transform.y0 : -InvTransform.ToY.Value;
      var Duration = InvTransform.Duration.TotalSeconds;

      //Debug.WriteLine("TRANSLATE FROM: " + FromX + ", " + FromY);
      //Debug.WriteLine("TRANSLATE TO: " + ToX + ", " + ToY);

      var AnimationKey = iOSAnimationSet.NewAnimationKey();

      UIKit.UIView.BeginAnimations(AnimationKey);

      iOSPanel.Translate(FromX, ToX, FromY, ToY, Duration, InvTransform.Offset?.TotalSeconds ?? 0, () => // WeakHelpers.WeakWrapper(InvAnimation, InvTransform, CompleteAction, (invAnimation, invTransform, completeAction) =>
      {
        if (!iOSAnimationSet.IsCancelled)
          CompleteAction();
      }, true);

      UIKit.UIView.CommitAnimations();

      iOSAnimationSet.Add(InvPanel, iOSPanel, AnimationKey).StopEvent += () =>
      {
      };
    }
    private AVFoundation.AVAudioPlayer LoadSound(Inv.Sound InvSound)
    {
      var SoundBuffer = InvSound.GetBuffer();

      // NOTE: this is an attempted workaround when a sound cannot be played:
      //       "Could not initialize an instance of the type 'AVFoundation.AVAudioPlayer': the native 'initWithContentsOfURL:error:' method returned nil."

      Foundation.NSError Error;
      var Retry = 0;
      do
      {
        var Result = AVFoundation.AVAudioPlayer.FromData(Foundation.NSData.FromArray(SoundBuffer), out Error);
        if (Error == null)
          return Result;

        Error.Dispose();
        System.Threading.Thread.Sleep(10);

        Retry++;
      }
      while (Error != null && Retry < 100);

      return null;
    }
    private AVFoundation.AVAudioPlayer GetSound(Inv.Sound InvSound)
    {
      if (InvSound == null)
        return null;

      var iOSSound = InvSound.Node as AVFoundation.AVAudioPlayer;

      if (iOSSound == null)
      {
        iOSSound = LoadSound(InvSound);
        if (iOSSound == null)
          return null;

        InvSound.Node = iOSSound;
      }

      return iOSSound;
    }
    private CoreGraphics.CGRect TranslateRect(Inv.Rect InvRect)
    {
      return new CoreGraphics.CGRect(InvRect.Left, InvRect.Top, InvRect.Width, InvRect.Height);
    }
    private iOSContainer TranslatePanel(Inv.Panel InvPanel)
    {
      var InvControl = InvPanel?.Control;

      if (InvPanel == null)
        return null;
      else
        return RouteArray[InvControl.ControlType](InvControl).Container;
    }
    private void TranslateLayout(Inv.Control InvControl, iOSContainer iOSContainer, iOSElement iOSElement)
    {
      var iOSView = iOSElement.View;

      if (InvControl.Opacity.Render())
        iOSView.Alpha = InvControl.Opacity.Get();

      var InvBackground = InvControl.Background;
      if (InvBackground.Render())
        TranslateBackground(InvBackground.Colour, iOSView);

      var InvCorner = InvControl.Corner;

      var InvBorder = InvControl.Border;
      var RenderBorder = InvBorder.IsChanged;

      var RenderBorderOnCornerRadius = InvCorner.Render();
      if (InvBorder.Render())
        RenderBorderOnCornerRadius = true;

      if (RenderBorderOnCornerRadius)
      {
        var TranslatedColour = TranslateCGColor(InvBorder.Colour);

        // NOTE: iOS is the only platform which renders corner radii greater than half the largest
        // dimension of a view (going from a circle to a diamond shape for square views). Other
        // platforms appear to cap out at forming a circle. The Bezier curve circle approximation
        // code (which runs when not all corner radii are equal) will also cap out at a circle.
        if (InvBorder.IsUniform && InvCorner.IsUniform)
        {
          iOSView.Layer.BorderColor = TranslatedColour;
          iOSView.Layer.BorderWidth = InvBorder.Left;
          iOSView.Layer.CornerRadius = InvCorner.TopLeft;
          //iOSView.Layer.MasksToBounds = true;

          iOSElement.Border.TopLeftCornerRadius = 0;
          iOSElement.Border.TopRightCornerRadius = 0;
          iOSElement.Border.BottomRightCornerRadius = 0;
          iOSElement.Border.BottomLeftCornerRadius = 0;

          iOSElement.Border.LeftBorderThickness = 0;
          iOSElement.Border.TopBorderThickness = 0;
          iOSElement.Border.RightBorderThickness = 0;
          iOSElement.Border.BottomBorderThickness = 0;
        }
        else
        {
          iOSView.Layer.BorderWidth = 0;
          iOSView.Layer.CornerRadius = 0;
          //iOSView.Layer.MasksToBounds = false;

          iOSElement.Border.TopLeftCornerRadius = InvCorner.TopLeft;
          iOSElement.Border.TopRightCornerRadius = InvCorner.TopRight;
          iOSElement.Border.BottomRightCornerRadius = InvCorner.BottomRight;
          iOSElement.Border.BottomLeftCornerRadius = InvCorner.BottomLeft;

          iOSElement.Border.LeftBorderThickness = InvBorder.Left;
          iOSElement.Border.TopBorderThickness = InvBorder.Top;
          iOSElement.Border.RightBorderThickness = InvBorder.Right;
          iOSElement.Border.BottomBorderThickness = InvBorder.Bottom;

          iOSElement.Border.BorderColour = TranslatedColour;
        }

        iOSView.SetNeedsLayout();
      }

      var InvElevation = InvControl.Elevation;
      if (InvElevation.Render())
      {
        var Depth = InvElevation.Get();

        if (Depth > 0)
        {
          iOSView.Layer.ShadowColor = TranslateCGColor(Inv.Colour.Black);
          iOSView.Layer.ShadowOffset = new CoreGraphics.CGSize(0, Depth);
          iOSView.Layer.ShadowRadius = Depth;
          iOSView.Layer.ShadowOpacity = 0.7F;
        }
        else
        {
          //iOSView.Layer.ShadowColor = null;
          iOSView.Layer.ShadowOffset = CoreGraphics.CGSize.Empty;
          iOSView.Layer.ShadowRadius = 0;
          iOSView.Layer.ShadowOpacity = 0.0F;
        }
      }

      var InvMargin = InvControl.Margin;

      if (InvMargin.Render())
      {
        iOSContainer.LayoutMargins = new UIKit.UIEdgeInsets(InvMargin.Top, InvMargin.Left, InvMargin.Bottom, InvMargin.Right);
        iOSContainer.Arrange();
      }

      var InvPadding = InvControl.Padding;
      if (InvPadding.Render() || RenderBorder)
      {
        iOSView.LayoutMargins = new UIKit.UIEdgeInsets(InvPadding.Top + InvControl.Border.Top, InvPadding.Left + InvControl.Border.Left, InvPadding.Bottom + InvControl.Border.Bottom, InvPadding.Right + InvControl.Border.Right);
        iOSView.Arrange();
      }

      TranslateVisibilitySizeAlignment(InvControl, iOSContainer);

      iOSContainer.SizeChanged -= PanelSizeChanged;
      if (InvControl.HasAdjust)
        iOSContainer.SizeChanged += PanelSizeChanged;
    }
    private void TranslateVisibilitySizeAlignment(Inv.Control InvControl, iOSContainer iOSContainer)
    {
      var InvVisibility = InvControl.Visibility;
      if (InvVisibility.Render())
        iOSContainer.SetContentVisibility(InvVisibility.Get());

      var InvSize = InvControl.Size;
      if (InvSize.Render())
      {
        iOSContainer.SetContentWidth(InvSize.Width);
        iOSContainer.SetContentHeight(InvSize.Height);
        iOSContainer.SetContentMinimumWidth(InvSize.MinimumWidth);
        iOSContainer.SetContentMinimumHeight(InvSize.MinimumHeight);
        iOSContainer.SetContentMaximumWidth(InvSize.MaximumWidth);
        iOSContainer.SetContentMaximumHeight(InvSize.MaximumHeight);
      }

      var InvAlignment = InvControl.Alignment;
      if (InvAlignment.Render())
      {
        switch (InvAlignment.Get())
        {
          case Placement.BottomStretch:
            iOSContainer.SetContentAlignment(iOSVertical.Bottom, iOSHorizontal.Stretch);
            break;

          case Placement.BottomLeft:
            iOSContainer.SetContentAlignment(iOSVertical.Bottom, iOSHorizontal.Left);
            break;

          case Placement.BottomCenter:
            iOSContainer.SetContentAlignment(iOSVertical.Bottom, iOSHorizontal.Center);
            break;

          case Placement.BottomRight:
            iOSContainer.SetContentAlignment(iOSVertical.Bottom, iOSHorizontal.Right);
            break;

          case Placement.TopStretch:
            iOSContainer.SetContentAlignment(iOSVertical.Top, iOSHorizontal.Stretch);
            break;

          case Placement.TopLeft:
            iOSContainer.SetContentAlignment(iOSVertical.Top, iOSHorizontal.Left);
            break;

          case Placement.TopCenter:
            iOSContainer.SetContentAlignment(iOSVertical.Top, iOSHorizontal.Center);
            break;

          case Placement.TopRight:
            iOSContainer.SetContentAlignment(iOSVertical.Top, iOSHorizontal.Right);
            break;

          case Placement.CenterStretch:
            iOSContainer.SetContentAlignment(iOSVertical.Center, iOSHorizontal.Stretch);
            break;

          case Placement.CenterLeft:
            iOSContainer.SetContentAlignment(iOSVertical.Center, iOSHorizontal.Left);
            break;

          case Placement.Center:
            iOSContainer.SetContentAlignment(iOSVertical.Center, iOSHorizontal.Center);
            break;

          case Placement.CenterRight:
            iOSContainer.SetContentAlignment(iOSVertical.Center, iOSHorizontal.Right);
            break;

          case Placement.Stretch:
            iOSContainer.SetContentAlignment(iOSVertical.Stretch, iOSHorizontal.Stretch);
            break;

          case Placement.StretchLeft:
            iOSContainer.SetContentAlignment(iOSVertical.Stretch, iOSHorizontal.Left);
            break;

          case Placement.StretchCenter:
            iOSContainer.SetContentAlignment(iOSVertical.Stretch, iOSHorizontal.Center);
            break;

          case Placement.StretchRight:
            iOSContainer.SetContentAlignment(iOSVertical.Stretch, iOSHorizontal.Right);
            break;

          default:
            throw EnumHelper.UnexpectedValueException(InvAlignment.Get());
        }
      }
    }
    private void TranslateBackground(Inv.Colour InvColour, UIKit.UIView iOSView)
    {
      iOSView.BackgroundColor = TranslateUIColor(InvColour);

      // TODO: this appears to have no useful effect?
      //iOSView.Opaque = true;// InvColour.IsOpaque;
    }
    private void TranslateFont(Inv.Font InvFallbackFont, Inv.Font InvSpecificFont, out UIKit.UIFont iOSFont, out UIKit.UIColor iOSColor)
    {
      iOSFont = TranslateUIFont(InvSpecificFont.Name ?? InvFallbackFont.Name, InvSpecificFont.Size ?? InvFallbackFont.Size ?? 14, InvSpecificFont.Weight ?? InvFallbackFont.Weight ?? Inv.FontWeight.Regular);
      iOSColor = TranslateUIColor(InvSpecificFont.Colour ?? InvFallbackFont.Colour ?? Inv.Colour.Black);

      var iOSSmallCaps = (InvSpecificFont.IsSmallCaps ?? InvFallbackFont.IsSmallCaps ?? false);
      var iOSItalics = (InvSpecificFont.IsItalics ?? InvFallbackFont.IsItalics ?? false);

      if (iOSSmallCaps || iOSItalics)
      {
        var iOSFontAttributes = new UIKit.UIFontAttributes();

        if (iOSSmallCaps)
        {
          iOSFontAttributes.FeatureSettings = new[]
          {
            new UIKit.UIFontFeature(CoreText.CTFontFeatureLowerCase.Selector.LowerCaseSmallCaps), // turns lower case letters into small caps.
            new UIKit.UIFontFeature(CoreText.CTFontFeatureNumberCase.Selector.UpperCaseNumbers)   // TODO: we want numbers to be in uppercase, but this doesn't work.
          };
        }

        if (iOSItalics)
        {
          iOSFontAttributes.Traits = new UIKit.UIFontTraits()
          {
            SymbolicTrait = UIKit.UIFontDescriptorSymbolicTraits.Italic
          };

          if ((InvSpecificFont.Weight ?? InvFallbackFont.Weight) == FontWeight.Bold)
            iOSFontAttributes.Traits.SymbolicTrait |= UIKit.UIFontDescriptorSymbolicTraits.Bold;
        }

        var Descriptor = iOSFont.FontDescriptor.CreateWithAttributes(iOSFontAttributes);
        iOSFont = UIKit.UIFont.FromDescriptor(Descriptor, iOSFont.PointSize);
      }
    }
    private void TranslateFont(Inv.Font InvFont, Action<UIKit.UIFont, UIKit.UIColor> Action)
    {
      if (InvFont.Render())
      {
        TranslateFont(InvFont, InvFont, out var FontFace, out var FontColor); // TODO: annoying that we have to pass the InvFont twice.
        Action(FontFace, FontColor);
      }
    }
    private void TranslateFocus(Inv.Focus InvFocus, UIKit.UIView iOSView)
    {
      if (InvFocus.Render())
      {
        // TODO: (dis)connect the GotFocus / LostFocus events from the control?
      }
    }
    private void TranslateTooltip(Inv.Tooltip InvTooltip, UIKit.UIView iOSView)
    {
      if (InvTooltip.Render())
      {
        // TODO: does iOS support tooltips?
      }
    }
    private UIKit.UIViewContentMode TranslateFit(Inv.FitMethod InvFitMethod)
    {
      switch (InvFitMethod)
      {
        case Inv.FitMethod.Original:
          return UIKit.UIViewContentMode.Center;

        case Inv.FitMethod.Contain:
          return UIKit.UIViewContentMode.ScaleAspectFit;

        case Inv.FitMethod.Stretch:
          return UIKit.UIViewContentMode.ScaleToFill;

        case Inv.FitMethod.Cover:
          return UIKit.UIViewContentMode.ScaleAspectFill;

        default:
          throw EnumHelper.UnexpectedValueException(InvFitMethod);
      }
    }
    private UIKit.UITextAlignment TranslateJustify(Inv.Justify InvJustify)
    {
      switch (InvJustify.Get())
      {
        case Inv.Justification.Left:
          return UIKit.UITextAlignment.Left;

        case Inv.Justification.Center:
          return UIKit.UITextAlignment.Center;

        case Inv.Justification.Right:
          return UIKit.UITextAlignment.Right;

        default:
          throw EnumHelper.UnexpectedValueException(InvJustify.Get());
      }
    }
    private Inv.Point TranslatePoint(CoreGraphics.CGPoint iOSPoint)
    {
      return new Inv.Point((int)iOSPoint.X, (int)iOSPoint.Y);
    }
    private void PanelSizeChanged(Inv.Control InvControlTag)
    {
      if (InvControlTag != null)
        InvControlTag.AdjustInvoke();
    }
    private System.Timers.Timer AccessTimer(Inv.WindowTimer InvTimer, Func<Inv.WindowTimer, System.Timers.Timer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (System.Timers.Timer)InvTimer.Node;
      }
    }
    private iOSNode<TView> AccessPanel<TControl, TView>(TControl InvControl, Func<TControl, TView> BuildFunction)
      where TControl : Inv.Control
      where TView : iOSElement
    {
      if (InvControl.Node == null)
      {
        var Result = new iOSNode<TView>();
        Result.Container = new iOSContainer(InvControl);
        Result.Element = BuildFunction(InvControl);
        Result.Container.SetContentElement(Result.Element);

        InvControl.Node = Result;

        return Result;
      }
      else
      {
        return (iOSNode<TView>)InvControl.Node;
      }
    }
    private void RenderPanel<TView>(Inv.Control InvControl, iOSNode<TView> iOSNode, Action<iOSContainer, TView> Action)
      where TView : iOSElement
    {
      if (InvControl.Render())
      {
        // trap any exceptions as it will cascade to disrupt other panels that may not have any problems.
        try
        {
          Action(iOSNode.Container, iOSNode.Element);
        }
        catch (Exception Exception)
        {
          InvApplication.HandleExceptionInvoke(Exception);
        }
      }
    }
    private Inv.DeviceTheme GetOperatingSystemTheme()
    {
      try
      {
        if (iOSFoundation.iOS12_0)
        {
          // TraitCollection.UserInterfaceStyle was introduced in iOS 12.0.
          switch (iOSRootController.TraitCollection.UserInterfaceStyle)
          {
            case UIKit.UIUserInterfaceStyle.Light:
              return Inv.DeviceTheme.Light;

            case UIKit.UIUserInterfaceStyle.Dark:
              return Inv.DeviceTheme.Dark;
          }
        }
      }
      catch
      {
        // in case of access errors, default theme is not important enough to prevent startup.
      }

      return Inv.DeviceTheme.Light;
    }
    private bool RefreshWindowSize()
    {
      var ScreenBounds = UIKit.UIScreen.MainScreen.Bounds;
      var NewWidth = (int)ScreenBounds.Width;
      var NewHeight = (int)ScreenBounds.Height;

      if (NewWidth != InvApplication.Window.Width || NewHeight != InvApplication.Window.Height)
      {
        iOSWindow.Frame = ScreenBounds;

        InvApplication.Window.Width = NewWidth;
        InvApplication.Window.Height = NewHeight;

        return true;
      }

      return false;
    }

    private readonly Inv.EnumArray<Inv.ControlType, Func<Inv.Control, iOSNode>> RouteArray;
    private readonly Inv.EnumArray<Inv.FontWeight, UIKit.UIFontWeight> iOSFontWeightArray;
    private readonly EnumArray<FontWeight, string> iOSFontFamilyArray;
    private CoreLocation.CLLocationManager iOSLocationManager;
    private CoreAnimation.CADisplayLink iOSDisplayLink;
    private UIKit.UIDocumentInteractionController iOSDocumentInteractionController;
    private string iOSInteractTemporaryPath;
    private iOSDateTimePicker iOSDateTimePicker;
    private readonly Inv.EnumArray<UIKit.UIImpactFeedbackStyle, UIKit.UIImpactFeedbackGenerator> iOSImpactFeedbackArray;
    private UIKit.UINotificationFeedbackGenerator iOSNotificationFeedbackGenerator;
    private int TransitionCount;

    private sealed class iOSAnimationSet
    {
      public iOSAnimationSet()
      {
        this.ItemList = new DistinctList<iOSAnimationItem>();
      }

      public bool IsCancelled { get; set; }

      public string NewAnimationKey()
      {
        return "a_" + GlobalAnimationCount++;
      }
      public iOSAnimationItem Add(Inv.Panel InvPanel, UIKit.UIView iOSPanel, CoreAnimation.CAAnimation iOSAnimation)
      {
        var iOSAnimationKey = NewAnimationKey();
        iOSPanel.Layer.AddAnimation(iOSAnimation, iOSAnimationKey);
        return Add(InvPanel, iOSPanel, iOSAnimationKey);
      }
      public iOSAnimationItem Add(Inv.Panel InvPanel, UIKit.UIView iOSPanel, string iOSAnimationKey)
      {
        var Result = new iOSAnimationItem(InvPanel, iOSPanel, iOSAnimationKey);
        ItemList.Add(Result);
        return Result;
      }
      public IEnumerable<iOSAnimationItem> Get()
      {
        return ItemList;
      }
      public int Count
      {
        get { return ItemList.Count; }
      }

      private readonly Inv.DistinctList<iOSAnimationItem> ItemList;

      private static int GlobalAnimationCount;
    }

    private sealed class iOSAnimationItem
    {
      public iOSAnimationItem(Inv.Panel InvPanel, UIKit.UIView iOSPanel, string iOSAnimationKey)
      {
        this.InvPanel = InvPanel;
        this.iOSPanel = iOSPanel;
        this.iOSAnimationKey = iOSAnimationKey;
      }

      public readonly Inv.Panel InvPanel;
      public readonly UIKit.UIView iOSPanel;
      public readonly string iOSAnimationKey;
      public event Action StopEvent;

      public void StopInvoke()
      {
        StopEvent?.Invoke();
      }
    }
  }

  internal abstract class iOSNode
  {
    public iOSContainer Container { get; set; }
    public iOSElement Element { get; set; }
  }

  internal sealed class iOSNode<T> : iOSNode
    where T : iOSElement
  {
    public new T Element
    {
      get => (T)base.Element;
      set => base.Element = value;
    }
  }

  internal static class iOSDateTimeExtensions
  {
    public static DateTime NSDateToDateTime(this Foundation.NSDate Source)
    {
      if (Source == null)
        return DateTime.MinValue;
      else
        return new DateTime(2001, 1, 1, 0, 0, 0).AddSeconds(Source.SecondsSinceReferenceDate);
    }
    public static Foundation.NSDate DateTimeToNSDate(this DateTime Source)
    {
      return Foundation.NSDate.FromTimeIntervalSinceReferenceDate((Source - new DateTime(2001, 1, 1, 0, 0, 0)).TotalSeconds);
    }
  }

  internal sealed class iOSSpeechRecognition
  {
    // NOTE: SpeechRecognition was introduced in iOS 10.
    //       iOS 13.2 is a buggy version of iOS that breaks speechrecognition API (final transcriptions are not returned).
    public static bool IsSupported => iOSFoundation.iOS10_0 && !(iOSFoundation.iOS13_2 && !iOSFoundation.iOS13_3);

    public event Action<Speech.SFTranscription, bool> TranscriptionEvent;

    public void Start()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        Debug.WriteLine($"[{CaptureSessionCount}] SpeechRecognition: START");

        this.iOSSpeechRecognizer = new Speech.SFSpeechRecognizer();
        this.iOSAudioEngine = new AVFoundation.AVAudioEngine();

        ToggleAudioSession(Active: true);

        Speech.SFSpeechRecognizer.RequestAuthorization(S =>
        {
          Debug.WriteLine($"[{CaptureSessionCount}] SpeechRecognition: Authorization {S}");

          switch (S)
          {
            case Speech.SFSpeechRecognizerAuthorizationStatus.Authorized:
              MakeRequest();

              this.iOSAudioNode = iOSAudioEngine.InputNode;
              var iOSAudioFormat = iOSAudioNode.GetBusOutputFormat(0);
              iOSAudioNode.InstallTapOnBus(0, 1024, iOSAudioFormat, (Buffer, When) =>
              {
                iOSRecognitionRequest.Append(Buffer);

#if AUDIO_FILE
                if (iOSAudioFile != null)
                {
                  iOSAudioFile.WriteFromBuffer(Buffer, out var BufferError);

                  Debug.Assert(BufferError == null, BufferError?.ToString());
                }
#endif
              });

              iOSAudioEngine.Prepare();
              iOSAudioEngine.StartAndReturnError(out var iOSAudioError);

              if (iOSAudioError == null)
              {
                this.iOSRecognitionTimer = new System.Timers.Timer();
                iOSRecognitionTimer.Interval = 1500; // 1.5 seconds.
                iOSRecognitionTimer.AutoReset = false; // stop after the first interval elapses.
                iOSRecognitionTimer.Elapsed += (Sender, Event) =>
                {
                  Debug.WriteLine($"[{CaptureSessionCount}] SpeechRecognition: Idle elapsed");
                  iOSRecognitionTask?.Finish();
                };

                Capture();
              }
              else
              {
                var ErrorMessage = iOSAudioError.ToString();
                iOSAudioError.Dispose();
                throw new Exception(ErrorMessage);
              }
              break;

            case Speech.SFSpeechRecognizerAuthorizationStatus.Denied:
            case Speech.SFSpeechRecognizerAuthorizationStatus.Restricted:
            case Speech.SFSpeechRecognizerAuthorizationStatus.NotDetermined:
              break;
          }
        });
      }
    }
    public void Stop()
    {
      if (IsActive)
      {
        this.IsActive = false;

        Debug.WriteLine($"[{CaptureSessionCount}] SpeechRecognition: STOP");

        if (iOSRecognitionTask != null)
        {
          iOSRecognitionTask.Finish();
          this.iOSRecognitionTask = null;
        }

        if (iOSAudioNode != null)
        {
          iOSAudioNode.RemoveTapOnBus(0);
          this.iOSAudioNode = null;
        }

        if (iOSRecognitionRequest != null)
        {
          iOSRecognitionRequest.EndAudio();
          this.iOSRecognitionRequest = null;
        }

        if (iOSAudioEngine != null)
        {
          iOSAudioEngine.Dispose();
          this.iOSAudioEngine = null;
        }

        if (iOSSpeechRecognizer != null)
        {
          iOSSpeechRecognizer.Dispose();
          this.iOSSpeechRecognizer = null;
        }

        if (iOSRecognitionTimer != null)
        {
          iOSRecognitionTimer.Stop();
          iOSRecognitionTimer.Dispose();
          this.iOSRecognitionTimer = null;
        }

        ToggleAudioSession(Active: false);

#if AUDIO_FILE
        if (iOSAudioFile != null)
        {
          iOSAudioFile.Dispose();
          this.iOSAudioFile = null;

          if (!System.IO.File.Exists(iOSAudioUrl.ToString()))
            Debug.WriteLine("File not found.");
          else
            Debug.WriteLine("FileLength: " + new System.IO.FileInfo(iOSAudioUrl.ToString()).Length);

          var iOSAudioPlayer = AVFoundation.AVAudioPlayer.FromUrl(iOSAudioUrl, out var PlayError);
          Debug.Assert(PlayError == null, PlayError?.ToString());
          iOSAudioPlayer?.Play();
        }
#endif
      }
    }
    public void Recapture()
    {
      if (iOSRecognitionTask != null)
      {
        Debug.WriteLine($"[{CaptureSessionCount}] SpeechRecognition: Recapture Continued");

        iOSRecognitionTimer.Stop();

        iOSRecognitionTask.Finish();
        this.iOSRecognitionTask = null;

        MakeRequest();

        Capture();
      }
      else
      {
        Debug.WriteLine($"[{CaptureSessionCount}] SpeechRecognition: Recapture Skipped");
      }
    }

    private void MakeRequest()
    {
      this.iOSRecognitionRequest = new Speech.SFSpeechAudioBufferRecognitionRequest();
      iOSRecognitionRequest.ShouldReportPartialResults = true;

#if AUDIO_FILE
      var iOSAudioSettings = iOSAudioEngine.InputNode.GetBusInputFormat(0).Settings;
      /*
      var iOSAudioSettings = new AVFoundation.AudioSettings()
      {
        Format = AudioToolbox.AudioFormatType.MPEG4AAC,
        SampleRate = 44100,
        NumberChannels = 2,
        //EncoderAudioQualityForVBR = AVFoundation.AVAudioQuality.High,
        //LinearPcmBitDepth = 32,
        //LinearPcmFloat = true,
        //LinearPcmBigEndian = false
      };
      */
      this.iOSAudioUrl = new Foundation.NSUrl(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "test.caf")); // .m4a?

      if (iOSAudioFile != null)
      {
        iOSAudioFile.Dispose();
        this.iOSAudioFile = null;
      }

      if (System.IO.File.Exists(iOSAudioUrl.ToString()))
        System.IO.File.Delete(iOSAudioUrl.ToString());

      this.iOSAudioFile = new AVFoundation.AVAudioFile(iOSAudioUrl, iOSAudioSettings, out var CreateError);

      Debug.Assert(CreateError == null, CreateError?.ToString());
#endif
    }
    private void Capture()
    {
      var CurrentSessionID = ++CaptureSessionCount;

      // NOTE: do not start the recognition timer, until after the first utterance.
      this.iOSRecognitionTask = iOSSpeechRecognizer.GetRecognitionTask(iOSRecognitionRequest, (Result, Error) =>
      {
        if (Error != null)
        {
          Debug.WriteLine($"[{CurrentSessionID}] SpeechRecognition: Error " + Error.ToString());

          // Error=Retry means nothing was captured before it was Finish'd.
          if (Error.Code == 203)
            Recapture();
        }
        else if (Result != null)
        {
          var Text = Result.BestTranscription.FormattedString;

          Debug.WriteLine($"[{CurrentSessionID}] SpeechRecognition: " + Text + (Result.Final ? " [FINAL]" : ""));

          if (iOSRecognitionTimer != null)
          {
            iOSRecognitionTimer.Stop();
            iOSRecognitionTimer.Start();
          }

          TranscriptionEvent?.Invoke(Result.BestTranscription, Result.Final);

          if (Result.Final)
            Recapture();
        }
        else
        {
          Debug.WriteLine($"[{CurrentSessionID}] SpeechRecognition: Unknown");
        }
      });
    }
    private void ToggleAudioSession(bool Active)
    {
      Debug.WriteLine($"[{CaptureSessionCount}] SpeechRecognition: ToggleAudio " + Active);

      var iOSAudioSession = AVFoundation.AVAudioSession.SharedInstance();
      var iOSAudioCategoryError = iOSAudioSession.SetCategory(Active ? AVFoundation.AVAudioSessionCategory.Record : AVFoundation.AVAudioSessionCategory.Ambient);
      iOSAudioSession.SetMode(AVFoundation.AVAudioSession.ModeMeasurement, out var iOSAudioModeError);
      var iOSAudioSessionError = iOSAudioSession.SetActive(Active, AVFoundation.AVAudioSessionSetActiveOptions.NotifyOthersOnDeactivation);
    }

    private AVFoundation.AVAudioEngine iOSAudioEngine;
    private Speech.SFSpeechRecognizer iOSSpeechRecognizer;
    private Speech.SFSpeechAudioBufferRecognitionRequest iOSRecognitionRequest;
    private Speech.SFSpeechRecognitionTask iOSRecognitionTask;
    private AVFoundation.AVAudioInputNode iOSAudioNode;
#if AUDIO_FILE
    private AVFoundation.AVAudioFile iOSAudioFile;
    private Foundation.NSUrl iOSAudioUrl;
#endif
    private System.Timers.Timer iOSRecognitionTimer;
    private bool IsActive;
    private int CaptureSessionCount;
  }
}