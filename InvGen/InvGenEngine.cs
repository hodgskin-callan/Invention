﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  public static class GenShell
  {
    public static void Execute(string Input)
    {
      var SW = System.Diagnostics.Stopwatch.StartNew();

      var Argument = Input;

      if (Path.GetExtension(Argument).ToLower() == ".csproj")
      {
        var Project = new Inv.CSharpProject();
        Project.LoadFromFile(Argument);

        foreach (var Reference in Project.IncludeReference)
        {
          if (Path.GetExtension(Reference.File.Path).ToLower() == ".invresourcepackage")
          {
            Argument = Path.Combine(Path.GetDirectoryName(Argument), Reference.File.Path);
            break;
          }
        }

        if (Argument == Input)
        {
          //WriteLine("Install the Invention VSIX if you want to add a resource package to project " + Argument);
          return;
        }
      }
      else if (Path.GetExtension(Argument).ToLower() != ".invresourcepackage")
      {
        WriteLine("Argument must be a .InvResourcePackage or a .csproj that contains a .InvResourcePackage: " + Argument);
        return;
      }

      if (!System.IO.File.Exists(Argument))
      {
        WriteLine("Argument file path does not exist: " + Argument);
        Environment.ExitCode = 1;
        return;
      }

      var ArgumentFilePath = GetExactPathName(Argument); // NOTE: VisualStudio right click 'Shell (Open)' makes the argument lower case for some reason.

      WriteLine(ArgumentFilePath + " started generation ...");

      var SourceFolderPath = Path.GetDirectoryName(ArgumentFilePath);
      var RootFolderName = Path.GetFileName(SourceFolderPath);
      var DataNamespace = Path.GetFileNameWithoutExtension(ArgumentFilePath);
      var CodeNamespace = DataNamespace;
      var PublicVisibility = true;
      var GraftList = new Inv.DistinctList<string>();

      var SyntaxGrammar = new Inv.Syntax.Grammar()
      {
        IgnoreComment = true,
        IgnoreWhitespace = true,
        KeywordCaseSensitive = true,
        AllowMultiLineString = false,
        SingleLineCommentPrefix = "//",
        MultiLineCommentStart = "/*",
        MultiLineCommentFinish = "*/",
        StringAroundQuote = '"',
        StringEscape = '\\',
        StringVerbatim = '@',
        IdentifierOpenQuote = '[',
        IdentifierCloseQuote = ']',
        DateTimePrefix = "dt ",
        TimeSpanPrefix = "ts ",
        UseHexadecimalPrefix = true,
        UseBase64Prefix = true,
        AlwaysQuoteIdentifier = true
      };
      const string GraftKeyword = "graft";
      const string InternalKeyword = "internal";
      SyntaxGrammar.Keyword.AddArray(GraftKeyword, InternalKeyword);

      Inv.Syntax.Extension.ReadTextFile(SyntaxGrammar, ArgumentFilePath, Reader =>
      {
        PublicVisibility = !Reader.ReadOptionalKeywordValue(InternalKeyword);

        if (Reader.NextIsIdentifier())
          CodeNamespace = Reader.ReadIdentifierChainString(Inv.Syntax.TokenType.Period);

        while (Reader.ReadOptionalKeywordValue(GraftKeyword))
          GraftList.Add(Path.Combine(SourceFolderPath, Reader.ReadString()));
      });

      var RsFilePath = Argument + ".rs";
      var CsFilePath = Argument + ".cs";

      var ResourcePackage = new Inv.Resource.Header()
      {
        DirectoryList = new Inv.DistinctList<Inv.Resource.DirectoryHeader>()
      };

      var InsightDictionary = new Dictionary<Inv.Resource.FileHeader, Insight>();

      foreach (var FolderPath in new[] { SourceFolderPath }.Union(GraftList))
      {
        foreach (var DirectoryInfo in new DirectoryInfo(FolderPath).GetDirectories())
        {
          var ResourceDirectoryName = DirectoryInfo.Name.ConvertToCSharpIdentifier();

          var ResourceDirectory = ResourcePackage.DirectoryList.Find(D => string.Equals(D.Name, ResourceDirectoryName, StringComparison.InvariantCultureIgnoreCase));

          var ResourceDirectoryIsNew = ResourceDirectory == null;
          if (ResourceDirectoryIsNew)
          {
            ResourceDirectory = new Inv.Resource.DirectoryHeader();
            ResourceDirectory.Name = ResourceDirectoryName;
            ResourceDirectory.FileList = new Inv.DistinctList<Inv.Resource.FileHeader>();
          }

          //WriteLine(ResourceDirectory.Name + " importing ...");

          foreach (var FileInfo in DirectoryInfo.GetFiles())
          {
            var ResourceFileName = Path.GetFileNameWithoutExtension(FileInfo.Name).Replace('.', ' ').Replace('-', ' ').PascalCaseToTitleCase().ToTitleCase().ConvertToCSharpIdentifier();
            var ResourceFileContent = System.IO.File.ReadAllBytes(FileInfo.FullName);
            var ResourceFileFormat = Inv.Resource.FileFormat.Binary;

            //WriteLine(ResourceDirectory.Name + "\\" + Name);

            switch (FileInfo.Extension.ToLower())
            {
              case ".mp3":
              case ".ogg":
              case ".wav":
                ResourceFileFormat = Inv.Resource.FileFormat.Sound;
                break;

              case ".png":
              case ".jpg":
              case ".jpeg":
              case ".bmp":
              case ".tif":
              case ".tiff":
              case ".gif":
                ResourceFileFormat = Inv.Resource.FileFormat.Image;
                break;

              case ".sql":
              case ".txt":
              case ".csv":
                ResourceFileFormat = Inv.Resource.FileFormat.Text;
                break;

              default:
                System.Diagnostics.Debug.WriteLine("File not handled " + FileInfo.Extension);
                break;
            }

            if (ResourceDirectory.FileList.Any(F => string.Equals(F.Name, ResourceFileName)))
              throw new Exception($"Duplicate file '{ResourceFileName}' found in folder '{ResourceDirectory.Name}'.");

            var ResourceFile = new Inv.Resource.FileHeader()
            {
              Name = ResourceFileName,
              Format = ResourceFileFormat,
              Extension = FileInfo.Extension.ToLower(),
              Length = (int)ResourceFileContent.Length
            };
            ResourceDirectory.FileList.Add(ResourceFile);

            InsightDictionary.Add(ResourceFile, new Insight(ResourceFileFormat.GetInsightText(FileInfo.FullName), ResourceFileContent));
          }

          if (ResourceDirectoryIsNew && ResourceDirectory.FileList.Count > 0)
            ResourcePackage.DirectoryList.Add(ResourceDirectory);
        }
      }
      var FormatTypeArray = new Inv.EnumArray<Inv.Resource.FileFormat, string>
      {
        { Inv.Resource.FileFormat.Text, "global::Inv.Resource.TextReference" },
        { Inv.Resource.FileFormat.Sound, "global::Inv.Resource.SoundReference" },
        { Inv.Resource.FileFormat.Image, "global::Inv.Resource.ImageReference" },
        { Inv.Resource.FileFormat.Binary, "global::Inv.Resource.BinaryReference" }
      };

      var CsFileInfo = new FileInfo(CsFilePath);

      var OldText = CsFileInfo.Exists ? System.IO.File.ReadAllText(CsFileInfo.FullName) : null;

      string NewText;

      using (var StringWriter = new StringWriter())
      {
        var Visibility = PublicVisibility ? "public" : "internal";

        StringWriter.WriteLine("#pragma warning disable 0649");       
        StringWriter.WriteLine("namespace " + CodeNamespace);
        StringWriter.WriteLine("{");
        StringWriter.WriteLine("  {0} static class {1}", Visibility, RootFolderName);
        StringWriter.WriteLine("  {");
        StringWriter.WriteLine("    static {0}()", RootFolderName);
        StringWriter.WriteLine("    {");
        StringWriter.WriteLine("      global::Inv.Resource.Foundation.Import(typeof({0}), \"{0}.{1}.InvResourcePackage.rs\");", RootFolderName, DataNamespace);
        StringWriter.WriteLine("    }");
        StringWriter.WriteLine("");
        foreach (var Directory in ResourcePackage.DirectoryList)
          StringWriter.WriteLine("    {0} static readonly {1}{2} {2};", "public", RootFolderName, Directory.Name);
        StringWriter.WriteLine("  }");

        foreach (var ResourceDirectory in ResourcePackage.DirectoryList)
        {
          StringWriter.WriteLine("");
          StringWriter.WriteLine("  {0} sealed class {1}{2}", Visibility, RootFolderName, ResourceDirectory.Name);
          StringWriter.WriteLine("  {");
          StringWriter.WriteLine("    {0} {1}{2}() {{ }}", "public", RootFolderName, ResourceDirectory.Name);
          StringWriter.WriteLine("");
          foreach (var ResourceFile in ResourceDirectory.FileList)
          {
            StringWriter.WriteLine("    ///<Summary>({0}) {1} ({2:F1}KB)</Summary>", ResourceFile.Extension, InsightDictionary[ResourceFile].Text, ResourceFile.Length / 1024.0);
            StringWriter.WriteLine("    {0} readonly {1} {2};", "public", FormatTypeArray[ResourceFile.Format], ResourceFile.Name);
          }
          StringWriter.WriteLine("  }");
        }
        StringWriter.Write("}");

        NewText = StringWriter.ToString();
      }

      WriteLine("Next .cs length = " + NewText.Length.ToString("N0"));

      if (OldText == null || OldText != NewText)
      {
        if (CsFileInfo.Exists && CsFileInfo.IsReadOnly)
          CsFileInfo.IsReadOnly = false;

        System.IO.File.WriteAllText(CsFileInfo.FullName, NewText);
      }

      byte[] NewData;
      using (var MemoryStream = new MemoryStream())
      {
        var ResourceGovernor = new Inv.Resource.Governor();
        ResourceGovernor.Save(ResourcePackage, MemoryStream);

        // allocate the remainder stream in one go.
        var FullCapacity = (int)(MemoryStream.Position + InsightDictionary.Values.Sum(C => C.Content.Length));
        MemoryStream.Capacity = FullCapacity;

        // TODO: header marker?

        foreach (var ResourceDirectory in ResourcePackage.DirectoryList)
        {
          foreach (var ResourceFile in ResourceDirectory.FileList)
          {
            // TODO: content marker?

            var Content = InsightDictionary[ResourceFile].Content;

            MemoryStream.Write(Content, 0, Content.Length);
          }
        }

        MemoryStream.Flush();

        System.Diagnostics.Debug.Assert(MemoryStream.Capacity == FullCapacity, "Estimated full capacity was not correct.");

        NewData = MemoryStream.ToArray();
      }

      WriteLine("Next .rs length = " + Inv.DataSize.FromBytes(NewData.Length).ToString());

      var RsFileInfo = new FileInfo(RsFilePath);
      var OldData = RsFileInfo.Exists ? System.IO.File.ReadAllBytes(RsFilePath) : null;

      if (OldData == null || !OldData.ShallowEqualTo(NewData))
      {
        if (RsFileInfo.Exists && RsFileInfo.IsReadOnly)
          RsFileInfo.IsReadOnly = false;

        var Retry = 0;
        Exception LastException;
        do
        {
          try
          {
            System.IO.File.WriteAllBytes(RsFilePath, NewData);
            LastException = null;
          }
          catch (Exception Exception)
          {
            Retry++;
            LastException = Exception.Preserve();
          }
        }
        while (LastException != null && Retry < 5);

        if (LastException != null)
          throw LastException;
      }

      SW.Stop();

      WriteLine(ArgumentFilePath + " generated in " + SW.ElapsedMilliseconds.ToString("N0") + " ms.");
    }

    private static void WriteLine(string Text)
    {
      Console.WriteLine(Text);
      System.Diagnostics.Debug.WriteLine(Text);
    }

    private static string GetExactPathName(string pathName)
    {
      if (!(System.IO.File.Exists(pathName) || System.IO.Directory.Exists(pathName)))
        return pathName;

      var di = new DirectoryInfo(pathName);

      if (di.Parent != null)
        return Path.Combine(GetExactPathName(di.Parent.FullName), di.Parent.GetFileSystemInfos(di.Name)[0].Name);
      else
        return di.Name.ToUpper();
    }

    private sealed class Insight
    {
      internal Insight(string Text, byte[] Content)
      {
        this.Text = Text;
        this.Content = Content;
      }

      public string Text { get; }
      public byte[] Content { get; }
    }
  }
}
