﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;
using System.IO;

namespace Inv
{
  public static class GenProgram
  {
    static void Main(string[] args)
    {
      if (args.Length != 1)
      {
        Console.WriteLine("Exactly one argument must be provided.");
        return;
      }

      GenShell.Execute(args[0]);
    }
  }
}
