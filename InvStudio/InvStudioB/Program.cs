using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace InvStudioB
{
  public class Program
  {
    public static async Task Main(string[] args)
    {
      var Builder = WebAssemblyHostBuilder.CreateDefault(args);
      Builder.RootComponents.Add<App>("#app");

      Builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(Builder.HostEnvironment.BaseAddress) });
      Builder.Services.AddSingleton(services => (IJSInProcessRuntime)services.GetRequiredService<IJSRuntime>());

      await Builder.Build().RunAsync();
    }
  }
}
