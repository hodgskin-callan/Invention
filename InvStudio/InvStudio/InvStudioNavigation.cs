﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Material;
using Inv.Support;

namespace Inv.Studio
{
  public sealed class NavigationConsole
  {
    public NavigationConsole(Inv.Application Base, NavigationTheme Theme)
    {
      this.Base = Base;
      this.Theme = Theme;

      Base.Window.Background.In(Inv.Colour.Black);
      Base.ExitQuery += () =>
      {
        ExitCheck();

        return false;
      };

      this.CloseSurface = Base.Window.NewSurface();
    }

    public NavigationTheme Theme { get; private set; }
    public string Title
    {
      get => Base.Title;
      set => Base.Title = value;
    }
    public Inv.Clipboard Clipboard => Base.Clipboard;
    public Inv.Directory Directory => Base.Directory;
    public Inv.Audio Audio => Base.Audio;
    public Inv.Graphics Graphics => Base.Graphics;
    public Inv.Haptics Haptics => Base.Haptics;
    public Inv.Location Location => Base.Location;
    public Inv.Process Process => Base.Process;
    public Inv.Window Window => Base.Window;
    public Inv.Keyboard Keyboard => Base.Keyboard;
    public Inv.Journal Journal => Base.Journal;
    public event Action StartEvent
    {
      add => Base.StartEvent += value;
      remove => Base.StartEvent -= value;
    }
    public event Action StopEvent
    {
      add => Base.StopEvent += value;
      remove => Base.StopEvent -= value;
    }
    public event Action SuspendEvent
    {
      add => Base.SuspendEvent += value;
      remove => Base.SuspendEvent -= value;
    }
    public event Action ResumeEvent
    {
      add => Base.ResumeEvent += value;
      remove => Base.ResumeEvent -= value;
    }
    public event Action<Exception> HandleExceptionEvent
    {
      add => Base.HandleExceptionEvent += value;
      remove => Base.HandleExceptionEvent -= value;
    }

    public NavigationSurface NewNavigationSurface()
    {
      return new NavigationSurface(this);
    }

    internal Inv.Application Base { get; private set; }

    public static implicit operator Inv.Application(NavigationConsole Application)
    {
      return Application?.Base;
    }

    internal void ExitCheck()
    {
      var LastSurface = Base.Window.ActiveSurface;

      if (LastSurface != CloseSurface)
      {
        if (CloseSurface.Content == null)
        {
          var CloseStack = CloseSurface.NewVerticalStack();
          CloseSurface.Content = CloseStack;
          CloseStack.Alignment.Center();

          var CloseButton = CloseSurface.NewFlatButton();
          CloseStack.AddPanel(CloseButton);
          CloseButton.Background.Colour = Inv.Colour.Red;
          CloseButton.Padding.Set(20);
          CloseButton.Margin.Set(20);
          CloseButton.SingleTapEvent += () => Base.Exit();

          var CloseLabel = CloseSurface.NewLabel();
          CloseButton.Content = CloseLabel;
          CloseLabel.Font.Size = 40;
          CloseLabel.Font.Colour = Inv.Colour.White;
          CloseLabel.Justify.Center();
          CloseLabel.Text = "CLOSE";

          var CancelButton = CloseSurface.NewFlatButton();
          CloseStack.AddPanel(CancelButton);
          CancelButton.Background.Colour = Inv.Colour.WhiteSmoke;
          CancelButton.Padding.Set(20);
          CancelButton.Margin.Set(20);
          CancelButton.SingleTapEvent += () => Base.Window.Transition(LastSurface).Fade();

          var CancelLabel = CloseSurface.NewLabel();
          CancelButton.Content = CancelLabel;
          CancelLabel.Font.Size = 40;
          CancelLabel.Font.Colour = Inv.Colour.Black;
          CancelLabel.Justify.Center();
          CancelLabel.Text = "CANCEL";
        }

        var Transition = Base.Window.Transition(CloseSurface);
        //Transition.Duration = TimeSpan.FromSeconds(2);
        Transition.Fade();
      }
    }

    private readonly Inv.Surface CloseSurface;
  }

  public sealed class NavigationTheme
  {
    public NavigationTheme(Inv.Colour Colour)
    {
      this.Colour = Colour;
    }

    public Inv.Colour Colour { get; private set; }
  }

  public sealed class NavigationSurface : Inv.Mimic<Inv.Surface>
  {
    internal NavigationSurface(NavigationConsole Application)
    {
      this.Application = Application;
      this.Base = Application.Window.NewSurface();
      
      this.Scaffold = new Inv.Material.Scaffold();
      Base.Content = Scaffold;
      Scaffold.Drawer.Title = "Invention";
      Scaffold.AppBar.SetLeading(Inv.Material.Resources.Images.Menu, () =>
      {
        Scaffold.ShowDrawer();
      });

      var ExitIcon = Scaffold.AppBar.AddTrailingIconButton();
      ExitIcon.Tooltip.Content = new Inv.Material.TooltipText() { Text = "Exit App" };
      ExitIcon.Image = Resources.Images.ClearWhite;
      ExitIcon.SingleTapEvent += () =>
      {
        Application.ExitCheck();
      };

      Base.ArrangeEvent += () =>
      {
        /*
        var ArrangeWide = IsWide();

        DrawerDock.Size.SetWidth(Math.Min(300, Application.Window.Width - 64));

        MenuIcon.Visibility.Set(!ArrangeWide);
        MenuIcon.Background.In(Toolbar.BackgroundColour);

        if (ArrangeWide)
        {
          MenuScrim.Dismiss();
          MenuScrim.Transition(null);

          if (!MainDock.HasHeaders())
          {
            MainDock.AddHeader(DrawerDock);
            MainDock.AddHeader(DrawerDivider);
          }
        }
        else
        {
          MainDock.RemoveHeaders();
        }

        Toolbar.Padding.Set(ArrangeWide ? 16 : 0, 0, 0, 0);*/
      };
    }

    public string Title
    {
      get => Scaffold.Drawer.Title;
      set => Scaffold.Drawer.Title = value;
    }
    public Inv.Material.Drawer Drawer => Scaffold.Drawer;
    public event Action ArrangeEvent
    {
      add => Base.ArrangeEvent += value;
      remove => Base.ArrangeEvent -= value;
    }
    public event Action ComposeEvent
    {
      add { Base.ComposeEvent += value; }
      remove { Base.ComposeEvent -= value; }
    }
    public event Action EnterEvent
    {
      add => Base.EnterEvent += value;
      remove => Base.EnterEvent -= value;
    }
    public event Action LeaveEvent
    {
      add => Base.LeaveEvent += value;
      remove => Base.LeaveEvent -= value;
    }
    public event Action GestureBackwardEvent
    {
      add { Base.GestureBackwardEvent += value; }
      remove { Base.GestureBackwardEvent -= value; }
    }
    public event Action GestureForwardEvent
    {
      add { Base.GestureForwardEvent += value; }
      remove { Base.GestureForwardEvent -= value; }
    }

    public void Rearrange()
    {
      Base.Rearrange();
    }
    public void GestureBackward()
    {
      Base.GestureBackward();
    }
    public void GestureForward()
    {
      Base.GestureForward();
    }
    public Inv.Transition TransitionClient(string AppBarTitle, Inv.Panel Panel)
    {
      Scaffold.AppBar.ContentAsTitle(AppBarTitle);

      Scaffold.HideDrawer();

      return Scaffold.TransitionContent(Panel);
    }

    private readonly Inv.Material.Scaffold Scaffold;
    private readonly NavigationConsole Application;
  }

  public sealed class NavigationToolbar : Inv.Panel<Inv.Dock>
  {
    public NavigationToolbar(NavigationConsole Application, Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = Surface.NewHorizontalDock();
      Base.Background.In(Application.Theme.Colour);
      Base.Size.SetHeight(56);
      Base.Elevation.Set(2);

      this.LeadingStack = Surface.NewHorizontalStack();
      Base.AddHeader(LeadingStack);

      this.TitleLabel = Surface.NewLabel();
      Base.AddClient(TitleLabel);
      TitleLabel.Alignment.CenterLeft();
      TitleLabel.Font.Custom(20).Medium().In(Inv.Colour.White);

      this.TrailingStack = Surface.NewHorizontalStack();
      Base.AddFooter(TrailingStack);

      this.IconButtonList = new Inv.DistinctList<NavigationButton>();
    }

    public string Title
    {
      get => TitleLabel.Text;
      set => TitleLabel.Text = value;
    }
    public Inv.Colour BackgroundColour
    {
      get => Base.Background.Colour;
      set
      {
        Base.Background.In(value);
        foreach (var IconButton in IconButtonList)
          IconButton.Background.In(value);
      }
    }
    public Inv.Colour ForegroundColour
    {
      get => TitleLabel.Font.Colour;
      set => TitleLabel.Font.Colour = value;
    }

    public NavigationButton AddLeadingIcon(Inv.Image Image = null)
    {
      var Result = new NavigationButton(Surface);
      Result.Image = Image;
      Result.Background.In(BackgroundColour);
      Result.Margin.Set(0, 0, 10, 0);

      LeadingStack.AddPanel(Result);

      return Result;
    }
    public NavigationButton AddTrailingIcon(Inv.Image Image = null)
    {
      var Result = new NavigationButton(Surface);
      Result.Image = Image;
      Result.Background.In(BackgroundColour);
      Result.Margin.Set(10, 0, 0, 0);

      TrailingStack.AddPanel(Result);

      return Result;
    }

    internal Inv.Alignment Alignment => Base.Alignment;
    internal Inv.Padding Padding => Base.Padding;

    private readonly Inv.Surface Surface;
    private readonly Inv.Label TitleLabel;
    private readonly Inv.Stack LeadingStack;
    private readonly Inv.Stack TrailingStack;
    private readonly Inv.DistinctList<NavigationButton> IconButtonList;
  }

  public sealed class NavigationButton : Inv.Panel<Inv.Button>
  {
    public NavigationButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewFlatButton();
      Base.Size.Set(56, 56);

      this.Graphic = Surface.NewGraphic();
      Base.Content = Graphic;
      Graphic.Size.Set(32);
      Graphic.Alignment.Center();
    }

    public Inv.Image Image
    {
      get => Graphic.Image;
      set => Graphic.Image = value;
    }
    public event Action SingleTapEvent
    {
      add => Base.SingleTapEvent += value;
      remove => Base.SingleTapEvent -= value;
    }
    public string Hint
    {
      get => Base.Hint;
      set => Base.Hint = value;
    }

    internal Inv.Alignment Alignment => Base.Alignment;
    internal Inv.Margin Margin => Base.Margin;
    internal Inv.Background Background => Base.Background;
    internal Inv.Visibility Visibility => Base.Visibility;

    private readonly Inv.Graphic Graphic;
  }

  public sealed class NavigationScrim : Inv.Panel<Inv.Overlay>
  {
    public NavigationScrim(Inv.Surface Surface)
    {
      this.Base = Surface.NewOverlay();

      this.Shade = Surface.NewStarkButton();
      Base.AddPanel(Shade);
      Shade.Visibility.Collapse();
      Shade.Background.In(Inv.Colour.Black.Opacity(1.0F / 3.0F));
      Shade.SingleTapEvent += () => Dismiss();

      this.Frame = Surface.NewFrame();
      Base.AddPanel(Frame);

      this.FadeInAnimation = Surface.NewAnimation();
      FadeInAnimation.AddTarget(Shade).FadeOpacityIn(Surface.Window.DefaultTransitionDuration);
      FadeInAnimation.CommenceEvent += () =>
      {
        if (ShowEvent != null)
          ShowEvent();

        Shade.Visibility.Show();
      };
      FadeInAnimation.CompleteEvent += () =>
      {
        if (ShowedEvent != null)
          ShowedEvent();
      };

      this.FadeOutAnimation = Surface.NewAnimation();
      FadeOutAnimation.AddTarget(Shade).FadeOpacityOut(Surface.Window.DefaultTransitionDuration);
      FadeOutAnimation.CommenceEvent += () =>
      {
        if (DismissEvent != null)
          DismissEvent();
      };
      FadeOutAnimation.CompleteEvent += () =>
      {
        Shade.Visibility.Collapse();

        if (DismissedEvent != null)
          DismissedEvent();
      };
    }

    public event Action ShowEvent;
    public event Action ShowedEvent;
    public event Action DismissEvent;
    public event Action DismissedEvent;

    public void Show()
    {
      FadeOutAnimation.Stop();

      if (!Shade.Visibility.Get() && !FadeInAnimation.IsActive)
        FadeInAnimation.Start();
    }
    public void Dismiss()
    {
      FadeInAnimation.Stop();

      if (Shade.Visibility.Get() && !FadeOutAnimation.IsActive)
        FadeOutAnimation.Start();
    }

    public Inv.Transition Transition(Inv.Panel Panel)
    {
      return Frame.Transition(Panel);
    }

    private readonly Inv.Button Shade;
    private readonly Inv.Frame Frame;
    private readonly Inv.Animation FadeInAnimation;
    private readonly Inv.Animation FadeOutAnimation;
  }
}