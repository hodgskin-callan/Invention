﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvStudioClient
{
  class Program
  {
    [STAThread]
    static void Main(string[] args)
    {
      var ScreenWidth = Inv.WpfShell.PrimaryScreenWidth;
      var ScreenHeight = Inv.WpfShell.PrimaryScreenHeight;

      var ClientWidth = Math.Min(1024, ScreenWidth / 2);
      var ClientHeight = Math.Min(1600, ScreenHeight - 40);

      Inv.ServerShell.SocketHost = "10.250.22.43";

      Inv.WpfShell.Options.FullScreenMode = false;
      Inv.WpfShell.Options.DefaultWindowX = 0;
      Inv.WpfShell.Options.DefaultWindowY = 0;
      Inv.WpfShell.Options.DefaultWindowWidth = ClientWidth;
      Inv.WpfShell.Options.DefaultWindowHeight = ClientHeight;
      Inv.WpfShell.Options.SingletonIdentity = "TEST-01";

      Inv.WpfShell.Run(A => new Inv.ServerApplication(A, Guid.NewGuid()));
    }
  }
}
