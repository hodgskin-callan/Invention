# Inv Build Instructions

The following steps will build the Inv platform for Windows Desktop (WPF), Android, iOS and Universal Windows (UWP).

Please note that you need a Mac box to build for iOS due to Apple's licensing requirements.

---

1.	Fork or download the .zip for the repository.

2.	Open `Inv.sln`.

3.	Build `InvGen` project (this will nuget all the required packages).

4.	Build `InvTest` (this will use` InvGen.exe` in a build event).

5.	Run `InvTestWpf` (you should see the test case browser).

6.	Build `InvPlatformA`, but you may get these failures:

	```
	Assembly generation failed -- Referenced assembly 'Xamarin.Android.Support.v4' does not have a strong name
	Assembly generation failed -- Referenced assembly 'Xamarin.Android.Support.Design' does not have a strong name
	```

7.	Install a strong name signing tool (eg. https://brutaldev.com/download/StrongNameSigner_Setup.exe)

8.	Sign these assemblies:
	```
	packages\Xamarin.Android.Support.Design.23.4.0.1\lib\MonoAndroid43\Xamarin.Android.Support.Design.dll
	packages\Xamarin.Android.Support.v4.23.4.0.1\lib\MonoAndroid403\Xamarin.Android.Support.v4.dll
	```

9.	Set your startup project to `InvTestAndroid` and run on your device (you should see the same test case browser).

10. Set your startup project to `InvTestiOS` and connect Xamarin to your Mac box.

11. Run on a simulator such as iPhone 6 10.1 (you should see the same test case browser).

12. Set your startup project to `InvTestUwa` (you may need to change the build platform back to Any CPU) and run (you should see the same test case browser).

---

Callan Hodgskin, 2016-12-09