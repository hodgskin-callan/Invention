﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Reflection;
using Inv.Support;

namespace Inv
{
  public sealed class WeakReferenceCollection<TItem> : IEnumerable<TItem>
  {
    public WeakReferenceCollection()
    {
      this.CriticalSection = new Inv.ExclusiveCriticalSection("WeakReferenceCollection");
      this.ReferenceList = new Inv.DistinctList<WeakReference>();
    }

    public void Clear()
    {
      using (CriticalSection.Lock())
      {
        ReferenceList.Clear();
      }
    }
    public void Add(TItem Item)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Item, nameof(Item));

      var WeakReference = new WeakReference(Item);

      using (CriticalSection.Lock())
      {
        Compact();

        ReferenceList.Add(WeakReference);
      }
    }
    public void Remove(TItem Item)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Item, nameof(Item));

      using (CriticalSection.Lock())
      {
        ReferenceList.RemoveAll(R => Item.Equals(R.Target) || !R.IsAlive);
      }
    }

    private void Compact()
    {
      ReferenceList.RemoveAll(R => !R.IsAlive);
    }
    private Inv.DistinctList<TItem> Retrieve()
    {
      using (CriticalSection.Lock())
      {
        var Result = new Inv.DistinctList<TItem>(ReferenceList.Count);

        var Index = 0;
        while (Index < ReferenceList.Count)
        {
          var Item = ReferenceList[Index].Target;

          if (Item == null)
          {
            ReferenceList.RemoveAt(Index);
          }
          else
          {
            Result.Add((TItem)Item);
            Index++;
          }
        }

        return Result;
      }
    }

    IEnumerator<TItem> IEnumerable<TItem>.GetEnumerator()
    {
      return Retrieve().GetEnumerator();
    }
    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return Retrieve().GetEnumerator();
    }

    private readonly Inv.ExclusiveCriticalSection CriticalSection;
    private readonly Inv.DistinctList<WeakReference> ReferenceList;
  }
}