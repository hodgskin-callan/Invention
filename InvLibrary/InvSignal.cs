﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Diagnostics;
using System.Threading;
using Inv.Support;

namespace Inv
{
  public sealed class AutoResetSignal : Signal
  {
    public AutoResetSignal(bool initialState, string name)
      : base (initialState, System.Threading.EventResetMode.AutoReset, name)
    {
    }
  }

  public sealed class ManualResetSignal : Signal
  {
    public ManualResetSignal(bool initialState, string name)
      : base(initialState, System.Threading.EventResetMode.ManualReset, name)
    {
    }

    public void Reset()
    {
      Handle.Reset();
    }
  }

  public abstract class Signal : IDisposable
  {
    internal Signal(bool initialState, System.Threading.EventResetMode mode, string name)
    {
      Debug.Assert(!string.IsNullOrEmpty(name), "Name is mandatory for Inv.Signals");
      // Name length is actually 260, but headway for the system prefix and the guid suffix means i've guessed a max of 200.
      Debug.Assert(name.Length < 200, "Name must be shorter than 200 characters");

      if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(System.Runtime.InteropServices.OSPlatform.Windows))
        this.Handle = new EventWaitHandle(initialState, mode, $"{name}-{Guid.NewGuid()}");
      else
        this.Handle = new EventWaitHandle(initialState, mode);
    }

    public void Dispose()
    {
      Handle.Dispose();
    }

    public void Set()
    {
      Handle.Set();
    }

    public bool WaitOne()
    {
      return Handle.WaitOne();
    }
    public bool WaitOne(int millisecondsTimeout)
    {
      return Handle.WaitOne(millisecondsTimeout);
    }
    public bool WaitOne(TimeSpan timeout)
    {
      return Handle.WaitOne(timeout);
    }
    public Task<bool> WaitOneAsync()
    {
      return WaitOneAsync(Timeout.InfiniteTimeSpan);
    }
    public Task<bool> WaitOneAsync(TimeSpan timeout)
    {
      var tcs = new TaskCompletionSource<bool>();
      var registration = ThreadPool.RegisterWaitForSingleObject(Handle, (state, timedOut) =>
      {
        var localTcs = (TaskCompletionSource<bool>)state;
        localTcs.SetResult(!timedOut);
      }, tcs, timeout, executeOnlyOnce: true);

      // clean up the RegisterWaitHandle
      tcs.Task.ContinueWith((_, state) =>
      {
        ((RegisteredWaitHandle)state).Unregister(null);
      }, registration, TaskScheduler.Default);

      return tcs.Task;
    }

    internal readonly EventWaitHandle Handle;
  }
}
