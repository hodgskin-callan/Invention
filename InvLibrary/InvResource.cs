﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Inv.Support;

namespace Inv.Resource
{
  /// <summary>
  /// Used for loading files from embedded resources.
  /// </summary>
  public static class Foundation
  {
    static Foundation()
    {
      Governor = new Governor();
    }

    /// <summary>
    /// Import the resource crate from the embedded resource.
    /// </summary>
    /// <param name="PackageType"></param>
    /// <param name="PackagePath"></param>
    public static void Import(Type PackageType, string PackagePath)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(PackageType != null, $"{nameof(PackageType)} must be specified.");

      var SW = new Stopwatch();
      SW.Measure(PackagePath, () =>
      {
        var TypeInfo = PackageType.GetReflectionInfo();
        //if (!TypeInfo.IsClass || !TypeInfo.IsAbstract || !TypeInfo.IsSealed)
        //  throw new Exception("PackageType must be a static type.");

        Manager Manager;

        System.IO.Stream PackageStream = null;
        try
        {
          PackageStream = TypeInfo.Assembly.GetManifestResourceStream(PackagePath);

          // most likely: the assembly name and root namespace are the same.
          if (PackageStream == null)
            PackageStream = TypeInfo.Assembly.GetManifestResourceStream(TypeInfo.Assembly.GetName().Name + "." + PackagePath);

          if (PackageStream == null)
          {
            // do we have any embedded resources?
            var ResourceArray = TypeInfo.Assembly.GetManifestResourceNames();
            if (ResourceArray.Length == 0)
              throw new Exception(string.Format("Resource package '{0}' was not found in the embedded resources.\nPlease make sure the file build action is set to 'Embedded Resource'.", PackagePath));

            // find a resource that ends with the expected package path.
            var LikelyResource = ResourceArray.Where(R => R.EndsWith("." + PackagePath)).ToArray();
            if (LikelyResource.Length == 1)
              PackageStream = TypeInfo.Assembly.GetManifestResourceStream(LikelyResource[0]);

            // there are resources but none of them match the expected name.
            if (PackageStream == null)
              throw new Exception(string.Format("Resource package '{0}' was not found in the embedded resources.\nPlease make sure the file build action is set to 'Embedded Resource'.\nResource names found in this assembly:\n{1}", PackagePath, ResourceArray.Take(20).AsLineSeparatedText()));
          }

          Manager = new Manager(PackageStream);

          PackageStream = null;
        }
        finally
        {
          PackageStream?.Dispose();
        }

        var PackageFieldArray = TypeInfo.GetReflectionFields();

        if (Manager.Header.DirectoryList == null)
        {
          foreach (var FieldInfo in PackageFieldArray)
          {
            var FieldValue = Activator.CreateInstance(FieldInfo.FieldType, FieldInfo.FieldType.IsNotPublic);
            FieldInfo.SetValue(null, FieldValue);
          }
        }
        else
        {
          var OffsetIndex = 0;

          foreach (var Directory in Manager.Header.DirectoryList)
          {
            var FieldInfo = PackageFieldArray.Find(P => P.Name == Directory.Name);

            if (FieldInfo != null)
            {
              var FieldValue = Activator.CreateInstance(FieldInfo.FieldType, FieldInfo.FieldType.IsNotPublic); // this is the compartment class.
              FieldInfo.SetValue(null, FieldValue);

              if (Directory.FileList.Count > 0)
              {
                var DirectoryInfo = FieldInfo.FieldType.GetReflectionInfo();
                var DirectoryFieldDictionary = DirectoryInfo.GetReflectionFields().ToDictionary(F => F.Name, V => V);

                foreach (var File in Directory.FileList)
                {
                  var FileInfo = DirectoryFieldDictionary.GetValueOrNull(File.Name);

                  if (FileInfo != null)
                  {
                    switch (File.Format)
                    {
                      case FileFormat.Text:
                        FileInfo.SetValue(FieldValue, new Inv.Resource.TextReference(Manager, OffsetIndex, File));
                        break;

                      case FileFormat.Sound:
                        FileInfo.SetValue(FieldValue, new Inv.Resource.SoundReference(Manager, OffsetIndex, File));
                        break;

                      case FileFormat.Image:
                        FileInfo.SetValue(FieldValue, new Inv.Resource.ImageReference(Manager, OffsetIndex, File));
                        break;

                      case FileFormat.Binary:
                        FileInfo.SetValue(FieldValue, new Inv.Resource.BinaryReference(Manager, OffsetIndex, File));
                        break;

                      default:
                        throw new Exception("Format not handled: " + File.Format);
                    }
                  }

                  OffsetIndex += File.Length;
                }
              }
            }
          }
        }
      });
    }

    internal static readonly Governor Governor;
  }

  internal sealed class Manager : IDisposable
  {
    public Manager(Stream Stream)
    {
      this.Stream = Stream;
      this.Header = new Header();
      
      Foundation.Governor.Load(Header, Stream);

      this.ContentIndex = Stream.Position;
    }
    public void Dispose()
    {
      Stream.Dispose();
    }

    public Header Header { get; }

    public byte[] Load(int OffsetIndex, int ContentLength)
    {
      var ResultArray = new byte[ContentLength];

      Stream.Position = ContentIndex + OffsetIndex;
      Stream.ReadByteArray(ResultArray);

      return ResultArray;
    }

    private readonly Stream Stream;
    private readonly long ContentIndex;
  }

  public sealed class ImageReference
  {
    internal ImageReference(Manager Manager, int OffsetIndex, FileHeader File)
    {
      this.Manager = Manager;
      this.OffsetIndex = OffsetIndex;
      this.File = File;
    }

    public Inv.Image Load()
    {
      if (CacheField != null)
        return CacheField;

      lock (Manager)
      {
        if (CacheField == null)
          CacheField = new Inv.Image(Manager.Load(OffsetIndex, File.Length), File.Extension);

        return CacheField;
      }
    }
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public static implicit operator Inv.Image(ImageReference Self) => Self?.Load();

    private readonly Manager Manager;
    private readonly int OffsetIndex;
    private readonly FileHeader File;
    private Inv.Image CacheField;
  }

  public sealed class SoundReference
  {
    internal SoundReference(Manager Manager, int OffsetIndex, FileHeader File)
    {
      this.Manager = Manager;
      this.OffsetIndex = OffsetIndex;
      this.File = File;
    }

    public Inv.Sound Load()
    {
      if (CacheField != null)
        return CacheField;

      lock (Manager)
      {
        if (CacheField == null)
          CacheField = new Inv.Sound(Manager.Load(OffsetIndex, File.Length), File.Extension);

        return CacheField;
      }
    }
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public static implicit operator Inv.Sound(SoundReference Self) => Self?.Load();

    private readonly Manager Manager;
    private readonly int OffsetIndex;
    private readonly FileHeader File;
    private Inv.Sound CacheField;
  }

  public sealed class BinaryReference
  {
    internal BinaryReference(Manager Manager, int OffsetIndex, FileHeader File)
    {
      this.Manager = Manager;
      this.OffsetIndex = OffsetIndex;
      this.File = File;
    }

    public Inv.Binary Load()
    {
      if (CacheField != null)
        return CacheField;

      lock (Manager)
      {
        if (CacheField == null)
          CacheField = new Inv.Binary(Manager.Load(OffsetIndex, File.Length), File.Extension);

        return CacheField;
      }
    }
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public static implicit operator Inv.Binary(BinaryReference Self) => Self?.Load();

    private readonly Manager Manager;
    private readonly int OffsetIndex;
    private readonly FileHeader File;
    private Inv.Binary CacheField;
  }

  public sealed class TextReference
  {
    internal TextReference(Manager Manager, int OffsetIndex, FileHeader File)
    {
      this.Manager = Manager;
      this.OffsetIndex = OffsetIndex;
      this.File = File;
    }

    public string Name => File.Name;

    public string Load()
    {
      if (CacheField != null)
        return CacheField;

      lock (Manager)
      {
        if (CacheField == null)
        {
          using (var MemoryStream = new MemoryStream(Manager.Load(OffsetIndex, File.Length)))
          using (var StreamReader = new StreamReader(MemoryStream))
            CacheField = StreamReader.ReadToEnd();
        }

        return CacheField;
      }
    }

    /// <summary>
    /// In case of implicit use where an object is expected (such as string.Format and $"").
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      return Load();
    }

    public static implicit operator string(TextReference Self) => Self?.Load();

    private readonly Manager Manager;
    private readonly int OffsetIndex;
    private readonly FileHeader File;
    private string CacheField;
  }

  public sealed class Governor
  {
    public Governor()
    {
      this.Base = new Inv.Persist.Governor();

      Base.Register<Header>();
      Base.Register<DirectoryHeader>();
      Base.Register<FileHeader>();

#if DEBUG
      Base.Validate<Header>();
#endif
    }

    public void Save(Header Header, Stream Stream)
    {
      Base.Save(Header, Stream);
    }
    public void Load(Header Header, Stream Stream)
    {
      Base.Load(Header, Stream);
    }

    private readonly Inv.Persist.Governor Base;
  }

  public sealed class Header
  {
    public Header() { }

    public Inv.DistinctList<DirectoryHeader> DirectoryList { get; set; }
  }

  public sealed class DirectoryHeader
  {
    public DirectoryHeader() { }

    public string Name { get; set; }
    public Inv.DistinctList<FileHeader> FileList { get; set; }
  }

  public sealed class FileHeader
  {
    public FileHeader() { }

    public string Name { get; set; }
    public FileFormat Format { get; set; }
    public string Extension { get; set; }
    public int Length { get; set; }
  }

  public enum FileFormat
  {
    Text,
    Image,
    Sound,
    Binary
  }
}
