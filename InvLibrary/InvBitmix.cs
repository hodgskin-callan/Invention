﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using Inv.Support;
using System.Diagnostics;

namespace Inv
{
  /// <summary>
  /// BitmixSet is a struct used for manipulating bit flags on a 64 bit integer.
  /// </summary>
  public struct BitmixSet
  {
    public static BitmixSet FromRaw(long Bits)
    {
      var Result = new BitmixSet();
      Result.Bits = Bits;
      return Result;
    }
    public long GetRaw()
    {
      return Bits;
    }

    public override string ToString() => Convert.ToString(Bits, 2);

    public void Clear()
    {
      this.Bits = 0L;
    }
    /// <summary>
    /// Any of the bits are set.
    /// </summary>
    /// <returns></returns>
    public bool Any()
    {
      return Bits != 0;
    }
    /// <summary>
    /// Count the number of active bits.
    /// </summary>
    /// <returns></returns>
    public int Count()
    {
      var Result = 0;

      var Current = Bits;

      while (Current != 0)
      {
        // remove the end bit.
        Current &= (Current - 1);

        Result++;
      }

      return Result;
    }
    public int Count(params BitmixFlag[] FlagArray)
    {
      if (FlagArray == null || FlagArray.Length == 0)
        return 0;

      var Result = 0;

      foreach (var Flag in FlagArray)
      {
        if ((Bits & Flag.Bits) == Flag.Bits)
          Result++;
      }

      return Result;
    }
    public int Count(IEnumerable<BitmixFlag> Flags)
    {
      var Result = 0;

      foreach (var Flag in Flags)
      {
        if ((Bits & Flag.Bits) == Flag.Bits)
          Result++;
      }

      return Result;
    }
    public bool ContainsAny(IEnumerable<BitmixFlag> Flags)
    {
      foreach (var Flag in Flags)
      {
        if ((Bits & Flag.Bits) == Flag.Bits)
          return true;
      }

      return false;
    }
    public bool ContainsAny(params BitmixFlag[] FlagArray)
    {
      if (FlagArray == null || FlagArray.Length == 0)
        return false;

      foreach (var Flag in FlagArray)
      {
        if ((Bits & Flag.Bits) == Flag.Bits)
          return true;
      }

      return false;
    }
    public bool ContainsAll(IEnumerable<BitmixFlag> Flags)
    {
      foreach (var Flag in Flags)
      {
        if ((Bits & Flag.Bits) != Flag.Bits)
          return false;
      }

      return true;
    }
    public bool ContainsAll(params BitmixFlag[] FlagArray)
    {
      if (FlagArray == null || FlagArray.Length == 0)
        return false;

      foreach (var Flag in FlagArray)
      {
        if ((Bits & Flag.Bits) != Flag.Bits)
          return false;
      }

      return true;
    }
    public bool Contains(BitmixFlag Flag)
    {
      return (Bits & Flag.Bits) == Flag.Bits;
    }
    public void AddFlag(BitmixFlag Flag)
    {
      Bits |= Flag.Bits;
    }
    public void AddSet(BitmixSet Set)
    {
      this.Bits |= Set.Bits;
    }
    public void MaskSet(BitmixSet Set)
    {
      this.Bits &= Set.Bits;
    }
    public void Assign(BitmixSet Set)
    {
      this.Bits = Set.Bits;
    }
    public void RemoveSet(BitmixSet Set)
    {
      this.Bits &= ~Set.Bits;
    }
    public void AddRange(IEnumerable<BitmixFlag> Flags)
    {
      foreach (var Flag in Flags)
        this.Bits |= Flag.Bits;
    }
    public void RemoveRange(IEnumerable<BitmixFlag> Flags)
    {
      foreach (var Flag in Flags)
        this.Bits &= ~Flag.Bits;
    }
    public void Remove(BitmixFlag Flag)
    {
      this.Bits &= ~Flag.Bits;
    }
    public BitmixSet Exclude(BitmixFlag Flag)
    {
      var Result = new BitmixSet();
      Result.Bits = Bits & ~Flag.Bits;
      return Result;
    }
    public BitmixSet Include(BitmixFlag Flag)
    {
      var Result = new BitmixSet();
      Result.Bits = Bits | Flag.Bits;
      return Result;
    }
    public BitmixSet Except(BitmixSet Set)
    {
      var Result = new BitmixSet();
      Result.Bits = Bits & ~Set.Bits;
      return Result;
    }
    public BitmixSet Union(BitmixSet Set)
    {
      var Result = new BitmixSet();
      Result.Bits = Bits | Set.Bits;
      return Result;
    }
    public BitmixSet Intersect(BitmixSet Set)
    {
      var Result = new BitmixSet();
      Result.Bits = Bits & Set.Bits;
      return Result;
    }
    public IEnumerator<int> GetEnumerator()
    {
      for (var Index = 0; Index < BitmixFlag.Size; Index++)
      {
        var FlagBits = 1L << Index;

        if ((Bits & FlagBits) == FlagBits)
          yield return Index;
      }
    }

    private long Bits;
  }

  /// <summary>
  /// Create a flag for each bit you wish to manipulate in the BitmixSet.
  /// </summary>
  public struct BitmixFlag
  {
    public BitmixFlag(int Index)
      : this()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Index < Size, "BitmixSet only supports up to {0} flags.", Size);

      this.Bits = 1L << Index;
    }

    // TODO: how to find the only set bit in the long?
    public override string ToString() => Convert.ToString(Bits, 2);

    internal readonly long Bits;

    internal const int Size = 64;
  }
}