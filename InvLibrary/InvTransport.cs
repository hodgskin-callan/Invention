﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public static class TransportFoundation
  {
    public const int SendBufferSize = 65536;
    public const int ReceiveBufferSize = 65536;
    public const int ExceptionMessageLengthLimit = 15000;

    public const byte ProtocolSecureStream = 0x53; // ASCII 'S'
    public const byte ProtocolNegotiateStream = 0x4D; // ASCII 'N'
  }

  public interface TransportFactory
  {
    TransportConnection JoinConnection();
  }

  public interface TransportConnection
  {
    TransportFlow Flow { get; }
    TransportIdentity Identity { get; }
    void Drop();
  }

  public sealed class TransportIdentity
  {
    public TransportIdentity(string Name, string SID)
    {
      this.Name = Name;
      this.SID = SID;
    }

    public string Name { get; }
    public string SID { get; }
  }

  public sealed class TransportLink : IDisposable
  {
    public TransportLink(Inv.TransportFactory TransportFactory)
    {
      this.TransportFactory = TransportFactory;
      this.ConnectionPool = new Pool<TransportConnection>(() =>
      {
        var Connection = TransportFactory.JoinConnection(); // join the connection.

        JoinEvent?.Invoke(Connection);

        return Connection;
      }, C =>
      {
        C.Drop(); // drop the connection.

        DropEvent?.Invoke(C);
      }, Timeout: null, MinCapacity: 0);
      this.FaultedSignal = new AutoResetSignal(true, "TransportLink-Faulted");
      this.EstablishedSignal = new ManualResetSignal(false, "TransportLink-Established");
      this.FaultCriticalSection = new Inv.ExclusiveCriticalSection("Inv.TransportLink-Broker");
      this.ExchangeCriticalSection = new Inv.ReaderWriterCriticalSection("Inv.TransportLink-Exchange");
      ExchangeCriticalSection.NoTimeout();
    }
    public void Dispose()
    {
      ExchangeCriticalSection.Dispose();
      FaultedSignal.Dispose();
      EstablishedSignal.Dispose();
    }

    public bool Established { get; private set; }
    public bool IsOffline { get; private set; }
    public event Action ConnectEvent;
    public event Action DisconnectEvent;
    public event Action<Inv.TransportConnection> JoinEvent;
    public event Action<Inv.TransportConnection> DropEvent;
    public event Action<Exception> FaultBeginEvent;
    public event Action<Exception> FaultContinueEvent;
    public event Action<Exception> FaultEndEvent;
    public event Action FaultReconnectEvent;

    public void Connect()
    {
      if (!Established)
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(ConnectionPool.Count == 0, "ConnectionPool should be empty.");

        this.IsOffline = false;

        var Connection = ConnectionPool.Take();
        try
        {
          ConnectEvent?.Invoke();
        }
        catch (Exception Exception)
        {
          Connection.Drop();

          throw Exception.Preserve();
        }
        ConnectionPool.Return(Connection);

        this.Established = true;
      }
    }
    public void Disconnect()
    {
      if (Established)
      {
        this.Established = false;

        DisconnectEvent?.Invoke();

        ConnectionPool.Drain(); // disposes all active connections.

        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(ConnectionPool.Count == 0, "ConnectionPool should be empty.");
      }
    }
    public int ConnectionCount()
    {
      return ConnectionPool.Count;
    }
    public Inv.TransportConnection TakeConnection()
    {
      return ConnectionPool.Take();
    }
    public void ReturnConnection(Inv.TransportConnection Connection)
    {
      Debug.Assert(Connection != null);

      if (Connection != null)
        ConnectionPool.Return(Connection);
    }
    public void Exchange(Action<Inv.TransportConnection> Action)
    {
      if (ExchangeCriticalSection.IsAnyLockHeld)
      {
        // NOTE: this is a remote call made from within a call - or during a fault.
        CallConnection(Action);
      }
      else
      {
        Exception FaultException;
        do
        {
          using (ExchangeCriticalSection.EnterReadLock())
          {
            try
            {
              CallConnection(Action);

              FaultException = null;
            }
            catch (Inv.TransportFlowException FlowException)
            {
              // NOTE: a reconnection is required.
              FaultException = FlowException;
            }
            catch (Inv.TransportMarshalException MarshalException)
            {
              //Security exceptions of failure type Version or Session require reconnection
              if (MarshalException.RequiresReconnection)
                FaultException = MarshalException;
              else // NOTE: transport exceptions are marshalled over the RPC and do not signify a reconnection is required.
                throw MarshalException.Preserve();
            }
          }

          if (FaultException != null)
            FaultRecovery(FaultException);
        }
        while (Established && FaultException != null);
      }
    }
    public T Exchange<T>(Func<Inv.TransportConnection, T> Func)
    {
      T Result = default;

      Exchange((Action<Inv.TransportConnection>)(Connection => Result = Func(Connection)));

      return Result;
    }
    public void SimulateFault(TimeSpan TimeSpan)
    {
      var StartTime = DateTime.Now;

      void FaultDelay()
      {
        // NOTE: we want the reconnection to be attempted until the end of the simulation time.
        if (StartTime + TimeSpan > DateTime.Now)
          throw new Inv.TransportFlowException("Simulated unable to reconnect fault.");
      }

      Inv.TaskGovernor.RunActivity("InvTransportLink.SimulateFault", () =>
      {
        SimulateReconnectAction = FaultDelay;
        try
        {
          FaultRecovery(new Exception("Simulated remote connection fault."));
        }
        catch
        {
          // ignore exceptions during fault recovery?
        }
        finally
        {
          SimulateReconnectAction = null;
        }
      });
    }

    private void CallConnection(Action<Inv.TransportConnection> Action)
    {
      var Connection = ConnectionPool.Take();
      try
      {
        Interlocked.Increment(ref ActiveConnectionCount);
        try
        {
          Action(Connection);
        }
        finally
        {
          Interlocked.Decrement(ref ActiveConnectionCount);
        }
      }
      finally
      {
        ConnectionPool.Return(Connection);
      }
    }
    private void FaultRecovery(Exception FaultException)
    {
      bool IsFirstFaultThread;

      using (FaultCriticalSection.Lock())
      {
        IsFirstFaultThread = FaultedSignal.WaitOne(0);

        if (IsFirstFaultThread)
          EstablishedSignal.Reset();
      }

      if (!IsFirstFaultThread)
      {
        // Block this thread until reconnection is complete.
        EstablishedSignal.WaitOne();
      }
      else
      {
        using (ExchangeCriticalSection.EnterWriteLock())
        {
          try
          {
            var FaultSignalSet = false;

            try
            {
              var IsConnectionFailed = IsConnectionException(FaultException);

              if (IsConnectionFailed)
                this.IsOffline = true;

              FaultBeginEvent?.Invoke(FaultException);

              var Reconnecting = true;
              while (Reconnecting)
              {
                try
                {
                  if (Established)
                  {
                    SimulateReconnectAction?.Invoke();

                    if (Established)
                    {
                      ConnectionPool.Drain();
                      ConnectionPool.EnsureCapacity();

                      FaultReconnectEvent?.Invoke();
                    }
                  }

                  Reconnecting = false;
                }
                catch (Inv.TransportFlowException ContinueException)
                {
                  FaultContinueEvent?.Invoke(ContinueException);

                  // wait one second before trying again.
                  Inv.TaskGovernor.Sleep(1000);

                  Reconnecting = true;
                }
              }

              FaultEndEvent?.Invoke(FaultException);

              if (IsConnectionFailed)
                this.IsOffline = true;

              FaultSignalSet = true;
              FaultedSignal.Set();
            }
            catch (Exception TerminalException)
            {
              // NOTE: safety measure to ensure the fault management can't completely fail.
              if (!FaultSignalSet)
                FaultedSignal.Set();

              throw TerminalException.Preserve();
            }
          }
          finally
          {
            // Allow subsequent faulting threads to continue.
            EstablishedSignal.Set();
          }
        }
      }
    }
    private bool IsConnectionException(Exception Exception)
    {
      return Exception is Inv.TransportFlowException;
    }

    private readonly Inv.TransportFactory TransportFactory;
    private readonly Inv.Pool<Inv.TransportConnection> ConnectionPool;
    private readonly Inv.ReaderWriterCriticalSection ExchangeCriticalSection;
    private readonly Inv.ExclusiveCriticalSection FaultCriticalSection;
    private readonly AutoResetSignal FaultedSignal;
    private readonly ManualResetSignal EstablishedSignal;
    private Action SimulateReconnectAction;
    private int ActiveConnectionCount;
  }

  public sealed class TransportPacket
  {
    public TransportPacket(byte[] Buffer)
    {
      this.Buffer = Buffer;
    }

    public byte[] Buffer { get; }

    public TransportReceiver ToReceiver() => new TransportReceiver(this);
  }

  public sealed class TransportFlow
  {
    public TransportFlow(System.IO.Stream Stream)
    {
      this.Stream = Stream;
      this.IsActive = true;
    }
    public void Dispose()
    {
      if (IsActive)
      {
        this.IsActive = false;
        Stream.Dispose();
      }
    }

    public bool IsActive { get; private set; }
    public System.IO.Stream Stream { get; }

    public void SendPacket(Inv.TransportPacket RoutePacket)
    {
      if (!TrySendPacket(RoutePacket))
        throw new TransportFlowException("Send connection lost.");
    }
    public bool TrySendPacket(Inv.TransportPacket RoutePacket)
    {
      var Buffer = RoutePacket.Buffer;
      return Send(Buffer, 0, Buffer.Length);
    }
    public Inv.TransportPacket ReceivePacket()
    {
      var Result = TryReceivePacket();

      if (Result == null)
        throw new TransportFlowException("Receive connection lost.");

      return Result;
    }
    public Inv.TransportPacket TryReceivePacket()
    {
      // TODO: more efficient buffered socket reading?

      var DataBuffer = new byte[4];

      if (!Receive(DataBuffer, 0, 4))
        return null;

      var DataLength = BitConverter.ToInt32(DataBuffer, 0);

      if (DataLength > 0)
      {
        Array.Resize(ref DataBuffer, DataLength);

        if (!Receive(DataBuffer, 4, DataLength))
          return null;

        return new Inv.TransportPacket(DataBuffer);
      }
      else
      {
        return null;
      }
    }
    public async Task SendPacketAsync(Inv.TransportPacket RoutePacket)
    {
      if (!await TrySendPacketAsync(RoutePacket))
        throw new TransportFlowException("Send connection lost.");
    }
    public async Task<bool> TrySendPacketAsync(Inv.TransportPacket RoutePacket)
    {
      var Buffer = RoutePacket.Buffer;
      return await SendAsync(Buffer, 0, Buffer.Length);
    }
    public async Task<Inv.TransportPacket> ReceivePacketAsync()
    {
      var Result = await TryReceivePacketAsync();

      if (Result == null)
        throw new TransportFlowException("Receive connection lost.");

      return Result;
    }
    public async Task<Inv.TransportPacket> TryReceivePacketAsync()
    {
      // TODO: more efficient buffered socket reading?

      var DataBuffer = new byte[4];

      if (!await ReceiveAsync(DataBuffer, 0, 4))
        return null;

      var DataLength = BitConverter.ToInt32(DataBuffer, 0);

      if (DataLength > 0)
      {
        Array.Resize(ref DataBuffer, DataLength);

        if (!await ReceiveAsync(DataBuffer, 4, DataLength))
          return null;

        return new Inv.TransportPacket(DataBuffer);
      }
      else
      {
        return null;
      }
    }

    private bool Send(byte[] Buffer, int Offset, int Length)
    {
      try
      {
        Stream.Write(Buffer, Offset, Length);
      }
      catch
      {
        return false;
      }

      return true;
    }
    private bool Receive(byte[] Buffer, int Offset, int Length)
    {
      // NOTE: don't break on exception in here as socket disconnections are expected.
      //       downstream code must cope with ReceivePacket returning null.
      try
      {
        var Index = Offset;
        while (Index < Length)
        {
          var Actual = Stream.Read(Buffer, Index, Length - Index);

          if (Actual == 0)
            return false;

          Index += Actual;
        }
      }
      catch
      {
        return false;
      }

      return true;
    }
    private async Task<bool> SendAsync(byte[] Buffer, int Offset, int Length)
    {
      try
      {
        await Stream.WriteAsync(Buffer, Offset, Length);
      }
      catch
      {
        return false;
      }

      return true;
    }
    private async Task<bool> ReceiveAsync(byte[] Buffer, int Offset, int Length)
    {
      // NOTE: don't break on exception in here as socket disconnections are expected.
      //       downstream code must cope with ReceivePacket returning null.
      try
      {
        var Index = Offset;
        while (Index < Length)
        {
          var Actual = await Stream.ReadAsync(Buffer, Index, Length - Index);

          if (Actual == 0)
            return false;

          Index += Actual;
        }
      }
      catch
      {
        return false;
      }

      return true;
    }

    public IAsyncResult BeginReceivePacket(Action<Inv.TransportPacket> ReturnAction)
    {
      var Data = new TransportData(ReturnAction);

      return Stream.BeginRead(Data.Buffer, Data.Index, Data.Count, CallbackReceiveHeader, Data);
    }
    private void CallbackReceiveHeader(IAsyncResult Result)
    {
      var Data = (TransportData)Result.AsyncState;
      try
      {
        var Actual = IsActive ? Stream.EndRead(Result) : 0; // EndRead may throw exceptions.

        if (Actual == 0)
        {
          // disconnect.
          Data.Return(null);
          return;
        }

        Data.Index += Actual;
      }
      catch
      {
        // disconnect.
        Data.Return(null);
        return;
      }

      if (Data.Index < Data.Count)
      {
        Stream.BeginRead(Data.Buffer, Data.Index, Data.Count - Data.Index, CallbackReceiveHeader, Data);
      }
      else
      {
        var DataLength = BitConverter.ToInt32(Data.Buffer, 0);
        if (DataLength <= 0)
        {
          Data.Return(null);
        }
        else
        {
          Data.Count = DataLength;
          Array.Resize(ref Data.Buffer, Data.Count);

          Stream.BeginRead(Data.Buffer, Data.Index, Data.Count - Data.Index, CallbackReceivePayload, Data);
        }
      }
    }
    private void CallbackReceivePayload(IAsyncResult Result)
    {
      var Data = (TransportData)Result.AsyncState;
      try
      {
        var Actual = IsActive ? Stream.EndRead(Result) : 0; // EndRead may throw exceptions.

        if (Actual == 0)
        {
          // disconnect.
          Data.Return(null);
          return;
        }

        Data.Index += Actual;
      }
      catch
      {
        // disconnect.
        Data.Return(null);
        return;
      }

      if (Data.Index < Data.Count)
        Stream.BeginRead(Data.Buffer, Data.Index, Data.Count - Data.Index, CallbackReceivePayload, Data);
      else
        Data.Return(new Inv.TransportPacket(Data.Buffer));
    }

    private sealed class TransportData
    {
      internal TransportData(Action<Inv.TransportPacket> Return)
      {
        this.Return = Return;
      }

      public readonly Action<Inv.TransportPacket> Return;
      public byte[] Buffer = new byte[4];
      public int Index = 0;
      public int Count = 4;
    }
  }

  public sealed class TransportFlowException : Exception
  {
    public TransportFlowException(string Message)
      : base(Message)
    {
    }
  }

  public sealed class TransportMarshalException : Exception
  {
    public TransportMarshalException(Exception Exception, bool RequiresReconnection)
      : base(Exception.Message)
    {
      this.Type = Exception.GetType().FullName;

      this.RequiresReconnection = RequiresReconnection;
    }
    public TransportMarshalException(string Type, string Message, string Source, string StackTrace, bool RequiresReconnection)
      : base(Message)
    {
      this.Type = Type;
      this.SourceField = Source;
      this.StackTraceField = StackTrace;
      this.RequiresReconnection = RequiresReconnection;
    }

    public string Type { get; private set; }
    public bool RequiresReconnection { get; private set; }
    public override string Source => SourceField;
    public override string StackTrace => StackTraceField;

    private readonly string SourceField;
    private readonly string StackTraceField;
  }

  public enum TransportMarker : byte
  {
    Invalid = 0,
    AuthenticateRequest = 1,
    AuthenticateSuccess = 2,
    AuthenticateException = 3,
    CallCommand = 4,
    CallResult = 5,
    CallException = 6,
    Message = 7,
    DownloadRequest = 8,
    DownloadStream = 9,
    DownloadException = 10,
    KeepAlive = 255
  }

  public sealed class TransportSender : IDisposable
  {
    public TransportSender()
    {
      this.MemoryStream = new MemoryStream();

      this.Writer = new CompactWriter(MemoryStream, DisposeStream: true);
      Writer.WriteInt32(0); // reserve four bytes for the packet length.
    }
    public void Dispose()
    {
      Writer.Dispose();
    }

    public bool HasData => MemoryStream.Position > 4;
    public int PacketLength => (int)MemoryStream.Length;
    public Inv.CompactWriter Writer { get; }

    public void Reset()
    {
      MemoryStream.Position = 0;
      MemoryStream.SetLength(0);
      Writer.WriteInt32(0);
    }
    public Inv.TransportPacket ToPacket()
    {
      // re-write the packet length in the first four bytes.
      MemoryStream.Flush();
      MemoryStream.Position = 0;
      Writer.WriteInt32((int)MemoryStream.Length);

      return new Inv.TransportPacket(MemoryStream.ToArray());
    }
    public void WriteMarker(Inv.TransportMarker Marker)
    {
      Writer.WriteByte((byte)Marker);
    }
    public void WriteException(Inv.TransportMarshalException Exception)
    {
      Writer.WriteString(Exception.Type);
      Writer.WriteString(Exception.Message);
      Writer.WriteString(Exception.Source);
      Writer.WriteString(Exception.StackTrace);
      Writer.WriteBoolean(Exception.RequiresReconnection);
    }

    private readonly MemoryStream MemoryStream;
  }

  public sealed class TransportReceiver : IDisposable
  {
    internal TransportReceiver(Inv.TransportPacket Packet)
    {
      this.MemoryStream = new MemoryStream(Packet.Buffer);
      this.Reader = new CompactReader(MemoryStream, DisposeStream: true);

      this.PacketLength = Reader.ReadInt32();
      if (PacketLength != Packet.Buffer.Length)
        throw new Exception("Packet length is invalid.");
    }
    public void Dispose()
    {
      Reader.Dispose();
    }

    public Inv.CompactReader Reader { get; }
    public int PacketLength { get; }
    public bool EndOfPacket => MemoryStream.Position >= MemoryStream.Length;

    public TransportMarker ReadMarker()
    {
      return (TransportMarker)Reader.ReadByte();
    }
    public Inv.TransportMarshalException ReadException()
    {
      var Type = Reader.ReadString();
      var Message = Reader.ReadString();
      var Source = Reader.ReadString();
      var StackTrace = Reader.ReadString();
      var RequiresReconnection = Reader.ReadBoolean();

      return new Inv.TransportMarshalException(Type, Message, Source, StackTrace, RequiresReconnection);
    }

    private readonly MemoryStream MemoryStream;
  }
}