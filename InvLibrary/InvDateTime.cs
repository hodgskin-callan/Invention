﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Xml;
using System.Xml.Serialization;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Represents a calendar date.
  /// </summary>
  [XmlSchemaProvider("MySchema")]
  public struct Date : IComparable, IComparable<Inv.Date>, IEquatable<Inv.Date>, IFormattable, IDiscrete, IXmlSerializable
  {
    public Date(DateTime DateTime)
    {
      this.DateTime = System.DateTime.SpecifyKind(DateTime.Date, DateTimeKind.Unspecified);
    }
    public Date(int Year, int Month, int Day)
    {
      this.DateTime = new System.DateTime(Year, Month, Day, 0, 0, 0, DateTimeKind.Unspecified);
    }

    public static Inv.Date Now => new Inv.Date(DateTime.Now); // obsolete?
    public static Inv.Date Today => new Inv.Date(DateTime.Now);
    public static Inv.Date Yesterday => Now.AddDays(-1);
    public static Inv.Date Tomorrow => Now.AddDays(+1);
    public static readonly Inv.Date MinValue = new Inv.Date(DateTime.MinValue);
    public static readonly Inv.Date MaxValue = new Inv.Date(DateTime.MaxValue);

    /// <summary>
    /// Gets the day of the month represented by this instance, expressed as a value between 1 and 31.
    /// </summary>
    public int Day => DateTime.Day;
    /// <summary>
    /// Gets the month component of the date represented by this instance, expressed as a value between 1 and 12.
    /// </summary>
    public int Month => DateTime.Month;
    /// <summary>
    /// Gets the year component of the date represented by this instance, between 1 and 9999.
    /// </summary>
    public int Year => DateTime.Year;
    public int DayOfYear => DateTime.DayOfYear;
    public DayOfWeek DayOfWeek => DateTime.DayOfWeek;
    public int YearAndMonth => DateTime.Year * 100 + DateTime.Month;

    public DateTime ToDateTime()
    {
      return DateTime;
    }
    public int CompareTo(Inv.Date Date)
    {
      return DateTime.CompareTo(Date.DateTime);
    }
    public bool EqualTo(Inv.Date Date)
    {
      return DateTime == Date.DateTime;
    }
    public override bool Equals(object obj)
    {
      var Source = obj as Date?;

      if (Source == null)
        return false;
      else
        return EqualTo(Source.Value);
    }
    public override int GetHashCode()
    {
      return DateTime.GetHashCode();
    }
    public override string ToString()
    {
      return ToString(null, null);
    }
    public string ToString(IFormatProvider FormatProvider)
    {
      return ToString(null, FormatProvider);
    }
    public string ToString([StringSyntax(StringSyntaxAttribute.DateOnlyFormat)] string Format, IFormatProvider FormatProvider = null)
    {
      return DateTime.ToString(Format ?? GetDefaultFormat(FormatProvider), FormatProvider);
    }
    /// <summary>
    /// Returns the date as a string with details omitted relative to the current date within the local time zone.
    /// </summary>
    /// <remarks>
    /// <example>E.g.; "Fri, 23", "Fri, 23 Aug", or "Fri, 23 Aug 2024"</example>
    /// </remarks>
    /// <returns>The date as a string with details omitted relative to the current date within the local time zone.</returns>
    public string ToRelativeString()
    {
      string Result;
      var CurrentDate = Date.Now;

      if (DateTime.Year != CurrentDate.Year)
        Result = DateTime.ToString("ddd, d MMM yyyy");
      else if (DateTime.Month != CurrentDate.Month)
        Result = DateTime.ToString("ddd, d MMM");
      else
        Result = DateTime.ToString("ddd, d");

      return Result;
    }
    /// <summary>
    /// Returns the date as a string using short day and month names and an ordinal day of month.
    /// </summary>
    /// <remarks>
    /// <example>E.g.; "Fri 23rd Aug 2024"</example>
    /// </remarks>
    /// <returns>The date as a string using short day and month names and an ordinal day of month.</returns>
    public string ToAbbreviatedString()
    {
      return DateTime.ToString("ddd ") + DateTime.Day.ToOrdinal() + DateTime.ToString(" MMM yyyy");
    }
    /// <summary>
    /// Returns the date as a string using long day and month names and an ordinal day of month.
    /// </summary>
    /// <remarks>
    /// <example>E.g.; "Friday 23rd August 2024"</example>
    /// </remarks>
    /// <returns>The date as a string using long day and month names and an ordinal day of month.</returns>
    public string ToVerboseString()
    {
      return DateTime.ToString("dddd ") + DateTime.Day.ToOrdinal() + DateTime.ToString(" MMMM yyyy");
    }
    public string ToBriefPeriodString(Inv.Date LaterDate)
    {
      string Result = null;

      var PeriodDifference = LaterDate.ToTimePeriodDifference(this);

      if (PeriodDifference.Years > 0)
      {
        Result = PeriodDifference.Years.ToString() + "Y";

        if (PeriodDifference.HasOtherUnit(Inv.TimePeriodUnit.Years))
          Result += "+";
      }
      else if (PeriodDifference.Months > 0)
      {
        Result = PeriodDifference.Months.ToString() + "M";

        if (PeriodDifference.HasOtherUnit(Inv.TimePeriodUnit.Months))
          Result += "+";
      }
      else if (PeriodDifference.Weeks > 0)
      {
        Result = PeriodDifference.Weeks.ToString() + "W";

        if (PeriodDifference.HasOtherUnit(Inv.TimePeriodUnit.Weeks))
          Result += "+";
      }
      else if (PeriodDifference.Days > 0)
      {
        Result = PeriodDifference.Days.ToString() + "D";

        if (PeriodDifference.HasOtherUnit(Inv.TimePeriodUnit.Days))
          Result += "+";
      }

      return Result;
    }
    public Inv.Date AddHours(long Value)
    {
      return new Date(DateTime.AddHours(Value));
    }
    public Inv.Date AddDays(long Value)
    {
      return new Date(DateTime.AddDays(Value));
    }
    public Inv.Date AddDays(double Value)
    {
      return new Date(this.DateTime.AddDays(Value));
    }
    public Inv.Date AddWeeks(long Value)
    {
      return AddDays(Value * 7);
    }
    public Inv.Date AddMonths(int Value)
    {
      return new Date(this.DateTime.AddMonths(Value));
    }
    public Inv.Date AddYears(int Value)
    {
      return new Date(this.DateTime.AddYears(Value));
    }
    public Inv.Date AddDatePeriod(TimePeriod Period)
    {
      return AddYears(Period.Years).AddMonths(Period.Months).AddWeeks(Period.Weeks).AddDays(Period.Days).AddHours(Period.Hours);
    }
    public DateTimeOffset AddDateTimePeriod(TimePeriod Period)
    {
      return AddDatePeriod(Period) + new TimeSpan(0, Period.Hours, Period.Minutes, Period.Seconds, Period.Milliseconds);
    }
    public bool IsBetween(Inv.Date From, Inv.Date Until)
    {
      return this >= From && this <= Until;
    }
    /// <summary>
    /// Determines if this <see cref="Inv.Date"/> is a weekend.
    /// </summary>
    /// <returns></returns>
    public bool IsWeekend()
    {
      var DayOfWeek = DateTime.DayOfWeek;

      return DayOfWeek == DayOfWeek.Saturday || DayOfWeek == DayOfWeek.Sunday;
    }
    public bool IsWeekday()
    {
      return !IsWeekend();
    }
    public Inv.Date StartOfFinancialYear()
    {
      var Target = new Date(Year, 7, 1);

      if (this < Target)
        Target = new Date(Year - 1, 7, 1);

      return Target;
    }
    public Inv.Date EndOfFinancialYear()
    {
      var Target = new Date(Year, 06, 30);

      if (this > Target)
        Target = new Date(Year + 1, 06, 30);

      return Target;
    }
    public Inv.Date StartOfWeek(DayOfWeek StartOfWeek)
    {
      return DateTime.StartOfWeek(StartOfWeek).AsDate();
    }
    public Inv.Date StartOfWeek()
    {
      return DateTime.StartOfWeek().AsDate();
    }
    public Inv.Date EndOfWeek(DayOfWeek EndOfWeek)
    {
      return DateTime.EndOfWeek(EndOfWeek).AsDate();
    }
    public Inv.Date EndOfWeek()
    {
      return DateTime.EndOfWeek().AsDate();
    }
    public Inv.Date StartOfMonth()
    {
      return new Date(Year, Month, 1);
    }
    public Inv.Date EndOfMonth()
    {
      return new Date(Year, Month, DateTime.DaysInMonth(Year, Month));
    }
    public Inv.Date StartOfQuarter()
    {
      return new Date(DateTime.StartOfQuarter());
    }
    public Inv.Date EndOfQuarter()
    {
      return new Date(DateTime.EndOfQuarter());
    }
    public Inv.Date StartOfYear()
    {
      return new Date(Year, 1, 1);
    }
    public Inv.Date EndOfYear()
    {
      return new Date(Year, 12, 31);
    }
    public DateTime StartOfDay()
    {
      return ToDateTime().StartOfDay();
    }
    public DateTime EndOfDay()
    {
      return ToDateTime().EndOfDay();
    }
    /// <summary>
    /// Determines the next date which falls the day indicated by <paramref name="DayOfWeek"/>.
    /// </summary>
    /// <param name="DayOfWeek">The <see cref="DayOfWeek"/> value to consider.</param>
    /// <returns>A <see cref="Date"/> representing the next occurrence of <paramref name="DayOfWeek"/>.</returns>
    public Inv.Date NextDayOfWeek(DayOfWeek DayOfWeek)
    {
      var DayOfWeekSeries = DayOfWeekHelper.DayOfWeekSeries().ToDistinctList();

      var CurrentIndex = DayOfWeekSeries.IndexOf(DateTime.DayOfWeek);
      var FindIndex = DayOfWeekSeries.IndexOf(DayOfWeek);

      return AddDays(CurrentIndex < FindIndex ? FindIndex - CurrentIndex : 7 - (CurrentIndex - FindIndex));
    }
    /// <summary>
    /// Determines the next date which falls on a day indicated by <paramref name="DayOfWeeks"/>.
    /// </summary>
    /// <param name="DayOfWeeks">The <see cref="DayOfWeek"/> values to consider.</param>
    /// <returns>A <see cref="Date"/> representing the next occurrence of a day of week from <paramref name="DayOfWeeks"/>.</returns>
    /// <exception cref="Exception"><paramref name="DayOfWeeks"/> is empty.</exception>
    public Inv.Date NextDayOfWeek(IEnumerable<DayOfWeek> DayOfWeeks)
    {
      if (!DayOfWeeks.Any())
        throw new Exception($"{nameof(DayOfWeeks)} must not be empty");

      var Attempt = AddDays(1);
      while (!DayOfWeeks.Contains(Attempt.DayOfWeek))
        Attempt = Attempt.AddDays(1);

      return Attempt;
    }
    /// <summary>
    /// Determines the previous date which falls the day indicated by <paramref name="DayOfWeek"/>.
    /// </summary>
    /// <param name="DayOfWeek">The <see cref="DayOfWeek"/> value to consider.</param>
    /// <returns>A <see cref="Date"/> representing the previous occurrence of <paramref name="DayOfWeek"/>.</returns>
    public Inv.Date PreviousDayOfWeek(DayOfWeek DayOfWeek)
    {
      var DayOfWeekSeries = DayOfWeekHelper.DayOfWeekSeries().ToDistinctList();

      var CurrentIndex = DayOfWeekSeries.IndexOf(DateTime.DayOfWeek);
      var FindIndex = DayOfWeekSeries.IndexOf(DayOfWeek);

      return AddDays(CurrentIndex > FindIndex ? FindIndex - CurrentIndex : (FindIndex - CurrentIndex) - 7);
    }
    /// <summary>
    /// Determines the previous date which falls on a day indicated by <paramref name="DayOfWeeks"/>.
    /// </summary>
    /// <param name="DayOfWeeks">The <see cref="DayOfWeek"/> values to consider.</param>
    /// <returns>A <see cref="Date"/> representing the previous occurrence of a day of week from <paramref name="DayOfWeeks"/>.</returns>
    /// <exception cref="Exception"><paramref name="DayOfWeeks"/> is empty.</exception>
    public Inv.Date PreviousDayOfWeek(IEnumerable<DayOfWeek> DayOfWeeks)
    {
      if (!DayOfWeeks.Any())
        throw new Exception($"{nameof(DayOfWeeks)} must not be empty");

      var Attempt = AddDays(-1);
      while (!DayOfWeeks.Contains(Attempt.DayOfWeek))
        Attempt = Attempt.AddDays(-1);

      return Attempt;
    }
    /// <summary>
    /// Determines the number of dates in the inclusive range from <paramref name="StartDate"/> to this date.
    /// </summary>
    /// <param name="StartDate">The starting date of the range. If it comes before this date, the result is zero.</param>
    /// <returns>A count of the dates in the range from <paramref name="StartDate"/> to this date.</returns>
    public int CountDaysInRange(Inv.Date StartDate)
    {
      return (int)Math.Floor(ToTimeSpanDifference(StartDate).TotalDays) + 1;
    }
    /// <summary>
    /// Determines the number of whole weeks in the inclusive range from <paramref name="StartDate"/> to this date.
    /// </summary>
    /// <param name="StartDate">The starting date of the range. If it comes before this date, the result is zero.</param>
    /// <returns>A count of whole weeks in the range from <paramref name="StartDate"/> to this date.</returns>
    public int CountWeeksInRange(Inv.Date StartDate)
    {
      return CountDaysInRange(StartDate) / 7;
    }
    /// <summary>
    /// Determines whether this <see cref="Inv.Date"/> is within the bounds defined by <paramref name="Left"/> and <paramref name="Right"/> and not equal to either of the bounds.
    /// </summary>
    /// <param name="Left">The lower bound or null to indicate an unbounded range.</param>
    /// <param name="Right">The upper bound or null to indicate an unbounded range.</param>
    /// <returns>A <see cref="bool"/> indicating whether the value is within the bounds.</returns>
    public bool InClosedIntervalAllowingNulls(Inv.Date? Left, Inv.Date? Right)
    {
      return (Left is null || Left < this) && (Right is null || this < Right);
    }
    /// <summary>
    /// Determines whether this <see cref="Inv.Date"/> is within the bounds defined by <paramref name="Left"/> and <paramref name="Right"/> and not equal to the <paramref name="Right"/> bound.
    /// </summary>
    /// <param name="Left">The lower bound or null to indicate an unbounded range.</param>
    /// <param name="Right">The upper bound or null to indicate an unbounded range.</param>
    /// <returns>A <see cref="bool"/> indicating whether the value is within the bounds.</returns>
    public bool InLeftOpenIntervalAllowingNulls(Inv.Date? Left, Inv.Date? Right)
    {
      return (Left is null || Left <= this) && (Right is null || this < Right);
    }
    /// <summary>
    /// Determines whether this <see cref="Inv.Date"/> is within the bounds defined by <paramref name="Left"/> and <paramref name="Right"/> and not equal to the <paramref name="Left"/> bound.
    /// </summary>
    /// <param name="Left">The lower bound or null to indicate an unbounded range.</param>
    /// <param name="Right">The upper bound or null to indicate an unbounded range.</param>
    /// <returns>A <see cref="bool"/> indicating whether the value is within the bounds.</returns>
    public bool InRightOpenIntervalAllowingNulls(Inv.Date? Left, Inv.Date? Right)
    {
      return (Left is null || Left < this) && (Right is null || this <= Right);
    }
    /// <summary>
    /// Determines whether this <see cref="Inv.Date"/> is within the bounds defined by <paramref name="Left"/> and <paramref name="Right"/>, inclusive of those values.
    /// </summary>
    /// <param name="Left">The lower bound or null to indicate an unbounded range.</param>
    /// <param name="Right">The upper bound or null to indicate an unbounded range.</param>
    /// <returns>A <see cref="bool"/> indicating whether the value is within the bounds.</returns>
    public bool BetweenInclusiveAllowingNulls(Inv.Date? Left, Inv.Date? Right)
    {
      return (Left is null || Left <= this) && (Right is null || this <= Right);
    }
    /// <summary>
    /// Enumerates all <see cref="Inv.Date"/> values from this to <paramref name="LastDate"/>. If <paramref name="LastDate"/> comes before this <see cref="Inv.Date"/>, an empty enumerable will be returned.
    /// </summary>
    /// <param name="LastDate">The final date to include in the enumerable.</param>
    /// <returns>An enumerable representing all dates from this to <paramref name="LastDate"/>.</returns>
    public IEnumerable<Inv.Date> ToDateSeries(Inv.Date LastDate)
    {
      if (this > LastDate)
        yield break;

      var CurrentDate = this;
      do
      {
        yield return CurrentDate;

        CurrentDate = CurrentDate.AddDays(1);
      } while (CurrentDate <= LastDate);
    }
    public Inv.TimePeriod ToTimePeriodDifference(Inv.Date Date)
    {
      return Inv.TimePeriod.FromDateTimeDifference(this.DateTime, Date.DateTime);
    }
    public TimeSpan ToTimeSpanDifference(Inv.Date StartDate)
    {
      return this.DateTime - StartDate.DateTime;
    }
    public Inv.Date NextAnniversaryDate(Inv.Date RelativeDate, Inv.TimePeriod TimePeriod)
    {
      Inv.Date NextDate;
      var Count = 1;
      do
      {
        NextDate = this.AddDatePeriod(TimePeriod * Count);
        Count++;
      }
      while (NextDate < RelativeDate);

      return NextDate;
    }
    public bool IsAnniversaryOf(Inv.Date AnniversaryDate, Inv.TimePeriod TimePeriod)
    {
      if (this <= AnniversaryDate)
        return false;

      return AnniversaryDate.NextAnniversaryDate(this, TimePeriod) == this;
    }
    public Inv.Date NextEndOfPeriodDate(Inv.Date RelativeDate, Inv.TimePeriod TimePeriod)
    {
      Inv.Date NextDate;
      var Count = 1;
      do
      {
        NextDate = this.AddDatePeriod(TimePeriod * Count).AddDays(-1);
        Count++;
      }
      while (NextDate < RelativeDate);

      return NextDate;
    }
    public bool IsEndOfPeriodAnniversaryOf(Inv.Date PeriodFromDate, Inv.TimePeriod TimePeriod)
    {
      if (this <= PeriodFromDate)
        return false;

      return PeriodFromDate.NextEndOfPeriodDate(this, TimePeriod) == this;
    }
    public long ToDateTimeBinary()
    {
      return DateTime.ToBinary();
    }

    public static Inv.Date Min(Inv.Date d1, Inv.Date d2)
    {
      if (d1 <= d2)
        return d1;
      else
        return d2;
    }
    public static Inv.Date Max(Inv.Date d1, Inv.Date d2)
    {
      if (d1 >= d2)
        return d1;
      else
        return d2;
    }
    public static bool TryParse(string Input, out Inv.Date Date)
    {
      var Result = DateTime.TryParse(Input, out var Value);

      if (Result)
        Date = new Inv.Date(Value);
      else
        Date = Inv.Date.MinValue;

      return Result;
    }
    public static Inv.Date Parse(string Text)
    {
      if (!TryParse(Text, out var Result))
        throw new Exception("Invalid date format: " + Text);

      return Result;
    }
    public static bool TryParseExact(string Input, string Format, out Inv.Date Date)
    {
      var Result = DateTime.TryParseExact(Input, Format, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out var Value);

      if (Result)
        Date = new Inv.Date(Value);
      else
        Date = Inv.Date.MinValue;

      return Result;
    }
    public static bool TryParseExact(string Input, string[] FormatArray, out Inv.Date Date)
    {
      var Result = DateTime.TryParseExact(Input, FormatArray, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out var Value);

      if (Result)
        Date = new Inv.Date(Value);
      else
        Date = Inv.Date.MinValue;

      return Result;
    }
    public static Inv.Date ParseExact(string Text, string Format)
    {
      if (!TryParseExact(Text, Format, out var Result))
        throw new Exception("Invalid date format: " + Text);

      return Result;
    }
    public static Inv.Date ParseExact(string Text, string[] FormatArray)
    {
      if (!TryParseExact(Text, FormatArray, out var Result))
        throw new Exception("Invalid date format: " + Text);

      return Result;
    }
    public static bool operator >(Inv.Date d1, Inv.Date d2)
    {
      return d1.DateTime > d2.DateTime;
    }
    public static bool operator >(Inv.Date d1, DateTimeOffset d2)
    {
      return d1.DateTime > d2.DateTime;
    }
    public static bool operator >(DateTimeOffset d1, Inv.Date d2)
    {
      return d1.DateTime > d2.DateTime;
    }
    public static bool operator <(Inv.Date d1, Inv.Date d2)
    {
      return d1.DateTime < d2.DateTime;
    }
    public static bool operator <(Inv.Date d1, DateTimeOffset d2)
    {
      return d1.DateTime < d2.DateTime;
    }
    public static bool operator <(DateTimeOffset d1, Inv.Date d2)
    {
      return d1.DateTime < d2.DateTime;
    }
    public static bool operator ==(Inv.Date d1, Inv.Date d2)
    {
      return d1.DateTime == d2.DateTime;
    }
    public static bool operator ==(Inv.Date d1, DateTimeOffset d2)
    {
      return d1.DateTime == d2.DateTime;
    }
    public static bool operator !=(Inv.Date d1, Inv.Date d2)
    {
      return d1.DateTime != d2.DateTime;
    }
    public static bool operator !=(Inv.Date d1, DateTimeOffset d2)
    {
      return d1.DateTime != d2.DateTime;
    }
    public static bool operator >=(Inv.Date d1, Inv.Date d2)
    {
      return d1.DateTime >= d2.DateTime;
    }
    public static bool operator >=(Inv.Date d1, DateTimeOffset d2)
    {
      return d1.DateTime >= d2.DateTime;
    }
    public static bool operator >=(DateTimeOffset d1, Inv.Date d2)
    {
      return d1.DateTime >= d2.DateTime;
    }
    public static bool operator <=(Inv.Date d1, Inv.Date d2)
    {
      return d1.DateTime <= d2.DateTime;
    }
    public static bool operator <=(Inv.Date d1, DateTimeOffset d2)
    {
      return d1.DateTime <= d2.DateTime;
    }
    public static bool operator <=(DateTimeOffset d1, Inv.Date d2)
    {
      return d1.DateTime <= d2.DateTime;
    }
    public static DateTime operator +(Inv.Date d1, TimeSpan t2)
    {
      return d1.DateTime + t2;
    }
    public static DateTime operator +(Inv.Date d, Inv.TimePeriod p)
    {
      var Result = d;

      if (p.Years > 0)
        Result = Result.AddYears(p.Years);

      if (p.Months > 0)
        Result = Result.AddMonths(p.Months);

      if (p.Weeks > 0)
        Result = Result.AddWeeks(p.Weeks);

      if (p.Days > 0)
        Result = Result.AddDays(p.Days);

      return Result.DateTime + new TimeSpan(p.Hours, p.Minutes, p.Seconds, p.Milliseconds);
    }
    public static DateTime operator +(Inv.Date d, Inv.Time p)
    {
      return d.DateTime + p.ToTimeSpan();
    }
    public static DateTime operator -(Inv.Date d, Inv.TimePeriod p)
    {
      var Result = d;

      if (p.Years > 0)
        Result = Result.AddYears(-p.Years);

      if (p.Months > 0)
        Result = Result.AddMonths(-p.Months);

      if (p.Weeks > 0)
        Result = Result.AddWeeks(-p.Weeks);

      if (p.Days > 0)
        Result = Result.AddDays(-p.Days);

      return Result.DateTime - new TimeSpan(p.Hours, p.Minutes, p.Seconds, p.Milliseconds);
    }
    public static string FormatInclusiveDateDifference(Inv.Date From, Inv.Date Until)
    {
      return (Until.AddDays(1).ToTimePeriodDifference(From)).ToStringComprehensive();
    }

    int IComparable<Inv.Date>.CompareTo(Inv.Date other)
    {
      return CompareTo(other);
    }
    bool IEquatable<Inv.Date>.Equals(Inv.Date other)
    {
      return EqualTo(other);
    }
    int IComparable.CompareTo(object obj)
    {
      return CompareTo((Date)obj);
    }
    string IFormattable.ToString([StringSyntax(StringSyntaxAttribute.DateOnlyFormat)] string format, IFormatProvider formatProvider)
    {
      return ToString(format, formatProvider);
    }
    object IDiscrete.PreviousValue()
    {
      return AddDays(-1);
    }
    object IDiscrete.NextValue()
    {
      return AddDays(+1);
    }

    private static XmlQualifiedName MySchema(object xs)
    {
      return new XmlQualifiedName("date", "http://www.w3.org/2001/XMLSchema");
    }
    System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
    {
      return null; // NOTE: this is required to be null.
    }
    void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
    {
      var Result = reader.ReadInnerXml();

      DateTime = DateTime.ParseExact(Result, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None);
    }
    void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
    {
      writer.WriteRaw(DateTime.ToString("yyyy-MM-dd"));
    }

    private static string GetDefaultFormat(IFormatProvider FormatProvider) => System.Globalization.DateTimeFormatInfo.GetInstance(FormatProvider).ShortDatePattern;

    private /*readonly*/ DateTime DateTime;
  }

  /// <summary>
  /// Represents a clock time.
  /// </summary>
  [XmlSchemaProvider("MySchema")]
  public struct Time : IComparable, IComparable<Inv.Time>, IEquatable<Inv.Time>, IFormattable, IDiscrete, IXmlSerializable
  {
    public Time(DateTime DateTime)
    {
      // The year has been changed from 1 to 2000 to prevent overflow exceptions when adding (reasonable quantities of) negative units of time.
      this.DateTime = new System.DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Unspecified) + DateTime.TimeOfDay;
    }
    public Time(TimeSpan TimeSpan)
    {
      this.DateTime = new System.DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Unspecified) + TimeSpan;
    }
    public Time(int Hour, int Minute, int Second = 0, int Millisecond = 0)
    {
      this.DateTime = new System.DateTime(2000, 1, 1, Hour, Minute, Second, Millisecond, DateTimeKind.Unspecified);
    }

    public static Inv.Time Now => new Inv.Time(DateTime.Now);
    public static readonly Inv.Time MinValue = new Inv.Time(DateTime.MinValue);
    public static readonly Inv.Time MaxValue = new Inv.Time(DateTime.MaxValue);
    public static readonly Inv.Time Midday = new Inv.Time(12, 00);
    public static readonly Inv.Time Midnight = new Inv.Time(00, 00);

    public int Hour => DateTime.Hour;
    public int Minute => DateTime.Minute;
    public int Second => DateTime.Second;
    public int Millisecond => DateTime.Millisecond;

    public DateTime ToDateTime()
    {
      return DateTime;
    }
    public TimeSpan ToTimeSpan()
    {
      return DateTime.TimeOfDay;
    }
    public long ToDateTimeBinary()
    {
      return DateTime.ToBinary();
    }
    public int CompareTo(Inv.Time Value)
    {
      return DateTime.CompareTo(Value.DateTime);
    }
    public bool EqualTo(Inv.Time Value)
    {
      return DateTime == Value.DateTime;
    }
    public int CompareTo(object obj)
    {
      return CompareTo((Time)obj);
    }
    public override bool Equals(object obj)
    {
      var Source = obj as Time?;

      if (Source == null)
        return false;
      else
        return EqualTo(Source.Value);
    }
    public override int GetHashCode()
    {
      return DateTime.GetHashCode();
    }
    public override string ToString()
    {
      return ToString(null, null);
    }
    public string ToLongString(IFormatProvider FormatProvider = null)
    {
      return ToString(System.Globalization.DateTimeFormatInfo.GetInstance(FormatProvider).LongTimePattern, FormatProvider);
    }
    public string ToString([StringSyntax(StringSyntaxAttribute.TimeOnlyFormat)] string Format)
    {
      return ToString(Format, null);
    }
    public string ToString([StringSyntax(StringSyntaxAttribute.TimeOnlyFormat)] string Format, IFormatProvider FormatProvider)
    {
      return DateTime.ToString(Format ?? GetDefaultFormat(FormatProvider), FormatProvider);
    }
    public Inv.Time AddHours(double value)
    {
      return new Time(this.DateTime.AddHours(value));
    }
    public Inv.Time AddMinutes(double value)
    {
      return new Time(this.DateTime.AddMinutes(value));
    }
    public Inv.Time AddSeconds(double value)
    {
      return new Time(this.DateTime.AddSeconds(value));
    }
    public Inv.Time AddMilliseconds(double value)
    {
      return new Time(this.DateTime.AddMilliseconds(value));
    }
    public Inv.Time AddPeriod(TimePeriod Period)
    {
      return AddHours(Period.Hours).AddMinutes(Period.Minutes).AddSeconds(Period.Seconds).AddMilliseconds(Period.Milliseconds);
    }
    public bool IsBetween(Inv.Time From, Inv.Time Until)
    {
      if (From <= Until)
        return this >= From && this <= Until;
      else
        return this >= From || this <= Until;
    }
    public Inv.Time RoundUp(TimeSpan ts)
    {
      return new Time(DateTime.RoundUp(ts));
    }
    public Inv.Time RoundDown(TimeSpan ts)
    {
      return new Time(DateTime.RoundDown(ts));
    }
    /// <summary>
    /// Returns this time with any sub-minute precision components zeroed.
    /// </summary>
    /// <returns></returns>
    public Inv.Time StartOfMinute()
    {
      return new Inv.Time(Hour, Minute, 0, 0);
    }
    /// <summary>
    /// Returns this time with any sub-second precision components zeroed.
    /// </summary>
    /// <returns></returns>
    public Inv.Time StartOfSecond()
    {
      return new Inv.Time(Hour, Minute, Second, 0);
    }

    public static Inv.Time Min(Inv.Time t1, Inv.Time t2)
    {
      if (t1 <= t2)
        return t1;
      else
        return t2;
    }
    public static Inv.Time Max(Inv.Time t1, Inv.Time t2)
    {
      if (t1 >= t2)
        return t1;
      else
        return t2;
    }
    public static Inv.Time Parse(string Text)
    {
      if (!TryParse(Text, out var Result))
        throw new Exception("Invalid time format: " + Text);

      return Result;
    }
    public static bool TryParse(string Input, out Inv.Time Time)
    {
      var Result = DateTime.TryParse(Input, out var Value);

      if (Result)
        Time = new Inv.Time(Value);
      else
        Time = Inv.Time.MinValue;

      return Result;
    }
    public static bool TryParseExact(string Input, string Format, out Inv.Time Time)
    {
      var Result = DateTime.TryParseExact(Input, Format, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out var Value);

      if (Result)
        Time = new Inv.Time(Value);
      else
        Time = Inv.Time.MinValue;

      return Result;
    }
    public static bool TryParseExact(string Input, string[] FormatArray, out Inv.Time Time)
    {
      var Result = DateTime.TryParseExact(Input, FormatArray, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out var Value);

      if (Result)
        Time = new Inv.Time(Value);
      else
        Time = Inv.Time.MinValue;

      return Result;
    }
    public static Inv.Time ParseExact(string Text, string Format)
    {
      if (!TryParseExact(Text, Format, out var Result))
        throw new Exception("Invalid time format: " + Text);

      return Result;
    }
    public static Inv.Time ParseExact(string Text, string[] FormatArray)
    {
      if (!TryParseExact(Text, FormatArray, out var Result))
        throw new Exception("Invalid time format: " + Text);

      return Result;
    }
    public static Inv.Time? ParseOrDefault(string Value, Time? Default = null)
    {
      if (TryParse(Value, out var Result))
        return Result;
      else
        return Default;
    }
    public static bool operator >(Inv.Time d1, Inv.Time d2)
    {
      return d1.DateTime > d2.DateTime;
    }
    public static bool operator <(Inv.Time d1, Inv.Time d2)
    {
      return d1.DateTime < d2.DateTime;
    }
    public static bool operator ==(Inv.Time d1, Inv.Time d2)
    {
      return d1.DateTime == d2.DateTime;
    }
    public static bool operator !=(Inv.Time d1, Inv.Time d2)
    {
      return d1.DateTime != d2.DateTime;
    }
    public static bool operator >=(Inv.Time d1, Inv.Time d2)
    {
      return d1.DateTime >= d2.DateTime;
    }
    public static bool operator <=(Inv.Time d1, Inv.Time d2)
    {
      return d1.DateTime <= d2.DateTime;
    }
    public static Inv.Time operator -(Inv.Time d, TimeSpan t)
    {
      return new Time(d.DateTime - t);
    }
    public static TimeSpan operator -(Inv.Time d1, Inv.Time d2)
    {
      return d1.DateTime - d2.DateTime;
    }
    public static Inv.Time operator +(Inv.Time d, TimeSpan t)
    {
      return new Time(d.DateTime + t);
    }

    int IComparable<Inv.Time>.CompareTo(Inv.Time other)
    {
      return CompareTo(other);
    }
    bool IEquatable<Inv.Time>.Equals(Inv.Time other)
    {
      return EqualTo(other);
    }
    int IComparable.CompareTo(object obj)
    {
      return CompareTo((Time)obj);
    }
    string IFormattable.ToString([StringSyntax(StringSyntaxAttribute.TimeOnlyFormat)] string format, IFormatProvider formatProvider)
    {
      return ToString(format, formatProvider);
    }
    object IDiscrete.PreviousValue()
    {
      if (Hour == 0 && Minute == 0)
        return this;

      return AddMinutes(-1);
    }
    object IDiscrete.NextValue()
    {
      if (Hour == 23 && Minute == 59)
        return this;

      return AddMinutes(+1);
    }

    private static XmlQualifiedName MySchema(object xs)
    {
      return new XmlQualifiedName("time", "http://www.w3.org/2001/XMLSchema");
    }
    System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
    {
      return null; // NOTE: this is required to be null.
    }
    void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
    {
      var Result = reader.ReadInnerXml();
      DateTime = new DateTime(2000, 1, 1) + DateTime.ParseExact(Result, new[] { "HH:mm:ss.fffffff", "HH:mm:ss.ffffff" }, null, System.Globalization.DateTimeStyles.None).TimeOfDay;
    }
    void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
    {
      writer.WriteRaw(DateTime.ToString("HH:mm:ss.fffffff"));
    }

    private static string GetDefaultFormat(IFormatProvider FormatProvider) => System.Globalization.DateTimeFormatInfo.GetInstance(FormatProvider).ShortTimePattern;

    private /*readonly*/ DateTime DateTime;
  }

  /// <summary>
  /// Represents a time interval using calendar and clock concepts.
  /// </summary>
  [XmlSchemaProvider("MySchema")]
  public struct TimePeriod : IEquatable<TimePeriod>, IXmlSerializable
  {
    // Note: TimePeriod cannot implement the IComparable or IComparable<T> interfaces due to it being a structure that operates on relative dates.
    //  e.g. "1 month" is greater than "4 weeks" when starting from any date not inside in February, but from within February the two are the same.
    //  To compare two time periods, add them both to a specific consistent date and then compare the resultant dates.
    public static readonly Inv.TimePeriod Zero = new Inv.TimePeriod();

    public TimePeriod(int Years = 0, int Months = 0, int Weeks = 0, int Days = 0, int Hours = 0, int Minutes = 0, int Seconds = 0, int Milliseconds = 0)
    {
      this.Years = Years;
      this.Months = Months;
      this.Weeks = Weeks;
      this.Days = Days;
      this.Hours = Hours;
      this.Minutes = Minutes;
      this.Seconds = Seconds;
      this.Milliseconds = Milliseconds;
    }
    public TimePeriod(Inv.TimePeriodUnit PeriodUnit, int PeriodValue)
      : this()
    {
      switch (PeriodUnit)
      {
        case Inv.TimePeriodUnit.Years: this.Years = PeriodValue; break;
        case Inv.TimePeriodUnit.Months: this.Months = PeriodValue; break;
        case Inv.TimePeriodUnit.Weeks: this.Weeks = PeriodValue; break;
        case Inv.TimePeriodUnit.Days: this.Days = PeriodValue; break;
        case Inv.TimePeriodUnit.Hours: this.Hours = PeriodValue; break;
        case Inv.TimePeriodUnit.Minutes: this.Minutes = PeriodValue; break;
        case Inv.TimePeriodUnit.Seconds: this.Seconds = PeriodValue; break;
        case Inv.TimePeriodUnit.Milliseconds: this.Milliseconds = PeriodValue; break;
        default:
          throw new Exception("PeriodUnits not handled.");
      }
    }

    public int Years { get; private set; }
    public int Months { get; private set; }
    public int Weeks { get; private set; }
    public int Days { get; private set; }
    public int Hours { get; private set; }
    public int Minutes { get; private set; }
    public int Seconds { get; private set; }
    public int Milliseconds { get; private set; }

    /// <summary>
    /// Returns a <see cref="string"/> representation of the time period as a value relative to now in all caps with at most two units of accuracy.
    /// </summary>
    /// <remarks>
    /// <example>E.g.; "1 YEAR AND 2 MONTHS AGO" or "YESTERDAY" or "TOMORROW" or "JUST NOW"</example>
    /// </remarks>
    /// <param name="IsInPast">Whether or not the period represents a time in the past.</param>
    /// <returns>A <see cref="string"/> representation of the time period as a time relative to now.</returns>
    public string ToStringRelative(bool IsInPast)
    {
      var FormatTimePeriod = IsInPast ? this : -this;

      if (FormatTimePeriod.Years > 1)
      {
        var Result = "OVER " + FormatTimePeriod.Years.ToString() + " YEAR".Plural(FormatTimePeriod.Years);

        if (IsInPast)
          Result += " AGO";

        return Result;
      }
      else if (FormatTimePeriod.Years == 1)
      {
        var Result = "1 YEAR";

        if (FormatTimePeriod.Months > 0)
          Result += " AND " + FormatTimePeriod.Months.ToString() + " MONTH".Plural(FormatTimePeriod.Months);

        if (IsInPast)
          Result += " AGO";

        return Result;
      }
      else if (FormatTimePeriod.Months > 0)
      {
        var Result = FormatTimePeriod.Months.ToString() + " MONTH".Plural(FormatTimePeriod.Months);

        if (IsInPast)
          Result += " AGO";

        return Result;
      }
      else if (FormatTimePeriod.Weeks > 0)
      {
        var Result = FormatTimePeriod.Weeks.ToString() + " WEEK".Plural(FormatTimePeriod.Weeks);

        if (IsInPast)
          Result += " AGO";

        return Result;
      }
      else if (FormatTimePeriod.Days == 1)
      {
        return IsInPast ? "YESTERDAY" : "TOMORROW";
      }
      else if (FormatTimePeriod.Days > 0)
      {
        var Result = FormatTimePeriod.Days.ToString() + " DAY".Plural(FormatTimePeriod.Days);

        if (IsInPast)
          Result += " AGO";

        return Result;
      }
      else if (FormatTimePeriod.Hours == 0 && FormatTimePeriod.Minutes == 0)
      {
        return "JUST NOW";
      }
      else
      {
        var Result = "";

        if (FormatTimePeriod.Hours > 0)
        {
          Result += FormatTimePeriod.Hours.ToString() + " HOUR".Plural(FormatTimePeriod.Hours);

          if (FormatTimePeriod.Minutes > 0)
            Result += " AND ";
        }

        if (FormatTimePeriod.Minutes > 0)
          Result += FormatTimePeriod.Minutes.ToString() + " MINUTE".Plural(FormatTimePeriod.Minutes);

        if (IsInPast)
          Result += " AGO";

        return Result;
      }
    }
    /// <summary>
    /// Returns the time period as a human-readable <see cref="string"/> including each non-zero unit in the time period.
    /// </summary>
    /// <remarks>
    /// <example>E.g.; "1 year, 2 months, 3 weeks, 4 days, 5 hours, 6 minutes, 7 seconds and 8 milliseconds"</example>
    /// </remarks>
    /// <returns>A human-readable <see cref="string"/> describing each unit in the time period.</returns>
    public string ToStringComprehensive()
    {
      var TimePeriod = this;

      return EnumHelper.GetEnumerable<Inv.TimePeriodUnit>().Where(U => TimePeriod.HasUnit(U)).Select(U =>
      {
        var Value = TimePeriod.ValueByUnit(U);

        return Value.ToString() + " " + U.ToString().ToLower().Singular(Value);
      }).AsNaturalAndSeparatedText();
    }
    /// <summary>
    /// Returns the time period as a human-readable <see cref="string"/> describing a repeating interval.
    /// </summary>
    /// <remarks>
    /// <example>E.g.; "Yearly" or "2 Monthly" or "3 Weekly and 4 Daily"</example>
    /// </remarks>
    /// <returns>A human-readable <see cref="string"/> representing the period as a repeating interval.</returns>
    public string ToStringAsAnniversary()
    {
      var TimePeriod = this;

      return EnumHelper.GetEnumerable<Inv.TimePeriodUnit>().Where(U => TimePeriod.HasUnit(U)).Select(U =>
      {
        var Value = TimePeriod.ValueByUnit(U);

        if (Value > 1)
          return string.Format("{0} {1}", Value, FormatAdverb(U));
        else if (Value == 1)
          return FormatAdverb(U);
        else
          return string.Empty;
      }).AsNaturalAndSeparatedText();
    }
    /// <summary>
    /// Returns the time period as a <see cref="string"/> in an ISO 8601-like duration format.
    /// </summary>
    /// <remarks>
    /// <example>E.g.; "P5Y2M3W10DT15H30M15S500s" is a period of five years, two months, 3 weeks, 10 days, 15 hours, 30 minutes, 15 seconds and 500 milliseconds.</example>
    /// </remarks>
    /// <returns>A concise duration <see cref="string"/> not intended for presentation.</returns>
    public override string ToString()
    {
      var Result = new StringBuilder("P");

      if (Years != 0L)
        Result.Append(Years + "Y");

      if (Months != 0L)
        Result.Append(Months + "M");

      if (Weeks != 0L)
        Result.Append(Weeks + "W");

      if (Days != 0)
        Result.Append(Days + "D");

      if (Hours != 0 || Minutes != 0 || Seconds != 0 || Milliseconds != 0)
      {
        Result.Append("T");

        if (Hours != 0)
          Result.Append(Hours + "H");

        if (Minutes != 0)
          Result.Append(Minutes + "M");

        if (Seconds != 0)
          Result.Append(Seconds + "S");

        if (Milliseconds != 0)
          Result.Append(Milliseconds + "s");
      }

      return Result.ToString();
    }
    public override bool Equals(object other)
    {
      return EqualTo((TimePeriod)other);
    }
    public override int GetHashCode()
    {
      var Result = 17;

      unchecked
      {
        Result = (Result * 37) + Years.GetHashCode();
        Result = (Result * 37) + Months.GetHashCode();
        Result = (Result * 37) + Weeks.GetHashCode();
        Result = (Result * 37) + Days.GetHashCode();
        Result = (Result * 37) + Hours.GetHashCode();
        Result = (Result * 37) + Minutes.GetHashCode();
        Result = (Result * 37) + Seconds.GetHashCode();
        Result = (Result * 37) + Milliseconds.GetHashCode();
      }

      return Result;
    }
    public bool EqualTo(Inv.TimePeriod Value)
    {
      return Years == Value.Years &&
          Months == Value.Months &&
          Weeks == Value.Weeks &&
          Days == Value.Days &&
          Hours == Value.Hours &&
          Minutes == Value.Minutes &&
          Seconds == Value.Seconds &&
          Milliseconds == Value.Milliseconds;
    }
    public bool HasUnit(Inv.TimePeriodUnit Unit)
    {
      return Unit switch
      {
        Inv.TimePeriodUnit.Years => Years > 0,
        Inv.TimePeriodUnit.Months => Months > 0,
        Inv.TimePeriodUnit.Weeks => Weeks > 0,
        Inv.TimePeriodUnit.Days => Days > 0,
        Inv.TimePeriodUnit.Hours => Hours > 0,
        Inv.TimePeriodUnit.Minutes => Minutes > 0,
        Inv.TimePeriodUnit.Seconds => Seconds > 0,
        Inv.TimePeriodUnit.Milliseconds => Milliseconds > 0,
        _ => throw new Exception("Inv.TimePeriodUnit not handled: " + Unit),
      };
    }
    public bool HasOtherUnit(Inv.TimePeriodUnit Unit)
    {
      var ThisPeriod = this;

      return EnumHelper.GetEnumerable<TimePeriodUnit>().Except(Unit).Any(U => ThisPeriod.HasUnit(U));
    }
    public long ValueByUnit(Inv.TimePeriodUnit Unit)
    {
      return Unit switch
      {
        Inv.TimePeriodUnit.Years => Years,
        Inv.TimePeriodUnit.Months => Months,
        Inv.TimePeriodUnit.Weeks => Weeks,
        Inv.TimePeriodUnit.Days => Days,
        Inv.TimePeriodUnit.Hours => Hours,
        Inv.TimePeriodUnit.Minutes => Minutes,
        Inv.TimePeriodUnit.Seconds => Seconds,
        Inv.TimePeriodUnit.Milliseconds => (long)Milliseconds,
        _ => throw new Exception("Inv.TimePeriodUnit not handled: " + Unit),
      };
    }
    public Inv.Date NextAnniversaryDateInclusive(Inv.Date AnniversaryDate, Inv.Date RelativeDate)
    {
      var NextDate = AnniversaryDate;

      while (NextDate < RelativeDate)
        NextDate = NextDate.AddDatePeriod(this);

      return NextDate;
    }

    public static bool TryParse(string Text, out Inv.TimePeriod Period)
    {
      // TODO: this could be stricter in terms of duplicate letters and enforcing the letter order.

      var Result = true;
      var Years = 0;
      var Months = 0;
      var Weeks = 0;
      var Days = 0;
      var Hours = 0;
      var Minutes = 0;
      var Seconds = 0;
      var Milliseconds = 0;

      var CharEnumerator = Text.GetEnumerable().GetEnumerator();
      var Continue = CharEnumerator.MoveNext();

      if (Continue && CharEnumerator.Current == 'P')
      {
        Continue = CharEnumerator.MoveNext();

        while (Continue && CharEnumerator.Current != 'T')
        {
          if (!TryParseField(CharEnumerator, out int Number, out char Letter))
          {
            Result = false;
            break;
          }

          switch (Letter)
          {
            case 'Y':
              Years = Number;
              break;

            case 'M':
              Months = Number;
              break;

            case 'W':
              Weeks = Number;
              break;

            case 'D':
              Days = Number;
              break;

            default:
              Result = false;
              break;
          }

          Continue = CharEnumerator.MoveNext();
        }

        if (Result && Continue && CharEnumerator.Current == 'T')
        {
          while (CharEnumerator.MoveNext())
          {
            if (!TryParseField(CharEnumerator, out int Number, out char Letter))
            {
              Result = false;
              break;
            }

            switch (Letter)
            {
              case 'H':
                Hours = Number;
                break;

              case 'M':
                Minutes = Number;
                break;

              case 'S':
                Seconds = Number;
                break;

              case 's':
                Milliseconds = Number;
                break;

              default:
                Result = false;
                break;
            }
          }
        }
      }
      else
      {
        Result = false;
      }

      if (Result)
        Period = new Inv.TimePeriod(Years, Months, Weeks, Days, Hours, Minutes, Seconds, Milliseconds);
      else
        Period = Zero;

      return Result;
    }
    public static Inv.TimePeriod Parse(string Text)
    {
      if (!TryParse(Text, out var Result))
        throw new Exception("Invalid time period format: " + Text);

      return Result;
    }
    public static Inv.TimePeriod FromYears(int years)
    {
      return new Inv.TimePeriod(Inv.TimePeriodUnit.Years, years);
    }
    public static Inv.TimePeriod FromWeeks(int weeks)
    {
      return new Inv.TimePeriod(Inv.TimePeriodUnit.Weeks, weeks);
    }
    public static Inv.TimePeriod FromMonths(int months)
    {
      return new Inv.TimePeriod(Inv.TimePeriodUnit.Months, months);
    }
    public static Inv.TimePeriod FromDays(int days)
    {
      return new Inv.TimePeriod(Inv.TimePeriodUnit.Days, days);
    }
    public static Inv.TimePeriod FromHours(int hours)
    {
      return new Inv.TimePeriod(Inv.TimePeriodUnit.Hours, hours);
    }
    public static Inv.TimePeriod FromMinutes(int minutes)
    {
      return new Inv.TimePeriod(Inv.TimePeriodUnit.Minutes, minutes);
    }
    public static Inv.TimePeriod FromSeconds(int seconds)
    {
      return new Inv.TimePeriod(Inv.TimePeriodUnit.Seconds, seconds);
    }
    public static Inv.TimePeriod FromMilliseconds(int milliseconds)
    {
      return new Inv.TimePeriod(Inv.TimePeriodUnit.Milliseconds, milliseconds);
    }
    public static Inv.TimePeriod FromDateDifference(Inv.Date d1, Inv.Date d2)
    {
      return FromDateTimeDifference(d1.ToDateTime(), d2.ToDateTime());
    }
    public static Inv.TimePeriod FromDateTimeDifference(DateTime d1, DateTime d2)
    {
      var Negate = 1;

      if (d1 < d2)
      {
        var d3 = d2;
        d2 = d1;
        d1 = d3;
        Negate = -1;
      }

      var years = 0;
      var months = 0;
      var weeks = 0;
      var days = 0;
      var hours = 0;
      var minutes = 0;
      var seconds = 0;
      var milliseconds = 0;

      while (d1 > d2.AddYears(years))
        years++;

      d2 = d2.AddYears(years);

      if (d2 > d1)
      {
        d2 = d2.AddYears(-1);
        years--;
      }

      while (d1 > d2.AddMonths(months))
        months++;

      d2 = d2.AddMonths(months);

      if (d2 > d1)
      {
        d2 = d2.AddMonths(-1);
        months--;
      }

      while (d1 > d2)
      {
        d2 = d2.AddDays(7);
        weeks++;
      }

      if (d2 > d1)
      {
        d2 = d2.AddDays(-7);
        weeks--;
      }

      while (d1 > d2)
      {
        d2 = d2.AddDays(1);
        days++;
      }

      if (d2 > d1)
      {
        d2 = d2.AddDays(-1);
        days--;
      }

      while (d1 > d2)
      {
        d2 = d2.AddHours(1);
        hours++;
      }

      if (d2 > d1)
      {
        d2 = d2.AddHours(-1);
        hours--;
      }

      while (d1 > d2)
      {
        d2 = d2.AddMinutes(1);
        minutes++;
      }

      if (d2 > d1)
      {
        d2 = d2.AddMinutes(-1);
        minutes--;
      }

      while (d1 > d2)
      {
        d2 = d2.AddSeconds(1);
        seconds++;
      }

      if (d2 > d1)
      {
        d2 = d2.AddSeconds(-1);
        seconds--;
      }

      while (d1 > d2)
      {
        d2 = d2.AddMilliseconds(1);
        milliseconds++;
      }

      if (d2 > d1)
      {
        /*d2 =*/
        d2.AddMilliseconds(-1);
        milliseconds--;
      }

      return new Inv.TimePeriod(years * Negate, months * Negate, weeks * Negate, days * Negate, hours * Negate, minutes * Negate, seconds * Negate, milliseconds * Negate);
    }
    public static Inv.TimePeriod operator -(Inv.TimePeriod value)
    {
      return new Inv.TimePeriod(-value.Years, -value.Months, -value.Weeks, -value.Days, -value.Hours, -value.Minutes, -value.Seconds, -value.Milliseconds);
    }
    public static Inv.TimePeriod operator +(Inv.TimePeriod value)
    {
      return value;
    }
    public static Inv.TimePeriod operator +(Inv.TimePeriod left, Inv.TimePeriod right)
    {
      return new Inv.TimePeriod(left.Years + right.Years, left.Months + right.Months, left.Weeks + right.Weeks, left.Days + right.Days, left.Hours + right.Hours, left.Minutes + right.Minutes, left.Seconds + right.Seconds, left.Milliseconds + right.Milliseconds);
    }
    public static DateTimeOffset operator +(DateTimeOffset left, Inv.TimePeriod right)
    {
      return left.AddYears(right.Years).AddMonths(right.Months).AddDays((right.Weeks * 7) + right.Days) + new TimeSpan(0, right.Hours, right.Minutes, right.Seconds, right.Milliseconds);
    }
    public static DateTimeOffset operator -(DateTimeOffset left, Inv.TimePeriod right)
    {
      return left.AddYears(-right.Years).AddMonths(-right.Months).AddDays((-right.Weeks * 7) - right.Days) - new TimeSpan(0, right.Hours, right.Minutes, right.Seconds, right.Milliseconds);
    }
    public static DateTime operator +(DateTime left, Inv.TimePeriod right)
    {
      return left.AddYears(right.Years).AddMonths(right.Months).AddDays((right.Weeks * 7) + right.Days) + new TimeSpan(0, right.Hours, right.Minutes, right.Seconds, right.Milliseconds);
    }
    public static DateTime operator -(DateTime left, Inv.TimePeriod right)
    {
      return left.AddYears(-right.Years).AddMonths(-right.Months).AddDays((-right.Weeks * 7) - right.Days) - new TimeSpan(0, right.Hours, right.Minutes, right.Seconds, right.Milliseconds);
    }
    public static Inv.TimePeriod operator -(Inv.TimePeriod left, Inv.TimePeriod right)
    {
      return new Inv.TimePeriod(left.Years - right.Years, left.Months - right.Months, left.Weeks - right.Weeks, left.Days - right.Days, left.Hours - right.Hours, left.Minutes - right.Minutes, left.Seconds - right.Seconds, left.Milliseconds - right.Milliseconds);
    }
    public static Inv.TimePeriod operator /(Inv.TimePeriod left, int right)
    {
      return new Inv.TimePeriod(left.Years / right, left.Months / right, left.Weeks / right, left.Days / right, left.Hours / right, left.Minutes / right, left.Seconds / right, left.Milliseconds / right);
    }
    public static Inv.TimePeriod operator *(Inv.TimePeriod left, int right)
    {
      return new Inv.TimePeriod(left.Years * right, left.Months * right, left.Weeks * right, left.Days * right, left.Hours * right, left.Minutes * right, left.Seconds * right, left.Milliseconds * right);
    }
    public static bool operator ==(Inv.TimePeriod left, Inv.TimePeriod right)
    {
      return left.EqualTo(right);
    }
    public static bool operator !=(Inv.TimePeriod left, Inv.TimePeriod right)
    {
      return !left.EqualTo(right);
    }

    private static bool TryParseField(IEnumerator<char> CharEnumerator, out string NumberText, out char Letter)
    {
      var Result = true;

      var NumberValue = new StringBuilder();
      while (CharEnumerator.Current >= '0' && CharEnumerator.Current <= '9')
      {
        NumberValue.Append(CharEnumerator.Current);

        if (!CharEnumerator.MoveNext())
        {
          Result = false;
          break;
        }
      }

      // number not found.
      if (Result && NumberValue.Length == 0)
        Result = false;

      if (Result)
      {
        NumberText = NumberValue.ToString();
        Letter = CharEnumerator.Current; // character after the number.
      }
      else
      {
        NumberText = "";
        Letter = '\0';
      }

      return Result;
    }
    private static bool TryParseField(IEnumerator<char> CharEnumerator, out int Number, out char Letter)
    {
      var Result = TryParseField(CharEnumerator, out string NumberText, out Letter);

      if (Result)
        Number = int.Parse(NumberText);
      else
        Number = 0;

      return Result;
    }
    private static string FormatAdverb(TimePeriodUnit Unit)
    {
      return Unit switch
      {
        Inv.TimePeriodUnit.Years => "Yearly",
        Inv.TimePeriodUnit.Months => "Monthly",
        Inv.TimePeriodUnit.Weeks => "Weekly",
        Inv.TimePeriodUnit.Days => "Daily",
        Inv.TimePeriodUnit.Hours => "Hourly",
        Inv.TimePeriodUnit.Minutes => "Every minute",
        Inv.TimePeriodUnit.Seconds => "Every second",
        Inv.TimePeriodUnit.Milliseconds => "Every millisecond",
        _ => throw new Exception("Inv.TimePeriodUnit not handled: " + Unit),
      };
    }

    bool IEquatable<Inv.TimePeriod>.Equals(Inv.TimePeriod other)
    {
      return EqualTo(other);
    }

    private static XmlQualifiedName MySchema(object xs)
    {
      return new XmlQualifiedName("duration", "http://www.w3.org/2001/XMLSchema");
    }
    System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
    {
      return null; // NOTE: this is required to be null.
    }
    void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
    {
      var Result = Parse(reader.ReadInnerXml());

      this.Years = Result.Years;
      this.Months = Result.Months;
      this.Weeks = Result.Weeks;
      this.Days = Result.Days;
      this.Hours = Result.Hours;
      this.Minutes = Result.Minutes;
      this.Seconds = Result.Seconds;
      this.Milliseconds = Result.Milliseconds;
    }
    void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
    {
      // NOTE: not a nice workaround but the standard duration format does not have a 'W' or 's'.
      var TempWeeks = Weeks;
      var TempMilliseconds = Milliseconds;
      this.Weeks = 0;
      this.Days += TempWeeks * 7;
      this.Milliseconds = 0; // not supported in XML.
      try
      {
        writer.WriteRaw(ToString());
      }
      finally
      {
        this.Days -= TempWeeks * 7;
        this.Weeks = TempWeeks;
        this.Milliseconds = TempMilliseconds;
      }
    }
  }

  /// <summary>
  /// Logical unit of time periods.
  /// </summary>
  public enum TimePeriodUnit
  {
    Years,
    Months,
    Weeks,
    Days,
    Hours,
    Minutes,
    Seconds,
    Milliseconds
  }

  /// <summary>
  /// Represents a range of time spans.
  /// </summary>
  public sealed class TimeSpanRange
  {
    public TimeSpanRange(TimeSpan? Begin, TimeSpan? End)
    {
      this.Begin = Begin;
      this.End = End;
    }

    public TimeSpan? Begin { get; }
    public TimeSpan? End { get; }

    public int CompareTo(Inv.TimeSpanRange Value)
    {
      var Result = Begin.HasValue.CompareTo(Value.Begin.HasValue);

      if (Result == 0 && Begin.HasValue)
        Result = Begin.Value.CompareTo(Value.Begin.Value);

      if (Result == 0)
        Result = End.HasValue.CompareTo(Value.End.HasValue);

      if (Result == 0 && End.HasValue)
        Result = End.Value.CompareTo(Value.End.Value);

      return Result;
    }
    public bool EqualTo(Inv.TimeSpanRange Value)
    {
      return Begin.Equals(Value.Begin) && End.Equals(Value.End);
    }

    public override string ToString()
    {
      return Begin + " - " + End;
    }
  }

  /// <summary>
  /// Represents a range of date/times (System.DateTime).
  /// </summary>
  public sealed class DateTimeRange
  {
    public DateTimeRange(Inv.Date Date, Inv.Time From, Inv.Time Until)
      : this(Date + From, Date + Until)
    {
    }
    public DateTimeRange(DateTime? From, DateTime? Until)
    {
      this.From = From;
      this.Until = Until;
    }
    internal DateTimeRange(Range<DateTimeDiscrete> DiscreteRange)
      : this(DiscreteRange.From.Index, DiscreteRange.Until.Index)
    {
    }

    public DateTime? From { get; private set; }
    public DateTime? Until { get; private set; }

    public int CompareTo(Inv.DateTimeRange Value)
    {
      var Result = From.CompareTo(Value.From);

      if (Result == 0)
        Result = Until.CompareTo(Value.Until);

      return Result;
    }
    public bool EqualTo(Inv.DateTimeRange Value)
    {
      return From == Value.From && Until == Value.Until;
    }

    public override string ToString()
    {
      return ToString(false, true, null);
    }
    public string ToString(bool FormatLocalTime, bool IncludeTime, TimeSpanRange TimeSpanRange)
    {
      var StringBuilder = new StringBuilder();

      if (From == null && Until == null)
      {
        StringBuilder.Append("-");
      }
      else if (From == null)
      {
        var UntilDateTime = FormatLocalTime ? Until.Value.ToLocalTime() : Until.Value;

        StringBuilder.Append("Until ");

        if (IncludeTime && (TimeSpanRange == null || UntilDateTime.TimeOfDay != TimeSpanRange.End))
          StringBuilder.Append(UntilDateTime.ToString("h:mmtt").ToLower() + " ");

        StringBuilder.Append(UntilDateTime.ToString("ddd d MMM yyy"));
      }
      else if (Until == null)
      {
        var FromDateTime = FormatLocalTime ? From.Value.ToLocalTime() : From.Value;

        StringBuilder.Append("From ");

        if (IncludeTime && (TimeSpanRange == null || FromDateTime.TimeOfDay != TimeSpanRange.End))
          StringBuilder.Append(FromDateTime.ToString("h:mmtt").ToLower() + " ");

        StringBuilder.Append(FromDateTime.ToString("ddd d MMM yyy"));
      }
      else
      {
        var FromDateTime = FormatLocalTime ? From.Value.ToLocalTime() : From.Value;
        var UntilDateTime = FormatLocalTime ? Until.Value.ToLocalTime() : Until.Value;

        if (FromDateTime.Date == UntilDateTime.Date)
        {
          if (IncludeTime && ((TimeSpanRange == null || FromDateTime.TimeOfDay != TimeSpanRange.Begin || UntilDateTime.TimeOfDay != TimeSpanRange.End)))
            StringBuilder.Append(FromDateTime.ToString("h:mmtt").ToLower() + " - " + UntilDateTime.ToString("h:mmtt").ToLower() + " ");

          StringBuilder.Append(FromDateTime.ToString("ddd d MMM yyy"));
        }
        else
        {
          if (IncludeTime && (TimeSpanRange == null || FromDateTime.TimeOfDay != TimeSpanRange.Begin))
            StringBuilder.Append(FromDateTime.ToString("h:mmtt").ToLower() + " ");

          StringBuilder.Append(FromDateTime.ToString("ddd d "));

          if (FromDateTime.Month != UntilDateTime.Month)
            StringBuilder.Append(FromDateTime.ToString("MMM "));

          if (FromDateTime.Year != UntilDateTime.Year)
            StringBuilder.Append(FromDateTime.ToString("yyy "));

          StringBuilder.Append("- ");

          if (IncludeTime && (TimeSpanRange == null || UntilDateTime.TimeOfDay != TimeSpanRange.End))
            StringBuilder.Append(UntilDateTime.ToString("h:mmtt").ToLower() + " ");

          StringBuilder.Append(UntilDateTime.ToString("ddd d MMM yyy"));
        }
      }

      return StringBuilder.ToString();
    }

    public static bool TryParse(string Value, out DateTimeRange Range)
    {
      Debug.Fail("TODO: DateTimeRange is not yet implemented.");

      Range = new DateTimeRange(null, null);

      return false;
    }

    internal Range<DateTimeDiscrete> ToDiscreteRange() => new Range<DateTimeDiscrete>(DateTimeDiscrete.FromDateTime(From), DateTimeDiscrete.FromDateTime(Until));
  }

  internal struct DateTimeDiscrete : IComparable, IDiscrete
  {
    public DateTimeDiscrete(DateTime Value)
    {
      this.Base = Value;
    }

    public static implicit operator DateTime(DateTimeDiscrete Value) => Value.Base;

    int IComparable.CompareTo(object obj) => Base.CompareTo(((DateTimeDiscrete)obj).Base);
    object IDiscrete.PreviousValue() => new DateTimeDiscrete(Base.AddMinutes(-1));
    object IDiscrete.NextValue() => new DateTimeDiscrete(Base.AddMinutes(+1));

    internal static DateTimeDiscrete? FromDateTime(DateTime? Value) => Value != null ? new DateTimeDiscrete(Value.Value) : (DateTimeDiscrete?)null;

    private DateTime Base;
  }

  public sealed class DateTimeRangeSet : IEnumerable<Inv.DateTimeRange>
  {
    public DateTimeRangeSet()
    {
      this.Base = new RangeSet<DateTimeDiscrete>();
      Base.IntersectOnBoundary = false;
    }
    public DateTimeRangeSet(Inv.DateTimeRange DateTimeRange)
    {
      this.Base = new RangeSet<DateTimeDiscrete>(DateTimeRange.ToDiscreteRange());
      Base.IntersectOnBoundary = false;
    }
    public DateTimeRangeSet(DateTime? From, DateTime? Until)
      : this(new Inv.DateTimeRange(From, Until))
    {
    }
    public DateTimeRangeSet(DateTime Date)
      : this(Date, Date)
    {
    }

    public int Count => Base.Count;
    public Inv.DateTimeRange FirstRange => AsDateTimeRange(Base.FirstRange);
    public Inv.DateTimeRange LastRange => AsDateTimeRange(Base.LastRange);

    public override bool Equals(object obj)
    {
      var Source = obj as Inv.DateTimeRangeSet;

      if (Source != null)
        return Base.Equals(Source.Base);
      else
        return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return Base.GetHashCode();
    }
    public override string ToString()
    {
      return Base.ToString();
    }

    public bool IsEmpty()
    {
      return Base.IsEmpty();
    }
    public bool IsUniversal()
    {
      return Base.IsUniversal();
    }
    public bool IsEqualTo(Inv.DateTimeRangeSet CompareSet)
    {
      return Base.IsEqualTo(CompareSet.Base);
    }
    public bool Includes(DateTime Value)
    {
      return Base.Includes(Convert(Value));
    }
    public bool Intersects(Inv.DateTimeRange Range)
    {
      return Base.Intersects(Range.ToDiscreteRange());
    }
    public bool Intersects(DateTime? From, DateTime? Until)
    {
      return Base.Intersects(Convert(From), Convert(Until));
    }
    public Inv.DateTimeRangeSet Invert()
    {
      return new Inv.DateTimeRangeSet(Base.Invert());
    }
    public Inv.DateTimeRangeSet Union(DateTime? From, DateTime? Until)
    {
      return new Inv.DateTimeRangeSet(Base.Union(Convert(From), Convert(Until)));
    }
    public Inv.DateTimeRangeSet Union(Inv.DateTimeRange Range)
    {
      return new Inv.DateTimeRangeSet(Base.Union(Range.ToDiscreteRange()));
    }
    public Inv.DateTimeRangeSet Intersect(DateTime? From, DateTime? Until)
    {
      return new Inv.DateTimeRangeSet(Base.Intersect(Convert(From), Convert(Until)));
    }
    public Inv.DateTimeRangeSet Intersect(Inv.DateTimeRangeSet IntersectSet)
    {
      return new Inv.DateTimeRangeSet(Base.Intersect(IntersectSet.Base));
    }
    public Inv.DateTimeRangeSet Subtract(Inv.DateTimeRangeSet SubtractSet)
    {
      return new Inv.DateTimeRangeSet(Base.Subtract(SubtractSet.Base));
    }
    public Inv.DateTimeRangeSet Subtract(DateTime? From, DateTime? Until)
    {
      return new Inv.DateTimeRangeSet(Base.Subtract(Convert(From), Convert(Until)));
    }
    public Inv.DateTimeRangeSet Union(Inv.DateTimeRangeSet UnionSet)
    {
      return new Inv.DateTimeRangeSet(Base.Union(UnionSet.Base));
    }

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      throw new NotImplementedException();
    }
    System.Collections.Generic.IEnumerator<Inv.DateTimeRange> System.Collections.Generic.IEnumerable<Inv.DateTimeRange>.GetEnumerator()
    {
      return EnumerateRanges().GetEnumerator();
    }

    private DateTimeDiscrete Convert(DateTime Value) => new DateTimeDiscrete(Value);
    private DateTimeDiscrete? Convert(DateTime? Value) => Value != null ? new DateTimeDiscrete(Value.Value) : (DateTimeDiscrete?)null;
    private Inv.DateTimeRange AsDateTimeRange(Inv.Range<DateTimeDiscrete> Range)
    {
      return Range != null ? new Inv.DateTimeRange(Range) : null;
    }
    private IEnumerable<Inv.DateTimeRange> EnumerateRanges()
    {
      return Base.Select(R => new DateTimeRange(R));
    }

    public static DateTimeRangeSet FromDateTimeRanges(IEnumerable<Inv.DateTimeRange> DateTimeRanges)
    {
      var Result = Empty;

      foreach (var DateTimeRange in DateTimeRanges)
        Result = Result.Union(DateTimeRange);

      return Result;
    }
    public static DateTimeRangeSet FromDateTimeRangeArray(params Inv.DateTimeRange[] DateTimeRangeArray)
    {
      var Result = Empty;

      foreach (var DateTimeRange in DateTimeRangeArray)
        Result = Result.Union(DateTimeRange);

      return Result;
    }

    public static readonly DateTimeRangeSet Empty = new DateTimeRangeSet();
    public static readonly DateTimeRangeSet Universal = new DateTimeRangeSet(null, null);

    private DateTimeRangeSet(Inv.RangeSet<DateTimeDiscrete> Base)
    {
      this.Base = Base;
      Base.IntersectOnBoundary = false;
    }

    private readonly Inv.RangeSet<DateTimeDiscrete> Base;
  }

  /// <summary>
  /// Represents a range of timestamps (System.DateTimeOffset).
  /// </summary>
  public sealed class TimestampRange
  {
    public TimestampRange(Inv.Date Date, Inv.Time From, Inv.Time Until)
    {
      this.From = new DateTimeOffset(Date.ToDateTime() + From.ToTimeSpan());
      this.Until = new DateTimeOffset(Date.ToDateTime() + Until.ToTimeSpan());
    }
    public TimestampRange(DateTimeOffset? From, DateTimeOffset? Until)
    {
      this.From = From;
      this.Until = Until;
    }

    public DateTimeOffset? From { get; private set; }
    public DateTimeOffset? Until { get; private set; }

    public int CompareTo(Inv.TimestampRange Value)
    {
      var Result = From.CompareTo(Value.From);

      if (Result == 0)
        Result = Until.CompareTo(Value.Until);

      return Result;
    }
    public bool EqualTo(Inv.TimestampRange Value)
    {
      return From == Value.From && Until == Value.Until;
    }

    public override string ToString()
    {
      return ToString(false, true, false, null);
    }
    public string ToString(bool FormatLocalTime, bool IncludeTime, bool IncludeOffset, TimeSpanRange TimeSpanRange)
    {
      var StringBuilder = new StringBuilder();

      if (From == null && Until == null)
      {
        StringBuilder.Append("-");
      }
      else if (From == null)
      {
        var UntilDateTime = FormatLocalTime ? Until.Value.ToLocalTime() : Until.Value;

        StringBuilder.Append("Until ");

        if (IncludeTime && (TimeSpanRange == null || UntilDateTime.TimeOfDay != TimeSpanRange.End))
        {
          StringBuilder.Append(UntilDateTime.ToString("h:mmtt").ToLower() + " ");

          if (IncludeOffset && !FormatLocalTime)
            StringBuilder.Append(string.Format(" ({0:%z}) ", UntilDateTime));
        }

        StringBuilder.Append(UntilDateTime.ToString("ddd d MMM yyy"));
      }
      else if (Until == null)
      {
        var FromDateTime = FormatLocalTime ? From.Value.ToLocalTime() : From.Value;

        StringBuilder.Append("From ");

        if (IncludeTime && (TimeSpanRange == null || FromDateTime.TimeOfDay != TimeSpanRange.End))
        {
          StringBuilder.Append(FromDateTime.ToString("h:mmtt").ToLower() + " ");

          if (IncludeOffset && !FormatLocalTime)
            StringBuilder.Append(string.Format(" ({0:%z}) ", FromDateTime));
        }

        StringBuilder.Append(FromDateTime.ToString("ddd d MMM yyy"));
      }
      else
      {
        var FromDateTime = FormatLocalTime ? From.Value.ToLocalTime() : From.Value;
        var UntilDateTime = FormatLocalTime ? Until.Value.ToLocalTime() : Until.Value;

        if (FromDateTime.Date == UntilDateTime.Date)
        {
          if (IncludeTime && ((TimeSpanRange == null || FromDateTime.TimeOfDay != TimeSpanRange.Begin || UntilDateTime.TimeOfDay != TimeSpanRange.End)))
          {
            StringBuilder.Append(FromDateTime.ToString("h:mmtt").ToLower() + " - " + UntilDateTime.ToString("h:mmtt").ToLower() + " ");

            if (IncludeOffset && !FormatLocalTime)
              StringBuilder.Append(string.Format(" ({0:%z}) ", FromDateTime));
          }

          StringBuilder.Append(FromDateTime.ToString("ddd d MMM yyy"));
        }
        else
        {
          if (IncludeTime && (TimeSpanRange == null || FromDateTime.TimeOfDay != TimeSpanRange.Begin))
            StringBuilder.Append(FromDateTime.ToString("h:mmtt").ToLower() + " ");

          StringBuilder.Append(FromDateTime.ToString("ddd d "));

          if (FromDateTime.Month != UntilDateTime.Month)
            StringBuilder.Append(FromDateTime.ToString("MMM "));

          if (FromDateTime.Year != UntilDateTime.Year)
            StringBuilder.Append(FromDateTime.ToString("yyy "));

          StringBuilder.Append("- ");

          if (IncludeTime && (TimeSpanRange == null || UntilDateTime.TimeOfDay != TimeSpanRange.End))
          {
            StringBuilder.Append(UntilDateTime.ToString("h:mmtt").ToLower() + " ");

            if (IncludeOffset && !FormatLocalTime)
              StringBuilder.Append(string.Format(" ({0:%z}) ", FromDateTime));
          }

          StringBuilder.Append(UntilDateTime.ToString("ddd d MMM yyy"));
        }
      }

      return StringBuilder.ToString();
    }

    public static bool TryParse(string Value, out TimestampRange Range)
    {
      Debug.Fail("TODO: TimestampRange is not yet implemented.");

      Range = new TimestampRange(null, null);

      return false;
    }
  }
}