﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using Inv.Support;

namespace Inv
{
  [DataContract]
  public struct Xml : IComparable
  {
    public static Xml Empty()
    {
      return new Xml(""); 
    }
    public static Xml Hollow(long Length)
    {
      return new Xml(null, Length);
    }

    public Xml(string Text)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Text, nameof(Text));

      this.Text = Text;
      this.Length = Text.Length;
    }
    internal Xml(string Text, long Length)
    {
      this.Text = Text;
      this.Length = Length;
    }
    internal Xml(Xml Xml)
    {
      this.Text = Xml.Text;
      this.Length = Xml.Length;
    }

    public string GetText()
    {
      var Result = this.Text;

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Result != null, "Cannot use GetText() on a hollow Xml.");

      return Result;
    }
    public long GetLength()
    {
      return Length;
    }
    public Inv.DataSize GetSize()
    {
      return Inv.DataSize.FromBytes(Length);
    }
    public bool IsEmpty()
    {
      return Length == 0;
    }
    public bool IsHollow()
    {
      return Text == null;
    }
    public bool EqualTo(Xml Xml)
    {
      if (this.IsHollow() || Xml.IsHollow())
        return this.Length == Xml.Length;
      else
        return this.Text.Equals(Xml.Text, StringComparison.Ordinal);
    }
    public int CompareTo(Xml Xml)
    {
      if (this.IsHollow() || Xml.IsHollow())
        return this.Length.CompareTo(Xml.Length);
      else
        return this.Text.CompareTo(Xml.Text);
    }

    public override string ToString()
    {
      return "Xml[" + Length.ToString(CultureInfo.InvariantCulture) + "]";
    }
    public override bool Equals(object obj)
    {
      var Source = obj as Xml?;

      return Source != null && EqualTo(Source.Value);
    }
    public override int GetHashCode()
    {
      if (IsHollow())
        return base.GetHashCode();
      else
        return this.Text.GetHashCode();
    }

    int IComparable.CompareTo(object obj)
    {
      return CompareTo((Xml)obj);
    }

    [DataMember]
    private readonly string Text;
    [DataMember]
    private readonly long Length;
  }

  public sealed class XmlDocument
  {
    public XmlDocument()
    {
    }

    public XmlElement RootElement { get; private set; }

    public XmlElement AccessRoot(string LocalName)
    {
      var Result = RootElement;

      if (Result == null)
        throw new Exception("Expected root element was not installed.");

      if (Result.LocalName != LocalName)
        throw new Exception(string.Format("Expected root element to be named '{0}' but was named '{1}'.", LocalName, Result.LocalName));

      return Result;
    }
    public XmlElement AccessRoot(string LocalName, string Namespace)
    {
      var Result = AccessRoot(LocalName);

      if (Result.Namespace != Namespace)
        throw new Exception(string.Format("Expected root element to be namespaced '{0}' but was namespaced '{1}'.", Namespace, Result.Namespace));

      return Result;
    }
    public XmlElement InstallRoot(string LocalName, string Namespace)
    {
      return InstallRoot(Prefix: null, LocalName, Namespace);
    }
    public XmlElement InstallRoot(string Prefix, string LocalName, string Namespace)
    {
      this.RootElement = new XmlElement(Prefix, LocalName, Namespace);
      return RootElement;
    }
    public void WriteToStream(System.IO.Stream Stream, Encoding Encoding = null, bool OmitXMLDeclaration = false)
    {
      XmlDocumentStreamWriteEngine.Execute
      (
        XmlDocument: this,
        Stream: Stream,
        Encoding: Encoding,
        OmitXMLDeclaration: OmitXMLDeclaration
      );
    }
    public void ReadFromStream(System.IO.Stream Stream)
    {
      XmlDocumentStreamReadEngine.Execute
      (
        Stream: Stream,
        XmlDocument: this
      );
    }
    public string WriteToString()
    {
      XmlDocumentStringWriteEngine.Execute
      (
        XmlDocument: this,
        Text: out string Result
      );
      return Result;
    }
    public void ReadFromString(string Text)
    {
      using (var TextReader = new System.IO.StringReader(Text ?? ""))
      {
        XmlDocumentTextReadEngine.Execute
        (
          TextReader: TextReader,
          XmlDocument: this
        );
      }
    }
    public Inv.Binary WriteToBinary(Encoding Encoding = null, string Extension = ".xml")
    {
      using (var MemoryStream = new MemoryStream())
      {
        XmlDocumentStreamWriteEngine.Execute
        (
          XmlDocument: this,
          Stream: MemoryStream,
          Encoding: Encoding
        );

        return new Inv.Binary(MemoryStream.ToArray(), Extension);
      }
    }
    public void ReadFromBinary(Inv.Binary Binary)
    {
      using (var MemoryStream = new MemoryStream(Binary.GetBuffer()))
      {
        XmlDocumentStreamReadEngine.Execute
        (
          Stream: MemoryStream,
          XmlDocument: this
        );
      }
    }
  }

  internal enum XmlNodeType
  {
    Element,
    Content,
    Raw,
    Comment
  }

  internal sealed class XmlNode
  {
    internal static XmlNode AsElement(XmlElement Element) => new XmlNode(XmlNodeType.Element, Element, null);
    internal static XmlNode AsContent(string Content) => new XmlNode(XmlNodeType.Content, null, Content);
    internal static XmlNode AsRaw(string Content) => new XmlNode(XmlNodeType.Raw, null, Content);
    internal static XmlNode AsComment(string Comment) => new XmlNode(XmlNodeType.Comment, null, Comment);

    private XmlNode(XmlNodeType Type, XmlElement Element, string Content)
    {
      this.Type = Type;
      this.Element = Element;
      this.Content = Content;
    }

    public XmlNodeType Type { get; private set; }
    public XmlElement Element { get; private set; }
    public string Content { get; internal set; }

    public override string ToString()
    {
      switch (Type)
      {
        case XmlNodeType.Comment:
          return $"<!--{Content}-->";

        case XmlNodeType.Element:
          return Element.ToString();

        case XmlNodeType.Raw:
        case XmlNodeType.Content:
          return Content;

        default:
          throw Inv.Support.EnumHelper.UnexpectedValueException<XmlNodeType>(Type);
      }
    }
  }

  public sealed class XmlElement
  {
    internal XmlElement(string Prefix, string LocalName, string Namespace)
    {
      this.AttributeList = new Inv.DistinctList<XmlAttribute>();
      this.NodeList = new Inv.DistinctList<XmlNode>();

      Set(Prefix, LocalName, Namespace);
    }

    public string Prefix { get; private set; }
    public string LocalName { get; private set; }
    public string Namespace { get; private set; }
    public bool HasContent => NodeList.Count == 1 && NodeList[0].Type == XmlNodeType.Content;
    public string Content
    {
      get => HasContent ? NodeList[0].Content : null;
      set
      {
        if (value == null)
          NodeList.Clear();
        else if (NodeList.Count == 0)
          NodeList.Add(XmlNode.AsContent(value));
        else if (NodeList.Count > 1)
          throw new Exception("Cannot set content when there is more than one child node.");
        else if (NodeList[0].Type != XmlNodeType.Content)
          throw new Exception("Cannot set content when the child node is not of type content.");
        else
          NodeList[0].Content = value;
      }
    }

    public void ClearNodes()
    {
      NodeList.Clear();
    }
    public void AddRaw(string Text)
    {
      NodeList.Add(XmlNode.AsRaw(Text));
    }
    public void AddContent(string Text)
    {
      NodeList.Add(XmlNode.AsContent(Text));
    }
    public void AddComment(string Text)
    {
      NodeList.Add(XmlNode.AsComment(Text));
    }
    public XmlElement AddChildElement(string Prefix, string LocalName, string Namespace)
    {
      var Result = new XmlElement(Prefix, LocalName, Namespace);

      NodeList.Add(XmlNode.AsElement(Result));

      return Result;
    }
    public XmlElement AddChildElement(string LocalName)
    {
      return AddChildElement(null, LocalName, null);
    }
    public XmlElement InsertChildElement(int Index, string LocalName)
    {
      return InsertChildElement(Index, null, LocalName, null);
    }
    public XmlElement InsertChildElement(int Index, string Prefix, string LocalName, string Namespace)
    {
      var Result = new XmlElement(Prefix, LocalName, Namespace);

      NodeList.Insert(Index, XmlNode.AsElement(Result));

      return Result;
    }
    public void ImportAsChildElement(Inv.Xml ImportXml)
    {
      var ImportXmlDocument = new XmlDocument();

      ImportXmlDocument.ReadFromString(ImportXml.GetText());

      NodeList.Add(XmlNode.AsElement(ImportXmlDocument.RootElement));
    }
    public XmlAttribute AddAttribute(string Prefix, string LocalName, string Namespace, string Value)
    {
      var Result = new XmlAttribute(Prefix, LocalName, Namespace, Value);
      AttributeList.Add(Result);
      return Result;
    }
    public XmlAttribute AddAttribute(string LocalName, string Namespace, string Value)
    {
      return AddAttribute(Prefix: null, LocalName, Namespace, Value);
    }
    public XmlAttribute AddAttribute(string LocalName, string Value)
    {
      return AddAttribute(Prefix: null, LocalName, Namespace: null, Value);
    }
    public bool HasAttribute(string LocalName)
    {
      return AttributeList.Exists(Attribute => Attribute.LocalName == LocalName);
    }
    public string AccessAttribute(string LocalName)
    {
      var Result = AttributeList.Find(Attribute => Attribute.LocalName == LocalName);

      if (Result == null)
        throw new Exception("Unable to access attribute " + LocalName);

      return Result.Value;
    }
    public string AccessAttribute(string Prefix, string LocalName)
    {
      var Result = AttributeList.Find(Attribute => Attribute.Prefix == Prefix && Attribute.LocalName == LocalName);

      if (Result == null)
        throw new Exception("Unable to access attribute " + Prefix + ":" + LocalName);

      return Result.Value;
    }
    public void UpdateAttribute(string Prefix, string LocalName, string Value)
    {
      var Result = AttributeList.Find(Attribute => Attribute.Prefix == Prefix && Attribute.LocalName == LocalName);

      if (Result == null)
        throw new Exception("Unable to update attribute " + Prefix + ":" + LocalName);

      Result.SetValue(Value);
    }
    public void UpdateAttribute(string LocalName, string Value)
    {
      var Result = AttributeList.Find(Attribute => Attribute.LocalName == LocalName);

      if (Result == null)
        throw new Exception("Unable to update attribute " + LocalName);

      Result.SetValue(Value);
    }
    public bool HasAttributes()
    {
      return AttributeList.Count > 0;
    }
    public IEnumerable<XmlAttribute> GetAttributes()
    {
      return AttributeList;
    }
    public bool HasChildElements()
    {
      return NodeList.Any(N => N.Type == XmlNodeType.Element);
    }
    public IEnumerable<XmlElement> AccessChildElement(string LocalName)
    {
      return GetChildElements().Where(Element => Element.LocalName == LocalName);
    }
    public string AccessChildContent(string LocalName)
    {
      var XmlElement = AccessChildElement(LocalName).SingleOrDefault();

      if (XmlElement != null && XmlElement.HasContent)
        return XmlElement.Content;
      else
        return null;
    }
    public IEnumerable<XmlElement> GetChildElements()
    {
      foreach (var Node in NodeList)
      {
        if (Node.Type == XmlNodeType.Element)
          yield return Node.Element;
      }
    }

    public override string ToString()
    {
      string ElementType;
      if (Prefix == null)
        ElementType = $"{LocalName}";
      else
        ElementType = $"{Prefix}:{LocalName}";

      string ElementText;
      if (HasContent)
        ElementText = $">{Content}</{ElementType}";
      else if (NodeList.Count == 0)
        ElementText = " /";
      else
        ElementText = "";

      var AttributeText = AttributeList.Select(A => A.ToString()).AsSeparatedText(" ");

      if (Namespace == null)
        return $"<{ElementType}{AttributeText}{ElementText}>";
      else 
        return $"<{ElementType} xmlns=\"{Namespace}\"{AttributeText}{ElementText}>";
    }

    internal void Set(string Prefix, string LocalName, string Namespace)
    {
      Debug.Assert(Prefix != "", "Prefix must be null or a valid identifier.");
      Debug.Assert(LocalName != "", "LocalName must be null or a valid identifier.");
      Debug.Assert(Namespace != "", "Namespace must be null or a valid URL.");

      this.Prefix = Prefix;
      this.LocalName = LocalName;
      this.Namespace = Namespace;
    }
    internal IEnumerable<XmlNode> GetNodes() => NodeList;

    private readonly Inv.DistinctList<XmlAttribute> AttributeList;
    private readonly Inv.DistinctList<XmlNode> NodeList;
  }

  public sealed class XmlAttribute
  {
    internal XmlAttribute(string Prefix, string LocalName, string Namespace, string Value)
    {
      this.Prefix = Prefix;
      this.LocalName = LocalName;
      this.Namespace = Namespace;

      SetValue(Value);
    }

    public string Prefix { get; }
    public string LocalName { get; }
    public string Namespace { get; }
    public string Value { get; private set; }

    public override string ToString()
    {
      string AttributeName;
      if (Prefix == null)
        AttributeName = $"{LocalName}";
      else
        AttributeName = $"{Prefix}:{LocalName}";

      // TODO: what is namespace?

      return $"{AttributeName}={Value.ConvertToCSharpString()}";
    }

    internal void SetValue(string Value)
    {
      this.Value = Value;
    }
  }

  internal static class XmlElementWriterEngine
  {
    public static void Execute
    (
      System.Xml.XmlWriter XmlWriter,
      Inv.XmlElement XmlElement
    )
    {
      if (XmlElement.Namespace == null)
        XmlWriter.WriteStartElement(XmlElement.LocalName);
      else if (XmlElement.Prefix == null)
        XmlWriter.WriteStartElement(XmlElement.LocalName, XmlElement.Namespace);
      else
        XmlWriter.WriteStartElement(XmlElement.Prefix, XmlElement.LocalName, XmlElement.Namespace);

      foreach (var XmlAttribute in XmlElement.GetAttributes())
      {
        if (XmlAttribute.Prefix == null && XmlAttribute.Namespace == null)
          XmlWriter.WriteAttributeString(XmlAttribute.LocalName, XmlAttribute.Value);
        else if (XmlAttribute.Prefix == null && XmlAttribute.Namespace != null)
          XmlWriter.WriteAttributeString(XmlAttribute.LocalName, XmlAttribute.Namespace, XmlAttribute.Value);
        else
          XmlWriter.WriteAttributeString(XmlAttribute.Prefix, XmlAttribute.LocalName, XmlAttribute.Namespace, XmlAttribute.Value);
      }

      foreach (var Node in XmlElement.GetNodes())
      {
        switch (Node.Type)
        {
          case Inv.XmlNodeType.Element:
            Inv.XmlElementWriterEngine.Execute
            (
              XmlWriter: XmlWriter,
              XmlElement: Node.Element
            );
            break;

          case Inv.XmlNodeType.Raw:
            XmlWriter.WriteRaw(Node.Content);
            break;

          case Inv.XmlNodeType.Content:
            XmlWriter.WriteString(Node.Content);
            break;

          case Inv.XmlNodeType.Comment:
            XmlWriter.WriteComment(Node.Content);
            break;

          default:
            throw new Exception("Inv.XmlNodeType not handled.");
        }
      }

      XmlWriter.WriteEndElement();
    }
  }

  internal static class XmlElementReaderEngine
  {
    public static void Execute
    (
      System.Xml.XmlReader XmlReader,
      Inv.XmlElement XmlElement
    )
    {
      if (XmlReader.NodeType != System.Xml.XmlNodeType.Element)
        throw new Exception("Xml must be an element.");

      XmlElement.Set(XmlReader.Prefix.EmptyAsNull(), XmlReader.LocalName, XmlReader.NamespaceURI.EmptyAsNull());

      if (XmlReader.MoveToFirstAttribute())
      {
        do
        {
          var LocalName = XmlReader.Name;
          var NameArray = LocalName.Split(new [] { ':' }, 2);
          
          LocalName = NameArray.Length == 2 ? NameArray[1] : LocalName;
          var PrefixName = NameArray.Length == 2 ? NameArray[0] : null;
          
          // TODO: what about namespace?
          XmlElement.AddAttribute(PrefixName, LocalName, null, XmlReader.Value);
        }
        while (XmlReader.MoveToNextAttribute());

        XmlReader.MoveToElement();
      }

      if (XmlReader.IsEmptyElement)
      {
        XmlReader.Read();
      }
      else
      {
        XmlReader.ReadStartElement();

        while (XmlReader.NodeType != System.Xml.XmlNodeType.EndElement)
        {
          switch (XmlReader.NodeType)
          {
            case System.Xml.XmlNodeType.Element:
              Inv.XmlElementReaderEngine.Execute
              (
                XmlReader: XmlReader,
                XmlElement: XmlElement.AddChildElement(Prefix: null, LocalName: null, Namespace: null)
              );
              break;

            case System.Xml.XmlNodeType.Text:
              XmlElement.AddContent(XmlReader.ReadContentAsString());
              break;

            case System.Xml.XmlNodeType.Comment:
              XmlElement.AddComment(XmlReader.Value);
              XmlReader.Read();
              break;

            default:
              throw new Exception("XmlNodeType not handled.");
          }
        }

        XmlReader.ReadEndElement();
      }
    }
  }

  internal static class XmlElementStreamWriteEngine
  {
    public static void Execute
    (
      Stream Stream,
      Inv.XmlElement XmlElement,
      System.Text.Encoding Encoding = null,
      bool OmitXMLDeclaration = false
    )
    {
      using (var XmlWriter = System.Xml.XmlWriter.Create(Stream, Encoding != null ? new System.Xml.XmlWriterSettings() { Indent = true, Encoding = Encoding, OmitXmlDeclaration = OmitXMLDeclaration } : new System.Xml.XmlWriterSettings() { Indent = true, OmitXmlDeclaration = OmitXMLDeclaration }))
      {
        Inv.XmlElementWriterEngine.Execute
        (
          XmlWriter: XmlWriter,
          XmlElement: XmlElement
        );
      }
    }
  }

  internal static class XmlDocumentStreamWriteEngine
  {
    public static void Execute
    (
      Stream Stream,
      Inv.XmlDocument XmlDocument,
      System.Text.Encoding Encoding = null,
      bool OmitXMLDeclaration = false
    )
    {
      using (var XmlWriter = System.Xml.XmlWriter.Create(Stream, Encoding != null ? new System.Xml.XmlWriterSettings() { Indent = true, Encoding = Encoding, OmitXmlDeclaration = OmitXMLDeclaration } : new System.Xml.XmlWriterSettings() { Indent = true, OmitXmlDeclaration = OmitXMLDeclaration }))
      {
        XmlWriter.WriteStartDocument();

        Inv.XmlElementWriterEngine.Execute
        (
          XmlWriter: XmlWriter,
          XmlElement: XmlDocument.RootElement
        );

        XmlWriter.WriteEndDocument();
      }
    }
  }

  internal static class XmlDocumentStringReadEngine
  {
    public static void Execute
    (
      string Text,
      Inv.XmlDocument XmlDocument
    )
    {
      using (var StringReader = new StringReader(Text))
      {
        Inv.XmlDocumentTextReadEngine.Execute
        (
          TextReader: StringReader,
          XmlDocument: XmlDocument
        );
      }
    }
  }

  internal static class XmlDocumentStringWriteEngine
  {
    public static void Execute
    (
      Inv.XmlDocument XmlDocument,
      out string Text
    )
    {
      var StringBuilder = new StringBuilder();

      using (var XmlWriter = System.Xml.XmlWriter.Create(StringBuilder, new System.Xml.XmlWriterSettings() { Indent = true }))
      {
        XmlWriter.WriteStartDocument();

        Inv.XmlElementWriterEngine.Execute
        (
          XmlWriter: XmlWriter,
          XmlElement: XmlDocument.RootElement
        );

        XmlWriter.WriteEndDocument();
      }

      Text = StringBuilder.ToString();
    }
  }

  internal static class XmlDocumentStreamReadEngine
  {
    public static void Execute
    (
      Stream Stream,
      Inv.XmlDocument XmlDocument
    )
    {
      using (var StreamReader = new StreamReader(Stream))
      {
        Inv.XmlDocumentTextReadEngine.Execute
        (
          TextReader: StreamReader,
          XmlDocument: XmlDocument
        );
      }
    }
  }

  internal static class XmlDocumentTextReadEngine
  {
    public static void Execute
    (
      TextReader TextReader,
      Inv.XmlDocument XmlDocument
    )
    {
      using (var XmlReader = System.Xml.XmlReader.Create(TextReader, new System.Xml.XmlReaderSettings() { IgnoreWhitespace = true, DtdProcessing = System.Xml.DtdProcessing.Ignore }))
      {
        var RootElement = XmlDocument.InstallRoot(null, null, null);

        XmlReader.Read();

        if (XmlReader.NodeType == System.Xml.XmlNodeType.XmlDeclaration)
        {
          //throw new Exception("Xml document must have a declaration node type.");

          XmlReader.Read();
        }

        Inv.XmlElementReaderEngine.Execute
        (
          XmlReader: XmlReader,
          XmlElement: RootElement
        );
      }
    }
  }
}
