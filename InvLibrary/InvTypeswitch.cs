﻿  using System;
  using System.Collections.Generic;
  using System.Diagnostics;
  using System.Linq;
  using System.Reflection;
  using System.Text;
  using Inv.Support;

namespace Inv
{
  public sealed class Typeswitch<TContext, TBase>
  {
    public Typeswitch()
    {
      this.ContextType = typeof(TContext);
      this.BaseType = typeof(TBase);
      this.AddMethod = this.GetType().GetReflectionMethod("Add");
      this.ActionDictionary = new Dictionary<Type, Action<TContext, object>>();
    }

    public void Compile(object Owner)
    {
      var OwnerType = Owner.GetType().GetReflectionInfo();
      var MethodInfoArray = OwnerType.GetReflectionMethods().Where(M => !M.IsPublic && !M.IsStatic).ToArray();

      foreach (var MethodInfo in MethodInfoArray)
      {
        var ParameterInfoArray = MethodInfo.GetParameters();

        if (ParameterInfoArray.Length == 2 && ParameterInfoArray[0].ParameterType == ContextType && ParameterInfoArray[1].ParameterType.GetReflectionInfo().IsSubclassOf(BaseType) && !ParameterInfoArray[1].ParameterType.GetReflectionInfo().IsAbstract)
        {
          var RecordType = ParameterInfoArray[1].ParameterType;

          Debug.Assert(MethodInfo.Name == RecordType.Name.ExcludeAfter(BaseType.Name), $"{ContextType.Name} method name {MethodInfo.Name} does not match the record type name {RecordType.Name}.");

          var MethodDelegate = MethodInfo.CreateDelegate(typeof(Action<,>).MakeGenericType(ContextType, RecordType), Owner);
          AddMethod.MakeGenericMethod(RecordType).Invoke(this, new object[] { MethodDelegate });
        }
      }
    }
    public void Validate()
    {
      var SubtypeArray = BaseType.GetReflectionInfo().Assembly.GetReflectionTypes().Where(T => !T.GetReflectionInfo().IsAbstract && T.GetReflectionInfo().IsSubclassOf(BaseType)).ToArray();

      var MissingTypeArray = SubtypeArray.Except(ActionDictionary.Keys).ToArray();
      if (MissingTypeArray.Length > 0)
        throw new Exception("Compiled route must declare methods for missing subtypes:\n" + MissingTypeArray.Select(T => T.FullName).AsLineSeparatedText());
    }
    public void Handle(Type RecordType, Action<TContext, TBase> Action)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(RecordType.GetReflectionInfo().IsSubclassOf(BaseType), $"Handled type {RecordType} must inherit directly from {BaseType}.");

      ActionDictionary.Add(RecordType, (C, R) => Action(C, (TBase)R));
    }
    public bool IsHandled(Type RecordType)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(RecordType.GetReflectionInfo().IsSubclassOf(BaseType), $"Handled type {RecordType} must inherit directly from {BaseType}.");

      return ActionDictionary.ContainsKey(RecordType);
    }
    public void Execute(TContext Context, TBase Record)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Record, nameof(Record));

      if (Record != null)
      {
        var RegisterType = Record.GetType();
        var Action = ActionDictionary.GetValueOrNull(RegisterType);

        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(Action != null, $"Type not registered: {RegisterType.FullName}");

        Action?.Invoke(Context, Record);
      }
    }

    // used by reflection.
#pragma warning disable IDE0051 // Remove unused private members
    private void Add<TSpecific>(Action<TContext, TSpecific> Action)
      where TSpecific : TBase
    {
      ActionDictionary.Add(typeof(TSpecific), (C, R) => Action(C, (TSpecific)R));
    }
#pragma warning restore IDE0051 // Remove unused private members

    private readonly Dictionary<Type, Action<TContext, object>> ActionDictionary;
    private readonly Type ContextType;
    private readonly Type BaseType;
    private readonly MethodInfo AddMethod;
  }
}
  