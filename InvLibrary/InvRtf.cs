﻿#if DEBUG
//#define READ_TRACE
#endif

using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  public sealed class RtfWriter 
  {
    public RtfWriter(Stream stream)
    {
      this.StreamWriter = new StreamWriter(stream, Encoding.GetEncoding("ASCII"));
    }

    public void StartGroup()
    {
      StreamWriter.Write('{');
      lineCount++;
      wasControl = true;
    }
    public void CloseGroup()
    {
      wasControl = false;
      StreamWriter.Write('}');
      lineCount++;
    }
    public void Break(string word)
    {
      wasControl = true;
      StreamWriter.Write(word);
      lineCount += word.Length;
    }
    public void Control(string word)
    {
      wasControl = true;
      StreamWriter.Write('\\' + word);
      lineCount += word.Length + 1;
    }
    public void ControlBoolean(string word, bool on)
    {
      if (on)
        Control(word);
      else
        Control(word + '0');
    }
    public void Text(string text)
    {
      if (!string.IsNullOrEmpty(text))
      {
        if (wasControl)
        {
          StreamWriter.Write(' ');
          lineCount++;
        }

        foreach (var c in text)
        {
          if (c == '\\' || c == '{' || c == '}')
          {
            StreamWriter.Write('\\' + c);
            lineCount += 2;
          }
          else if (c > 'ÿ')
          {
            StreamWriter.Write("\\u" + ((int)c).ToString() + "?");
            lineCount += 3 + ((int)c).ToString().Length;
          }
          else
          {
            StreamWriter.Write(c);
            lineCount++;
          }
        }

        wasControl = false;
      }
    }
    public void Write(byte[] c)
    {
      var p = new string(Encoding.UTF8.GetChars(c));
      StreamWriter.Write(p);
    }
    public void Close()
    {
      StreamWriter.Flush();
      StreamWriter.Dispose();
    }

    private bool wasControl;
    private int lineCount;
    private readonly StreamWriter StreamWriter;
  }

  public sealed class RtfReader : StreamReader
  {
    public RtfReader(Stream Stream)
      : base(Stream, Encoding.GetEncoding("ASCII"))
    {
      Control = null;
      Cache = string.Empty;
    }

    public void ConsumeHeader()
    {
      ConsumeGroupOpen();
      ConsumeControl("rtf", "rtf1");
    }
    public bool PeekIsGroupOpen()
    {
      return PeekCharacter() == '{';
    }
    public bool PeekIsGroupClose()
    {
      return PeekCharacter() == '}';
    }
    public void ConsumeGroupOpen()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(PeekCharacter() == '{', "ConsumeGroupStart, Found \"{0}\" consuming {{", PeekCharacter());
      ConsumeCharacter();

      Trace("group open");
    }
    public void ConsumeGroupClose()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(PeekCharacter() == '}', $"ConsumeGroupClose, Found \"{PeekCharacter()}\" consuming }}");
      ConsumeCharacter();
      ConsumeVerticalSpace();

      Trace("group close");
    }
    public bool PeekIsControl()
    {
      return PeekCharacter() == '\\' && PeekCharacter(1) != '\\';
    }
    public string PeekControl()
    {
      var Result = ReadControl();
      this.Control = Result;
      return Result;
    }
    public string ConsumeControl()
    {
      var Result = ReadControl();
      Trace("control", Result);
      return Result;
    }
    public void ConsumeControl(params string[] ValueArray)
    {
      Debug.Assert(ValueArray != null && ValueArray.Length > 0, "Expected value(s) must be provided");

      var Control = ConsumeControl();
      if (!ValueArray.Any(V => string.Equals(Control, V, StringComparison.InvariantCultureIgnoreCase)))
        throw new Exception($"Found [{Control}] trying to read [{ValueArray.AsNaturalOrSeparatedText()}])");
    }
    public bool PeekIsNumericControl(string name)
    {
      var val = PeekControl();
      return val.Length >= name.Length && val.Substring(0, name.Length) == name && val.Length > name.Length && int.TryParse(val.Substring(name.Length), out var _);
    }
    public int ConsumeNumericControl(string name)
    {
      var val = ConsumeControl();
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(val.Length >= name.Length && val.Substring(0, name.Length) == name && val.Length > name.Length && int.TryParse(val.Substring(name.Length), out var _), "ConsumeNumericControl, Found '{0}' trying to read '{1}'.", val, name);
      return Int32.Parse(val.Substring(name.Length));
    }
    public bool PeekIsBooleanControl(string name)
    {
      var val = PeekControl();
      return val.Length >= name.Length && val.Substring(0, name.Length) == name && (val.Length == name.Length || val.Substring(name.Length) == "0");
    }
    public bool ConsumeBooleanControl(string name)
    {
      var val = ConsumeControl();
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(val.Length >= name.Length && val.Substring(0, name.Length) == name && (val.Length == name.Length || val.Substring(name.Length) == "0"), "ConsumeNumericControl, Found '{0}' trying to read '{1}'.", val, name);
      return val.Length == name.Length;
    }
    public bool PeekIsBreak() => PeekCharacter() == ';';
    public void ConsumeBreak()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(PeekIsBreak(), $"ConsumeGroupClose, Found \"{PeekCharacter()}\" consuming ;");

      ConsumeCharacter();
      
      ConsumeVerticalSpace();

      Trace("break");
    }
    public bool PeekIsText()
    {
      return !(PeekIsBreak() || PeekIsControl() || CharInSet(PeekCharacter(), new[] { '{', '}' }));
    }
    public string ConsumeText()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(PeekIsText(), "ConsumeText, not at text consuming text");

      var Text = ConsumeUntilCharacterSet(new char[] { '\\', '{', '}', '\r', '\n' });
      while (PeekCharacter(out var Value) && Value == '\\' && PeekCharacter(1, out Value) && Value == '\\')
      {
        ConsumeCharacterCount(2);
        Text += "\\" + ConsumeUntilCharacterSet(new char[] {'\\', '{', '}', '\r', '\n'});
      }

      EatEoln();

      Trace("text", Text);

      return Text;
    }
    public bool PeekIsUnicodeChar()
    {
      return TryParseUnicodeChar(PeekControl(), out var _);
    }
    public char ReadUnicodeChar()
    {
      var Control = ConsumeControl();

      if (!TryParseUnicodeChar(Control, out var Text))
        throw new Exception("Unable to parse expected unicode character");

      if (PeekIsControl())
      {
        var Hex = PeekControl();
        if (Hex.Length > 0 && Hex[0] == '\'')
          ConsumeControl(); // ignore the legacy hex codes, because we can understand Unicode.
      }

      return Text.IsNumeric() ? (char)Convert.ToUInt32(Text) : '?';
    }
    public void ConsumeOptionalSpace()
    {
      while (PeekCharacter() == ' ')
        ConsumeCharacter();
    }
    public void ConsumeOptionalBreak()
    {
      if (PeekIsBreak())
        ConsumeBreak();
    }

    private bool PeekCharacter(out char? Value)
    {
      return PeekCharacter(0, out Value);
    }
    private bool PeekCharacter(int Offset, out char? Value)
    {
      while (Cache.Length <= Offset)
      {
        if (EndOfStream)
          break;

        Cache += (char)Read();
      }

      if (Offset >= Cache.Length)
      {
        Value = null;
        return false;
      }

      Value = Cache[Offset];
      return true;
    }
    private char PeekCharacter(int Offset = 0)
    {
      if (!PeekCharacter(Offset, out var Value))
        throw new Exception("Attempted to peek past end of stream");

      return Value.Value;
    }
    private char ConsumeCharacter()
    {
      if (Cache.Length == 0)
        return (char)Read();

      var Result = Cache[0];
      Cache = Cache.Length == 1 ? string.Empty : Cache.Substring(1);
      return Result;
    }
    private string ReadControl()
    {
      if (Control != null)
      {
        var Result = Control;
        this.Control = null;
        return Result;
      }
      else
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(PeekCharacter() == '\\', "ReadControl, Found \"{0}\" consuming control (\\)", PeekCharacter());

        ConsumeCharacter(); // '\'

        var Character = ConsumeCharacter();
        var Result = string.Empty + Character;

        // work around an apparent Word Bug: \~ does not have a following space
        
        if (Character != '~')
        {
          if (Character == '\'')
          {
            Result += ConsumeCharacterCount(2);
          }
          else if (Character == '\\' || Character == '{' || Character == '}')
          {
            // do nothing as we've encountered a special character
          }
          else
          {
            Result += ConsumeUntilCharacterSet(new char[] { ' ', '\\', '{', '}', ';', '\r', '\n', '?' });

            // for unicode control codes.
            if (PeekCharacter() == '?')
              Result += ConsumeCharacter();
            else if (PeekCharacter() == ' ')
              ConsumeCharacter();

            ConsumeVerticalSpace();
          }
        }

        EatEoln();

        return Result;
      }
    }
    private void ConsumeVerticalSpace()
    {
      while (!EndOfStream && CharInSet(PeekCharacter(), VerticalCharSet))
        ConsumeCharacter();
    }
    private void EatEoln()
    {
      while (!EndOfStream && CharInSet(PeekCharacter(), VerticalCharSet))
        ConsumeCharacter();
    }
    private string ConsumeUntilCharacterSet(char[] tokens)
    {
      var b = new StringBuilder();
      while (!EndOfStream && !CharInSet(PeekCharacter(), tokens))
        b.Append(ConsumeCharacter());
      return b.ToString();
    }
    private string ConsumeCharacterCount(int count)
    {
      var b = new StringBuilder();
      for (var i = 0; i < count; i++)
        b.Append(ConsumeCharacter());
      return b.ToString();
    }
    private bool CharInSet(char p, char[] set)
    {
      var res = false;
      foreach (var c in set)
        res = res || c == p;
      return res;
    }
    private bool TryParseUnicodeChar(string Text, out string Value)
    {
      if (Text.Length == 0 || Text[0] != 'u')
      {
        Value = null;
        return false;
      }

      var TrimEndCount = Text[Text.Length - 1] == '?' ? 1 : 0;

      Value = Text.Substring(1, Text.Length - 1 - TrimEndCount);
      return int.TryParse(Value, out _);
    }
    [Conditional("READ_TRACE")]
    private void Trace(string Type, string Value = null)
    {
      Debug.WriteLine($"{Type}{(string.IsNullOrEmpty(Value) ? string.Empty : $" {Value}")}");
    }

    private string Control;
    private string Cache;

    private static char[] VerticalCharSet = new char[] { '\r', '\n', '\t' };
  }
}