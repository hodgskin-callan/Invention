﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using System.Text;
using Inv.Support;
using System.Threading;

[assembly:Inv.TraceInstall]

namespace Inv
{
  [AttributeUsage(validOn: AttributeTargets.Assembly)]
  public sealed class TraceInstallAttribute : global::System.Attribute
  {
  }

  [AttributeUsage(validOn: AttributeTargets.Class)]
  public sealed class TraceRecruitAttribute : global::System.Attribute 
  {
  }

  [Inv.TraceRecruit]
  public static class Trace
  {
    static Trace()
    {
      IsRunning = false;
    }

    public static bool IsRunning { get; private set; }
    public static readonly Inv.TraceFeature PlatformFeature = NewFeature("Invention Platform");

    public static void Start(Action<string> WriteDelegate)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!IsRunning, "Attempting to start diagnostic log that is already running");

      WriteAction = WriteDelegate;
      IsRunning = true;
      BypassFeatureCheck = !IsManuallyEnabled && RecruitList == null;

      WriteLine("STARTED");
    }
    public static void Stop()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IsRunning, "Attempting to stop diagnostic log that is not running");

      WriteLine("STOPPED");

      WriteAction = null;
      IsRunning = false;
      BypassFeatureCheck = false;
    }

    public static IEnumerable<Inv.TraceFeature> GetFeatures()
    {
      Recruit();
      return RecruitList;
    }
    public static TraceFeature NewFeature(string Name)
    {
      return new TraceFeature(Name);
    }
    public static TraceClass NewClass(string Name, TraceFeature Feature, params TraceFeature[] FeatureArray)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Feature, nameof(Feature));

      var FeatureSet = new HashSet<TraceFeature>();
      FeatureSet.Add(Feature);
      if (FeatureArray != null)
        FeatureSet.AddRange(FeatureArray);

      return new TraceClass(Name, FeatureSet.ToArray());
    }
    public static TraceClass NewClass<T>(TraceFeature Feature, params TraceFeature[] FeatureArray)
    {
      string GetName(Type Type)
      {
        if (Type.GenericTypeArguments.Length == 0)
          return Type.FullName;

        return $"{Type.Namespace}.{Type.Name}<{Type.GenericTypeArguments.Select(T => GetName(T)).AsSeparatedText(", ")}>";
      }

      return NewClass(GetName(typeof(T)), Feature, FeatureArray);
    }
    public static void Enter(string Text = null, [CallerMemberName] string CallerMemberName = "", [CallerFilePath] string CallerFilePath = "", [CallerLineNumber] int CallerLineNumber = 0)
    {
      if (IsRunning)
      {
        Write($"ENTER {Format(Text, CallerMemberName)}", string.Empty, CallerFilePath, CallerLineNumber);
        Indent.Value += new string(' ', 2);
      }
    }
    public static void Leave(string Text = null, [CallerMemberName] string CallerMemberName = "", [CallerFilePath] string CallerFilePath = "", [CallerLineNumber] int CallerLineNumber = 0)
    {
      if (IsRunning)
      {
        var CurrentIndent = Indent.Value;
        Indent.Value = CurrentIndent.Length > 2 ? CurrentIndent.Substring(0, CurrentIndent.Length - 2) : string.Empty;

        Write($"LEAVE {Format(Text, CallerMemberName)}", string.Empty, CallerFilePath, CallerLineNumber);
      }
    }
    public static void Write(string Text, [CallerMemberName] string CallerMemberName = "", [CallerFilePath] string CallerFilePath = "", [CallerLineNumber] int CallerLineNumber = 0)
    {
      if (IsRunning)
      {
        var Line = $"{System.Threading.Thread.CurrentThread.ManagedThreadId} {Indent.Value}{Format(Text, CallerMemberName)} (in {CallerFilePath}:line {CallerLineNumber})";

        WriteLine(Line);
      }
    }

    internal static bool IsManuallyEnabled { get; set; }

    internal static bool IsActive(Inv.TraceFeature[] FeatureArray)
    {
      if (!IsRunning)
        return false;

      if (BypassFeatureCheck)
        return true;

      foreach (var Feature in FeatureArray)
      {
        if (Feature.IsEnabled)
          return true;
      }

      return false;
    }

    private static string Format(string Text, string CallerMemberName) => $"{CallerMemberName.NullAsEmpty()}{(string.IsNullOrEmpty(CallerMemberName) || string.IsNullOrEmpty(Text) ? string.Empty : " ")}{Text.NullAsEmpty()}";
    private static void Recruit()
    {
      if (Inv.Trace.RecruitList == null)
      {
        var CandidateList = new Inv.DistinctList<TraceFeature>();

        var RecruitTypeList = new Inv.DistinctList<Type>();
        foreach (var Assembly in AppDomain.CurrentDomain.GetAssemblies().Where(Index => Index.GetCustomAttributes(typeof(TraceInstallAttribute), false).Length > 0))
          RecruitTypeList.AddRange(Assembly.GetTypes().Where(Item => Item.IsSealed && Item.GetCustomAttributes(typeof(TraceRecruitAttribute), false).Length > 0));

        var FeatureType = typeof(Inv.TraceFeature);

        foreach (var RecruitType in RecruitTypeList.OrderBy(T => T.Name))
        {
          foreach (var FeatureField in RecruitType.GetFields(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public).Where(P => P.FieldType == FeatureType))
          {
            try
            {
              var Feature = FeatureField.GetValue(null) as Inv.TraceFeature;

              if (Feature != null)
                CandidateList.Add(Feature);
            }
            catch (Exception E)
            {
              throw new System.Exception($"Failed to retrieve feature {RecruitType.FullName}.{FeatureField.Name}", E);
            }
          }
        }

        if (BypassFeatureCheck)
        {
          // If this is true, then all of the features are implicitly enabled.

          CandidateList.ForEach(F => F.IsEnabled = true);

          BypassFeatureCheck = false;
        }

        Inv.Trace.RecruitList = CandidateList;

        if (DebugHelper.IsDebugMode())
        {
          var DuplicateGroupArray = RecruitList.GroupBy(R => R.Name, StringComparer.InvariantCultureIgnoreCase).Where(G => G.Count() > 1).ToArray();
          if (DuplicateGroupArray.Length > 0)
            throw new Exception($"Duplicate feature names: {DuplicateGroupArray.Select(G => $"'{G.Key}'").AsSeparatedText(", ")}");
        }
      }
    }
    private static void WriteLine(string Text)
    {
      if (IsRunning)
      {
        System.Diagnostics.Trace.WriteLine(Text);
        WriteAction?.Invoke(Text);
      }
    }

    private static Inv.DistinctList<TraceFeature> RecruitList;
    private static Action<string> WriteAction;
    private static ThreadLocal<string> Indent = new ThreadLocal<string>(() => string.Empty);
    /// This is to allow the logging to be started potentially before all of the Inv.TraceInstall assemblies have been loaded.
    private static bool BypassFeatureCheck;
  }

  public sealed class TraceFeature
  {
    internal TraceFeature(string Name)
    {
      this.Name = Name;
    }

    public string Name { get; }
    public bool IsEnabled { get; set; }

    /// <summary>
    /// Enable this feature for use at compile time.
    /// </summary>
    /// <returns></returns>
    public TraceFeature Enable()
    {
      if (DebugHelper.IsDebugMode())
      {
        this.IsEnabled = true;
        Inv.Trace.IsManuallyEnabled = true;
      }

      return this;
    }
  }

  public sealed class TraceClass
  {
    internal TraceClass(string Name, TraceFeature[] FeatureArray)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.CheckNotNull(Name, nameof(Name));
        Inv.Assert.CheckNotNull(FeatureArray, nameof(FeatureArray));
        Inv.Assert.Check(FeatureArray.Length > 0, $"{nameof(FeatureArray)} must include at least one item");
      }

      this.Name = Name;
      this.FeatureArray = FeatureArray;
    }

    public bool IsActive => Inv.Trace.IsActive(FeatureArray);

    public void Enter(string Text = "", [CallerMemberName] string CallerMemberName = "", [CallerFilePath] string CallerFilePath = "", [CallerLineNumber] int CallerLineNumber = 0)
    {
      if (IsActive)
        Trace.Enter(Prefix(Text, CallerMemberName), CallerMemberName: string.Empty, CallerFilePath, CallerLineNumber);
    }
    public void Leave(string Text = "", [CallerMemberName] string CallerMemberName = "", [CallerFilePath] string CallerFilePath = "", [CallerLineNumber] int CallerLineNumber = 0)
    {
      if (IsActive)
        Trace.Leave(Prefix(Text, CallerMemberName), CallerMemberName: string.Empty, CallerFilePath, CallerLineNumber);
    }
    public void Write(string Text = "", [CallerMemberName] string CallerMemberName = "", [CallerFilePath] string CallerFilePath = "", [CallerLineNumber] int CallerLineNumber = 0)
    {
      if (IsActive)
        Trace.Write(Prefix(Text, CallerMemberName), CallerMemberName: string.Empty, CallerFilePath, CallerLineNumber);
    }
    public void Measure(string Text, Action Action, [CallerMemberName] string CallerMemberName = "", [CallerFilePath] string CallerFilePath = "", [CallerLineNumber] int CallerLineNumber = 0)
    {
      var Stopwatch = null as System.Diagnostics.Stopwatch;

      if (IsActive)
      {
        Stopwatch = Stopwatch.StartNew();
        Trace.Enter(Prefix($"{Text} START", CallerMemberName), CallerMemberName: string.Empty, CallerFilePath, CallerLineNumber);
      }

      Action?.Invoke();

      if (Stopwatch != null)
      {
        Stopwatch.Stop();
        Trace.Leave(Prefix($"{Text} FINISH: {Stopwatch.ElapsedMilliseconds}ms", CallerMemberName), CallerMemberName: string.Empty, CallerFilePath, CallerLineNumber);
      }
    }
    public TraceInstance NewInstance(string Name = null)
    {
      return new TraceInstance(this, Name, Seed++);
    }

    private string Prefix(string Text, string CallerMemberName) => $"{Name}.{CallerMemberName}{(string.IsNullOrWhiteSpace(Text) ? string.Empty : $" {Text}")}";

    private readonly string Name;
    private readonly TraceFeature[] FeatureArray;
    private int Seed = 1;
  }

  public sealed class TraceInstance
  {
    internal TraceInstance(TraceClass Class, string Name, int Identifier)
    {
      this.Class = Class;
      this.Name = Name;
      this.Identifier = Identifier;
    }

    public string Name { get; set; }
    public bool IsActive => Class.IsActive;

    public string Format() => $"'{Name ?? UnnamedText}' 0x{Identifier:X4}";
    public void Enter(string Text = null, [CallerMemberName] string CallerMemberName = "", [CallerFilePath] string CallerFilePath = "", [CallerLineNumber] int CallerLineNumber = 0)
    {
      if (IsActive)
        Class.Enter(Prefix(Text), CallerMemberName: CallerMemberName, CallerFilePath, CallerLineNumber);
    }
    public void Leave(string Text = null, [CallerMemberName] string CallerMemberName = "", [CallerFilePath] string CallerFilePath = "", [CallerLineNumber] int CallerLineNumber = 0)
    {
      if (IsActive)
        Class.Leave(Prefix(Text), CallerMemberName: CallerMemberName, CallerFilePath, CallerLineNumber);
    }
    public void Write(string Text = null, [CallerMemberName] string CallerMemberName = "", [CallerFilePath] string CallerFilePath = "", [CallerLineNumber] int CallerLineNumber = 0)
    {
      if (IsActive)
        Class.Write(Prefix(Text), CallerMemberName: CallerMemberName, CallerFilePath, CallerLineNumber);
    }

    private string Prefix(string Text) => $"{Format()}{(string.IsNullOrWhiteSpace(Text) ? string.Empty : $" ({Text})")}";

    private readonly TraceClass Class;
    private readonly int Identifier;

    private const string UnnamedText = "<unnamed>";
  }
}