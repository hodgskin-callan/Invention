﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Support
{
  /// <summary>
  /// <see cref="Inv.Colour"/> extension methods.
  /// </summary>
  public static class ColourHelper
  {
    /// <summary>
    /// Return a new colour with the applied opacity percentage.
    /// </summary>
    /// <param name="Colour"></param>
    /// <param name="Percent"></param>
    /// <returns></returns>
    public static Inv.Colour Opacity(this Inv.Colour Colour, float Percent)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Percent >= 0.0F && Percent <= 1.0F, "Opacity percent must be between 0.0F and 1.0F.");

      if (Colour == null)
        return null;

      if (Percent >= 1.0F)
        return Colour;

      if (Percent <= 0.0F)
        return Colour.AdjustAlpha(0);

      var Record = Colour.GetARGBRecord();

      Record.A = (byte)(Record.A * Percent);

      return ColourFactory.Resolve(Record.Argb);
    }
    /// <summary>
    /// Return a new colour lightened by the applied percentage using the HSL colour space.
    /// </summary>
    /// <param name="Colour"></param>
    /// <param name="Percent"></param>
    /// <returns></returns>
    public static Inv.Colour Lighten(this Inv.Colour Colour, float Percent)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Percent >= 0.0F && Percent <= 1.0F, "Lighten percent must be between 0.0F and 1.0F.");

      if (Colour == null)
        return null;

      if (Percent == 0.0F)
        return Colour;

      var ARGBRecord = Colour.GetARGBRecord();
      var HSLRecord = ARGBRecord.GetHSLRecord();

      var L = HSLRecord.L == 0.0 ? Percent : HSLRecord.L + (HSLRecord.L * Percent);

      if (L > 1.0)
        L = 1.0F;
      else if (L <= 0.0)
        L = 0.0F;

      return Inv.Colour.ConvertHSLToColour(ARGBRecord.A, HSLRecord.H, HSLRecord.S, L);
    }
    /// <summary>
    /// Return a new colour darkened by the applied percentage using the HSL colour space.
    /// </summary>
    /// <param name="Colour"></param>
    /// <param name="Percent"></param>
    /// <returns></returns>
    public static Inv.Colour Darken(this Inv.Colour Colour, float Percent)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Percent >= 0.0F && Percent <= 1.0F, "Darken percent must be between 0.0F and 1.0F.");

      if (Colour == null)
        return null;

      if (Percent == 0.0F)
        return Colour;

      var ARGBRecord = Colour.GetARGBRecord();
      var HSLRecord = ARGBRecord.GetHSLRecord();

      // NOTE: asymmetrical with lighten because black can't get any darker.
      var L = HSLRecord.L - (HSLRecord.L * Percent);

      if (L > 1.0)
        L = 1.0F;
      else if (L <= 0.0)
        L = 0.0F;

      return Inv.Colour.ConvertHSLToColour(ARGBRecord.A, HSLRecord.H, HSLRecord.S, L);
    }
    /// <summary>
    /// Return a new colour with the Alpha value overridden by the provided value.
    /// </summary>
    /// <param name="Colour"></param>
    /// <param name="AlphaValue"></param>
    /// <returns></returns>
    public static Inv.Colour AdjustAlpha(this Inv.Colour Colour, byte AlphaValue)
    {
      if (Colour == null)
        return null;

      var ARGBColour = Colour.GetARGBRecord();

      return Inv.Colour.FromArgb(AlphaValue, ARGBColour.R, ARGBColour.G, ARGBColour.B);
    }
    /// <summary>
    /// Return a new colour by inverting the lightness in the HSL colour space.
    /// This can be used to determine the foreground text colour given an arbitrary background colour.
    /// </summary>
    /// <param name="Colour"></param>
    /// <returns></returns>
    public static Inv.Colour BackgroundToForeground(this Inv.Colour Colour)
    {
      if (Colour == null)
        return null;

      var ARGBColour = Colour.GetARGBRecord();
      var HSLRecord = ARGBColour.GetHSLRecord();

      return Inv.Colour.ConvertHSLToColour(ARGBColour.A, HSLRecord.H, HSLRecord.S, HSLRecord.L <= 0.5F ? 1.0F : 0.0F);
    }
    /// <summary>
    /// Return appropriate black or white foreground colour by analysing brightness of input background colour.
    /// </summary>
    /// <param name="Colour"></param>
    /// <returns></returns>
    public static Inv.Colour BackgroundToBlackWhiteForeground(this Inv.Colour Colour)
    {
      if (Colour == null)
        return null;

      var ARGBColour = Colour.GetARGBRecord();

      int Threshold = 130;
      if ((255 - Convert.ToInt32((ARGBColour.R * 0.299) + (ARGBColour.G * 0.587) + (ARGBColour.B * 0.114))) < Threshold)
        return Inv.Colour.Black;
      else
        return Inv.Colour.White;
    }
    /// <summary>
    /// Return black or white where return is closest approximation of input colour.
    /// </summary>
    /// <param name="Colour"></param>
    /// <returns></returns>
    public static Inv.Colour ToBlackWhite(this Inv.Colour Colour)
    {
      if (Colour == null)
        return null;

      var ARGBColour = Colour.GetARGBRecord();

      int Threshold = 130;
      if ((255 - Convert.ToInt32((ARGBColour.R * 0.299) + (ARGBColour.G * 0.587) + (ARGBColour.B * 0.114))) < Threshold)
        return Inv.Colour.White;
      else
        return Inv.Colour.Black;
    }
    /// <summary>
    /// Return null if the input colour is null or transparent.
    /// </summary>
    /// <param name="Colour"></param>
    /// <returns></returns>
    public static Inv.Colour TransparentAsNull(this Inv.Colour Colour)
    {
      if (Colour == null || Colour.IsTransparent)
        return null;

      return Colour;
    }
  }

  /// <summary>
  /// System.Decimal extension methods.
  /// </summary>
  public static class DecimalHelper
  {
    /// <summary>
    /// Remove trailing zeros.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static decimal Normalise(this decimal Source)
    {
      return Source / 1.000000000000000000000000000000000m;
    }
  }

  /// <summary>
  /// <see cref="double"/> extension methods.
  /// </summary>
  public static class DoubleHelper
  {
    /// <summary>
    /// Compares this instance to a specified double-precision floating-point number and returns an integer that indicates whether the value of this instance is less than, equal to, or greater than the value of the specified double-precision floating-point number.
    /// <para>Note: this method consider variations that are less than or equal <paramref name="Precision"/> to be insignificant.</para>
    /// </summary>
    /// <param name="A">This double.</param>
    /// <param name="B">A double-precision floating-point number to compare to.</param>
    /// <param name="Precision">The threshold at which floating point imprecision should be considered insignificant.</param>
    /// <returns></returns>
    public static int PrecisionCompareTo(this double A, double B, double Precision)
    {
      if (Math.Abs(A - B) < Precision)
        return 0;

      return A.CompareTo(B);
    }
    /// <summary>
    /// Returns a value indicating whether this instance and a specified double-precision floating-point number represent the same value.
    /// <para>Note: this method consider variations that are less than or equal <paramref name="Precision"/> to be insignificant.</para>
    /// </summary>
    /// <param name="A">This double.</param>
    /// <param name="B">A double-precision floating-point number to compare to.</param>
    /// <param name="Precision">The threshold at which floating point imprecision should be considered insignificant.</param>
    /// <returns></returns>
    public static bool PrecisionEquals(this double A, double B, double Precision)
    {
      return A.PrecisionCompareTo(B, Precision) == 0;
    }
  }

  /// <summary>
  /// <see cref="Assembly"/> extension methods.
  /// </summary>
  public static class AssemblyHelper
  {
    /// <summary>
    /// Render the assembly version excluding the revision number.
    /// This means the version is just three numbers Major.Minor.Build.
    /// </summary>
    /// <param name="Assembly"></param>
    /// <returns></returns>
    public static string GetRelease(this Assembly Assembly)
    {
      var Version = Assembly.GetName().Version;

      return Version.Major + "." + Version.Minor + "." + Version.Build;
    }
    /// <summary>
    /// Render the revision number from the assembly version.
    /// </summary>
    /// <param name="Assembly"></param>
    /// <returns></returns>
    public static string GetBuild(this Assembly Assembly)
    {
      var Version = Assembly.GetName().Version;

      return Version.Revision.ToString();
    }
    /// <summary>
    /// Render the full assembly version.
    /// This is four numbers Major.Minor.Build.Revision.
    /// </summary>
    /// <param name="Assembly"></param>
    /// <returns></returns>
    public static string GetVersion(this Assembly Assembly)
    {
      return Assembly.GetName().Version.ToString();
    }
    /// <summary>
    /// Extract a given named resource from this <see cref="Assembly"/> as a byte array.
    /// </summary>
    /// <param name="Assembly">This <see cref="Assembly"/>.</param>
    /// <param name="ResourceName">The name of the resource to extract.</param>
    /// <param name="DefaultNamespace">The default namespace used to find the resource.</param>
    /// <returns>The contents of the named resource as a byte array.</returns>
    public static byte[] ExtractResourceBuffer(this Assembly Assembly, string ResourceName, string DefaultNamespace = "")
    {
      using (var ResourceStream = ExtractResourceStream(Assembly, ResourceName, DefaultNamespace))
      {
        if (ResourceStream == null)
          return null;

        var Result = new byte[ResourceStream.Length];

        ResourceStream.ReadByteArray(Result);

        return Result;
      }
    }
    /// <summary>
    /// Extract a given named resource from this <see cref="Assembly"/> as a <see cref="String"/>.
    /// </summary>
    /// <param name="Assembly">This <see cref="Assembly"/>.</param>
    /// <param name="ResourceName">The name of the resource to extract.</param>
    /// <param name="DefaultNamespace">The default namespace used to find the resource.</param>
    /// <returns>The contents of the named resource as a <see cref="String"/>.</returns>
    public static string ExtractResourceString(this Assembly Assembly, string ResourceName, string DefaultNamespace = "")
    {
      using (var ResourceStream = ExtractResourceStream(Assembly, ResourceName, DefaultNamespace))
      {
        if (ResourceStream == null)
          return null;

        using (var StreamReader = new global::System.IO.StreamReader(ResourceStream))
          return StreamReader.ReadToEnd();
      }
    }
    /// <summary>
    /// Extract a given named resource from this <see cref="Assembly"/> as a <see cref="String"/>.
    /// </summary>
    /// <param name="Assembly">This <see cref="Assembly"/>.</param>
    /// <param name="ResourceName">The name of the resource to extract.</param>
    /// <param name="DefaultNamespace">The default namespace used to find the resource.</param>
    /// <returns>The contents of the named resource as a <see cref="Stream"/> that must be Disposed when you are finished.</returns>
    public static System.IO.Stream ExtractResourceStream(this Assembly Assembly, string ResourceName, string DefaultNamespace = "")
    {
      var ResourceLocation = CanonicaliseResourceName(Assembly, ResourceName, DefaultNamespace);

      var ResourceStream = Assembly.GetManifestResourceStream(ResourceLocation);

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(ResourceStream != null, "Identifier '{0}' is not an embedded resource.", ResourceLocation);

      return ResourceStream;
    }

    /// <summary>
    /// Given a resource name, assembly, and optionally default namespace to look in, attempt to find the fully-qualified name of a matching resource in the assembly.
    /// </summary>
    /// <param name="Assembly"></param>
    /// <param name="ResourceName"></param>
    /// <param name="DefaultNamespace"></param>
    /// <returns></returns>
    public static string CanonicaliseResourceName(this Assembly Assembly, string ResourceName, string DefaultNamespace = null)
    {
      var ResourceNameArray = Assembly.GetManifestResourceNames();

      string Candidate = null;

      // If a namespace is specified already, check that first.
      if (!string.IsNullOrWhiteSpace(DefaultNamespace))
        Candidate = ResourceNameArray.Where(x => x == DefaultNamespace + "." + ResourceName).FirstOrDefault();

      if (Candidate != null)
        return Candidate;

      // Otherwise, try to find resources that look like they match at least the name
      Candidate = ResourceNameArray.Where(x => x.EndsWith("." + ResourceName)).OrderByDescending(x => x.Length).FirstOrDefault();

      if (Candidate != null)
        return Candidate;

      // Otherwise, try a partial name match as a last resort - this probably won't be the correct resource.
      return ResourceNameArray.Where(x => x.EndsWith(ResourceName)).OrderByDescending(x => x.Length).FirstOrDefault() ?? ResourceName;
    }
  }

  /// <summary>
  /// <see cref="ICollection{T}"/> extension methods.
  /// </summary>
  public static class ICollectionHelper
  {
    /// <summary>
    /// Add a range of elements to this <see cref="ICollection{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="ICollection{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="ICollection{T}"/>.</param>
    /// <param name="Array">An <see cref="IEnumerable{T}"/> containing the elements to add to this <see cref="ICollection{T}"/>.</param>
    public static void AddRange<T>(this ICollection<T> Source, IEnumerable<T> Array)
    {
      foreach (var Item in Array)
        Source.Add(Item);
    }
    /// <summary>
    /// Remove a range of elements from this <see cref="ICollection{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="ICollection{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="ICollection{T}"/>.</param>
    /// <param name="Array">An <see cref="IEnumerable{T}"/> containing the elements to remove from this <see cref="ICollection{T}"/>.</param>
    public static void RemoveRange<T>(this ICollection<T> Source, IEnumerable<T> Array)
    {
      foreach (var Item in Array)
        Source.Remove(Item);
    }
    /// <summary>
    /// Remove all items from a collection except the input enumerable.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Source"></param>
    /// <param name="Array"></param>
    /// <returns></returns>
    public static int RemoveExcept<T>(this ICollection<T> Source, IEnumerable<T> Array)
    {
      var Result = 0;

      var RemoveArray = Source.Except(Array).ToDistinctList();

      foreach (var Item in RemoveArray)
      {
        Source.Remove(Item);
        Result++;
      }

      return Result;
    }
    /// <summary>
    /// Determine whether or not this <see cref="ICollection{T}"/> has an element numbered <paramref name="Index"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="ICollection{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="ICollection{T}"/>.</param>
    /// <param name="Index">The index value to check.</param>
    /// <returns>True if <paramref name="Index"/> is within the bounds of this <see cref="ICollection{T}"/>; false otherwise.</returns>
    public static bool ExistsAt<T>(this System.Collections.Generic.ICollection<T> Source, int Index)
    {
      return Index >= 0 && Index < Source.Count;
    }
    /// <summary>
    /// Determine whether or not there exists an element in this <see cref="ICollection{T}"/> that satisfies the given predicate.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="ICollection{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="ICollection{T}"/>.</param>
    /// <param name="Predicate">The predicate to check.</param>
    /// <returns>True if any element of this <see cref="ICollection{T}"/> satisfies <paramref name="Predicate"/>; false otherwise.</returns>
    public static bool Exists<T>(this System.Collections.ICollection Source, Predicate<T> Predicate)
    {
      return Source.Cast<T>().Any(Item => Predicate(Item));
    }
  }

  /// <summary>
  /// <see cref="Inv.Date"/> extension methods.
  /// </summary>
  public static class DateHelper
  {
    /// <summary>
    /// Determine age (in whole number years) based on a given birth date.
    /// </summary>
    /// <param name="BirthDate">The birth date.</param>
    /// <param name="NowDate">The current date.</param>
    /// <returns></returns>
    public static int AgeInYears(this Inv.Date BirthDate, Inv.Date NowDate)
    {
      var Result = NowDate.Year - BirthDate.Year;

      if (NowDate < BirthDate.AddYears(Result))
        Result = Math.Max(0, Result - 1);

      return Result;
    }

    /// <summary>
    /// Is this date the anniversary of the original date.
    /// </summary>
    /// <param name="Source">The date to check as the anniversary of the original date</param>
    /// <param name="AnniversaryDate">The original date</param>
    /// <param name="TimePeriod">Can be set to year anniversary, month anniversary, four week anniversary, etc.</param>
    /// <returns></returns>
    public static bool IsAnniversaryOf(this Inv.Date? Source, Inv.Date AnniversaryDate, Inv.TimePeriod TimePeriod)
    {
      if (Source == null)
        return false;

      return AnniversaryDate.NextAnniversaryDate(Source.Value, TimePeriod) == Source.Value;
    }
    /// <summary>
    /// Is this date the last day of the month.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static bool IsEndOfMonth(this Inv.Date Source)
    {
      return Source.EndOfMonth() == Source;
    }
    /// <summary>
    /// Is this date the last day of the year.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static bool IsEndOfYear(this Inv.Date Source)
    {
      return Source.EndOfYear() == Source;
    }
    /// <summary>
    /// Return the source date as a DateTime representing the start of the day.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static DateTime? StartOfDay(this Inv.Date? Source)
    {
      return Source?.StartOfDay();
    }
    /// <summary>
    /// Return the source date as a DateTime representing the end of the day.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static DateTime? EndOfDay(this Inv.Date? Source)
    {
      return Source?.EndOfDay();
    }

    /// <summary>
    /// Good Friday is the Friday before Easter.
    /// </summary>
    /// <param name="Year"></param>
    /// <returns></returns>
    public static Inv.Date EasterFriday(int Year)
    {
      return EasterSunday(Year).AddDays(-2);
    }
    /// <summary>
    /// Easter falls on the first Sunday following the Paschal Full Moon, the full moon on or after 21 March, taken to be the date of the vernal equinox.
    /// </summary>
    /// <param name="Year"></param>
    /// <returns></returns>
    public static Inv.Date EasterSunday(int Year)
    {
      var g = Year % 19;
      var c = Year / 100;
      var h = (c - (c / 4) - ((8 * c + 13) / 25) + 19 * g + 15) % 30;
      var i = h - (h / 28) * (1 - (h / 28) * (29 / (h + 1)) * ((21 - g) / 11));

      var Day = i - ((Year + (Year / 4) + i + 2 - c + (c / 4)) % 7) + 28;
      var Month = 3;

      if (Day > 31)
      {
        Month++;
        Day -= 31;
      }

      return new Inv.Date(Year, Month, Day);
    }

    public static readonly int[] YearMonthDays = new int[12] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    public static readonly int[] LeapYearMonthDays = new int[12] { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  }

  /// <summary>
  /// <see cref="DateTime"/> extension methods.
  /// </summary>
  public static class DateTimeHelper
  {
    /// <summary>
    /// Add a number of weeks to the date time.
    /// </summary>
    /// <param name="Source"></param>
    /// <param name="Weeks"></param>
    /// <returns></returns>
    public static DateTime AddWeeks(this DateTime Source, int Weeks)
    {
      return Source.AddDays(Weeks * 7);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the year of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The start of the year of this <see cref="DateTime"/>.</returns>
    public static DateTime StartOfYear(this DateTime Source)
    {
      return new DateTime(Source.Year, 1, 1, 0, 0, 0, 0, Source.Kind);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the end of the year of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The end of the year of this <see cref="DateTime"/>.</returns>
    public static DateTime EndOfYear(this DateTime Source)
    {
      return new DateTime(Source.Year, 12, 31, 23, 59, 59, 999, Source.Kind);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the quarter of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The start of the quarter of this <see cref="DateTime"/>.</returns>
    public static DateTime StartOfQuarter(this DateTime Source)
    {
      var Quarter = (Source.Month - 1) / 3;
      var Month = (Quarter * 3) + 1;

      return new DateTime(Source.Year, Month, 1, 0, 0, 0, Source.Kind);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the end of the quarter of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The end of the quarter of this <see cref="DateTime"/>.</returns>
    public static DateTime EndOfQuarter(this DateTime Source)
    {
      var Quarter = (Source.Month - 1) / 3;
      var Month = (Quarter * 3) + 3;

      return new DateTime(Source.Year, Month, DateTime.DaysInMonth(Source.Year, Month), 23, 59, 59, 999, Source.Kind);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the month of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The start of the Month of this <see cref="DateTime"/>.</returns>
    public static DateTime StartOfMonth(this DateTime Source)
    {
      return new DateTime(Source.Year, Source.Month, 1, 0, 0, 0, 0, Source.Kind);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the end of the month of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The end of the month of this <see cref="DateTime"/>.</returns>
    public static DateTime EndOfMonth(this DateTime Source)
    {
      return new DateTime(Source.Year, Source.Month, 1, 23, 59, 59, 999, Source.Kind).AddMonths(1).AddDays(-1);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the financial year of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The start of the financial year of this <see cref="DateTime"/>.</returns>
    public static DateTime StartOfFinancialYear(this DateTime Source)
    {
      var Target = new DateTime(Source.Year, 7, 1, 0, 0, 0, 0, Source.Kind);

      if (Source < Target)
        Target = new DateTime(Source.Year - 1, 7, 1, 0, 0, 0, 0, Source.Kind);

      return Target;
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the end of the financial year of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>The end of the financial year of this <see cref="DateTime"/>.</returns>
    public static DateTime EndOfFinancialYear(this DateTime Source)
    {
      var Target = new DateTime(Source.Year, 06, 30, 23, 59, 59, 999, Source.Kind);

      if (Source > Target)
        Target = new DateTime(Source.Year + 1, 06, 30, 23, 59, 59, 999, Source.Kind);

      return Target;
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the week of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <param name="StartOfWeek">The day considered to be the start of the week.</param>
    /// <returns>A <see cref="DateTime"/> representing the start of the week of this <see cref="DateTime"/>.</returns>
    public static DateTime StartOfWeek(this DateTime Source, DayOfWeek StartOfWeek)
    {
      var Difference = Source.DayOfWeek - StartOfWeek;

      if (Difference < 0)
        Difference += 7;

      return Source.AddDays(-1 * Difference).Date;
    }
    public static DateTime StartOfWeek(this DateTime Source)
    {
      return StartOfWeek(Source, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the end of the week of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <param name="EndOfWeek">The day considered to be the end of the week.</param>
    /// <returns>A <see cref="DateTime"/> representing the end of the week of this <see cref="DateTime"/>.</returns>
    public static DateTime EndOfWeek(this DateTime Source, DayOfWeek EndOfWeek)
    {
      var Target = (int)EndOfWeek;

      if (Target < (int)Source.DayOfWeek)
        Target += 7;

      return Source.AddDays(Target - (int)Source.DayOfWeek);
    }
    public static DateTime EndOfWeek(this DateTime Source)
    {
      return EndOfWeek(Source, EnumHelper.Previous(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek));
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the day of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>A <see cref="DateTime"/> representing the start of the day of this <see cref="DateTime"/>.</returns>
    public static DateTime StartOfDay(this DateTime Source)
    {
      return Source.Date;
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the end of the day of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>A <see cref="DateTime"/> representing the end of the day of this <see cref="DateTime"/>.</returns>
    public static DateTime EndOfDay(this DateTime Source)
    {
      return Source.Date.AddDays(1).AddTicks(-1);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the day of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>A <see cref="DateTime"/> representing the start of the day of this <see cref="DateTime"/>.</returns>
    public static DateTime? StartOfDay(this DateTime? Source)
    {
      return Source?.StartOfDay();
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the end of the day of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>A <see cref="DateTime"/> representing the end of the day of this <see cref="DateTime"/>.</returns>
    public static DateTime? EndOfDay(this DateTime? Source)
    {
      return Source?.EndOfDay();
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the minute of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>A <see cref="DateTime"/> representing the start of the minute of this <see cref="DateTime"/>.</returns>
    public static DateTime StartOfMinute(this DateTime Source)
    {
      return new DateTime(Source.Year, Source.Month, Source.Day, Source.Hour, Source.Minute, 0, Source.Kind);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the end of the minute of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>A <see cref="DateTime"/> representing the end of the minute of this <see cref="DateTime"/>.</returns>
    public static DateTime EndOfMinute(this DateTime Source)
    {
      return Source.StartOfMinute().AddMinutes(1).AddTicks(-1);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the second of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>A <see cref="DateTime"/> representing the start of the second of this <see cref="DateTime"/>.</returns>
    public static DateTime StartOfSecond(this DateTime Source)
    {
      return new DateTime(Source.Ticks - (Source.Ticks % TimeSpan.TicksPerSecond), Source.Kind);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the start of the hour of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>A <see cref="DateTime"/> representing the start of the hour of this <see cref="DateTime"/>.</returns>
    public static DateTime StartOfHour(this DateTime Source)
    {
      return new DateTime(Source.Year, Source.Month, Source.Day, Source.Hour, 0, 0, Source.Kind);
    }
    /// <summary>
    /// Get a <see cref="DateTime"/> object representing the end of the hour of this <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <returns>A <see cref="DateTime"/> representing the end of the hour of this <see cref="DateTime"/>.</returns>
    public static DateTime EndOfHour(this DateTime Source)
    {
      return Source.StartOfHour().AddHours(1).AddTicks(-1);
    }
    /// <summary>
    /// Check whether this <see cref="DateTime"/> is within a threshold amount of another <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Left">This <see cref="DateTime"/>.</param>
    /// <param name="Right">The <see cref="DateTime"/> to compare to.</param>
    /// <param name="Threshold">The amount of leeway to consider two <see cref="DateTime"/>s to be identical.</param>
    /// <returns>True if the difference between this <see cref="DateTime"/> and <paramref name="Right"/> is shorter than <paramref name="Threshold"/></returns>
    public static bool EqualTo(this System.DateTime Left, DateTime Right, TimeSpan Threshold)
    {
      return (Left - Right).Duration() <= Threshold;
    }
    /// <summary>
    /// Is this datetime on the weekend.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static bool IsWeekend(this System.DateTime Source)
    {
      var Day = Source.DayOfWeek;

      return Day == DayOfWeek.Saturday || Day == DayOfWeek.Sunday;
    }
    /// <summary>
    /// Is this datetime on a weekday.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static bool IsWeekday(this System.DateTime Source)
    {
      return !IsWeekend(Source);
    }
    /// <summary>
    /// Enumerate each date between the first and last date, inclusive.
    /// </summary>
    /// <param name="FirstDate"></param>
    /// <param name="LastDate"></param>
    /// <returns></returns>
    public static IEnumerable<DateTime> DateSeries(this DateTime FirstDate, DateTime LastDate)
    {
      if (FirstDate > LastDate)
        yield break;

      var CurrentDate = FirstDate.Date;
      do
      {
        yield return CurrentDate;

        CurrentDate = CurrentDate.AddDays(1);
      }
      while (CurrentDate <= LastDate.Date);
    }
    public static void AddRange(this ICollection<DateTime> Source, IEnumerable<DateTimeOffset> Range)
    {
      Source.AddRange(Range.Select(Index => Index.DateTime));
    }
    public static System.DateTime AddPeriod(this System.DateTime Source, Inv.TimePeriod TimePeriod)
    {
      return (Source.AsDate().AddDatePeriod(TimePeriod) + Source.TimeOfDay).AddHours(TimePeriod.Hours).AddMinutes(TimePeriod.Minutes).AddSeconds(TimePeriod.Seconds).AddMilliseconds(TimePeriod.Milliseconds);
    }
    /// <summary>
    /// Convert a <see cref="DateTime"/> to an <see cref="Inv.Date"/> by removing the time component.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static Inv.Date? AsDate(this DateTime? Source)
    {
      return Source != null ? new Inv.Date(Source.Value) : (Inv.Date?)null;
    }
    /// <summary>
    /// Convert a <see cref="DateTime"/> to an <see cref="Inv.Date"/> by removing the time component.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static Inv.Date AsDate(this DateTime Source)
    {
      return new Inv.Date(Source);
    }
    /// <summary>
    /// Convert a <see cref="DateTime"/> to an <see cref="Inv.Time"/> by removing the date component.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static Inv.Time? AsTime(this DateTime? Source)
    {
      return Source != null ? new Inv.Time(Source.Value) : (Inv.Time?)null;
    }
    /// <summary>
    /// Convert a <see cref="DateTime"/> to an <see cref="Inv.Time"/> by removing the date component.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static Inv.Time AsTime(this DateTime Source)
    {
      return new Inv.Time(Source);
    }
    /// <summary>
    /// Use the current culture to display the short date time format of a <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static string ToShortDateTimeString(this DateTime Source)
    {
      var DateTimeFormat = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat;

      return Source.ToString(DateTimeFormat.ShortDatePattern + " " + DateTimeFormat.ShortTimePattern);
    }
    /// <summary>
    /// Use the current culture to display the short date time format of a <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static string ToShortDateTimeString(this DateTime? Source)
    {
      return Source?.ToShortDateTimeString();
    }
    /// <summary>
    /// Use the current culture to display the short date format of a <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static string ToShortDateString(this DateTime Source)
    {
      return Source.ToString("d");
    }
    /// <summary>
    /// Use the current culture to display the short date format of a <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static string ToShortDateString(this DateTime? Source)
    {
      return Source?.ToShortDateString();
    }
    /// <summary>
    /// Convert a <see cref="DateTime"/> to unix time format.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static double ToUnixTimeSeconds(this DateTime Source)
    {
      var Epoch = new DateTime(1970, 1, 1, 0, 0, 0).ToLocalTime();

      return (Source.ToLocalTime() - Epoch).TotalSeconds;
    }
    /// <summary>
    /// Round a <see cref="DateTime"/> up to the granularity specified by a <see cref="TimeSpan"/>.
    /// </summary>
    /// <param name="dt"></param>
    /// <param name="ts"></param>
    /// <returns></returns>
    public static DateTime RoundUp(this DateTime dt, TimeSpan ts)
    {
      return new DateTime((dt.Ticks + ts.Ticks - 1) / ts.Ticks * ts.Ticks, dt.Kind);
    }
    /// <summary>
    /// Round a <see cref="DateTime"/> down to the granularity specified by a <see cref="TimeSpan"/>.
    /// </summary>
    /// <param name="dt"></param>
    /// <param name="ts"></param>
    /// <returns></returns>
    public static DateTime RoundDown(this DateTime dt, TimeSpan ts)
    {
      return new DateTime(dt.Ticks / ts.Ticks * ts.Ticks, dt.Kind);
    }
    /// <summary>
    /// Return the canonical format for a <see cref="DateTime"/> based on the provided date and time accuracy levels.
    /// </summary>
    /// <param name="Source"></param>
    /// <param name="DateAccuracy"></param>
    /// <param name="TimeAccuracy"></param>
    /// <returns></returns>
    public static string FormatCanonical(this DateTime Source, DateAccuracyLevel DateAccuracy, TimeAccuracyLevel TimeAccuracy)
    {
      var Format = "";

      if (DateAccuracy < DateAccuracyLevel.Day && DateAccuracy != DateAccuracyLevel.None && TimeAccuracy != TimeAccuracyLevel.None)
        throw new Exception("Cannot Show Time without at least having Day shown");

      if (DateAccuracy == DateAccuracyLevel.Day)
        Format = "yyyyMMdd";
      else if (DateAccuracy == DateAccuracyLevel.Month)
        Format = "yyyyMM";
      else if (DateAccuracy == DateAccuracyLevel.Year)
        Format = "yyyy";

      if (TimeAccuracy == TimeAccuracyLevel.Hour)
        Format += "HH";
      else if (TimeAccuracy == TimeAccuracyLevel.Minute)
        Format += "HHmm";
      else if (TimeAccuracy == TimeAccuracyLevel.Second)
        Format += "HHmmss";
      else if (TimeAccuracy == TimeAccuracyLevel.Millisecond)
        Format += "HHmmssfff";

      return Source.ToString(Format);
    }
    /// <summary>
    /// Determines if this <see cref="DateTime"/> is between the range of two <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <param name="From">The starting <see cref="DateTime"/>.</param>
    /// <param name="Until">The ending <see cref="DateTime"/>.</param>
    /// <returns>True if this <see cref="DateTime"/> is between the calculated range.</returns>
    public static bool Between(this DateTime Source, DateTime From, DateTime Until)
    {
      return Source >= From && Source <= Until;
    }
    /// <summary>
    /// Returns a string that represents the approximate time of an event as compared to now.
    /// </summary>
    /// <param name="DateTime"></param>
    /// <returns></returns>
    public static string ToRelativeDate(this System.DateTime DateTime)
    {
      return ToRelativeDate(System.DateTime.Now - DateTime);
    }
    /// <summary>
    /// Determines if this <see cref="DateTime"/> is between the range of two <see cref="DateTime"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTime"/>.</param>
    /// <param name="From">The starting <see cref="DateTime"/>.</param>
    /// <param name="Until">The ending <see cref="DateTime"/>.</param>
    /// <returns>True if this <see cref="DateTime"/> is between the calculated range.</returns>
    public static bool IsBetween(this DateTime Source, DateTime From, DateTime Until)
    {
      return Source >= From && Source <= Until;
    }
    /// <summary>
    /// Returns the smaller of two <see cref="DateTime"/> values.
    /// </summary>
    /// <param name="A">The first value to compare.</param>
    /// <param name="B">The second value to compare.</param>
    /// <returns>The smaller of the two values.</returns>
    public static DateTime Min(DateTime A, DateTime B) => A < B ? A : B;
    /// <summary>
    /// Returns the larger of the two <see cref="DateTime"/> values.
    /// </summary>
    /// <param name="A">The first value to compare.</param>
    /// <param name="B">The second value to compare.</param>
    /// <returns>The larger of the two values.</returns>
    public static DateTime Max(DateTime A, DateTime B) => A > B ? A : B;
    /// <summary>
    /// Convert a <see cref="DateTime"/> to a <see cref="DateTimeOffset"/> using the UTC offset for the time in the given time zone.
    /// </summary>
    /// <remarks>If <paramref name="Source"/> is not a valid time or is ambiguous within <paramref name="TimeZone"/>, the base UTC offset from the zone will be used. <paramref name="TimeZone"/> must be the zone indicated by the <see cref="DateTime.Kind"/> value for <paramref name="Source"/> unless it is <see cref="DateTimeKind.Unspecified"/>.</remarks>
    /// <param name="Source">The time to convert.</param>
    /// <param name="TimeZone">The time zone which <paramref name="Source"/> belongs to.</param>
    /// <returns>A <see cref="DateTime"/> value representing <paramref name="Source"/> within <paramref name="TimeZone"/>.</returns>
    /// <exception cref="ArgumentException">When <paramref name="TimeZone"/> is not compatible with <paramref name="Source"/>'s <see cref="DateTime.Kind"/> value.</exception>
    public static DateTimeOffset AsDateTimeOffset(this DateTime Source, TimeZoneInfo TimeZone)
    {
      return new DateTimeOffset(Source, TimeZone.GetUtcOffset(Source));
    }

    /// <summary>
    /// Indicates whether the two specified date time ranges intersect or are contiguous on their boundaries.
    /// </summary>
    /// <param name="LowerA">The lower bound of the first range</param>
    /// <param name="UpperA">The upper bound of the first range</param>
    /// <param name="LowerB">The lower bound of the second range</param>
    /// <param name="UpperB">The upper bound of the second range</param>
    /// <returns></returns>
    public static bool IsContiguousRange(this DateTime LowerA, DateTime UpperA, DateTime LowerB, DateTime UpperB)
    {
      if (UpperA == LowerA || LowerA == UpperB)
        return true;

      if (LowerA.Between(LowerB, UpperB))
        return true;

      if (LowerB.Between(LowerA, UpperA))
        return true;

      return false;
    }

    private static string ToRelativeDate(TimeSpan TimeSpan)
    {
      if (TimeSpan <= TimeSpan.FromSeconds(60))
      {
        var Seconds = Math.Max(1, TimeSpan.Seconds);
        return Seconds + " " + Inv.Support.StringHelper.Plural("second", Seconds) + " ago";
      }

      if (TimeSpan <= TimeSpan.FromMinutes(60))
        return TimeSpan.Minutes > 1 ? "about " + TimeSpan.Minutes + " minutes ago" : "about a minute ago";

      if (TimeSpan <= TimeSpan.FromHours(24))
        return TimeSpan.Hours > 1 ? "about " + TimeSpan.Hours + " hours ago" : "about an hour ago";

      if (TimeSpan <= TimeSpan.FromDays(30))
        return TimeSpan.Days > 1 ? "about " + TimeSpan.Days + " days ago" : "about a day ago";

      if (TimeSpan <= TimeSpan.FromDays(365))
        return TimeSpan.Days > 30 ? "about " + TimeSpan.Days / 30 + " months ago" : "about a month ago";

      return TimeSpan.Days > 365 ? "about " + TimeSpan.Days / 365 + " years ago" : "about a year ago";
    }
  }

  public enum DateAccuracyLevel
  {
    None,
    Year,
    Month,
    Day
  }

  public enum TimeAccuracyLevel
  {
    None,
    Hour,
    Minute,
    Second,
    Millisecond
  }

  /// <summary>
  /// <see cref="DayOfWeek"/> extension methods.
  /// </summary>
  public static class DayOfWeekHelper
  {
    static DayOfWeekHelper()
    {
      SeriesList = new DistinctList<DayOfWeek>();

      RankArray = new int[7];

      var FirstDayIndex = (int)FirstDayOfWeek;
      for (var DayIndex = 0; DayIndex < 7; DayIndex++)
      {
        var RankIndex = (FirstDayIndex + DayIndex) % 7;
        RankArray[RankIndex] = DayIndex;

        SeriesList.Add((DayOfWeek)RankIndex);
      }
    }

    /// <summary>
    /// Return the current cultures first day of the week.
    /// </summary>
    public static DayOfWeek FirstDayOfWeek
    {
      get { return System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek; }
    }
    /// <summary>
    /// Return the current cultures last day of the week.
    /// </summary>
    public static DayOfWeek LastDayOfWeek
    {
      get
      {
        var DaysOfWeek = DayOfWeekSeries().ToDistinctList();
        var FirstDayOfWeekIndex = DaysOfWeek.IndexOf(FirstDayOfWeek);

        return FirstDayOfWeekIndex == 0 ? DaysOfWeek.Last() : DaysOfWeek[FirstDayOfWeekIndex - 1];
      }
    }
    public static DayOfWeek NextDayOfWeek(this DayOfWeek DayOfWeek)
    {
      var DaysOfWeek = DayOfWeekSeries().ToDistinctList();
      var DayOfWeekIndex = DaysOfWeek.IndexOf(DayOfWeek);

      return DayOfWeekIndex == DaysOfWeek.Count - 1 ? DaysOfWeek.First() : DaysOfWeek[DayOfWeekIndex + 1];
    }
    public static DayOfWeek PreviousDayOfWeek(this DayOfWeek DayOfWeek)
    {
      var DaysOfWeek = DayOfWeekSeries().ToDistinctList();
      var DayOfWeekIndex = DaysOfWeek.IndexOf(DayOfWeek);

      return DayOfWeekIndex == 0 ? DaysOfWeek.Last() : DaysOfWeek[DayOfWeekIndex - 1];
    }
    /// <summary>
    /// Return an enumerable of the days of the week in the current culture order.
    /// </summary>
    /// <returns></returns>
    public static IEnumerable<DayOfWeek> DayOfWeekSeries()
    {
      return SeriesList;
    }
    /// <summary>
    /// Returns the integer rank of the day of the week in the current culture order.
    /// </summary>
    /// <param name="DayOfWeek"></param>
    /// <returns></returns>
    public static int Rank(this DayOfWeek DayOfWeek)
    {
      return RankArray[(int)DayOfWeek];
    }
    /// <summary>
    /// Return the full name of the day in the current culture.
    /// </summary>
    /// <param name="DayOfWeek"></param>
    /// <returns></returns>
    public static string ToNameString(this DayOfWeek DayOfWeek)
    {
      return System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek);
    }
    /// <summary>
    /// Return the abbreviated name of the day in the current culture.
    /// </summary>
    /// <param name="DayOfWeek"></param>
    /// <returns></returns>
    public static string ToAbbreviatedString(this DayOfWeek DayOfWeek)
    {
      return System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedDayName(DayOfWeek);
    }

    public static string FormatRankRange(this IEnumerable<DayOfWeek> DayOfWeeks)
    {
      return DayOfWeeks.OrderBy(D => D.Rank()).Select(D => D.Rank().ToString()).AsSeparatedText("");
    }
    public static string FormatNameRange(this IEnumerable<DayOfWeek> DayOfWeeks)
    {
      return DayOfWeeks.FormatRange(D => D.ToNameString());
    }
    public static string FormatAbbreviatedRange(this IEnumerable<DayOfWeek> DayOfWeeks)
    {
      return DayOfWeeks.FormatRange(D => D.ToAbbreviatedString());
    }
    public static string FormatRange(this IEnumerable<DayOfWeek> DayOfWeeks, Func<DayOfWeek, string> FormatQuery)
    {
      if (DayOfWeeks == null || !DayOfWeeks.Any())
        return "";

      var PartList = new List<string>();

      var DayOfWeekList = EnumHelper.GetEnumerable<DayOfWeek>().OrderBy(D => D.Rank()).ToDistinctList();

      var FirstDayOfWeek = (DayOfWeek?)null;
      var LastDayOfWeek = (DayOfWeek?)null;
      foreach (var DayOfWeek in DayOfWeekList)
      {
        if (DayOfWeeks.Contains(DayOfWeek))
        {
          if (FirstDayOfWeek == null)
            FirstDayOfWeek = DayOfWeek;

          LastDayOfWeek = DayOfWeek;

          if (DayOfWeekList.Last() != DayOfWeek)
            continue;
        }

        if (FirstDayOfWeek != null)
        {
          var Part = FormatQuery(FirstDayOfWeek.Value);

          if (FirstDayOfWeek != LastDayOfWeek && LastDayOfWeek != null)
            Part += "-" + FormatQuery(LastDayOfWeek.Value);

          PartList.Add(Part);

          FirstDayOfWeek = null;
          LastDayOfWeek = null;
        }
      }

      return PartList.AsNaturalAndSeparatedText();
    }

    private static readonly Inv.DistinctList<DayOfWeek> SeriesList;
    private static readonly int[] RankArray;
  }

  /// <summary>
  /// <see cref="Inv.DateRange"/> extension methods.
  /// </summary>
  public static class DateRangeHelper
  {
    public static string ToApproximation(this Inv.DateRange Range)
    {
      var Days = (int)Math.Abs((Range.Until.Value.ToDateTime() - Range.From.Value.ToDateTime()).TotalDays);

      if (Days == 0)
        return "Today";
      else if (Days == 1)
        return "Tomorrow";

      const int DaysInAWeek = 7;
      const int DaysInAMonth = 30;

      var Weeks = Days / DaysInAWeek;
      var Months = Days / DaysInAMonth;

      if (Months > 0)
      {
        if (Days - (Months * DaysInAMonth) > 15)
          Months++;

        return string.Format("{0} {1}", Months, "month".Plural(Months));
      }
      else if (Weeks > 1)
      {
        if (Days - (Weeks * DaysInAWeek) > 4)
          Weeks++;

        return string.Format("{0} {1}", Weeks, "week".Plural(Weeks));
      }
      else
        return string.Format("{0} days", Days);
    }
  }

  /// <summary>
  /// <see cref="DateTimeOffset"/> extension methods.
  /// </summary>
  public static class DateTimeOffsetHelper
  {
    /// <summary>
    /// Check whether this <see cref="DateTimeOffset"/> is within a threshold amount of another <see cref="DateTimeOffset"/>.
    /// </summary>
    /// <param name="Left">This <see cref="DateTimeOffset"/>.</param>
    /// <param name="Right">The <see cref="DateTimeOffset"/> to compare to.</param>
    /// <param name="Threshold">The amount of leeway to consider two <see cref="DateTimeOffset"/>s to be identical.</param>
    /// <returns>True if the difference between this <see cref="DateTimeOffset"/> and <paramref name="Right"/> is shorter than <paramref name="Threshold"/></returns>
    public static bool EqualTo(this System.DateTimeOffset Left, DateTimeOffset Right, TimeSpan Threshold)
    {
      return (Left - Right).Duration() <= Threshold;
    }
    /// <summary>
    /// Enumerate each date between the first and last date, inclusive.
    /// </summary>
    /// <param name="FirstDate"></param>
    /// <param name="LastDate"></param>
    /// <returns></returns>
    public static IEnumerable<DateTimeOffset> DateSeries(this DateTimeOffset FirstDate, DateTimeOffset LastDate)
    {
      if (FirstDate > LastDate)
        yield break;

      var CurrentDate = FirstDate.Date;
      do
      {
        yield return CurrentDate;

        CurrentDate = CurrentDate.AddDays(1);
      }
      while (CurrentDate <= LastDate.Date);
    }
    public static void AddRange(this ICollection<DateTimeOffset> Source, IEnumerable<DateTime> Range)
    {
      Source.AddRange(Range.Select(Index => (DateTimeOffset)Index));
    }
    /// <summary>
    /// Returns a <see cref="DateTimeOffset"/> representing the date and time from this <see cref="DateTimeOffset"/> in the specified time zone.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <param name="NewTimeZone">The time zone to adjust this <see cref="DateTimeOffset"/> to.</param>
    /// <returns>A <see cref="DateTimeOffset"/> instance representing this <see cref="DateTimeOffset"/> in the specified time zone.</returns>
    public static DateTimeOffset AlterBaseTimeZone(this DateTimeOffset Source, TimeZoneInfo NewTimeZone)
    {
      var Converted = TimeZoneInfo.ConvertTime(Source, NewTimeZone);
      return Converted.Subtract(Converted.Offset.Subtract(Source.Offset));
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the start of the minute of this <see cref="DateTimeOffset"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <returns>A <see cref="DateTimeOffset"/> representing the start of the minute of this <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset StartOfMinute(this DateTimeOffset Source)
    {
      return new DateTimeOffset(Source.Year, Source.Month, Source.Day, Source.Hour, Source.Minute, 0, Source.Offset);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the end of the minute of this <see cref="DateTimeOffset"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <returns>A <see cref="DateTimeOffset"/> representing the end of the minute of this <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset EndOfMinute(this DateTimeOffset Source)
    {
      return Source.StartOfMinute().AddMinutes(1).AddTicks(-1);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the start of the second of this <see cref="DateTimeOffset"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <returns>A <see cref="DateTimeOffset"/> representing the start of the second of this <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset StartOfSecond(this DateTimeOffset Source)
    {
      return new DateTimeOffset(Source.Ticks - (Source.Ticks % TimeSpan.TicksPerSecond), Source.Offset);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the start of the hour of this <see cref="DateTimeOffset"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <returns>A <see cref="DateTimeOffset"/> representing the start of the hour of this <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset StartOfHour(this DateTimeOffset Source)
    {
      return new DateTimeOffset(Source.Year, Source.Month, Source.Day, Source.Hour, 0, 0, Source.Offset);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the end of the hour of this <see cref="DateTimeOffset"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <returns>A <see cref="DateTimeOffset"/> representing the end of the hour of this <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset EndOfHour(this DateTimeOffset Source)
    {
      return Source.StartOfHour().AddHours(1).AddTicks(-1);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the start of the day of this <see cref="DateTimeOffset"/>.
    /// As the start of day is relative to a time zone, a zone is required in order to account for ambiguous <see cref="DateTimeOffset"/>s that occur during daylight savings transition.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <param name="Zone">The <see cref="TimeZoneInfo"/> within which a day is defined.</param>
    /// <returns>A <see cref="DateTimeOffset"/> representing the start of the day of this <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset StartOfDay(this DateTimeOffset Source, TimeZoneInfo Zone)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Source.Date != DateTimeOffset.MinValue.Date, "Date must be at least one day greater than '1/01/0001'");

      var StartDateTime = TimeZoneInfo.ConvertTime(Source, Zone).DateTime.StartOfDay();

      if (Zone.IsInvalidTime(StartDateTime))
        StartDateTime = BruteForceNextValidTime(Zone, StartDateTime, +1);

      var Offset = Zone.IsAmbiguousTime(StartDateTime)
        ? Zone.GetAmbiguousTimeOffsets(StartDateTime).Max()
        : Zone.GetUtcOffset(StartDateTime);

      return new DateTimeOffset(StartDateTime, Offset);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the end of the day of this <see cref="DateTimeOffset"/>.
    /// As the end of day is relative to a time zone, a zone is required in order to account for ambiguous <see cref="DateTimeOffset"/>s that occur during daylight savings transition.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <param name="Zone">The <see cref="TimeZoneInfo"/> within which a day is defined.</param>
    /// <returns>A <see cref="DateTimeOffset"/> representing the end of the day of this <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset EndOfDay(this DateTimeOffset Source, TimeZoneInfo Zone)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Source.Date != DateTimeOffset.MaxValue.Date, "Date must be at least one day before '31/12/9999'");

      var EndDateTime = TimeZoneInfo.ConvertTime(Source, Zone).DateTime.EndOfDay();

      if (Zone.IsInvalidTime(EndDateTime))
        EndDateTime = BruteForceNextValidTime(Zone, EndDateTime, -1);

      var Offset = Zone.IsAmbiguousTime(EndDateTime)
        ? Zone.GetAmbiguousTimeOffsets(EndDateTime).Min()
        : Zone.GetUtcOffset(EndDateTime);

      return new DateTimeOffset(EndDateTime, Offset);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the start of the day of this <see cref="DateTimeOffset"/> relative to the local time zone.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <returns>A <see cref="DateTimeOffset"/> representing the start of the day of this <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset StartOfDayLocal(this DateTimeOffset Source) => Source.StartOfDay(TimeZoneInfo.Local);
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the end of the day of this <see cref="DateTimeOffset"/> relative to the local time zone.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <returns>A <see cref="DateTimeOffset"/> representing the end of the day of this <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset EndOfDayLocal(this DateTimeOffset Source) => Source.EndOfDay(TimeZoneInfo.Local);
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the start of the week for this <see cref="DateTimeOffset"/>.
    /// A time zone is required in order to account for ambiguous <see cref="DateTimeOffset"/>s that occur during daylight savings transition.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <param name="StartOfWeek">The day considered to be the start of the week.</param>
    /// <returns>A <see cref="DateTimeOffset"/> representing the start of the week of this <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset StartOfWeek(this DateTimeOffset Source, DayOfWeek StartOfWeek, TimeZoneInfo Zone)
    {
      var Difference = Source.DayOfWeek - StartOfWeek;

      if (Difference < 0)
        Difference += 7;

      return new DateTimeOffset(Source.AddDays(-1 * Difference).Date, Source.Offset).StartOfDay(Zone);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the start of the week for this <see cref="DateTimeOffset"/>.
    /// A time zone is required in order to account for ambiguous <see cref="DateTimeOffset"/>s that occur during daylight savings transition.
    /// </summary>
    public static DateTimeOffset StartOfWeek(this DateTimeOffset Source, TimeZoneInfo Zone)
    {
      return StartOfWeek(Source, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek, Zone);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the start of the week for this <see cref="DateTimeOffset"/> relative to the local time zone.
    /// </summary>
    public static DateTimeOffset StartOfWeekLocal(this DateTimeOffset Source) => Source.StartOfWeek(TimeZoneInfo.Local);
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the end of the week for this <see cref="DateTimeOffset"/>.
    /// A time zone is required in order to account for ambiguous <see cref="DateTimeOffset"/>s that occur during daylight savings transition.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <param name="EndOfWeek">The day considered to be the end of the week.</param>
    /// <returns>A <see cref="DateTimeOffset"/> representing the end of the week of this <see cref="DateTimeOffset"/>.</returns>
    public static DateTimeOffset EndOfWeek(this DateTimeOffset Source, DayOfWeek EndOfWeek, TimeZoneInfo Zone)
    {
      var Target = (int)EndOfWeek;

      if (Target < (int)Source.DayOfWeek)
        Target += 7;

      return Source.AddDays(Target - (int)Source.DayOfWeek).EndOfDay(Zone);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the end of the week for this <see cref="DateTimeOffset"/>.
    /// A time zone is required in order to account for ambiguous <see cref="DateTimeOffset"/>s that occur during daylight savings transition.
    /// </summary>
    public static DateTimeOffset EndOfWeek(this DateTimeOffset Source, TimeZoneInfo Zone)
    {
      return EndOfWeek(Source, EnumHelper.Previous(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek), Zone);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the end of the week for this <see cref="DateTimeOffset"/> relative to the local time zone.
    /// </summary>
    public static DateTimeOffset EndOfWeekLocal(this DateTimeOffset Source) => Source.EndOfWeek(TimeZoneInfo.Local);
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the start of the month for this <see cref="DateTimeOffset"/>.
    /// A time zone is required in order to account for ambiguous <see cref="DateTimeOffset"/>s that occur during daylight savings transition.
    /// </summary>
    public static DateTimeOffset StartOfMonth(this DateTimeOffset Source, TimeZoneInfo Zone)
    {
      return new DateTimeOffset(Source.Year, Source.Month, 1, 0, 0, 0, Source.Offset).StartOfDay(Zone);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the start of the month for this <see cref="DateTimeOffset"/> relative to the local time zone.
    /// </summary>
    public static DateTimeOffset StartOfMonthLocal(this DateTimeOffset Source) => Source.StartOfMonth(TimeZoneInfo.Local);
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the end of the month for this <see cref="DateTimeOffset"/>.
    /// A time zone is required in order to account for ambiguous <see cref="DateTimeOffset"/>s that occur during daylight savings transition.
    /// </summary>
    public static DateTimeOffset EndOfMonth(this DateTimeOffset Source, TimeZoneInfo Zone)
    {
      return new DateTimeOffset(Source.Year, Source.Month, DateTime.DaysInMonth(Source.Year, Source.Month), 0, 0, 0, Source.Offset).EndOfDay(Zone);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the end of the month for this <see cref="DateTimeOffset"/> relative to the local time zone.
    /// </summary>
    public static DateTimeOffset EndOfMonthLocal(this DateTimeOffset Source) => Source.EndOfMonth(TimeZoneInfo.Local);
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the start of the year for this <see cref="DateTimeOffset"/>.
    /// A time zone is required in order to account for ambiguous <see cref="DateTimeOffset"/>s that occur during daylight savings transition.
    /// </summary>
    public static DateTimeOffset StartOfYear(this DateTimeOffset Source, TimeZoneInfo Zone)
    {
      return new DateTimeOffset(Source.Year, 1, 1, 0, 0, 0, Source.Offset).StartOfDay(Zone);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the start of the year for this <see cref="DateTimeOffset"/> relative to the local time zone.
    /// </summary>
    public static DateTimeOffset StartOfYearLocal(this DateTimeOffset Source) => Source.StartOfYear(TimeZoneInfo.Local);
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the end of the year for this <see cref="DateTimeOffset"/>.
    /// A time zone is required in order to account for ambiguous <see cref="DateTimeOffset"/>s that occur during daylight savings transition.
    /// </summary>
    public static DateTimeOffset EndOfYear(this DateTimeOffset Source, TimeZoneInfo Zone)
    {
      return new DateTimeOffset(Source.Year, 12, 31, 0, 0, 0, Source.Offset).EndOfDay(Zone);
    }
    /// <summary>
    /// Get a <see cref="DateTimeOffset"/> object representing the end of the year for this <see cref="DateTimeOffset"/> relative to the local time zone.
    /// </summary>
    public static DateTimeOffset EndOfYearLocal(this DateTimeOffset Source) => Source.EndOfYear(TimeZoneInfo.Local);
    public static DateTimeOffset AddWeeks(this DateTimeOffset Source, int Weeks)
    {
      return Source.AddDays(Weeks * 7);
    }
    public static string ToVerboseString(this DateTimeOffset Source)
    {
      return string.Format("{0} {1}", Source.AsDate().ToVerboseString(), Source.AsTime().ToString());
    }
    public static string ToAbbreviatedDateLongTimeString(this DateTimeOffset Source)
    {
      return string.Format("{0} {1}", Source.AsDate().ToAbbreviatedString(), Source.AsTime().ToLongString());
    }
    public static string ToLocalShortDateTimeString(this DateTimeOffset Source)
    {
      return Source.ToLocalTime().DateTime.ToString("g");
    }
    public static string ToLocalShortDateTimeString(this DateTimeOffset? Source)
    {
      return Source?.ToLocalShortDateTimeString();
    }
    public static string ToShortDateTimeString(this DateTimeOffset Source)
    {
      return Source.DateTime.ToString("g");
    }
    public static string ToShortDateTimeString(this DateTimeOffset? Source)
    {
      return Source?.ToShortDateTimeString();
    }
    public static string ToShortDateString(this DateTimeOffset Source)
    {
      return Source.ToString("d");
    }
    public static string ToShortDateString(this DateTimeOffset? Source)
    {
      return Source?.ToString("d");
    }
    /// <summary>
    /// Converts the value of the current <see cref="DateTimeOffset"/> object to its equivalent string representation relative to the current date.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <returns>A <see cref="string"/> representing this <see cref="DateTimeOffset"/> relative to the current date.</returns>
    public static string ToRelativeString(this DateTimeOffset Source)
    {
      return Source.ToRelativeString(DateTimeOffset.Now);
    }
    /// <summary>
    /// Converts the value of the current <see cref="DateTimeOffset"/> object to its equivalent string representation relative to some specified date.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <param name="RelativeDate">The <see cref="DateTimeOffset"/> to treat as the base date.</param>
    /// <returns>A <see cref="string"/> representing this <see cref="DateTimeOffset"/> relative to the specified date.</returns>
    public static string ToRelativeString(this DateTimeOffset Source, DateTimeOffset RelativeDate)
    {
      string Format;

      if (Source.Date == RelativeDate.Date)
        Format = "{0:t}";
      else if (Source.Year != RelativeDate.Year)
        Format = string.Format("{{0:{0}}}", YearMonthDayFormat);
      else if (Source.Month != RelativeDate.Month)
        Format = string.Format("{{0:{0}}}", MonthDayFormat);
      else
        Format = string.Format("{{0:{0}}}", DayFormat);

      return string.Format(Format, Source);
    }
    /// <summary>
    /// Formats a range formed by two <see cref="DateTimeOffset"/> instances for display.
    /// </summary>
    /// <param name="From">The starting <see cref="DateTimeOffset"/>.</param>
    /// <param name="Until">The ending <see cref="DateTimeOffset"/>.</param>
    /// <param name="VerboseSameDay">Whether output should be verbose when both range parameters represent the same day.</param>
    /// <returns>A <see cref="string"/> representing the date range.</returns>
    public static string FormatDateRange(DateTimeOffset From, DateTimeOffset Until, bool VerboseSameDay = false)
    {
      var SameDayFormat = (VerboseSameDay ? "D" : YearMonthDayFormat);
      return string.Format(From.Date == Until.Date ? string.Format("{{0:{0}}} {{0:t}} - {{1:t}} ({{0:%z}})", SameDayFormat) : string.Format("{0} until {{1:{1}}}", From.ToRelativeString(Until), YearMonthDayFormat), From, Until);
    }
    /// <summary>
    /// Determines if this <see cref="DateTimeOffset"/> is between the range of two <see cref="DateTimeOffset"/>.
    /// </summary>
    /// <param name="Source">This <see cref="DateTimeOffset"/>.</param>
    /// <param name="From">The starting <see cref="DateTimeOffset"/>.</param>
    /// <param name="Until">The ending <see cref="DateTimeOffset"/>.</param>
    /// <returns>True if this <see cref="DateTimeOffset"/> is between the calculated range.</returns>
    public static bool IsBetween(this DateTimeOffset Source, DateTimeOffset From, DateTimeOffset Until)
    {
      return Source >= From && Source <= Until;
    }
    /// <summary>
    /// Determines if the date of this DateTimeOffset is a weekend.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static bool IsWeekend(this DateTimeOffset Source) => Source.AsDate().IsWeekend();
    public static Inv.Date AsDate(this DateTimeOffset Source)
    {
      return new Inv.Date(Source.Date);
    }
    public static Inv.Date? AsDate(this DateTimeOffset? Source)
    {
      if (Source == null)
        return null;
      else
        return new Inv.Date(Source.Value.Date);
    }
    public static Inv.Time AsTime(this DateTimeOffset Source)
    {
      return new Inv.Time(Source.TimeOfDay);
    }
    public static Inv.Time? AsTime(this DateTimeOffset? Source)
    {
      if (Source == null)
        return null;
      else
        return new Inv.Time(Source.Value.TimeOfDay);
    }
    /// <summary>
    /// Round a <see cref="DateTimeOffset"/> up to the granularity specified by a <see cref="TimeSpan"/>.
    /// </summary>
    /// <param name="dt"></param>
    /// <param name="ts"></param>
    /// <returns></returns>
    public static DateTimeOffset RoundUp(this DateTimeOffset dt, TimeSpan ts)
    {
      return new DateTimeOffset((dt.Ticks + ts.Ticks - 1) / ts.Ticks * ts.Ticks, dt.Offset);
    }
    /// <summary>
    /// Round a <see cref="DateTimeOffset"/> down to the granularity specified by a <see cref="TimeSpan"/>.
    /// </summary>
    /// <param name="dt"></param>
    /// <param name="ts"></param>
    /// <returns></returns>
    public static DateTimeOffset RoundDown(this DateTimeOffset dt, TimeSpan ts)
    {
      return new DateTimeOffset(dt.Ticks / ts.Ticks * ts.Ticks, dt.Offset);
    }

    private static DateTime BruteForceNextValidTime(TimeZoneInfo Zone, DateTime Source, int Multiplier)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Zone.IsInvalidTime(Source), $"{nameof(Source)} must be an invalid time");

      var Result = Source;

      while (Result.AddHours(Multiplier) is var NextTime && Zone.IsInvalidTime(NextTime))
        Result = NextTime;

      while (Result.AddMinutes(Multiplier) is var NextTime && Zone.IsInvalidTime(NextTime))
        Result = NextTime;

      while (Result.AddSeconds(Multiplier) is var NextTime && Zone.IsInvalidTime(NextTime))
        Result = NextTime;

      while (Result.AddMilliseconds(Multiplier) is var NextTime && Zone.IsInvalidTime(NextTime))
        Result = NextTime;

      while (Result.AddTicks(Multiplier) is var NextTime && Zone.IsInvalidTime(NextTime))
        Result = NextTime;

      return Result.AddTicks(Multiplier);
    }

    private const string YearMonthDayFormat = "ddd, d MMM yyyy";
    private const string MonthDayFormat = "ddd, d MMM";
    private const string DayFormat = "ddd, d";
  }

  /// <summary>
  /// Enum extension methods.
  /// </summary>
  public static class EnumHelper
  {
    public static IEnumerable<T?> AsNullable<T>(this IEnumerable<T> Enumerable)
      where T : struct, System.Enum
    {
      return Enumerable.Select(E => (T?)E);
    }
    public static IEnumerable<T> GetEnumerable<T>()
      where T : struct, System.Enum
    {
      var EnumType = typeof(T);
      var EnumTypeInfo = EnumType.GetTypeInfo();

      if (EnumTypeInfo.IsGenericType && EnumTypeInfo.GetGenericTypeDefinition() == typeof(Nullable<>))
      {
        EnumType = EnumType.GenericTypeArguments[0];
        EnumTypeInfo = EnumType.GetTypeInfo();

        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(EnumTypeInfo.IsEnum, "Type must be an enum: {0}", EnumType.FullName);

        return Enum.GetValues(EnumType).Cast<T>();
      }
      else
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(EnumTypeInfo.IsEnum, "Type must be an enum: {0}", EnumType.FullName);

        return Enum.GetValues(EnumType).Cast<T>();
      }
    }
    public static T Parse<T>(string Value, bool IgnoreCase = false)
      where T : struct, System.Enum
    {
      return (T)Enum.Parse(typeof(T), Value, IgnoreCase);
    }
    public static T? ParseOrDefault<T>(string Value, T? Default = null, bool IgnoreCase = false)
      where T : struct, System.Enum
    {
      if (Enum.TryParse(Value, IgnoreCase, out T Result))
        return Result;
      else
        return Default;
    }
    public static T? ParseOrDefaultByName<T>(string Value, T? Default = null, bool IgnoreCase = false)
      where T : struct, System.Enum
    {
      var NameSet = GetEnumerable<T>().ToDictionary(E => E.ToString(), IgnoreCase ? StringComparer.CurrentCultureIgnoreCase : StringComparer.CurrentCulture);

      if (NameSet.ContainsKey(Value))
        return NameSet[Value];
      else
        return Default;
    }
    public static T Next<T>(T Value)
      where T : struct, System.Enum
    {
      var List = GetEnumerable<T>().ToList();

      var Index = List.IndexOf(Value) + 1;
      if (Index >= List.Count)
        Index = 0;

      return List[Index];
    }
    public static T Previous<T>(T Value)
      where T : struct, System.Enum
    {
      var List = GetEnumerable<T>().ToList();

      var Index = List.IndexOf(Value) - 1;
      if (Index < 0)
        Index = List.Count - 1;

      return List[Index];
    }

    public static Exception UnexpectedValueException<T>(T Value)
      where T : struct, System.Enum
    {
      return new Exception($"Unexpected {typeof(T).FullName} value: '{Value}'");
    }
    public static Exception UnexpectedValueException<T>(T? Value)
      where T : struct, System.Enum
    {
      return new Exception($"Unexpected {typeof(T).FullName} value: '{(Value != null ? Value.Value.ToString() : "<null>")}'");
    }
    public static T Min<T>()
      where T : struct, System.Enum
    {
      return Enum.GetValues(typeof(T)).Cast<T>().Min();
    }
    public static T Max<T>()
      where T : struct, System.Enum
    {
      return Enum.GetValues(typeof(T)).Cast<T>().Max();
    }
  }

  /// <summary>
  /// <see cref="Exception"/> extension methods.
  /// </summary>
  public static class ExceptionHelper
  {
    static ExceptionHelper()
    {
      PreserveMethodInfo = typeof(Exception).GetReflectionMethod("InternalPreserveStackTrace");
    }

    /// <summary>
    /// Compile the input exceptions into a new <see cref="AggregateException"/>.
    /// </summary>
    /// <param name="Exceptions"></param>
    /// <param name="Message"></param>
    /// <returns></returns>
    public static Exception Aggregate(this IEnumerable<Exception> Exceptions, string Message)
    {
      var ExceptionArray = Exceptions.ToArray();

      if (ExceptionArray.Length > 0)
      {
        ExceptionArray.ForEach(Exception => Exception.PreserveStackTrace());

        if (ExceptionArray.Length == 1)
          return ExceptionArray[0];
        else
          return new AggregateException(Message, ExceptionArray);
      }
      else
      {
        return null;
      }
    }
    /// <summary>
    /// Preserve the stack trace when catching first level exceptions.
    /// This is a workaround for a flaw in the .NET framework.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Exception"></param>
    /// <returns></returns>
    public static T Preserve<T>(this T Exception)
      where T : Exception
    {
      Exception.PreserveStackTrace();

      return Exception;
    }

    /// <summary>
    /// Create a combined message of the exception including inner exception messages.
    /// </summary>
    /// <param name="DescribeException"></param>
    /// <returns></returns>
    public static string Describe(this Exception DescribeException)
    {
      void BuildExceptionReport(List<string> StringList, Exception Exception)
      {
        // ignore exact duplicate messages.
        if (StringList.Count == 0 || StringList.Last() != Exception.Message)
          StringList.Add(Exception.Message);

        if (Exception is ReflectionTypeLoadException ReflectionTypeLoadException)
        {
          foreach (var LoaderSystemException in ReflectionTypeLoadException.LoaderExceptions)
            BuildExceptionReport(StringList, LoaderSystemException);

          return;
        }

        if (Exception is AggregateException AggregateException)
        {
          foreach (var InnerAggregateException in AggregateException.InnerExceptions)
            BuildExceptionReport(StringList, InnerAggregateException);

          return;
        }

        if (Exception.InnerException != null)
          BuildExceptionReport(StringList, Exception.InnerException);
      }

      var ResultList = new List<string>();

      if (DescribeException != null)
        BuildExceptionReport(ResultList, DescribeException);

      return ResultList.AsSeparatedText(" | ");
    }

    /// <summary>
    /// Create a text report of the exception including inner exceptions and full stack traces.
    /// </summary>
    /// <param name="ReportException"></param>
    /// <returns></returns>
    public static string AsReport(this Exception ReportException)
    {
      void BuildExceptionReport(StringBuilder StringBuilder, Exception Exception)
      {
        StringBuilder.AppendLine(Exception.GetType().FullName);
        StringBuilder.AppendLine(Exception.Message);
        StringBuilder.AppendLine();
        StringBuilder.Append(Exception.StackTrace ?? "");

        if (Exception is ReflectionTypeLoadException ReflectionTypeLoadException)
        {
          foreach (var LoaderSystemException in ReflectionTypeLoadException.LoaderExceptions)
          {
            StringBuilder.AppendLine();
            StringBuilder.AppendLine();
            BuildExceptionReport(StringBuilder, LoaderSystemException);
          }
        }
        else if (Exception is AggregateException AggregateException)
        {
          foreach (var InnerAggregateException in AggregateException.InnerExceptions)
          {
            StringBuilder.AppendLine();
            StringBuilder.AppendLine();
            BuildExceptionReport(StringBuilder, InnerAggregateException);
          }
        }
        else if (Exception.InnerException != null)
        {
          StringBuilder.AppendLine();
          StringBuilder.AppendLine();
          BuildExceptionReport(StringBuilder, Exception.InnerException);
        }
      }

      var ResultBuilder = new StringBuilder();

      if (ReportException != null)
        BuildExceptionReport(ResultBuilder, ReportException);

      return ResultBuilder.ToString();
    }

    private static void PreserveStackTrace(this Exception Exception)
    {
      // NOTE: mobile platforms may not have this internal method.
      if (Exception != null && PreserveMethodInfo != null)
        PreserveMethodInfo.Invoke(Exception, null);
    }

    private static readonly MethodInfo PreserveMethodInfo;
  }

  public static class FileHelper
  {
    public static void SetReadOnly(string FilePath, bool Value)
    {
      var Attributes = File.GetAttributes(FilePath);

      if (!Value && (Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
        File.SetAttributes(FilePath, Attributes & ~FileAttributes.ReadOnly);
      else if (Value && (Attributes & FileAttributes.ReadOnly) != FileAttributes.ReadOnly)
        File.SetAttributes(FilePath, Attributes | FileAttributes.ReadOnly);
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public static bool FileInUse(string FilePath)
    {
      if (!File.Exists(FilePath))
        return false;

      var Result = false;
      try
      {
        using (var FileStream = File.Open(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
          FileStream.Close();
      }
      catch (IOException)
      {
        Result = true;
      }

      return Result;
    }
    public static void Enter(string FilePath, Action InUseAction, Action DeniedAction, Action AccessAction, Action CompleteAction = null)
    {
      void InUse() => InUseAction?.Invoke();

      var IsInUse = false;
      try
      {
        IsInUse = FileInUse(FilePath);
      }
      catch (UnauthorizedAccessException)
      {
        DeniedAction?.Invoke();
        return;
      }

      if (!IsInUse)
      {
        try
        {
          AccessAction?.Invoke();
        }
        catch (UnauthorizedAccessException)
        {
          DeniedAction?.Invoke();
        }
        catch (IOException E)
        {
          if (E.IsSharingViolation())
            InUse();
          else
            throw;
        }
        catch (System.ComponentModel.Win32Exception E)
        {
          if (E.IsTransactionalConflict())
            InUse();
          else
            throw;
        }

        CompleteAction?.Invoke();
      }
      else
      {
        InUse();
      }
    }
  }

  /// <summary>
  /// IOException extension methods.
  /// </summary>
  public static class IOExceptionHelper
  {
    /// <summary>
    /// Determines whether this <see cref="IOException"/> was thrown because of a sharing violation (the file or directory is already in use).
    /// <para>See https://learn.microsoft.com/en-us/dotnet/standard/io/handling-io-errors#handling-ioexception for more information.</para>
    /// </summary>
    /// <param name="Exception"></param>
    /// <returns></returns>
    public static bool IsSharingViolation(this IOException Exception)
    {
      // Note: The bitwise algebra is trimming the HResult to the bottom 16 bits.

      return Exception != null && (Exception.HResult & ((1 << 16) - 1)) == 32;
    }
  }

  /// <summary>
  /// Win32ExceptionHelper extension methods.
  /// </summary>
  public static class Win32ExceptionHelper
  {
    /// <summary>
    /// Determines whether this <see cref="System.ComponentModel.Win32Exception"/> was thrown because of a transaction conflict (attempted to use a name that is reserved for use by another transaction).
    /// </summary>
    /// <param name="Exception"></param>
    /// <returns></returns>
    public static bool IsTransactionalConflict(this System.ComponentModel.Win32Exception Exception)
    {
      return Exception != null && Exception.NativeErrorCode == 0x1A90;
    }
  }

  /// <summary>
  /// Inv.Time helper methods.
  /// </summary>
  public static class TimeHelper
  {
    /// <summary>
    /// Determines whether the two provided time periods overlap with each other.
    /// </summary>
    /// <param name="FromTimeA">The starting time of the first time period.</param>
    /// <param name="UntilTimeA">The finish time of the first time period.</param>
    /// <param name="FromTimeB">The starting time of the period to test for overlapping.</param>
    /// <param name="UntilTimeB">The finish time of the period to test for overlapping.</param>
    /// <returns></returns>
    public static bool OverlapsMinutes(Inv.Time FromTimeA, Inv.Time UntilTimeA, Inv.Time FromTimeB, Inv.Time UntilTimeB)
    {
      var NormalFromTimeA = FromTimeA.StartOfMinute();
      var NormalUntilTimeA = UntilTimeA.StartOfMinute();
      var NormalFromTimeB = FromTimeB.StartOfMinute();
      var NormalUntilTimeB = UntilTimeB.StartOfMinute();

      if (NormalUntilTimeB == Inv.Time.Midnight)
        return NormalUntilTimeA == Inv.Time.Midnight || NormalUntilTimeA > NormalFromTimeB;

      if (NormalUntilTimeA == Inv.Time.Midnight)
        return NormalUntilTimeB > NormalFromTimeA;

      return NormalUntilTimeB > NormalFromTimeA && NormalUntilTimeA > NormalFromTimeB;
    }
    /// <summary>
    /// Determines whether the time period calculated as Time + Duration overlaps with the provided time range.
    /// </summary>
    /// <param name="Time">The starting time of the time period.</param>
    /// <param name="Duration">The duration of the time period.</param>
    /// <param name="FromTime">The starting time of the period to test for overlapping.</param>
    /// <param name="UntilTime">The finish time of the period to test for overlapping.</param>
    /// <returns></returns>
    public static bool OverlapsMinutes(Inv.Time Time, TimeSpan Duration, Inv.Time FromTime, Inv.Time UntilTime)
    {
      var BaseDate = Inv.Date.Today;

      // Convert to DateTime so that if Time + Duration overflows Inv.Time, we can truncate to midnight.
      var TestUntilDateTime = BaseDate + Time + Duration;

      return OverlapsMinutes(Time, TestUntilDateTime.AsDate() == BaseDate ? TestUntilDateTime.AsTime() : Inv.Time.Midnight, FromTime, UntilTime);
    }
  }

  /// <summary>
  /// <see cref="TimeSpan"/> extension methods.
  /// </summary>
  public static class TimeSpanHelper
  {
    /// <summary>
    /// Truncates the millisecond field from this <see cref="TimeSpan"/>.
    /// </summary>
    /// <param name="Source">This <see cref="TimeSpan"/>.</param>
    /// <returns>A <see cref="TimeSpan"/> object equivalent to this <see cref="TimeSpan"/> with the millisecond field zeroed out.</returns>
    public static TimeSpan TruncateMilliseconds(this TimeSpan Source)
    {
      return new TimeSpan(Source.Days, Source.Hours, Source.Minutes, Source.Seconds, 0);
    }
    /// <summary>
    /// Truncates the second field from this <see cref="TimeSpan"/>.
    /// </summary>
    /// <param name="Source">This <see cref="TimeSpan"/>.</param>
    /// <returns>A <see cref="TimeSpan"/> object equivalent to this <see cref="TimeSpan"/> with the second field zeroed out.</returns>
    public static TimeSpan TruncateSeconds(this TimeSpan Source)
    {
      return new TimeSpan(Source.Days, Source.Hours, Source.Minutes, 0);
    }
    /// <summary>
    /// Format this <see cref="TimeSpan"/> into a human-readable string.
    /// </summary>
    /// <param name="Source">This <see cref="TimeSpan"/>.</param>
    /// <returns>A human-readable string representing this <see cref="TimeSpan"/>.</returns>
    public static string FormatTimeSpan(this TimeSpan Source)
    {
      string Result;

      if (Source < new TimeSpan(0, 0, 0, 1))
        Result = Source.Milliseconds + " ms";
      else if (Source < new TimeSpan(0, 1, 0))
        Result = Source.Seconds + " secs";
      else if (Source < new TimeSpan(1, 0, 0))
        Result = Source.Minutes + " mins";
      else if (Source < new TimeSpan(1, 0, 0, 0))
        Result = Source.Hours + " hrs";
      else
        Result = Source.Days + " days";

      return Result;
    }
    public static string FormatTimeSpanShort(this TimeSpan Source, bool HideFractions = false)
    {
      string Result;
      var Decimals = HideFractions ? 0 : 1;

      var Negative = Source.Ticks < 0;
      if (Negative)
        Source = TimeSpan.FromTicks(Math.Abs(Source.Ticks));

      if (Source < new TimeSpan(0, 0, 0, 1))
        Result = Math.Round(Source.TotalMilliseconds, Decimals) + " ms";
      else if (Source < new TimeSpan(0, 1, 0))
      {
        var Seconds = Math.Round(Source.TotalSeconds, Decimals);
        Result = Seconds + " sec".Plural((decimal)Seconds);
      }
      else if (Source < new TimeSpan(1, 0, 0))
      {
        var Minutes = Math.Round(Source.TotalMinutes, Decimals);
        Result = Minutes + " min".Plural((decimal)Minutes);
      }
      else if (Source < new TimeSpan(1, 0, 0, 0))
      {
        var Hours = Math.Round(Source.TotalHours, Decimals);
        Result = Hours + " hr".Plural((decimal)Hours);
      }
      else if (Source.Days < 365)
      {
        var Days = Math.Round(Source.TotalDays, Decimals);
        Result = Days + " day".Plural((decimal)Days);
      }
      else
      {
        var Years = Math.Round(Source.TotalDays / 365.0, Decimals);
        Result = Years + " year".Plural((decimal)Years);
      }

      if (Negative)
        Result = "-" + Result;

      return Result;
    }
    public static string FormatTimeSpanMedium(this TimeSpan Source)
    {
      string Result;

      if (Source < new TimeSpan(0, 1, 0))
      {
        if (Source < new TimeSpan(0, 0, 1))
          Result = "0 secs";
        else
          Result = Source.Seconds + " sec".Plural(Source.Seconds);
      }
      else if (Source < new TimeSpan(1, 0, 0))
      {
        Result = Source.Minutes + " min".Plural(Source.Minutes);

        if (Source.Seconds > 0)
          Result += " " + Source.Seconds + " sec".Plural(Source.Seconds);
      }
      else if (Source < new TimeSpan(1, 0, 0, 0))
      {
        Result = Source.Hours + " hr".Plural(Source.Hours);

        if (Source.Minutes > 0)
          Result += " " + Source.Minutes + " min".Plural(Source.Minutes);
      }
      else if (Source.Days < 365)
      {
        Result = Source.Days + " day".Plural(Source.Days);

        if (Source.Hours > 0)
          Result += " " + Source.Hours + " hr".Plural(Source.Hours);
      }
      else
      {
        var Years = Math.Floor(Source.Days / 365.0);
        Result = Years + " year".Plural((int)Years);

        var RemainingDays = Source.Days - Years * 365;
        if (RemainingDays > 0)
          Result += " " + RemainingDays + " day".Plural((int)RemainingDays);
      }

      return Result;
    }
    public static string FormatTimeSpanLong(this TimeSpan Source)
    {
      string Result;

      if (Source < new TimeSpan(0, 1, 0))
      {
        if (Source < new TimeSpan(0, 0, 1))
          Result = "0 secs";
        else
          Result = Source.Seconds + " sec".Plural(Source.Seconds);
      }
      else if (Source < new TimeSpan(1, 0, 0))
      {
        Result = Source.Minutes + " min".Plural(Source.Minutes);

        if (Source.Seconds > 0)
          Result += " " + Source.Seconds + " sec".Plural(Source.Seconds);
      }
      else if (Source < new TimeSpan(1, 0, 0, 0))
      {
        Result = Source.Hours + " hr".Plural(Source.Hours);

        if (Source.Minutes > 0)
          Result += " " + Source.Minutes + " min".Plural(Source.Minutes);
      }
      else if (Source.Days < 365)
      {
        Result = Source.Days + " day".Plural(Source.Days);

        if (Source.Hours > 0)
          Result += " " + Source.Hours + " hr".Plural(Source.Hours);

        if (Source.Minutes > 0)
          Result += " " + Source.Minutes + " min".Plural(Source.Minutes);
      }
      else
      {
        var Years = Math.Floor(Source.Days / 365.0);
        Result = Years + " year".Plural((int)Years);

        var RemainingDays = Source.Days - Years * 365;
        if (RemainingDays > 0)
          Result += " " + RemainingDays + " day".Plural((int)RemainingDays);

        if (Source.Hours > 0)
          Result += " " + Source.Hours + " hr".Plural(Source.Hours);

        if (Source.Minutes > 0)
          Result += " " + Source.Minutes + " min".Plural(Source.Minutes);
      }

      return Result;
    }
    public static TimeSpan Max(this TimeSpan Left, TimeSpan Right)
    {
      if (Left > Right)
        return Left;
      else
        return Right;
    }
    public static TimeSpan Min(this TimeSpan Left, TimeSpan Right)
    {
      if (Left < Right)
        return Left;
      else
        return Right;
    }
    public static decimal FractionalNumberOfIncludedPeriods(this TimeSpan TimeSpan, long PeriodInMinutes)
    {
      return (decimal)(TimeSpan.TotalMinutes / PeriodInMinutes);
    }
    /// <summary>
    /// Returns true if <paramref name="Source"/> is greater than <paramref name="Lower"/> and less than <paramref name="Upper"/>.
    /// </summary>
    /// <returns>Returns true if <paramref name="Source"/> is between <paramref name="Lower"/> and <paramref name="Upper"/>, otherwise returns false</returns>
    public static bool BetweenExclusive(this TimeSpan Source, TimeSpan Lower, TimeSpan Upper)
    {
      return Source > Lower && Source < Upper;
    }
    public static bool BetweenInclusive(this TimeSpan Source, TimeSpan Lower, TimeSpan Upper)
    {
      return Source >= Lower && Source <= Upper;
    }
    /// <summary>
    /// Returns a timespan from the number of days provided. If the number of days exceeeds the maximum number, then the timespan returned is the one with the maximum number of days.
    /// </summary>
    public static TimeSpan FromDays(long Days)
    {
      var MaxDaysValue = TimeSpan.MaxValue.Days;
      return new TimeSpan(Days > MaxDaysValue ? MaxDaysValue : (int)Days, 0, 0, 0);
    }
  }

  public static class ComparableHelper
  {
    public static int CompareTo<T>(this T? A, T? B)
      where T : struct, IComparable
    {
      var Result = (A != null).CompareTo(B != null);

      if (Result == 0 && A != null && B != null)
        Result = A.Value.CompareTo(B.Value);

      return Result;
    }
  }

  /// <summary>
  /// Extension helpers for <see cref="char"/>.
  /// </summary>
  public static class CharHelper
  {
    /// <summary>
    /// Ask if the character is a numeric digit (0-9).
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static bool IsNumeric(this char Source)
    {
      return Source >= '0' && Source <= '9';
    }
    /// <summary>
    /// Ask if the character is in the 26-letter alphabet (a-zA-Z).
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static bool IsAlphabetic(this char Source)
    {
      return (Source >= 'a' && Source <= 'z') || (Source >= 'A' && Source <= 'Z');
    }
    /// <summary>
    /// Ask if the character is a vowel (a, e, i, o, u).
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static bool IsVowel(this char Source)
    {
      var LowerSource = char.ToLower(Source);

      return LowerSource == 'a' || LowerSource == 'e' || LowerSource == 'i' || LowerSource == 'o' || LowerSource == 'u';
    }
    public static string ToStringInvariant(this char Source)
    {
      if (Source == '\a')
        return @"\a";

      if (Source == '\b')
        return @"\b";

      if (Source == '\r')
        return @"\r";

      if (Source == '\n')
        return @"\n";

      if (Source == '\t')
        return @"\t";

      if (Source == '\v')
        return @"\v";

      if (Source == '\f')
        return @"\f";

      if (Source == '\0')
        return @"\0";

      return Source < ' ' ? $@"\u{((int)Source).ToString(System.Globalization.CultureInfo.InvariantCulture)}" : Source.ToString(System.Globalization.CultureInfo.InvariantCulture);
    }
  }

  /// <summary>
  /// Extension helpers for <see cref="Dictionary{TKey, TValue}"/>
  /// </summary>
  public static class DictionaryHelper
  {
    public static TValue GetValueOrNull<TKey, TValue>(this Dictionary<TKey, TValue> Dictionary, TKey Key)
      where TValue : class
    {
      if (Dictionary.TryGetValue(Key, out var ResultValue))
        return ResultValue;
      else
        return null;
    }
    public static TValue GetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> Dictionary, TKey Key, TValue DefaultValue = default)
    {
      if (Dictionary.TryGetValue(Key, out var ResultValue))
        return ResultValue;
      else
        return DefaultValue;
    }
    public static TValue RemoveValueOrNull<TKey, TValue>(this Dictionary<TKey, TValue> Dictionary, TKey Key)
      where TValue : class
    {
      if (Dictionary.TryGetValue(Key, out var ResultValue))
      {
        Dictionary.Remove(Key);

        return ResultValue;
      }
      else
      {
        return null;
      }
    }
    public static TValue RemoveValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> Dictionary, TKey Key, TValue DefaultValue = default)
    {
      if (Dictionary.TryGetValue(Key, out var ResultValue))
      {
        Dictionary.Remove(Key);

        return ResultValue;
      }
      else
      {
        return DefaultValue;
      }
    }
    public static void RemoveAll<TKey, TValue>(this Dictionary<TKey, TValue> Dictionary, Func<KeyValuePair<TKey, TValue>, bool> Function)
    {
      var RemoveKeyArray = Dictionary.Where(Function).Select(Index => Index.Key).ToArray();

      if (RemoveKeyArray.Any(RemoveKey => !Dictionary.Remove(RemoveKey)))
        throw new Exception("RemoveAll key equality failure.");
    }

    public static TValue GetOrAdd<TKey, TValue>(this Dictionary<TKey, TValue> Dictionary, TKey Key, Func<TKey, TValue> ValueFunction)
    {
      if (!Dictionary.TryGetValue(Key, out var Result))
      {
        Result = ValueFunction(Key);
        Dictionary.Add(Key, Result);
      }

      return Result;
    }
  }

  /// <summary>
  /// Extension helpers for <see cref="IReadOnlyDictionary{TKey, TValue}"/>
  /// </summary>
  public static class ReadOnlyDictionaryHelper
  {
    public static TValue GetValueOrDefault<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> Dictionary, TKey Key, TValue DefaultValue = default)
    {
      if (Dictionary.TryGetValue(Key, out var ResultValue))
        return ResultValue;
      else
        return DefaultValue;
    }
  }

  /// <summary>
  /// Static class with extension methods for <see cref="IEnumerable{T}"/>.
  /// </summary>
  public static class IEnumerableHelper
  {
    public static IEnumerable<T> Convert<T>(this System.Collections.IEnumerable Enumerable)
    {
      var enumerator = Enumerable.GetEnumerator();
      while (enumerator.MoveNext())
        yield return (T)enumerator.Current;
    }
    public static void ForEachOfType<TCast>(this System.Collections.IEnumerable Enumerable, Action<TCast> Action)
      where TCast : class
    {
      foreach (var Item in Enumerable.OfType<TCast>())
        Action(Item);
    }
    public static bool Exists<T>(this IEnumerable<T> Source, Predicate<T> Predicate)
    {
      return Source.Any(Item => Predicate(Item));
    }
    /// <summary>
    /// Determines whether a sequence is empty or contains a specified element by using the default equality comparer.
    /// </summary>
    /// <typeparam name="T">The type of the elements of source.</typeparam>
    /// <param name="Source">A sequence in which to locate a value.</param>
    /// <param name="Value">The value to locate in the sequence.</param>
    /// <returns>true if the source sequence is empty or contains an element that has the specified value; otherwise, false</returns>
    public static bool IsRestrict<T>(this IEnumerable<T> Source, T Value) => !Source.Any() || Source.Contains(Value);
    /// <summary>
    /// Determines whether a sequence is empty or contains any of the specified elements by using the default equality comparer.
    /// </summary>
    /// <typeparam name="T">The type of the elements of source.</typeparam>
    /// <param name="Source">A sequence in which to locate a value.</param>
    /// <param name="Value">A sequence of values to locate in the source sequence.</param>
    /// <returns>true if the source sequence is empty or contains an element that has any of the specified values; otherwise, false</returns>
    public static bool IsRestrictAny<T>(this IEnumerable<T> Source, IEnumerable<T> Value) => !Source.Any() || Source.ContainsAny(Value);
    public static IEnumerable<T> Except<T, TValue>(this IEnumerable<T> Source, IEnumerable<T> Target, Func<T, TValue> Function)
    {
      return Source.Except(Target, new ProjectionEqualityComparer<T, TValue>(Function));
    }
    public static IEnumerable<T> Except<T>(this IEnumerable<T> Source, params T[] Targets)
    {
      return Source.Except(Targets.AsEnumerable());
    }
    public static IEnumerable<T> Intersect<T, TValue>(this IEnumerable<T> Source, IEnumerable<T> Target, Func<T, TValue> Function)
    {
      return Source.Intersect(Target, new ProjectionEqualityComparer<T, TValue>(Function));
    }
    public static IEnumerable<T> Intersect<T, TValue>(this IEnumerable<T> Source, IEnumerable<T> Target, Func<T, T, bool> Function)
    {
      return Source.Intersect(Target, new ProjectionEqualityComparer<T>(Function));
    }
    public static IEnumerable<T> Intersect<T>(this IEnumerable<T> Source, params T[] Targets)
    {
      return Source.Intersect(Targets.AsEnumerable());
    }
    public static IEnumerable<T> Union<T, TValue>(this IEnumerable<T> Source, IEnumerable<T> Target, Func<T, TValue> Function)
    {
      return Source.Union(Target, new ProjectionEqualityComparer<T, TValue>(Function));
    }
    /// <summary>
    /// Filters a sequence of values down to non-null values.
    /// </summary>
    /// <typeparam name="T">The type of the elements of <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <returns>An <see cref="IEnumerable{T}"/> containing all of the non-null elements from <paramref name="Source"/>.</returns>
    public static IEnumerable<T> ExceptNull<T>(this IEnumerable<T?> Source)
      where T : struct
    {
      return System.Linq.Enumerable.Where(Source, Item => Item != null).Select(Item => Item.Value);
    }
    /// <summary>
    /// Filters a sequence of values down to non-null values.
    /// </summary>
    /// <typeparam name="T">The type of the elements of <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <returns>An <see cref="IEnumerable{T}"/> containing all of the non-null elements from <paramref name="Source"/>.</returns>
    public static IEnumerable<T> ExceptNull<T>(this IEnumerable<T> Source)
      where T : class
    {
      return (IEnumerable<T>)System.Linq.Enumerable.Where(Source, Item => Item != null);
    }
    public static IEnumerable<T> ExceptLast<T>(this IEnumerable<T> Source)
    {
      return Source.Take(Source.Count() - 1);
    }
    /// <summary>
    /// Inverts the order of the elements in a sequence.
    /// <para>Note: This is only present because List reintroduces Reverse() to physically reorder the list.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static IEnumerable<T> ReverseX<T>(this IEnumerable<T> Source)
    {
      return Source.Reverse();
    }
    /// <summary>
    /// Map the elements of this <see cref="IEnumerable{TKey}"/> into a <see cref="HashSet{TValue}"/>using the specified delegate to derive values.
    /// </summary>
    /// <typeparam name="TKey">The type of the elements of <see cref="IEnumerable{TKey}"/>.</typeparam>
    /// <typeparam name="TValue">The type of derived values.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{TKey}"/>.</param>
    /// <param name="Function">A delegate that will generate values to store for each element in the <see cref="IEnumerable{TKey}"/>.</param>
    /// <returns>The generated <see cref="HashSet{TValue}"/>.</returns>
    public static HashSet<TValue> ToHashSet<TKey, TValue>(this IEnumerable<TKey> Source, Func<TKey, TValue> Function)
    {
      var ResultHashSet = new HashSet<TValue>();

      foreach (var SourceEntry in Source)
        ResultHashSet.Add(Function(SourceEntry));

      return ResultHashSet;
    }
    /// <summary>
    /// Map the elements of this <see cref="IEnumerable{TKey}"/> into a <see cref="HashSet{TValue}"/>using the specified delegate to derive values.
    /// </summary>
    /// <typeparam name="TKey">The type of the elements of <see cref="IEnumerable{TKey}"/>.</typeparam>
    /// <typeparam name="TValue">The type of derived values.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{TKey}"/>.</param>
    /// <param name="Function">A delegate that will generate values to store for each element in the <see cref="IEnumerable{TKey}"/>.</param>
    /// <param name="Comparer">IEqualityComparer to use.</param>
    /// <returns>The generated <see cref="HashSet{TValue}"/>.</returns>
    public static HashSet<TValue> ToHashSet<TKey, TValue>(this IEnumerable<TKey> Source, Func<TKey, TValue> Function, IEqualityComparer<TValue> Comparer)
    {
      var ResultHashSet = new HashSet<TValue>(Comparer);

      foreach (var SourceEntry in Source)
        ResultHashSet.Add(Function(SourceEntry));

      return ResultHashSet;
    }
    /// <summary>
    /// Convert the elements of this <see cref="IEnumerable{TValue}"/> into a <see cref="HashSet{TValue}"/>.
    /// NOTE: This method is named ToHashSetX to avoid collisions with the System.Linq.Enumerable extension method 'ToHashSet' which was introduced after .NET 4.5.0.
    /// </summary>
    /// <typeparam name="TValue">The type of the elements of this <see cref="IEnumerable{TKey}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{TKey}"/>.</param>
    /// <returns>The generated <see cref="HashSet{TValue}"/>.</returns>
    public static HashSet<TValue> ToHashSetX<TValue>(this IEnumerable<TValue> Source)
    {
      return new HashSet<TValue>(Source);
    }
    /// <summary>
    /// Convert the elements of this <see cref="IEnumerable{TValue}"/> into a <see cref="HashSet{TValue}"/>.
    /// NOTE: This method is named 'ToHashSetX' to avoid collisions with the System.Linq.Enumerable extension method 'ToHashSet' which was introduced after .NET 4.5.0.
    /// </summary>
    /// <typeparam name="TValue">The type of the elements of this <see cref="IEnumerable{TKey}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{TKey}"/>.</param>
    /// <param name="Comparer">Comparer <see cref="IEnumerable{TKey}"/>.</param>
    /// <returns>The generated <see cref="HashSet{TValue}"/>.</returns>
    public static HashSet<TValue> ToHashSetX<TValue>(this IEnumerable<TValue> Source, IEqualityComparer<TValue> Comparer)
    {
      return new HashSet<TValue>(Source, Comparer);
    }
    /// <summary>
    /// Trim from the start and the end of this <see cref="IEnumerable{T}"/> according to the specified function.
    /// </summary>
    /// <typeparam name="T">The type of elements in the <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Function">The function determining where to start and stop trimming.</param>
    /// <returns>The elements of this <see cref="IEnumerable{T}"/> from the first element for which <paramref name="Function"/> returns false to the last element for which
    /// <paramref name="Function"/> returns false.</returns>
    public static IEnumerable<T> Trim<T>(this IEnumerable<T> Source, Func<T, bool> Function)
    {
      return Source.TrimStart(Function).TrimEnd(Function);
    }
    /// <summary>
    /// Trim elements from the start of this <see cref="IEnumerable{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Function">The function determining which element to stop trimming at.</param>
    /// <returns>The elements of this <see cref="IEnumerable{T}"/> from the first element for which <paramref name="Function"/> returns false until the
    /// end of this <see cref="IEnumerable{T}"/></returns>
    public static IEnumerable<T> TrimStart<T>(this IEnumerable<T> Source, Func<T, bool> Function)
    {
      var Result = new List<T>();
      var Matching = true;

      foreach (var Entry in Source)
      {
        if (Matching)
          Matching = Function(Entry);

        if (!Matching)
          Result.Add(Entry);
      }

      return Result;
    }
    /// <summary>
    /// Trim elements from the end of this <see cref="IEnumerable{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Function">The function determining which element to start trimming at.</param>
    /// <returns>The elements of this <see cref="IEnumerable{T}"/> from the first element until the last element for which <paramref name="Function"/> returns false.</returns>
    public static IEnumerable<T> TrimEnd<T>(this IEnumerable<T> Source, Func<T, bool> Function)
    {
      var Result = new List<T>();
      var Window = new List<T>();
      var Matching = false;

      foreach (var Entry in Source)
      {
        Matching = Function(Entry);

        if (Matching)
        {
          Window.Add(Entry);
        }
        else
        {
          Result.AddRange(Window);
          Result.Add(Entry);

          Window.Clear();
        }
      }

      if (!Matching)
        Result.AddRange(Window);

      return Result;
    }
    /// <summary>
    /// Find the first element of this <see cref="IEnumerable{T}"/> which satisfies the matching function <paramref name="Function"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Function">The matching delegate which will return true if an element matches.</param>
    /// <returns>The first element of this <see cref="IEnumerable{T}"/> for which <paramref name="Function"/> returns true, or the default value if no match is found.</returns>
    public static T Find<T>(this IEnumerable<T> Source, Func<T, bool> Function)
    {
      return Source.Where(Function).FirstOrDefault();
    }
    /// <summary>
    /// Returns the minimum value in a sequence of System.Int32 values.
    /// </summary>
    /// <param name="source">A sequence of System.Int32 values to determine the minimum value of.</param>
    /// <returns>The minimum  value in the sequence, or null if the sequence is empty.</returns>
    public static int? MinOrNull(this IEnumerable<int> source)
    {
      return source.Any() ? source.Min() : null as int?;
    }
    /// <summary>
    /// Invokes a transform function on each element of a sequence and returns the minimum  System.Int32 value.
    /// </summary>
    /// <param name="source">A sequence of values to determine the minimum  value of.</param>
    /// <param name="ProjectQuery">A transform function to apply to each element.</param>
    /// <typeparam name="T">The type of the elements of source.</typeparam>
    /// <returns>The minimum  value in the sequence, or null if the sequence is empty.</returns>
    public static int? MinOrNull<T>(this IEnumerable<T> source, Func<T, int> ProjectQuery)
    {
      return source.Any() ? source.Min(ProjectQuery) : null as int?;
    }
    /// <summary>
    /// Returns the maximum value in a sequence of System.Int32 values.
    /// </summary>
    /// <param name="source">A sequence of System.Int32 values to determine the maximum value of.</param>
    /// <returns>The maximum value in the sequence, or null if the sequence is empty.</returns>
    public static int? MaxOrNull(this IEnumerable<int> source)
    {
      return source.Any() ? source.Max() : null as int?;
    }
    /// <summary>
    /// Invokes a transform function on each element of a sequence and returns the maximum System.Int32 value.
    /// </summary>
    /// <param name="source">A sequence of values to determine the maximum value of.</param>
    /// <param name="ProjectQuery">A transform function to apply to each element.</param>
    /// <typeparam name="T">The type of the elements of source.</typeparam>
    /// <returns>The maximum value in the sequence, or null if the sequence is empty.</returns>
    public static int? MaxOrNull<T>(this IEnumerable<T> source, Func<T, int> ProjectQuery)
    {
      return source.Any() ? source.Max(ProjectQuery) : null as int?;
    }
    /// <summary>
    /// Returns the minimum value in a sequence of System.Int64 values.
    /// </summary>
    /// <param name="source">A sequence of System.Int64 values to determine the minimum value of.</param>
    /// <returns>The minimum  value in the sequence, or null if the sequence is empty.</returns>
    public static long? MinOrNull(this IEnumerable<long> source)
    {
      return source.Any() ? source.Min() : null as long?;
    }
    /// <summary>
    /// Invokes a transform function on each element of a sequence and returns the minimum  System.Int64 value.
    /// </summary>
    /// <param name="source">A sequence of values to determine the minimum  value of.</param>
    /// <param name="ProjectQuery">A transform function to apply to each element.</param>
    /// <typeparam name="T">The type of the elements of source.</typeparam>
    /// <returns>The minimum  value in the sequence, or null if the sequence is empty.</returns>
    public static long? MinOrNull<T>(this IEnumerable<T> source, Func<T, long> ProjectQuery)
    {
      return source.Any() ? source.Min(ProjectQuery) : null as long?;
    }
    /// <summary>
    /// Returns the maximum value in a sequence of System.Int64 values.
    /// </summary>
    /// <param name="source">A sequence of System.Int64 values to determine the maximum value of.</param>
    /// <returns>The maximum value in the sequence, or null if the sequence is empty.</returns>
    public static long? MaxOrNull(this IEnumerable<long> source)
    {
      return source.Any() ? source.Max() : null as long?;
    }
    /// <summary>
    /// Invokes a transform function on each element of a sequence and returns the maximum System.Int64 value.
    /// </summary>
    /// <param name="source">A sequence of values to determine the maximum value of.</param>
    /// <param name="ProjectQuery">A transform function to apply to each element.</param>
    /// <typeparam name="T">The type of the elements of source.</typeparam>
    /// <returns>The maximum value in the sequence, or null if the sequence is empty.</returns>
    public static long? MaxOrNull<T>(this IEnumerable<T> source, Func<T, long> ProjectQuery)
    {
      return source.Any() ? source.Max(ProjectQuery) : null as long?;
    }
    /// <summary>
    /// Returns the minimum value in a sequence of Inv.Date values.
    /// </summary>
    /// <param name="source">A sequence of Inv.Date values to determine the minimum value of.</param>
    /// <returns>The minimum value in the sequence, or null if the sequence is empty.</returns>
    public static Inv.Date? MinOrNull(this IEnumerable<Inv.Date> source)
    {
      return source.Any() ? source.Min() : null as Inv.Date?;
    }
    /// <summary>
    /// Invokes a transform function on each element of a sequence and returns the minimum Inv.Date value.
    /// </summary>
    /// <param name="source">A sequence of values to determine the minimum  value of.</param>
    /// <param name="ProjectQuery">A transform function to apply to each element.</param>
    /// <typeparam name="T">The type of the elements of source.</typeparam>
    /// <returns>The minimum value in the sequence, or null if the sequence is empty.</returns>
    public static Inv.Date? MinOrNull<T>(this IEnumerable<T> source, Func<T, Inv.Date> ProjectQuery)
    {
      return source.Any() ? source.Min(ProjectQuery) : null as Inv.Date?;
    }
    /// <summary>
    /// Returns the maximum value in a sequence of Inv.Date values.
    /// </summary>
    /// <param name="source">A sequence of Inv.Date values to determine the maximum value of.</param>
    /// <returns>The maximum value in the sequence, or null if the sequence is empty.</returns>
    public static Inv.Date? MaxOrNull(this IEnumerable<Inv.Date> source)
    {
      return source.Any() ? source.Max() : null as Inv.Date?;
    }
    /// <summary>
    /// Returns the maximum value in a sequence of Inv.Date values.
    /// </summary>
    /// <param name="source">A sequence of Inv.Date values to determine the maximum value of.</param>
    /// <param name="ProjectQuery">A transform function to apply to each element.</param>
    /// <returns>The maximum value in the sequence, or null if the sequence is empty.</returns>
    public static Inv.Date? MaxOrNull<T>(this IEnumerable<T> source, Func<T, Inv.Date> ProjectQuery)
    {
      return source.Any() ? source.Max(ProjectQuery) : null as Inv.Date?;
    }
    /// <summary>
    /// Returns the minimum value in a sequence of DateTime values.
    /// </summary>
    /// <param name="source">A sequence of DateTime values to determine the minimum value of.</param>
    /// <returns>The minimum value in the sequence, or null if the sequence is empty.</returns>
    public static DateTime? MinOrNull(this IEnumerable<DateTime> source)
    {
      return source.Any() ? source.Min() : null as DateTime?;
    }
    /// <summary>
    /// Invokes a transform function on each element of a sequence and returns the minimum DateTime value.
    /// </summary>
    /// <param name="source">A sequence of values to determine the minimum  value of.</param>
    /// <param name="ProjectQuery">A transform function to apply to each element.</param>
    /// <typeparam name="T">The type of the elements of source.</typeparam>
    /// <returns>The minimum value in the sequence, or null if the sequence is empty.</returns>
    public static DateTime? MinOrNull<T>(this IEnumerable<T> source, Func<T, DateTime> ProjectQuery)
    {
      return source.Any() ? source.Min(ProjectQuery) : null as DateTime?;
    }
    /// <summary>
    /// Returns the maximum value in a sequence of DateTime values.
    /// </summary>
    /// <param name="source">A sequence of DateTime values to determine the maximum value of.</param>
    /// <returns>The maximum value in the sequence, or null if the sequence is empty.</returns>
    public static DateTime? MaxOrNull(this IEnumerable<DateTime> source)
    {
      return source.Any() ? source.Max() : null as DateTime?;
    }
    /// <summary>
    /// Returns the maximum value in a sequence of DateTime values.
    /// </summary>
    /// <param name="source">A sequence of DateTime values to determine the maximum value of.</param>
    /// <param name="ProjectQuery">A transform function to apply to each element.</param>
    /// <returns>The maximum value in the sequence, or null if the sequence is empty.</returns>
    public static DateTime? MaxOrNull<T>(this IEnumerable<T> source, Func<T, DateTime> ProjectQuery)
    {
      return source.Any() ? source.Max(ProjectQuery) : null as DateTime?;
    }
    /// <summary>
    /// Returns the minimum value in a sequence of DateTimeOffset values.
    /// </summary>
    /// <param name="source">A sequence of DateTimeOffset values to determine the minimum value of.</param>
    /// <returns>The minimum value in the sequence, or null if the sequence is empty.</returns>
    public static DateTimeOffset? MinOrNull(this IEnumerable<DateTimeOffset> source)
    {
      return source.Any() ? source.Min() : null as DateTimeOffset?;
    }
    /// <summary>
    /// Invokes a transform function on each element of a sequence and returns the minimum DateTimeOffset value.
    /// </summary>
    /// <param name="source">A sequence of values to determine the minimum  value of.</param>
    /// <param name="ProjectQuery">A transform function to apply to each element.</param>
    /// <typeparam name="T">The type of the elements of source.</typeparam>
    /// <returns>The minimum value in the sequence, or null if the sequence is empty.</returns>
    public static DateTimeOffset? MinOrNull<T>(this IEnumerable<T> source, Func<T, DateTimeOffset> ProjectQuery)
    {
      return source.Any() ? source.Min(ProjectQuery) : null as DateTimeOffset?;
    }
    /// <summary>
    /// Returns the maximum value in a sequence of DateTimeOffset values.
    /// </summary>
    /// <param name="source">A sequence of DateTimeOffset values to determine the maximum value of.</param>
    /// <returns>The maximum value in the sequence, or null if the sequence is empty.</returns>
    public static DateTimeOffset? MaxOrNull(this IEnumerable<DateTimeOffset> source)
    {
      return source.Any() ? source.Max() : null as DateTimeOffset?;
    }
    /// <summary>
    /// Returns the maximum value in a sequence of DateTimeOffset values.
    /// </summary>
    /// <param name="source">A sequence of DateTimeOffset values to determine the maximum value of.</param>
    /// <param name="ProjectQuery">A transform function to apply to each element.</param>
    /// <returns>The maximum value in the sequence, or null if the sequence is empty.</returns>
    public static DateTimeOffset? MaxOrNull<T>(this IEnumerable<T> source, Func<T, DateTimeOffset> ProjectQuery)
    {
      return source.Any() ? source.Max(ProjectQuery) : null as DateTimeOffset?;
    }
    /// <summary>
    /// Returns the minimum value in a sequence of DataSize values.
    /// </summary>
    /// <param name="source">A sequence of DataSize values to determine the minimum value of.</param>
    /// <returns>The minimum value in the sequence, or null if the sequence is empty.</returns>
    public static DataSize? MinOrNull(this IEnumerable<DataSize> source)
    {
      return source.Any() ? source.Min() : null as DataSize?;
    }
    /// <summary>
    /// Invokes a transform function on each element of a sequence and returns the minimum DataSize value.
    /// </summary>
    /// <param name="source">A sequence of values to determine the minimum  value of.</param>
    /// <param name="ProjectQuery">A transform function to apply to each element.</param>
    /// <typeparam name="T">The type of the elements of source.</typeparam>
    /// <returns>The minimum value in the sequence, or null if the sequence is empty.</returns>
    public static DataSize? MinOrNull<T>(this IEnumerable<T> source, Func<T, DataSize> ProjectQuery)
    {
      return source.Any() ? source.Min(ProjectQuery) : null as DataSize?;
    }
    /// <summary>
    /// Returns the maximum value in a sequence of DataSize values.
    /// </summary>
    /// <param name="source">A sequence of DataSize values to determine the maximum value of.</param>
    /// <returns>The maximum value in the sequence, or null if the sequence is empty.</returns>
    public static DataSize? MaxOrNull(this IEnumerable<DataSize> source)
    {
      return source.Any() ? source.Max() : null as DataSize?;
    }
    /// <summary>
    /// Returns the maximum value in a sequence of DataSize values.
    /// </summary>
    /// <param name="source">A sequence of DataSize values to determine the maximum value of.</param>
    /// <param name="ProjectQuery">A transform function to apply to each element.</param>
    /// <returns>The maximum value in the sequence, or null if the sequence is empty.</returns>
    public static DataSize? MaxOrNull<T>(this IEnumerable<T> source, Func<T, DataSize> ProjectQuery)
    {
      return source.Any() ? source.Max(ProjectQuery) : null as DataSize?;
    }
    /// <summary>
    /// Returns the minimum value in a sequence of <typeparamref name="T"/> values. A return value indicates whether a value could be determined.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="source">A sequence of <typeparamref name="T"/> values to determine the minimum value of.</param>
    /// <param name="Value">The minimum <typeparamref name="T"/> value in the sequence, and default if a value could not be determined.</param>
    /// <returns>Whether a value could be determined, and a parameter containing the minimum value in the sequence.</returns>
    public static bool TryMin<T>(this IEnumerable<T> source, out T Value)
    {
      Value = source.Any() ? source.Min() : default;

      return source.Any();
    }
    /// <summary>
    /// Returns the maximum value in a sequence of <typeparamref name="T"/> values. A return value indicates whether a value could be determined.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="source">A sequence of <typeparamref name="T"/> values to determine the minimum value of.</param>
    /// <param name="Value">The maximum <typeparamref name="T"/> value in the sequence, and default if a value could not be determined.</param>
    /// <returns>Whether a value could be determined, and a parameter containing the maximum value in the sequence.</returns>
    public static bool TryMax<T>(this IEnumerable<T> source, out T Value)
    {
      Value = source.Any() ? source.Max() : default;

      return source.Any();
    }
    /// <summary>
    /// Finds the element after <paramref name="Key"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Key">The element before the element to be retrieved.</param>
    /// <returns>The element after <paramref name="Key"/>, or the default value if <paramref name="Key"/> is either not found or the last element.</returns>
    public static T NextOrDefault<T>(this IEnumerable<T> Source, T Key)
    {
      var Found = false;

      foreach (var Value in Source)
      {
        if (Found)
          return Value;
        else if (Equals(Value, Key))
          Found = true;
      }

      return default;
    }
    /// <summary>
    /// Finds the element before <paramref name="Key"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Key">The element after the element to be retrieved.</param>
    /// <returns>The element before <paramref name="Key"/>, or the default value if <paramref name="Key"/> is either not found or the first element.</returns>
    public static T PreviousOrDefault<T>(this IEnumerable<T> Source, T Key)
    {
      T Previous = default;

      foreach (var Value in Source)
      {
        if (Equals(Value, Key))
          return Previous;

        Previous = Value;
      }

      return default;
    }
    /// <summary>
    /// Get the next element in this <see cref="IEnumerable{T}"/> after <paramref name="Key"/>. If there is no element after <paramref name="Key"/>, the last element is returned. If <paramref name="Key"/> is not found, the first element is returned.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Key">The element to look for.</param>
    /// <returns>The next element in this <see cref="IEnumerable{T}"/> after <paramref name="Key"/>, or if there is no element after <paramref name="Key"/>, the last element, or if <paramref name="Key"/> is not found, the first element.</returns>
    /// <exception cref="ArgumentOutOfRangeException">If <paramref name="Source"/> is empty.</exception>
    public static T NextOrLast<T>(this IEnumerable<T> Source, T Key)
    {
      var List = Source.ToList();

      var Index = List.IndexOf(Key) + 1;
      if (Index > List.Count - 1)
        Index = List.Count - 1;

      return List[Index];
    }
    public static T NextOrLast<T>(this IEnumerable<T> Source, T Key, Func<T, bool> Predicate)
    {
      var List = Source.ToList();

      var Index = List.IndexOf(Key) + 1;

      while (Index < List.Count - 1 && !Predicate(List[Index]))
        Index++;

      if (Index > List.Count - 1)
        Index = List.Count - 1;

      return List[Index];
    }
    /// <summary>
    /// Get the previous element in this <see cref="IEnumerable{T}"/> before <paramref name="Key"/>. If there is no element before <paramref name="Key"/> or <paramref name="Key"/> is not found, the first element is returned.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Key">The element to look for.</param>
    /// <returns>The previous element in this <see cref="IEnumerable{T}"/> before <paramref name="Key"/>, or if there is no element before <paramref name="Key"/> or <paramref name="Key"/> is not found, the first element.</returns>
    /// <exception cref="ArgumentOutOfRangeException">If <paramref name="Source"/> is empty.</exception>
    public static T PreviousOrFirst<T>(this IEnumerable<T> Source, T Key)
    {
      var List = Source.ToList();

      var Index = List.IndexOf(Key) - 1;
      if (Index < 0)
        Index = 0;

      return List[Index];
    }
    public static T PreviousOrFirst<T>(this IEnumerable<T> Source, T Key, Func<T, bool> Predicate)
    {
      var List = Source.ToList();

      var Index = List.IndexOf(Key) - 1;

      while (Index > 0 && !Predicate(List[Index]))
        Index--;

      if (Index < 0)
        Index = 0;

      return List[Index];
    }
    /// <summary>
    /// Convert this <see cref="IEnumerable{T}"/> to an <see cref="Inv.DistinctList{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <returns>A new <see cref="Inv.DistinctList{T}"/> containing the elements of this <see cref="IEnumerable{T}"/>.</returns>
    public static Inv.DistinctList<T> ToDistinctList<T>(this IEnumerable<T> Source)
    {
      if (Source == null)
        return new Inv.DistinctList<T>();
      else
        return new Inv.DistinctList<T>(Source);
    }
    /// <summary>
    /// Create an <see cref="Inv.DistinctList{TSource}"/> containing this object.
    /// </summary>
    /// <typeparam name="TSource">The type of this object.</typeparam>
    /// <param name="Source">This object.</param>
    /// <returns>A new <see cref="Inv.DistinctList{TSource}"/> containing this object as its only element.</returns>
    public static Inv.DistinctList<TSource> SingleToDistinctList<TSource>(this TSource Source)
    {
      if (Source == null)
        return new Inv.DistinctList<TSource>();
      else
        return new Inv.DistinctList<TSource>() { Source };
    }
    /// <summary>
    /// Concatenates an item onto the end of a sequence.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of the input sequence.</typeparam>
    /// <param name="Left">The sequence to concatenate.</param>
    /// <param name="Right">The item to concatenate to the first sequence.</param>
    /// <returns>An <see cref="IEnumerable{T}"/> that contains the concatenated elements of the input sequence and item.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="Left"/> is null.</exception>
    public static IEnumerable<TSource> Concat<TSource>(this IEnumerable<TSource> Left, TSource Right)
    {
      return Left.Concat(new TSource[] { Right });
    }
    /// <summary>
    /// Produces the set union of a sequence and the specified item using the default equality comparer.
    /// </summary>
    /// <typeparam name="TSource">The type of elements in this <see cref="IEnumerable{TSource}"/>.</typeparam>
    /// <param name="Left">This <see cref="IEnumerable{TSource}"/>.</param>
    /// <param name="Right">The element to union into this <see cref="IEnumerable{TSource}"/>.</param>
    /// <returns>The result of the union.</returns>
    public static IEnumerable<TSource> Union<TSource>(this IEnumerable<TSource> Left, TSource Right)
    {
      return Left.Union(new TSource[] { Right });
    }
    /// <summary>
    /// Produces the set union of a sequence and the specified item using the specified equality comparer.
    /// </summary>
    /// <typeparam name="TSource">The type of elements in this <see cref="IEnumerable{TSource}"/>.</typeparam>
    /// <param name="Left">This <see cref="IEnumerable{TSource}"/>.</param>
    /// <param name="Right">The element to union into this <see cref="IEnumerable{TSource}"/>.</param>
    /// <param name="Comparer">The equality comparer to use.</param>
    /// <returns>The result of the union.</returns>
    public static IEnumerable<TSource> Union<TSource>(this IEnumerable<TSource> Left, TSource Right, IEqualityComparer<TSource> Comparer)
    {
      return Left.Union(new TSource[] { Right }, Comparer);
    }
    /// <summary>
    /// Get an <see cref="IEnumerable{TSource}"/> for this object.
    /// </summary>
    /// <typeparam name="TSource">The type of this object.</typeparam>
    /// <param name="Source">This object.</param>
    /// <returns>An <see cref="IEnumerable{TSource}"/> containing this object.</returns>
    public static IEnumerable<TSource> ToEnumerable<TSource>(this TSource Source)
    {
      yield return Source;
    }
    /// <summary>
    /// Perform an action on every element of this <see cref="IEnumerable{TSource}"/>.
    /// </summary>
    /// <typeparam name="TSource">The type of elements in this <see cref="IEnumerable{TSource}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{TSource}"/>.</param>
    /// <param name="Action">The action to perform on each element.</param>
    public static void ForEach<TSource>(this IEnumerable<TSource> Source, Action<TSource> Action)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.CheckNotNull(Source, nameof(Source));
        Inv.Assert.CheckNotNull(Action, nameof(Action));
      }

      if (Source != null && Action != null)
      {
        foreach (var Item in Source)
          Action(Item);
      }
    }
    /// <summary>
    /// Build a frequency histogram from this <see cref="IEnumerable{TSource}"/> and the specified equality comparer.
    /// </summary>
    /// <typeparam name="TSource">The type of elements in this <see cref="IEnumerable{TSource}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{TSource}"/>.</param>
    /// <param name="Comparer">The <see cref="IEqualityComparer{TSource}"/> to use, or null to use the default.</param>
    /// <returns>A list of <see cref="FrequencyRecord{TSource}"/> elements representing the frequencies with which each element in this <see cref="IEnumerable{TSource}"/> occur.</returns>
    public static IEnumerable<FrequencyRecord<TSource>> Frequency<TSource>(this IEnumerable<TSource> Source, IEqualityComparer<TSource> Comparer = null)
    {
      var Dictionary = Comparer == null ? new Dictionary<TSource, int>() : new Dictionary<TSource, int>(Comparer);

      foreach (var Item in Source)
        Dictionary[Item] = Dictionary.GetValueOrDefault(Item, 0) + 1;

      return Dictionary.Select(Index => new FrequencyRecord<TSource>(Index.Key, Index.Value));
    }
    /// <summary>
    /// Order this <see cref="IEnumerable{TSource}"/> in ascending order based on the default item comparison.
    /// </summary>
    /// <typeparam name="TSource">The type of elements in this <see cref="IEnumerable{TSource}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{TSource}"/>.</param>
    /// <returns>An ordered version of this <see cref="IEnumerable{TSource}"/>.</returns>
    public static IOrderedEnumerable<TSource> OrderBy<TSource>(this IEnumerable<TSource> Source)
    {
      return Source.OrderBy(Index => Index);
    }
    public static IOrderedEnumerable<TSource> OrderBy<TSource>(this IEnumerable<TSource> Source, Comparison<TSource> Comparison)
    {
      return Source.OrderBy(Index => Index, new Inv.Support.DelegateComparer<TSource>(Comparison));
    }
    public static IOrderedEnumerable<TSource> OrderBy<TSource, TKey>(this IEnumerable<TSource> Source, Func<TSource, TKey> Selection, Comparison<TKey> Comparison)
    {
      return Source.OrderBy(Index => Selection(Index), new Inv.Support.DelegateComparer<TKey>(Comparison));
    }
    public static IOrderedEnumerable<T> OrderByWithNullsFirst<T, TKey>(this IEnumerable<T> Source, Func<T, TKey> ProjectQuery)
    {
      return Source.OrderBy(I => ProjectQuery(I) != null).ThenBy(ProjectQuery);
    }
    public static IOrderedEnumerable<T> OrderByWithNullsLast<T, TKey>(this IEnumerable<T> Source, Func<T, TKey> ProjectQuery)
    {
      return Source.OrderBy(I => ProjectQuery(I) == null).ThenBy(ProjectQuery);
    }
    /// <summary>
    /// Order this <see cref="IEnumerable{TSource}"/> in descending order based on the default item comparison.
    /// </summary>
    /// <typeparam name="TSource">The type of elements in this <see cref="IEnumerable{TSource}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{TSource}"/>.</param>
    /// <returns>An ordered version of this <see cref="IEnumerable{TSource}"/>.</returns>
    public static IOrderedEnumerable<TSource> OrderByDescending<TSource>(this IEnumerable<TSource> Source)
    {
      return Source.OrderByDescending(Index => Index);
    }
    public static IOrderedEnumerable<T> ThenByWithNullsFirst<T, TKey>(this IOrderedEnumerable<T> Source, Func<T, TKey> ProjectQuery)
    {
      return Source.ThenBy(I => ProjectQuery(I) != null).ThenBy(ProjectQuery);
    }
    public static IOrderedEnumerable<T> ThenByWithNullsLast<T, TKey>(this IOrderedEnumerable<T> Source, Func<T, TKey> ProjectQuery)
    {
      return Source.ThenBy(I => ProjectQuery(I) == null).ThenBy(ProjectQuery);
    }
    /// <summary>
    /// Computes the sum of the sequence of Inv.Money values that are obtained by invoking a transform function on each element of the input sequence.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of <paramref name="Source"/>.</typeparam>
    /// <param name="Source">A sequence of values that are used to calculate a sum.</param>
    /// <param name="Selector">A transform function to apply to each element.</param>
    /// <returns>The sum of the projected values.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="Source"/> or <paramref name="Selector"/> is null.</exception>
    /// <exception cref="OverflowException">The sum is larger than <see cref="decimal.MaxValue"/>.</exception>
    public static Inv.Money Sum<TSource>(this IEnumerable<TSource> Source, Func<TSource, Inv.Money> Selector)
    {
      return Source.Aggregate(Inv.Money.Zero, (Current, Item) => Current + Selector(Item));
    }
    public static Inv.Money Sum(this IEnumerable<Inv.Money> Source)
    {
      return Source.Sum(M => M);
    }
    public static Inv.DataSize Sum<TSource>(this IEnumerable<TSource> Source, Func<TSource, Inv.DataSize> Selector)
    {
      return Source.Aggregate(Inv.DataSize.Zero, (Current, Item) => Current + Selector(Item));
    }
    public static TimeSpan Sum<TSource>(this IEnumerable<TSource> Source, Func<TSource, TimeSpan> Selector)
    {
      return Source.Aggregate(TimeSpan.Zero, (Current, Item) => Current + Selector(Item));
    }
    public static Inv.Money Average<TSource>(this IEnumerable<TSource> Source, Func<TSource, Inv.Money> Selector)
    {
      return new Inv.Money(Source.Average(Item => Selector(Item).GetAmount()));
    }
    public static IEnumerable<CombineJoinRecord<TSource, TSource>> FullOuterJoin<TSource, TItem>(this IEnumerable<TSource> LeftSource, IEnumerable<TSource> RightSource, Func<TSource, TItem> ItemFunction)
    {
      return FullOuterJoin(LeftSource, RightSource, ItemFunction, ItemFunction, (Left, Right) => new CombineJoinRecord<TSource, TSource>(Left, Right));
    }
    public static IEnumerable<CombineJoinRecord<TLeft, TRight>> FullOuterJoin<TLeft, TRight, TItem>(this IEnumerable<TLeft> LeftSource, IEnumerable<TRight> RightSource, Func<TLeft, TItem> LeftFunction, Func<TRight, TItem> RightFunction)
    {
      return FullOuterJoin(LeftSource, RightSource, LeftFunction, RightFunction, (Left, Right) => new CombineJoinRecord<TLeft, TRight>(Left, Right));
    }
    public static IEnumerable<TResult> FullOuterJoin<TLeft, TRight, TItem, TResult>(this IEnumerable<TLeft> LeftSource, IEnumerable<TRight> RightSource, Func<TLeft, TItem> LeftFunction, Func<TRight, TItem> RightFunction, OuterJoinProjection<TLeft, TRight, TResult> ResultFunction)
    {
      // (select from A left join B) union (select from A right join B).

      var LeftDictionary = LeftSource.ToDictionary(LeftItem => LeftFunction(LeftItem));
      var RightDictionary = RightSource.ToDictionary(RightItem => RightFunction(RightItem));

      foreach (var LeftPair in LeftDictionary)
      {
        var RightItem = RightDictionary.GetValueOrDefault(LeftPair.Key);

        yield return ResultFunction(LeftPair.Value, RightItem);
      }

      foreach (var RightPair in RightDictionary)
      {
        if (!LeftDictionary.ContainsKey(RightPair.Key))
          yield return ResultFunction(default, RightPair.Value);
      }
    }
    public static IEnumerable<CombineJoinRecord<TLeft, TRight>> FullOuterJoin<TLeft, TRight>(this IEnumerable<TLeft> LeftSource, IEnumerable<TRight> RightSource, Func<TLeft, TRight, bool> IsEqualToFunction)
    {
      return FullOuterJoin(LeftSource, RightSource, IsEqualToFunction, (Left, Right) => new CombineJoinRecord<TLeft, TRight>(Left, Right));
    }
    public static IEnumerable<TResult> FullOuterJoin<TLeft, TRight, TResult>(this IEnumerable<TLeft> LeftSource, IEnumerable<TRight> RightSource, Func<TLeft, TRight, bool> IsEqualToFunction, OuterJoinProjection<TLeft, TRight, TResult> ResultFunction)
    {
      var RightJoinedSet = new HashSet<TRight>();
      foreach (var LeftItem in LeftSource)
      {
        var RightItem = RightSource.Find(R => IsEqualToFunction(LeftItem, R));

        if (RightItem != null)
          RightJoinedSet.Add(RightItem);

        yield return ResultFunction(LeftItem, RightItem);
      }

      foreach (var RightItem in RightSource.Except(RightJoinedSet))
      {
        var LeftItem = LeftSource.Find(L => IsEqualToFunction(L, RightItem));

        yield return ResultFunction(LeftItem, RightItem);
      }
    }
    public static IEnumerable<TSource> Distinct<TSource, TIdentity>(this IEnumerable<TSource> Source, Func<TSource, TIdentity> Function)
    {
      return Source.Distinct(new ProjectionEqualityComparer<TSource, TIdentity>(Function));
    }
    public static TSource FirstByOrder<TSource, TKey>(this IEnumerable<TSource> Source, Func<TSource, TKey> KeySelector)
    {
      return Source.OrderBy(KeySelector).First();
    }
    public static TSource LastByOrder<TSource, TKey>(this IEnumerable<TSource> Source, Func<TSource, TKey> KeySelector)
    {
      return Source.OrderBy(KeySelector).Last();
    }
    public static TSource LastOrDefaultByOrder<TSource, TKey>(this IEnumerable<TSource> Source, Func<TSource, TKey> KeySelector)
    {
      return Source.OrderBy(KeySelector).LastOrDefault();
    }
    public static TSource FirstOrDefaultByOrder<TSource, TKey>(this IEnumerable<TSource> Source, Func<TSource, TKey> KeySelector)
    {
      return Source.OrderBy(KeySelector).FirstOrDefault();
    }
    /// <summary>
    /// Enumerates distinct elements from a sequence where those elements appeared at least twice within the source sequence, using the specified equality comparer to compare values.
    /// </summary>
    /// <typeparam name="T">The type of the elements of <paramref name="Source"/>.</typeparam>
    /// <param name="Source">The sequence to find duplicate elements from.</param>
    /// <param name="Comparer">An <see cref="IEqualityComparer{T}"/> to compare values.</param>
    /// <returns>An <see cref="IEnumerable{T}"/> that contains distinct elements from the source sequence where those elements appeared at least twice.</returns>
    public static IEnumerable<T> Duplicates<T>(this IEnumerable<T> Source, IEqualityComparer<T> Comparer = null)
    {
      var UniqueSet = new HashSet<T>(Comparer);
      var DuplicateSet = new HashSet<T>(Comparer);

      foreach (var Item in Source)
      {
        if (!UniqueSet.Add(Item))
        {
          if (DuplicateSet.Add(Item))
            yield return Item;
        }
      }
    }
    /// <summary>
    /// Determines whether there are any duplicate elements within the source sequence, using the specified equality comparer to compare values.
    /// </summary>
    /// <typeparam name="T">The type of the elements of <paramref name="Source"/>.</typeparam>
    /// <param name="Source">The sequence to find duplicate elements from.</param>
    /// <param name="Comparer">An <see cref="IEqualityComparer{T}"/> to compare values.</param>
    /// <returns><see langword="true"/> if there are any duplicate elements, otherwise <see langword="false"/>.</returns>
    public static bool HasDuplicates<T>(this IEnumerable<T> Source, IEqualityComparer<T> Comparer = null)
    {
      return Source.Duplicates(Comparer).Any();
    }
    public static IEnumerable<T> Except<T>(this IEnumerable<T> Source, T Value)
    {
      return Source.Except(Value.ToEnumerable());
    }
    public static bool ContainsAny<T>(this IEnumerable<T> Source, IEnumerable<T> Items)
    {
      return Source.Intersect(Items).Any();
    }
    public static bool ContainsAny<T>(this IEnumerable<T> Source, IEnumerable<T> Items, IEqualityComparer<T> Comparer)
    {
      return Source.Intersect(Items, Comparer).Any();
    }
    /// <summary>
    /// Determines whether all elements from <paramref name="Items"/> are contained within <paramref name="Source"/>.
    /// </summary>
    /// <remarks>If <paramref name="Items"/> contains any duplicate values the result will be <see langword="false"/>.</remarks>
    /// <typeparam name="T">The type of elements in the compared sequences.</typeparam>
    /// <param name="Source">The sequence to check for items being contained within.</param>
    /// <param name="Items">The sequence of items to check for being contained in <paramref name="Source"/>.</param>
    /// <returns><see langword="true"/> if all elements from <paramref name="Items"/> are contained within <paramref name="Source"/>.</returns>
    public static bool ContainsAll<T>(this IEnumerable<T> Source, IEnumerable<T> Items)
    {
      return Source.Intersect(Items).Count() == Items.Count();
    }
    public static double StandardDeviation(this IEnumerable<long> Source)
    {
      var SourceCount = Source.Count();

      if (SourceCount <= 1)
        return 0;

      var Average = Source.Average();
      var DifferenceSum = Source.Sum(I => Math.Pow(I - Average, 2));

      return Math.Sqrt(DifferenceSum / SourceCount);
    }
    public static double StandardDeviation(this IEnumerable<double> Source)
    {
      var SourceCount = Source.Count();

      if (SourceCount <= 1)
        return 0;

      var Average = Source.Average();
      var DifferenceSum = Source.Sum(I => Math.Pow(I - Average, 2));

      return Math.Sqrt(DifferenceSum / SourceCount);
    }
    public static IEnumerable<T> TakeLast<T>(this IReadOnlyList<T> Source, int Count)
    {
      return Source.Skip(Math.Max(0, Source.Count - Count));
    }
    public static bool In<T>(this T Source, params T[] List)
    {
      if (Source == null)
        throw new ArgumentNullException(nameof(Source));

      return List.Contains(Source);
    }
    /// <summary>
    /// Flattens <paramref name="Source"/> to an <see cref="IEnumerable{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this enumerable.</typeparam>
    /// <param name="Source">An enumerable of <see cref="IEnumerable{T}"/>.</param>
    /// <returns>An <see cref="IEnumerable{T}"/> containing each element from each nested <see cref="IEnumerable{T}"/>.</returns>
    public static IEnumerable<T> FlatMap<T>(this IEnumerable<IEnumerable<T>> Source)
    {
      foreach (var Enumerable in Source)
        foreach (var Item in Enumerable)
          yield return Item;
    }
    public static IEnumerable<IGrouping<TKey, TElement>> Flatten<TKey, TElement>(this IEnumerable<IGrouping<TKey, IEnumerable<TElement>>> Source)
    {
      foreach (var Grouping in Source)
        yield return new FlattenedGrouping<TKey, TElement>(Grouping.Key, Grouping);
    }
    /// <summary>
    /// Split this <see cref="IEnumerable{T}"/> into partitions of <paramref name="Size"/> elements.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Size">The number of elements to place in each partition.</param>
    /// <returns>Multiple <see cref="IEnumerable{T}"/>, each containing a partition of elements from this <see cref="IEnumerable{T}"/> of up to <paramref name="Size"/> elements.</returns>
    public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> Source, int Size)
    {
      var Result = new T[Size];

      var Index = 0;

      foreach (var Item in Source)
      {
        Result[Index] = Item;
        Index++;

        if (Index >= Size)
        {
          yield return Result;

          Result = new T[Size];
          Index = 0;
        }
      }

      if (Index > 0)
      {
        Array.Resize(ref Result, Index); // trailing partition.

        yield return Result;
      }
    }
    /// <summary>
    /// Split this <see cref="IEnumerable{T}"/> into partitions of distinct items based on <paramref name="ItemSelector"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <typeparam name="TItem">The type of elements being compared for uniqueness.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="ItemSelector">A function which selects the items being compared for uniqueness.</param>
    /// <param name="EqualityComparer">An <see cref="IEqualityComparer{T}"/> for comparing items.</param>
    /// <returns>Zero or more <see cref="IEnumerable{T}"/>, each containing a sequence of elements from <paramref name="Source"/>, using <paramref name="ItemSelector"/> and <paramref name="EqualityComparer"/> to ensure each sequence contains only unique elements.</returns>
    public static IEnumerable<IEnumerable<T>> DistinctPartition<T, TItem>(this IEnumerable<T> Source, Func<T, TItem> ItemSelector, IEqualityComparer<TItem> EqualityComparer = null)
    {
      var ItemSet = new HashSet<TItem>(EqualityComparer);
      var CurrentPartition = new List<T>();

      foreach (var Value in Source)
      {
        var Item = ItemSelector(Value);
        if (!ItemSet.Add(Item))
        {
          yield return CurrentPartition;
          CurrentPartition.Clear();

          ItemSet.Clear();
          ItemSet.Add(Item);
        }

        CurrentPartition.Add(Value);
      }

      if (CurrentPartition.Count != 0)
        yield return CurrentPartition;
    }
    /// <summary>
    /// Retrieves the first element of <paramref name="Source"/> or <see langword="null"/> if <paramref name="Source"/> is empty.
    /// </summary>
    /// <typeparam name="T">The type of elements of <paramref name="Source"/>.</typeparam>
    /// <param name="Source">The sequence to find the first element of.</param>
    /// <returns>The first element of <paramref name="Source"/> or <see langword="null"/> if <paramref name="Source"/> is empty.</returns>
    public static T? FirstOrNull<T>(this IEnumerable<T> Source)
      where T : struct
    {
      return Source.Any() ? Source.First() : (T?)null;
    }
    /// <summary>
    /// Retrieves the last element of <paramref name="Source"/> or <see langword="null"/> if <paramref name="Source"/> is empty.
    /// </summary>
    /// <typeparam name="T">The type of elements of <paramref name="Source"/>.</typeparam>
    /// <param name="Source">The sequence to find the last element of.</param>
    /// <returns>The last element of <paramref name="Source"/> or <see langword="null"/> if <paramref name="Source"/> is empty.</returns>
    public static T? LastOrNull<T>(this IEnumerable<T> Source)
      where T : struct
    {
      return Source.Any() ? Source.Last() : (T?)null;
    }
    /// <summary>
    /// Determines whether all elements of a sequence satisfy a condition by incorporating the element's index.
    /// </summary>
    /// <typeparam name="T">The type of the elements of <paramref name="Source"/>.</typeparam>
    /// <param name="Source">An <see cref="IEnumerable{T}"/> that contains the elements to apply the predicate to.</param>
    /// <param name="Predicate">A function to test each element for a condition; the second parameter of the function represents the index of the source element.</param>
    /// <returns><see langword="true"/> if every element of the source sequence passes the test in the specified predicate, or if the sequence is empty; otherwise, <see langword="false"/>.</returns>
    public static bool All<T>(this IEnumerable<T> Source, Func<T, int, bool> Predicate)
    {
      var Index = 0;
      foreach (var Item in Source)
        if (!Predicate(Item, Index++))
          return false;

      return true;
    }

    /// <summary>
    /// Produces a sequence of tuples with elements from the two specified sequences.
    /// </summary>
    /// <typeparam name="TFirst">The type of the elements of the first input sequence.</typeparam>
    /// <typeparam name="TSecond">The type of the elements of the second input sequence.</typeparam>
    /// <param name="First">The first sequence to merge.</param>
    /// <param name="Second">The second sequence to merge.</param>
    /// <returns>A sequence of tuples with elements taken from the first and second sequences, in that order.</returns>
    /// <remarks>NOTE: This method is named ZipX to avoid collisions with the System.Linq.Enumerable extension method 'Zip' with matching signature which was introduced in .NET 6.</remarks>
    public static IEnumerable<(TFirst First, TSecond Second)> ZipX<TFirst, TSecond>(this IEnumerable<TFirst> First, IEnumerable<TSecond> Second)
    {
      if (First is null)
        throw new ArgumentNullException(nameof(First));
      if (Second is null)
        throw new ArgumentNullException(nameof(Second));

      // This is intentionally split out into a separate method due to the use of yield. This ensures that exceptions are thrown immediately rather than being deferred until enumeration.
      return ZipIterator(First, Second);
    }

    public static Array ToEnumerableArray(this System.Collections.IEnumerable Source)
    {
      var Enumerator = Source.GetEnumerator();

      var List = new List<object>();

      while (Enumerator.MoveNext())
        List.Add(Enumerator.Current);

      return List.ToArray();
    }

    public delegate TResult OuterJoinProjection<TLeft, TRight, TResult>(TLeft Left, TRight Right);

    private static IEnumerable<(TFirst First, TSecond Second)> ZipIterator<TFirst, TSecond>(IEnumerable<TFirst> First, IEnumerable<TSecond> Second)
    {
      using var E1 = First.GetEnumerator();
      using var E2 = Second.GetEnumerator();

      while (E1.MoveNext() && E2.MoveNext())
        yield return (E1.Current, E2.Current);
    }

    public sealed class CombineJoinRecord<TLeft, TRight>
    {
      public CombineJoinRecord(TLeft Left, TRight Right)
      {
        this.Left = Left;
        this.Right = Right;
      }

      public TLeft Left { get; set; }
      public TRight Right { get; set; }
    }

    /// <summary>
    /// Represents a bucket in a frequency histogram.
    /// </summary>
    /// <typeparam name="T">The type of element being counted.</typeparam>
    public sealed class FrequencyRecord<T>
    {
      internal FrequencyRecord(T Object, int Count)
      {
        this.Key = Object;
        this.Count = Count;
      }

      /// <summary>
      /// The key value of this bucket.
      /// </summary>
      public T Key { get; }

      /// <summary>
      /// The number of records in this bucket.
      /// </summary>
      public int Count { get; }
    }

    private sealed class FlattenedGrouping<TKey, TElement> : IGrouping<TKey, TElement>
    {
      public FlattenedGrouping(TKey Key, IEnumerable<IEnumerable<TElement>> Elements)
      {
        this.Key = Key;
        this.Elements = Elements.SelectMany(E => E);
      }

      public TKey Key { get; }

      public IEnumerator<TElement> GetEnumerator() => Elements.GetEnumerator();
      System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() => Elements.GetEnumerator();

      private readonly IEnumerable<TElement> Elements;
    }
  }

  public static class HexHelper
  {
    public static bool IsHexadecimal(this string Value) => Value.ToUpper().ContainsOnly(HexadecimalCharacters);

    public static readonly char[] HexadecimalCharacters = new char[16] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
  }

  /// <summary>
  /// <see cref="IList{T}"/> extension methods.
  /// </summary>
  public static class IListHelper
  {
    /// <summary>
    /// Remove a range of elements from this <see cref="System.Collections.IList"/>.
    /// </summary>
    /// <param name="Source">This <see cref="System.Collections.IList"/>.</param>
    /// <param name="Array">An <see cref="System.Collections.IEnumerable"/> containing the elements to remove from this <see cref="System.Collections.IList"/>.</param>
    public static void RemoveRange(this System.Collections.IList Source, System.Collections.IEnumerable Array)
    {
      foreach (var Item in Array)
        Source.Remove(Item);
    }
    /// <summary>
    /// Truncate the list to the number of items specified.
    /// </summary>
    /// <param name="List"></param>
    /// <param name="Limit"></param>
    public static void Truncate<T>(this IList<T> List, int Limit)
    {
      while (List.Count > Limit)
        List.RemoveAt(List.Count - 1); // NOTE: delete from the end of the list to avoid array copies.
    }
    public static IEnumerable<T> ExceptFirst<T>(this IList<T> Source) where T : class
    {
      return Source.Skip(1);
    }
    public static IEnumerable<T> ExceptLast<T>(this IList<T> Source) where T : class
    {
      return Source.Take(Source.Count - 1);
    }
    public static int ShallowCompareTo<T>(this IList<T> Left, IList<T> Right) where T : IComparable<T>
    {
      var Count = Math.Min(Left.Count, Right.Count);
      var Result = 0;
      var Index = 0;

      while (Result == 0 && Index < Count)
      {
        Result = Left[Index].CompareTo(Right[Index]);

        Index++;
      }

      if (Result == 0)
        Result = Left.Count.CompareTo(Right.Count);

      return Result;
    }
    /// <summary>
    /// Determine whether or not this <see cref="IReadOnlyList{T}"/> is equivalent to another <see cref="IReadOnlyList{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IReadOnlyList{T}"/>.</typeparam>
    /// <param name="Left">This <see cref="IReadOnlyList{T}"/>.</param>
    /// <param name="Right">The <see cref="IReadOnlyList{T}"/> to compare to.</param>
    /// <returns>True if this <see cref="IReadOnlyList{T}"/> is equivalent to <paramref name="Right"/>; false otherwise.</returns>
    public static bool ShallowEqualTo<T>(this IReadOnlyList<T> Left, IReadOnlyList<T> Right)
    {
      var Count = Left.Count;
      var Result = Count == Right.Count;
      var Index = 0;

      while (Result && Index < Count)
      {
        Result = object.Equals(Left[Index], Right[Index]);

        Index++;
      }

      return Result;
    }
    /// <summary>
    /// Determine whether or not this byte array is equivalent to another byte array.
    /// </summary>
    /// <param name="Left"></param>
    /// <param name="Right"></param>
    /// <returns></returns>
    public static bool ShallowEqualTo(this byte[] Left, byte[] Right)
    {
      var Count = Left.Length;
      if (Count != Right.Length)
        return false;

      var Index = 0;

      while (Index < Count)
      {
        if (Left[Index] != Right[Index])
          return false;

        Index++;
      }

      return true;
    }
    /// <summary>
    /// Determine whether or not a list contains only distinct elements.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <returns></returns>
    public static bool IsDistinct<T>(this IEnumerable<T> Source)
    {
      var DistinctSet = new HashSet<T>();

      foreach (var Item in Source)
      {
        if (!DistinctSet.Add(Item))
          return false;
      }

      return true;
    }
    /// <summary>
    /// Determine whether or not this <see cref="IList{Int64}"/> is contiguous in nature.
    /// </summary>
    /// <param name="Source">This <see cref="IList{Int64}"/>.</param>
    /// <returns>True if the list contains a contiguous incrementing set of integers; false otherwise.</returns>
    public static bool IsContiguous(this IList<long> Source)
    {
      var Result = Source.Count > 0;

      if (Result)
      {
        var Value = Source[0] + 1;

        for (var Index = 1; Index < Source.Count; Index++)
        {
          if (Source[Index] != Value)
          {
            Result = false;
            break;
          }
          Value++;
        }
      }

      return Result;
    }
    /// <summary>
    /// Determine whether or not this <see cref="IList{Int32}"/> is contiguous in nature.
    /// </summary>
    /// <param name="Source">This <see cref="IList{Int32}"/>.</param>
    /// <returns>True if the list contains a contiguous incrementing set of integers; false otherwise.</returns>
    public static bool IsContiguous(this IList<int> Source)
    {
      var Result = Source.Count > 0;

      if (Result)
      {
        var Value = Source[0] + 1;

        for (var Index = 1; Index < Source.Count; Index++)
        {
          if (Source[Index] != Value)
          {
            Result = false;
            break;
          }
          Value++;
        }
      }

      return Result;
    }
    /// <summary>
    /// Enumerate the elements of this <see cref="IList{T}"/> from the zero-based index <paramref name="Low"/> to the end of the <see cref="IList{T}"/>.
    /// </summary>
    /// <remarks>
    /// This should probably be deprecated in favour of Skip().
    /// </remarks>
    /// <typeparam name="T">The type of elements in this <see cref="IList{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IList{T}"/>.</param>
    /// <param name="Low">The zero-based index of the first element to enumerate.</param>
    /// <returns>The set of elements of this <see cref="IList{T}"/> from <paramref name="Low"/> to the end of the <see cref="IList{T}"/>.</returns>
    public static IEnumerable<T> EnumerateRange<T>(this IList<T> Source, int Low)
    {
      return EnumerateRange(Source, Low, Source.Count);
    }
    /// <summary>
    /// Enumerate the elements of this <see cref="IList{T}"/> from the zero-based index <paramref name="Low"/> to the zero-based index <paramref name="High"/>.
    /// </summary>
    /// <remarks>
    /// This should probably be deprecated in favour of Skip().Take().
    /// </remarks>
    /// <typeparam name="T">The type of elements in this <see cref="IList{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IList{T}"/>.</param>
    /// <param name="Low">The zero-based index of the first element to enumerate.</param>
    /// <param name="High">The zero-based index of the last element to enumerate.</param>
    /// <returns>The set of elements of this <see cref="IList{T}"/> from <paramref name="Low"/> to <paramref name="High"/>.</returns>
    public static IEnumerable<T> EnumerateRange<T>(this IList<T> Source, int Low, int High)
    {
      for (var Index = Low; Index <= High; Index++)
        yield return Source[Index];
    }
    /// <summary>
    /// Create a new <see cref="System.Collections.ObjectModel.ReadOnlyCollection{T}"/> based on this <see cref="IList{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IList{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IList{T}"/>.</param>
    /// <returns>The new <see cref="System.Collections.ObjectModel.ReadOnlyCollection{T}"/>.</returns>
    public static System.Collections.ObjectModel.ReadOnlyCollection<T> AsReadOnlyX<T>(this IList<T> Source)
    {
      return new System.Collections.ObjectModel.ReadOnlyCollection<T>(Source);
    }
    /// <summary>
    /// Pop the last element from this <see cref="IList{T}"/> and return it; if the list has no elements, return the specified default value.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IList{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IList{T}"/>.</param>
    /// <param name="Default">The optional default value to return.</param>
    /// <returns>The element removed from the list, or the value of <paramref name="Default"/> if the list has no elements.</returns>
    public static T RemoveLastOrDefault<T>(this IList<T> Source, T Default = default)
    {
      if (Source.Count == 0)
      {
        return Default;
      }
      else
      {
        var Result = Source[Source.Count - 1];

        Source.RemoveAt(Source.Count - 1);

        return Result;
      }
    }
    public static T RemoveLast<T>(this IList<T> Source)
    {
      var Result = Source[Source.Count - 1];

      Source.RemoveAt(Source.Count - 1);

      return Result;
    }
    /// <summary>
    /// Pop the first element from this <see cref="IList{T}"/> and return it; if the list has no elements, return the specified default value.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IList{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IList{T}"/>.</param>
    /// <param name="Default">The optional default value to return.</param>
    /// <returns>The element removed from the list, or the value of <paramref name="Default"/> if the list has no elements.</returns>
    public static T RemoveFirstOrDefault<T>(this IList<T> Source, T Default = default)
    {
      if (Source.Count == 0)
      {
        return Default;
      }
      else
      {
        var Result = Source[0];

        Source.RemoveAt(0);

        return Result;
      }
    }
    public static T RemoveFirstOrDefault<T>(this IList<T> Source, Predicate<T> Predicate)
    {
      if (Source.Count == 0)
      {
        return default;
      }
      else
      {
        var Index = Source.FindIndex(Predicate);

        if (!Source.ExistsAt(Index))
          return default;

        var Result = Source[Index];

        Source.RemoveAt(Index);

        return Result;
      }
    }
    public static T RemoveFirst<T>(this IList<T> Source, Predicate<T> Predicate)
    {
      var Index = Source.FindIndex(Predicate);

      var Result = Source[Index];

      Source.RemoveAt(Index);

      return Result;
    }
    public static T RemoveFirst<T>(this IList<T> Source)
    {
      var Result = Source[0];

      Source.RemoveAt(0);

      return Result;
    }

    /// <summary>
    /// Pop the first element from this <see cref="IList{T}"/> and return it; if the list has no elements, return the specified default value.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IList{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IList{T}"/>.</param>
    /// <returns>The element removed from the list, or null if the list has no elements.</returns>
    public static T RemoveFirstOrNull<T>(this IList<T> Source)
      where T : class
    {
      if (Source.Count == 0)
      {
        return null;
      }
      else
      {
        var Result = Source[0];

        Source.RemoveAt(0);

        return Result;
      }
    }
    /// <summary>
    /// Pop the last element from this <see cref="IList{T}"/> and return it; if the list has no elements, return the specified default value.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IList{T}"/>.</typeparam>
    /// <param name="Source">This <see cref="IList{T}"/>.</param>
    /// <returns>The element removed from the list, or null if the list has no elements.</returns>
    public static T RemoveLastOrNull<T>(this IList<T> Source)
      where T : class
    {
      if (Source.Count == 0)
      {
        return null;
      }
      else
      {
        var Result = Source[Source.Count - 1];

        Source.RemoveAt(Source.Count - 1);

        return Result;
      }
    }

    public static int FindIndex<T>(this IList<T> Source, Predicate<T> Match)
    {
      return FindIndex(Source, 0, Source.Count, Match);
    }
    public static int FindIndex<T>(this IList<T> Source, int StartIndex, Predicate<T> Match)
    {
      return FindIndex(Source, StartIndex, Source.Count - StartIndex, Match);
    }
    public static int FindIndex<T>(this IList<T> Source, int Index, int Length, Predicate<T> Match)
    {
      var Current = Index + Length;

      for (var i = Index; i < Current; i++)
      {
        if (Match(Source[i]))
          return i;
      }

      return -1;
    }
    public static void Swap<T>(this IList<T> List, int Index1, int Index2)
    {
      var temp = List[Index1];
      List[Index1] = List[Index2];
      List[Index2] = temp;
    }

    public static int IndexOf<T>(this IReadOnlyList<T> Source, T Item)
    {
      for (var Index = 0; Index < Source.Count; Index++)
      {
        if (object.Equals(Source[Index], Item))
          return Index;
      }

      return -1;
    }
  }

  /// <summary>
  /// <see cref="HashSet{T}"/> extension methods.
  /// </summary>
  public static class HashSetHelper
  {
    public static void Assign<T>(this HashSet<T> Target, HashSet<T> Source)
    {
      Target.Clear();
      Target.AddRange(Source);
    }
    public static bool Toggle<T>(this HashSet<T> Source, T Item)
    {
      if (Source.Add(Item))
        return true;
      else if (Source.Remove(Item))
        return false;
      else
        throw new Exception("Inconsistent state in hash set toggle.");
    }
  }

  /// <summary>
  /// <see cref="Stack{T}"/> extension methods.
  /// </summary>
  public static class StackHelper
  {
    public static T PeekOrDefault<T>(this Stack<T> Source)
    {
      return Source.Count == 0 ? default : Source.Peek();
    }
  }

  /// <summary>
  /// <see cref="Queue{T}"/> extension methods.
  /// </summary>
  public static class QueueHelper
  {
    public static bool TryDequeue<T>(this Queue<T> Source, out T Item)
    {
      if (Source.Count > 0)
      {
        Item = Source.Dequeue();
        return true;
      }
      else
      {
        Item = default;
        return false;
      }
    }
  }

  public static class MemoryHelper
  {
    /// <summary>
    /// Extract a slice from the beginning of a byte array.
    /// </summary>
    /// <param name="ByteArray">The source data.</param>
    /// <param name="Length">The number of bytes to copy.</param>
    /// <returns><paramref name="Length"/> bytes from the start of <paramref name="ByteArray"/>.</returns>
    public static byte[] Slice(this byte[] ByteArray, int Length)
    {
      var ResultBuffer = new byte[Length];
      Array.Copy(ByteArray, ResultBuffer, Length);

      return ResultBuffer;
    }
    /// <summary>
    /// Compare this byte array to another byte array.
    /// </summary>
    /// <param name="LeftByteArray">This byte array.</param>
    /// <param name="RightByteArray">The byte array to compare to.</param>
    /// <returns>True if this byte array contains the same data as <paramref name="RightByteArray"/>; false otherwise.</returns>
    public static bool EqualTo(this byte[] LeftByteArray, byte[] RightByteArray)
    {
      var Length = LeftByteArray.Length;
      var Result = Length == RightByteArray.Length;
      var Index = 0;

      while (Result && Index < Length)
      {
        Result = LeftByteArray[Index] == RightByteArray[Index];

        Index++;
      }

      return Result;
    }
    /// <summary>
    /// Compare this byte array to another byte array.
    /// </summary>
    /// <param name="LeftByteArray">This byte array.</param>
    /// <param name="RightByteArray">The byte array to compare to.</param>
    /// <returns><para>Zero if the two arrays are the same length and contain the same data.</para>
    /// <para>- otherwise -</para>
    /// <para>If the two arrays are of differing length: the result of this.Length.CompareTo(<paramref name="RightByteArray"/>.Length)</para>
    /// <para>- otherwise -</para>
    /// <para>If the two arrays of the same length, the result of CompareTo() on the bytes of each array at the first index where the two arrays differ in content.</para>
    /// </returns>
    public static int CompareTo(this byte[] LeftByteArray, byte[] RightByteArray)
    {
      var Length = LeftByteArray.Length;
      var Result = Length.CompareTo(RightByteArray.Length);
      var Index = 0;

      while (Result == 0 && Index < Length)
      {
        Result = LeftByteArray[Index].CompareTo(RightByteArray[Index]);

        Index++;
      }

      return Result;
    }
    /// <summary>
    /// Convert this byte array to a hexadecimal string.
    /// </summary>
    /// <param name="ByteArray">This byte array.</param>
    /// <param name="Prefix">A string prefix to place before the generated hexadecimal characters.</param>
    /// <returns>A string representing this byte array in hexadecimal digits plus the prefix string if specified.</returns>
    public static string ToHexadecimal(this byte[] ByteArray, string Prefix = null)
    {
      var ResultArray = new char[(ByteArray.Length * 2) + (Prefix == null ? 0 : Prefix.Length)];

      var ResultArrayIndex = 0;

      if (Prefix != null)
      {
        foreach (var PrefixChar in Prefix)
          ResultArray[ResultArrayIndex++] = PrefixChar;
      }

      var HexadecimalTable = HexHelper.HexadecimalCharacters;

      foreach (var Byte in ByteArray)
      {
        ResultArray[ResultArrayIndex++] = HexadecimalTable[Byte >> 4];
        ResultArray[ResultArrayIndex++] = HexadecimalTable[Byte & 0xF];
      }

      return new string(ResultArray);
    }
    public static byte[] FromHexadecimal(this string Source)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Source.Length % 2 == 0, "Source string must be an even number of characters to convert to hexadecimal.");

      var Result = new byte[Source.Length / 2];
      var SourceIndex = 0;

      for (var ResultIndex = 0; ResultIndex < Result.Length; ResultIndex++)
      {
        var HexString = Source[SourceIndex++].ToString() + Source[SourceIndex++].ToString();
        var HexByte = Convert.ToByte(HexString, 16);

        Result[ResultIndex] = HexByte;
      }

      return Result;
    }
  }

  /// <summary>
  /// Static class with extension methods for number value types.
  /// </summary>
  public static class NumberHelper
  {
    /// <summary>
    /// Round this decimal value up to a certain level of accuracy.
    /// </summary>
    /// <param name="Source">This decimal value.</param>
    /// <param name="AccuracyValue">The level of accuracy required.</param>
    /// <returns>This decimal value, rounded up to the specified level of accuracy.</returns>
    public static decimal RoundUp(this decimal Source, decimal AccuracyValue)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(AccuracyValue == 0, "Cannot round to an accuracy of 0.");

      return (Source / AccuracyValue).Ceiling() * AccuracyValue;
    }
    /// <summary>
    /// Round this decimal value down to a certain level of accuracy.
    /// </summary>
    /// <param name="Source">This decimal value.</param>
    /// <param name="AccuracyValue">The level of accuracy required.</param>
    /// <returns>This decimal value, rounded down to the specified level of accuracy.</returns>
    public static decimal RoundDown(this decimal Source, decimal AccuracyValue)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(AccuracyValue == 0, "Cannot round to an accuracy of 0.");

      return (Source / AccuracyValue).Floor() * AccuracyValue;
    }
    /// <summary>
    /// Round this decimal value to a certain level of accuracy.
    /// </summary>
    /// <param name="Source">This decimal value.</param>
    /// <param name="AccuracyValue">The level of accuracy required.</param>
    /// <returns>This decimal value, rounded to the specified level of accuracy.</returns>
    public static decimal RoundNearest(this decimal Source, decimal AccuracyValue)
    {
      return Math.Round(Source / AccuracyValue) * AccuracyValue;
    }
    /// <summary>
    /// Round this nearest decimal value using bankers logic
    /// </summary>
    public static Inv.Money? RoundBankers(this Inv.Money? Money)
    {
      return Money != null ? Money.Value.RoundBankers() : (Inv.Money?)null;
    }
    public static decimal RoundBankers(this decimal Source)
    {
      return Source.RoundToEven(2);
    }
    public static Inv.Money? RoundPricing(this Inv.Money? Money)
    {
      return Money != null ? Money.Value.RoundPricing() : (Inv.Money?)null;
    }
    public static decimal RoundPricing(this decimal Source)
    {
      return Source.RoundAwayFromZero(2);
    }
    public static decimal RoundQuantity(this decimal Source)
    {
      return Source.RoundAwayFromZero(4);
    }
    public static bool IsRoundedQuantity(this decimal Source)
    {
      return Source.RoundQuantity() == Source;
    }
    /// <summary>
    /// Returns the value as an ordinal number string using numerals and letter suffixes.
    /// </summary>
    /// <remarks>
    /// <example>E.g.; "1st", "2nd", "3rd", "4th", etc.</example>
    /// </remarks>
    /// <param name="Value">The value to convert.</param>
    /// <returns>The value as an ordinal number string.</returns>
    public static string ToOrdinal(this int Value)
    {
      switch (Value % 100)
      {
        case 11:
        case 12:
        case 13:
          return Value.ToString() + "th";
      }

      return (Value % 10) switch
      {
        1 => Value.ToString() + "st",
        2 => Value.ToString() + "nd",
        3 => Value.ToString() + "rd",
        _ => Value.ToString() + "th",
      };
    }
    /// <inheritdoc cref="ToOrdinal(int)"/>
    public static string ToOrdinal(this long Value)
    {
      switch (Value % 100)
      {
        case 11:
        case 12:
        case 13:
          return Value.ToString() + "th";
      }

      return (Value % 10) switch
      {
        1 => Value.ToString() + "st",
        2 => Value.ToString() + "nd",
        3 => Value.ToString() + "rd",
        _ => Value.ToString() + "th",
      };
    }
    public static string ToDecimalString(this decimal Value)
    {
      return new decimal((double)Value).ToString();
    }
    /// <summary>
    /// Determines whether <paramref name="Value"/> is within the inclusive range from <paramref name="LeftValue"/> to <paramref name="RightValue"/>.
    /// </summary>
    /// <param name="Value">The value to check.</param>
    /// <param name="LeftValue">The lower bound of the inclusive range.</param>
    /// <param name="RightValue">The upper bound of the inclusive range.</param>
    /// <returns><see langword="true"/> if <paramref name="Value"/> is within the inclusive range from <paramref name="LeftValue"/> to <paramref name="RightValue"/>, otherwise <see langword="false"/></returns>
    public static bool Between(this int Value, int LeftValue, int RightValue)
    {
      return Value >= LeftValue && Value <= RightValue;
    }
    /// <inheritdoc cref="Between(int, int, int)"/>
    public static bool Between(this long Value, long LeftValue, long RightValue)
    {
      return Value >= LeftValue && Value <= RightValue;
    }
    public static decimal RoundAwayFromZero(this decimal Value, int DecimalPlaces = 0)
    {
      return Math.Round(Value, DecimalPlaces, MidpointRounding.AwayFromZero);
    }
    public static double RoundAwayFromZero(this double Value, int DecimalPlaces = 0)
    {
      return Math.Round(Value, DecimalPlaces, MidpointRounding.AwayFromZero);
    }
    public static decimal RoundToEven(this decimal Value, int DecimalPlaces = 0)
    {
      return Math.Round(Value, DecimalPlaces, MidpointRounding.ToEven);
    }
    public static double RoundToEven(this double Value, int DecimalPlaces = 0)
    {
      return Math.Round(Value, DecimalPlaces, MidpointRounding.ToEven);
    }
    public static decimal Ceiling(this decimal Value)
    {
      return Math.Ceiling(Value);
    }
    public static decimal Floor(this decimal Value)
    {
      return Math.Floor(Value);
    }
    public static decimal Truncate(this decimal Value)
    {
      return Math.Truncate(Value);
    }
    public static double Truncate(this double Value)
    {
      return Math.Truncate(Value);
    }
    public static int GetPrecision(this decimal Source)
    {
      return (int)BitConverter.GetBytes(decimal.GetBits(Source)[3])[2];
    }
    public static decimal GetFractionalPart(this decimal Value) => Value % 1.0m;
    public static bool HasFractionalPart(this decimal Value) => Value.GetFractionalPart() > 0.0m;
    /// <summary>
    /// Returns <paramref name="Value"/> clamped to the inclusive range of <paramref name="Min"/> and <paramref name="Max"/>.
    /// </summary>
    /// <param name="Value">The value to be clamped.</param>
    /// <param name="Min">The inclusive lower bound of the result.</param>
    /// <param name="Max">The inclusive upper bound of the result.</param>
    /// <returns><paramref name="Value"/> if it is within the range, else <paramref name="Min"/> if <paramref name="Value"/> is less than <paramref name="Min"/>, else <paramref name="Max"/>.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int Clamp(this int Value, int Min, int Max)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Min <= Max, $"{nameof(Min)} cannot be greater than {nameof(Max)}");

      if (Value < Min)
        return Min;

      if (Value > Max)
        return Max;

      return Value;
    }
    /// <summary>
    /// Returns <paramref name="Value"/> clamped to the inclusive range of <paramref name="Min"/> and <paramref name="Max"/>.
    /// </summary>
    /// <param name="Value">The value to be clamped.</param>
    /// <param name="Min">The inclusive lower bound of the result.</param>
    /// <param name="Max">The inclusive upper bound of the result.</param>
    /// <returns><paramref name="Value"/> if it is within the range, else <paramref name="Min"/> if <paramref name="Value"/> is less than <paramref name="Min"/>, else <paramref name="Max"/>.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int Clamp(this long Value, int Min, int Max)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Min <= Max, $"{nameof(Min)} cannot be greater than {nameof(Max)}");

      if (Value < Min)
        return Min;

      if (Value > Max)
        return Max;

      return (int)Value;
    }
    /// <summary>
    /// Returns <paramref name="Value"/> clamped to the inclusive range of <paramref name="Min"/> and <paramref name="Max"/>.
    /// </summary>
    /// <param name="Value">The value to be clamped.</param>
    /// <param name="Min">The inclusive lower bound of the result.</param>
    /// <param name="Max">The inclusive upper bound of the result.</param>
    /// <returns><paramref name="Value"/> if it is within the range, else <paramref name="Min"/> if <paramref name="Value"/> is less than <paramref name="Min"/>, else <paramref name="Max"/>.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static long Clamp(this long Value, long Min, long Max)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Min <= Max, $"{nameof(Min)} cannot be greater than {nameof(Max)}");

      if (Value < Min)
        return Min;

      if (Value > Max)
        return Max;

      return Value;
    }
    /// <summary>
    /// Returns <paramref name="Value"/> clamped to the inclusive range of <paramref name="Min"/> and <paramref name="Max"/>.
    /// </summary>
    /// <param name="Value">The value to be clamped.</param>
    /// <param name="Min">The inclusive lower bound of the result.</param>
    /// <param name="Max">The inclusive upper bound of the result.</param>
    /// <returns><paramref name="Value"/> if it is within the range, else <paramref name="Min"/> if <paramref name="Value"/> is less than <paramref name="Min"/>, else <paramref name="Max"/>.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static double Clamp(this double Value, double Min, double Max)
    {
      if (Inv.Assert.IsEnabled)
        // This is intentionally not Min <= Max in order to handle double.NaN
        Inv.Assert.Check(!(Min > Max), $"{nameof(Min)} cannot be greater than {nameof(Max)}");

      if (Value < Min)
        return Min;

      if (Value > Max)
        return Max;

      return Value;
    }

    public static IEnumerable<uint> NumberSeries(this uint Count)
    {
      for (var i = (uint)0; i < Count; i++)
        yield return i;
    }
    public static IEnumerable<int> NumberSeries(this int Count)
    {
      for (var i = 0; i < Count; i++)
        yield return i;
    }
    public static IEnumerable<long> NumberSeries(this long Count)
    {
      for (var i = 0L; i < Count; i++)
        yield return i;
    }
  }

  /// <summary>
  /// Extension methods for reflection.
  /// </summary>
  public static class ReflectionHelper
  {
    public static object CreateInstance(this Type Type)
    {
      return Activator.CreateInstance(Type);
    }
    public static object CreateInstance(this Type Type, Type ParamType, object ParamValue)
    {
      var Constructor = Type.GetTypeInfo().DeclaredConstructors.Find(C => C.GetParameters().Length == 1 && C.GetParameters()[0].ParameterType == ParamType);

      if (Constructor != null)
        return Constructor.Invoke(new object[] { ParamValue });

      return Activator.CreateInstance(Type, ParamValue);
    }
    public static object CreateInstance(this Type Type, Type ParamType1, object ParamValue1, Type ParamType2, object ParamValue2)
    {
      var Constructor = Type.GetTypeInfo().DeclaredConstructors.Find(C => C.GetParameters().Length == 2 && C.GetParameters()[0].ParameterType == ParamType1 && C.GetParameters()[1].ParameterType == ParamType2);

      if (Constructor != null)
        return Constructor.Invoke(new object[] { ParamValue1, ParamValue2 });

      return Activator.CreateInstance(Type, ParamValue1, ParamValue2);
    }
    public static ConstructorInfo ReflectionDefaultConstructor(this Type Type)
    {
      return Type.GetTypeInfo().DeclaredConstructors.Find(C => C.GetParameters().Length == 0);
    }
    public static Type[] GetReflectionTypes(this Assembly Assembly)
    {
      return Assembly.DefinedTypes.Select(T => T.AsType()).ToArray();
    }
    public static TypeInfo GetReflectionInfo(this Type type)
    {
      return type.GetTypeInfo();
    }
    public static FieldInfo GetReflectionField(this TypeInfo TypeInfo, string name)
    {
      return TypeInfo.GetReflectionFields().Find(M => M.Name == name);
    }
    public static FieldInfo[] GetReflectionFields(this TypeInfo TypeInfo)
    {
      return TypeInfo.DeclaredFields.ToArray();
    }
    public static FieldInfo[] GetReflectionFields(this Type Type)
    {
      return Type.GetRuntimeFields().ToArray();
    }
    public static PropertyInfo GetReflectionProperty(this Type Type, string Name)
    {
      return Type.GetTypeInfo().DeclaredProperties.Find(P => P.Name == Name);
    }
    public static PropertyInfo[] GetReflectionProperties(this TypeInfo TypeInfo)
    {
      return TypeInfo.DeclaredProperties.ToArray();
    }
    public static MethodInfo GetReflectionMethod(this Type type, string name)
    {
      return type.GetReflectionInfo().GetReflectionMethods().Find(M => M.Name == name);
    }
    public static MethodInfo GetReflectionMethod(this Type type, string name, Type[] ParameterTypeArray)
    {
      return type.GetReflectionInfo().GetReflectionMethods().Find(M => M.Name == name && ParameterTypeArray.ShallowEqualTo(M.GetParameters().Select(P => P.ParameterType).ToArray()));
    }
    public static MethodInfo[] GetReflectionMethods(this TypeInfo TypeInfo)
    {
      return TypeInfo.DeclaredMethods.ToArray();
    }
    public static Type[] GetReflectionGenericTypeArguments(this Type Type)
    {
      return Type.GenericTypeArguments;
    }
    public static MethodInfo GetReflectionInfo(this Delegate del)
    {
      return del.GetMethodInfo();
    }
    public static Type GetReflectionNestedType(this Type Type, string Name)
    {
      var Result = Type.GetTypeInfo().DeclaredNestedTypes.Find(T => T.Name == Name);

      return Result?.AsType();
    }
  }

  /// <summary>
  /// <see cref="int"/> extension methods.
  /// </summary>
  public static class Int32Helper
  {
    public static bool IsPowerOfTwo(this int Value)
    {
      var x = (ulong)Value;

      return (x != 0) && ((x & (x - 1)) == 0);
    }
    public static int RoundUpToPowerOf2(this int Value)
    {
      var v = (uint)Value;

      v--;
      v |= v >> 1;
      v |= v >> 2;
      v |= v >> 4;
      v |= v >> 8;
      v |= v >> 16;
      v++;

      return (int)v;
    }
  }

  /// <summary>
  /// Static class with extension methods for Boolean.
  /// </summary>
  public static class BooleanHelper
  {
    /// <summary>
    /// Return 'true' or 'false' to reflect the C# keywords.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static string ConvertToCSharpKeyword(this bool Source)
    {
      return Source.ToString().ToLowerInvariant();
    }
  }

  public static class DebugHelper
  {
    public static bool IsDebugMode()
    {
#if DEBUG
      return true;
#else
      return false;
#endif
    }
    public static void Break()
    {
      if (Debugger.IsAttached)
        Debugger.Break();
    }
  }

  public static class GrammarHelper
  {
    /// <summary>
    /// Return the relevant ownership verb (i.e. 'has' or 'have') for the given number of items.
    /// </summary>
    public static string Has(long Count)
    {
      if (Count == 1)
        return "has";

      return "have";
    }
    /// <summary>
    /// Return the relevant existence verb (i.e. 'is' or 'are') for the given number of items.
    /// </summary>
    public static string Is(long Count)
    {
      if (Count == 1)
        return "is";

      return "are";
    }
    /// <summary>
    /// Return the relevant past-tense existence verb (i.e. 'was' or 'were') for the given number of items.
    /// </summary>
    public static string Was(long Count)
    {
      if (Count == 1)
        return "was";

      return "were";
    }
    /// <summary>
    /// Return the relevant third-person indefinite article (i.e. 'it' or 'they') for the given number of items.
    /// </summary>
    public static string It(long Count)
    {
      if (Count == 1)
        return "it";

      return "they";
    }
    /// <summary>
    /// Return the relevant third-person definite article (i.e. 'this' or 'these') for the given number of items.
    /// </summary>
    public static string This(long Count)
    {
      if (Count == 1)
        return "this";

      return "these";
    }
    /// <summary>
    /// Return the relevant third-person present tense verb form of the word 'do'.
    /// <para>i.e. 'does' (singular) or 'do' (plural) for the given number of items.</para>
    /// </summary>
    public static string Does(long Count)
    {
      if (Count == 1)
        return "does";

      return "do";
    }
    /// <summary>
    /// Return the relevant third-person possessive pronoun (i.e. 'its' or 'their') for the given number of items.
    /// </summary>
    /// <param name="Count"></param>
    /// <returns></returns>
    public static string Its(long Count)
    {
      if (Count == 1)
        return "its";

      return "their";
    }
  }

  /// <summary>
  /// Static class with extension methods for <see cref="String"/>.
  /// </summary>
  public static class StringHelper
  {
    /// <summary>
    /// Returns first line in a string or entire string if no linebreaks are included
    /// </summary>
    /// <param name="str">String value</param>
    /// <returns>Returns first line in the string</returns>
    public static string FirstLine(this string str)
    {
      if (string.IsNullOrWhiteSpace(str))
        return str;

      var newLinePos = str.IndexOf(Environment.NewLine, StringComparison.CurrentCulture);

      return newLinePos >= 0 ? str.Substring(0, newLinePos) : str;
    }
    public static string Reverse(this string Source)
    {
      var CharArray = Source.ToCharArray();
      Array.Reverse(CharArray);
      return new string(CharArray);
    }
    public static string Prepend(this string Source, string Prepend, string Delimiter = "")
    {
      if (string.IsNullOrEmpty(Source))
        return Prepend;
      else if (string.IsNullOrEmpty(Prepend))
        return Source;
      else
        return Prepend + Delimiter + Source;
    }
    public static string Append(this string Source, string Append, string Delimiter = "")
    {
      if (string.IsNullOrEmpty(Source))
        return Append;
      else if (string.IsNullOrEmpty(Append))
        return Source;
      else
        return Source + Delimiter + Append;
    }
    public static string Append(this string Source, string Append, string Prefix, string Suffix)
    {
      if (string.IsNullOrEmpty(Source))
        return Append;
      else if (string.IsNullOrEmpty(Append))
        return Source;
      else
        return Source + Prefix + Append + Suffix;
    }
    public static string Surround(this string Source, string Surround)
    {
      return $"{Surround}{Source}{Surround}";
    }
    public static string SurroundSingleQuote(this string Source) => Source.Surround("'");
    public static string SurroundDoubleQuote(this string Source) => Source.Surround("\"");
    public static bool Contains(this string Source, string ToCheck, StringComparison Comparison)
    {
      return Source.IndexOf(ToCheck, Comparison) >= 0;
    }
    public static bool ContainsAny(this string Source, IEnumerable<string> SearchTerms, StringComparison Comparison = StringComparison.CurrentCultureIgnoreCase)
    {
      return SearchTerms.Any(T => Source.Contains(T, Comparison));
    }
    public static bool ContainsAll(this string Source, IEnumerable<string> SearchTerms, StringComparison Comparison = StringComparison.CurrentCultureIgnoreCase)
    {
      return SearchTerms.All(T => Source.Contains(T, Comparison));
    }
    public static bool ContainsOnly(this string Source, char[] Characters)
    {
      if (Source == null)
        return false;

      foreach (var Character in Source)
      {
        if (!Characters.Contains(Character))
          return false;
      }

      return true;
    }
    public static string StripWhitespace(this string Source)
    {
      if (Source == null)
        return null;

      return new string(Source.ToCharArray().Where(C => !char.IsWhiteSpace(C)).ToArray());
    }
    public static string StripPunctuation(this string Source)
    {
      if (Source == null)
        return null;

      return new string(Source.ToCharArray().Where(C => !char.IsPunctuation(C)).ToArray());
    }
    public static string ReplacePunctuation(this string Source, char Replacement)
    {
      if (Source == null)
        return null;

      return new string(Source.ToCharArray().Select(C => char.IsPunctuation(C) ? Replacement : C).ToArray());
    }
    public static bool IsWhitespace(this string Source)
    {
      return Source != null && string.IsNullOrWhiteSpace(Source);
    }
    public static bool ToBoolean(this string Source)
    {
      if (Source == null)
        throw new System.Exception("String must not be null and must be a valid boolean value.");

      if (string.Equals(Source, "False", StringComparison.OrdinalIgnoreCase))
        return false;

      if (string.Equals(Source, "True", StringComparison.OrdinalIgnoreCase))
        return true;

      throw new System.Exception($"String '{Source}' is not a valid boolean value.");
    }
    public static string ToTitleCase(this string Source)
    {
      if (Source == null)
        return null;

      var WordArray = Source.Split(' ');
      for (var WordIndex = 0; WordIndex < WordArray.Length; WordIndex++)
      {
        if (WordArray[WordIndex].Length == 0)
          continue;

        var FirstChar = char.ToUpper(WordArray[WordIndex][0]);
        var RestChars = "";

        if (WordArray[WordIndex].Length > 1)
          RestChars = WordArray[WordIndex].Substring(1).ToLower();

        WordArray[WordIndex] = FirstChar + RestChars;
      }

      return string.Join(" ", WordArray);
    }
    public static string ToTitleCaseInvariant(this string Source)
    {
      if (Source == null)
        return null;

      var WordArray = Source.Split(' ');
      for (var WordIndex = 0; WordIndex < WordArray.Length; WordIndex++)
      {
        if (WordArray[WordIndex].Length == 0)
          continue;

        var FirstChar = char.ToUpperInvariant(WordArray[WordIndex][0]);
        var RestChars = "";

        if (WordArray[WordIndex].Length > 1)
          RestChars = WordArray[WordIndex].Substring(1).ToLowerInvariant();

        WordArray[WordIndex] = FirstChar + RestChars;
      }

      return string.Join(" ", WordArray);
    }
    public static string ToSentenceCase(this string Source)
    {
      // start by converting entire string to lower case
      var LowerCase = Source.ToLower();

      // matches the first sentence of a string, as well as subsequent sentences
      var Expression = new System.Text.RegularExpressions.Regex(@"(^[a-z])|\.\s+(.)", System.Text.RegularExpressions.RegexOptions.ExplicitCapture);

      // MatchEvaluator delegate defines replacement of sentence starts to uppercase
      return Expression.Replace(LowerCase, S => S.Value.ToUpper());
    }
    public static string ToSentenceCaseInvariant(this string Source)
    {
      // start by converting entire string to lower case
      var LowerCase = Source.ToLowerInvariant();

      // matches the first sentence of a string, as well as subsequent sentences
      var Expression = new System.Text.RegularExpressions.Regex(@"(^[a-z])|\.\s+(.)", System.Text.RegularExpressions.RegexOptions.ExplicitCapture | System.Text.RegularExpressions.RegexOptions.CultureInvariant);

      // MatchEvaluator delegate defines replacement of sentence starts to uppercase
      return Expression.Replace(LowerCase, S => S.Value.ToUpper());
    }
    /// <summary>
    /// Singularise this <see cref="string"/> for the given number of instances.
    /// Note: Value is expected to be a plural form. As such, if Count is supplied and != 1, Value will be returned unchanged.
    /// </summary>
    /// <param name="Source">This <see cref="string"/>.</param>
    /// <param name="Count">The number to pluralise for.</param>
    /// <returns>A singular version of this <see cref="string"/>.</returns>
    public static string Singular(this string Source, long Count = 1)
    {
      if (Source == null)
        return null;

      var Result = Source;

      if (Source.Length > 0 && Count == 1)
      {
        string[] Exceptions = { "series" };
        char[] ArrayJXO = { 'j', 'x', 'o' };
        char[] ArrayCS = { 'c', 's' };
        char[] ArraySH = { 's', 'h' };

        var StringLength = Source.Length;

        if ((StringLength <= 0) || (StringLength == 1) || (Source[StringLength - 1] != 's'))
        {
          Result = Source;
        }
        else
        {
          var Exception = false;
          var Loop = 0;
          var ExceptionCount = Exceptions.Length;

          while (!Exception && (Loop < ExceptionCount))
          {
            Exception = Source.EndsWith(Exceptions[Loop], StringComparison.CurrentCultureIgnoreCase);

            Loop++;
          }

          if (Exception)
          {
            Result = Source;
          }
          else if (Source[StringLength - 2] == 'e')
          {
            if ((StringLength > 2) && (Source[StringLength - 3] == 'i'))
            {
              if (StringLength > 3)
                Result = Source.Substring(0, StringLength - 3) + 'y';
              else
                Result = Source.Substring(0, StringLength - 1);
            }
            else if (((StringLength > 3) &&
                    (ArrayJXO.Contains(Source[StringLength - 3]) || // eg. boxes
                    (Source[StringLength - 4].Equals('z') && Source[StringLength - 3].Equals('z')) || // eg. fuzzes
                    (Source.EndsWith("aliases", StringComparison.CurrentCultureIgnoreCase)) || // eg. aliases
                    (ArrayCS.Contains(Source[StringLength - 4]) && ArraySH.Contains(Source[StringLength - 3])) // eg. beaches, gasses
                )))
            {
              Result = Source.Substring(0, StringLength - 2);
            }
            else
            {
              Result = Source.Substring(0, StringLength - 1);
            }
          }
          else
          {
            Result = Source.Substring(0, StringLength - 1);
          }
        }
      }

      return Result;
    }
    public static string RelativePlural(this string Source, long Count)
    {
      if (Count == 1)
        return Source;
      else
        return $"{Count} {Source.Plural(Count)}";
    }
    public static string Plural(this string Source, decimal Count)
    {
      return Plural(Source, Count == 1 ? 1L : 0L);
    }
    public static string Plural(this string Source, double Count)
    {
      return Plural(Source, Count == 1 ? 1L : 0L);
    }
    /// <summary>
    /// Pluralise this <see cref="string"/> for the given number of instances.
    /// Note: Value is expected to be a singular form.
    /// </summary>
    /// <param name="Source">This <see cref="string"/>.</param>
    /// <param name="Count">The number to pluralise for.</param>
    /// <returns>A plural version of this <see cref="string"/>.</returns>
    public static string Plural(this string Source, long Count = 0)
    {
      if (Source == null)
        return null;

      var Result = Source;

      if (Source.Length > 0 && Count != 1)
      {
        var IsException = false;
        var Index = 0;

        while (!IsException && Index < PluralExceptionArray.Length)
        {
          IsException = Source.EndsWith(PluralExceptionArray[Index], StringComparison.CurrentCultureIgnoreCase);
          Index++;
        }

        if (!IsException)
        {
          Result = Source;

          string Pluralisation;
          var LastCharacter = Source[Source.Length - 1];

          switch (char.ToUpper(LastCharacter))
          {
            case 'H':
              var SecondLastCharacter = char.ToUpper(Source[Source.Length - 2]);
              if (Source.Length > 1 && (SecondLastCharacter == 'T' || SecondLastCharacter == 'G'))
                Pluralisation = "s";
              else
                Pluralisation = "es";
              break;

            case 'S':
              if (Source.Length > 2 && Source.Substring(Source.Length - 2).Equals("es", StringComparison.OrdinalIgnoreCase))
                Pluralisation = "";
              else
                Pluralisation = "es";
              break;

            case 'X':
            case 'J':
            case 'Z':
              Pluralisation = "es";
              break;

            case 'Y':
              if (Source.EndsWith("yby", StringComparison.OrdinalIgnoreCase))
              {
                Pluralisation = "s"; // eg. Laybys
              }
              else if (Source.Length > 2 && (!PluralVowelArray.Contains(Source[Source.Length - 2])))
              {
                Result = Source.Substring(0, Source.Length - 1);
                Pluralisation = "ies";
              }
              else
              {
                Pluralisation = "s";
              }
              break;

            default:
              Pluralisation = "s";
              break;
          }

          if (char.IsUpper(LastCharacter))
            Pluralisation = Pluralisation.ToUpper();

          Result += Pluralisation;
        }
      }

      return Result;
    }
    /// <summary>
    /// Return the relevant indefinite article (i.e. 'a' or 'an') for the provided <see cref="string"/>.
    /// <para>Words that start with a consonant sound use the indefinite article 'a', all others use 'an'.</para>
    /// </summary>
    /// <param name="Source">This <see cref="string"/>.</param>
    /// <returns>The relevant existence verb for the number of items.</returns>
    public static string IndefiniteArticle(this string Source)
    {
      return Source.StartsWithVowelSound() ? "an" : "a";
    }
    /// <summary>
    /// Return this <see cref="String"/>, or null if this <see cref="String"/> is empty.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <returns>This <see cref="String"/>, or null if this <see cref="String"/> is empty.</returns>
    public static string EmptyAsNull(this string Source)
    {
      if (Source == "")
        return null;
      else
        return Source;
    }
    public static string EmptyOrWhitespaceAsNull(this string Source)
    {
      if (Source == "" || string.IsNullOrWhiteSpace(Source))
        return null;
      else
        return Source;
    }
    public static string EmptyOrWhitespaceAsEmpty(this string Source)
    {
      if (Source == "" || string.IsNullOrWhiteSpace(Source))
        return "";
      else
        return Source;
    }
    /// <summary>
    /// Return this <see cref="String"/>, or an empty string if this <see cref="String"/> is null.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <returns>This <see cref="String"/>, or an empty string if this <see cref="String"/> is null.</returns>
    public static string NullAsEmpty(this string Source)
    {
      if (Source == null)
        return "";
      else
        return Source;
    }
    /// <summary>
    /// 'Reduce' a string by removing every second occurrence of <paramref name="Character"/> from the string.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <param name="Character">The character to remove.</param>
    /// <returns>A reduced string where every second occurrence of <paramref name="Character"/> has been removed.</returns>
    public static string Reduce(this string Source, char Character)
    {
      if (Source == null)
        return null;

      var ResultStringBuilder = new StringBuilder();

      var Reducing = false;

      foreach (var Item in Source)
      {
        if (Item == Character)
        {
          if (!Reducing)
          {
            Reducing = true;
            ResultStringBuilder.Append(Character);
          }
        }
        else
        {
          Reducing = false;
          ResultStringBuilder.Append(Item);
        }
      }

      return ResultStringBuilder.ToString();
    }
    /// <summary>
    /// Count the number of occurrences of <paramref name="Character"/> in this <see cref="String"/>.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <param name="Character">The character to count.</param>
    /// <returns>The number of occurrences of <paramref name="Character"/> in this <see cref="String"/>.</returns>
    public static int Count(this string Source, char Character)
    {
      var Result = 0;

      if (Source != null)
      {
        foreach (var Item in Source)
        {
          if (Item == Character)
            Result++;
        }
      }

      return Result;
    }
    /// <summary>
    /// Keep only numeric or specifically specified characters of <see cref="String"/>.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <param name="Keep">The character other than digits to keep.</param>
    /// <returns>A copy of this <see cref="String"/> where any characters that aren't digits or specified in <paramref name="Keep"/> have been removed.</returns>
    public static string KeepNumeric(this string Source, params char[] Keep)
    {
      if (Source == null)
        return null;

      return Source.KeepOnly(Item => Item.IsNumeric(), Keep);
    }
    /// <summary>
    /// Keep only alphabetic or specifically specified characters of <see cref="String"/>.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <param name="Keep">The character other than alpha character to keep.</param>
    /// <returns>A copy of this <see cref="String"/> where any characters that aren't alphabet characters or specified in <paramref name="Keep"/> have been removed.</returns>
    public static string KeepAlphabetic(this string Source, params char[] Keep)
    {
      if (Source == null)
        return null;

      return Source.KeepOnly(Item => Item.IsAlphabetic(), Keep);
    }
    /// <summary>
    /// Keep only characters that are validated by the Validate parameter or specifically allowed characters.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <param name="Validate">The predicate that tests each character. Return true for allowed characters and false otherwise.</param>
    /// <param name="Keep">The character other than digits to keep.</param>
    /// <returns>A copy of this <see cref="String"/> where any characters that aren't allowed by <paramref name="Validate"/> or specified in <paramref name="Keep"/> have been removed.</returns>
    public static string KeepOnly(this string Source, Func<char, bool> Validate, params char[] Keep)
    {
      if (Source == null)
        return null;

      var Builder = new StringBuilder();

      foreach (var Character in Source)
      {
        if (Validate(Character) || Keep.Contains(Character))
          Builder.Append(Character);
      }

      return Builder.ToString();
    }
    /// <summary>
    /// Strip every occurrence of any character in <paramref name="StripArray"/> from this <see cref="String"/>.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <param name="StripArray">The characters to strip from this <see cref="String"/>.</param>
    /// <returns>A copy of this <see cref="String"/> where any characters in <paramref name="StripArray"/> have been removed.</returns>
    public static string Strip(this string Source, params char[] StripArray)
    {
      if (Source == null)
        return null;

      var Result = new StringBuilder();

      foreach (var Index in Source)
      {
        if (!StripArray.Contains(Index))
          Result.Append(Index);
      }

      return Result.ToString();
    }
    public static string Strip(this string Source, Func<char, bool> StripFunction)
    {
      if (Source == null)
        return null;

      var Result = new StringBuilder();

      foreach (var Index in Source)
      {
        if (!StripFunction(Index))
          Result.Append(Index);
      }

      return Result.ToString();
    }
    /// <summary>
    /// Transform a Pascal case string to sentence case on the specified <see cref="String"/>.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <returns>Sentence case version of this <see cref="String"/>.</returns>
    public static string PascalCaseToSentenceCase(this string Source)
    {
      if (Source == null || Source.Length <= 1)
        return Source;

      var Previous = Source[0];
      var Result = new StringBuilder();

      Result.Append(Previous);

      for (var Index = 1; Index < Source.Length - 1; Index++)
      {
        var Current = Source[Index];
        var Next = Source[Index + 1];

        if (char.IsWhiteSpace(Current))
        {
          Result.Append(Current);
        }
        else if (Current == '_')
        {
          Result.Append(' ');
        }
        else if (char.IsUpper(Previous) && char.IsUpper(Current) && char.IsLower(Next))
        {
          Result.Append(' ');

          if (Index > 1)
            Result.Append(char.ToLower(Current));
          else
            Result.Append(Current);
        }
        else
        {
          if (char.IsLower(Previous) && char.IsUpper(Current))
            Result.Append(' ');

          if (Index > 1 && char.IsLower(Next))
            Result.Append(char.ToLower(Current));
          else
            Result.Append(Current);
        }

        Previous = Current;
      }

      var Last = Source[Source.Length - 1];

      if (!char.IsUpper(Previous) && char.IsUpper(Last))
        Result.Append(' ');

      Result.Append(Last);

      return Result.ToString();
    }
    /// <summary>
    /// Transform a Pascal case string to title case on the specified <see cref="String"/>.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <returns>Title case version of this <see cref="String"/>.</returns>
    public static string PascalCaseToTitleCase(this string Source)
    {
      if (Source == null || Source.Length <= 1)
        return Source;

      var Previous = Source[0];
      var Result = new StringBuilder();

      Result.Append(Previous);

      for (var Index = 1; Index < Source.Length - 1; Index++)
      {
        var Current = Source[Index];
        var Next = Source[Index + 1];

        if (char.IsWhiteSpace(Current))
        {
          Result.Append(Current);
        }
        else if (Current == '_')
        {
          Result.Append(' ');
        }
        else if (char.IsUpper(Previous) && char.IsUpper(Current) && char.IsLower(Next))
        {
          Result.Append(' ');
          Result.Append(Current);
        }
        else if (char.IsLower(Previous) && char.IsUpper(Current))
        {
          Result.Append(' ');
          Result.Append(Current);
        }
        else
        {
          Result.Append(Current);
        }

        Previous = Current;
      }

      var Last = Source[Source.Length - 1];

      if (!char.IsUpper(Previous) && char.IsUpper(Last))
        Result.Append(' ');

      Result.Append(Last);

      return Result.ToString();
    }
    public static string ProperCaseToCommonCase(this string Source)
    {
      if (Source == null || Source.Length <= 1)
        return Source;

      var Result = new StringBuilder();

      var AllCapsRun = false;

      for (var Index = 0; Index < Source.Length - 1; Index++)
      {
        var Current = Source[Index];
        var Next = Source[Index + 1];

        if (!char.IsUpper(Current))
        {
          Result.Append(Current);
        }
        else if (char.IsUpper(Next))
        {
          Result.Append(Current);
          AllCapsRun = true;
        }
        else if (AllCapsRun)
        {
          AllCapsRun = false;
          Result.Append(Current);
        }
        else
        {
          Result.Append(char.ToLower(Current));
        }
      }

      var Last = Source[Source.Length - 1];

      if (AllCapsRun)
        Result.Append(Last);
      else
        Result.Append(char.ToLower(Last));

      return Result.ToString();
    }
    public static string CapitaliseFirstCharacter(this string Source)
    {
      if (string.IsNullOrWhiteSpace(Source))
        return Source;

      var Builder = new StringBuilder(Source.Length);

      Builder.Append(Source[0].ToString().ToUpper());

      if (Source.Length > 1)
        Builder.Append(Source.Substring(1));

      return Builder.ToString();
    }

    public static long? ParseOrDefault(string Value, long? Default = null)
    {
      if (Value != null && long.TryParse(Value, out long Result))
        return Result;
      else
        return Default;
    }

    public static bool Contains(this string Source, params char[] CharacterArray)
    {
      if (string.IsNullOrEmpty(Source))
        return false;

      return Source.GetEnumerable().Intersect(CharacterArray).Any();
    }

    public static bool StartsWith(this string Source, params char[] CharacterArray)
    {
      if (string.IsNullOrEmpty(Source))
        return false;

      return CharacterArray.Contains(Source[0]);
    }
    public static bool StartsWith(this string Source, params string[] StringArray)
    {
      if (string.IsNullOrEmpty(Source))
        return false;

      foreach (var String in StringArray)
      {
        if (Source.StartsWith(String))
          return true;
      }

      return false;
    }
    public static bool EndsWith(this string Source, params char[] CharacterArray)
    {
      if (string.IsNullOrEmpty(Source))
        return false;

      return CharacterArray.Contains(Source[Source.Length - 1]);
    }
    public static bool EndsWith(this string Source, params string[] StringArray)
    {
      if (string.IsNullOrEmpty(Source))
        return false;

      foreach (var String in StringArray)
        if (Source.EndsWith(String))
          return true;

      return false;
    }
    public static string ExtractLines(this string Text, int LineCount, out bool HasTruncated)
    {
      HasTruncated = false;

      var ExtractedLines = Text.TrimStart('\r', '\n').TrimEnd('\r', '\n');

      var Lines = ExtractedLines.Split(new string[] { "\r\n" }, StringSplitOptions.None);
      if (Lines.Length > LineCount)
      {
        ExtractedLines = string.Empty;

        for (int Index = 0; Index < LineCount; Index++)
        {
          ExtractedLines += Lines[Index];

          if (Index < (LineCount - 1))
            ExtractedLines += "\r\n";
        }

        HasTruncated = true;
      }

      return ExtractedLines;
    }

    /// <summary>
    /// Count the number of matching characters between this <see cref="String"/> and the specified <see cref="String"/>.
    /// </summary>
    /// <param name="Left">This <see cref="String"/>.</param>
    /// <param name="Right">The <see cref="String"/> to compare to.</param>
    /// <returns>The zero-based index of the first mismatch between this <see cref="String"/> and <paramref name="Right"/>.</returns>
    public static int StartsWithMatchLength(this string Left, string Right)
    {
      var LeftLength = Left.Length;
      var RightLength = Right.Length;
      var MaxLength = Math.Min(LeftLength, RightLength);

      int MaxIndex;

      for (MaxIndex = 0; MaxIndex < MaxLength; MaxIndex++)
      {
        if (Left[MaxIndex] != Right[MaxIndex])
          break;
      }

      return MaxIndex;
    }
    /// <summary>
    /// Calculate the Levenshtein distance between this <see cref="String"/> and another <see cref="String"/>.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <param name="Target">The <see cref="String"/> to calculate the Levenshtein distance to.</param>
    /// <returns>The Levenshtein distance between this <see cref="String"/> and <paramref name="Target"/>.</returns>
    public static int LevenshteinDistance(this string Source, string Target)
    {
      var N = Source.Length;
      var M = Target.Length;
      var D = new int[N + 1, M + 1];

      // Step 1
      if (N == 0)
        return M;

      if (M == 0)
        return N;

      // Step 2
      for (var i = 0; i <= N; D[i, 0] = i++)
      {
      }
      for (var j = 0; j <= M; D[0, j] = j++)
      {
      }

      // Step 3
      for (var i = 1; i <= N; i++)
      {
        //Step 4
        for (var j = 1; j <= M; j++)
        {
          // Step 5
          var Cost = (Target[j - 1] == Source[i - 1]) ? 0 : 1;

          // Step 6
          D[i, j] = Math.Min(Math.Min(D[i - 1, j] + 1, D[i, j - 1] + 1), D[i - 1, j - 1] + Cost);
        }
      }

      // Step 7
      return D[N, M];
    }
    public static string Replace(this string Source, Func<char, bool> FindFunction, char ReplaceCharacter)
    {
      if (Source == null)
        return null;

      var Result = new StringBuilder();

      foreach (var Index in Source)
      {
        if (FindFunction(Index))
          Result.Append(ReplaceCharacter);
        else
          Result.Append(Index);
      }

      return Result.ToString();
    }
    /// <summary>
    /// Returns a new string in which all occurrences of all items in the specified string array are replaced with another specified string.
    /// </summary>
    /// <param name="Source"></param>
    /// <param name="FindArray">The strings to be replaced.</param>
    /// <param name="ReplaceValue">The string to replace all occurrences of the items in FindArray.</param>
    /// <returns>A string that is equivalent to the Source string except that all instances of all items in FindArray are replaced with ReplaceValue. If no replacements are found, the method returns the unchanged string.</returns>
    public static string Replace(this string Source, string[] FindArray, string ReplaceValue)
    {
      var Result = Source;

      if (Result != null)
      {
        foreach (var Find in FindArray)
          Result = Result.Replace(Find, ReplaceValue);
      }

      return Result;
    }
    public static string Replace(this string Source, string FindValue, string ReplaceValue, StringComparison Comparison)
    {
      var Result = new StringBuilder();

      var PreviousIndex = 0;
      var CurrentIndex = Source.IndexOf(FindValue, Comparison);
      while (CurrentIndex != -1)
      {
        Result.Append(Source.Substring(PreviousIndex, CurrentIndex - PreviousIndex));
        Result.Append(ReplaceValue);
        CurrentIndex += FindValue.Length;

        PreviousIndex = CurrentIndex;
        CurrentIndex = Source.IndexOf(FindValue, CurrentIndex, Comparison);
      }
      Result.Append(Source.Substring(PreviousIndex));

      return Result.ToString();
    }
    public static string Replace(this string Source, char[] FindValue, char ReplaceValue)
    {
      return Replace(Source, Index => FindValue.Contains(Index), ReplaceValue);
    }
    public static string ReplaceStart(this string Source, string FindValue, string ReplaceValue)
    {
      if (Source.StartsWith(FindValue))
        return ReplaceValue + Source.Substring(FindValue.Length);
      else
        return Source;
    }
    public static string ReplaceEnd(this string Source, string FindValue, string ReplaceValue)
    {
      if (Source.EndsWith(FindValue))
        return Source.Substring(0, Source.Length - FindValue.Length) + ReplaceValue;
      else
        return Source;
    }
    public static string RemoveSequentialDuplicates(this string Source)
    {
      if (Source.Length <= 1)
      {
        return Source;
      }
      else
      {
        var Previous = Source[0];
        var Result = new StringBuilder();

        Result.Append(Previous);

        for (var Index = 1; Index < Source.Length; Index++)
        {
          var Current = Source[Index];

          if (Current != Previous)
            Result.Append(Current);

          Previous = Current;
        }

        return Result.ToString();
      }
    }
    public static string NormaliseSpaces(this string Source, bool Trim = true)
    {
      var Result = Trim ? Source.Trim() : Source;

      var Regex = new System.Text.RegularExpressions.Regex(@"[ ]{2,}", System.Text.RegularExpressions.RegexOptions.None);
      Result = Regex.Replace(Result, @" ");

      return Result;
    }

    /// <summary>
    /// Returns a string array that contains the substrings in this string that are delimited by elements of a specified string array. A parameter specifies whether to return empty array elements.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <param name="Separator">The strings to split the <see cref="String"/> with.</param>
    /// <returns> An array whose elements contain the substrings in this string that are delimited by one or more strings in separator. For more information, see the Remarks section. </returns>
    public static string[] Split(this string Source, params string[] Separator)
    {
      return Source.Split(Separator, StringSplitOptions.None);
    }
    public static string[] Split(this string Source, char Separator, StringSplitOptions Options)
    {
      return Source.Split(new[] { Separator }, Options);
    }
    public static string[] Split(this string Source, string Separator, StringSplitOptions Options)
    {
      return Source.Split(new[] { Separator }, Options);
    }
    public static IEnumerable<string> SplitLines(this string Source)
    {
      if (Source == null)
        yield break;

      using (var StringReader = new System.IO.StringReader(Source))
      {
        string Line;
        while ((Line = StringReader.ReadLine()) != null)
          yield return Line;
      }
    }
    public static List<string> QuotedSplit(this string Source, params char[] Delimiter)
    {
      var EscapeChar = '\\';
      var PhraseChar = '"';
      var IgnoreDelimiter = false;
      var PreviousWasEscape = false;

      var Pieces = new List<string>();
      var ContentLength = Source.Length;

      var CurrentPiece = new StringBuilder();
      for (var i = 0; i < ContentLength; i++)
      {
        var CurrentChar = Source[i];

        if (CurrentChar == PhraseChar && !PreviousWasEscape)
        {
          if (IgnoreDelimiter)
          {
            if (CurrentPiece.Length > 0)
            {
              Pieces.Add(CurrentPiece.ToString());
              CurrentPiece = new StringBuilder();
            }

            IgnoreDelimiter = false;
          }
          else
          {
            IgnoreDelimiter = true;
          }
        }
        else if (Delimiter.Contains(CurrentChar))
        {
          if (!IgnoreDelimiter)
          {
            if (CurrentPiece.Length > 0)
            {
              Pieces.Add(CurrentPiece.ToString());
              CurrentPiece = new StringBuilder();
            }
          }
          else
          {
            CurrentPiece.Append(CurrentChar);
          }
        }
        else
        {
          CurrentPiece.Append(CurrentChar);
        }

        PreviousWasEscape = (CurrentChar == EscapeChar);
      }

      if (CurrentPiece.Length > 0)
        Pieces.Add(CurrentPiece.ToString());

      return Pieces;
    }

    public static string WordWrap(this string Source, int WrapWidth)
    {
      var PartList = new List<string>();
      var StartIndex = 0;
      while (true)
      {
        var NextIndex = Source.IndexOfAny(new char[] { ' ', '-', '\t' }, StartIndex);

        if (NextIndex == -1)
        {
          PartList.Add(Source.Substring(StartIndex));
          break;
        }

        var NextWord = Source.Substring(StartIndex, NextIndex - StartIndex);
        var NextChar = Source.Substring(NextIndex, 1)[0];

        if (char.IsWhiteSpace(NextChar))
        {
          PartList.Add(NextWord);
          PartList.Add(NextChar.ToString());
        }
        else
        {
          PartList.Add(NextWord + NextChar);
        }

        StartIndex = NextIndex + 1;
      }

      var CurrentLineLength = 0;
      var StringBuilder = new StringBuilder();

      foreach (var PartItem in PartList)
      {
        var Part = PartItem;

        if (CurrentLineLength + Part.Length > WrapWidth)
        {
          if (CurrentLineLength > 0)
          {
            StringBuilder.Append(Environment.NewLine);
            CurrentLineLength = 0;
          }

          while (Part.Length > WrapWidth)
          {
            StringBuilder.Append(Part.Substring(0, WrapWidth - 1) + "-");
            Part = Part.Substring(WrapWidth - 1);

            StringBuilder.Append(Environment.NewLine);
          }

          Part = Part.TrimStart();
        }

        StringBuilder.Append(Part);
        CurrentLineLength += Part.Length;
      }

      return StringBuilder.ToString();
    }

    public static string ExcludeBefore(this string Source, string Exclusion, StringComparison StringComparison = default)
    {
      var ExclusionLength = Exclusion.Length;
      var SourceLength = Source.Length;

      if (SourceLength >= ExclusionLength && Source.Substring(0, ExclusionLength).Equals(Exclusion, StringComparison))
        return Source.Substring(ExclusionLength, SourceLength - ExclusionLength);
      else
        return Source;
    }
    public static bool HasBeforeAndAfter(this string Source, string Before, string After)
    {
      return Source.StartsWith(Before) && Source.EndsWith(After);
    }
    public static bool HasBeforeAndAfter(this string Source, string Match)
    {
      return Source.StartsWith(Match) && Source.EndsWith(Match);
    }
    public static string ExcludeAfter(this string Source, string Exclusion)
    {
      var ExclusionLength = Exclusion.Length;
      var SourceLength = Source.Length;

      if (SourceLength >= ExclusionLength && Source.Substring(SourceLength - ExclusionLength, ExclusionLength).CompareTo(Exclusion) == 0)
        return Source.Substring(0, SourceLength - ExclusionLength);
      else
        return Source;
    }

    public static int CompareToNullable(this string A, string B, StringComparison Comparison)
    {
      return string.Compare(A, B, Comparison);
    }
    public static int CompareToNullable(this string A, string B)
    {
      return string.Compare(A, B);
    }

    public static string RemoveLast(this string Source, int Length)
    {
      return Source.Substring(0, Source.Length - Length);
    }
    public static string Truncate(this string Source, int Length)
    {
      if (Source != null && Source.Length > Length)
        return Source.Substring(0, Length);
      else
        return Source;
    }

    public static List<string> Caption(this string Source, int MaxLines)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(MaxLines > 1, "MaxLines must be more than 1.");

      var CaptionPieces = CaptionSplit(Source);
      if (CaptionPieces.Count <= MaxLines)
        return CaptionPieces;

      var PermutationResultList = CalculatePermutations(CaptionPieces, MaxLines);

      Permutation ShortestPermutation = null;
      var ShortestPermutationLength = Source.Length;

      foreach (var Permutation in PermutationResultList)
      {
        var PermutationLength = Permutation.LineList.Max(Line => Line.Length);
        if (PermutationLength < ShortestPermutationLength)
        {
          ShortestPermutationLength = PermutationLength;
          ShortestPermutation = Permutation;
        }
      }

      return (ShortestPermutation != null) ? ShortestPermutation.LineList : CaptionPieces;
    }
    public static string[] SplitGroupedBy(this string Source, char SplitCharacter, char GroupCharacter)
    {
      if (!Source.Contains(GroupCharacter))
        return Source.Split(SplitCharacter);
      else
      {
        var Results = new List<string>();
        var InGroup = false;
        var LastSplit = 0;

        for (int Index = 0; Index < Source.Length; Index++)
        {
          var Character = Source[Index];

          if (Character == GroupCharacter)
          {
            InGroup = !InGroup;

            if (Index != Source.Length - 1)
              continue;
          }

          if ((!InGroup && Character == SplitCharacter) | Index == Source.Length - 1)
          {
            var Result = Source.Substring(LastSplit, (Index + 1) - LastSplit).Trim().ExcludeBefore(GroupCharacter.ToString()).ExcludeAfter(GroupCharacter.ToString());
            Results.Add(Result);
            LastSplit = Index;
            if (Character == SplitCharacter)
              LastSplit += 1;
          }
        }

        return Results.ToArray();
      }
    }
    public static IEnumerable<string> Batch(this string Source, int Size)
    {
      var Index = 0;

      while (Index + Size < Source.Length)
      {
        yield return Source.Substring(Index, Size);
        Index += Size;
      }

      if (Index < Source.Length)
        yield return Source.Substring(Index, Source.Length - Index);
    }

    public static bool IsLetter(this string Source)
    {
      return Source.All(Index => char.IsLetter(Index));
    }
    public static bool IsLowerOrNonLetter(this string Source)
    {
      return Source.All(Index => char.IsLower(Index) || !char.IsLetter(Index));
    }
    public static bool IsUpperOrNonLetter(this string Source)
    {
      return Source.All(Index => char.IsUpper(Index) || !char.IsLetter(Index));
    }
    public static bool IsDigit(this string Source)
    {
      return Source.Length > 0 && Source.All(Index => char.IsDigit(Index));
    }
    public static bool IsDigitOrNonLetter(this string Source)
    {
      return Source.All(Index => char.IsDigit(Index) || !char.IsLetter(Index));
    }
    public static bool IsAlphabetic(this string Source)
    {
      return Source.All(Index => char.IsLetter(Index));
    }
    public static bool IsAlphaNumeric(this string Source)
    {
      return Source.All(Index => char.IsLetter(Index) || char.IsDigit(Index));
    }
    public static bool IsNumeric(this string Source)
    {
      return Source.All(Index => char.IsDigit(Index));
    }

    /// <summary>
    /// Convert this <see cref="String"/> into a format valid for use as a C# identifier.
    /// </summary>
    /// <param name="Source">This <see cref="String"/>.</param>
    /// <returns>This <see cref="String"/>, converted into a format valid for use as a C# identifier.</returns>
    public static string ConvertToCSharpIdentifier(this string Source)
    {
      var StringBuilder = new StringBuilder();

      foreach (var Index in Source)
      {
        if (char.IsDigit(Index) || char.IsLetter(Index) || Index == '_')
        {
          if (StringBuilder.Length == 0 && char.IsDigit(Index))
            StringBuilder.Append('_');

          StringBuilder.Append(Index);
        }
      }

      return StringBuilder.ToString();
    }
    public static string ConvertToCSharpString(this string Source)
    {
      if (Source == null)
        return "null";
      else
        return "\"" + Source.Replace(@"\", @"\\").Replace("\t", @"\t").Replace("\r", @"\r").Replace("\n", @"\n").Replace("\"", "\\\"") + "\"";
    }

    /// <summary>
    /// Remove unicode diacritics from the string.
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static string RemoveDiacritics(this string Source)
    {
      if (Source == null)
        return null;

      if (Source.Length > 0)
      {
        var Normalised = Source.Normalize(NormalizationForm.FormD);

        var ResultArray = new char[Normalised.Length];
        var ResultIndex = 0;

        for (var NormalisedIndex = 0; NormalisedIndex < Normalised.Length; NormalisedIndex++)
        {
          var Character = Normalised[NormalisedIndex];

          if (System.Globalization.CharUnicodeInfo.GetUnicodeCategory(Character) != System.Globalization.UnicodeCategory.NonSpacingMark)
            ResultArray[ResultIndex++] = Character;
        }

        return new string(ResultArray, 0, ResultIndex).Normalize(NormalizationForm.FormC);
      }

      return Source;
    }

    public static IEnumerable<char> GetEnumerable(this string Source)
    {
      for (var Index = 0; Index < Source.Length; Index++)
        yield return Source[Index];
    }
    public static bool All(this string Source, Func<char, bool> Function)
    {
      foreach (var Item in Source)
      {
        if (!Function(Item))
          return false;
      }

      return true;
    }
    public static bool Any(this string Source, Func<char, bool> Function)
    {
      foreach (var Item in Source)
      {
        if (Function(Item))
          return true;
      }

      return false;
    }
    public static char Last(this string Source)
    {
      if (Source != null && Source.Length > 0)
        return Source[Source.Length - 1];
      else
        throw new ArgumentException("Source string must not be null or empty for the last function.");
    }

    public static System.Security.SecureString ToSecureString(this string Source)
    {
      if (Source == null)
        return null;

      var Result = new System.Security.SecureString();
      foreach (var c in Source)
        Result.AppendChar(c);
      return Result;
    }
    public static string ToPlainString(this System.Security.SecureString Source)
    {
      if (Source == null)
        return null;

      var valuePtr = IntPtr.Zero;
      try
      {
        valuePtr = System.Runtime.InteropServices.Marshal.SecureStringToGlobalAllocUnicode(Source);
        return System.Runtime.InteropServices.Marshal.PtrToStringUni(valuePtr);
      }
      finally
      {
        System.Runtime.InteropServices.Marshal.ZeroFreeGlobalAllocUnicode(valuePtr);
      }
    }

    /// <summary>
    /// Prepend a string value to every element in this list.
    /// </summary>
    /// <param name="Source">This list.</param>
    /// <param name="PrefixValue">The value to prepend.</param>
    /// <returns>A new <see cref="List{String}"/> containing every element in this list with <paramref name="PrefixValue"/> prepended to each element.</returns>
    public static IList<string> PrependString(this IList<string> Source, string PrefixValue)
    {
      return Source.Select(SelfString => PrefixValue + SelfString).ToList();
    }

    /// <summary>
    /// Prepend a formatted string to every element in this list.
    /// </summary>
    /// <param name="Source">This list.</param>
    /// <param name="PrefixFormat">The format string to prepend</param>
    /// <param name="PrefixArguments">The arguments to insert into <paramref name="PrefixFormat"/>.</param>
    /// <returns>A new <see cref="List{String}"/> containing every element in this list with the formatted string prepended.</returns>
    public static IList<string> PrependFormat(this IList<string> Source, string PrefixFormat, params object[] PrefixArguments)
    {
      return PrependString(Source, string.Format(System.Globalization.CultureInfo.CurrentCulture, PrefixFormat, PrefixArguments));
    }
    /// <summary>
    /// Join the elements of the list together with the value of <paramref name="Separator"/>. Roughly equivalent to Perl's join().
    /// </summary>
    /// <param name="Source">This list.</param>
    /// <param name="Separator">The string to join the elements with.</param>
    /// <param name="FinalSeparator">The string to use as the separator between the final two elements (if necessary).</param>
    /// <returns>A string containing each element of this list interspersed with <paramref name="Separator"/>.</returns>
    public static string AsSeparatedText(this IEnumerable<string> Source, string Separator, string FinalSeparator = null)
    {
      if (FinalSeparator == null)
        return string.Join(Separator, Source);

      var StringBuilder = new StringBuilder();

      var SourceCount = Source.Count();
      var SourceIndex = 0;
      foreach (var Element in Source)
      {
        StringBuilder.Append(Element);

        if (SourceCount > 1)
        {
          if (FinalSeparator != null && SourceIndex == SourceCount - 2)
            StringBuilder.Append(FinalSeparator);
          else if (SourceIndex < SourceCount - 1)
            StringBuilder.Append(Separator);
        }

        SourceIndex++;
      }

      return StringBuilder.ToString();
    }
    /// <summary>
    /// Concatenates the members of <paramref name="Source"/> into a comma and space separated series with "and" separating the final two elements.
    /// </summary>
    /// <param name="Source">A collection that contains the strings to concatenate.</param>
    /// <returns>A string consisting of a comma and space separated series with "and" separating the final two elements.</returns>
    /// <remarks>
    /// <example>
    /// Example inputs:
    /// <code>
    /// new[] { "A" }; // Produces "A"
    /// new[] { "A", "B" }; // Produces "A and B"
    /// new[] { "A", "B", "C" }; // Produces "A, B and C"
    /// </code>
    /// </example>
    /// </remarks>
    public static string AsNaturalAndSeparatedText(this IEnumerable<string> Source)
    {
      return Source.AsSeparatedText(", ", " and ");
    }
    /// <summary>
    /// Concatenates the members of <paramref name="Source"/> into a comma and space separated series with "or" separating the final two elements.
    /// </summary>
    /// <param name="Source">A collection that contains the strings to concatenate.</param>
    /// <returns>A string consisting of a comma and space separated series with "or" separating the final two elements.</returns>
    /// <remarks>
    /// <example>
    /// Example inputs:
    /// <code>
    /// new[] { "A" }; // Produces "A"
    /// new[] { "A", "B" }; // Produces "A or B"
    /// new[] { "A", "B", "C" }; // Produces "A, B or C"
    /// </code>
    /// </example>
    /// </remarks>
    public static string AsNaturalOrSeparatedText(this IEnumerable<string> Source)
    {
      return Source.AsSeparatedText(", ", " or ");
    }
    /// <summary>
    /// Concatenates the members of <paramref name="Source"/> into a new-line-separated string.
    /// </summary>
    /// <param name="Source">A collection that contains the strings to concatenate.</param>
    /// <returns>A string consisting of the elements of <paramref name="Source"/> separated by new lines.</returns>
    public static string AsLineSeparatedText(this IEnumerable<string> Source)
    {
      return string.Join(Environment.NewLine, Source);
    }
    /// <summary>
    /// Concatenates the members of <paramref name="Source"/> into a tab-separated string.
    /// </summary>
    /// <param name="Source">A collection that contains the strings to concatenate.</param>
    /// <returns>A string consisting of the elements of <paramref name="Source"/> separated by tabs.</returns>
    public static string AsTabSeparatedText(this IEnumerable<string> Source)
    {
      return string.Join("\t", Source);
    }
    /// <summary>
    /// Concatenates the members of <paramref name="Source"/>, truncating the collection if the number of elements exceeds <paramref name="MaximumDisplayCount"/>. Any truncated elements will be included as "others", e.g.; "A, B and 2 others".
    /// </summary>
    /// <param name="Source">A collection that contains the strings to concatenate.</param>
    /// <param name="MaximumDisplayCount">The maximum number of strings to include.</param>
    /// <param name="Separator">The separator to include between all but the final two strings.</param>
    /// <param name="FinalSeparator">The separator to include between the final two strings.</param>
    /// <returns>A string consisting of the elements of <paramref name="Source"/> truncated to <paramref name="MaximumDisplayCount"/> elements.</returns>
    public static string AsShortSeparatedText(this IEnumerable<string> Source, int MaximumDisplayCount = 3, string Separator = ", ", string FinalSeparator = " and ")
    {
      return Source.AsShortSeparatedText(S => S, MaximumDisplayCount, Separator, FinalSeparator);
    }
    /// <summary>
    /// Concatenates the members of <paramref name="Source"/>, truncating the collection if the number of elements exceeds <paramref name="MaximumDisplayCount"/>. Any truncated elements will be included as "others", e.g.; "A, B and 2 others".
    /// </summary>
    /// <typeparam name="T">The type of elements in the source collection.</typeparam>
    /// <param name="Source">A collection that contains the element to concatenate.</param>
    /// <param name="FormatQuery">Delegate which projects the elements into the strings to be concatenated.</param>
    /// <param name="MaximumDisplayCount">The maximum number of strings to include.</param>
    /// <param name="Separator">The separator to include between all but the final two strings.</param>
    /// <param name="FinalSeparator">The separator to include between the final two strings.</param>
    /// <returns>A string consisting of the elements of <paramref name="Source"/> truncated to <paramref name="MaximumDisplayCount"/> elements.</returns>
    public static string AsShortSeparatedText<T>(this IEnumerable<T> Source, Func<T, string> FormatQuery, int MaximumDisplayCount = 3, string Separator = ", ", string FinalSeparator = " and ")
    {
      // A
      // A, B
      // A, B, C
      // A, B and 2 others

      var SourceCount = Source.Count();

      if (SourceCount == 0)
        return "none";

      if (SourceCount <= MaximumDisplayCount)
        return Source.Select(FormatQuery).AsSeparatedText(Separator, FinalSeparator);

      var RemainingCount = SourceCount - MaximumDisplayCount;
      var TruncatedSource = Source.Take(MaximumDisplayCount);
      return TruncatedSource.Select(FormatQuery).AsSeparatedText(Separator) + FinalSeparator + RemainingCount.ToString() + " other".Plural(RemainingCount);
    }
    /// <summary>
    /// Concatenates the members of <paramref name="Source"/>, separating each with a new line and truncating the collection if the number of elements exceeds <paramref name="MaximumTitleCount"/>. Any truncated elements will be included as "others", e.g.; "A&#8629;B&#8629;2 others".
    /// </summary>
    /// <param name="Source">A collection that contains the strings to concatenate.</param>
    /// <param name="MaximumTitleCount">The maximum number of strings to include.</param>
    /// <returns>A string consisting of the elements of <paramref name="Source"/> truncated to <paramref name="MaximumTitleCount"/> elements.</returns>
    public static string AsShortSeparatedMultilineText(this IEnumerable<string> Source, int MaximumTitleCount = 2)
    {
      return Source.AsShortSeparatedText(MaximumTitleCount, Environment.NewLine, Environment.NewLine);
    }

    /// <summary>
    /// Removes all leading and trailing white-space characters from all elements in <paramref name="Source"/>.
    /// </summary>
    /// <param name="Source">A collection that contains the strings to trim.</param>
    /// <returns>A collection containing the strings that remain after all white-space characters are removed from the start and end of each string.</returns>
    public static IEnumerable<string> TrimAll(this IEnumerable<string> Source) => Source.Select(S => S.Trim());

    private static bool StartsWithVowelSound(this string Source)
    {
      if (Source.Length == 0)
        return false;

      var IsVowel = Source[0].IsVowel();

      if (StartsWithVowelSoundExceptionArray.Any(Exception => Source.StartsWith(Exception)))
        IsVowel = !IsVowel;

      return IsVowel;
    }
    private static List<string> CaptionSplit(string Caption)
    {
      return CaptionRegex.Matches(Caption).Cast<System.Text.RegularExpressions.Match>().Where(Match => Match.Success).Select(Match => Match.Value).ToList();
    }
    private static Inv.DistinctList<Permutation> CalculatePermutations(IEnumerable<string> Fragments, int LineCount)
    {
      var Response = new Inv.DistinctList<Permutation>();
      var FragmentList = Fragments.ToList();

      var ExtraFragmentCount = FragmentList.Count - LineCount;

      if (ExtraFragmentCount == 0)
      {
        var Result = new Permutation();
        Response.Add(Result);
        Result.LineList.AddRange(FragmentList);
      }
      else if (LineCount == 1)
      {
        var Result = new Permutation();
        Response.Add(Result);
        Result.LineList.Add(FragmentList.AsSeparatedText(" "));
      }
      else
      {
        for (var LineFragmentCount = 1; LineFragmentCount <= ExtraFragmentCount + 1; LineFragmentCount++)
        {
          var InteriorResultList = CalculatePermutations(FragmentList.Skip(LineFragmentCount), LineCount - 1);

          foreach (var InteriorResult in InteriorResultList)
          {
            var Result = new Permutation();
            Response.Add(Result);
            Result.LineList.Add(FragmentList.Take(LineFragmentCount).AsSeparatedText(" "));
            Result.LineList.AddRange(InteriorResult.LineList);
          }
        }
      }

      return Response;
    }
    private static readonly System.Text.RegularExpressions.Regex CaptionRegex = new System.Text.RegularExpressions.Regex(@"[^\s\(\[]+|[\(\[][^\)\]]*[\)\]]");

    private static readonly string[] PluralExceptionArray = new string[] { "series" };
    private static readonly char[] PluralVowelArray = new char[] { 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U' };
    private static readonly string[] StartsWithVowelSoundExceptionArray = new string[] { "unicorn", "unique", "one", "hour" };

    private sealed class Permutation
    {
      internal Permutation()
      {
        this.LineList = new List<string>();
      }

      public List<string> LineList { get; }
    }
  }

  /// <summary>
  /// <see cref="Type"/> extension methods.
  /// </summary>
  public static class TypeHelper
  {
    public static bool IsAssignableFrom(this Type ThisType, Type AssignType)
    {
      return ThisType.GetTypeInfo().IsAssignableFrom(AssignType.GetTypeInfo());
    }
    public static bool IsInstanceOfType(this Type type, object obj)
    {
      return obj != null && type.IsAssignableFrom(obj.GetType());
    }
  }

  public sealed class ObjectEqualityComparer<T> : IEqualityComparer<object>
  {
    public ObjectEqualityComparer(IEqualityComparer<T> Base)
    {
      this.Base = Base;
    }

    bool IEqualityComparer<object>.Equals(object x, object y) => Base.Equals((T)x, (T)y);
    int IEqualityComparer<object>.GetHashCode(object obj) => Base.GetHashCode((T)obj);

    private readonly IEqualityComparer<T> Base;
  }

  public sealed class ProjectionEqualityComparer<T, TIdentity> : IEqualityComparer<T>
  {
    public ProjectionEqualityComparer(Func<T, TIdentity> ProjectQuery)
    {
      this.ProjectQuery = ProjectQuery;
    }

    public bool Equals(T x, T y)
    {
      return Equals(ProjectQuery(x), ProjectQuery(y));
    }
    public int GetHashCode(T obj)
    {
      return ProjectQuery(obj).GetHashCode();
    }

    private readonly Func<T, TIdentity> ProjectQuery;
  }

  public sealed class ProjectionEqualityComparer<T> : IEqualityComparer<T>
  {
    public ProjectionEqualityComparer(Func<T, T, bool> ProjectQuery)
    {
      this.ProjectQuery = ProjectQuery;
    }

    public bool Equals(T x, T y)
    {
      return ProjectQuery(x, y);
    }
    public int GetHashCode(T obj)
    {
      return obj.GetHashCode();
    }

    private readonly Func<T, T, bool> ProjectQuery;
  }

  public sealed class LambdaEqualityComparer<T> : IEqualityComparer<T>
  {
    public LambdaEqualityComparer(Func<T, T, bool> EqualsQuery, Func<T, int> GetHashCodeQuery)
    {
      this.EqualsQuery = EqualsQuery;
      this.GetHashCodeQuery = GetHashCodeQuery;
    }

    public bool Equals(T x, T y)
    {
      return EqualsQuery(x, y);
    }
    public int GetHashCode(T obj)
    {
      return GetHashCodeQuery(obj);
    }

    private readonly Func<T, T, bool> EqualsQuery;
    private readonly Func<T, int> GetHashCodeQuery;
  }

  public sealed class ReverseComparer<T> : IComparer<T>
  {
    public ReverseComparer()
      : this(null)
    {
    }
    public ReverseComparer(IComparer<T> inner)
    {
      this.inner = inner ?? Comparer<T>.Default;
    }

    int IComparer<T>.Compare(T x, T y)
    {
      return inner.Compare(y, x);
    }

    private readonly IComparer<T> inner;
  }

  public sealed class DelegateComparer<T> : System.Collections.IComparer, IComparer<T>
  {
    public DelegateComparer(Comparison<T> Delegate)
    {
      this.Delegate = Delegate;
    }

    public int Compare(T x, T y)
    {
      return Delegate(x, y);
    }

    int System.Collections.IComparer.Compare(object x, object y)
    {
      return Compare((T)x, (T)y);
    }

    private readonly Comparison<T> Delegate;
  }

  /// <summary>
  /// Http extension methods.
  /// </summary>
  public static class HttpHelper
  {
    public static Stream GetRequestStream(this HttpWebRequest Request)
    {
      var Result = Request.BeginGetRequestStream(null, null);

      if (Result == null)
        return null;

      Result.AsyncWaitHandle.WaitOne();

      return Request.EndGetRequestStream(Result);
    }
    public static HttpWebResponse GetResponse(this HttpWebRequest Request)
    {
      var Result = Request.BeginGetResponse(null, null);

      if (Result == null)
        return null;

      Result.AsyncWaitHandle.WaitOne();

      return (HttpWebResponse)Request.EndGetResponse(Result);
    }

    public static HttpStatusCode IntToHTTPStatusCode(int Value)
    {
      if (System.Enum.IsDefined(typeof(HttpStatusCode), Value))
        return (HttpStatusCode)Value;
      else
        throw new Exception("System.Net.HttpStatusCode not handled: " + Value);
    }
  }

  /// <summary>
  /// <see cref="System.Collections.BitArray"/> extension methods.
  /// </summary>
  public static class BitArrayHelper
  {
    public static void CopyTo(this System.Collections.BitArray Source, Array Array, int Index)
    {
      ((System.Collections.ICollection)Source).CopyTo(Array, Index);
    }
  }

  /// <summary>
  /// <see cref="System.Threading.Tasks.Task"/> extension methods.
  /// </summary>
  public static class TaskHelper
  {
    public static void Forget(this System.Threading.Tasks.Task Task)
    {
      Debug.Assert(Task != null);

      // Absorbs this warning:
      // warning CS4014: Because this call is not awaited, execution of the current method continues before the call is completed. Consider applying the 'await' operator to the result of the call.
    }
  }

  /// <summary>
  /// Csv helper methods.
  /// </summary>
  public static class CsvHelper
  {
    public static string ToCsvLine(this string[] Source)
    {
      using (var StringWriter = new StringWriter())
      {
        using (var CsvWriter = new CsvWriter(StringWriter))
          CsvWriter.WriteRecord(Source);

        return StringWriter.ToString();
      }
    }
    public static string[] ToCsvArray(this string Source)
    {
      using (var CsvReader = new CsvReader(Source))
        return CsvReader.ReadRecord();
    }
    public static string EncodeCsvField(this string Source)
    {
      if (Source.IndexOfAny("\",\r\n".ToCharArray()) >= 0)
        return '"' + Source.Replace("\"", "\"\"") + '"';
      else
        return Source;
    }
  }

  ///<summary>
  ///Static class with extension methods for arrays.
  ///</summary>
  public static class ArrayHelper
  {
    /// <summary>
    /// Fill this array with a specified value.
    /// </summary>
    /// <typeparam name="T">The type of elements in this array.</typeparam>
    /// <param name="Array">This array.</param>
    /// <param name="Value">The value to fill the array with.</param>
    public static void Fill<T>(this T[] Array, T Value)
    {
      for (var Index = 0; Index < Array.Length; Index++)
        Array[Index] = Value;
    }
    public static void Fill<T>(this T[] Array, Func<int, T> ValueFunction)
    {
      for (var Index = 0; Index < Array.Length; Index++)
        Array[Index] = ValueFunction(Index);
    }
    public static T[] Subset<T>(this T[] Source, int StartIndex)
    {
      return Subset(Source, StartIndex, Source.Length - StartIndex);
    }
    public static T[] Subset<T>(this T[] Source, int StartIndex, int Length)
    {
      var Result = new T[Length];
      Array.Copy(Source, StartIndex, Result, 0, Length);
      return Result;
    }
    public static IEnumerable<T> ExceptNull<T>(this T[,] Source)
    {
      for (int i = 0; i < Source.GetLength(0); i++)
      {
        for (int j = 0; j < Source.GetLength(1); j++)
        {
          var cell = Source[i, j];

          if (cell != null)
            yield return cell;
        }
      }
    }
    public static T[] Segment<T>(this T[] Array, int Offset, int Count)
    {
      return Array.Skip(Offset).Take(Count).ToArray();
    }
    /// <summary>
    /// Generate all permutations of a given array.
    /// The number of distinct permutations is equal to the factorial of given array's length.
    /// The array new [] { 1, 2, 3, 4} will have 4! permutations = 24.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Input"></param>
    /// <returns></returns>
    public static IEnumerable<T[]> GeneratePermutations<T>(this T[] Input)
    {
      IEnumerable<T[]> GetPermutations(T[] Array, int k, int m)
      {
        void Swap(ref T a, ref T b)
        {
          var Temp = a;
          a = b;
          b = Temp;
        }

        if (k == m)
        {
          yield return Array.ToArray();
        }
        else
        {
          for (var i = k; i <= m; i++)
          {
            Swap(ref Array[k], ref Array[i]);

            foreach (var Result in GetPermutations(Array, k + 1, m))
              yield return Result;

            Swap(ref Array[k], ref Array[i]);
          }
        }
      };

      // NOTE: recursive algorithm.
      return GetPermutations(Input, 0, Input.Length - 1);
    }
  }

  /// <summary>
  /// <see cref="Stopwatch"/> extension methods.
  /// </summary>
  public static class StopwatchHelper
  {
    public static void Measure(this Stopwatch Stopwatch, string Name, Action Action)
    {
      if (DebugHelper.IsDebugMode())
        Stopwatch.Restart();

      Action();

      if (DebugHelper.IsDebugMode())
      {
        Stopwatch.Stop();
        Stopwatch.Write(Name);
      }
    }
    public static T Measure<T>(this Stopwatch Stopwatch, string Name, Func<T> Function)
    {
      T Result = default;

      Measure(Stopwatch, Name, () =>
      {
        Result = Function();
      });

      return Result;
    }
    public static void Write(this Stopwatch Stopwatch, string Name)
    {
      if (DebugHelper.IsDebugMode())
        Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + " " + Name + ": " + Stopwatch.ElapsedMilliseconds + "ms");
    }
  }

  /// <summary>
  /// Parallel helper methods.
  /// </summary>
  public static class ParallelHelper
  {
    public static void ForEachInParallel<T>(this System.Collections.Generic.IEnumerable<T> Enumerable, Action<T> Action)
    {
      Parallel.ForEach(Enumerable, new ParallelOptions() { MaxDegreeOfParallelism = Environment.ProcessorCount }, Action);
    }
    public static void ForEachInParallel<T>(this System.Collections.Generic.IEnumerable<T> Enumerable, Action<T, ParallelLoopState> Action)
    {
      Parallel.ForEach(Enumerable, new ParallelOptions() { MaxDegreeOfParallelism = Environment.ProcessorCount }, Action);
    }
  }

  /// <summary>
  /// Xor cryptography extension methods.
  /// </summary>
  public static class XorCryptographer
  {
    public static byte[] EncryptDecrypt(byte[] data, byte[] key)
    {
      var Result = new byte[data.Length];

      for (var i = 0; i < data.Length; i++)
        Result[i] = (byte)(data[i] ^ key[i % key.Length]);

      return Result;
    }
  }

  /// <summary>
  /// Result of checking an email address.
  /// </summary>
  public enum EmailInvalid
  {
    Blank,
    Characters,
    StartingCharacter,
    EndingCharacter,
    AtPeriodFormat,
    DoublePeriod,
    MultipleAt,
    AtPeriodLocation,
  }

  /// <summary>
  /// Email extension methods.
  /// </summary>
  public static class EmailHelper
  {
    public static EmailInvalid? CheckAddress(string EmailAddress)
    {
      if (string.IsNullOrWhiteSpace(EmailAddress))
        return EmailInvalid.Blank;

      // This is actually the valid set [a-z0-9!#$%&'*+/=?^_`{|}~-]
      if (EmailAddress.Contains(' ', ',', '"', ':', ';', '\\', '(', ')', '<', '>'))
        return EmailInvalid.Characters;

      if (EmailAddress.StartsWith('@', '.'))
        return EmailInvalid.StartingCharacter;

      if (EmailAddress.EndsWith('@', '.', '-', '_'))
        return EmailInvalid.EndingCharacter;

      var FirstAt = EmailAddress.IndexOf("@", StringComparison.InvariantCultureIgnoreCase);
      var FirstPeriod = FirstAt < 0 ? -1 : EmailAddress.IndexOf(".", FirstAt, StringComparison.InvariantCultureIgnoreCase);
      if (FirstAt < 0 || FirstPeriod < 0)
        return EmailInvalid.AtPeriodFormat;

      if (EmailAddress.Contains(".."))
        return EmailInvalid.DoublePeriod;

      if (EmailAddress.Count('@') >= 2)
        return EmailInvalid.MultipleAt;

      if (EmailAddress.Contains(".@") || EmailAddress.Contains("@."))
        return EmailInvalid.AtPeriodLocation;

      return null;
    }
    public static string FormatTitle(this EmailInvalid Invalid)
    {
      switch (Invalid)
      {
        case EmailInvalid.Blank:
          return "must not be empty or contain only white space";

        case EmailInvalid.Characters:
          return "must not have invalid characters";

        case EmailInvalid.StartingCharacter:
          return "must start with a valid character";

        case EmailInvalid.EndingCharacter:
          return "must end with a valid character";

        case EmailInvalid.AtPeriodFormat:
          return "must contain one @ and at least one .";

        case EmailInvalid.DoublePeriod:
          return "must not have two periods in a row";

        case EmailInvalid.MultipleAt:
          return "must not have more than one @";

        case EmailInvalid.AtPeriodLocation:
          return "must not have @ and . next to each other";

        default:
          throw new Exception("EmailInvalid not handled: " + Invalid);
      }
    }
    public static string FormatTitle(this EmailInvalid? Invalid)
    {
      return Invalid != null ? Invalid.Value.FormatTitle() : "";
    }
  }

  /// <summary>
  /// Result of checking a phone number.
  /// </summary>
  public enum PhoneInvalid
  {
    Blank,
    NonNumeric,
  }

  public static class PhoneHelper
  {
    public static PhoneInvalid? CheckNumber(string PhoneNumber)
    {
      if (string.IsNullOrWhiteSpace(PhoneNumber))
        return PhoneInvalid.Blank;

      if (!PhoneNumber.Any(N => N.IsNumeric()))
        return PhoneInvalid.NonNumeric;

      // TODO: more sophisticated checking of valid phone numbers.

      return null;
    }
    public static string FormatTitle(this PhoneInvalid Invalid)
    {
      return Invalid switch
      {
        PhoneInvalid.Blank => "must not be empty or contain only white space",
        PhoneInvalid.NonNumeric => "must contain at least one digit",
        _ => throw EnumHelper.UnexpectedValueException(Invalid)
      };
    }
    public static string FormatTitle(this PhoneInvalid? Invalid)
    {
      return Invalid != null ? Invalid.Value.FormatTitle() : "";
    }

    /// <summary>
    /// Formats phone number text according to standard Australian formatting rules
    /// </summary>
    public static string FormatPhoneNumberForAustralia(string Number)
    {
      // Formatting criteria sourced from:
      // https://en.wikipedia.org/wiki/National_conventions_for_writing_telephone_numbers#Australia
      // https://en.wikipedia.org/wiki/Telephone_numbers_in_Australia

      var NumericNumber = Number.KeepNumeric();

      // Handle non-geographic numbers
      if (NumericNumber.Length == 6  && NumericNumber.StartsWith("13"))
        return $"{NumericNumber.Substring(0, 2)} {NumericNumber.Substring(2, 2)} {NumericNumber.Substring(4, 2)}";

      if (NumericNumber.Length.In(7, 10) && NumericNumber[0] == '1' && NumericNumber[2] == '0')
      {
        // TODO: Should we only format if the second digit is an allowed number, i.e. 8, 3, or 9?
        if (NumericNumber.Length == 10)
          return $"{NumericNumber.Substring(0, 4)} {NumericNumber.Substring(4, 3)} {NumericNumber.Substring(7, 3)}";
        else
          return $"{NumericNumber.Substring(0, 3)} {NumericNumber.Substring(3, 4)}";
      }

      if (NumericNumber.Length == 8) // local number
      {
        return $"{NumericNumber.Substring(0, 4)} {NumericNumber.Substring(4, 4)}";
      }
      else if (NumericNumber.Length == 10)
      {
        if (NumericNumber.StartsWith("04")) // mobile number
          return $"{NumericNumber.Substring(0, 4)} {NumericNumber.Substring(4, 3)} {NumericNumber.Substring(7, 3)}";
        else // geographic number
          return $"{NumericNumber.Substring(0, 2)} {NumericNumber.Substring(2, 4)} {NumericNumber.Substring(6, 4)}";
      }
      else if (NumericNumber.StartsWith("61")) // international
      {
        if (NumericNumber.Length == 11)
        {
          if (NumericNumber[2] == '4') // mobile
            return $"+{NumericNumber.Substring(0, 2)} {NumericNumber.Substring(2, 3)} {NumericNumber.Substring(5, 3)} {NumericNumber.Substring(8, 3)}";
          else // geographic
            return $"+{NumericNumber.Substring(0, 2)} {NumericNumber.Substring(2, 1)} {NumericNumber.Substring(3, 4)} {NumericNumber.Substring(7, 4)}";
        }
        else if (NumericNumber.Length == 12 && NumericNumber[2] == '0') // international geographic number with trunk access code
        {
          return $"+{NumericNumber.Substring(0, 2)} {NumericNumber.Substring(3, 1)} {NumericNumber.Substring(4, 4)} {NumericNumber.Substring(8, 4)}";
        }
      }

      return Number;
    }
    /// <summary>
    /// Formats phone number text according to standard New Zealand formatting rules
    /// </summary>
    public static string FormatPhoneNumberForNewZealand(string Number)
    {
      // Formatting criteria sourced from:
      // https://en.wikipedia.org/wiki/National_conventions_for_writing_telephone_numbers#New_Zealand
      // https://en.wikipedia.org/wiki/Telephone_numbers_in_New_Zealand

      var NumericNumber = Number.KeepNumeric();

      if (NumericNumber.Length == 7) // local numbers
        return $"{NumericNumber.Substring(0, 3)} {NumericNumber.Substring(3, 4)}";

      if (NumericNumber.StartsWith("0") && NumericNumber.Length == 9 && NumericNumber[1] != '2') // geographic numbers
        return $"{NumericNumber.Substring(0, 2)} {NumericNumber.Substring(2, 3)} {NumericNumber.Substring(5, 4)}";

      if (NumericNumber.StartsWith("02") && NumericNumber.Length.In(9, 10, 11)) // mobile numbers
      {
        if (NumericNumber.Length == 9)
          return $"{NumericNumber.Substring(0, 3)} {NumericNumber.Substring(3, 3)} {NumericNumber.Substring(6, 3)}";
        else if (NumericNumber.Length == 10)
          return $"{NumericNumber.Substring(0, 3)} {NumericNumber.Substring(3, 3)} {NumericNumber.Substring(6, 4)}";
        else if (NumericNumber.Length == 11)
          return $"{NumericNumber.Substring(0, 3)} {NumericNumber.Substring(3, 4)} {NumericNumber.Substring(7, 4)}";
      }

      if (NumericNumber.Length == 10 && (NumericNumber.StartsWith("0800") || NumericNumber.StartsWith("0508"))) //free-phone numbers
        return $"{NumericNumber.Substring(0, 4)} {NumericNumber.Substring(4, 3)} {NumericNumber.Substring(7, 3)}";

      if (NumericNumber.StartsWith("64") && NumericNumber.Length.In(10, 11, 12)) // international numbers
      {
        if (NumericNumber.Length == 10)
        {
          if (NumericNumber[2] == '2') // mobile
            return $"+{NumericNumber.Substring(0, 2)} {NumericNumber.Substring(2, 2)} {NumericNumber.Substring(4, 3)} {NumericNumber.Substring(7, 3)}";
          else // geographic
            return $"+{NumericNumber.Substring(0, 2)} {NumericNumber.Substring(2, 1)} {NumericNumber.Substring(3, 3)} {NumericNumber.Substring(6, 4)}";
        }
        else if (NumericNumber.Length == 11)
        {
          if (NumericNumber[2] == '2') // mobile
            return $"+{NumericNumber.Substring(0, 2)} {NumericNumber.Substring(2, 2)} {NumericNumber.Substring(4, 3)} {NumericNumber.Substring(7, 4)}";
          else if (NumericNumber[2] == '0') // international geographic number with trunk access code
            return $"+{NumericNumber.Substring(0, 2)} {NumericNumber.Substring(3, 1)} {NumericNumber.Substring(4, 3)} {NumericNumber.Substring(7, 4)}";
        }
        else if (NumericNumber.Length == 12 && NumericNumber[2] == '2')
        {
          return $"+{NumericNumber.Substring(0, 2)} {NumericNumber.Substring(2, 2)} {NumericNumber.Substring(4, 4)} {NumericNumber.Substring(8, 4)}";
        }
      }

      return Number;
    }
  }

  /// <summary>
  /// <see cref="Random"/> extension methods.
  /// </summary>
  public static class RandomHelper
  {
    public static bool NextBoolean(this Random Random)
    {
      return Random.NextDouble() >= 0.5;
    }
    public static byte NextByte(this Random Random)
    {
      return (byte)Random.Next(byte.MaxValue);
    }
    public static long NextInt64(this Random Random)
    {
      var ByteArray = new byte[sizeof(long)];

      Random.NextBytes(ByteArray);

      return BitConverter.ToInt64(ByteArray, 0);
    }
    public static T NextItem<T>(this Random Random, IReadOnlyList<T> Source)
    {
      if (Source.Count == 0)
        return default;
      else
        return Source[Random.Next(Source.Count)];
    }
    public static TEnum NextEnum<TEnum>(this Random Random) where TEnum : struct
    {
      var EnumType = typeof(TEnum);

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(EnumType.GetTypeInfo().IsEnum, "EnumType must be an enumeration");

      var EnumValueArray = (TEnum[])System.Enum.GetValues(EnumType);

      return EnumValueArray[Random.Next(EnumValueArray.Length)];
    }
    public static void Shuffle<T>(this Random Random, Inv.DistinctList<T> List)
    {
      List.Shuffle(Random);
    }
  }

  /// <summary>
  /// <see cref="RandomNumberGenerator"/> extension methods.
  /// </summary>
  public static class RandomNumberGeneratorHelper
  {
    /// <summary>
    /// Returns a random integer that is within a specified range.
    /// </summary>
    /// <param name="Generator"></param>
    /// <param name="MinValue">The inclusive lower bound of the random number returned.</param>
    /// <param name="MaxValue">The exclusive upper bound of the random number returned. <paramref name="MaxValue"/> must be greater than or equal to <paramref name="MinValue"/>.</param>
    /// <returns>A 32-bit signed integer greater than or equal to <paramref name="MinValue"/> and less than <paramref name="MaxValue"/>; that is, the range of return values includes <paramref name="MinValue"/> but not <paramref name="MaxValue"/>. If <paramref name="MinValue"/> equals <paramref name="MaxValue"/>, <paramref name="MinValue"/> is returned.</returns>
    /// <exception cref="ArgumentOutOfRangeException"><paramref name="MinValue"/> is greater than <paramref name="MaxValue"/>.</exception>
    public static int GetInt(this RandomNumberGenerator Generator, int MinValue = 0, int MaxValue = int.MaxValue)
    {
      if (MinValue > MaxValue)
        throw new ArgumentOutOfRangeException(nameof(MinValue), $"'{nameof(MinValue)}' cannot be greater than {nameof(MaxValue)}.");

      if (MinValue == MaxValue || MinValue + 1 == MaxValue)
        return MinValue;

      var RangeSize = (uint)(MaxValue - MinValue);
      var MaxValidValue = uint.MaxValue - (uint.MaxValue % RangeSize) - 1;

      var RandomBytes = new byte[sizeof(int)];
      uint Result;

      // Any number generated beyond MaxValidValue would result in an uneven distribution of results. The loop rejects such values to ensure an even probability of each result.

      do
      {
        Generator.GetBytes(RandomBytes);
        Result = BitConverter.ToUInt32(RandomBytes, 0);
      } while (Result > MaxValidValue);

      return MinValue + (int)(Result % RangeSize);
    }
  }

  public static class StreamHelper
  {
    public static void ReadByteArray(this System.IO.Stream Stream, byte[] BufferArray, int? BufferLength = null)
    {
      var TotalBytes = BufferLength ?? BufferArray.Length;
      var BufferBytes = 0;
      while (BufferBytes < TotalBytes)
      {
        var ReadBytes = Stream.Read(BufferArray, BufferBytes, TotalBytes - BufferBytes);
        if (ReadBytes == 0)
          throw new EndOfStreamException($"Unexpected end of stream while reading byte array of length {TotalBytes}.");

        BufferBytes += ReadBytes;
      }
    }
  }

  public static class WebHelper
  {
    public static string UrlEncode(string Source)
    {
      return Source.Replace('/', '_').Replace('+', '-');
    }
    public static string UrlDecode(string Source)
    {
      return Source.Replace('_', '/').Replace('-', '+');
    }

    public static string WithoutScheme(this Uri Uri)
    {
      return (!string.IsNullOrEmpty(Uri.UserInfo) ? Uri.UserInfo + "@" : "") + Uri.Host + (Uri.PathAndQuery == "/" ? "" : Uri.PathAndQuery) + Uri.Fragment;
    }
  }
}

namespace Inv
{
  /// <summary>
  /// Func wrapper to support deferred composition of a result <typeparamref name="T"/>.
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public sealed class Result<T>
  {
    public Result(Func<T> Func)
    {
      this.Func = Func;
    }

    /// <summary>
    /// Invokes the deferred composition function and returns the result.
    /// </summary>
    /// <returns></returns>
    public T Compose()
    {
      if (Func == null)
        return default;

      return Func();
    }

    private readonly Func<T> Func;
  }
}