﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inv
{
  public struct Vector
  {
    public Vector(double X, double Y)
    {
      this.X = X;
      this.Y = Y;
    }
    public Vector(Inv.Point Point)
    {
      this.X = Point.X;
      this.Y = Point.Y;
    }
   
    public double X { get; set; }
    public double Y { get; set; }

    public override int GetHashCode() => X.GetHashCode() ^ Y.GetHashCode();
    public override bool Equals(object Other) => Other is Inv.Vector Point && EqualTo(Point);
    public override string ToString() => $"({X},{Y})";
    public bool EqualTo(Inv.Vector Other) => X == Other.X && Y == Other.Y;
    public Inv.Point Truncate() => new Inv.Point((int)X, (int)Y);
    public Inv.Vector Translate(double X, double Y) => new Inv.Vector(this.X + X, this.Y + Y);

    public static Inv.Vector operator -(Inv.Vector A) => new Inv.Vector(-A.X, -A.Y);
    public static bool operator ==(Inv.Vector A, Inv.Vector B)
    {
      return A.X == B.X && A.Y == B.Y;
    }
    public static bool operator !=(Inv.Vector A, Inv.Vector B)
    {
      return A.X != B.X || A.Y != B.Y;
    }
    public static Inv.Vector operator +(Inv.Vector A, Inv.Vector B)
    {
      return new Inv.Vector(A.X + B.X, A.Y + B.Y);
    }
    public static Inv.Vector operator +(Inv.Vector A, Inv.Point B)
    {
      return new Inv.Vector(A.X + B.X, A.Y + B.Y);
    }
    public static Inv.Vector operator +(Inv.Point A, Inv.Vector B)
    {
      return new Inv.Vector(A.X + B.X, A.Y + B.Y);
    }
    public static Inv.Vector operator +(Inv.Vector A, Inv.Dimension B)
    {
      return new Inv.Vector(A.X + B.Width, A.Y + B.Height);
    }
    public static Inv.Vector operator -(Inv.Vector A, Inv.Vector B)
    {
      return new Inv.Vector(A.X - B.X, A.Y - B.Y);
    }
    public static Inv.Vector operator -(Inv.Vector A, Inv.Point B)
    {
      return new Inv.Vector(A.X - B.X, A.Y - B.Y);
    }
    public static Inv.Vector operator *(Inv.Vector A, double Factor)
    {
      return new Inv.Vector(A.X * Factor, A.Y * Factor);
    }
    public static Inv.Vector operator /(Inv.Vector A, double Factor)
    {
      return new Inv.Vector(A.X / Factor, A.Y / Factor);
    }

    public static Inv.Vector Zero => new Inv.Vector(0, 0);
  }
}