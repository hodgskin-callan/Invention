﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Globalization;
using System.Xml;
using System.Xml.Serialization;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// A serializable class representing a monetary amount.
  /// </summary>
  [XmlSchemaProvider("MySchema")]
  public struct Money : IComparable, IComparable<Money>, IEquatable<Money>, IFormattable, IXmlSerializable
  {
    /// <summary>
    /// Initializes a new <see cref="Money"/> object with the specified decimal amount.
    /// </summary>
    /// <param name="Amount">The decimal value representing the money amount.</param>
    public Money(decimal Amount)
    {
      this.AmountField = As4DP(Amount);
    }

    /// <summary>
    /// Represents a zero amount.
    /// </summary>
    public static readonly Money Zero = new Money(0.0m);

    /// <summary>
    /// Converts the string representation of an amount to its <see cref="Money"/> equivalent.
    /// </summary>
    /// <param name="Value">The string representation of the amount to convert.</param>
    /// <returns>The equivalent to the amount contained in <paramref name="Value"/>.</returns>
    /// <exception cref="FormatException"><paramref name="Value"/> is not in the correct format.</exception>
    public static Money Parse(string Value)
    {
      if (!TryParse(Value, out var Result))
        throw new FormatException(string.Format("'{0}' is not a valid money representation", Value));

      return Result;
    }
    /// <summary>
    /// Converts the string representation of an amount to its <see cref="Money"/> equivalent. A return value indicates whether the conversion succeeded or failed.
    /// </summary>
    /// <param name="Value">The string representation of the amount to convert.</param>
    /// <param name="Money"></param>
    /// <returns><see langword="true"/> if <paramref name="Value"/> was converted successfully; otherwise, <see langword="false"/>.</returns>
    public static bool TryParse(string Value, out Money Money)
    {
      var Result = decimal.TryParse(Value, NumberStyles.Currency, NumberFormatInfo.CurrentInfo, out var DecimalValue);

      Money = new Money(DecimalValue);

      return Result;
    }

    /// <summary>
    /// Returns the larger of two amounts.
    /// </summary>
    /// <param name="Value1">The first of two amounts to compare.</param>
    /// <param name="Value2">The second of two amounts to compare.</param>
    /// <returns>Parameter <paramref name="Value1"/> or <paramref name="Value2"/>, whichever is larger.</returns>
    public static Money Max(Money Value1, Money Value2)
    {
      return new Money(Math.Max(Value1.GetAmount(), Value2.GetAmount()));
    }
    /// <summary>
    /// Returns the smaller of two amounts.
    /// </summary>
    /// <param name="Value1">The first of two amounts to compare.</param>
    /// <param name="Value2">The second of two amounts to compare.</param>
    /// <returns>Parameter <paramref name="Value1"/> or <paramref name="Value2"/>, whichever is smaller.</returns>
    public static Money Min(Money Value1, Money Value2)
    {
      return new Money(Math.Min(Value1.GetAmount(), Value2.GetAmount()));
    }
    /// <summary>
    /// Returns the absolute value of a <see cref="Money"/> amount.
    /// </summary>
    /// <param name="Value"></param>
    /// <returns>The absolute value of <paramref name="Value"/>.</returns>
    public static Money Abs(Money Value)
    {
      return new Money(Math.Abs(Value.GetAmount()));
    }

    /// <summary>
    /// Get the decimal value for the amount represented by this <see cref="Money"/> object.
    /// </summary>
    /// <returns>The decimal value for the amount represented by this <see cref="Money"/> object.</returns>
    public decimal GetAmount()
    {
      return AmountField;
    }
    /// <summary>
    /// Returns the absolute value of this <see cref="Money"/> object.
    /// </summary>
    /// <returns>A new <see cref="Money"/> object which represents the absolute value of this <see cref="Money"/> object.</returns>
    public Money AsAbsolute()
    {
      return new Money(Math.Abs(this.AmountField));
    }
    /// <summary>
    /// Create a clone of this <see cref="Money"/> object.
    /// </summary>
    /// <returns>A new <see cref="Money"/> object which represents the same amount as this <see cref="Money"/> object.</returns>
    public Money Clone()
    {
      return new Money(this.AmountField);
    }
    /// <summary>
    /// Compare this <see cref="Money"/> object to another <see cref="Money"/> object.
    /// </summary>
    /// <param name="Money">The <see cref="Money"/> object to compare to this <see cref="Money"/> object.</param>
    /// <returns>
    /// <para>A signed number indicating the relative values of this <see cref="Money"/> object and <paramref name="Money"/></para>
    /// <para>
    /// <list type="table">
    /// <listheader>
    /// <term>Return Value</term>
    /// <term>Description</term>
    /// </listheader>
    /// <item>
    /// <description>Less than zero</description>
    /// <description>This <see cref="Money"/> is less than <paramref name="Money"/>.</description>
    /// </item>
    /// <item>
    /// <description>Zero</description>
    /// <description>This <see cref="Money"/> is equivalent to <paramref name="Money"/>.</description>
    /// </item>
    /// <item>
    /// <description>Greater than zero</description>
    /// <description>This <see cref="Money"/> is greater to <paramref name="Money"/>.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </returns>
    public int CompareTo(Money Money)
    {
      if (Money == this)
        return 0;
      else
        return this.AmountField.CompareTo(Money.AmountField);
    }
    /// <summary>
    /// Returns a value indicating whether this instance and a specified <see cref="Money"/> object represent the same value.
    /// </summary>
    /// <param name="Money">An object to compare to this instance.</param>
    /// <returns><see langword="true"/> if <paramref name="Money"/> is equal to this instance; otherwise, <see langword="false"/>.</returns>
    public bool EqualTo(Money Money)
    {
      return CompareTo(Money) == 0;
    }
    /// <summary>
    /// Returns a value representing this <see cref="Money"/> object rounded up to the nearest multiple of <paramref name="AccuracyValue"/>.
    /// </summary>
    /// <param name="AccuracyValue"></param>
    /// <returns>A new <see cref="Money"/> object representing this <see cref="Money"/> object rounded up to the nearest multiple of <paramref name="AccuracyValue"/>.</returns>
    public Money RoundUp(Money AccuracyValue)
    {
      return new Money((AmountField / AccuracyValue.AmountField).Ceiling() * AccuracyValue.AmountField);
    }
    /// <summary>
    /// Returns a value representing this <see cref="Money"/> object rounded down to the nearest multiple of <paramref name="AccuracyValue"/>.
    /// </summary>
    /// <param name="AccuracyValue"></param>
    /// <returns>A new <see cref="Money"/> object representing this <see cref="Money"/> object rounded down to the nearest multiple of <paramref name="AccuracyValue"/>.</returns>
    public Money RoundDown(Money AccuracyValue)
    {
      return new Money((AmountField / AccuracyValue.AmountField).Floor() * AccuracyValue.AmountField);
    }
    /// <summary>
    /// Returns a value representing this <see cref="Money"/> object rounded to the nearest multiple of <paramref name="AccuracyValue"/>.
    /// </summary>
    /// <param name="AccuracyValue"></param>
    /// <returns>A new <see cref="Money"/> object representing this <see cref="Money"/> object rounded to the nearest multiple of <paramref name="AccuracyValue"/>.</returns>
    public Money RoundNearest(Money AccuracyValue)
    {
      return new Money(Math.Round(AmountField / AccuracyValue.AmountField) * AccuracyValue.AmountField);
    }
    /// <summary>
    /// Returns a value representing this <see cref="Money"/> object rounded to the specified denomination using to-even midpoint rounding.
    /// </summary>
    /// <returns>A new <see cref="Money"/> object representing this <see cref="Money"/> object in cents rounded to the specified denomination using to-even midpoint rounding.</returns>
    public Money RoundBankers(Money AccuracyValue)
    {
      var RoundedAmountField = Math.Round(AmountField, 2);

      var Whole = Math.Truncate(RoundedAmountField / AccuracyValue.AmountField);
      var Fraction = (RoundedAmountField - (Whole * AccuracyValue.AmountField)) / AccuracyValue.AmountField;

      return new Money(Fraction > 0.5M || (Fraction == 0.5M && Whole % 2 == 1) ? (Whole + 1) : Whole) * AccuracyValue.AmountField;
    }
    /// <summary>
    /// Returns a value representing this <see cref="Money"/> object rounded to 2 decimal places using to-even midpoint rounding.
    /// </summary>
    /// <returns>A new <see cref="Money"/> object representing this <see cref="Money"/> object rounded to 2 decimal places using to-even midpoint rounding.</returns>
    public Money RoundBankers()
    {
      return new Money(AmountField.RoundBankers());
    }
    /// <summary>
    /// Returns a value representing this <see cref="Money"/> object rounded to 2 decimal places using away-from-zero midpoint rounding.
    /// </summary>
    /// <returns>A new <see cref="Money"/> object representing this <see cref="Money"/> object rounded to 2 decimal places using away-from-zero midpoint rounding.</returns>
    public Money RoundPricing()
    {
      return new Money(AmountField.RoundPricing());
    }
    /// <summary>
    /// Calculates the fractional part of this <see cref="Money"/> object.
    /// </summary>
    /// <returns>A new <see cref="Money"/> object which represents the fractional part of this <see cref="Money"/> object.</returns>
    public Money AsCents()
    {
      return new Money(AmountField - AmountField.Truncate());
    }
    /// <summary>
    /// Calculates the integral part of this <see cref="Money"/> object.
    /// </summary>
    /// <returns>A new <see cref="Money"/> object which represents the integral part of this <see cref="Money"/> object.</returns>
    public Money AsWholeDollars()
    {
      return new Money(AmountField.Truncate());
    }

    /// <summary>
    /// Get a string representation of this <see cref="Money"/> object.
    /// </summary>
    /// <returns>A string in the current money format.</returns>
    public override string ToString()
    {
      return ToString(Format: null, Provider: null);
    }
    /// <summary>
    /// Get a string representation of this <see cref="Money"/> object.
    /// </summary>
    /// <param name="Format">A numeric format string.</param>
    /// <returns>The string representation of the value of this instance as specified by format and provider.</returns>
    /// <exception cref="FormatException">Format is invalid.</exception>
    public string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string Format)
    {
      return ToString(Format, Provider: null);
    }
    /// <summary>
    /// Get a string representation of this <see cref="Money"/> object.
    /// </summary>
    /// <param name="Format">A numeric format string.</param>
    /// <param name="Provider">An object that supplies culture-specific formatting information.</param>
    /// <returns>The string representation of the value of this instance as specified by format and provider.</returns>
    /// <exception cref="FormatException">Format is invalid.</exception>
    public string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string Format, IFormatProvider Provider)
    {
      return AmountField.ToString(Format ?? "C", Provider);
    }
    /// <summary>
    /// Get a string representation of this <see cref="Money"/> object rounded away from zero to the specified number of decimal places.
    /// </summary>
    /// <param name="DecimalPlaces">The number of decimal places to include.</param>
    /// <returns>A string in the current money format rounded away from zero to the specified number of decimal places.</returns>
    public string ToString(int DecimalPlaces)
    {
      return AmountField.ToString("C" + DecimalPlaces);
    }
    /// <summary>
    /// Get a string representation of this <see cref="Money"/> object without currency symbol.
    /// </summary>
    /// <returns>A string in the current money format without currency symbol.</returns>
    public string ToStringWithoutCurrencySymbol()
    {
      return AmountField.ToString("C").Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol, "");
    }
    /// <summary>
    /// Get a string representation of this <see cref="Money"/> object without currency symbol rounded away from zero to the specified number of decimal places.
    /// </summary>
    /// <param name="DecimalPlaces">The number of decimal places to include.</param>
    /// <returns>A string in the current money format without currency symbol rounded away from zero to the specified number of decimal places.</returns>
    public string ToStringWithoutCurrencySymbol(int DecimalPlaces)
    {
      return AmountField.ToString("C" + DecimalPlaces).Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol, "");
    }
    /// <summary>
    /// Get a string representation of this <see cref="Money"/> object rounded away from zero to the specified number of decimal places, with the integral part removed if zero.
    /// </summary>
    /// <param name="DecimalPlaces">The number of decimal places to round to, rounding away from zero.</param>
    /// <returns>A string in the current money format, rounded away from zero to the specified number of decimal places, with the integral part removed if zero.</returns>
    public string ToStringCentPart(int DecimalPlaces)
    {
      return string.Format("{0:#." + new string('0', DecimalPlaces) + "}", GetAmount());
    }
    /// <summary>
    /// Compare this <see cref="Money"/> object to another <see cref="Money"/> object.
    /// </summary>
    /// <param name="obj">The <see cref="Money"/> object to compare to this <see cref="Money"/> object.</param>
    /// <returns>True if the two <see cref="Money"/> objects represent the same amount.</returns>
    public override bool Equals(object obj)
    {
      var Source = obj as Money?;

      return Source != null && EqualTo(Source.Value);
    }
    /// <summary>
    /// Returns the hash code for this <see cref="Money"/>.
    /// </summary>
    /// <returns>The hash code for this <see cref="Money"/>.</returns>
    public override int GetHashCode()
    {
      return this.AmountField.GetHashCode();
    }

    /// <summary>
    /// Negates the value of the specified <see cref="Money"/> operand.
    /// </summary>
    /// <param name="t">The value to negate.</param>
    /// <returns>The result of <paramref name="t"/> multiplied by negative one (-1).</returns>
    public static Money operator -(Money t)
    {
      return new Money(-t.AmountField);
    }
    /// <summary>
    /// Subtracts two specified System.Decimal values.
    /// </summary>
    /// <param name="t1">The minuend.</param>
    /// <param name="t2">The subtrahend.</param>
    /// <returns>The result of subtracting <paramref name="t2"/> from <paramref name="t1"/>.</returns>
    public static Money operator -(Money t1, Money t2)
    {
      return new Money(t1.AmountField - t2.AmountField);
    }
    /// <summary>
    /// Returns the value of the <see cref="Money"/> operand (the sign of the operand is unchanged).
    /// </summary>
    /// <param name="t">The operand to return.</param>
    /// <returns>The value of the operand, <paramref name="t"/>.</returns>
    public static Money operator +(Money t)
    {
      return new Money(+t.AmountField);
    }
    /// <summary>
    /// Adds two specified <see cref="Money"/> values.
    /// </summary>
    /// <param name="t1">The first value to add.</param>
    /// <param name="t2">The second value to add.</param>
    /// <returns>The result of adding <paramref name="t1"/> and <paramref name="t2"/>.</returns>
    /// <exception cref="OverflowException">The return value is less than <see cref="decimal.MinValue"/> or greater than <see cref="decimal.MaxValue"/>.</exception>
    public static Money operator +(Money t1, Money t2)
    {
      return new Money(t1.AmountField + t2.AmountField);
    }
    /// <summary>
    /// Multiplies two specified <see cref="Money"/> values.
    /// </summary>
    /// <param name="t1">The first value to multiply.</param>
    /// <param name="t2">The second value to multiply.</param>
    /// <returns>The result of multiplying <paramref name="t1"/> by <paramref name="t2"/>.</returns>
    /// <exception cref="OverflowException">The return value is less than <see cref="decimal.MinValue"/> or greater than <see cref="decimal.MaxValue"/>.</exception>
    public static Money operator *(Money t1, Money t2)
    {
      return new Money(t1.AmountField * t2.AmountField);
    }
    /// <summary>
    /// Multiplies a <see cref="Money"/> value by a <see cref="decimal"/> value.
    /// </summary>
    /// <param name="t1">The first value to multiply.</param>
    /// <param name="t2">The second value to multiply.</param>
    /// <returns>The result of multiplying <paramref name="t1"/> by <paramref name="t2"/>.</returns>
    /// <exception cref="OverflowException">The return value is less than <see cref="decimal.MinValue"/> or greater than <see cref="decimal.MaxValue"/>.</exception>
    public static Money operator *(Money t1, decimal t2)
    {
      return new Money(t1.AmountField * t2);
    }
    /// <summary>
    /// Multiplies a <see cref="decimal"/> value by a <see cref="Money"/> value.
    /// </summary>
    /// <param name="t1">The first value to multiply.</param>
    /// <param name="t2">The second value to multiply.</param>
    /// <returns>The result of multiplying <paramref name="t1"/> by <paramref name="t2"/>.</returns>
    /// <exception cref="OverflowException">The return value is less than <see cref="decimal.MinValue"/> or greater than <see cref="decimal.MaxValue"/>.</exception>
    public static Money operator *(decimal t1, Money t2)
    {
      return new Money(t1 * t2.AmountField);
    }
    /// <summary>
    /// Divides two specified <see cref="Money"/> values.
    /// </summary>
    /// <param name="t1">The dividend.</param>
    /// <param name="t2">The divisor.</param>
    /// <returns>The result of dividing <paramref name="t1"/> by <paramref name="t2"/>.</returns>
    /// <exception cref="DivideByZeroException"><paramref name="t2"/> is zero.</exception>
    /// <exception cref="OverflowException">The return value is less than <see cref="decimal.MinValue"/> or greater than <see cref="decimal.MaxValue"/>.</exception>
    public static Money operator /(Money t1, Money t2)
    {
      return new Money(t1.AmountField / t2.AmountField);
    }
    /// <summary>
    /// Divides a <see cref="Money"/> value by a <see cref="decimal"/> value.
    /// </summary>
    /// <param name="t1">The dividend.</param>
    /// <param name="t2">The divisor.</param>
    /// <returns>The result of dividing <paramref name="t1"/> by <paramref name="t2"/>.</returns>
    /// <exception cref="DivideByZeroException"><paramref name="t2"/> is zero.</exception>
    /// <exception cref="OverflowException">The return value is less than <see cref="decimal.MinValue"/> or greater than <see cref="decimal.MaxValue"/>.</exception>
    public static Money operator /(Money t1, decimal t2)
    {
      return new Money(t1.AmountField / t2);
    }
    /// <summary>
    /// Divides a <see cref="decimal"/> value by a <see cref="Money"/> value.
    /// </summary>
    /// <param name="t1">The dividend.</param>
    /// <param name="t2">The divisor.</param>
    /// <returns>The result of dividing <paramref name="t1"/> by <paramref name="t2"/>.</returns>
    /// <exception cref="DivideByZeroException"><paramref name="t2"/> is zero.</exception>
    /// <exception cref="OverflowException">The return value is less than <see cref="decimal.MinValue"/> or greater than <see cref="decimal.MaxValue"/>.</exception>
    public static Money operator /(decimal t1, Money t2)
    {
      return new Money(t1 / t2.AmountField);
    }
    /// <summary>
    /// Returns a value indicating whether a specified <see cref="Money"/> is less than another specified <see cref="Money"/>.
    /// </summary>
    /// <param name="t1">The first value to compare.</param>
    /// <param name="t2">The second value to compare.</param>
    /// <returns><see langword="true"/> if <paramref name="t1"/> is less than <paramref name="t2"/>; otherwise, <see langword="false"/>.</returns>
    public static bool operator <(Money t1, Money t2)
    {
      return t1.AmountField < t2.AmountField;
    }
    /// <summary>
    /// Returns a value indicating whether a specified <see cref="Money"/> is less than or equal to another specified <see cref="Money"/>.
    /// </summary>
    /// <param name="t1">The first value to compare.</param>
    /// <param name="t2">The second value to compare.</param>
    /// <returns><see langword="true"/> if <paramref name="t1"/> is less than or equal to <paramref name="t2"/>; otherwise, <see langword="false"/>.</returns>
    public static bool operator <=(Money t1, Money t2)
    {
      return t1.AmountField <= t2.AmountField;
    }
    /// <summary>
    /// Returns a value that indicates whether two <see cref="Money"/> values are equal.
    /// </summary>
    /// <param name="t1">The first value to compare.</param>
    /// <param name="t2">The second value to compare.</param>
    /// <returns><see langword="true"/> if <paramref name="t1"/> and <paramref name="t2"/> are equal; otherwise, <see langword="false"/>.</returns>
    public static bool operator ==(Money t1, Money t2)
    {
      return t1.AmountField == t2.AmountField;
    }
    /// <summary>
    /// Returns a value that indicates whether two <see cref="Money"/> objects have different values.
    /// </summary>
    /// <param name="t1">The first value to compare.</param>
    /// <param name="t2">The second value to compare.</param>
    /// <returns><see langword="true"/> if <paramref name="t1"/> and <paramref name="t2"/> are not equal; otherwise, <see langword="false"/>.</returns>
    public static bool operator !=(Money t1, Money t2)
    {
      return t1.AmountField != t2.AmountField;
    }
    /// <summary>
    /// Returns a value indicating whether a specified <see cref="Money"/> is greater than another specified <see cref="Money"/>.
    /// </summary>
    /// <param name="t1">The first value to compare.</param>
    /// <param name="t2">The second value to compare.</param>
    /// <returns><see langword="true"/> if <paramref name="t1"/> is greater than <paramref name="t2"/>; otherwise, <see langword="false"/>.</returns>
    public static bool operator >(Money t1, Money t2)
    {
      return t1.AmountField > t2.AmountField;
    }
    /// <summary>
    /// Returns a value indicating whether a specified <see cref="Money"/> is greater than or equal to another specified <see cref="Money"/>.
    /// </summary>
    /// <param name="t1">The first value to compare.</param>
    /// <param name="t2">The second value to compare.</param>
    /// <returns><see langword="true"/> if <paramref name="t1"/> is greater than or equal to <paramref name="t2"/>; otherwise, <see langword="false"/>.</returns>
    public static bool operator >=(Money t1, Money t2)
    {
      return t1.AmountField >= t2.AmountField;
    }

    private static decimal As4DP(decimal Value)
    {
      var Result = Math.Round(Value, 4);
      
      if (Value > 0 && Result > Value)
        return Result - new decimal(1, 0, 0, false, 4);
      
      if (Value < 0 && Result < Value)
        return Result + new decimal(1, 0, 0, false, 4);

      return Result.Normalise();
    }

    int IComparable<Money>.CompareTo(Money other)
    {
      return CompareTo(other);
    }
    bool IEquatable<Money>.Equals(Money other)
    {
      return EqualTo(other);
    }
    int IComparable.CompareTo(object obj)
    {
      return CompareTo((Money)obj);
    }

    private static XmlQualifiedName MySchema(object xs)
    {
      return new XmlQualifiedName("decimal", "http://www.w3.org/2001/XMLSchema");
    }
    System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
    {
      return null; // NOTE: this is required to be null.
    }
    void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
    {
      AmountField = XmlConvert.ToDecimal(reader.ReadInnerXml());
    }
    void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
    {
      writer.WriteRaw(XmlConvert.ToString(AmountField));
    }

    private /*readonly*/ decimal AmountField;
  }
}