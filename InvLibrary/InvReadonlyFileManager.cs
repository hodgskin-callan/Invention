﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Inv.Support;

namespace Inv
{
  public sealed class ReadonlyFileManager
  {
    public ReadonlyFileManager(string FolderPath)
      : this
        (
          ListFunc: (Mask) => new System.IO.DirectoryInfo(FolderPath).GetFiles(Mask).Select(F => F.Name),
          ExistsFunc: (Name) => System.IO.File.Exists(System.IO.Path.Combine(FolderPath, Name)),
          OpenFunc: (Name) => new System.IO.FileStream(System.IO.Path.Combine(FolderPath, Name), System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read, 65536)
        )
    {
    }
    public ReadonlyFileManager(Func<string, IEnumerable<string>> ListFunc, Func<string, bool> ExistsFunc, Func<string, System.IO.Stream> OpenFunc)
    {
      this.ListFunc = ListFunc;
      this.ExistsFunc = ExistsFunc;
      this.OpenFunc = OpenFunc;
    }

    public IEnumerable<string> List(string Mask) => ListFunc(Mask);
    public bool Exists(string Name) => ExistsFunc(Name);
    public System.IO.Stream Open(string Name) => OpenFunc(Name);

    private readonly Func<string, IEnumerable<string>> ListFunc;
    private readonly Func<string, bool> ExistsFunc;
    private readonly Func<string, System.IO.Stream> OpenFunc;
  }
}
