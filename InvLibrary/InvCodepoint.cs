﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Records a file:line stack frame which can be used to automate opening the source code in Visual Studio.
  /// </summary>
  public sealed class Codepoint
  {
    /// <summary>
    /// Create a new codepoint.
    /// </summary>
    public Codepoint()
    {
    }

    /// <summary>
    /// Codepoint is set.
    /// </summary>
    public bool IsSet => LabelFilePath != null;

    /// <summary>
    /// Set the file:line for the stack frame that called Here().
    /// </summary>
    [Conditional("DEBUG")]
    public void Here()
    {
      Apply(new StackFrame(1, true));
    }
    /// <summary>
    /// Set the file:line for the stack frame that is the caller code for the method that called Caller().
    /// </summary>
    [Conditional("DEBUG")]
    public void Caller()
    {
      Apply(new StackFrame(2, true));
    }
    /// <summary>
    /// Set the file:line for the stack frame at the specified depth.
    /// </summary>
    /// <param name="Depth"></param>
    [Conditional("DEBUG")]
    public void Set(int Depth = 0)
    {
      Apply(new StackFrame(Depth + 1, true));
    }
    /// <summary>
    /// Clones the value of <paramref name="Other"/> into this instance.
    /// </summary>
    /// <param name="Other">A <see cref="Inv.Codepoint"/> to copy.</param>
    [Conditional("DEBUG")]
    public void Copy(Inv.Codepoint Other)
    {
      this.LabelFilePath = Other.LabelFilePath;
      this.LabelLineNumber = Other.LabelLineNumber;
    }
    /// <summary>
    /// Unset the current file:line.
    /// </summary>
    [Conditional("DEBUG")]
    public void Clear()
    {
      this.LabelFilePath = null;
    }

    public override string ToString() => IsSet ? LabelFilePath + ":" + LabelLineNumber : "N/A";

    private void Apply(StackFrame StackFrame)
    {
      this.LabelFilePath = StackFrame.GetFileName();
      this.LabelLineNumber = StackFrame.GetFileLineNumber();
    }

    internal string LabelFilePath { get; private set; }
    internal int LabelLineNumber { get; private set; }
  }
}
