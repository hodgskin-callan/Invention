﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  public readonly struct Point
  {
    public Point(int X, int Y)
      : this()
    {
      this.X = X;
      this.Y = Y;
    }

    public int X { get; }
    public int Y { get; }

    public override string ToString() => X + "," + Y;
    public override bool Equals(object Other) => Other is Inv.Point Point && EqualTo(Point);
    public override int GetHashCode() => X.GetHashCode() ^ Y.GetHashCode();
    public bool EqualTo(Inv.Point Other) => X == Other.X && Y == Other.Y;
    public Inv.Point Abs() => new Inv.Point(Math.Abs(X), Math.Abs(Y));
    public int ManhattanDistance(Inv.Point Other) => Math.Abs(Other.X - X) + Math.Abs(Other.Y - Y);
    public int EuclideanDistance(Inv.Point Other) => (int)(Math.Sqrt((Math.Pow(this.X - Other.X, 2) + Math.Pow(this.Y - Other.Y, 2))));
    /// <summary>
    /// Rotates this point around the origin point by an angle.
    /// </summary>
    /// <param name="AroundOrigin">The center point to rotate around.</param>
    /// <param name="AngleInDegrees">The rotation angle in degrees.</param>
    /// <returns>Rotated point</returns>
    public Inv.Point Rotate(Inv.Point AroundOrigin, double AngleInDegrees)
    {
      var AngleInRadians = AngleInDegrees * (Math.PI / 180);
      var CosTheta = Math.Cos(AngleInRadians);
      var SinTheta = Math.Sin(AngleInRadians);

      return new Inv.Point
      (
        X: (int)(CosTheta * (this.X - AroundOrigin.X) - SinTheta * (this.Y - AroundOrigin.Y) + AroundOrigin.X),
        Y: (int)(SinTheta * (this.X - AroundOrigin.X) + CosTheta * (this.Y - AroundOrigin.Y) + AroundOrigin.Y)
      );
    }
    /// <summary>
    /// Calculate the angle in degrees from the origin point to this point.
    /// </summary>
    /// <param name="Origin"></param>
    /// <returns></returns>
    public double AngleInDegreesFrom(Inv.Point Origin)
    {
      return Math.Atan2(this.Y - Origin.Y, this.X - Origin.X) * (180 / Math.PI);
    }
    public Inv.Point Translate(int ShiftX, int ShiftY)
    {
      return new Inv.Point(X + ShiftX, Y + ShiftY);
    }
    /// <summary>
    /// Check if the point is inside the circle by comparing the radius of circle with the distance from its origin.
    /// </summary>
    /// <param name="Circle"></param>
    /// <returns></returns>
    public bool IsInside(Inv.Circle Circle)
    {
      return (X - Circle.Origin.X) * (X - Circle.Origin.X) + (Y - Circle.Origin.Y) * (Y - Circle.Origin.Y) <= Circle.Radius * Circle.Radius;
    }
    public static readonly Inv.Point Zero = new Inv.Point(0, 0);
    public static bool operator ==(Inv.Point Left, Inv.Point Right) => Left.EqualTo(Right);
    public static bool operator !=(Inv.Point Left, Inv.Point Right) => !Left.EqualTo(Right);
    public static Inv.Point operator +(Inv.Point Left, Inv.Point Right) => new Inv.Point(Left.X + Right.X, Left.Y + Right.Y);
    public static Inv.Point operator -(Inv.Point Left, Inv.Point Right) => new Inv.Point(Left.X - Right.X, Left.Y - Right.Y);
    public static Inv.Point operator *(Inv.Point Left, int Right) => new Inv.Point(Left.X * Right, Left.Y * Right);
    public static Inv.Point operator /(Inv.Point Left, int Right) => new Inv.Point(Left.X / Right, Left.Y / Right);
    public static Inv.Point operator %(Inv.Point Left, int Right) => new Inv.Point(Left.X % Right, Left.Y % Right);
    public static Inv.Point operator +(Inv.Point Left, Inv.Dimension Right) => new Inv.Point(Left.X + Right.Width, Left.Y + Right.Height);
    public static Inv.Point operator -(Inv.Point Left, Inv.Dimension Right) => new Inv.Point(Left.X - Right.Width, Left.Y - Right.Height);
  }

  public readonly struct Dimension
  {
    public Dimension(int Width, int Height)
      : this()
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Width >= 0, "Width must be zero or positive: {0}", Width);
        Inv.Assert.Check(Height >= 0, "Height must be zero or positive: {0}", Height);
      }

      this.Width = Width;
      this.Height = Height;
    }

    public int Width { get; }
    public int Height { get; }

    public override string ToString() => Width + "x" + Height;
    public override bool Equals(object Other) => Other is Inv.Dimension Dimension && EqualTo(Dimension);
    public override int GetHashCode() => Width.GetHashCode() ^ Height.GetHashCode();
    public bool EqualTo(Inv.Dimension Other) => Width == Other.Width && Height == Other.Height;
    public Inv.Point ToPoint() => new Inv.Point(Width, Height);

    public static readonly Inv.Dimension Zero = new Inv.Dimension(0, 0);
    public static bool operator ==(Inv.Dimension Left, Inv.Dimension Right) => Left.EqualTo(Right);
    public static bool operator !=(Inv.Dimension Left, Inv.Dimension Right) => !Left.EqualTo(Right);
    public static Inv.Dimension operator /(Inv.Dimension Left, int Value) => new Inv.Dimension(Left.Width / Value, Left.Height / Value);
    public static Inv.Dimension operator *(Inv.Dimension Left, int Value) => new Inv.Dimension(Left.Width * Value, Left.Height * Value);
    public static Inv.Dimension operator -(Inv.Dimension Left, Inv.Dimension Right) => new Inv.Dimension(Left.Width - Right.Width, Left.Height - Right.Height);
    public static Inv.Dimension operator +(Inv.Dimension Left, Inv.Dimension Right) => new Inv.Dimension(Left.Width + Right.Width, Left.Height + Right.Height);
  }

  public readonly struct Rect
  {
    public Rect(int Left, int Top, int Width, int Height)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Width >= 0, "Width must not be zero or negative: {0}", Width);
        Inv.Assert.Check(Height >= 0, "Height must not be zero or negative: {0}", Height);
      }

      this.Left = Left;
      this.Top = Top;
      this.Right = Left + Width - 1; // denormalised for use by Android.
      this.Bottom = Top + Height - 1; // denormalised for use by Android.
      this.Width = Width;
      this.Height = Height;
    }
    public Rect(Inv.Point Origin, Inv.Dimension Dimension)
      : this(Origin.X, Origin.Y, Dimension.Width, Dimension.Height)
    {
    }
    public Rect(Inv.Point TopLeft, Inv.Point BottomRight)
      : this(TopLeft.X, TopLeft.Y, BottomRight.X - TopLeft.X + 1, BottomRight.Y - TopLeft.Y + 1)
    {
    }

    public int Left { get; }
    public int Top { get; }
    public int Right { get; }
    public int Bottom { get; }
    public int Width { get; }
    public int Height { get; }

    public override string ToString() => Left + "," + Top + ";" + Width + "x" + Height;
    public override bool Equals(object Other) => Other is Inv.Rect Rect && EqualTo(Rect);
    public override int GetHashCode() => Left.GetHashCode() ^ Top.GetHashCode() ^ Width.GetHashCode() ^ Height.GetHashCode();
    public bool EqualTo(Rect Other) => Left == Other.Left && Top == Other.Top && Width == Other.Width && Height == Other.Height;
    public bool Contains(Inv.Point Point) => Point.X >= Left && Point.X <= Right && Point.Y >= Top && Point.Y <= Bottom;
    public Inv.Point TopLeft() => new Inv.Point(Left, Top);
    public Inv.Point TopRight() => new Inv.Point(Right, Top);
    public Inv.Point BottomLeft() => new Inv.Point(Left, Bottom);
    public Inv.Point BottomRight() => new Inv.Point(Right, Bottom);
    public Inv.Point TopCenter() => new Inv.Point(Left + (Width / 2), Top);
    public Inv.Point BottomCenter() => new Inv.Point(Left + (Width / 2), Bottom);
    public Inv.Point Center() => new Inv.Point(Left + (Width / 2), Top + (Height / 2));
    public Inv.Point CenterRight() => new Inv.Point(Right, Top + (Height / 2));
    public Inv.Point CenterLeft() => new Inv.Point(Left, Top + (Height / 2));
    public Inv.Dimension Dimension() => new Inv.Dimension(Width, Height);
    /// <summary>
    /// Return a new rect expanded around the center of this rect.
    /// This means the rect shifts up and to the left by the <paramref name="Size"/> and width and height increase by 2 x <paramref name="Size"/>.
    /// For example:
    /// <code>Rect(10, 10, 5, 5).Expand(5) -> Rect(5, 5, 15, 15)</code>
    /// </summary>
    /// <param name="Size"></param>
    /// <returns></returns>
    public Inv.Rect Expand(int Size) => new Inv.Rect(Left - Size, Top - Size, Width + (Size * 2), Height + (Size * 2));
    public Inv.Rect Reduce(int Size) => new Inv.Rect(Left + Size, Top + Size, Width - (Size * 2), Height - (Size * 2));
    /// <summary>
    /// Returns an array of rects that represents the removal of OtherRect from this Inv.Rect.
    /// </summary>
    /// <param name="OtherRect"></param>
    /// <returns>An array of rects representing the region left after removing OtherRect from this Inv.Rect. If the two rects do not intersect, null is returned.</returns>
    public Inv.Rect[] Subtract(Inv.Rect OtherRect)
    {
      if (Top > OtherRect.Bottom || Bottom < OtherRect.Top || Left > OtherRect.Right || Right < OtherRect.Left)
        return null;

      static Inv.Rect? CheckRect(int Left, int Top, int Right, int Bottom) => Left > Right || Top > Bottom ? (Inv.Rect?)null : new Inv.Rect(Left, Top, Right - Left + 1, Bottom - Top + 1);

      var AboveRect = CheckRect(Left, Top, Right, OtherRect.Top - 1);
      var BelowRect = CheckRect(Left, OtherRect.Bottom + 1, Right, Bottom);
      var LeftRect = CheckRect(Left, Math.Max(Top, OtherRect.Top), OtherRect.Left - 1, Math.Min(Bottom, OtherRect.Bottom));
      var RightRect = CheckRect(OtherRect.Right + 1, Math.Max(Top, OtherRect.Top), Right, Math.Min(Bottom, OtherRect.Bottom));

      return new[]
      {
        AboveRect,
        BelowRect,
        LeftRect,
        RightRect
      }.ExceptNull().ToArray();
    }
    public Inv.Rect Translate(int X, int Y)
    {
      return new Inv.Rect(Left + X, Top + Y, Width, Height);
    }
    public Inv.Rect Translate(Inv.Point Point)
    {
      return new Inv.Rect(Left + Point.X, Top + Point.Y, Width, Height);
    }
    public bool IntersectsWith(Inv.Rect OtherRect)
    {
      return 
        OtherRect.Left < this.Left + this.Width && 
        this.Left < OtherRect.Left + OtherRect.Width && 
        OtherRect.Top < this.Top + this.Height &&
        this.Top < OtherRect.Top + OtherRect.Height;
    }

    public static readonly Inv.Rect Empty = new Inv.Rect(0, 0, 0, 0);
    public static bool operator ==(Inv.Rect Left, Inv.Rect Right) => Left.EqualTo(Right);
    public static bool operator !=(Inv.Rect Left, Inv.Rect Right) => !Left.EqualTo(Right);
    public static Inv.Rect operator /(Inv.Rect Rect, int Factor) => new Inv.Rect(Rect.TopLeft() / Factor, Rect.BottomRight() / Factor);
  }

  public readonly struct Circle
  {
    public Circle(Inv.Point Origin, int Radius)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Radius >= 0, "Radius must not be a negative number.");

      this.Origin = Origin;
      this.Radius = Radius;
    }
    public Circle(int X, int Y, int Radius)
      : this(new Inv.Point(X, Y), Radius)
    {
    }

    public Inv.Point Origin { get; }
    public int X => Origin.X;
    public int Y => Origin.Y;
    public int Radius { get; }

    public override string ToString() => $"({Origin}) r{Radius}";
    public override bool Equals(object Other) => Other is Inv.Circle Circle && EqualTo(Circle);
    public override int GetHashCode() => X.GetHashCode() ^ Y.GetHashCode();
    public bool EqualTo(Circle Other) => Origin == Other.Origin && Radius == Other.Radius;

    /// <summary>
    /// Return a new circle with the origin unchanged but an increased radius.
    /// </summary>
    /// <param name="Size"></param>
    /// <returns></returns>
    public Inv.Circle Expand(int Size) => new Inv.Circle(X, Y, Radius + Size);
    /// <summary>
    /// Return a new circle with the origin unchanged but a decreased radius.
    /// </summary>
    /// <param name="Size"></param>
    /// <returns></returns>
    public Inv.Circle Reduce(int Size) => new Inv.Circle(X, Y, Radius - Size);
    /// <summary>
    /// Returns each point that exists inside the circle.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<Inv.Point> GetAreaPoints()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Radius > 0, "Radius must be greater than zero.");

      static IEnumerable<Inv.Point> Plot4points(Inv.Point origin, int px, int py)
      {
        for (var i = origin.X - px; i <= origin.X + px; ++i)
          yield return new Inv.Point(i, origin.Y + py);

        if (py != 0)
        {
          for (var i = origin.X - px; i <= origin.X + px; ++i)
            yield return new Inv.Point(i, origin.Y - py);
        }
      }

      var error = -Radius;
      var x = Radius;
      var y = 0;

      while (x >= y)
      {
        var lastY = y;

        error += y;
        ++y;
        error += y;

        foreach (var Point in Plot4points(Origin, x, lastY))
          yield return Point;

        if (error >= 0)
        {
          if (x != lastY)
          {
            foreach (var Point in Plot4points(Origin, lastY, x))
              yield return Point;
          }

          error -= x;
          --x;
          error -= x;
        }
      }
    }
    public Inv.Point TopLeftBoundingPoint() => new Inv.Point(X - Radius, Y - Radius);
    public Inv.Point BottomRightBoundingPoint() => new Inv.Point(X + Radius + 1, Y + Radius + 1);
    public Inv.Rect BoundingRect() => new Inv.Rect(TopLeftBoundingPoint(), BottomRightBoundingPoint());

    public static readonly Inv.Circle Zero = new Inv.Circle(Inv.Point.Zero, 0);
    public static bool operator ==(Inv.Circle Left, Inv.Circle Right) => Left.EqualTo(Right);
    public static bool operator !=(Inv.Circle Left, Inv.Circle Right) => !Left.EqualTo(Right);
  }

  public readonly struct Triangle
  {
    public Triangle(Inv.Point VertexA, Inv.Point VertexB, Inv.Point VertexC)
    {
      this.VertexA = VertexA;
      this.VertexB = VertexB; 
      this.VertexC = VertexC;
    }

    public Inv.Point VertexA { get; }
    public Inv.Point VertexB { get; }
    public Inv.Point VertexC { get; }

    public override string ToString() => $"({VertexA}),({VertexB}),({VertexC})";
    public override bool Equals(object Other) => Other is Inv.Triangle Triangle && EqualTo(Triangle);
    public override int GetHashCode() => VertexA.GetHashCode() ^ VertexB.GetHashCode() ^ VertexC.GetHashCode();
    public bool EqualTo(Triangle Other) => VertexA == Other.VertexA && VertexB == Other.VertexB && VertexC == Other.VertexC;

    public Inv.Point Midpoint()
    {
      return new Inv.Point((VertexA.X + VertexB.X + VertexC.X) / 3, (VertexA.Y + VertexB.Y + VertexC.Y) / 3);
    }
    public Inv.Point MinimumBound()
    {
      var MinX = VertexA.X;
      
      if (VertexB.X < MinX)
        MinX = VertexB.X;
      
      if (VertexC.X < MinX)
        MinX = VertexC.X;

      var MinY = VertexA.Y;

      if (VertexB.Y < MinY)
        MinY = VertexB.Y;

      if (VertexC.Y < MinY)
        MinY = VertexC.Y;

      return new Inv.Point(MinX, MinY);
    }
    public Inv.Point MaximumBound()
    {
      var MaxX = VertexA.X;

      if (VertexB.X > MaxX)
        MaxX = VertexB.X;

      if (VertexC.X > MaxX)
        MaxX = VertexC.X;

      var MaxY = VertexA.Y;

      if (VertexB.Y > MaxY)
        MaxY = VertexB.Y;

      if (VertexC.Y > MaxY)
        MaxY = VertexC.Y;

      return new Inv.Point(MaxX, MaxY);
    }
    public IEnumerable<Inv.Point> GetAreaPoints()
    {
      var x0 = VertexA.X;
      var y0 = VertexA.Y;
      var x1 = VertexB.X;
      var y1 = VertexB.Y;
      var x2 = VertexC.X;
      var y2 = VertexC.Y;

      // sort the points vertically
      if (y1 > y2)
      {
        (x2, x1) = (x1, x2);
        (y2, y1) = (y1, y2);
      }

      if (y0 > y1)
      {
        (x1, x0) = (x0, x1);
        (y1, y0) = (y0, y1);
      }

      if (y1 > y2)
      {
        (x2, x1) = (x1, x2);
        (y2, y1) = (y1, y2);
      }

      double dx_far = Convert.ToDouble(x2 - x0) / (y2 - y0 + 1);
      double dx_upper = Convert.ToDouble(x1 - x0) / (y1 - y0 + 1);
      double dx_low = Convert.ToDouble(x2 - x1) / (y2 - y1 + 1);
      double xf = x0;
      double xt = x0 + dx_upper; // if y0 == y1, special case

      for (var y = y0; y <= y2; y++)
      {
        if (y >= 0)
        {
          for (int x = (xf > 0 ? Convert.ToInt32(xf) : 0); x <= xt; x++)
            yield return new Inv.Point(x, y);

          for (int x = Convert.ToInt32(xf); x >= (xt > 0 ? xt : 0); x--)
            yield return new Inv.Point(x, y);
        }

        xf += dx_far;

        if (y < y1)
          xt += dx_upper;
        else
          xt += dx_low;
      }
    }
    public bool IntersectsWith(Inv.Rect Rect)
    {
      foreach (var Point in GetAreaPoints())
      {
        if (Rect.Contains(Point))
          return true;
      }

      return false;
    }

    public static readonly Inv.Triangle Zero = new Inv.Triangle(Inv.Point.Zero, Inv.Point.Zero, Inv.Point.Zero);
    public static bool operator ==(Inv.Triangle Left, Inv.Triangle Right) => Left.EqualTo(Right);
    public static bool operator !=(Inv.Triangle Left, Inv.Triangle Right) => !Left.EqualTo(Right);
  }

  public readonly struct Coordinate
  {
    public Coordinate(double Latitude, double Longitude, double Altitude)
    {
      this.Latitude = Latitude;
      this.Longitude = Longitude;
      this.Altitude = Altitude;
    }

    public double Latitude { get; }
    public double Longitude { get; }
    public double Altitude { get; }

    /// <summary>
    /// Returns the latitude and longitude as a human readable string. eg. 37.3861° N, 122.0839° W
    /// </summary>
    public string ToPosition()
    {
      string Result;

      if (Latitude < 0)
        Result = -Latitude + "° S";
      else if (Latitude > 0)
        Result = Latitude + "° N";
      else
        Result = Latitude + "°";

      Result += ", ";

      if (Longitude < 0)
        Result += -Longitude + "° E";
      else if (Longitude > 0)
        Result += Longitude + "° W";
      else
        Result += Longitude + "°";

      return Result;
    }
  }
}