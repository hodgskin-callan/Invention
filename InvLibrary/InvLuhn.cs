﻿using System.Linq;
using System.Text.RegularExpressions;

namespace Inv
{
  public static class Luhn
  {
    public static bool IsValid(string CardNumber)
    {
      var DigitArray = new int[CardNumber.Length];
      var NumberIndex = 0;
      foreach (var Number in CardNumber)
        DigitArray[NumberIndex++] = Number - '0';

      if (DigitArray.Any(D => D < 0 || D > 9))
        return false;

      var Index = 0;
      var LengthMod = DigitArray.Length % 2;

      var Result = DigitArray.Sum(d => Index++ % 2 == LengthMod ? results[d] : d) % 10;

      return Result == 0;
    }
    // Minimum valid card is 7 (6 digit IIN plus 1 check digit). 
    public static bool IsMinimumCardLength(string CardNumber)
    {
      return CardNumber.Length > 6;
    }
    public static bool IsAmex(string CardNumber)
    {
      if (!IsMinimumCardLength(CardNumber))
        return false;

      return AmexRegex.IsMatch(CardNumber);
    }
    public static bool IsDiners(string CardNumber)
    {
      if (!IsMinimumCardLength(CardNumber))
        return false;

      return DinersRegex.IsMatch(CardNumber);
    }
    public static bool IsDinersOrAmex(string CardNumber)
    {
      if (!IsMinimumCardLength(CardNumber))
        return false;

      return DinersRegex.IsMatch(CardNumber) || AmexRegex.IsMatch(CardNumber);
    }
    public static int? Mod10V01CheckDigit(string IdNumber)
    {
      var DigitArray = new int[IdNumber.Length];
      var NumberIndex = 0;
      foreach (var Number in IdNumber)
        DigitArray[NumberIndex++] = Number - '0';

      if (DigitArray.Any(D => D < 0 || D > 9))
        return null;

      var Index = 0;
      var LengthMod = DigitArray.Length % 2;

      var Result = DigitArray.Sum(d => ++Index % 2 == LengthMod ? results[d] : d) % 10;
      return Result % 10 == 0 ? 0 : 10 - Result;
    }
    public static int? Mod10V05CheckDigit(string IdNumber)
    {
      var DigitArray = new int[IdNumber.Length];
      var NumberIndex = 0;
      foreach (var Number in IdNumber)
        DigitArray[NumberIndex++] = Number - '0';

      if (DigitArray.Any(D => D < 0 || D > 9))
        return null;

      var Index = 0;
      return DigitArray.Sum(D => D * ++Index) % 10;
    }
    /// <summary>
    /// Generate a Bpay compatible Customer Reference Number(CRN).
    /// <para>(Appends a check digit to the identifier)</para>
    /// </summary>
    /// <param name="IdNumber">From 2 to 19 digits length</param>
    /// <param name="Version">Choose check digit encoding algorithm</param>
    /// <returns>Empty string returned if incompatible IdNumber</returns>
    public static string BpayCrn(string IdNumber, BpayAlgorithm Version)
    {
      if (IdNumber.Length < 2 || IdNumber.Length > 19)
        return string.Empty;

      var CheckDigit = Version == BpayAlgorithm.MOD10V01 ? Mod10V01CheckDigit(IdNumber) : Mod10V05CheckDigit(IdNumber);

      return CheckDigit.HasValue ? $"{IdNumber}{CheckDigit}" : string.Empty;
    }

    /// <summary>
    /// Bpay checkdigit encoding types (Seems at least one (MOD10V01 or MOD10V05) will satisfy default encoding accepted by the major banks)
    /// </summary>
    public enum BpayAlgorithm
    {
      MOD10V01,
      MOD10V05
    }

    private static readonly int[] results = { 0, 2, 4, 6, 8, 1, 3, 5, 7, 9 };
    // DINERS CLUB start with 300 through 305, 36 or 38.
    private static readonly Regex DinersRegex = new Regex("^3(?:0[0-5]|[68][0-9])[0-9]{4,}$");
    // AMEX start with 34 or 37.
    private static readonly Regex AmexRegex = new Regex("^3[47][0-9]{5,}$");
  }
}
