﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Abstract grid of colours.
  /// Implemented as several classes because each platform uses a different underlying data structure and using a Grid of Colour required too much memory.
  /// </summary>
  public interface Pixels
  {
    int Width { get; }
    int Height { get; }
    Inv.Colour this[int X, int Y] { get; set; }
    void Fill(Inv.Colour Value);
    Inv.Pixels Crop(Inv.Rect Rect);
    Inv.Pixels Copy();
    int FindBaseline();
  }

  /// <summary>
  /// Byte array of RGBA pixels that make up an image.
  /// </summary>
  public sealed class RGBAByteArrayPixels : Pixels
  {
    public RGBAByteArrayPixels(Inv.Pixels Pixels)
      : this(Pixels.Width, Pixels.Height)
    {
      for (var PixelY = 0; PixelY < Height; PixelY++)
      {
        for (var PixelX = 0; PixelX < Width; PixelX++)
        {
          var PixelIndex = (PixelY * PixelStride) + (PixelSize * PixelX);

          var PixelColour = Pixels[PixelX, PixelY].GetARGBRecord();

          PixelArray[PixelIndex] = PixelColour.R;
          PixelArray[PixelIndex + 1] = PixelColour.G;
          PixelArray[PixelIndex + 2] = PixelColour.B;
          PixelArray[PixelIndex + 3] = PixelColour.A;
        }
      }
    }
    public RGBAByteArrayPixels(int Width, int Height)
      : this(Width, Height, new byte[Width * PixelSize * Height])
    {
    }
    public RGBAByteArrayPixels(int Width, int Height, byte[] PixelArray)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Width * Height * PixelSize == PixelArray.Length, "PixelArray must match the provided width and height.");

      this.Width = Width;
      this.Height = Height;
      this.PixelCount = Width * Height;
      this.PixelStride = Width * PixelSize;
      this.PixelArray = PixelArray;
    }

    /// <summary>
    /// Underlying data structure for optimised access.
    /// </summary>
    public byte[] PixelArray { get; }
    /// <summary>
    /// Total number of pixels (Width * Height).
    /// </summary>
    public int PixelCount { get; }
    /// <summary>
    /// Number of bytes per row of pixels.
    /// </summary>
    public int PixelStride { get; }
    /// <summary>
    /// The width of the grid.
    /// </summary>
    public int Width { get; }
    /// <summary>
    /// The height of the grid.
    /// </summary>
    public int Height { get; }
    /// <summary>
    /// Gets or sets the pixel at the specified zero-based index coordinates.
    /// </summary>
    /// <param name="X">The zero-based index of the X coordinate.</param>
    /// <param name="Y">The zero-based index of the Y coordinate.</param>
    /// <returns>The pixel at the specified coordinates.</returns>
    public Inv.Colour this[int X, int Y]
    {
      [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
      get
      {
        var PixelIndex = (Y * PixelStride) + (X * PixelSize);
        var PixelR = PixelArray[PixelIndex];
        var PixelG = PixelArray[PixelIndex + 1];
        var PixelB = PixelArray[PixelIndex + 2];
        var PixelA = PixelArray[PixelIndex + 3];

        return Inv.Colour.FromArgb(PixelA, PixelR, PixelG, PixelB);
      }
      [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
      set
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.CheckNotNull(value, nameof(value));

        var PixelRecord = value?.GetARGBRecord() ?? new ColourARGBRecord();

        var PixelIndex = (Y * PixelStride) + (X * PixelSize);
        PixelArray[PixelIndex] = PixelRecord.R;
        PixelArray[PixelIndex + 1] = PixelRecord.G;
        PixelArray[PixelIndex + 2] = PixelRecord.B;
        PixelArray[PixelIndex + 3] = PixelRecord.A;
      }
    }

    /// <summary>
    /// Populate the grid with a default colour.
    /// </summary>
    public void Fill(Inv.Colour Value)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Value, nameof(Value));

      var PixelRecord = Value?.GetARGBRecord() ?? new ColourARGBRecord();

      var PixelIndex = 0;
      while (PixelIndex < PixelCount)
      {
        PixelArray[PixelIndex] = PixelRecord.R;
        PixelArray[PixelIndex + 1] = PixelRecord.G;
        PixelArray[PixelIndex + 2] = PixelRecord.B;
        PixelArray[PixelIndex + 3] = PixelRecord.A;

        PixelIndex += PixelSize;
      }
    }
    /// <summary>
    /// Make a shallow copy of the pixels.
    /// </summary>
    /// <returns></returns>    
    public Inv.RGBAByteArrayPixels Copy()
    {
      return new Inv.RGBAByteArrayPixels(Width, Height, (byte[])PixelArray.Clone());
    }
    /// <summary>
    /// Crop out the pixels to the provided rect.
    /// </summary>
    public Inv.RGBAByteArrayPixels Crop(Inv.Rect Rect)
    {
      var Result = new Inv.RGBAByteArrayPixels(Rect.Width, Rect.Height);

      var ResultY = 0;
      for (var CropY = Rect.Top; CropY <= Rect.Bottom; CropY++)
      {
        var ResultX = 0;
        for (var CropX = Rect.Left; CropX <= Rect.Right; CropX++)
        {
          var ResultIndex = (ResultY * PixelStride) + (ResultX * PixelSize);
          var CropIndex = (CropY * PixelStride) + (CropX * PixelSize);

          PixelArray[ResultIndex] = PixelArray[CropIndex];
          PixelArray[ResultIndex + 1] = PixelArray[CropIndex + 1];
          PixelArray[ResultIndex + 2] = PixelArray[CropIndex + 2];
          PixelArray[ResultIndex + 3] = PixelArray[CropIndex + 3];

          ResultX++;
        }

        ResultY++;
      }

      return Result;
    }
    /// <summary>
    /// Check to see if the reference pixels are an exact match.
    /// </summary>
    /// <param name="Reference"></param>
    /// <returns></returns>
    public bool EqualTo(Inv.RGBAByteArrayPixels Reference)
    {
      return PixelArray.ShallowEqualTo(Reference.PixelArray);
    }
    /// <summary>
    /// Find the baseline of the image - the first row, from the bottom, that contains a non-transparent pixel.
    /// </summary>
    /// <returns></returns>
    public int FindBaseline()
    {
      var PixelRow = (Height - 1) * PixelStride;
      for (var PixelY = Height - 1; PixelY >= 0; PixelY--)
      {
        var CropRow = true;

        var PixelColumn = PixelRow;
        for (var PixelX = 0; PixelX < Width; PixelX++)
        {
          if (PixelArray[PixelColumn + 3] != 0x00)
          {
            CropRow = false;
            break;
          }

          PixelColumn += PixelSize;
        }

        if (!CropRow)
          return PixelY;

        PixelRow -= PixelStride;
      }

      return Height - 1;
    }

    Inv.Pixels Inv.Pixels.Copy() => Copy();
    Inv.Pixels Inv.Pixels.Crop(Inv.Rect Rect) => Crop(Rect);

    public const int PixelSize = 4;
  }

  /// <summary>
  /// Byte array of RGBA pixels that make up an image.
  /// </summary>
  public sealed class BGRAByteArrayPixels : Pixels
  {
    public BGRAByteArrayPixels(Inv.Pixels Pixels)
      : this(Pixels.Width, Pixels.Height)
    {
      for (var PixelY = 0; PixelY < Height; PixelY++)
      {
        for (var PixelX = 0; PixelX < Width; PixelX++)
        {
          var PixelIndex = (PixelY * PixelStride) + (PixelSize * PixelX);

          var PixelColour = Pixels[PixelX, PixelY].GetARGBRecord();

          PixelArray[PixelIndex] = PixelColour.B;
          PixelArray[PixelIndex + 1] = PixelColour.G;
          PixelArray[PixelIndex + 2] = PixelColour.R;
          PixelArray[PixelIndex + 3] = PixelColour.A;
        }
      }
    }
    public BGRAByteArrayPixels(int Width, int Height)
      : this(Width, Height, new byte[Width * PixelSize * Height])
    {
    }
    public BGRAByteArrayPixels(int Width, int Height, byte[] PixelArray)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Width * Height * PixelSize == PixelArray.Length, "PixelArray must match the provided width and height.");

      this.Width = Width;
      this.Height = Height;
      this.PixelCount = Width * Height;
      this.PixelStride = Width * PixelSize;
      this.PixelArray = PixelArray;
    }

    /// <summary>
    /// Underlying data structure for optimised access.
    /// </summary>
    public byte[] PixelArray { get; }
    /// <summary>
    /// Total number of pixels (Width * Height).
    /// </summary>
    public int PixelCount { get; }
    /// <summary>
    /// Number of bytes per row of pixels.
    /// </summary>
    public int PixelStride { get; }
    /// <summary>
    /// The width of the grid.
    /// </summary>
    public int Width { get; }
    /// <summary>
    /// The height of the grid.
    /// </summary>
    public int Height { get; }
    /// <summary>
    /// Gets or sets the pixel at the specified zero-based index coordinates.
    /// </summary>
    /// <param name="X">The zero-based index of the X coordinate.</param>
    /// <param name="Y">The zero-based index of the Y coordinate.</param>
    /// <returns>The pixel at the specified coordinates.</returns>
    public Inv.Colour this[int X, int Y]
    {
      [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
      get
      {
        var PixelIndex = (Y * PixelStride) + (X * PixelSize);
        var PixelB = PixelArray[PixelIndex];
        var PixelG = PixelArray[PixelIndex + 1];
        var PixelR = PixelArray[PixelIndex + 2];
        var PixelA = PixelArray[PixelIndex + 3];

        return Inv.Colour.FromArgb(PixelA, PixelR, PixelG, PixelB);
      }
      [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
      set
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.CheckNotNull(value, nameof(value));

        var PixelRecord = value?.GetARGBRecord() ?? new ColourARGBRecord();

        var PixelIndex = (Y * PixelStride) + (X * PixelSize);
        PixelArray[PixelIndex] = PixelRecord.B;
        PixelArray[PixelIndex + 1] = PixelRecord.G;
        PixelArray[PixelIndex + 2] = PixelRecord.R;
        PixelArray[PixelIndex + 3] = PixelRecord.A;
      }
    }

    /// <summary>
    /// Populate the grid with a default colour.
    /// </summary>
    public void Fill(Inv.Colour Value)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Value, nameof(Value));

      var PixelRecord = Value?.GetARGBRecord() ?? new ColourARGBRecord();

      var PixelIndex = 0;
      while (PixelIndex < PixelCount)
      {
        PixelArray[PixelIndex] = PixelRecord.B;
        PixelArray[PixelIndex + 1] = PixelRecord.G;
        PixelArray[PixelIndex + 2] = PixelRecord.R;
        PixelArray[PixelIndex + 3] = PixelRecord.A;

        PixelIndex += PixelSize;
      }
    }
    /// <summary>
    /// Make a shallow copy of the pixels.
    /// </summary>
    /// <returns></returns>    
    public Inv.BGRAByteArrayPixels Copy()
    {
      return new Inv.BGRAByteArrayPixels(Width, Height, (byte[])PixelArray.Clone());
    }
    /// <summary>
    /// Crop out the pixels to the provided rect.
    /// </summary>
    public Inv.BGRAByteArrayPixels Crop(Inv.Rect Rect)
    {
      var Result = new Inv.BGRAByteArrayPixels(Rect.Width, Rect.Height);

      var ResultY = 0;
      for (var CropY = Rect.Top; CropY <= Rect.Bottom; CropY++)
      {
        var ResultX = 0;
        for (var CropX = Rect.Left; CropX <= Rect.Right; CropX++)
        {
          var ResultIndex = (ResultY * PixelStride) + (ResultX * PixelSize);
          var CropIndex = (CropY * PixelStride) + (CropX * PixelSize);

          PixelArray[ResultIndex] = PixelArray[CropIndex];
          PixelArray[ResultIndex + 1] = PixelArray[CropIndex + 1];
          PixelArray[ResultIndex + 2] = PixelArray[CropIndex + 2];
          PixelArray[ResultIndex + 3] = PixelArray[CropIndex + 3];

          ResultX++;
        }

        ResultY++;
      }

      return Result;
    }
    /// <summary>
    /// Check to see if the reference pixels are an exact match.
    /// </summary>
    /// <param name="Reference"></param>
    /// <returns></returns>
    public bool EqualTo(Inv.BGRAByteArrayPixels Reference)
    {
      return PixelArray.ShallowEqualTo(Reference.PixelArray);
    }
    /// <summary>
    /// Find the baseline of the image - the first row, from the bottom, that contains a non-transparent pixel.
    /// </summary>
    /// <returns></returns>
    public int FindBaseline()
    {
      var PixelRow = (Height - 1) * PixelStride;
      for (var PixelY = Height - 1; PixelY >= 0; PixelY--)
      {
        var CropRow = true;

        var PixelColumn = PixelRow;
        for (var PixelX = 0; PixelX < Width; PixelX++)
        {
          if (PixelArray[PixelColumn + 3] != 0x00)
          {
            CropRow = false;
            break;
          }

          PixelColumn += PixelSize;
        }

        if (!CropRow)
          return PixelY;

        PixelRow -= PixelStride;
      }

      return Height - 1;
    }

    Inv.Pixels Inv.Pixels.Copy() => Copy();
    Inv.Pixels Inv.Pixels.Crop(Inv.Rect Rect) => Crop(Rect);

    public const int PixelSize = 4;
  }

  /// <summary>
  /// Int32 array of ARGB pixels that make up an image.
  /// </summary>
  public sealed class ARGBInt32ArrayPixels : Pixels
  {
    public ARGBInt32ArrayPixels(Inv.Pixels Pixels)
      : this(Pixels.Width, Pixels.Height)
    {
      for (var PixelY = 0; PixelY < Height; PixelY++)
      {
        for (var PixelX = 0; PixelX < Width; PixelX++)
        {
          var PixelIndex = (PixelY * Width) + PixelX;
          var PixelRecord = Pixels[PixelX, PixelY].RawValue;

          PixelArray[PixelIndex] = PixelRecord;
        }
      }
    }
    public ARGBInt32ArrayPixels(int Width, int Height)
      : this(Width, Height, new int[Width * Height])
    {
    }
    public ARGBInt32ArrayPixels(int Width, int Height, int[] PixelArray)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Width * Height == PixelArray.Length, "PixelArray must match the provided width and height.");

      this.Width = Width;
      this.Height = Height;
      this.PixelCount = Width * Height;
      this.PixelArray = PixelArray;
    }

    /// <summary>
    /// Underlying data structure for optimised access.
    /// </summary>
    public int[] PixelArray { get; }
    /// <summary>
    /// Total number of pixels (Width * Height).
    /// </summary>
    public int PixelCount { get; }
    /// <summary>
    /// The width of the grid.
    /// </summary>
    public int Width { get; }
    /// <summary>
    /// The height of the grid.
    /// </summary>
    public int Height { get; }
    /// <summary>
    /// Gets or sets the pixel at the specified zero-based index coordinates.
    /// </summary>
    /// <param name="X">The zero-based index of the X coordinate.</param>
    /// <param name="Y">The zero-based index of the Y coordinate.</param>
    /// <returns>The pixel at the specified coordinates.</returns>
    public Inv.Colour this[int X, int Y]
    {
      [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
      get
      {
        var PixelIndex = (Y * Width) + X;
        var PixelRecord = PixelArray[PixelIndex];

        return Inv.Colour.FromArgb(PixelRecord);
      }
      [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
      set
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.CheckNotNull(value, nameof(value));

        var PixelIndex = (Y * Width) + X;
        var PixelRecord = value?.RawValue ?? 0;

        PixelArray[PixelIndex] = PixelRecord;
      }
    }

    /// <summary>
    /// Populate the grid with a default colour.
    /// </summary>
    public void Fill(Inv.Colour Value)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Value, nameof(Value));

      var PixelRecord = Value?.RawValue ?? 0;

      PixelArray.Fill(PixelRecord);
    }
    /// <summary>
    /// Make a shallow copy of the pixels.
    /// </summary>
    /// <returns></returns>    
    public Inv.ARGBInt32ArrayPixels Copy()
    {
      return new Inv.ARGBInt32ArrayPixels(Width, Height, (int[])PixelArray.Clone());
    }
    /// <summary>
    /// Crop out the pixels to the provided rect.
    /// </summary>
    public Inv.ARGBInt32ArrayPixels Crop(Inv.Rect Rect)
    {
      var Result = new Inv.ARGBInt32ArrayPixels(Rect.Width, Rect.Height);

      var ResultY = 0;
      for (var CropY = Rect.Top; CropY <= Rect.Bottom; CropY++)
      {
        var ResultX = 0;
        for (var CropX = Rect.Left; CropX <= Rect.Right; CropX++)
        {
          var CropIndex = (CropY * this.Width) + CropX;
          var ResultIndex = (ResultY * Result.Width) + ResultX;

          Result.PixelArray[ResultIndex] = this.PixelArray[CropIndex];

          ResultX++;
        }

        ResultY++;
      }

      return Result;
    }
    /// <summary>
    /// Check to see if the reference pixels are an exact match.
    /// </summary>
    /// <param name="Reference"></param>
    /// <returns></returns>
    public bool EqualTo(Inv.ARGBInt32ArrayPixels Reference)
    {
      return PixelArray.ShallowEqualTo(Reference.PixelArray);
    }
    /// <summary>
    /// Find the baseline of the image - the first row, from the bottom, that contains a non-transparent pixel.
    /// </summary>
    /// <returns></returns>
    public int FindBaseline()
    {
      var PixelRow = Height - 1;
      for (var PixelY = Height - 1; PixelY >= 0; PixelY--)
      {
        var CropRow = true;

        var PixelColumn = PixelRow;
        for (var PixelX = 0; PixelX < Width; PixelX++)
        {
          if ((PixelArray[PixelColumn] & 0xFF000000) != 0x00)
          {
            CropRow = false;
            break;
          }

          PixelColumn++;
        }

        if (!CropRow)
          return PixelY;

        PixelRow -= Width;
      }

      return Height - 1;
    }

    Inv.Pixels Inv.Pixels.Copy() => Copy();
    Inv.Pixels Inv.Pixels.Crop(Inv.Rect Rect) => Crop(Rect);
  }
}