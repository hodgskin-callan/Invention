#pragma warning disable 0649
namespace Inv
{
  public static class Resources
  {
    static Resources()
    {
      global::Inv.Resource.Foundation.Import(typeof(Resources), "Resources.InvBuild.InvResourcePackage.rs");
    }

    public static readonly ResourcesImages Images;
    public static readonly ResourcesSounds Sounds;
  }

  public sealed class ResourcesImages
  {
    public ResourcesImages() { }

    ///<Summary>(.png) 192 x 192 (2.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CheckCircleBlack;
    ///<Summary>(.png) 192 x 192 (2.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CheckCircleWhite;
    ///<Summary>(.png) 192 x 192 (3.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NavigateBackWhite;
    ///<Summary>(.png) 192 x 192 (3.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NotInterestedBlack;
    ///<Summary>(.png) 192 x 192 (3.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NotInterestedWhite;
    ///<Summary>(.png) 192 x 192 (2.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ReplayWhite;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference StopWhite;
  }

  public sealed class ResourcesSounds
  {
    public ResourcesSounds() { }

    ///<Summary>(.mp3) ~ (1.9KB)</Summary>
    public readonly global::Inv.Resource.SoundReference Completed;
    ///<Summary>(.mp3) ~ (16.2KB)</Summary>
    public readonly global::Inv.Resource.SoundReference Failed;
  }
}