﻿/* 
https://github.com/jhwcn/BlazorUnmarshalledCanvas
MIT License

Copyright (c) 2019 jhwcn

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
using System;

namespace Inv
{
  /// <summary>
  /// Provides invoking methods to the "2d" context of the HTML5 canvas.
  /// </summary>
  internal class BlazorCanvasBridge : BlazorInteropBridge
  {
    public BlazorCanvasBridge(Inv.BlazorCanvas Canvas)
      : base(Canvas)
    {
      Canvas.Environment.InvokeVoid("c2d.registerContext", Canvas.GetJSHandle()); // marshalled.
    }

    public string FillStyle
    {
      get => InvokeRet<string>("c2d.getFillStyle");
      set => Invoke("c2d.setFillStyle", value);
    }
    public string StrokeStyle
    {
      get => InvokeRet<string>("c2d.getStrokeStyle");
      set => Invoke("c2d.setStrokeStyle", value);
    }
    public string Font
    {
      get => InvokeRet<string>("c2d.getFont");
      set => Invoke("c2d.setFont", value);
    }
    public string TextAlign
    {
      get => InvokeRet<string>("c2d.getTextAlign");
      set => Invoke("c2d.setTextAlign", value);
    }
    public string TextBaseline
    {
      get => InvokeRet<string>("c2d.getTextBaseline");
      set => Invoke("c2d.setTextBaseline", value);
    }
    public float LineWidth
    {
      get
      {
        var str = InvokeRet<string>("c2d.getLineWidth");
        float.TryParse(str, out var ret);
        return ret;
      }
      set => Invoke("c2d.setLineWidth", value);
    }
    public string LineCap
    {
      get => InvokeRet<string>("c2d.getLineCap");
      set => Invoke("c2d.setLineCap", value);
    }
    public string LineJoin
    {
      get => InvokeRet<string>("c2d.getLineJoin");
      set => Invoke("c2d.setLineJoin", value);
    }
    public float MiterLimit
    {
      get
      {
        var str = InvokeRet<string>("c2d.getMiterLimit");
        float.TryParse(str, out var ret);
        return ret;
      }
      set => Invoke("c2d.setMiterLimit", value);
    }
    public float GlobalAlpha
    {
      get
      {
        var str = InvokeRet<string>("c2d.getGlobalAlpha");
        float.TryParse(str, out var ret);
        return ret;
      }
      set => Invoke("c2d.setGlobalAlpha", value);
    }
    public string GlobalCompositeOperation
    {
      get => InvokeRet<string>("c2d.getGlobalCompositeOperation");
      set => Invoke("c2d.setGlobalCompositeOperation", value);
    }
    public bool ImageSmoothingEnabled
    {
      get => InvokeRet<bool>("c2d.getImageSmoothingEnabled");
      set => Invoke("c2d.setImageSmoothingEnabled", value ? 1 : 0);
    }
    public float ShadowBlur
    {
      get
      {
        var str = InvokeRet<string>("c2d.getShadowBlur");
        float.TryParse(str, out var ret);
        return ret;
      }
      set => Invoke("c2d.setShadowBlur", value);
    }
    public string ShadowColor
    {
      get => InvokeRet<string>("c2d.getShadowColor");
      set => Invoke("c2d.setShadowColor", value);
    }
    public float ShadowOffsetX
    {
      get
      {
        var str = InvokeRet<string>("c2d.getShadowOffsetX");
        float.TryParse(str, out var ret);
        return ret;
      }
      set => Invoke("c2d.setShadowOffsetX", value);
    }
    public float ShadowOffsetY
    {
      get
      {
        var str = InvokeRet<string>("c2d.getShadowOffsetY");
        float.TryParse(str, out var ret);
        return ret;
      }
      set => Invoke("c2d.setShadowOffsetY", value);
    }

    public void ClearRect(float x, float y, float width, float height)
    {
      Invoke("c2d.clearRect", x, y, width, height);
    }
    public void FillRect(float x, float y, float width, float height)
    {
      Invoke("c2d.fillRect", x, y, width, height);
    }
    public void StrokeRect(float x, float y, float width, float height)
    {
      Invoke("c2d.strokeRect", x, y, width, height);
    }
    public void FillText(string text, float x, float y, float? maxWidth = null)
    {
      if (null == maxWidth)
        Invoke("c2d.fillText", text, x, y);
      else
        Invoke("c2d.fillText1", text, x, y, maxWidth.Value);
    }
    public void StrokeText(string text, float x, float y, float? maxWidth = null)
    {
      if (null == maxWidth)
        Invoke("c2d.strokeText", text, x, y);
      else
        Invoke("c2d.strokeText1", text, x, y, maxWidth.Value);
    }
    public void FillEllipse(float CenterX, float CenterY, float RadiusX, float RadiusY)
    {
      Invoke("c2d.fillEllipse", CenterX, CenterY, RadiusX, RadiusY);
    }
    public void StrokeEllipse(float CenterX, float CenterY, float RadiusX, float RadiusY)
    {
      Invoke("c2d.strokeEllipse", CenterX, CenterY, RadiusX, RadiusY);
    }
    public void BeginPath()
    {
      Invoke("c2d.beginPath");
    }
    public void ClosePath()
    {
      Invoke("c2d.closePath");
    }
    public bool IsPointInPath(float x, float y, bool evenodd = false)
    {
      return InvokeRet<float, float, int, bool>("c2d.isPointInPath", x, y, evenodd ? 1 : 0);
    }
    public bool IsPointInStroke(float x, float y)
    {
      return InvokeRet<float, float, bool>("c2d.isPointInStroke", x, y);
    }
    public void MoveTo(float x, float y)
    {
      Invoke("c2d.moveTo", x, y);
    }
    public void LineTo(float x, float y)
    {
      Invoke("c2d.lineTo", x, y);
    }
    public void BezierCurveTo(float cp1X, float cp1Y, float cp2X, float cp2Y, float x, float y)
    {
      Invoke("c2d.bezierCurveTo", cp1X, cp1Y, cp2X, cp2Y, x, y);
    }
    public void QuadraticCurveTo(float cpx, float cpy, float x, float y)
    {
      Invoke("c2d.quadraticCurveTo", cpx, cpy, x, y);
    }
    public void Arc(float x, float y, float radius, float startAngle, float endAngle, bool anticlockwise = false)
    {
      Invoke("c2d.arc", x, y, radius, startAngle, endAngle, anticlockwise ? 1 : 0);
    }
    public void Circle(float x, float y, float radius)
    {
      Arc(x, y, radius, 0.0f, (float)(Math.PI * 2));
    }
    public void ArcTo(float x1, float y1, float x2, float y2, float radius)
    {
      Invoke("c2d.arcTo", x1, y1, x2, y2, radius);
    }
    public void Rect(float x, float y, float width, float height)
    {
      Invoke("c2d.rect", x, y, width, height);
    }
    public void Ellipse(float x, float y, float radiusX, float radiusY, float rotation = 0f, float startAngle = 0f, float endAngle = (float)(Math.PI * 2), bool anticlockwise = false)
    {
      Invoke("c2d.ellipse", x, y, radiusX, radiusY, rotation, startAngle, endAngle, anticlockwise ? 1 : 0);
    }
    public void Fill()
    {
      Invoke("c2d.fill");
    }
    public void Stroke()
    {
      Invoke("c2d.stroke");
    }
    public void Clip()
    {
      Invoke("c2d.clip");
    }
    public void Rotate(float angle)
    {
      Invoke("c2d.rotate", angle);
    }
    public void RotateAt(float x, float y, float angle)
    {
      Invoke("c2d.rotateAt", x, y, angle);
    }
    public void Scale(float x, float y)
    {
      Invoke("c2d.scale", x, y);
    }
    public void Translate(float x, float y)
    {
      Invoke("c2d.translate", x, y);
    }
    public void Transform(float m11, float m12, float m21, float m22, float dx, float dy)
    {
      Invoke("c2d.transform", m11, m12, m21, m22, dx, dy);
    }
    public void SetTransform(float m11, float m12, float m21, float m22, float dx, float dy)
    {
      Invoke("c2d.setTransform", m11, m12, m21, m22, dx, dy);
    }
    public void ResetTransform()
    {
      Invoke("c2d.resetTransform");
    }
    public void Save()
    {
      Invoke("c2d.save");
    }
    public void Restore()
    {
      Invoke("c2d.restore");
    }
    public float[] GetLineDash()
    {
      var str = InvokeRet<string>("c2d.getLineDash");
      if (string.IsNullOrEmpty(str))
        return _emptyLineDash;
      var strs = str.Split(',');
      var cnt = 0;
      foreach (var ds in strs)
      {
        if (float.TryParse(ds, out var tmp))
          cnt++;
      }
      if (cnt == 0)
        return _emptyLineDash;
      var ret = new float[cnt];
      cnt = 0;
      foreach (var ds in strs)
      {
        if (float.TryParse(ds, out var tmp))
        {
          ret[cnt] = tmp;
          cnt++;
        }
      }
      return ret;
    }
    public void SetLineDash(float[] segments)
    {
      Invoke("c2d.setLineDash", segments);
    }
    public float MeasureText(string text)
    {
      var ret = InvokeRet<string, string>("c2d.measureText", text);
      float.TryParse(ret, out var width);
      return width;
    }
    public float GetFontHeight(string font)
    {
      var ret = InvokeRet<string, string>("c2d.getFontHeight", font);
      float.TryParse(ret, out var height);
      return height;
    }
    public void DrawImage(float x, float y, float width, float height, BlazorCanvasImage image)
    {
      Invoke("c2d.drawImage", image.Id, x, y, width, height);
    }
    public void DrawCanvas(float x, float y, float width, float height, BlazorCanvas canvas)
    {
      Invoke("c2d.drawCanvas", canvas.Id, x, y, width, height);
    }

    private static readonly float[] _emptyLineDash = Array.Empty<float>();
  }

  internal sealed class BlazorCanvasImage
  {
    internal BlazorCanvasImage()
    {
      this.Id = "_img" + (GlobalID++);
    }

    public string Id { get; }

    private static int GlobalID;
  }
}