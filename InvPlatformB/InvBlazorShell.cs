﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using Microsoft.JSInterop;
using Inv.Support;

namespace Inv
{
  public static class BlazorShell
  {
    public static void Run(Microsoft.JSInterop.IJSInProcessRuntime JSInProcessRuntime, Action<Inv.Application> InvAction)
    {
      Console.WriteLine("INVENTION BLAZOR APPLICATION C#");

      var Environment = new BlazorEnvironment(JSInProcessRuntime);
      try
      {
        Environment.InvokeVoid("console.log", "INVENTION BLAZOR APPLICATION JS");

        var Engine = new BlazorEngine(Environment, InvAction.Method.DeclaringType.Assembly);

        InvAction(Engine.InvApplication);

        Engine.Run();
      }
      catch (Exception Exception)
      {
        Console.WriteLine("APPLICATION FAULT: " + Exception.AsReport());

        Environment.InvokeVoid("messageWindow", Exception.AsReport().Replace(System.Environment.NewLine, "<br/>"), "monospace");
      }
    }

    internal static string BlazorColour(this Inv.Colour Colour)
    {
      if (Colour == null)
        return "";

      var Record = Colour.GetARGBRecord();

      return string.Format("rgba({0}, {1}, {2}, {3})", Record.R, Record.G, Record.B, Record.A / 255.0F);
    }
  }

  internal sealed class BlazorPlatform : Inv.Platform
  {
    public BlazorPlatform(BlazorEngine Engine)
    {
      this.Engine = Engine;
    }

    int Platform.ThreadAffinity()
    {
      return 1;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      var MailUri = string.Format("mailto:{0}?subject={1}&body={2}", EmailMessage.GetTos().Select(T => T.Address).AsSeparatedText(","), Uri.EscapeDataString(EmailMessage.Subject ?? ""), Uri.EscapeDataString(EmailMessage.Body ?? ""));

      Engine.BlazorWindow.Document.SetField("location", MailUri);

      // TODO: attachments are not supported by mailto protocol! I guess we could upload the file to the web server though and embed a link in the email?

      return true;
    }
    bool Platform.PhoneIsSupported => false;
    void Platform.PhoneDial(string PhoneNumber)
    {
      // TODO: iOS requirement? Engine.BlazorWindow.Handle.open("tel:" + PhoneNumber, "_system");

      //Engine.BlazorWindow.Handle.location.href = "tel:" + PhoneNumber;
    }
    void Platform.PhoneSMS(string PhoneNumber)
    {
      //Engine.BlazorWindow.Handle.location.href = "sms:" + PhoneNumber;
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      return Engine.BlazorEnvironment.Invoke<int>("dfs.lengthItem", Engine.SelectFilePath(File));
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      return DateTime.MinValue;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
    }
    System.IO.Stream Platform.DirectoryCreateFile(File File)
    {
      return new BlazorFileStream(Engine, File, LoadItem: false, SaveItem: true);
    }
    System.IO.Stream Platform.DirectoryAppendFile(File File)
    {
      return new BlazorFileStream(Engine, File, LoadItem: true, SaveItem: true);
    }
    System.IO.Stream Platform.DirectoryOpenFile(File File)
    {
      return new BlazorFileStream(Engine, File, LoadItem: true, SaveItem: false);
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      var Path = Engine.SelectFilePath(File);

      return Engine.BlazorEnvironment.Invoke<bool>("dfs.existsItem", Path);
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      var Path = Engine.SelectFilePath(File);

      Engine.BlazorEnvironment.InvokeVoid("dfs.deleteItem", Path);
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      var SourcePath = Engine.SelectFilePath(SourceFile);
      var TargetPath = Engine.SelectFilePath(TargetFile);

      Engine.BlazorEnvironment.InvokeVoid("dfs.copyItem", SourcePath, TargetPath);
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      var SourcePath = Engine.SelectFilePath(SourceFile);
      var TargetPath = Engine.SelectFilePath(TargetFile);

      Engine.BlazorEnvironment.InvokeVoid("dfs.moveItem", SourcePath, TargetPath);
    }
    void Platform.DirectoryReplaceFile(File SourceFile, File TargetFile)
    {
      var SourcePath = Engine.SelectFilePath(SourceFile);
      var TargetPath = Engine.SelectFilePath(TargetFile);

      Engine.BlazorEnvironment.InvokeVoid("dfs.replaceItem", SourcePath, TargetPath);
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      var Path = Engine.SelectFolderPath(Folder) + "/";

      var Result = Engine.BlazorEnvironment.Invoke<string>("dfs.findItems", Path);

      var Regex = new System.Text.RegularExpressions.Regex("^" + System.Text.RegularExpressions.Regex.Escape(FileMask).Replace(@"\*", ".*").Replace(@"\?", ".") + "$");

      foreach (var Name in Result.Split('|'))
      {
        if (Regex.IsMatch(Name))
          yield return Folder.NewFile(Name);
      }
    }
    long Platform.DirectoryGetLengthAsset(Asset Asset)
    {
      return Engine.AssetLength(Asset);
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcAsset(Asset Asset)
    {
      return DateTime.MinValue;
    }
    IEnumerable<Asset> Platform.DirectoryGetAssets(string AssetMask)
    {
      var Regex = new System.Text.RegularExpressions.Regex("^" + System.Text.RegularExpressions.Regex.Escape(AssetMask).Replace(@"\*", ".*").Replace(@"\?", ".") + "$");

      foreach (var Name in Engine.GetAssets())
      {
        if (Regex.IsMatch(Name))
          yield return Engine.InvApplication.Directory.NewAsset(Name);
      }
    }
    System.IO.Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      return Engine.OpenAsset(Asset);
    }
    bool Platform.DirectoryExistsAsset(Asset Asset)
    {
      return Engine.ExistsAsset(Asset);
    }
    bool Platform.LocationIsSupported => false;
    bool Platform.AudioSpeechRecognitionIsSupported => false;
    bool Platform.ClipboardIsTextSupported => false;
    bool Platform.ClipboardIsImageSupported => false;
    void Platform.LocationLookup(LocationResult LocationLookup)
    {
    }
    void Platform.AudioPlaySound(Sound Sound, float Volume, float Rate, float Pan)
    {
      Engine.PlaySound(Sound, Volume, Rate, Pan, Loop: false);
    }
    void Platform.WindowPost(Action Action)
    {
      Engine.Post(Action);
    }
    void Platform.WindowCall(Action Action)
    {
      Engine.Call(Action);
    }
    long Platform.ProcessMemoryUsedBytes()
    {
      return Engine.BlazorWindow.PerformanceMemoryBytes();
    }
    void Platform.WebClientConnect(WebClient WebClient)
    {
      // TODO: WebSockets work.
      //var WebSocket = new System.Net.WebSockets.ClientWebSocket();
      //WebSocket.ConnectAsync(new Uri(WebClient.Host + ":" + WebClient.Port), new System.Threading.CancellationToken()).Wait();
      //WebClient.Node = WebSocket;
      //WebClient.SetStreams(WebSocket.Stream, WebSocket.Stream);
    }
    void Platform.WebClientDisconnect(WebClient WebClient)
    {
    }
    void Platform.WebServerConnect(WebServer WebServer)
    {
    }
    void Platform.WebServerDisconnect(WebServer WebServer)
    {
    }
    void Platform.WebBroadcastConnect(WebBroadcast WebBroadcast)
    {
    }
    void Platform.WebBroadcastDisconnect(WebBroadcast WebBroadcast)
    {
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      // launch in a new tab.
      Engine.BlazorWindow.InvokeVoid("open", Uri.AbsoluteUri, "_blank");
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
    }
    string Platform.CalendarTimeZoneName()
    {
      return null;
    }
    IEnumerable<Folder> Platform.DirectoryGetFolderSubfolders(Folder Folder, string FolderMask)
    {
      var Path = Engine.SelectFolderPath(Folder) + "/";

      var Result = Engine.BlazorEnvironment.Invoke<string>("dfs.findContainers", Path);

      var Regex = new System.Text.RegularExpressions.Regex("^" + System.Text.RegularExpressions.Regex.Escape(FolderMask).Replace(@"\*", ".*").Replace(@"\?", ".") + "$");

      foreach (var Name in Result.Split('|').Distinct())
      {
        if (Regex.IsMatch(Name))
          yield return Folder.NewFolder(Name);
      }
    }
    void Platform.DirectoryShowFilePicker(DirectoryFilePicker FilePicker)
    {
      Engine.ShowFilePicker(FilePicker);
    }
    string Platform.DirectoryGetFolderPath(Folder Folder)
    {
      return Engine.SelectFolderPath(Folder);
    }
    string Platform.DirectoryGetFilePath(File File)
    {
      return Engine.SelectFilePath(File);
    }
    void Platform.LocationShowMap(string Location)
    {
    }
    TimeSpan Platform.AudioGetSoundLength(Sound Sound)
    {
      return TimeSpan.Zero;
    }
    void Platform.AudioReclaim(IReadOnlyList<Sound> SoundList)
    {
    }
    void Platform.AudioPlayClip(AudioClip Clip)
    {
      var BlazorClip = new BlazorClip();
      Clip.Node = BlazorClip;
      
      Engine.PlaySound(Clip.Sound, Clip.Volume, Clip.Rate, Clip.Pan, Clip.Looped, BlazorClip);
    }
    void Platform.AudioPauseClip(AudioClip Clip)
    {
      var BlazorClip = Clip.Node as BlazorClip;
      if (BlazorClip != null)
        Engine.PauseSound(BlazorClip);
    }
    void Platform.AudioResumeClip(AudioClip Clip)
    {
      var BlazorClip = Clip.Node as BlazorClip;
      if (BlazorClip != null)
        Engine.ResumeSound(BlazorClip);
    }
    void Platform.AudioStopClip(AudioClip Clip)
    {
      var BlazorClip = Clip.Node as BlazorClip;
      if (BlazorClip != null)
      {
        Clip.Node = null;
        Engine.StopSound(BlazorClip);
      }
    }
    void Platform.AudioModulateClip(AudioClip Clip)
    {
      var BlazorClip = Clip.Node as BlazorClip;
      if (BlazorClip != null)
        Engine.ModulateSound(BlazorClip, Clip.Volume, Clip.Rate, Clip.Pan, Clip.Looped);
    }
    void Platform.AudioStartSpeechRecognition(SpeechRecognition SpeechRecognition)
    {
    }
    void Platform.AudioStopSpeechRecognition(SpeechRecognition SpeechRecognition)
    {
    }
    void Platform.WindowStartAnimation(Animation Animation)
    {
      // TODO: animations are not implemented, but we've made them complete at the end of the time. 
      if (Animation.IsActive)
      {
        if (Animation.Targets.Count > 0)
        {
          var AllFinishedTime = Animation.Targets.Max(T => T.Transforms.Max(R => (R.Offset ?? TimeSpan.Zero) + R.Duration));

          var CancellationSource = new System.Threading.CancellationTokenSource();

          Animation.Node = CancellationSource;
          
          System.Threading.Tasks.Task.Delay(AllFinishedTime, CancellationSource.Token).ContinueWith(Task => Engine.Post(() =>
          {
            //CancellationSource.Dispose(); // TODO: is this required?
            Animation.Node = null;

            if (Animation.IsActive)
              Animation.Complete();
          }), CancellationSource.Token);
        }
        else
        {
          Animation.Complete();
        }
      }
    }
    void Platform.WindowStopAnimation(Animation Animation)
    {
      var CancellationToken = Animation.Node as System.Threading.CancellationTokenSource;
      if (CancellationToken != null)
      {
        Animation.Node = null;
        CancellationToken.Cancel();
      }
    }
    void Platform.WindowShowPopup(Inv.Popup Popup)
    {
      Engine.ShowPopup(Popup);
    }
    void Platform.WindowHidePopup(Inv.Popup Popup)
    {
      Engine.HidePopup(Popup);
    }
    void Platform.WindowBrowse(File File)
    {
      // TODO: launch a new tab with the File content?
    }
    void Platform.WindowShare(File File)
    {
      Engine.BlazorEnvironment.InvokeVoid("downloadFile", File.Name, Engine.SelectFilePath(File));
    }
    Dimension Platform.WindowGetDimension(Panel Panel)
    {
      var Result = Engine.GetPanel(Panel)?.GetDimension() ?? Inv.Dimension.Zero;

      // clamp to above zero.
      if (Result.Width < 0 && Result.Height < 0)
        Result = Inv.Dimension.Zero;
      else if (Result.Width < 0)
        Result = new Inv.Dimension(0, Result.Height);
      else if (Result.Height < 0)
        Result = new Inv.Dimension(Result.Width, 0 );

      return Result;
    }
    string Platform.ProcessAncillaryInformation()
    {
      return "";
    }
    void Platform.WebInstallUri(Uri Uri)
    {
    }
    void Platform.VaultLoadSecret(Secret Secret)
    {
    }
    void Platform.VaultSaveSecret(Secret Secret)
    {
    }
    void Platform.VaultDeleteSecret(Secret Secret)
    {
    }
    string Platform.ClipboardGetText()
    {
      return "";
    }
    void Platform.ClipboardSetText(string Text)
    {
    }
    Image Platform.ClipboardGetImage()
    {
      return null;
    }
    void Platform.ClipboardSetImage(Image Image)
    {
    }
    void Platform.HapticFeedback(HapticFeedback Feedback)
    {
    }
    Dimension Platform.GraphicsGetDimension(Image Image)
    {
      return Dimension.Zero;
    }
    Image Platform.GraphicsGrayscale(Image Image)
    {
      var BlazorImage = Engine.TranslateCanvasImage(Image);

      var Result = Engine.BlazorEnvironment.Invoke<string>("gfx.grayImage", BlazorImage.Id);

      return Engine.ConvertFromImageSource(Result);
    }
    Image Platform.GraphicsTint(Image Image, Colour Colour)
    {
      // TODO: tint.
      return Image;
    }
    Image Platform.GraphicsResize(Image Image, Dimension Dimension)
    {
      return Image;
    }
    Pixels Platform.GraphicsReadPixels(Image Image)
    {
      //var BlazorImage = Engine.TranslateCanvasImage(Image);

      //var Result = Engine.BlazorEnvironment.Invoke<byte[]>("gfx.readImagePixels", BlazorImage.Id);

      return new Inv.RGBAByteArrayPixels(0, 0);
    }
    Image Platform.GraphicsWritePixels(Pixels Pixels)
    {
      //var Result = Engine.BlazorEnvironment.Invoke<string>("gfx.writeImagePixels", BlazorImage.Id);

      //Console.WriteLine(Result);

      //return Engine.ConvertFromImageSource(Result) ?? Image;
      return null;
    }
    void Platform.GraphicsRender(Panel Panel, Action<Image> ReturnAction)
    {
      ReturnAction?.Invoke(Inv.Image.Empty(".png"));
    }
    void Platform.GraphicsReclaim(IReadOnlyList<Image> ImageList)
    {
      foreach (var Image in ImageList)
      {
        var BlazorImage = Image.Node as BlazorCanvasImage;

        if (BlazorImage != null)
        {
          Image.Node = null;

          Engine.BlazorEnvironment.InvokeVoid("gfx.releaseImage", BlazorImage.Id);
        }
      }
    }
    Dimension Platform.CanvasCalculateText(string TextFragment, DrawFont TextFont, int MaximumTextWidth, int MaximumTextHeight)
    {
      return Engine.BlazorRender.MeasureText(TextFragment, TextFont, MaximumTextWidth, MaximumTextHeight);
    }

    private readonly BlazorEngine Engine;
  }

  internal sealed class BlazorFileStream : System.IO.Stream
  {
    internal BlazorFileStream(BlazorEngine Engine, Inv.File File, bool LoadItem, bool SaveItem)
    {
      this.Engine = Engine;
      this.File = File;
      this.LoadItem = LoadItem;
      this.SaveItem = SaveItem;

      if (LoadItem)
      {
        var Key = Engine.SelectFilePath(File);
        var Value = Engine.BlazorEnvironment.Invoke<string>("dfs.getItem", Key);

        this.MemoryStream = new System.IO.MemoryStream();
        if (Value != null)
          MemoryStream.Write(Convert.FromBase64String(Value)); // NOTE: the other MemoryStream constructors, that take a byte array, prevent the stream from being expandable.

        MemoryStream.Position = SaveItem ? MemoryStream.Length : 0;
      }
      else
      {
        this.MemoryStream = new System.IO.MemoryStream();
      }
    }

    protected override void Dispose(bool disposing)
    {
      base.Dispose(disposing);

      if (MemoryStream != null)
      {
        if (SaveItem)
        {
          var Key = Engine.SelectFilePath(File);
          var Value = Convert.ToBase64String(MemoryStream.ToArray());

          Engine.BlazorEnvironment.InvokeVoid("dfs.setItem", Key, Value);
        }

        MemoryStream.Dispose();
        this.MemoryStream = null;
      }
    }

    public override bool CanRead => LoadItem;
    public override bool CanSeek => MemoryStream.CanSeek;
    public override bool CanWrite => SaveItem;
    public override long Length => MemoryStream.Length;
    public override long Position 
    {
      get => MemoryStream.Position;
      set => MemoryStream.Position = value;
    }

    public override void Flush()
    {
      MemoryStream.Flush();
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      return MemoryStream.Read(buffer, offset, count);
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      return MemoryStream.Seek(offset, origin);
    }
    public override void SetLength(long value)
    {
      MemoryStream.SetLength(Length);
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      MemoryStream.Write(buffer, offset, count);
    }

    private readonly BlazorEngine Engine;
    private readonly Inv.File File;
    private readonly bool LoadItem;
    private readonly bool SaveItem;
    private System.IO.MemoryStream MemoryStream;
  }

  internal static class BlazorKeyboard
  {
    static BlazorKeyboard()
    {
      KeyDictionary = new Dictionary<string, Inv.Key>()
      {
        { "Delete", Inv.Key.Delete },
        { "Backspace", Inv.Key.Backspace },
        { "Tab", Inv.Key.Tab },
        { "Enter", Inv.Key.Enter },
        { "Escape", Inv.Key.Escape },
        { " ", Inv.Key.Space },
        { "0", Inv.Key.n0 },
        { "1", Inv.Key.n1 },
        { "2", Inv.Key.n2 },
        { "3", Inv.Key.n3 },
        { "4", Inv.Key.n4 },
        { "5", Inv.Key.n5 },
        { "6", Inv.Key.n6 },
        { "7", Inv.Key.n7 },
        { "8", Inv.Key.n8 },
        { "9", Inv.Key.n9 },
        { ")", Inv.Key.n0 },
        { "!", Inv.Key.n1 },
        { "@", Inv.Key.n2 },
        { "#", Inv.Key.n3 },
        { "$", Inv.Key.n4 },
        { "%", Inv.Key.n5 },
        { "^", Inv.Key.n6 },
        { "&", Inv.Key.n7 },
        //{ "*", Inv.Key.n8 },
        { "(", Inv.Key.n9 },
        { "a", Inv.Key.A },
        { "b", Inv.Key.B },
        { "c", Inv.Key.C },
        { "d", Inv.Key.D },
        { "e", Inv.Key.E },
        { "f", Inv.Key.F },
        { "g", Inv.Key.G },
        { "h", Inv.Key.H },
        { "i", Inv.Key.I },
        { "j", Inv.Key.J },
        { "k", Inv.Key.K },
        { "l", Inv.Key.L },
        { "m", Inv.Key.M },
        { "n", Inv.Key.N },
        { "o", Inv.Key.O },
        { "p", Inv.Key.P },
        { "q", Inv.Key.Q },
        { "r", Inv.Key.R },
        { "s", Inv.Key.S },
        { "t", Inv.Key.T },
        { "u", Inv.Key.U },
        { "v", Inv.Key.V },
        { "w", Inv.Key.W },
        { "x", Inv.Key.X },
        { "y", Inv.Key.Y },
        { "z", Inv.Key.Z },
        { "A", Inv.Key.A },
        { "B", Inv.Key.B },
        { "C", Inv.Key.C },
        { "D", Inv.Key.D },
        { "E", Inv.Key.E },
        { "F", Inv.Key.F },
        { "G", Inv.Key.G },
        { "H", Inv.Key.H },
        { "I", Inv.Key.I },
        { "J", Inv.Key.J },
        { "K", Inv.Key.K },
        { "L", Inv.Key.L },
        { "M", Inv.Key.M },
        { "N", Inv.Key.N },
        { "O", Inv.Key.O },
        { "P", Inv.Key.P },
        { "Q", Inv.Key.Q },
        { "R", Inv.Key.R },
        { "S", Inv.Key.S },
        { "T", Inv.Key.T },
        { "U", Inv.Key.U },
        { "V", Inv.Key.V },
        { "W", Inv.Key.W },
        { "X", Inv.Key.X },
        { "Y", Inv.Key.Y },
        { "Z", Inv.Key.Z },
        { "Insert", Inv.Key.Insert },
        { "End", Inv.Key.End },
        { "ArrowDown", Inv.Key.Down },
        { "PageDown", Inv.Key.PageDown },
        { "ArrowLeft", Inv.Key.Left },
        { "Clear", Inv.Key.Clear },
        { "ArrowRight", Inv.Key.Right },
        { "Home", Inv.Key.Home },
        { "ArrowUp", Inv.Key.Up },
        { "PageUp", Inv.Key.PageUp },
        { "*", Inv.Key.Asterisk },
        { "+", Inv.Key.Plus },
        { "-", Inv.Key.Minus },
        { ".", Inv.Key.Period },
        { ">", Inv.Key.Period }, // shift + .
        { ",", Inv.Key.Comma },
        { "<", Inv.Key.Comma }, // shift + ,
        { "/", Inv.Key.Slash },
        { "?", Inv.Key.Slash }, // shift + /
        { "[", Inv.Key.OpenBracket },
        { "{", Inv.Key.OpenBracket },
        { "]", Inv.Key.CloseBracket },
        { "}", Inv.Key.CloseBracket },
        { "F1", Inv.Key.F1 },
        { "F2", Inv.Key.F2 },
        { "F3", Inv.Key.F3 },
        { "F4", Inv.Key.F4 },
        { "F5", Inv.Key.F5 },
        { "F6", Inv.Key.F6 },
        { "F7", Inv.Key.F7 },
        { "F8", Inv.Key.F8 },
        { "F9", Inv.Key.F9 },
        { "F10", Inv.Key.F10 },
        { "F11", Inv.Key.F11 },
        { "F12", Inv.Key.F12 },
        { "\\", Inv.Key.Backslash },
        { "|", Inv.Key.Backslash },
        { ":", Inv.Key.Colon },
        { ";", Inv.Key.Colon },
        { "~", Inv.Key.Grave },
        { "`", Inv.Key.Grave },
        { "'", Inv.Key.Quote },
        { "\"", Inv.Key.Quote },
        //{ "CapsLock", Inv.Key.CapsLock },
      };
#if DEBUG
      // NOTE: Left/Right function keys are handled in the TranslateKey method.
      var MissingKeySet = Inv.Support.EnumHelper.GetEnumerable<Inv.Key>().Except(KeyDictionary.Values).Except(new[] { Inv.Key.LeftAlt, Inv.Key.RightAlt, Inv.Key.LeftCtrl, Inv.Key.RightCtrl, Inv.Key.LeftShift, Inv.Key.RightShift }).ToHashSet();
      if (MissingKeySet.Count > 0)
        System.Diagnostics.Debug.WriteLine("Unhandled keys: " + MissingKeySet.Select(K => K.ToString()).AsSeparatedText(", "));
#endif
    }

    public static Inv.Key? TranslateKey(BlazorKeystroke BlazorKeystroke)
    {
      if (BlazorKeystroke.Key == null)
        return null;

      //const int DOM_KEY_LOCATION_STANDARD = 0x00;
      const int DOM_KEY_LOCATION_LEFT = 0x01;
      const int DOM_KEY_LOCATION_RIGHT = 0x02;
      //const int DOM_KEY_LOCATION_NUMPAD = 0x03;

      if (!KeyDictionary.TryGetValue(BlazorKeystroke.Key, out var InvKey))
      {
        if (BlazorKeystroke.Key == "Control" && BlazorKeystroke.Location == DOM_KEY_LOCATION_LEFT)
          InvKey = Key.LeftCtrl;
        else if (BlazorKeystroke.Key == "Control" && BlazorKeystroke.Location == DOM_KEY_LOCATION_RIGHT)
          InvKey = Key.RightCtrl;
        else if (BlazorKeystroke.Key == "Alt" && BlazorKeystroke.Location == DOM_KEY_LOCATION_LEFT)
          InvKey = Key.LeftAlt;
        else if (BlazorKeystroke.Key == "Alt" && BlazorKeystroke.Location == DOM_KEY_LOCATION_RIGHT)
          InvKey = Key.RightAlt;
        else if (BlazorKeystroke.Key == "Shift" && BlazorKeystroke.Location == DOM_KEY_LOCATION_LEFT)
          InvKey = Key.LeftShift;
        else if (BlazorKeystroke.Key == "Shift" && BlazorKeystroke.Location == DOM_KEY_LOCATION_RIGHT)
          InvKey = Key.RightShift;
        else
          return null;
      }

      return InvKey;
    }

    private static readonly Dictionary<string, Inv.Key> KeyDictionary;
  }

  internal sealed class BlazorEngine
  {
    public BlazorEngine(BlazorEnvironment BlazorEnvironment, System.Reflection.Assembly AppAssembly)
    {
      this.BlazorEnvironment = BlazorEnvironment;
      this.AppAssembly = AppAssembly;
      this.AppAssemblyName = AppAssembly.GetName().Name;
      this.AppManifestAssetSet = AppAssembly.GetManifestResourceNames().ToHashSet();

      this.InvApplication = new Inv.Application();
      InvApplication.SetPlatform(new BlazorPlatform(this));

      this.PostList = new List<Action>();

      this.LastKeyModifier = new Inv.KeyModifier();

      this.FontWeightArray = new Inv.EnumArray<Inv.FontWeight, string>()
      {
        { FontWeight.Thin, "100" },       // 100
        { FontWeight.Light, "300" },      // 200/300?
        { FontWeight.Regular, "normal" }, // 400
        { FontWeight.Medium, "600" },     // 500/600?
        { FontWeight.Bold, "bold" },      // 700
        { FontWeight.Heavy, "900" },      // 800/900?
      };

      this.RouteArray = new Inv.EnumArray<ControlType, Func<Inv.Control, BlazorNode>>()
      {
        { Inv.ControlType.Block, P => TranslateBlock((Inv.Block)P) },
        { Inv.ControlType.Board, P => TranslateBoard((Inv.Board)P) },
        { Inv.ControlType.Browser, P => TranslateBrowser((Inv.Browser)P) },
        { Inv.ControlType.Button, P => TranslateButton((Inv.Button)P) },
        { Inv.ControlType.Dock, P => TranslateDock((Inv.Dock)P) },
        { Inv.ControlType.Edit, P => TranslateEdit((Inv.Edit)P) },
        { Inv.ControlType.Flow, P => TranslateFlow((Inv.Flow)P) },
        { Inv.ControlType.Frame, P => TranslateFrame((Inv.Frame)P) },
        { Inv.ControlType.Graphic, P => TranslateGraphic((Inv.Graphic)P) },
        { Inv.ControlType.Label, P => TranslateLabel((Inv.Label)P) },
        { Inv.ControlType.Memo, P => TranslateMemo((Inv.Memo)P) },
        { Inv.ControlType.Native, P => TranslateNative((Inv.Native)P) },
        { Inv.ControlType.Overlay, P => TranslateOverlay((Inv.Overlay)P) },
        { Inv.ControlType.Canvas, P => TranslateCanvas((Inv.Canvas)P) },
        { Inv.ControlType.Scroll, P => TranslateScroll((Inv.Scroll)P) },
        { Inv.ControlType.Shape, P => TranslateShape((Inv.Shape)P) },
        { Inv.ControlType.Stack, P => TranslateStack((Inv.Stack)P) },
        { Inv.ControlType.Switch, P => TranslateSwitch((Inv.Switch)P) },
        { Inv.ControlType.Table, P => TranslateTable((Inv.Table)P) },
        { Inv.ControlType.Video, P => TranslateVideo((Inv.Video)P) },
        { Inv.ControlType.Wrap, P => TranslateWrap((Inv.Wrap)P) },
      };

      this.BlazorWindow = new BlazorWindow(this.BlazorEnvironment);
      BlazorWindow.ResizeEvent += () => Arrange();
      BlazorWindow.CloseEvent += () => Close(); // TODO: close query?
      BlazorWindow.MouseBackwardEvent += () => Guard(() => InvApplication.Window.ActiveSurface?.GestureBackwardInvoke());
      BlazorWindow.MouseForwardEvent += () => Guard(() => InvApplication.Window.ActiveSurface?.GestureForwardInvoke());

      void UpdateKeyModifier(Inv.Key? InvKey, BlazorKeystroke BlazorKeystroke, bool Toggle)
      {
        switch (InvKey)
        {
          case Inv.Key.LeftAlt: LastKeyModifier.IsLeftAlt = Toggle; break;
          case Inv.Key.RightAlt: LastKeyModifier.IsRightAlt = Toggle; break;
          case Inv.Key.LeftCtrl: LastKeyModifier.IsLeftCtrl = Toggle; break;
          case Inv.Key.RightCtrl: LastKeyModifier.IsRightCtrl = Toggle; break;
          case Inv.Key.LeftShift: LastKeyModifier.IsLeftShift = Toggle; break;
          case Inv.Key.RightShift: LastKeyModifier.IsRightShift = Toggle; break;
        }

        if (!BlazorKeystroke.IsAlt)
          LastKeyModifier.IsAlt = false;
        
        if (!BlazorKeystroke.IsCtrl)
          LastKeyModifier.IsCtrl = false;
        
        if (!BlazorKeystroke.IsShift)
          LastKeyModifier.IsShift = false;

        InvApplication.Keyboard.CheckModifier(LastKeyModifier);
      }

      BlazorWindow.KeyDownEvent += (BlazorKeystroke) =>
      {
        var InvKey = BlazorKeyboard.TranslateKey(BlazorKeystroke);

        UpdateKeyModifier(InvKey, BlazorKeystroke, Toggle: true);

        if (InvKey != null)
        {
          var InvActiveSurface = InvApplication.Window.ActiveSurface;

          if (InvActiveSurface != null)
            InvApplication.Keyboard.KeyPress(new Inv.Keystroke(InvKey.Value, LastKeyModifier.Clone()));
        }
      };
      BlazorWindow.KeyUpEvent += (BlazorKeystroke) =>
      {
        var InvKey = BlazorKeyboard.TranslateKey(BlazorKeystroke);

        UpdateKeyModifier(InvKey, BlazorKeystroke, Toggle: false);
      };
      BlazorWindow.KeyModifierEvent += (BlazorKeystroke) =>
      {
        UpdateKeyModifier(InvKey: null, BlazorKeystroke, Toggle: false);
      };
      
      var MainDiv = BlazorEnvironment.Invoke<Microsoft.JSInterop.IJSInProcessObjectReference>("getMainDiv");

      this.BlazorMaster = new BlazorDiv(BlazorWindow.Document);
      BlazorMaster.Class = "master";
      MainDiv.InvokeVoid("appendChild", BlazorMaster.GetJSHandle());

      this.BlazorFileInput = new BlazorInput(BlazorWindow.Document);
      BlazorFileInput.Type = "file";
      BlazorFileInput.Style.Opacity = "0"; // transparent
      BlazorFileInput.Style.ZIndex = "-1"; // under everything else
      BlazorFileInput.Style.Position = "absolute"; // don't take up any space.
      BlazorFileInput.InputFileEvent += (Name, Source) =>
      {
        if (ActiveFilePicker != null)
        {
          if (Source == null)
            ActiveFilePicker.CancelInvoke();
          else
            ActiveFilePicker.SelectInvoke(new Pick(Name, () => new System.IO.MemoryStream(ConvertFromImageSource(Source).GetBuffer()))); // TODO: converting a data url to a byte array doesn't need the Inv.Image.

          this.ActiveFilePicker = null;
        }
      };
      BlazorMaster.InvokeVoid("appendChild", BlazorFileInput.GetJSHandle());

      this.BlazorRender = new BlazorRender(this);

      // TODO: javascript for browser information?
      InvApplication.Version = AppAssembly.GetName().Version.ToString();

      InvApplication.Device.Target = Inv.DeviceTarget.BlazorWebAssembly;
      InvApplication.Device.Name = "";
      InvApplication.Device.Model = "";
      InvApplication.Device.System = "";
      InvApplication.Device.Keyboard = true;
      InvApplication.Device.Mouse = true;
      InvApplication.Device.Touch = true;
      InvApplication.Device.ProportionalFontName = "sans-serif"; // sans-serif?
      InvApplication.Device.MonospacedFontName = "monospace"; // monospace?

      Resize();
    }

    public void Run()
    {
      try
      {
        InvApplication.StartInvoke();
      }
      catch (Exception Exception)
      {
        HandleException(Exception);

        // NOTE: can't let the application start if the StartEvent failed so have the application crash instead.
        throw Exception.Preserve();
      }

      // set the document title (expected the title to be set in the Start event).
      if (InvApplication.Title != BlazorWindow.Document.Title)
        BlazorWindow.Document.Title = InvApplication.Title;

      Guard(() => Process());

      BlazorEnvironment.InvokeVoid("initApplication", Microsoft.JSInterop.DotNetObjectReference.Create(this));
    }

    internal BlazorEnvironment BlazorEnvironment { get; }
    internal Application InvApplication { get; }
    internal BlazorWindow BlazorWindow { get; }
    internal BlazorRender BlazorRender { get; }

    internal BlazorElement GetPanel(Inv.Panel Panel)
    {
      return RoutePanel(Panel);
    }
    internal void Post(Action Action)
    {
      lock (PostList)
        PostList.Add(Action);
    }
    internal void Call(Action Action)
    {
      // TODO: how do we marshal into the JS loop and block until it's executed?
      Action();
    }
    internal void Guard(Action Action)
    {
      try
      {
        Action();
      }
      catch (Exception Exception)
      {
        HandleException(Exception);
      }
    }
    internal void HandleException(Exception Exception)
    {
      Console.WriteLine(Exception.AsReport());

      InvApplication.HandleExceptionInvoke(Exception);
    }
    internal int HorizontalPtToPx(int Points)
    {
      // TODO: convert Inv Points to Html Px?
      return Points;
    }
    internal int VerticalPtToPx(int Points)
    {
      // TODO: convert Inv Points to Html Px?
      return Points;
    }
    internal int HorizontalPxToPt(int Pixels)
    {
      // TODO: convert Html Px to Inv Points?
      return Pixels;
    }
    internal int VerticalPxToPt(int Pixels)
    {
      // TODO: convert Html Px to Inv Points?
      return Pixels;
    }
    internal string TranslateLineCap(Inv.LineCap InvLineCap)
    {
      return InvLineCap switch
      {
        Inv.LineCap.Butt => "butt",
        Inv.LineCap.Round => "round",
        Inv.LineCap.Square => "square",
        _ => throw Inv.Support.EnumHelper.UnexpectedValueException(InvLineCap),
      };
    }
    internal string TranslateLineJoin(Inv.LineJoin InvLineJoin)
    {
      return InvLineJoin switch
      {
        Inv.LineJoin.Bevel => "bevel",
        Inv.LineJoin.Miter => "miter",
        Inv.LineJoin.Round => "round",
        _ => throw Inv.Support.EnumHelper.UnexpectedValueException(InvLineJoin),
      };
    }
    internal string TranslateFontAxis(Inv.FontAxis InvFontAxis)
    {
      return InvFontAxis switch
      {
        Inv.FontAxis.Superscript => "super",
        Inv.FontAxis.Subscript => "sub",
        Inv.FontAxis.Baseline => "baseline",
        Inv.FontAxis.Topline => "text-top",
        Inv.FontAxis.Bottomline => "text-bottom",
        _ => throw Inv.Support.EnumHelper.UnexpectedValueException(InvFontAxis),
      };
    }
    internal string TranslateFontWeight(Inv.FontWeight InvFontWeight)
    {
      return FontWeightArray[InvFontWeight];
    }

    internal IEnumerable<string> GetAssets()
    {
      var AssetPrefix = $"{AppAssemblyName}.Assets.";

      return AppAssembly.GetManifestResourceNames().Where(N => N.StartsWith(AssetPrefix)).Select(N => N.Substring(AssetPrefix.Length));
    }
    internal long AssetLength(Asset Asset)
    {
      using (var Stream = AppAssembly.GetManifestResourceStream(SelectAssetPath(Asset)))
        return Stream.Length;
    }
    internal System.IO.Stream OpenAsset(Asset Asset)
    {
      return AppAssembly.GetManifestResourceStream(SelectAssetPath(Asset));
    }
    internal bool ExistsAsset(Asset Asset)
    {
      return AppManifestAssetSet.Contains(SelectAssetPath(Asset));
    }
    internal Inv.Image ConvertFromImageSource(string Source)
    {
      const string Prefix = "data:image/";
      if (Source != null && Source.StartsWith(Prefix))
      {
        var Semicolon = Source.IndexOf(";", Prefix.Length, StringComparison.InvariantCultureIgnoreCase);
        if (Semicolon >= 0)
        {
          var Format = Source.Substring(Prefix.Length, Semicolon - Prefix.Length);

          var Comma = Source.IndexOf(",", Semicolon, StringComparison.InvariantCultureIgnoreCase);

          if (Comma > 0)
            return new Inv.Image(Convert.FromBase64String(Source.Substring(Comma + 1)), "." + Format);
        }
      }

      return null;
    }
    internal BlazorCanvasImage TranslateCanvasImage(Inv.Image ImageSource)
    {
      var Result = ImageSource.Node as BlazorCanvasImage;

      if (Result == null)
      {
        Result = new BlazorCanvasImage();

        var ImageBase64 = TranslateImageSource(ImageSource);

        BlazorEnvironment.InvokeVoid("gfx.registerImage", Result.Id, ImageBase64);

        ImageSource.Node = Result;
      }

      return Result;
    }
    internal void PlaySound(Inv.Sound Sound, float Volume, float Rate, float Pan, bool Loop, BlazorClip BlazorClip = null)
    {
      var BlazorSound = Sound.Node as BlazorSound;
      if (BlazorSound == null)
      {
        BlazorSound = new BlazorSound(BlazorEnvironment.Invoke<string>("sfx.newSound", Sound.GetBuffer()));
        Sound.Node = BlazorSound;
      }

      BlazorEnvironment.InvokeVoid("sfx.playSound", BlazorSound.Id, Volume, Rate, Pan, Loop, BlazorClip?.Id);
    }
    internal void StopSound(BlazorClip Clip)
    {
      BlazorEnvironment.InvokeVoid("sfx.stopSound", Clip.Id);
    }
    internal void ModulateSound(BlazorClip Clip, float Volume, float Rate, float Pan, bool Loop)
    {
      BlazorEnvironment.InvokeVoid("sfx.modulateSound", Clip.Id, Volume, Rate, Pan, Loop);
    }
    internal void PauseSound(BlazorClip Clip)
    {
      BlazorEnvironment.InvokeVoid("sfx.pauseSound", Clip.Id);
    }
    internal void ResumeSound(BlazorClip Clip)
    {
      BlazorEnvironment.InvokeVoid("sfx.resumeSound", Clip.Id);
    }
    internal string SelectFilePath(Inv.File File)
    {
      return System.IO.Path.Combine(SelectFolderPath(File.Folder) + "/", File.Name);
    }
    internal string SelectFolderPath(Inv.Folder Folder)
    {
      return Folder.GetRelativePath().AsSeparatedText("/");
    }
    internal void ShowFilePicker(DirectoryFilePicker FilePicker)
    {
      // keep the last reference so we can use it when a file is loaded.
      this.ActiveFilePicker = FilePicker;

      BlazorFileInput.Accept = FilePicker.PickType switch
      {
        PickType.Any => null,
        PickType.Image => "image/jpeg, image/png, image/jpg",
        PickType.Sound => "audio/mp3",
        _ => throw Inv.Support.EnumHelper.UnexpectedValueException(FilePicker.PickType),
      };

      // programmatically invoke the file picker dialog.
      BlazorEnvironment.InvokeVoid("triggerClick", BlazorFileInput.GetJSHandle());
    }
    internal void ShowPopup(Inv.Popup InvPopup)
    {
      throw new NotImplementedException();
    }
    internal void HidePopup(Inv.Popup InvPopup)
    {
      throw new NotImplementedException();
    }

    private void Arrange()
    {
      Guard(() =>
      {
        if (Resize())
        {
          var InvActiveSurface = InvApplication.Window.ActiveSurface;

          if (InvActiveSurface != null)
            InvActiveSurface.ArrangeInvoke();
        }
      });
    }
    private bool Resize()
    {
      var NewWidth = BlazorWindow.Width; // (int)(BlazorWindow.Width / 96.0F * 160.0F);
      var NewHeight = BlazorWindow.Height; // (int)(BlazorWindow.Height / 96.0F * 160.0F);

      if (InvApplication.Window.Width == NewWidth && InvApplication.Window.Height == NewHeight)
      {
        // window size hasn't actually changed.
        return false;
      }
      else
      {
        // NOTE: this helps to detect if we need to throttle the window resize event.
        Console.WriteLine($"RESIZE: {NewWidth} x {NewHeight}");

        InvApplication.Window.Width = NewWidth;
        InvApplication.Window.Height = NewHeight;

        return true;
      }
    }
    private void Close()
    {
      BlazorEnvironment.InvokeVoid("stopApplication", Microsoft.JSInterop.DotNetObjectReference.Create(this));

      Guard(() => InvApplication.StopInvoke());
    }
    private void Process()
    {
      // deferred actions.
      Action[] PostArray;
      lock (PostList)
      {
        PostArray = PostList.ToArray();
        PostList.Clear();
      }

      foreach (var Post in PostArray)
        Guard(() => Post());

      if (InvApplication.IsExit)
      {
        Close();

        BlazorWindow.Close();
      }
      else
      {
        var InvWindow = InvApplication.Window;
        var InvKeyboard = InvApplication.Keyboard;

        if (InvWindow.ActiveTimerSet.Count > 0)
        {
          foreach (var InvTimer in InvWindow.ActiveTimerSet.ToArray())
          {
            var BlazorTimer = AccessTimer(InvTimer, S =>
            {
              var Result = BlazorWindow.NewTimer();
              Result.IntervalEvent += () => InvTimer.IntervalInvoke();
              return Result;
            });

            if (BlazorTimer.IntervalTime != InvTimer.IntervalTime)
              BlazorTimer.IntervalTime = InvTimer.IntervalTime;

            if (InvTimer.IsEnabled && !BlazorTimer.Enabled)
              BlazorTimer.Start();
            else if (!InvTimer.IsEnabled && BlazorTimer.Enabled)
              BlazorTimer.Stop();

            if (!InvTimer.IsEnabled)
              InvWindow.ActiveTimerSet.Remove(InvTimer);
          }
        }

        var InvActiveSurface = InvWindow.ActiveSurface;

        if (InvActiveSurface != null)
        {
          var BlazorSurface = AccessSurface(InvActiveSurface, S =>
          {
            var Result = new BlazorSurface(BlazorWindow.Document);
            return Result;
          });

          if (!BlazorMaster.HasElement(BlazorSurface))
            InvActiveSurface.ArrangeInvoke();

          ProcessTransition(BlazorSurface);

          InvActiveSurface.ComposeInvoke();

          if (InvActiveSurface.Render())
          {
            TranslateBackground(InvActiveSurface.Background, BlazorSurface);

            BlazorSurface.SetContent(RoutePanel(InvActiveSurface.Content));
          }

          InvWindow.ProcessChanges(P => RoutePanel(P));

          if (InvKeyboard.Focus != null)
          {
            // TODO: focus.
          }
        }
        else
        {
          BlazorMaster.RemoveElements();
        }

        if (InvWindow.Render())
        {
          if (InvWindow.Background.Render())
            TranslateBackgroundColour(InvWindow.Background.Colour ?? Inv.Colour.Black, BlazorMaster);
        }

        // TODO: animations.

        InvWindow.DisplayRate.Calculate();
      }
    }
    private void ProcessTransition(BlazorSurface BlazorSurface)
    {
      var InvWindow = InvApplication.Window;

      var InvTransition = InvWindow.ActiveTransition;
      if (InvTransition == null)
      {
      }
      else
      {
        // TODO: animations.

        if (!BlazorMaster.HasElement(BlazorSurface))
        {
          BlazorMaster.RemoveElements();
          BlazorMaster.AddElement(BlazorSurface);

          InvTransition.Complete();
        }

        InvWindow.ActiveTransition = null;
      }
    }

    private BlazorNode TranslateBlock(Inv.Block InvBlock)
    {
      var BlazorNode = AccessPanel(InvBlock, P =>
      {
        var Result = new BlazorCaption(BlazorWindow.Document);
        Result.Class = "block";
        TranslateLineWrapping(P.LineWrapping, Result.Label);
        TranslateJustify(P.Justify, Result.Label);
        return Result;
      });

      RenderPanel(InvBlock, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorBlock = BlazorNode.PanelElement;

        TranslateLayout(InvBlock, BlazorLayout, BlazorBlock);
        TranslateTooltip(InvBlock.Tooltip, BlazorBlock.Label);
        TranslateFont(InvBlock.Font, BlazorBlock.Label);
        TranslateLineWrapping(InvBlock.LineWrapping, BlazorBlock.Label);
        TranslateJustify(InvBlock.Justify, BlazorBlock.Label);

        if (InvBlock.SpanCollection.Render())
        {
          BlazorBlock.Label.RemoveElements();

          foreach (var InvSpan in InvBlock.SpanCollection)
          {
            if (InvSpan.Style == BlockStyle.Break)
            {
              BlazorBlock.Label.AddElement(new BlazorBreak(BlazorWindow.Document));
            }
            else // BlockStyle.Run
            {
              var BlazorSpan = new BlazorSpan(BlazorWindow.Document);
              BlazorBlock.Label.AddElement(BlazorSpan);

              TranslateBackground(InvSpan.Background, BlazorSpan);
              TranslateFont(InvSpan.Font, BlazorSpan);

              BlazorSpan.Text = InvSpan.Text ?? "";
            }
          }
        }
      });

      return BlazorNode;
    }
    private BlazorNode TranslateBoard(Inv.Board InvBoard)
    {
      var BlazorNode = AccessPanel(InvBoard, P =>
      {
        var Result = new BlazorDiv(BlazorWindow.Document);
        Result.Class = "board";
        return Result;
      });

      RenderPanel(InvBoard, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorBoard = BlazorNode.PanelElement;

        TranslateLayout(InvBoard, BlazorLayout, BlazorBoard);

        if (InvBoard.PinCollection.Render())
        {
          BlazorBoard.RemoveElements();

          foreach (var InvPin in InvBoard.PinCollection)
          {
            var BlazorElement = new BlazorDiv(BlazorWindow.Document);
            BlazorBoard.AddElement(BlazorElement);
            BlazorElement.Class = "boardpin";
            // TODO: can left top width height be a single call?
            BlazorElement.Style.Left = TranslatePixels(HorizontalPtToPx(InvPin.Rect.Left));
            BlazorElement.Style.Top = TranslatePixels(VerticalPtToPx(InvPin.Rect.Top));
            BlazorElement.Style.Width = TranslatePixels(HorizontalPtToPx(InvPin.Rect.Width));
            BlazorElement.Style.Height = TranslatePixels(HorizontalPtToPx(InvPin.Rect.Height));

            var BlazorPanel = RoutePanel(InvPin.Panel);
            BlazorElement.AddElement(BlazorPanel);
          }
        }
      });

      return BlazorNode;
    }
    private BlazorNode TranslateBrowser(Inv.Browser InvBrowser)
    {
      var BlazorNode = AccessPanel(InvBrowser, P =>
      {
        var Result = new BlazorIFrame(BlazorWindow.Document);
        Result.Class = "browser";
        return Result;
      });

      RenderPanel(InvBrowser, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorBrowser = BlazorNode.PanelElement;

        TranslateLayout(InvBrowser, BlazorLayout, BlazorBrowser);

        var Navigate = InvBrowser.UriSingleton.Render();
        if (InvBrowser.HtmlSingleton.Render())
          Navigate = true;

        if (Navigate)
          BlazorBrowser.Load(InvBrowser.UriSingleton.Data, InvBrowser.HtmlSingleton.Data);
      });

      return BlazorNode;
    }
    private BlazorNode TranslateButton(Inv.Button InvButton)
    {
      Inv.Colour GetHoverColour(Inv.Colour InvColour) => InvColour?.Lighten(0.25F);
      Inv.Colour GetPressColour(Inv.Colour InvColour) => InvColour?.Darken(0.25F);

      var BlazorNode = AccessPanel(InvButton, P =>
      {
        var MouseHovered = false;

        var Result = new BlazorButton(BlazorWindow.Document);
        Result.Class = "button";
        Result.MouseEnterEvent += () =>
        {
          MouseHovered = true;

          Guard(() =>
          {
            if (P.Style == ButtonStyle.Flat)
              TranslateBackgroundColour(GetHoverColour(P.Background.Colour), Result);

            P.OverInvoke();
          });
        };
        Result.MouseLeaveEvent += () =>
        {
          MouseHovered = false;

          Guard(() =>
          {
            if (P.Style == ButtonStyle.Flat)
              TranslateBackgroundColour(P.Background.Colour, Result);

            P.AwayInvoke();
          });
        };
        Result.MouseDownEvent += () =>
        {
          Guard(() =>
          {
            if (P.Style == ButtonStyle.Flat)
              TranslateBackgroundColour(GetPressColour(P.Background.Colour), Result);

            P.PressInvoke();
          });
        };
        Result.MouseUpEvent += () =>
        {
          Guard(() =>
          {
            if (P.Style == ButtonStyle.Flat)
              TranslateBackgroundColour(MouseHovered ? GetHoverColour(P.Background.Colour) : P.Background.Colour, Result); // TODO: Hovered check.

            InvButton.ReleaseInvoke();
          });
        };
        Result.ClickEvent += () =>
        {
          Guard(() =>
          {
            if (InvApplication.Window.IsActiveSurface(P.Surface))
              P.SingleTapInvoke();
          });
        };
        Result.ContextMenuEvent += () =>
        {
          Guard(() =>
          {
            if (InvApplication.Window.IsActiveSurface(P.Surface))
              P.ContextTapInvoke();
          });
        };
        Result.GotFocusEvent += () =>
        {
          Guard(() =>
          {
            P.Focus.GotInvoke();
          });
        };
        Result.LostFocusEvent += () =>
        {
          Guard(() =>
          {
            P.Focus.LostInvoke();
          });
        };
        return Result;
      });

      RenderPanel(InvButton, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorButton = BlazorNode.PanelElement;

        TranslateLayout(InvButton, BlazorLayout, BlazorButton);
        TranslateTooltip(InvButton.Tooltip, BlazorButton);

        BlazorButton.Disabled = !InvButton.IsEnabled;
        BlazorButton.TabIndex = InvButton.IsFocusable ? null : "-1";

        if (InvButton.ContentSingleton.Render())
        {
          BlazorButton.RemoveElements();

          var BlazorContent = RoutePanel(InvButton.ContentSingleton.Data);
          if (BlazorContent != null)
            BlazorButton.AddElement(BlazorContent);
        }
      });

      return BlazorNode;
    }
    private BlazorNode TranslateCanvas(Inv.Canvas InvCanvas)
    {
      var BlazorNode = AccessPanel(InvCanvas, P =>
      {
        var IsLeftPressed = false;
        var IsRightPressed = false;
        var IsTouchPressed = false;
        var IsTouchReleased = false;

        var Result = new BlazorPainting(BlazorWindow.Document);
        Result.Class = "canvas";
        Result.Canvas.SingleClickEvent += (X, Y, Button) =>
        {
          if (Button == 0) // single left click only.
            Guard(() => P.SingleTapInvoke(new Inv.Point(HorizontalPxToPt(X), VerticalPxToPt(Y))));
        };
        Result.Canvas.DoubleClickEvent += (X, Y, Button) =>
        {
          if (Button == 0) // double left click only.
            Guard(() => P.DoubleTapInvoke(new Inv.Point(HorizontalPxToPt(X), VerticalPxToPt(Y))));
        };
        Result.Canvas.ContextClickEvent += (X, Y, Button) =>
        {
          Guard(() => P.ContextTapInvoke(new Inv.Point(HorizontalPxToPt(X), VerticalPxToPt(Y))));
        };
        Result.Canvas.MouseMoveEvent += (X, Y, Button) =>
        {
          if (IsLeftPressed || IsRightPressed || P.IsCaptured)
            Guard(() => P.MoveInvoke(new Inv.CanvasCommand(new Inv.Point(HorizontalPxToPt(X), VerticalPxToPt(Y)), IsLeftPressed, IsRightPressed)));
        };
        Result.Canvas.MouseDownEvent += (X, Y, Button) =>
        {
          if (Button == 0)
          {
            IsLeftPressed = true;

            Guard(() => P.PressInvoke(new Inv.CanvasCommand(new Inv.Point(HorizontalPxToPt(X), VerticalPxToPt(Y)), LeftMouseButton: true, RightMouseButton: false)));
          }
          else if (Button == 2)
          {
            IsRightPressed = true;

            Guard(() => P.PressInvoke(new Inv.CanvasCommand(new Inv.Point(HorizontalPxToPt(X), VerticalPxToPt(Y)), LeftMouseButton: false, RightMouseButton: true)));
          }
        };
        Result.Canvas.MouseUpEvent += (X, Y, Button) =>
        {
          if (Button == 0)
          {
            IsLeftPressed = false;

            Guard(() => P.ReleaseInvoke(new Inv.CanvasCommand(new Inv.Point(HorizontalPxToPt(X), VerticalPxToPt(Y)), LeftMouseButton: true, RightMouseButton: false)));
          }
          else if (Button == 2)
          {
            IsRightPressed = false;

            Guard(() => P.ReleaseInvoke(new Inv.CanvasCommand(new Inv.Point(HorizontalPxToPt(X), VerticalPxToPt(Y)), LeftMouseButton: false, RightMouseButton: true)));
          }
        };
        Result.Canvas.MouseWheelEvent += (X, Y, Delta) =>
        {
          Guard(() => P.ZoomInvoke(new Inv.Zoom(new Inv.Point(HorizontalPxToPt(X), VerticalPxToPt(Y)), Delta > 0 ? +1 : -1)));
        };

        // Touch state.
        const int DoubleTapTimeoutMS = 300;
        const int LongPressTimeoutMS = 500;
        var LastTime = 0;
        var LastX = 0;
        var LastY = 0;
        System.Threading.Tasks.Task LongPressedTask = null;
        System.Threading.CancellationTokenSource LongPressedTokenSource = null;

        async System.Threading.Tasks.Task LongPressedTaskAsync(Inv.Point Point)
        {
          try
          {
            await System.Threading.Tasks.Task.Delay(LongPressTimeoutMS, LongPressedTokenSource.Token);
          }
          catch (OperationCanceledException) when (LongPressedTokenSource.Token.IsCancellationRequested)
          {
            return; // token was cancelled, so longpressed was abandoned.
          }

          if (!LongPressedTokenSource.IsCancellationRequested)
          {
            IsTouchPressed = false;
            P.ContextTapInvoke(Point);
          }
        }

        void AddLongPressed(Inv.Point Point) 
        {
          Debug.Assert(LongPressedTokenSource == null && LongPressedTask == null);

          LongPressedTokenSource = new System.Threading.CancellationTokenSource();

          LongPressedTask = LongPressedTaskAsync(Point);
        }
        void RemoveLongPressed() 
        {
          if (LongPressedTokenSource != null)
          {
            LongPressedTokenSource.Cancel();
            LongPressedTokenSource = null;
          }

          if (LongPressedTask != null)
          {
            LongPressedTask.Wait();
            LongPressedTask = null;
          }
        }

        Result.Canvas.TouchStartEvent += (X, Y) =>
        {
          //Console.WriteLine($"TouchStart({X}, {Y})");

          RemoveLongPressed();
          IsTouchReleased = false;

          var DownX = HorizontalPxToPt(X);
          var DownY = VerticalPxToPt(Y);
          var DownPoint = new Inv.Point(DownX, DownY);

          LastX = DownX;
          LastY = DownY;

          var CurrentTime = System.Environment.TickCount;
          var IsDoubleTap = CurrentTime - LastTime < DoubleTapTimeoutMS;
          if (IsDoubleTap)
          {
            IsTouchPressed = false; // we don't want single tap to fire.
            Guard(() => P.DoubleTapInvoke(DownPoint));

            IsTouchReleased = true;
            Guard(() => P.ReleaseInvoke(new Inv.CanvasCommand(DownPoint, LeftMouseButton: false, RightMouseButton: false)));
          }
          else
          {
            if (IsTouchPressed)
              RemoveLongPressed();
            else
              IsTouchPressed = true;

            AddLongPressed(DownPoint);

            Guard(() => P.PressInvoke(new Inv.CanvasCommand(DownPoint, LeftMouseButton: false, RightMouseButton: false)));
          }

          LastTime = CurrentTime;
        };
        Result.Canvas.TouchMoveEvent += (X, Y) =>
        {
          //Console.WriteLine($"TouchMove({X}, {Y})");

          var MoveX = HorizontalPxToPt(X);
          var MoveY = VerticalPxToPt(Y);
          var MovePoint = new Inv.Point(MoveX, MoveY);

          if (IsTouchPressed)
          {
            var Distance = ((MoveY - LastY) * (MoveY - LastY)) + ((MoveX - LastX) * (MoveX - LastX));
            if (Distance > 400) // sqrt(400) == 20
            {
              IsTouchPressed = false; // moved too far to be 'pressed'.

              RemoveLongPressed();
            }
          }

          if (!IsTouchReleased)
            Guard(() => P.MoveInvoke(new Inv.CanvasCommand(MovePoint, LeftMouseButton: false, RightMouseButton: false)));
        };
        Result.Canvas.TouchEndEvent += (X, Y) =>
        {
          //Console.WriteLine($"TouchEnd({X}, {Y})");

          RemoveLongPressed();

          var EndX = HorizontalPxToPt(X);
          var EndY = VerticalPxToPt(Y);
          var EndPoint = new Inv.Point(EndX, EndY);

          if (IsTouchPressed)
          {
            IsTouchPressed = false;
            Guard(() => P.SingleTapInvoke(EndPoint));
          }

          if (!IsTouchReleased)
          {
            IsTouchReleased = true;
            Guard(() => P.ReleaseInvoke(new Inv.CanvasCommand(EndPoint, LeftMouseButton: false, RightMouseButton: false)));
          }
        };
        Result.Canvas.TouchCancelEvent += (X, Y) =>
        {
          //Console.WriteLine($"TouchCancel({X}, {Y})");

          RemoveLongPressed();

          if (IsTouchPressed)
            IsTouchPressed = false;

          if (!IsTouchReleased)
          {
            IsTouchReleased = true;

            var CancelX = HorizontalPxToPt(X);
            var CancelY = VerticalPxToPt(Y);
            var CancelPoint = new Inv.Point(CancelX, CancelY);

            Guard(() => P.ReleaseInvoke(new Inv.CanvasCommand(CancelPoint, LeftMouseButton: false, RightMouseButton: false)));
          }
        };
        return Result;
      }, R => R.Framing); // NOTE: we use the .Framing element for layout.

      RenderPanel(InvCanvas, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorCanvas = BlazorNode.PanelElement;

        TranslateLayout(InvCanvas, BlazorLayout, BlazorCanvas.Framing);

        if (InvCanvas.IsInvalidated)
          Guard(() => BlazorRender.Compose(InvCanvas, BlazorCanvas));
      });

      return BlazorNode;
    }
    private BlazorNode TranslateDock(Inv.Dock InvDock)
    {
      var BlazorNode = AccessPanel(InvDock, P =>
      {
        var Result = new BlazorSeries(BlazorWindow.Document);
        Result.Class = P.IsHorizontal ? "hdock" : "vdock";
        return Result;
      });

      RenderPanel(InvDock, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorDock = BlazorNode.PanelElement;

        var ChangeClass = InvDock.IsHorizontal ? "hdock" : "vdock";
        if (BlazorDock.Class != ChangeClass)
        {
          BlazorDock.Class = ChangeClass;

          var SwapTemplate = BlazorDock.Style.GridTemplateColumns;
          BlazorDock.Style.GridTemplateColumns = BlazorDock.Style.GridTemplateRows;
          BlazorDock.Style.GridTemplateRows = SwapTemplate;
        }

        TranslateLayout(InvDock, BlazorLayout, BlazorDock);

        if (InvDock.CollectionRender())
        {
          var ElementList = new List<BlazorElement>(InvDock.PanelCount);

          var TemplateBuilder = new StringBuilder();

          void AddPanel(Inv.Panel Panel, string Grow)
          {
            ElementList.Add(RoutePanel(Panel));

            TemplateBuilder.Append(Grow + " ");
          }

          foreach (var InvElement in InvDock.HeaderCollection)
            AddPanel(InvElement, "max-content");

          var AnyIsVisible = false;

          foreach (var InvElement in InvDock.ClientCollection)
          {
            var ElementIsVisible = true; //InvElement.Control.Visibility.Get(); // TODO: this doesn't seem required any more.
            if (ElementIsVisible)
              AnyIsVisible = true;

            AddPanel(InvElement, ElementIsVisible ? "minmax(0px, 1fr)" : "0px"); // TODO: this isn't reliable - you can't toggle Visibility and have the Dock update properly.
          }

          // fill unused client space so the footers are pushed to the bottom.
          if (!AnyIsVisible)
            AddPanel(Panel: null, "minmax(0px, 1fr)");

          foreach (var InvElement in InvDock.FooterCollection)
            AddPanel(InvElement, "max-content");

          BlazorDock.ComposeSlots(ElementList);

          if (InvDock.IsHorizontal)
          {
            BlazorDock.Style.GridTemplateColumns = TemplateBuilder.ToString();
            BlazorDock.Style.GridTemplateRows = null;
          }
          else
          {
            BlazorDock.Style.GridTemplateColumns = null;
            BlazorDock.Style.GridTemplateRows = TemplateBuilder.ToString();
          }
        }

        BlazorDock.UpdateSlotClass(InvDock.IsHorizontal ? "hdockitem" : "vdockitem");
      });

      return BlazorNode;
    }
    private BlazorNode TranslateEdit(Inv.Edit InvEdit)
    {
      var BlazorNode = AccessPanel(InvEdit, P =>
      {
        var Result = new BlazorInput(BlazorWindow.Document);
        Result.Class = "edit";

        switch (InvEdit.Input)
        {
          case EditInput.Text:
            break;

          default:
            break;
        }

        Result.ChangeEvent += () => P.ChangeText(Result.Text);
        return Result;
      });

      RenderPanel(InvEdit, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorEdit = BlazorNode.PanelElement;

        TranslateLayout(InvEdit, BlazorLayout, BlazorEdit);
        TranslateFont(InvEdit.Font, BlazorEdit);

        BlazorEdit.IsReadOnly = InvEdit.IsReadOnly;
        BlazorEdit.Text = InvEdit.Text ?? "";
      });

      return BlazorNode;
    }
    private BlazorNode TranslateFlow(Inv.Flow InvFlow)
    {
      var BlazorNode = AccessPanel(InvFlow, P =>
      {
        var Result = new BlazorFlow(BlazorWindow.Document);
        return Result;
      });

      RenderPanel(InvFlow, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorFlow = BlazorNode.PanelElement;

        TranslateLayout(InvFlow, BlazorLayout, BlazorFlow);

        // TODO: virtualisation.

        if (InvFlow.IsReload)
        {
          var PanelList = new Inv.DistinctList<BlazorElement>();

          void AddPanel(Inv.Panel Panel)
          {
            var ItemContent = RoutePanel(Panel);
            if (ItemContent != null)
              PanelList.Add(ItemContent);
          }

          var SectionCount = InvFlow.SectionCount;

          for (var SectionIndex = 0; SectionIndex < SectionCount; SectionIndex++)
          {
            AddPanel(InvFlow.Sections[SectionIndex]?.Header);

            var ItemCount = InvFlow.Sections[SectionIndex]?.ItemCount ?? 0;

            for (var ItemIndex = 0; ItemIndex < ItemCount; ItemIndex++)
              AddPanel(InvFlow.Sections[SectionIndex]?.ItemInvoke(ItemIndex));

            AddPanel(InvFlow.Sections[SectionIndex]?.Footer);
          }

          BlazorFlow.ComposeElements(PanelList);
        }
      });

      return BlazorNode;
    }
    private BlazorNode TranslateFrame(Inv.Frame InvFrame)
    {
      var BlazorNode = AccessPanel(InvFrame, P =>
      {
        var Result = new BlazorDiv(BlazorWindow.Document);
        Result.Class = "frame";
        return Result;
      });

      RenderPanel(InvFrame, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorFrame = BlazorNode.PanelElement;

        TranslateLayout(InvFrame, BlazorLayout, BlazorFrame);

        if (InvFrame.ContentSingleton.Render())
        {
          BlazorFrame.RemoveElements();

          var BlazorContent = RoutePanel(InvFrame.ContentSingleton.Data);
          if (BlazorContent != null)
            BlazorFrame.AddElement(BlazorContent);
        }
      });

      return BlazorNode;
    }
    private BlazorNode TranslateGraphic(Inv.Graphic InvGraphic)
    {
      var BlazorNode = AccessPanel(InvGraphic, P =>
      {
        var Result = new BlazorGraphic(BlazorWindow.Document);
        Result.Class = "graphic";
        Result.Img.Source = "";
        Result.Img.Style.Visibility = "hidden"; // no src by default.
        Result.Img.Style.ObjectFit = TranslateFit(P.Fit); // make sure we have the correct default.
        return Result;
      });

      RenderPanel(InvGraphic, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorGraphic = BlazorNode.PanelElement;

        TranslateLayout(InvGraphic, BlazorLayout, BlazorGraphic);
        TranslateTooltip(InvGraphic.Tooltip, BlazorGraphic.Img);

        if (InvGraphic.Fit.Render())
          BlazorGraphic.Img.Style.ObjectFit = TranslateFit(InvGraphic.Fit);

        if (InvGraphic.ImageSingleton.Render())
        {
          if (InvGraphic.Image == null)
          {
            BlazorGraphic.Img.Source = "";
            BlazorGraphic.Img.Style.Visibility = "hidden";
          }
          else
          {
            BlazorGraphic.Img.Source = TranslateImageSource(InvGraphic.Image);
            BlazorGraphic.Img.Style.Visibility = null;
          }
        }
      });

      return BlazorNode;
    }
    private BlazorNode TranslateLabel(Inv.Label InvLabel)
    {
      // blank or null strings will be given a height of 0px, so we need to use representative text with a width of 0px to emulate the desired behaviour.
      const string RepresentativeText = "Pp";
      const string RepresentativeWidth = "0px";

      var BlazorNode = AccessPanel(InvLabel, P =>
      {
        var Result = new BlazorCaption(BlazorWindow.Document);
        Result.Class = "label";
        Result.Label.Text = RepresentativeText;
        Result.Label.Style.Width = RepresentativeWidth;
        TranslateLineWrapping(P.LineWrapping, Result.Label);
        TranslateJustify(P.Justify, Result.Label);
        return Result;
      });

      RenderPanel(InvLabel, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorLabel = BlazorNode.PanelElement;

        TranslateLayout(InvLabel, BlazorLayout, BlazorLabel);
        TranslateTooltip(InvLabel.Tooltip, BlazorLabel.Label);
        TranslateFont(InvLabel.Font, BlazorLabel.Label);
        TranslateLineWrapping(InvLabel.LineWrapping, BlazorLabel.Label);
        TranslateJustify(InvLabel.Justify, BlazorLabel.Label);

        if (string.IsNullOrEmpty(InvLabel.Text))
        {
          BlazorLabel.Label.Text = RepresentativeText;
          BlazorLabel.Label.Style.Width = RepresentativeWidth;
        }
        else
        {
          BlazorLabel.Label.Text = InvLabel.Text;
          BlazorLabel.Label.Style.Width = null;
        }
      });

      return BlazorNode;
    }
    private BlazorNode TranslateMemo(Inv.Memo InvMemo)
    {
      var BlazorNode = AccessPanel(InvMemo, P =>
      {
        var Result = new BlazorTextArea(BlazorWindow.Document);
        Result.Class = "memo";
        Result.ChangeEvent += () => P.ChangeText(Result.Text);
        return Result;
      });

      RenderPanel(InvMemo, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorMemo = BlazorNode.PanelElement;

        TranslateLayout(InvMemo, BlazorLayout, BlazorMemo);
        TranslateFont(InvMemo.Font, BlazorMemo);

        BlazorMemo.IsReadOnly = InvMemo.IsReadOnly;
        BlazorMemo.Text = InvMemo.Text ?? "";

        // TODO: memo doesn't size to content correctly.
        //BlazorMemo.Style.Height = "auto";
        //BlazorMemo.Style.Height = TranslatePixels(BlazorMemo.GetField<int>("scrollHeight"));
      });

      return BlazorNode;
    }
    private BlazorNode TranslateNative(Inv.Native InvNative)
    {
      var BlazorNode = AccessPanel(InvNative, P =>
      {
        var Result = new BlazorDiv(BlazorWindow.Document);
        Result.Class = "native";
        return Result;
      });

      RenderPanel(InvNative, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorNative = BlazorNode.PanelElement;

        TranslateLayout(InvNative, BlazorLayout, BlazorNative);

        /* TODO: html Native. */
      });

      return BlazorNode;
    }
    private BlazorNode TranslateOverlay(Inv.Overlay InvOverlay)
    {
      var BlazorNode = AccessPanel(InvOverlay, P =>
      {
        var Result = new BlazorSeries(BlazorWindow.Document);
        Result.Class = "overlay";
        Result.UpdateSlotClass("layer");
        return Result;
      });

      RenderPanel(InvOverlay, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorOverlay = BlazorNode.PanelElement;

        TranslateLayout(InvOverlay, BlazorLayout, BlazorOverlay);

        if (InvOverlay.PanelCollection.Render())
          BlazorOverlay.ComposeSlots(InvOverlay.PanelCollection.Select(P => RoutePanel(P)).ToArray());
      });

      return BlazorNode;
    }
    private BlazorNode TranslateScroll(Inv.Scroll InvScroll)
    {
      var BlazorNode = AccessPanel(InvScroll, P =>
      {
        var Result = new BlazorDiv(BlazorWindow.Document);
        Result.Class = P.IsHorizontal ? "hscroll" : "vscroll";
        return Result;
      });

      RenderPanel(InvScroll, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorScroll = BlazorNode.PanelElement;

        BlazorScroll.Class = InvScroll.IsHorizontal ? "hscroll" : "vscroll";

        TranslateLayout(InvScroll, BlazorLayout, BlazorScroll);

        if (InvScroll.ContentSingleton.Render())
        {
          BlazorScroll.RemoveElements();

          // TODO: keep this item always.
          var BlazorItem = new BlazorDiv(BlazorWindow.Document);
          BlazorScroll.AddElement(BlazorItem);
          BlazorItem.Class = InvScroll.IsHorizontal ? "hscroll" : "vscroll";

          var BlazorContent = RoutePanel(InvScroll.ContentSingleton.Data);
          if (BlazorContent != null)
            BlazorItem.AddElement(BlazorContent);
        }

        var BlazorItemClass = InvScroll.IsHorizontal ? "hscrollitem" : "vscrollitem";
        foreach (var BlazorItem in BlazorScroll.GetElements())
          BlazorItem.Class = BlazorItemClass;

        var Request = InvScroll.HandleRequest();
        if (Request != null)
        {
          switch (Request.Value)
          {
            case ScrollRequest.Start:
              if (InvScroll.IsHorizontal)
                BlazorScroll.ScrollToLeft();
              else
                BlazorScroll.ScrollToTop();
              break;

            case ScrollRequest.End:
              if (InvScroll.IsHorizontal)
                BlazorScroll.ScrollToRight();
              else
                BlazorScroll.ScrollToBottom();
              break;

            default:
              throw EnumHelper.UnexpectedValueException(Request.Value);
          }
        }
      });

      return BlazorNode;
    }
    private BlazorNode TranslateShape(Inv.Shape InvShape)
    {
      var BlazorNode = AccessPanel(InvShape, P =>
      {
        var Result = new BlazorDiv(BlazorWindow.Document);
        Result.Class = "shape";
        return Result;
      });

      RenderPanel(InvShape, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorShape = BlazorNode.PanelElement;

        TranslateLayout(InvShape, BlazorLayout, BlazorShape);

        /* TODO: html Shape. */
      });

      return BlazorNode;
    }
    private BlazorNode TranslateStack(Inv.Stack InvStack)
    {
      var BlazorNode = AccessPanel(InvStack, P =>
      {
        var Result = new BlazorSeries(BlazorWindow.Document);
        Result.Class = P.IsHorizontal ? "hstack" : "vstack";
        return Result;
      });

      RenderPanel(InvStack, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorStack = BlazorNode.PanelElement;

        BlazorStack.Class = InvStack.IsHorizontal ? "hstack" : "vstack";

        TranslateLayout(InvStack, BlazorLayout, BlazorStack);

        if (InvStack.PanelCollection.Render())
          BlazorStack.ComposeSlots(InvStack.PanelCollection.Select(P => RoutePanel(P)).ToArray());

        BlazorStack.UpdateSlotClass(InvStack.IsHorizontal ? "hstackitem" : "vstackitem");
      });

      return BlazorNode;
    }
    private BlazorNode TranslateSwitch(Inv.Switch InvSwitch)
    {
      var BlazorNode = AccessPanel(InvSwitch, P =>
      {
        var Result = new BlazorDiv(BlazorWindow.Document);
        Result.Class = "switch";
        return Result;
      });

      RenderPanel(InvSwitch, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorSwitch = BlazorNode.PanelElement;

        TranslateLayout(InvSwitch, BlazorLayout, BlazorSwitch);

        /* TODO: html Switch. */
      });

      return BlazorNode;
    }
    private BlazorNode TranslateTable(Inv.Table InvTable)
    {
      var BlazorNode = AccessPanel(InvTable, P =>
      {
        var Result = new BlazorDiv(BlazorWindow.Document);
        Result.Class = "table";
        return Result;
      });

      RenderPanel(InvTable, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorTable = BlazorNode.PanelElement;
        
        TranslateLayout(InvTable, BlazorLayout, BlazorTable);

        if (InvTable.CollectionRender())
        {
          // TODO: switch to a series/slots and compose the elements?
          BlazorTable.RemoveElements();

          string Resolve(Inv.TableAxis Axis)
          {
            return Axis.LengthType switch
            {
              TableAxisLength.Auto => "max-content",
              TableAxisLength.Star => $"minmax(0px, {Axis.LengthValue}fr)",
              TableAxisLength.Fixed => TranslatePixels(Axis.LengthValue),
              _ => throw Inv.Support.EnumHelper.UnexpectedValueException(Axis.LengthType),
            };
          }

          var ColumnTemplateBuilder = new StringBuilder();
          var RowTemplateBuilder = new StringBuilder();

          void AddCell(Inv.Panel InvPanel, string Column, string Row)
          {
            var BlazorCell = new BlazorDiv(BlazorWindow.Document);
            BlazorTable.AddElement(BlazorCell);
            BlazorCell.Class = "tablecell";
            BlazorCell.Style.GridColumn = Column;
            BlazorCell.Style.GridRow = Row;

            var BlazorElement = RoutePanel(InvPanel);
            BlazorCell.AddElement(BlazorElement);
          }

          foreach (var InvColumn in InvTable.ColumnCollection)
          {
            ColumnTemplateBuilder.Append(Resolve(InvColumn) + " ");

            if (InvColumn.Content != null)
              AddCell(InvColumn.Content, (InvColumn.Index + 1).ToString(System.Globalization.CultureInfo.InvariantCulture), "1 / span " + InvTable.Rows.Count.ToString(System.Globalization.CultureInfo.InvariantCulture));
          }

          foreach (var InvRow in InvTable.RowCollection)
          {
            RowTemplateBuilder.Append(Resolve(InvRow) + " ");

            if (InvRow.Content != null)
              AddCell(InvRow.Content, "1 / span " + InvTable.Columns.Count.ToString(System.Globalization.CultureInfo.InvariantCulture), (InvRow.Index + 1).ToString(System.Globalization.CultureInfo.InvariantCulture));

            foreach (var InvCell in InvRow.GetCells())
            {
              if (InvCell.Content != null)
                AddCell(InvCell.Content, (InvCell.Column.Index + 1).ToString(System.Globalization.CultureInfo.InvariantCulture), (InvCell.Row.Index + 1).ToString(System.Globalization.CultureInfo.InvariantCulture));
            }
          }

          //Console.WriteLine("grid-template-columns: " + ColumnTemplateBuilder.ToString());
          //Console.WriteLine("grid-template-rows: " + RowTemplateBuilder.ToString());

          BlazorTable.Style.GridTemplateColumns = ColumnTemplateBuilder.ToString();
          BlazorTable.Style.GridTemplateRows = RowTemplateBuilder.ToString();
        }
      });

      return BlazorNode;
    }
    private BlazorNode TranslateWrap(Inv.Wrap InvWrap)
    {
      var BlazorNode = AccessPanel(InvWrap, P =>
      {
        var Result = new BlazorSeries(BlazorWindow.Document);
        Result.Class = P.IsHorizontal ? "hwrap" : "vwrap";
        return Result;
      });

      RenderPanel(InvWrap, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorWrap = BlazorNode.PanelElement;

        BlazorWrap.Class = InvWrap.IsHorizontal ? "hwrap" : "vwrap";

        TranslateLayout(InvWrap, BlazorLayout, BlazorWrap);

        if (InvWrap.PanelCollection.Render())
          BlazorWrap.ComposeSlots(InvWrap.PanelCollection.Select(P => RoutePanel(P)).ToArray());

        BlazorWrap.UpdateSlotClass(InvWrap.IsHorizontal ? "hwrapitem" : "vwrapitem");
      });

      return BlazorNode;
    }
    private BlazorNode TranslateVideo(Inv.Video InvVideo)
    {
      var BlazorNode = AccessPanel(InvVideo, P =>
      {
        var Result = new BlazorDiv(BlazorWindow.Document);
        Result.Class = "video";
        return Result;
      });

      RenderPanel(InvVideo, BlazorNode, () =>
      {
        var BlazorLayout = BlazorNode.LayoutElement;
        var BlazorVideo = BlazorNode.PanelElement;

        TranslateLayout(InvVideo, BlazorLayout, BlazorVideo);

        /* TODO: html video. */
      });

      return BlazorNode;
    }

    private string TranslatePixels(int Pixels) => Pixels.ToString(System.Globalization.CultureInfo.InvariantCulture) + "px";
    private string TranslateFit(Inv.Fit InvFit)
    {
      return InvFit.Method switch
      {
        Inv.FitMethod.Stretch => "fill",
        Inv.FitMethod.Cover => "cover",
        Inv.FitMethod.Original => "none",
        Inv.FitMethod.Contain => "contain",
        _ => throw Inv.Support.EnumHelper.UnexpectedValueException(InvFit.Method),
      };
    }
    private void TranslateJustify(Inv.Justify InvJustify, BlazorElement BlazorElement)
    {
      BlazorElement.Style.TextAlign = InvJustify.Get() == Justification.Left ? "left" : InvJustify.Get() == Justification.Right ? "right" : "center";
    }
    private void TranslateFont(Inv.Font Font, BlazorElement BlazorElement)
    {
      if (Font.Render())
      {
        BlazorElement.Style.FontFamily = Font.Name;
        BlazorElement.Style.FontSize = Font.Size == null ? null : TranslatePixels(Font.Size.Value);
        BlazorElement.Style.FontColor = Font.Colour == null ? null : Font.Colour.BlazorColour();
        BlazorElement.Style.FontWeight = Font.Weight == null ? null : TranslateFontWeight(Font.Weight.Value);
        BlazorElement.Style.FontStyle = Font.IsItalics == null ? null : Font.IsItalics.Value ? "italic" : "normal";
        BlazorElement.Style.FontVariant = Font.IsSmallCaps == null ? null : Font.IsSmallCaps.Value ? "small-caps" : "normal";
        BlazorElement.Style.TextDecoration = Font.IsUnderlined == null && Font.IsStrikethrough == null ? null :
          (Font.IsStrikethrough ?? false) && (Font.IsUnderlined ?? false) ? "underline line-through" :
          (Font.IsStrikethrough ?? false) && !(Font.IsUnderlined ?? false) ? "line-through" :
          !(Font.IsStrikethrough ?? false) && (Font.IsUnderlined ?? false) ? "underline" :
          "none";
        BlazorElement.Style.VerticalAlign = Font.Axis == null ? null : TranslateFontAxis(Font.Axis.Value);
      }
    }
    private void TranslateLineWrapping(bool LineWrapping, BlazorElement BlazorElement)
    {
      if (LineWrapping)
      {
        BlazorElement.Style.WhiteSpace = null;
        BlazorElement.Style.Overflow = null;
        BlazorElement.Style.TextOverflow = null;
      }
      else
      {
        BlazorElement.Style.WhiteSpace = "nowrap";
        BlazorElement.Style.Overflow = "hidden";
        BlazorElement.Style.TextOverflow = "ellipsis";
      }
    }
    private void TranslateLayout(Inv.Control InvControl, BlazorElement BlazorLayout, BlazorElement BlazorPanel)
    {
      var NeedsArranging = false;

      var InvSize = InvControl.Size;
      if (InvSize.Render())
        NeedsArranging = true;

      var InvAlignment = InvControl.Alignment;
      if (InvAlignment.Render())
        NeedsArranging = true;

      var InvMargin = InvControl.Margin;
      if (InvMargin.Render())
        NeedsArranging = true;

      var InvVisibility = InvControl.Visibility;
      if (InvVisibility.Render())
        NeedsArranging = true;

      if (InvControl.Opacity.Render())
        BlazorPanel.Style.Opacity = InvControl.Opacity.Get().ToString();

      TranslateBackground(InvControl.Background, BlazorPanel);

      var InvBorder = InvControl.Border;
      if (InvBorder.Render())
      {
        if (InvBorder.IsSet)
        {
          BlazorPanel.Style.BorderColor = InvBorder.Colour == null ? "transparent" : InvBorder.Colour.BlazorColour();
          BlazorPanel.Style.BorderStyle = "solid";
          BlazorPanel.Style.BorderWidth = $"{TranslatePixels(VerticalPtToPx(InvBorder.Top))} {TranslatePixels(HorizontalPtToPx(InvBorder.Right))} {TranslatePixels(VerticalPtToPx(InvBorder.Bottom))} {TranslatePixels(HorizontalPtToPx(InvBorder.Left))}";
        }
        else
        {
          BlazorPanel.Style.BorderColor = InvBorder.Colour == null ? null : InvBorder.Colour.BlazorColour();
          BlazorPanel.Style.BorderStyle = null;
          BlazorPanel.Style.BorderWidth = null;
        }
      }

      var InvCorner = InvControl.Corner;
      if (InvCorner.Render())
      {
        var Radius = InvCorner.IsSet ? $"{TranslatePixels(VerticalPtToPx(InvCorner.TopLeft))} {TranslatePixels(HorizontalPtToPx(InvCorner.TopRight))} {TranslatePixels(VerticalPtToPx(InvCorner.BottomRight))} {TranslatePixels(HorizontalPtToPx(InvCorner.BottomLeft))}" : null;

        BlazorLayout.Style.BorderRadius = Radius;
        BlazorPanel.Style.BorderRadius = Radius;
        BlazorPanel.Style.Overflow = InvCorner.IsSet ? "hidden" : null;
      }

      var InvPadding = InvControl.Padding;
      if (InvPadding.Render())
      {
        if (InvPadding.IsSet)
          BlazorPanel.Style.Padding = $"{TranslatePixels(VerticalPtToPx(InvPadding.Top))} {TranslatePixels(HorizontalPtToPx(InvPadding.Right))} {TranslatePixels(VerticalPtToPx(InvPadding.Bottom))} {TranslatePixels(HorizontalPtToPx(InvPadding.Left))}";
        else
          BlazorPanel.Style.Padding = null;
      }

      if (NeedsArranging)
      {
        var IsVisible = InvVisibility.Get();

        if (IsVisible)
        {
          var PxWidth = InvSize.Width == null ? (int?)null : HorizontalPtToPx(InvSize.Width.Value);
          var PxHeight = InvSize.Height == null ? (int?)null : HorizontalPtToPx(InvSize.Height.Value);
          var PxMinimumWidth = InvSize.MinimumWidth == null ? (int?)null : HorizontalPtToPx(InvSize.MinimumWidth.Value);
          var PxMinimumHeight = InvSize.MinimumHeight == null ? (int?)null : HorizontalPtToPx(InvSize.MinimumHeight.Value);
          var PxMaximumWidth = InvSize.MaximumWidth == null ? (int?)null : HorizontalPtToPx(InvSize.MaximumWidth.Value);
          var PxMaximumHeight = InvSize.MaximumHeight == null ? (int?)null : HorizontalPtToPx(InvSize.MaximumHeight.Value);

          var IsWidthConstrained = PxWidth != null || PxMaximumWidth != null;
          var IsHeightConstrained = PxHeight != null || PxMaximumHeight != null;

          var TranslatePlacement = InvAlignment.Get();

          if (TranslatePlacement == Placement.Stretch)
          {
            if (IsWidthConstrained && IsHeightConstrained)
              TranslatePlacement = Placement.Center;
            else if (IsWidthConstrained)
              TranslatePlacement = Placement.StretchCenter;
            else if (IsHeightConstrained)
              TranslatePlacement = Placement.CenterStretch;
          }
          else if (TranslatePlacement == Placement.StretchLeft)
          {
            if (IsHeightConstrained)
              TranslatePlacement = Placement.CenterLeft;
          }
          else if (TranslatePlacement == Placement.StretchRight)
          {
            if (IsHeightConstrained)
              TranslatePlacement = Placement.CenterRight;
          }
          else if (TranslatePlacement == Placement.StretchCenter)
          {
            if (IsHeightConstrained)
              TranslatePlacement = Placement.Center;
          }
          else if (TranslatePlacement == Placement.TopStretch)
          {
            if (IsWidthConstrained)
              TranslatePlacement = Placement.TopCenter;
          }
          else if (TranslatePlacement == Placement.CenterStretch)
          {
            if (IsWidthConstrained)
              TranslatePlacement = Placement.Center;
          }
          else if (TranslatePlacement == Placement.BottomStretch)
          {
            if (IsWidthConstrained)
              TranslatePlacement = Placement.BottomCenter;
          }

          var IsStretchHorizontal = InvAlignment.IsHorizontalStretch();
          var IsStretchVertical = InvAlignment.IsVerticalStretch();

          BlazorLayout.Class = TranslatePlacement.ToString();

          int MarginHeight;
          int MarginWidth;
          if (InvMargin.IsSet && IsVisible)
          {
            var MarginTop = VerticalPtToPx(InvMargin.Top);
            var MarginRight = HorizontalPtToPx(InvMargin.Right);
            var MarginBottom = VerticalPtToPx(InvMargin.Bottom);
            var MarginLeft = HorizontalPtToPx(InvMargin.Left);

            BlazorPanel.Style.Margin = $"{TranslatePixels(MarginTop)} {TranslatePixels(MarginRight)} {TranslatePixels(MarginBottom)} {TranslatePixels(MarginLeft)}";

            MarginWidth = MarginLeft + MarginRight;
            MarginHeight = MarginTop + MarginBottom;
          }
          else
          {
            BlazorPanel.Style.Margin = null;

            MarginWidth = 0;
            MarginHeight = 0;
          }

          string CalculateFullSize(int Margin) => Margin == 0 ? "100%" : "calc(100% - " + TranslatePixels(Margin) + ")";

          if (PxWidth != null)
            BlazorPanel.Style.Width = TranslatePixels(PxWidth.Value);
          else if (IsStretchHorizontal)
            BlazorPanel.Style.Width = CalculateFullSize(MarginWidth);
          else
            BlazorPanel.Style.Width = null;

          if (PxHeight != null)
            BlazorPanel.Style.Height = TranslatePixels(PxHeight.Value);
          else if (IsStretchVertical)
            BlazorPanel.Style.Height = CalculateFullSize(MarginHeight);
          else
            BlazorPanel.Style.Height = null;

          // This is necessary in Studio.ActionPanel to prevent the panel from drawing outside the parent div.
          BlazorLayout.Style.OverflowX = PxMinimumWidth == null ? null : "hidden";
          BlazorLayout.Style.OverflowY = PxMinimumHeight == null ? null : "hidden";

          BlazorPanel.Style.MinWidth = PxMinimumWidth == null ? null : TranslatePixels(PxMinimumWidth.Value);
          BlazorPanel.Style.MinHeight = PxMinimumHeight == null ? null : TranslatePixels(PxMinimumHeight.Value);

          BlazorPanel.Style.MaxWidth = PxMaximumWidth == null ? null : TranslatePixels(PxMaximumWidth.Value);
          BlazorPanel.Style.MaxHeight = PxMaximumHeight == null ? null : TranslatePixels(PxMaximumHeight.Value);
        }
        else
        {
          // NOTE: visibility collapse is not implemented in Html5 so we need to use a workaround.
          BlazorLayout.Class = "Collapsed";
          BlazorPanel.Style.Width = null;
          BlazorPanel.Style.Height = null;
          BlazorPanel.Style.MinWidth = null;
          BlazorPanel.Style.MinHeight = null;
          BlazorPanel.Style.MaxWidth = null;
          BlazorPanel.Style.MaxHeight = null;
          BlazorPanel.Style.OverflowX = null;
          BlazorPanel.Style.OverflowY = null;
          BlazorPanel.Style.Margin = null;
        }
      }

      if (InvControl.Elevation.Render())
      {
        var Depth = InvControl.Elevation.Get();
        if (Depth > 0)
          BlazorPanel.Style.BoxShadow = $"{TranslatePixels(Depth)} {TranslatePixels(Depth * 3)} {TranslatePixels(Depth * 2)} 0 rgba(187, 187, 187, 0.20)";
        else
          BlazorPanel.Style.BoxShadow = null;
      }

      // hookup resize event.
      BlazorPanel.ResizeEvent = InvControl.HasAdjust ? InvControl.AdjustInvoke : (Action)null;
    }
    private void TranslateBackground(Background InvBackground, BlazorElement BlazorElement)
    {
      if (InvBackground.Render())
        TranslateBackgroundColour(InvBackground.Colour, BlazorElement);
    }
    private void TranslateTooltip(Tooltip InvTooltip, BlazorElement BlazorElement)
    {
      if (InvTooltip.Render())
      {
        // TODO: custom tooltips.
      }
    }
    private void TranslateBackgroundColour(Inv.Colour InvColour, BlazorElement BlazorElement)
    {
      if (InvColour != null)
        BlazorElement.Style.BackgroundColor = InvColour.BlazorColour();
      else
        BlazorElement.Style.BackgroundColor = null;
    }
    private BlazorSurface AccessSurface(Inv.Surface InvSurface, Func<Inv.Surface, BlazorSurface> BuildFunction)
    {
      if (InvSurface.Node == null)
      {
        var Result = BuildFunction(InvSurface);

        InvSurface.Node = Result;

        return Result;
      }
      else
      {
        return (BlazorSurface)InvSurface.Node;
      }
    }
    private BlazorTimer AccessTimer(Inv.WindowTimer InvTimer, Func<Inv.WindowTimer, BlazorTimer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (BlazorTimer)InvTimer.Node;
      }
    }
    private BlazorElement RoutePanel(Inv.Panel InvPanel)
    {
      var InvControl = InvPanel?.Control;

      if (InvControl == null)
        return null;
      else
        return RouteArray[InvControl.ControlType](InvControl).LayoutElement;
    }
    private BlazorNode<TElement> AccessPanel<TElement, TControl>(TControl InvControl, Func<TControl, TElement> BuildFunction, Func<TElement, BlazorElement> TargetFunction = null)
      where TElement : BlazorElement
      where TControl : Inv.Control
    {
      if (InvControl.Node == null)
      {
        var Result = new BlazorNode<TElement>();

        // stretched to parent layout by default.
        Result.LayoutElement = new BlazorDiv(BlazorWindow.Document);
        Result.LayoutElement.Class = "Stretch";

        Result.PanelElement = BuildFunction(InvControl);

        var TargetElement = TargetFunction?.Invoke(Result.PanelElement) ?? Result.PanelElement;
        TargetElement.Style.Width = "100%";
        TargetElement.Style.Height = "100%";

        Result.LayoutElement.AddElement(Result.PanelElement);

        InvControl.Node = Result;

        return Result;
      }
      else
      {
        return (BlazorNode<TElement>)InvControl.Node;
      }
    }
    private void RenderPanel(Inv.Control InvControl, BlazorNode BlazorNode, Action Action)
    {
      if (InvControl.Render())
        Action();
    }
    private string TranslateImageSource(Inv.Image InvImage)
    {
      if (InvImage == null)
        return "";
      else
        return $"data:image/{InvImage.GetFormat().TrimStart('.')};base64,{Convert.ToBase64String(InvImage.GetBuffer())}"; // image source is a base64 string.
    }
    private string SelectAssetPath(Asset Asset) => $"{AppAssemblyName}.Assets.{Asset.Name}";

    [Microsoft.JSInterop.JSInvokable]
    public void ApplicationLoop(float timeStamp) => Guard(() => Process());

    private readonly System.Reflection.Assembly AppAssembly;
    private readonly string AppAssemblyName;
    private readonly HashSet<string> AppManifestAssetSet;
    private readonly Inv.EnumArray<Inv.ControlType, Func<Inv.Control, BlazorNode>> RouteArray;
    private readonly BlazorDiv BlazorMaster;
    private readonly BlazorInput BlazorFileInput;
    private readonly Inv.EnumArray<Inv.FontWeight, string> FontWeightArray;
    private readonly List<Action> PostList; // not distinct list because the same action is allowed to be posted multiple times.
    private readonly Inv.KeyModifier LastKeyModifier;
    private DirectoryFilePicker ActiveFilePicker;
  }

  internal abstract class BlazorNode
  {
    public BlazorElement LayoutElement { get; set; }
    public BlazorElement PanelElement { get; set; }
  }

  internal sealed class BlazorNode<T> : BlazorNode
    where T : BlazorElement
  {
    public new T PanelElement
    {
      get => (T)base.PanelElement;
      set => base.PanelElement = value;
    }
  }

  internal sealed class BlazorSurface
  {
    public BlazorSurface(BlazorDocument BlazorDocument)
    {
      this.Base = new BlazorDiv(BlazorDocument);
      Base.Class = "surface";
    }

    public void SetContent(BlazorElement BlazorElement)
    {
      if (Content != BlazorElement)
      {
        if (Content != null)
          Base.RemoveElement(Content);

        this.Content = BlazorElement;

        if (Content != null)
          Base.AddElement(Content);
      }
    }

    public static implicit operator BlazorDiv(BlazorSurface Surface) => Surface?.Base;

    private readonly BlazorDiv Base;
    private BlazorElement Content;
  }
}