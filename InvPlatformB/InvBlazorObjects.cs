﻿#if DEBUG
//#define LOGGING
#endif
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.JSInterop;
using Inv.Support;

namespace Inv
{
  internal sealed class BlazorEnvironment
  {
    internal BlazorEnvironment(IJSInProcessRuntime JSInProcessRuntime)
    {
      System.Diagnostics.Debug.Assert(JSInProcessRuntime != null);

      this.JSInProcessRuntime = JSInProcessRuntime;
    }

    internal void InvokeVoid(string Method, params object[] ArgumentArray)
    {
      Log(Method, ArgumentArray);

      JSInProcessRuntime.InvokeVoid(Method, ArgumentArray);
    }
    internal T Invoke<T>(string Method, params object[] ArgumentArray)
    {
      Log(Method, ArgumentArray);

      return JSInProcessRuntime.Invoke<T>(Method, ArgumentArray);
    }

    [Conditional("LOGGING")]
    internal void Log(string Method, params object[] ArgumentArray)
    {
      Console.WriteLine($"JS-INTEROP: {GetType().Name}.{Method}({ArgumentArray.Select(A => A.ToString()).AsSeparatedText(", ")});");
    }

    private readonly IJSInProcessRuntime JSInProcessRuntime;
  }

  internal abstract class BlazorObject
  {
    internal BlazorObject(BlazorEnvironment Environment, IJSInProcessObjectReference JSHandle, string Prefix = "")
    {
      System.Diagnostics.Debug.Assert(Environment != null);
      System.Diagnostics.Debug.Assert(JSHandle != null);

      this.Environment = Environment;
      this.JSHandle = JSHandle;
      this.Prefix = Prefix;
      this.Style = new BlazorObjectStyle(this);
    }

    public BlazorEnvironment Environment { get; }
    public BlazorObjectStyle Style { get; }

    internal void AddEvent(string Name, string Method)
    {
      Environment.InvokeVoid("addEvent", JSHandle, Name, DotNetObjectReference.Create(this), Method);
    }
    internal void AddChangeEvent(string Name, string Method)
    {
      Environment.InvokeVoid("addChangeEvent", JSHandle, Name, DotNetObjectReference.Create(this), Method);
    }
    internal void AddMouseEvent(string Name, string Method)
    {
      Environment.InvokeVoid("addMouseEvent", JSHandle, Name, DotNetObjectReference.Create(this), Method);
    }
    internal void AddMouseWheelEvent(string Name, string Method)
    {
      Environment.InvokeVoid("addMouseWheelEvent", JSHandle, Name, DotNetObjectReference.Create(this), Method);
    }
    internal void AddTouchEvent(string Name, string Method)
    {
      Environment.InvokeVoid("addTouchEvent", JSHandle, Name, DotNetObjectReference.Create(this), Method);
    }
    internal void AddKeyEvent(string Name, string Method)
    {
      Environment.InvokeVoid("addKeyEvent", JSHandle, Name, DotNetObjectReference.Create(this), Method);
    }
    internal void AddInputFileEvent(string Method)
    {
      Environment.InvokeVoid("addInputFileEvent", JSHandle, DotNetObjectReference.Create(this), Method);
    }
    internal void AddKeyModifierEvent(string Method)
    {
      Environment.InvokeVoid("addKeyModifierEvent", JSHandle, DotNetObjectReference.Create(this), Method);
    }
    internal void AddMouseGestureEvent(string BackwardMethod, string ForwardMethod)
    {
      Environment.InvokeVoid("addMouseGestureEvent", JSHandle, DotNetObjectReference.Create(this), BackwardMethod, ForwardMethod);
    }
    [Obsolete("JavaScript does not support removing all event handlers for an event", error: true)]
    internal void RemoveEvent(string Name)
    {
      //Environment.InvokeVoid("removeEvent", JSHandle, Name);
    }

    internal void InvokeVoid(string Method, params object[] ArgumentArray)
    {
      Log(Method, ArgumentArray);

      JSHandle.InvokeVoid(Prefix + Method, ArgumentArray);
    }
    internal T Invoke<T>(string Method, params object[] ArgumentArray)
    {
      Log(Method, ArgumentArray);

      return JSHandle.Invoke<T>(Prefix + Method, ArgumentArray);
    }
    internal T GetField<T>(string Name)
    {
      return Environment.Invoke<T>("getField", JSHandle, Name);
    }
    internal void SetField<T>(string Name, T Value)
    {
      Environment.InvokeVoid("setField", JSHandle, Name, Value);
    }
    internal IJSInProcessObjectReference GetJSHandle() => JSHandle; // TODO: can this return 'object', so we don't allow using the reference directly?

    [Conditional("LOGGING")]
    private void Log(string Method, params object[] ArgumentArray)
    {
      Environment.Log($"{Prefix}{Method}", ArgumentArray);
    }

    private readonly string Prefix;
    private readonly IJSInProcessObjectReference JSHandle;
  }

  internal abstract class BlazorElement : BlazorObject
  {
    internal BlazorElement(BlazorDocument Document, IJSInProcessObjectReference Handle, string Prefix = "")
      : base(Document.Window.Environment, Handle, Prefix)
    {
      this.Document = Document;
      this.ComposeElementList = new Inv.DistinctList<BlazorElement>();
      this.AttributeDictionary = new Dictionary<string, string>();
    }

    public BlazorDocument Document { get; }
    public string Id
    {
      get => GetAttribute("id");
      set => SetAttribute("id", value);
    }
    public string Class
    {
      get => GetAttribute("class");
      set => SetAttribute("class", value);
    }
    public Action ResizeEvent
    {
      get => ResizeDelegate;
      set
      {
        if (value == null)
        {
          if (ResizeDelegate != null)
            Document.Environment.InvokeVoid("releaseRegister", GetJSHandle());
        }
        else
        {
          if (ResizeDelegate == null)
            Document.Environment.InvokeVoid("resizeRegister", GetJSHandle(), DotNetObjectReference.Create(this), nameof(ResizeInvoke));
        }

        this.ResizeDelegate = value;
      }
    }

    public void ComposeElements(Inv.DistinctList<BlazorElement> ElementList)
    {
      if (!this.ComposeElementList.ShallowEqualTo(ElementList))
      {
        // TODO: can we do a more sophisticated rearrangement/insertion/deletion algorithm?
        RemoveElements();

        foreach (var Element in ElementList)
          AddElement(Element);

        this.ComposeElementList = ElementList;
      }
    }
    public void AddElement(BlazorElement Element)
    {
      InvokeVoid("appendChild", Element.GetJSHandle());
      ComposeElementList.Add(Element);
    }
    public void RemoveElement(BlazorElement Element)
    {
      InvokeVoid("removeChild", Element.GetJSHandle());
      ComposeElementList.Remove(Element);
    }
    public void RemoveElements()
    {
      if (ComposeElementList.Count > 0)
      {
        Environment.InvokeVoid("removeChildren", GetJSHandle());
        ComposeElementList.Clear();
      }
    }
    public void ScrollToTop()
    {
      Environment.InvokeVoid("scrollToTop", GetJSHandle());
    }
    public void ScrollToBottom()
    {
      Environment.InvokeVoid("scrollToBottom", GetJSHandle());
    }
    public void ScrollToLeft()
    {
      Environment.InvokeVoid("scrollToLeft", GetJSHandle());
    }
    public void ScrollToRight()
    {
      Environment.InvokeVoid("scrollToRight", GetJSHandle());
    }

    public bool HasElement(BlazorElement Element)
    {
      return ComposeElementList.Contains(Element);
    }
    public IEnumerable<BlazorElement> GetElements() => ComposeElementList;
    public Inv.Dimension GetDimension()
    {
      var Result = Environment.Invoke<JSDimension>("getDimension", GetJSHandle());

      return new Inv.Dimension(Result.Width, Result.Height);
    }

    [JSInvokable]
    public void ResizeInvoke() => ResizeDelegate?.Invoke();

    internal string GetAttribute(string Name)
    {
      return AttributeDictionary.GetValueOrDefault(Name) ?? Invoke<string>("getAttribute", Name);
    }
    internal void SetAttribute(string Name, string Value)
    {
      if (Value == null)
      {
        if (AttributeDictionary.Remove(Name))
          InvokeVoid("removeAttribute", Name);
      }
      else if (AttributeDictionary.TryGetValue(Name, out var Current) || Current != Value)
      {
        AttributeDictionary[Name] = Value;
        InvokeVoid("setAttribute", Name, Value);
      }
    }
    internal void ApplyAttribute(string Name, string Value)
    {
      if (Value == null)
        AttributeDictionary.Remove(Name);
      else if (AttributeDictionary.TryGetValue(Name, out var Current) || Current != Value)
        AttributeDictionary[Name] = Value;
    }
    internal void SetText(ref string TextField, string Value)
    {
      if (TextField != Value)
      {
        if (TextField != null)
          Environment.InvokeVoid("removeChildren", GetJSHandle());

        TextField = Value;

        if (TextField != null)
        {
          var FragmentArray = TextField.Split(System.Environment.NewLine);
          for (var FragmentIndex = 0; FragmentIndex < FragmentArray.Length; FragmentIndex++)
          {
            if (FragmentIndex > 0)
              InvokeVoid("appendChild", Document.CreateElement("br"));

            var Fragment = FragmentArray[FragmentIndex];

            if (!string.IsNullOrEmpty(Fragment))
              InvokeVoid("appendChild", Document.CreateTextNode(Fragment));
          }
        }
      }
    }

    private readonly Dictionary<string, string> AttributeDictionary;
    private Inv.DistinctList<BlazorElement> ComposeElementList;
    private Action ResizeDelegate;

    private sealed class JSDimension
    {
      public int Width { get; set; }
      public int Height { get; set; }
    }
  }

  internal sealed class BlazorObjectStyle
  {
    internal BlazorObjectStyle(BlazorObject Object)
    {
      this.Object = Object;
      this.PropertyDictionary = new Dictionary<string, string>();
    }

    public string Position
    {
      get => GetProperty("position");
      set => SetProperty("position", value);
    }
    public string Left
    {
      get => GetProperty("left");
      set => SetProperty("left", value);
    }
    public string Top
    {
      get => GetProperty("top");
      set => SetProperty("top", value);
    }
    public string Width
    {
      get => GetProperty("width");
      set => SetProperty("width", value);
    }
    public string Height
    {
      get => GetProperty("height");
      set => SetProperty("height", value);
    }
    public string MinWidth
    {
      get => GetProperty("min-width");
      set => SetProperty("min-width", value);
    }
    public string MinHeight
    {
      get => GetProperty("min-height");
      set => SetProperty("min-height", value);
    }
    public string MaxWidth
    {
      get => GetProperty("max-width");
      set => SetProperty("max-width", value);
    }
    public string MaxHeight
    {
      get => GetProperty("max-height");
      set => SetProperty("max-height", value);
    }
    public string FlexGrow
    {
      get => GetProperty("flex-grow");
      set => SetProperty("flex-grow", value);
    }
    public string TextAlign
    {
      get => GetProperty("text-align");
      set => SetProperty("text-align", value);
    }
    public string Margin
    {
      get => GetProperty("margin");
      set => SetProperty("margin", value);
    }
    public string BackgroundColor
    {
      get => GetProperty("background-color");
      set => SetProperty("background-color", value);
    }
    public string FontFamily
    {
      get => GetProperty("font-family");
      set => SetProperty("font-family", value);
    }
    public string FontSize
    {
      get => GetProperty("font-size");
      set => SetProperty("font-size", value);
    }
    public string FontWeight
    {
      get => GetProperty("font-weight");
      set => SetProperty("font-weight", value);
    }
    public string FontColor
    {
      get => GetProperty("color");
      set => SetProperty("color", value);
    }
    public string FontStyle
    {
      get => GetProperty("font-style");
      set => SetProperty("font-style", value);
    }
    public string FontVariant
    {
      get => GetProperty("font-variant");
      set => SetProperty("font-variant", value);
    }
    public string TextDecoration
    {
      get => GetProperty("text-decoration");
      set => SetProperty("text-decoration", value);
    }
    public string VerticalAlign
    {
      get => GetProperty("vertical-align");
      set => SetProperty("vertical-align", value);
    }
    public string Opacity
    {
      get => GetProperty("opacity");
      set => SetProperty("opacity", value);
    }
    public string ZIndex
    {
      get => GetProperty("z-index");
      set => SetProperty("z-index", value);
    }
    public string BorderStyle
    {
      get => GetProperty("border-style");
      set => SetProperty("border-style", value);
    }
    public string BorderColor
    {
      get => GetProperty("border-color");
      set => SetProperty("border-color", value);
    }
    public string BorderWidth
    {
      get => GetProperty("border-width");
      set => SetProperty("border-width", value);
    }
    public string BorderRadius
    {
      get => GetProperty("border-radius");
      set => SetProperty("border-radius", value);
    }
    public string ObjectFit
    {
      get => GetProperty("object-fit");
      set => SetProperty("object-fit", value);
    }
    public string Padding
    {
      get => GetProperty("padding");
      set => SetProperty("padding", value);
    }
    public string Visibility
    {
      get => GetProperty("visibility");
      set => SetProperty("visibility", value);
    }
    public string Display
    {
      get => GetProperty("display");
      set => SetProperty("display", value);
    }
    public string Overflow
    {
      get => GetProperty("overflow");
      set => SetProperty("overflow", value);
    }
    public string OverflowX
    {
      get => GetProperty("overflow-x");
      set => SetProperty("overflow-x", value);
    }
    public string OverflowY
    {
      get => GetProperty("overflow-y");
      set => SetProperty("overflow-y", value);
    }
    public string WhiteSpace
    {
      get => GetProperty("white-space");
      set => SetProperty("white-space", value);
    }
    public string TextOverflow
    {
      get => GetProperty("text-overflow");
      set => SetProperty("text-overflow", value);
    }
    public string BoxShadow
    {
      get => GetProperty("box-shadow");
      set => SetProperty("box-shadow", value);
    }
    public string JustifyContent
    {
      get => GetProperty("justify-content");
      set => SetProperty("justify-content", value);
    }
    public string AlignItems
    {
      get => GetProperty("align-items");
      set => SetProperty("align-items", value);
    }
    public string GridTemplateRows
    {
      get => GetProperty("grid-template-rows");
      set => SetProperty("grid-template-rows", value);
    }
    public string GridTemplateColumns
    {
      get => GetProperty("grid-template-columns");
      set => SetProperty("grid-template-columns", value);
    }
    public string GridRow
    {
      get => GetProperty("grid-row");
      set => SetProperty("grid-row", value);
    }
    public string GridColumn
    {
      get => GetProperty("grid-column");
      set => SetProperty("grid-column", value);
    }

    private void RemoveProperty(string Name)
    {
      if (PropertyDictionary.Remove(Name))
        Object.InvokeVoid("style.removeProperty", Name);
    }
    private string GetProperty(string Name)
    {
      return PropertyDictionary.GetValueOrDefault(Name) ?? Object.Invoke<string>("style.getPropertyValue", Name);
    }
    private void SetProperty(string Name, string Value)
    {
      if (Value == null)
      {
        RemoveProperty(Name);
      }
      else if (!PropertyDictionary.TryGetValue(Name, out var Current) || Current != Value)
      {
        PropertyDictionary[Name] = Value;
        Object.InvokeVoid("style.setProperty", Name, Value);
      }
    }

    private readonly BlazorObject Object;
    private readonly Dictionary<string, string> PropertyDictionary;
  }

  internal sealed class BlazorWindow : BlazorObject
  {
    internal BlazorWindow(BlazorEnvironment Environment)
      : base(Environment, Environment.Invoke<IJSInProcessObjectReference>("getWindow"))
    {
      this.Document = new BlazorDocument(this);

      AddEvent("resize", nameof(ResizeInvoke));
      AddEvent("close", nameof(CloseInvoke));
      AddKeyEvent("keydown", nameof(KeyDownInvoke));
      AddKeyEvent("keyup", nameof(KeyUpInvoke));
      AddKeyModifierEvent(nameof(KeyModifierInvoke));
      AddMouseGestureEvent(nameof(MouseBackwardInvoke), nameof(MouseForwardInvoke));
    }

    public BlazorDocument Document { get; }
    public bool IsShiftPressed { get; private set; }
    public bool IsCtrlPressed { get; private set; }
    public bool IsAltPressed { get; private set; }
    public int Width => GetField<int>("innerWidth");
    public int Height => GetField<int>("innerHeight");
    public event Action ResizeEvent;
    public event Action CloseEvent;
    public event Action MouseBackwardEvent;
    public event Action MouseForwardEvent;
    public event Action<BlazorKeystroke> KeyDownEvent;
    public event Action<BlazorKeystroke> KeyUpEvent;
    public event Action<BlazorKeystroke> KeyModifierEvent;

    public void Close()
    {
      Environment.InvokeVoid("messageWindow", "This app has exited.<br/>Close this tab or refresh the page to restart.", "sans-serif");
      
      // the current tab can only be closed if it was originally opened with javascript (so this probably won't work).
      InvokeVoid("close");
    }
    public BlazorTimer SetInterval(int Delay, Action Action)
    {
      var Result = new BlazorTimer(this);
      Result.IntervalEvent += Action;
      Result.IntervalTime = TimeSpan.FromMilliseconds(Delay);
      Result.Start();

      return Result;
    }
    public long PerformanceMemoryBytes()
    {
      var Memory = (int?)null;// Invoke<int?>("performance.memory.totalJSHeapSize");
      
      if (Memory == null)
        return 0L;
      else
        return Memory.Value;
    }
    public BlazorTimer NewTimer()
    {
      return new BlazorTimer(this);
    }

    [JSInvokable]
    public void CloseInvoke() => CloseEvent?.Invoke();
    [JSInvokable]
    public void ResizeInvoke() => ResizeEvent?.Invoke();
    [JSInvokable]
    public void MouseBackwardInvoke() => MouseBackwardEvent?.Invoke();
    [JSInvokable]
    public void MouseForwardInvoke() => MouseForwardEvent?.Invoke();
    [JSInvokable]
    public void KeyModifierInvoke(bool IsCtrl, bool IsAlt, bool IsShift) => KeyModifierEvent?.Invoke(new BlazorKeystroke()
    {
      Key = null,
      Location = 0,
      IsCtrl = IsCtrl,
      IsAlt = IsAlt,
      IsShift = IsShift
    });
    [JSInvokable]
    public void KeyDownInvoke(string Key, int Location, bool IsCtrl, bool IsAlt, bool IsShift) => KeyDownEvent?.Invoke(new BlazorKeystroke()
    {
      Key = Key,
      Location = Location,
      IsCtrl = IsCtrl,
      IsAlt = IsAlt,
      IsShift = IsShift
    });
    [JSInvokable]
    public void KeyUpInvoke(string Key, int Location, bool IsCtrl, bool IsAlt, bool IsShift) => KeyUpEvent?.Invoke(new BlazorKeystroke()
    {
      Key = Key,
      Location = Location,
      IsCtrl = IsCtrl,
      IsAlt = IsAlt,
      IsShift = IsShift
    });
  }

  internal sealed class BlazorTimer
  {
    internal BlazorTimer(BlazorWindow Window)
    {
      this.Window = Window;
      this.IntervalTimeField = TimeSpan.FromSeconds(1);
    }

    public event Action IntervalEvent;
    public TimeSpan IntervalTime
    {
      get => IntervalTimeField;
      set
      {
        if (IntervalTimeField != value)
        {
          this.IntervalTimeField = value;

          if (Handle != 0)
          {
            Stop();
            Start();
          }
        }
      }
    }
    public bool Enabled => Handle != 0;

    public void Start()
    {
      if (this.Handle == 0)
        this.Handle = Window.Environment.Invoke<int>("createTimer", DotNetObjectReference.Create(this), nameof(IntervalInvoke), IntervalTime.TotalMilliseconds);
    }
    public void Stop()
    {
      if (this.Handle != 0)
      {
        Window.InvokeVoid("destroyTimer", Handle);
        this.Handle = 0;
      }
    }

    [JSInvokable]
    public void IntervalInvoke()
    {
      IntervalEvent?.Invoke();
    }

    private readonly BlazorWindow Window;
    private int Handle;
    private TimeSpan IntervalTimeField;
  }

  internal sealed class BlazorKeystroke
  {
    public string Key { get; internal set; }
    public int Location { get; internal set; }
    public bool IsCtrl { get; internal set; }
    public bool IsAlt { get; internal set; }
    public bool IsShift { get; internal set; }
  }

  internal sealed class BlazorDocument : BlazorObject
  {
    internal BlazorDocument(BlazorWindow Window)
      : base(Window.Environment, Window.GetField<IJSInProcessObjectReference>("document"))
    {
      this.Window = Window;
    }

    public string Title
    {
      get => GetField<string>("title");
      set => SetField("title", value);
    }
    public BlazorWindow Window { get; }

    internal IJSInProcessObjectReference CreateTextNode(string Text)
    {
      return Invoke<IJSInProcessObjectReference>("createTextNode", Text);
    }
    internal IJSInProcessObjectReference CreateElement(string Type)
    {
      return Invoke<IJSInProcessObjectReference>("createElement", Type);
    }
  }

  internal sealed class BlazorRender : Inv.DrawContract
  {
    public BlazorRender(BlazorEngine Engine)
    {
      this.Engine = Engine;
      this.BufferCanvas = new BlazorCanvas(Engine.BlazorWindow.Document); // off-screen buffer for custom rendering.
    }

    public void Compose(Inv.Canvas InvCanvas, BlazorPainting BlazorPainting)
    {
      // ensure panel width/height is passed through.
      BlazorPainting.Style.Width = BlazorPainting.Framing.Style.Width;
      BlazorPainting.Style.Width = BlazorPainting.Framing.Style.Height;

      var CanvasDimension = BlazorPainting.Framing.GetDimension();

      var Measure = new Inv.CanvasMeasure(CanvasDimension);
      InvCanvas.MeasureInvoke(Measure);

      var MeasureDimension = Measure.Dimension ?? CanvasDimension;

      // NOTE: this is necessary so that the canvas has a fixed width/height, and can render properly.
      BlazorPainting.Canvas.Style.Position = Measure.Dimension == null ? "absolute" : "relative";
      BlazorPainting.Canvas.Width = MeasureDimension.Width.ToString(System.Globalization.CultureInfo.InvariantCulture);
      BlazorPainting.Canvas.Height = MeasureDimension.Height.ToString(System.Globalization.CultureInfo.InvariantCulture);
      BlazorPainting.Canvas.Bridge.ClearRect(0, 0, MeasureDimension.Width, MeasureDimension.Height);

      this.Bridge = BlazorPainting.Canvas.Bridge;
      InvCanvas.DrawInvoke(this);
    }
    public Inv.Dimension MeasureText(string TextFragment, Inv.DrawFont TextFont, int MaximumTextWidth, int MaximumTextHeight)
    {
      var BlazorFont = TranslateBlazorFont(TextFont);

      BufferCanvas.Bridge.Font = BlazorFont;

      // TODO: pass in MaximumTextWidth/Height.
      var TextWidth = (int)Bridge.MeasureText(TextFragment);
      var TextHeight = (int)Bridge.GetFontHeight(BlazorFont);

      return new Inv.Dimension(TextWidth, TextHeight);
    }

    string TranslateBlazorFont(Inv.DrawFont TextFont)
    {
      return Engine.TranslateFontWeight(TextFont.Weight) + " " + Engine.VerticalPtToPx(TextFont.Size) + "px " + (TextFont.Name.EmptyAsNull() ?? Engine.InvApplication.Device.ProportionalFontName);
    }

    void DrawContract.Mask(Inv.Rect ViewportRect, Action<Inv.DrawMask> MaskAction)
    {
      // TODO: implement.
    }
    void DrawContract.Unmask()
    {
      // TODO: implement.
    }
    void DrawContract.DrawText(string TextFragment, Inv.DrawFont TextFont, Point TextPoint, HorizontalPosition TextHorizontal, VerticalPosition TextVertical, int MaximumTextWidth, int MaximumTextHeight)
    {
      var BlazorX = Engine.HorizontalPtToPx(TextPoint.X);
      var BlazorY = Engine.VerticalPtToPx(TextPoint.Y);
      var BlazorFont = TranslateBlazorFont(TextFont);

      Bridge.Font = BlazorFont;
      Bridge.TextBaseline = "top"; // NOTE: the default textBaseline is "alphabetic", which is not useful for aligning text.
      Bridge.FillStyle = TextFont.Colour.BlazorColour();

      if (TextHorizontal != HorizontalPosition.Left)
      {
        // TODO: pass in MaximumTextWidth/Height.
        var TextWidth = (int)Bridge.MeasureText(TextFragment);

        if (TextHorizontal == HorizontalPosition.Right)
          BlazorX -= TextWidth;
        else if (TextHorizontal == HorizontalPosition.Center)
          BlazorX -= TextWidth / 2;
      }

      if (TextVertical != VerticalPosition.Top)
      {
        var TextHeight = (int)Bridge.GetFontHeight(BlazorFont);

        if (TextVertical == VerticalPosition.Bottom)
          BlazorY -= TextHeight;
        else if (TextVertical == VerticalPosition.Center)
          BlazorY -= TextHeight / 2;
      }

      Bridge.FillText(TextFragment, BlazorX, BlazorY);
    }
    void DrawContract.DrawLine(Colour LineStrokeColour, int LineStrokeThickness, Inv.LineJoin LineJoin, Inv.LineCap LineCap, Inv.Point LineSourcePoint, Point LineTargetPoint, params Point[] LineExtraPointArray)
    {
      if (LineStrokeThickness <= 0 || LineStrokeColour == Inv.Colour.Transparent)
        return;
      
      var StrokeWidth = 0.5F; // LineStrokeThickness / 2.0;

      Bridge.BeginPath();
      Bridge.MoveTo(Engine.HorizontalPtToPx(LineSourcePoint.X) + StrokeWidth, Engine.VerticalPtToPx(LineSourcePoint.Y) + StrokeWidth);
      Bridge.LineTo(Engine.HorizontalPtToPx(LineTargetPoint.X) + StrokeWidth, Engine.VerticalPtToPx(LineTargetPoint.Y) + StrokeWidth);

      foreach (var LineExtraPoint in LineExtraPointArray)
        Bridge.LineTo(Engine.HorizontalPtToPx(LineExtraPoint.X) + StrokeWidth, Engine.VerticalPtToPx(LineExtraPoint.Y) + StrokeWidth);

      Bridge.LineCap = Engine.TranslateLineCap(LineCap);
      Bridge.LineJoin = Engine.TranslateLineJoin(LineJoin);
      Bridge.LineWidth = Engine.VerticalPtToPx(LineStrokeThickness);
      Bridge.StrokeStyle = LineStrokeColour.BlazorColour();
      Bridge.Stroke();
    }
    void DrawContract.DrawRectangle(Colour RectangleFillColour, Colour RectangleStrokeColour, int RectangleStrokeThickness, Inv.Rect RectangleRect)
    {
      var RectangleX = (float)Engine.HorizontalPtToPx(RectangleRect.Left);
      var RectangleY = (float)Engine.VerticalPtToPx(RectangleRect.Top);
      var RectangleWidth = Engine.HorizontalPtToPx(RectangleRect.Width);
      var RectangleHeight = Engine.VerticalPtToPx(RectangleRect.Height);

      var StrokeWidth = RectangleStrokeThickness / 2.0F;
      RectangleX += StrokeWidth;
      RectangleY += StrokeWidth;

      if (RectangleWidth >= RectangleStrokeThickness)
        RectangleWidth -= RectangleStrokeThickness;

      if (RectangleHeight >= RectangleStrokeThickness)
        RectangleHeight -= RectangleStrokeThickness;

      if (RectangleFillColour != null)
      {
        Bridge.FillStyle = RectangleFillColour.BlazorColour();
        Bridge.FillRect(RectangleX, RectangleY, RectangleWidth, RectangleHeight);
      }

      if (RectangleStrokeColour != null && RectangleStrokeThickness > 0)
      {
        Bridge.LineWidth = Engine.VerticalPtToPx(RectangleStrokeThickness);
        Bridge.StrokeStyle = RectangleStrokeColour.BlazorColour();
        Bridge.StrokeRect(RectangleX, RectangleY, RectangleWidth, RectangleHeight);
      }
    }
    void DrawContract.DrawArc(Colour ArcFillColour, Inv.Colour ArcStrokeColour, int ArcStrokeThickness, Point ArcCenter, Inv.Point ArcRadius, float ArcStartAngle, float ArcSweepAngle)
    {
      // TODO: DrawArc.
    }
    void DrawContract.DrawEllipse(Colour EllipseFillColour, Inv.Colour EllipseStrokeColour, int EllipseStrokeThickness, Inv.Point EllipseCenter, Point EllipseRadius)
    {
      var EllipseCenterX = Engine.HorizontalPtToPx(EllipseCenter.X);
      var EllipseCenterY = Engine.VerticalPtToPx(EllipseCenter.Y);
      var EllipseRadiusX = Engine.HorizontalPtToPx(EllipseRadius.X);
      var EllipseRadiusY = Engine.VerticalPtToPx(EllipseRadius.Y);

      if (EllipseFillColour != null)
      {
        Bridge.FillStyle = EllipseFillColour.BlazorColour();
        Bridge.FillEllipse(EllipseCenterX, EllipseCenterY, EllipseRadiusX, EllipseRadiusY);
      }

      if (EllipseStrokeColour != null && EllipseStrokeThickness > 0)
      {
        Bridge.LineWidth = Engine.VerticalPtToPx(EllipseStrokeThickness);
        Bridge.StrokeStyle = EllipseStrokeColour.BlazorColour();
        Bridge.StrokeEllipse(EllipseCenterX, EllipseCenterY, EllipseRadiusX, EllipseRadiusY);
      }
    }
    void DrawContract.DrawImage(Inv.Image ImageSource, Inv.Rect ImageRect, float ImageOpacity, Inv.Colour ImageTint, Inv.Mirror? ImageMirror, Inv.Rotation? ImageRotation)
    {
      if (ImageSource == null)
        return;

      var BlazorImage = Engine.TranslateCanvasImage(ImageSource);
     
      var ImageX = Engine.HorizontalPtToPx(ImageRect.Left);
      var ImageY = Engine.VerticalPtToPx(ImageRect.Top);
      var ImageWidth = Engine.HorizontalPtToPx(ImageRect.Width);
      var ImageHeight = Engine.VerticalPtToPx(ImageRect.Height);

      if (ImageWidth <= 0 || ImageHeight <= 0)
        return;

      var Transformed = ImageOpacity != 1.0F || ImageMirror != null || ImageTint != null || ImageRotation != null;

      if (Transformed)
      {
        Bridge.Save();

        if (ImageOpacity != 1.0F)
          Bridge.GlobalAlpha = ImageOpacity;

        if (ImageRotation != null)
        {
          var RotationPivot = ImageRotation.Value.LocatePoint(ImageRect);
          var RotationX = Engine.HorizontalPtToPx(RotationPivot.X);
          var RotationY = Engine.VerticalPtToPx(RotationPivot.Y);

          Bridge.Translate(RotationX, RotationY);
          Bridge.Rotate((float)((ImageRotation.Value.Angle * Math.PI / 180)));

          if (ImageMirror == Inv.Mirror.Horizontal)
            Bridge.Scale(-1, +1);
          else if (ImageMirror == Inv.Mirror.Vertical)
            Bridge.Scale(+1, -1);
          else if (ImageMirror == Inv.Mirror.HorizontalAndVertical)
            Bridge.Scale(-1, -1);

          ImageX = -(RotationX - ImageX);
          ImageY = -(RotationY - ImageY);
        }
        else if (ImageMirror != null)
        {
          if (ImageMirror.Value == Inv.Mirror.Horizontal)
          {
            ImageX = -ImageX - ImageWidth;
            Bridge.Scale(-1, +1);
          }
          else if (ImageMirror.Value == Inv.Mirror.Vertical)
          {
            ImageY = -ImageY - ImageHeight;
            Bridge.Scale(+1, -1);
          }
          else if (ImageMirror.Value == Inv.Mirror.HorizontalAndVertical)
          {
            ImageX = -ImageX - ImageWidth;
            ImageY = -ImageY - ImageHeight;
            Bridge.Scale(-1, -1);
          }
        }
      }

      Bridge.DrawImage(ImageX, ImageY, ImageWidth, ImageHeight, BlazorImage);

      if (ImageTint != null)
      {
        BufferCanvas.Width = ImageWidth.ToString(System.Globalization.CultureInfo.InvariantCulture);
        BufferCanvas.Height = ImageHeight.ToString(System.Globalization.CultureInfo.InvariantCulture);

        BufferCanvas.Bridge.Save();

        BufferCanvas.Bridge.FillStyle = ImageTint.BlazorColour();
        BufferCanvas.Bridge.FillRect(0, 0, ImageWidth, ImageHeight);

        BufferCanvas.Bridge.GlobalCompositeOperation = "destination-atop";
        BufferCanvas.Bridge.DrawImage(0, 0, ImageWidth, ImageHeight, BlazorImage);

        BufferCanvas.Bridge.Restore();

        Bridge.DrawCanvas(ImageX, ImageY, ImageWidth, ImageHeight, BufferCanvas);
      }

      if (Transformed)
        Bridge.Restore();
    }
    void DrawContract.DrawPolygon(Colour FillColour, Inv.Colour StrokeColour, int StrokeThickness, Inv.LineJoin LineJoin, bool IsClosed, Inv.Point StartPoint, params Point[] PointArray)
    {
      // TODO: DrawPolygon.
    }

    private readonly BlazorEngine Engine;
    private readonly BlazorCanvas BufferCanvas;
    private BlazorCanvasBridge Bridge;
  }

  internal sealed class BlazorPainting : BlazorElement
  {
    internal BlazorPainting(BlazorDocument Document)
      : base(Document, Document.CreateElement("div"))
    {
      this.Framing = new BlazorDiv(Document);
      AddElement(Framing);
      Framing.Class = "framing";

      this.Canvas = new BlazorCanvas(Document);
      AddElement(Canvas);
      Canvas.Class = "painting";
    }

    public BlazorDiv Framing { get; }
    public BlazorCanvas Canvas { get; }
  }

  internal delegate void BlazorMouseDelegate(int X, int Y, int Button);
  internal delegate void BlazorMouseWheelDelegate(int X, int Y, int Delta);
  internal delegate void BlazorTouchDelegate(int X, int Y);

  internal sealed class BlazorCanvas : BlazorElement
  {
    internal BlazorCanvas(BlazorDocument Document)
      : base(Document, Document.CreateElement("canvas"))
    {
      this.Id = "__can" + (GlobalID++);

      AddMouseEvent("click", nameof(SingleClickInvoke));
      AddMouseEvent("dblclick", nameof(DoubleClickInvoke));
      AddMouseEvent("contextmenu", nameof(ContextClickInvoke));
      AddMouseEvent("mousedown", nameof(MouseDownInvoke));
      AddMouseEvent("mouseup", nameof(MouseUpInvoke));
      AddMouseEvent("mousemove", nameof(MouseMoveInvoke));
      AddMouseWheelEvent("mousewheel", nameof(MouseWheelInvoke));
      AddTouchEvent("touchstart", nameof(TouchStartInvoke));
      AddTouchEvent("touchend", nameof(TouchEndInvoke));
      AddTouchEvent("touchcancel", nameof(TouchCancelInvoke));
      AddTouchEvent("touchmove", nameof(TouchMoveInvoke));

      this.Bridge = new BlazorCanvasBridge(this);
    }

    public BlazorCanvasBridge Bridge { get; }
    public string Width
    {
      get => GetAttribute("width");
      set => SetAttribute("width", value);
    }
    public string Height
    {
      get => GetAttribute("height");
      set => SetAttribute("height", value);
    }
    public event BlazorMouseDelegate SingleClickEvent;
    public event BlazorMouseDelegate DoubleClickEvent;
    public event BlazorMouseDelegate ContextClickEvent;
    public event BlazorMouseDelegate MouseDownEvent;
    public event BlazorMouseDelegate MouseUpEvent;
    public event BlazorMouseDelegate MouseMoveEvent;
    public event BlazorMouseWheelDelegate MouseWheelEvent;
    public event BlazorTouchDelegate TouchStartEvent;
    public event BlazorTouchDelegate TouchEndEvent;
    public event BlazorTouchDelegate TouchCancelEvent;
    public event BlazorTouchDelegate TouchMoveEvent;

    [JSInvokable]
    public void SingleClickInvoke(int X, int Y, int Button) => SingleClickEvent?.Invoke(X, Y, Button);
    [JSInvokable]
    public void ContextClickInvoke(int X, int Y, int Button) => ContextClickEvent?.Invoke(X, Y, Button);
    [JSInvokable]
    public void DoubleClickInvoke(int X, int Y, int Button) => DoubleClickEvent?.Invoke(X, Y, Button);
    [JSInvokable]
    public void MouseDownInvoke(int X, int Y, int Button) => MouseDownEvent?.Invoke(X, Y, Button);
    [JSInvokable]
    public void MouseUpInvoke(int X, int Y, int Button) => MouseUpEvent?.Invoke(X, Y, Button);
    [JSInvokable]
    public void MouseMoveInvoke(int X, int Y, int Button) => MouseMoveEvent?.Invoke(X, Y, Button);
    [JSInvokable]
    public void MouseWheelInvoke(int X, int Y, int Delta) => MouseWheelEvent?.Invoke(X, Y, Delta);
    [JSInvokable]
    public void TouchStartInvoke(int X, int Y) => TouchStartEvent?.Invoke(X, Y);
    [JSInvokable]
    public void TouchEndInvoke(int X, int Y) => TouchEndEvent?.Invoke(X, Y);
    [JSInvokable]
    public void TouchCancelInvoke(int X, int Y) => TouchCancelEvent?.Invoke(X, Y);
    [JSInvokable]
    public void TouchMoveInvoke(int X, int Y) => TouchMoveEvent?.Invoke(X, Y);

    private static int GlobalID;
  }

  internal sealed class BlazorFlow : BlazorElement
  {
    internal BlazorFlow(BlazorDocument Document)
      : base(Document, Document.CreateElement("div"))
    {
      Class = "flow";
    }
  }

  internal sealed class BlazorButton : BlazorElement
  {
    internal BlazorButton(BlazorDocument Document)
      : base(Document, Document.CreateElement("button"))
    {
      AddEvent("click", nameof(ClickInvoke));
      AddEvent("contextmenu", nameof(ContextMenuInvoke));
      AddEvent("mouseenter", nameof(MouseEnterInvoke));
      AddEvent("mouseleave", nameof(MouseLeaveInvoke));
      AddEvent("mousedown", nameof(MouseDownInvoke));
      AddEvent("mouseup", nameof(MouseUpInvoke));
      AddEvent("focusin", nameof(GotFocusInvoke));
      AddEvent("focusout", nameof(LostFocusInvoke));
    }

    public string Type
    {
      get => GetAttribute("type");
      set => SetAttribute("type", value);
    }
    public bool Disabled
    {
      get => GetAttribute("disabled") != null;
      set => SetAttribute("disabled", value ? "" : null);
    }
    public string TabIndex
    {
      get => GetAttribute("tabindex");
      set => SetAttribute("tabindex", value);
    }
    public event Action ClickEvent;
    public event Action ContextMenuEvent;
    public event Action GotFocusEvent;
    public event Action LostFocusEvent;
    public event Action MouseEnterEvent;
    public event Action MouseLeaveEvent;
    public event Action MouseDownEvent;
    public event Action MouseUpEvent;

    // NOTE: has to be public??
    [JSInvokable]
    public void ClickInvoke() => ClickEvent?.Invoke();
    [JSInvokable]
    public void MouseEnterInvoke() => MouseEnterEvent?.Invoke();
    [JSInvokable]
    public void MouseLeaveInvoke() => MouseLeaveEvent?.Invoke();
    [JSInvokable]
    public void MouseDownInvoke() => MouseDownEvent?.Invoke();
    [JSInvokable]
    public void MouseUpInvoke() => MouseUpEvent?.Invoke();
    [JSInvokable]
    public void ContextMenuInvoke() => ContextMenuEvent?.Invoke();
    [JSInvokable]
    public void GotFocusInvoke() => GotFocusEvent?.Invoke();
    [JSInvokable]
    public void LostFocusInvoke() => LostFocusEvent?.Invoke();
  }

  internal sealed class BlazorGraphic : BlazorElement
  {
    internal BlazorGraphic(BlazorDocument Document)
      : base(Document, Document.CreateElement("div"))
    {
      this.Img = new BlazorImg(Document);
      AddElement(Img);
      Img.Class = "image";
    }

    public BlazorImg Img { get; }
  }

  internal sealed class BlazorImg : BlazorElement
  {
    internal BlazorImg(BlazorDocument Document)
      : base(Document, Document.CreateElement("img"))
    {
    }

    public int NaturalWidth
    {
      get => GetField<int>("naturalWidth");
      set => SetField("naturalWidth", value);
    }
    public int NaturalHeight
    {
      get => GetField<int>("naturalHeight");
      set => SetField("naturalHeight", value);
    }
    // TODO: caching this string seems like a waste of memory, but it might prevent a DOM update?
    public string Source
    {
      get => GetAttribute("src");
      set => SetAttribute("src", value);
    }
  }

  internal sealed class BlazorAudio : BlazorElement
  {
    internal BlazorAudio(BlazorDocument Document)
      : base(Document, Document.CreateElement("Audio"))
    {
    }

    public void Play(string Uri)
    {
      InvokeVoid("play", Uri);
    }
  }

  internal sealed class BlazorTextArea : BlazorElement
  {
    internal BlazorTextArea(BlazorDocument Document)
      : base(Document, Document.CreateElement("textarea"))
    {
      //AddNativeHandler("keyup", e => ChangeInvoke());
      AddEvent("change", nameof(ChangeInvoke));
    }

    public string Text
    {
      get => TextField;
      set
      {
        if (TextField != value)
        {
          if (TextField != null)
            Environment.InvokeVoid("removeChildren", GetJSHandle());

          this.TextField = value;

          if (TextField != null)
            InvokeVoid("appendChild", Document.CreateTextNode(TextField));
        }
      }
    }
    public bool IsReadOnly
    {
      get => GetAttribute("readOnly") != null;
      set => SetAttribute("readOnly", value ? "" : null);
    }
    public bool Wrap
    {
      set => SetAttribute("wrap", value ? "on" : "off");
    }
    public event Action ChangeEvent;

    [JSInvokable]
    public void ChangeInvoke() => ChangeEvent?.Invoke();

    private string TextField;
  }

  internal sealed class BlazorInput : BlazorElement
  {
    internal BlazorInput(BlazorDocument Document)
      : base(Document, Document.CreateElement("input"))
    {
      AddChangeEvent("input", nameof(ChangeInvoke));
      AddInputFileEvent(nameof(InputFileInvoke));
    }

    public string Type
    {
      get => GetAttribute("type");
      set => SetAttribute("type", value);
    }
    public string Accept
    {
      get => GetAttribute("accept");
      set => SetAttribute("accept", value);
    }
    public string Text
    {
      get => GetAttribute("value");
      set => SetAttribute("value", value);
    }
    public bool IsReadOnly
    {
      get => GetAttribute("readOnly") != null;
      set => SetAttribute("readOnly", value ? "" : null);
    }
    public event Action<string, string> InputFileEvent;
    public event Action ChangeEvent;

    [JSInvokable]
    public void ChangeInvoke(string Text)
    {
      ApplyAttribute("value", Text);
      ChangeEvent?.Invoke();
    }
    [JSInvokable]
    public void InputFileInvoke(string Name, string Source)
    {
      InputFileEvent?.Invoke(Name, Source);
    }
  }

  internal sealed class BlazorCaption : BlazorElement
  {
    internal BlazorCaption(BlazorDocument Document)
      : base(Document, Document.CreateElement("div"))
    {
      this.Label = new BlazorLabel(Document);
      AddElement(Label);
      Label.Class = "caption";
    }

    public BlazorLabel Label { get; }
  }

  internal sealed class BlazorLabel : BlazorElement
  {
    internal BlazorLabel(BlazorDocument Document)
      : base(Document, Document.CreateElement("label"))
    {
    }

    public string Text
    {
      get => TextField;
      set => SetText(ref TextField, value);
    }

    private string TextField;
  }

  internal sealed class BlazorDiv : BlazorElement
  {
    internal BlazorDiv(BlazorDocument Document)
      : base(Document, Document.CreateElement("div"))
    {
    }
  }

  internal sealed class BlazorSeries : BlazorElement
  {
    internal BlazorSeries(BlazorDocument Document)
      : base(Document, Document.CreateElement("div"))
    {
      this.SlotList = new Inv.DistinctList<BlazorSlot>();
    }

    public string DefaultSlotClass { get; private set; }
    public IReadOnlyList<BlazorSlot> Slots => SlotList;

    public void ComposeSlots(IReadOnlyList<BlazorElement> ElementList)
    {
      while (SlotList.Count > ElementList.Count)
      {
        RemoveElement(SlotList[SlotList.Count - 1]);
        SlotList.RemoveAt(SlotList.Count - 1);
      }

      while (SlotList.Count < ElementList.Count)
      {
        var Slot = new BlazorSlot(Document);
        SlotList.Add(Slot);
        Slot.Class = DefaultSlotClass;
        AddElement(Slot);
      }

      for (var SlotIndex = 0; SlotIndex < SlotList.Count; SlotIndex++)
        SlotList[SlotIndex].Compose(ElementList[SlotIndex]);
    }
    public void UpdateSlotClass(string SlotClass)
    {
      this.DefaultSlotClass = SlotClass;
      foreach (var Slot in SlotList)
        Slot.Class = SlotClass;
    }

    private readonly Inv.DistinctList<BlazorSlot> SlotList;
  }

  internal sealed class BlazorSlot : BlazorElement
  {
    internal BlazorSlot(BlazorDocument Document)
      : base(Document, Document.CreateElement("div"))
    {
    }

    public void Compose(BlazorElement Content)
    {
      if (this.Content != Content)
      {
        if (this.Content != null)
          RemoveElements();

        this.Content = Content;

        if (this.Content != null)
          AddElement(this.Content);
      }
    }

    private BlazorElement Content;
  }

  internal sealed class BlazorIFrame : BlazorElement
  {
    internal BlazorIFrame(BlazorDocument Document)
      : base(Document, Document.CreateElement("iframe"))
    {
    }

    public void Load(Uri Uri, string Html)
    {
      if (Html != null)
      {
        SetAttribute("srcdoc", Html);
      }
      else
      {
        SetAttribute("srcdoc", null);

        if (Uri != null)
          SetAttribute("src", Uri.AbsoluteUri);
        else
          SetAttribute("src", "about:blank");
      }
    }
  }

  internal sealed class BlazorBreak : BlazorElement
  {
    internal BlazorBreak(BlazorDocument Document)
      : base(Document, Document.CreateElement("br"))
    {
    }
  }

  internal sealed class BlazorSpan : BlazorElement
  {
    internal BlazorSpan(BlazorDocument Document)
      : base(Document, Document.CreateElement("span"))
    {
    }

    public string Text
    {
      get => TextField;
      set => SetText(ref TextField, value);
    }

    private string TextField;
  }

  internal sealed class BlazorTable : BlazorElement
  {
    internal BlazorTable(BlazorDocument Document)
      : base(Document, Document.CreateElement("table"))
    {
    }

    public BlazorTableRow AddRow()
    {
      return new BlazorTableRow(Document, Invoke<IJSInProcessObjectReference>("insertRow"));
    }
  }

  internal sealed class BlazorTableRow : BlazorElement
  {
    internal BlazorTableRow(BlazorDocument Document, IJSInProcessObjectReference JSHandle)
      : base(Document, JSHandle)
    {
    }

    public BlazorTableCell AddCell()
    {
      return new BlazorTableCell(Document, Invoke<IJSInProcessObjectReference>("insertCell"));
    }
  }

  internal sealed class BlazorTableCell : BlazorElement
  {
    internal BlazorTableCell(BlazorDocument Document, IJSInProcessObjectReference JSHandle)
      : base(Document, JSHandle)
    {
    }
  }

  internal sealed class BlazorClip
  {
    public BlazorClip()
    {
      this.Id = "_clp" + (++GlobalID).ToString(System.Globalization.CultureInfo.InvariantCulture);
    }

    public string Id { get; }

    private static int GlobalID;
  }

  internal sealed class BlazorSound
  {
    public BlazorSound(string Id)
    {
      this.Id = Id;
    }

    public string Id { get; }
  }
}
