# Invention Blazor

## Background

Blazor can run your client-side C# code directly in the browser, using WebAssembly. Blazor compiles your code to standard MSIL, which is then interpreted by the Mono WASM runtime. The Invention Blazor platform makes use of these fundamentals, but ignores the Razor-style component framework. You can write an Invention app and have it compile and run as a web app with 100% code sharing. You do not have to write a line of html, css or javascript.

> To see this technology in action, run InvStudioB.

## Caveats

The Mono .NET runtime, that has been compiled to WebAssembly, is downloaded to the client's browser along with the application DLLs and any dependencies. Once everything is in the browser, the Mono runtime is bootstrapped, which loads and executes the application DLLs. This payload is currently at least 2.4MB and means the 'time to first paint' may be unacceptable for some web applications. There is no download progress bar either, without hacking the Blazor javascript files.

Currently, Blazor WebAssembly uses interpreted mode to load and run your application. In this mode, the Mono IL interpreter executes your application DLLs inside the browser. The only part of the process that is compiled to WebAssembly is the Mono runtime. This means the performance for some algorithms can be around 20x slower than on .NET framework. When AOT (Ahead Of Time) compilation is supported, this will improve performance but the trade-off is a larger download size.

Blazor is currently locked down to a single application thread. This is an unfortunate limitation because the Mono WASM runtime already supports multiple threads. This is somewhat mitigated by following async/await patterns (much like in JS). That is, single-threaded but switching between tasks. This is not ideal because async/await in Blazor is reportedly about 40% slower. However, for Blazor to be suitable for non-trivial applications, we must wait for Microsoft. After all, it is common to have CPU intensive activities in real applications. The internet speculation is that we will have to wait until at least .NET 7 for multi-threaded Blazor support.

Some of the defects below are due to missing async/await patterns in Invention, which could be used to work around the lack of multi-threaded support in Blazor and/or the lack of synchronous APIs in Javascript. This may be an old school opinion but the casual concurrency of async/await is a dangerous programming pattern. Use of tasks/threads should be carefully considered and crafted. The strong preference is for a correct, functioning application that is a little janky in the UI, over one that runs at a smooth 60FPS but has terrifying and untestable race conditions.

## Defects

* flow virtualisation: currently just a non-virtual item list.

* directory: filesystem idb - https://github.com/ebidel/idb.filesystem.js/ and https://www.html5rocks.com/en/tutorials/file/filesystem// (requires async/await or multi-threaded Blazor).

* visibility: improve dock client hack.

* canvas: img is loaded asynchronously and may not be ready when needed for drawing.

* canvas: rendering is slow when static elements are placed on top of it.

* graphics: tint, resize, pixels get/put (https://stackoverflow.com/questions/49157880/how-to-handle-touch-events-with-html5-canvas) - img not loaded yet! (requires async/await).

* browser: anchors to local targets are interfered with by Blazor.

* image: base64 encoding is inefficient and passing an unmarshalled byte array doesn't seem to work (https://developer.mozilla.org/en-US/docs/Web/API/URL/createObjectURL).

* assets: are currently embedded resources because there is no obvious way to 'copy local files'.

* mobile: touch long press on buttons.

* mobile: implement Android-style canvas touch events - single tap, double tap, long tap.

* mobile: label sizing/wrapping issues on iOS Chrome, iOS Safari.

* mobile: test on Firefox.

* memory leaks: JS references seem to be leaking.

* possibly eliminate unnecessary layout divs: scroll, stack, dock, wrap.

* build automation and publish to azure.

* desirable to have a progress bar for the download of Blazor application assemblies/files.


## Unimplemented APIs

* focus.
* clipboard text, image.
* animation - don't bother unless we have background tasks?
* pickers.
* tooltips - https://www.w3schools.com/howto/howto_css_tooltip.asp
* canvas - draw arc, polygon.
* memo - rich formatting.
* vault.
* haptics.
* switch.
* video.
* shapes.


## Debugging in Visual Studio

* You must not use `DebugType=full`, or the debugger will not work (check the .targets file).
* All referenced projects must be `net90`, or the debugger will not work in those projects.
* Exceptions _do not_ break in the debugger; you need to be diligent and monitor the Output window.