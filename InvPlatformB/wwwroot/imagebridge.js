﻿window.gfx = {
  // images.
  images: {},
  writeImageId: 0,

  registerImage: function (id, src) {
    var img = document.createElement("img");
    img.src = src;

    gfx.images[id] = img;
  },
  releaseImage: function (id) {
    delete gfx.images[id];
  },
  readImagePixels: function (id) {
    var image = gfx.images[id];

    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');
    context.drawImage(image, 0, 0);

    // TODO: img may not be loaded, so natural width/height can't be read.

    return context.getImageData(0, 0, image.naturalWidth, image.naturalHeight).data;
  },
  writeImagePixels: function (d) {
    // TODO: not finished, won't work yet.
    const bytes = Blazor.platform.toUint8Array(d);
    var byteArray = new Uint8Array(bytes.length);
    byteArray.set(new Uint8Array(bytes), 0);

    var canvas = document.createElement('canvas');
    canvas.width = imageWidth;
    canvas.height = imageHeight;

    var context = canvas.getContext('2d');

    var imageData = context.createImageData(imageWidth, imageHeight);

    context.putImageData(imageData, 0, 0);

    var id = '_imx' + (++gfx.writeImageId);

    var img = document.createElement("img");
    img.src = canvas.toDataURL(); // data:image/png;base64, etc

    gfx.images[id] = img;

    return id;
  },
  grayImage: function (id) {
    var image = gfx.images[id];
    var imageWidth = image.naturalWidth;
    var imageHeight = image.naturalHeight;

    // TODO: only works if the img has been loaded.

    if (imageWidth <= 0 || imageHeight <= 0)
      return null;

    var canvas = document.createElement('canvas');
    canvas.width = imageWidth;
    canvas.height = imageHeight;

    var ctx = canvas.getContext('2d');
    ctx.drawImage(image, 0, 0, imageWidth, imageHeight);

    let imgData = ctx.getImageData(0, 0, imageWidth, imageHeight);
    let pixels = imgData.data;
    for (var i = 0; i < pixels.length; i += 4) {
      let lightness = parseInt((pixels[i] + pixels[i + 1] + pixels[i + 2]) / 3);

      pixels[i] = lightness;
      pixels[i + 1] = lightness;
      pixels[i + 2] = lightness;
    }
    ctx.putImageData(imgData, 0, 0);

    return canvas.toDataURL();
  }
};