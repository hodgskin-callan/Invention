﻿// show a full screen message box.
function messageWindow(message, font) {
  // replace the entire document with a message.
  var MainDiv = getMainDiv();
  removeChildren(MainDiv);
  MainDiv.innerHTML = '<div style="display: flex; width: 100%; height: 100%; justify-content: center; align-items: center; padding: 20px; background-color: black;"><div><label style="margin: 0px; color: white; font-size: 20px; font-family: ' + font + '">' + message + '</label></div></div>';
}

GlobalInstance = null;

function initApplication(instance) {
  if (GlobalInstance != null)
    return;

  GlobalInstance = instance;
  window.requestAnimationFrame(loopApplication);
};
function loopApplication(timeStamp) {
  if (GlobalInstance != null) {
    window.requestAnimationFrame(loopApplication);
    GlobalInstance.invokeMethod('ApplicationLoop', timeStamp);
  }
}
function stopApplication(instance) {
  GlobalInstance = null;

  if (audioCtx != null)
    audioCtx.suspend();
};

window.checkKeyModifier = false;

window.addEventListener('load', () => {
  try {
    // Fix up for prefixing
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    window.audioCtx = new AudioContext();

    // NOTE: the audio context starts suspended, and will not resume until the user has interacted - this is due to the Autoplay policy.
    //if (audioCtx.state === 'suspended') { audioCtx.resume(); }
  }
  catch (e) {
    alert('Web Audio API is not supported in this browser');
  }

  // Prevent default touch scrolling.
  document.body.addEventListener('touchmove', function (event) {
    if (GlobalInstance == null)
      return;

    event.preventDefault();
  }, false);

  // TODO: if it bubbles up to the window, stop it from showing the context menu.
  document.addEventListener('contextmenu', (e) => {
    if (GlobalInstance == null)
      return;

    e.preventDefault();
  }, false);

  // protection measures against accidental backward/forward.
  window.addEventListener('beforeunload', (e) => {
    if (GlobalInstance != null) {
      e.preventDefault();
      return e.returnValue = "Are you sure you want to exit?";
    }
  }, false);

  history.pushState(null, document.title, location.href);
  window.addEventListener('popstate', (e) => {
    if (GlobalInstance == null)
      return;

    history.pushState(null, document.title, location.href);
  }, false);

  // detecting key modifier changes when returning to the window.
  window.addEventListener('focus', () => {
    window.checkKeyModifier = true;
  }, false);
}, false);

function getWindow() {
  return window;
}
function triggerClick(control) {
  control.click();
}
function getMainDiv() {
  return document.body.getElementsByClassName('main')[0];
}

function removeChildren(node) {
  while (node.hasChildNodes()) {
    node.removeChild(node.lastChild);
  }
}
function getDimension(node) {
  return { Width: node.offsetWidth, Height: node.offsetHeight };
}

function getField(node, name) {
  return node[name];
}
function setField(node, name, value) {
  node[name] = value;
}

function downloadFile(filename, key) {
  var base64bytes = localStorage.getItem(key);

  if (base64bytes === null)
    return;

  var link = document.createElement('a');
  link.download = filename;
  link.href = "data:application/octet-stream;base64," + base64bytes;
  document.body.appendChild(link); // Needed for Firefox
  link.click();
  document.body.removeChild(link);

  // TODO: there is a more efficient version but it requires InvokeUnmarshalled  : https://www.meziantou.net/generating-and-downloading-a-file-in-a-blazor-webassembly-application.htm
}
