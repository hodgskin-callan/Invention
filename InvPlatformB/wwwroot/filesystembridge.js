﻿// TODO: how to marshal byte[] to keep the base64 conversions closer to the JS code.

window.dfs =
{
  MatchRuleShort: function(str, rule) {
    var escapeRegex = (str) => str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    return new RegExp("^" + rule.split("*").map(escapeRegex).join(".*") + "$").test(str);
  },
  ToBase64: function (u8) {
    return btoa(String.fromCharCode.apply(null, u8));
  },
  FromBase64: function (str) {
    return atob(str).split('').map(function (c) { return c.charCodeAt(0); });
  },
  getItem: function (key) {
    var value = localStorage.getItem(key);

    //console.log('** getItem(' + key + ') => ' + value);

    return value;
  },
  setItem: function (key, value) {
    //console.log('** setItem(' + key + ', ' + value + ')');

    localStorage.setItem(key, value);
  },
  existsItem: function (key) {
    //console.log('** existsItem(' + key + ')');

    return !(localStorage.getItem(key) === null);
  },
  lengthItem: function (key) {
    //console.log('** lengthItem(' + key + ')');

    var value = localStorage.getItem(key);

    if (value === null || value.length < 4)
      return 0;

    var padding = 0;
    if (value[value.length - 1] == '=')
      padding++;

    if (value[value.length - 2] == '=')
      padding++;

    return (value.length / 4 * 3) - padding;
  },
  findItems: function (folder) {
    //console.log('** findItems(' + folder + ')');

    var Result = [];

    for (var i = 0, len = localStorage.length; i < len; ++i)
    {
      var key = localStorage.key(i);

      // key is prefixed by the folder, but is not within a subfolder.
      if (key.startsWith(folder) && key.indexOf('/', folder.length + 1) < 0){
        Result.push(key.substring(folder.length));
      }
    }

    return Result.join('|');
  },
  findContainers: function (folder) {
    //console.log('** findContainers(' + folder + ')');

    var Result = [];

    for (var i = 0, len = localStorage.length; i < len; ++i) {
      var key = localStorage.key(i);

      // key is prefixed by the folder and is not a file.
      if (key.startsWith(folder)) {
        var Index = key.indexOf('/', folder.length + 1);

        if (Index >= 0) {
          Result.push(key.substring(folder.length, Index));
        }
      }
    }

    return Result.join('|');
  },
  deleteItem: function (key) {
    localStorage.removeItem(key);
  },
  copyItem: function (sourcekey, targetkey) {
    // TODO: should this throw an exception if the target item already exists to match typical file systems?

    localStorage.setItem(targetkey, localStorage.getItem(sourcekey));
  },
  moveItem: function (sourcekey, targetkey) {
    // TODO: should this throw an exception if the target item already exists to match typical file systems?

    localStorage.setItem(targetkey, localStorage.getItem(sourcekey));
    localStorage.removeItem(sourcekey);
  },
  replaceItem: function (sourcekey, targetkey) {
    // TODO: should this throw an exception if the source item DOES NOT exist to match typical file systems?

    localStorage.setItem(targetkey, localStorage.getItem(sourcekey));
    localStorage.removeItem(sourcekey);
  }
};