﻿window.sfx =
{
  soundbuffers: {},
  soundhandles: {},
  globalSoundId: 0,
  globalHandleId: 0,

  newSound: function (byteArray) {
    var id = '_snd' + (++sfx.globalSoundId);

    window.audioCtx.decodeAudioData(byteArray.buffer, function (buffer) {
      var command = sfx.soundbuffers[id];
      sfx.soundbuffers[id] = buffer;

      if (command != null) {
        sfx.playSoundBuffer(command.handleId, buffer, command.volume, command.rate, command.pan, command.loop, 0);
      }
    }, function (error) {
      alert('Web Audio API was unable to decode: ' + error);
    });

    return id;
  },
  playSound: function (id, volume, rate, pan, loop, handleId) {
    var buffer = sfx.soundbuffers[id];

    if (buffer instanceof AudioBuffer) {
      // already decoded, just play it.
      sfx.playSoundBuffer(handleId, buffer, volume, rate, pan, loop, 0);
    }
    else if (buffer == null) {
      // TODO: double-plays at exactly the same time, may as well only play once?
      sfx.soundbuffers[id] = { handleId, volume, rate, pan, loop }; // command
    }
  },
  stopSound: function (handleId) {
    var handle = sfx.soundhandles[handleId];

    if (handle.sourceNode != null) {
      handle.sourceNode.stop();
      delete sfx.soundhandles[handleId]; // stop tracking.
    }
  },
  pauseSound: function (handleId) {
    var handle = sfx.soundhandles[handleId];

    if (handle.sourceNode != null && handle.pause == null) {
      handle.pause = window.audioCtx.currentTime - handle.start;
      handle.sourceNode.stop();
    }
  },
  resumeSound: function (handleId) {
    var handle = sfx.soundhandles[handleId];

    if (handle.sourceNode == null && handle.pause != null) {
      sfx.playSoundBuffer(handleId, handle.buffer, handle.volume, handle.rate, handle.pan, handle.loop, handle.pause);
      handle.pause = null;
    }
  },
  modulateSound: function (handleId, volume, rate, pan, loop) {
    var handle = sfx.soundhandles[handleId];

    if (handle != null && handle.sourceNode != null) {
      handle.volume = volume;
      handle.rate = rate;
      handle.pan = pan;
      handle.loop = loop;

      handle.gainNode.gain.value = volume;
      handle.panNode.pan.value = pan;
      handle.sourceNode.playbackRate.value = rate;
      handle.sourceNode.loop = loop;
    }
  },
  playSoundBuffer: function (handleId, buffer, volume, rate, pan, loop, startAt) {
    var sourceNode = window.audioCtx.createBufferSource();
    sourceNode.buffer = buffer;

    var lastNode = sourceNode;

    var gainNode;
    if (handleId != null || volume != 1) {
      gainNode = window.audioCtx.createGain()
      gainNode.gain.value = volume;
      lastNode.connect(gainNode);
      lastNode = gainNode;
    }

    var panNode;
    if (handleId != null || pan != 0) {
      panNode = window.audioCtx.createStereoPanner();
      panNode.pan.value = pan;
      lastNode.connect(panNode);
      lastNode = panNode;
    }

    if (handleId != null || rate != 1) {
      sourceNode.playbackRate.value = rate;
    }

    lastNode.connect(window.audioCtx.destination);

    sourceNode.loop = loop;
    sourceNode.start(0, startAt);

    if (handleId != null) {
      sourceNode.onended = () => {
        var handle = sfx.soundhandles[handleId];
        if (handle != null) {
          handle.sourceNode = null;
          handle.gainNode = null;
          handle.panNode = null;
        }
      };
      sfx.soundhandles[handleId] = { sourceNode, gainNode, panNode, buffer, volume, rate, pan, loop, start: window.audioCtx.currentTime - startAt };
    }
  }
};