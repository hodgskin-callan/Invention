﻿window.c2d = {
  // contexts.
  contexts: {},

  // the only marshalled call.
  registerContext: function (canvas) {
    // TODO: If your application uses canvas and doesn’t need a transparent backdrop, set the alpha option to false when creating a drawing context with HTMLCanvasElement.getContext().
    // This information can be used internally by the browser to optimize rendering.
    var dc = canvas.getContext("2d"/*, { alpha: false }*/);
    c2d.contexts[canvas.id] = dc;
  },
  // TODO: we don't have a natural place to call releaseContext?
  releaseContext: function (id) {
    delete c2d.contexts[id];
  },
  // NOTE: this is effectively a private helper method.
  getContext: function (id) {
    var dc = c2d.contexts[id];
    // TODO: throw exception if null.
    return dc;
  },

  // drawing methods.
  getFillStyle: function (d) {
    var dc = c2d.getContext(d);
    return dc.fillStyle;
  },
  setFillStyle: function (d, fs) {
    var dc = c2d.getContext(d);
    dc.fillStyle = fs;
  },

  getStrokeStyle: function (d) {
    var dc = c2d.getContext(d);
    return dc.strokeStyle;
  },
  setStrokeStyle: function (d, ss) {
    var dc = c2d.getContext(d);
    dc.strokeStyle = ss;
  },

  getFont: function (d) {
    var dc = c2d.getContext(d);
    return dc.font;
  },
  setFont: function (d, f) {
    var dc = c2d.getContext(d);
    dc.font = f;
  },

  getTextAlign: function (d) {
    var dc = c2d.getContext(d);
    return dc.textAlign;
  },
  setTextAlign: function (d, a) {
    var dc = c2d.getContext(d);
    dc.textAlign = a;
  },

  getTextBaseline: function (d) {
    var dc = c2d.getContext(d);
    return dc.textBaseline;
  },
  setTextBaseline: function (d, b) {
    var dc = c2d.getContext(d);
    dc.textBaseline = b;
  },

  getLineWidth: function (d) {
    var dc = c2d.getContext(d);
    return dc.lineWidth.toString();
  },
  setLineWidth: function (d, w) {
    var dc = c2d.getContext(d);
    dc.lineWidth = w;
  },

  getLineCap: function (d) {
    var dc = c2d.getContext(d);
    return dc.lineCap;
  },
  setLineCap: function (d, a) {
    var dc = c2d.getContext(d);
    dc.lineCap = a;
  },

  getLineJoin: function (d) {
    var dc = c2d.getContext(d);
    return dc.lineJoin;
  },
  setLineJoin: function (d, a) {
    var dc = c2d.getContext(d);
    dc.lineJoin = a;
  },

  getMiterLimit: function (d) {
    var dc = c2d.getContext(d);
    return dc.miterLimit.toString();
  },
  setMiterLimit: function (d, a) {
    var dc = c2d.getContext(d);
    dc.miterLimit = a;
  },

  getGlobalAlpha: function (d) {
    var dc = c2d.getContext(d);
    return dc.globalAlpha.toString();
  },
  setGlobalAlpha: function (d, a) {
    var dc = c2d.getContext(d);
    dc.globalAlpha = a;
  },

  getGlobalCompositeOperation: function (d) {
    var dc = c2d.getContext(d);
    return dc.globalCompositeOperation;
  },
  setGlobalCompositeOperation: function (d, c) {
    var dc = c2d.getContext(d);
    dc.globalCompositeOperation = c;
  },

  getImageSmoothingEnabled: function (d) {
    var dc = c2d.getContext(d);
    return dc.imageSmoothingEnabled;
  },
  setImageSmoothingEnabled: function (d, s) {
    var dc = c2d.getContext(d);
    dc.imageSmoothingEnabled = s !== 0;
  },

  getShadowBlur: function (d) {
    var dc = c2d.getContext(d);
    return dc.shadowBlur.toString();
  },
  setShadowBlur: function (d, a) {
    var dc = c2d.getContext(d);
    dc.shadowBlur = a;
  },

  getShadowColor: function (d) {
    var dc = c2d.getContext(d);
    return dc.shadowColor;
  },
  setShadowColor: function (d, a) {
    var dc = c2d.getContext(d);
    dc.shadowColor = a;
  },

  getShadowOffsetX: function (d) {
    var dc = c2d.getContext(d);
    return dc.shadowOffsetX.toString();
  },
  setShadowOffsetX: function (d, a) {
    var dc = c2d.getContext(d);
    dc.shadowOffsetX = a;
  },

  getShadowOffsetY: function (d) {
    var dc = c2d.getContext(d);
    return dc.shadowOffsetY.toString();
  },
  setShadowOffsetY: function (d, a) {
    var dc = c2d.getContext(d);
    dc.shadowOffsetY = a;
  },

  clearRect: function (d, x, y, w, h) {
    var dc = c2d.getContext(d);
    dc.clearRect(x, y, w, h);
  },
  fillRect: function (d, x, y, w, h) {
    var dc = c2d.getContext(d);
    dc.fillRect(x, y, w, h);
  },
  strokeRect: function (d, x, y, w, h) {
    var dc = c2d.getContext(d);
    dc.strokeRect(x, y, w, h);
  },

  fillEllipse: function (d, centerx, centery, radiusx, radiusy) {
    var dc = c2d.getContext(d);

    dc.beginPath();
    dc.ellipse(centerx, centery, radiusx, radiusy, 0, 0, Math.PI * 2);
    dc.fill();
  },
  strokeEllipse: function (d, centerx, centery, radiusx, radiusy) {
    var dc = c2d.getContext(d);

    dc.beginPath();
    dc.ellipse(centerx, centery, radiusx, radiusy, 0, 0, Math.PI * 2);
    dc.stroke();
  },

  fillText: function (d, t, x, y) {
    var dc = c2d.getContext(d);
    dc.fillText(t, x, y);
  },
  fillText1: function (d, t, x, y, w) {
    var dc = c2d.getContext(d);
    dc.fillText(t, x, y, w);
  },
  strokeText: function (d, t, x, y) {
    var dc = c2d.getContext(d);
    dc.strokeText(t, x, y);
  },
  strokeText1: function (d, t, x, y, w) {
    var dc = c2d.getContext(d);
    dc.strokeText(t, x, y, w);
  },
  measureText: function (d, t) {
    var dc = c2d.getContext(d);
    var m = dc.measureText(t);
    return m.width.toString();
  },

  getLineDash: function (d) {
    var dc = c2d.getContext(d);
    var ds = dc.getLineDash();
    return ds.toString();
  },
  setLineDash: function (d, a) {
    var dc = c2d.getContext(d);
    var cnt = a.length;
    var ja = [];
    for (var ind = 0; ind < cnt; ind++) {
      var aiv = a[ind];
      ja.push(aiv);
    }
    dc.setLineDash(ja);
  },

  beginPath: function (d) {
    var dc = c2d.getContext(d);
    dc.beginPath();
  },
  closePath: function (d) {
    var dc = c2d.getContext(d);
    dc.closePath();
  },

  isPointInPath: function (d, x, y, e) {
    var dc = c2d.getContext(d);
    return dc.isPointInPath(x, y, e === 0 ? "nonzero" : "evenodd");
  },
  isPointInStroke: function (d, x, y) {
    var dc = c2d.getContext(d);
    return dc.isPointInStroke(x, y);
  },

  moveTo: function (d, x, y) {
    var dc = c2d.getContext(d);
    dc.moveTo(x, y);
  },
  lineTo: function (d, x, y) {
    var dc = c2d.getContext(d);
    dc.lineTo(x, y);
  },
  bezierCurveTo: function (d, cp1X, cp1Y, cp2X, cp2Y, x, y) {
    var dc = c2d.getContext(d);
    dc.bezierCurveTo(cp1X, cp1Y, cp2X, cp2Y, x, y);
  },
  quadraticCurveTo: function (d, cpX, cpY, x, y) {
    var dc = c2d.getContext(d);
    dc.quadraticCurveTo(cpX, cpY, x, y);
  },
  arc: function (d, x, y, r, s, e, a) {
    var dc = c2d.getContext(d);
    dc.arc(x, y, r, s, e, a !== 0);
  },
  arcTo: function (d, x1, y1, x2, y2, r) {
    var dc = c2d.getContext(d);
    dc.arcTo(x1, y1, x2, y2, r);
  },
  rect: function (d, x, y, w, h) {
    var dc = c2d.getContext(d);
    dc.rect(x, y, w, h);
  },
  ellipse: function (d, x, y, rx, ry, ro, s, e, a) {
    var dc = c2d.getContext(d);
    dc.ellipse(x, y, rx, ry, ro, s, e, a !== 0);
  },

  fill: function (d) {
    var dc = c2d.getContext(d);
    dc.fill();
  },
  stroke: function (d) {
    var dc = c2d.getContext(d);
    dc.stroke();
  },
  clip: function (d) {
    var dc = c2d.getContext(d);
    dc.clip();
  },

  rotate: function (d, a) {
    var dc = c2d.getContext(d);
    dc.rotate(a);
  },
  rotateAt: function (d, x, y, a) {
    var dc = c2d.getContext(d);
    dc.translate(x, y);
    dc.rotate(a);
    dc.translate(-x, -y);
  },
  scale: function (d, x, y) {
    var dc = c2d.getContext(d);
    dc.scale(x, y);
  },
  translate: function (d, x, y) {
    var dc = c2d.getContext(d);
    dc.translate(x, y);
  },
  transform: function (d, m11, m12, m21, m22, dx, dy) {
    var dc = c2d.getContext(d);
    dc.transform(m11, m12, m21, m22, dx, dy);
  },
  setTransform: function (d, m11, m12, m21, m22, dx, dy) {
    var dc = c2d.getContext(d);
    dc.setTransform(m11, m12, m21, m22, dx, dy);
  },
  resetTransform: function (d) {
    var dc = c2d.getContext(d);
    dc.setTransform(1, 0, 0, 1, 0, 0);
  },
  drawImage: function (d, id, x, y, width, height) {
    var dc = c2d.getContext(d);
    dc.drawImage(gfx.images[id], x, y, width, height);
  },
  drawCanvas: function (d, id, x, y, width, height) {
    var dc = c2d.getContext(d);
    dc.drawImage(c2d.contexts[id].canvas, x, y, width, height);
  },

  save: function (d) {
    var dc = c2d.getContext(d);
    dc.save();
  },
  restore: function (d) {
    var dc = c2d.getContext(d);
    dc.restore();
  },

  fontheights: {},

  getFontHeight: function (d, font) {
    //c2d.getContext(d);
    var height = c2d.fontheights[font];

    if (height == null) {
      // Reference: https://videlais.com/2014/03/16/the-many-and-varied-problems-with-measuring-font-height-for-html5-canvas/

      var fontDraw = document.createElement("canvas");
      var ctx = fontDraw.getContext('2d');
      ctx.fillRect(0, 0, fontDraw.width, fontDraw.height);
      ctx.textBaseline = 'top';
      ctx.fillStyle = 'white';
      ctx.font = font;
      ctx.fillText('gM', 0, 0);
      var pixels = ctx.getImageData(0, 0, fontDraw.width, fontDraw.height).data;
      var start = -1;
      var end = -1;
      for (var row = 0; row < fontDraw.height; row++) {
        for (var column = 0; column < fontDraw.width; column++) {
          var index = (row * fontDraw.width + column) * 4;
          if (pixels[index] === 0) {
            if (column === fontDraw.width - 1 && start !== -1) {
              end = row;
              row = fontDraw.height;
              break;
            }
            continue;
          }
          else {
            if (start === -1) {
              start = row;
            }
            break;
          }
        }
      }
      height = end - start;
      c2d.fontheights[font] = height;
    }

    return height.toString();
  }
};