﻿function addEvent(node, event, callbackInstance, callbackMethod) {
  node.addEventListener(event, function () {
    if (GlobalInstance == null)
      return;

    callbackInstance.invokeMethod(callbackMethod);
  }, false);
}
function removeEvent(node, event) {
  node['on' + event] = null; // clears all event listeners?
}

function addTouchEvent(node, event, callbackInstance, callbackMethod) {
  node.addEventListener(event, function (e) {
    if (GlobalInstance == null)
      return;

    var touch = e.changedTouches[0];

    var rect = e.target.getBoundingClientRect();

    // NOTE: this calculates the offsetXY of touch event which should be identical to offsetXY of mouse event:
    callbackInstance.invokeMethod(callbackMethod, Math.trunc(touch.pageX - rect.left), Math.trunc(touch.pageY - rect.top));
    e.preventDefault();
  }, false);
}

const BackMouseButton = 3;
const ForwardMouseButton = 4;

function addMouseEvent(node, event, callbackInstance, callbackMethod) {
  node.addEventListener(event, function (e) {
    if (GlobalInstance == null)
      return;

    // ignore back/forward buttons.
    if (e.button == BackMouseButton || e.button == ForwardMouseButton)
      return;

    callbackInstance.invokeMethod(callbackMethod, e.offsetX, e.offsetY, e.button);
    e.preventDefault();
  }, false);
}
function addMouseWheelEvent(node, event, callbackInstance, callbackMethod) {
  node.addEventListener(event, function (e) {
    if (GlobalInstance == null)
      return;

    callbackInstance.invokeMethod(callbackMethod, e.offsetX, e.offsetY, e.wheelDelta);
    e.preventDefault();
  }, false);
}
function addMouseGestureEvent(node, callbackInstance, backwardMethod, forwardMethod) {
  document.onmousedown = function (e) {
    if (GlobalInstance == null)
      return;

    if (e.button == BackMouseButton) {
      callbackInstance.invokeMethod(backwardMethod);
      e.preventDefault();
    }
    else if (e.button == ForwardMouseButton) {
      callbackInstance.invokeMethod(forwardMethod);
      e.preventDefault();
    }
  }
}

function addKeyEvent(node, event, callbackInstance, callbackMethod) {
  node.addEventListener(event, function (e) {
    if (GlobalInstance == null)
      return;

    callbackInstance.invokeMethod(callbackMethod, e.key, e.location, e.ctrlKey, e.altKey, e.shiftKey);

    if (e.ctrlKey && e.key == 's')
      return; // TODO: allow Ctrl+S to keep working for now, because it's useful for debugging.

    // prevent function keys.
    if (e.key.startsWith('F') && e.key.length >= 2)
      e.preventDefault();
  }, false);
}
function addChangeEvent(node, event, callbackInstance, callbackMethod) {
  node.addEventListener(event, function (e) {
    if (GlobalInstance == null)
      return;

    callbackInstance.invokeMethod(callbackMethod, e.target.value);
  }, false);
}

function addKeyModifierEvent(node, callbackInstance, callbackMethod) {
  node.addEventListener('mousemove', function (e) {
    if (GlobalInstance == null)
      return;

    if (window.checkKeyModifier) {
      window.checkKeyModifier = false;
      callbackInstance.invokeMethod(callbackMethod, e.ctrlKey, e.altKey, e.shiftKey);
    }
  }, false);
}

function addInputFileEvent(node, callbackInstance, callbackMethod) {
  node.addEventListener("change", function () {
    var file = this.files[0];

    const reader = new FileReader();
    reader.addEventListener("load", () => {
      var url = reader.result;

      callbackInstance.invokeMethod(callbackMethod, file.name, url);
    });
    reader.readAsDataURL(file);
  });
  // TODO: need a way to capture when the user cancels the file picker dialog.
  //node.onblur = function (event) {
  //  var target = event.target || event.srcElement;
  //
  //  if (target.value.length == 0) 
  //    callbackInstance.invokeMethod(callbackMethod, file.name, null);
  //};
}

function createTimer(callbackInstance, callbackMethod, intervalMS) {
  return setInterval(function () { callbackInstance.invokeMethod(callbackMethod); }, intervalMS);
}
function destroyTimer(handle) {
  clearInterval(handle);
}

ResizeObserver = new ResizeObserver(entries => {
  if (GlobalInstance == null)
    return;

  for (let entry of entries) {
    entry.target['onresize']();
  }
});

function resizeRegister(element, callbackInstance, callbackMethod) {
  element['onresize'] = function () {
    if (GlobalInstance == null)
      return;

    callbackInstance.invokeMethod(callbackMethod);
  };
  ResizeObserver.observe(element);
}
function resizeRelease(element) {
  ResizeObserver.unobserve(element);
}

function scrollToTop(node) {
  node.scrollTop = 0;
}
function scrollToBottom(node) {
  node.scrollTop = node.scrollHeight;
}
function scrollToLeft(node) {
  node.scrollLeft = 0;
}
function scrollToRight(node) {
  node.scrollLeft = node.scrollWidth;
}