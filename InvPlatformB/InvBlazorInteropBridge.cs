﻿/*
https://github.com/jhwcn/BlazorUnmarshalledCanvas
MIT License

Copyright (c) 2019 jhwcn

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Inv
{
  /// <summary>
  /// Provide the basic unmarshalled interop invoking methods to a HTML5 canvas.
  /// Use ValueTuple to pass the arguments as struct easily
  /// </summary>
  internal abstract class BlazorInteropBridge
  {
    public BlazorInteropBridge(Inv.BlazorElement Element)
    {
      this.Id = Element.Id;
      this.Environment = Element.Environment;
    }

    public string Id { get; }

    protected void Invoke(string identifier)
    {
      InvokeRet<object>(identifier);
    }
    protected TRes InvokeRet<TRes>(string identifier)
    {
      return Environment.Invoke<TRes>(identifier, Id);
    }
    protected void Invoke<T0>(string identifier, T0 arg0)
    {
      InvokeRet<T0, object>(identifier, arg0);
    }
    protected TRes InvokeRet<T0, TRes>(string identifier, T0 arg0)
    {
      return Environment.Invoke<TRes>(identifier, Id, arg0);
    }
    protected void Invoke<T0, T1>(string identifier, T0 arg0, T1 arg1)
    {
      InvokeRet<T0, T1, object>(identifier, arg0, arg1);
    }
    protected TRes InvokeRet<T0, T1, TRes>(string identifier, T0 arg0, T1 arg1)
    {
      return Environment.Invoke<TRes>(identifier, Id, arg0, arg1);
    }
    protected void Invoke<T0, T1, T2>(string identifier, T0 arg0, T1 arg1, T2 arg2)
    {
      InvokeRet<T0, T1, T2, object>(identifier, arg0, arg1, arg2);
    }
    protected TRes InvokeRet<T0, T1, T2, TRes>(string identifier, T0 arg0, T1 arg1, T2 arg2)
    {
      return Environment.Invoke<TRes>(identifier, Id, arg0, arg1, arg2);
    }
    protected void Invoke<T0, T1, T2, T3>(string identifier, T0 arg0, T1 arg1, T2 arg2, T3 arg3)
    {
      Environment.InvokeVoid(identifier, Id, arg0, arg1, arg2, arg3);
    }
    protected TRes InvokeRet<T0, T1, T2, T3, TRes>(string identifier, T0 arg0, T1 arg1, T2 arg2, T3 arg3)
    {
      return Environment.Invoke<TRes>(identifier, Id, arg0, arg1, arg2, arg3);
    }
    protected void Invoke<T0, T1, T2, T3, T4>(string identifier, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
    {
      InvokeRet<T0, T1, T2, T3, T4, object>(identifier, arg0, arg1, arg2, arg3, arg4);
    }
    protected TRes InvokeRet<T0, T1, T2, T3, T4, TRes>(string identifier, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
    {
      return Environment.Invoke<TRes>(identifier, Id, arg0, arg1, arg2, arg3, arg4);
    }
    protected void Invoke<T0, T1, T2, T3, T4, T5>(string identifier, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
    {
      InvokeRet<T0, T1, T2, T3, T4, T5, object>(identifier, arg0, arg1, arg2, arg3, arg4, arg5);
    }
    protected TRes InvokeRet<T0, T1, T2, T3, T4, T5, TRes>(string identifier, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
    {
      return Environment.Invoke<TRes>(identifier, Id, arg0, arg1, arg2, arg3, arg4, arg5);
    }
    protected void Invoke<T0, T1, T2, T3, T4, T5, T6>(string identifier, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
    {
      InvokeRet<T0, T1, T2, T3, T4, T5, T6, object>(identifier, arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    }
    protected TRes InvokeRet<T0, T1, T2, T3, T4, T5, T6, TRes>(string identifier, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
    {
      var arg = new ArgStruct8<string, T0, T1, T2, T3, T4, T5, T6>()
      {
        Arg0 = Id,
        Arg1 = arg0,
        Arg2 = arg1,
        Arg3 = arg2,
        Arg4 = arg3,
        Arg5 = arg4,
        Arg6 = arg5,
        Arg7 = arg6,
      };
      return Environment.Invoke<TRes>(identifier, arg);
    }
    protected void Invoke<T0, T1, T2, T3, T4, T5, T6, T7>(string identifier, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
    {
      InvokeRet<T0, T1, T2, T3, T4, T5, T6, T7, object>(identifier, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    }
    protected TRes InvokeRet<T0, T1, T2, T3, T4, T5, T6, T7, TRes>(string identifier, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
    {
      var arg = new ArgStruct9<string, T0, T1, T2, T3, T4, T5, T6, T7>()
      {
        Arg0 = Id,
        Arg1 = arg0,
        Arg2 = arg1,
        Arg3 = arg2,
        Arg4 = arg3,
        Arg5 = arg4,
        Arg6 = arg5,
        Arg7 = arg6,
        Arg8 = arg7,
      };
      return Environment.Invoke<TRes>(identifier, arg);
    }

    private readonly BlazorEnvironment Environment;

    private struct ArgStruct8<T0, T1, T2, T3, T4, T5, T6, T7>
    {
      public T0 Arg0;
      public T1 Arg1;
      public T2 Arg2;
      public T3 Arg3;
      public T4 Arg4;
      public T5 Arg5;
      public T6 Arg6;
      public T7 Arg7;
    }
    private struct ArgStruct9<T0, T1, T2, T3, T4, T5, T6, T7, T8>
    {
      public T0 Arg0;
      public T1 Arg1;
      public T2 Arg2;
      public T3 Arg3;
      public T4 Arg4;
      public T5 Arg5;
      public T6 Arg6;
      public T7 Arg7;
      public T8 Arg8;
    }
  }
}