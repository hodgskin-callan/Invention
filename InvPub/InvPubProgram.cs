﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;
using System.Diagnostics;

namespace Inv
{
  public static class BuildProgram
  {
    [STAThread]
    static void Main(string[] args)
    {
      /*
      Inv.Assert.Disable();
        
      var Script = new Inv.BuildScript("Demo Build");
      var Solution = Script.SelectSolution("Demo.sln");
      var Procedure = Script.AddProcedure("Demo App");

      // ANDROID.
      var ATarget = Solution.SelectTarget("DemoA");
      var AProject = Script.SelectProject(@"DemoA\DemoA.csproj");
      var AManifest = AProject.SelectAndroidManifest();

      Procedure.AddTask("Android", T =>
      {
        var Configuration = Inv.BuildConfiguration.Release;
        var Platform = Inv.BuildPlatform.AnyCPU;

        T.CleanAndroidTarget(ATarget, Configuration, Platform);
        T.IncrementAndroidVersion(AManifest);
        T.BuildAndroidTarget(ATarget, Configuration, Platform);
        T.SignAndroidBundle(AProject, Configuration, Platform);
        T.DeployAndroidBundle(AProject, Configuration, Platform, @"C:\Deployment\Demo", AManifest);
      });

      // iOS.
      var ITarget = Solution.SelectTarget("DemoI");
      var IProject = Script.SelectProject(@"DemoI\DemoI.csproj");
      var IManifest = IProject.SelectiOSManifest();

      var Mac = Script.SelectMacServer("CalMacPro", "callanh", "");

      Procedure.AddTask("iOS", T =>
      {
        var Configuration = Inv.BuildConfiguration.Release;
        var Platform = Inv.BuildPlatform.iPhone;

        T.CleaniOSProject(IProject, Configuration, Platform, Mac);
        T.IncrementiOSVersion(IManifest);
        T.BuildiOSTarget(ITarget, Configuration, Platform, Mac);
        T.DeployiOSPackage(IProject, Configuration, Platform, @"C:\Deployment\Demo", IManifest);
        T.DeployiOSdSYM(IProject, Configuration, Platform, @"C:\Deployment\Demo", IManifest);
      });

      // UNIVERSAL WINDOWS.
      var UTarget = Solution.SelectTarget("DemoU");
      var UProject = Script.SelectProject(@"DemoU\DemoU.csproj");
      var UManifest = UProject.SelectUwaManifest();

      Procedure.AddTask("Universal Windows", T =>
      {
        var Configuration = BuildConfiguration.Release;
        var PlatformArray = new[] { BuildPlatform.x86, BuildPlatform.x64, BuildPlatform.ARM };

        T.CleanUwaTarget(UTarget, Configuration, PlatformArray);
        T.IncrementUwaVersion(UManifest);
        T.BuildUwaTarget(UTarget, Configuration, PlatformArray);
        T.DeployUwaPackage(UProject, PlatformArray, @"C:\Deployment\Demo", UManifest);
      });

      BuildShell.Run(Script);   
      */
      BuildShell.Run(new InventionProcedure());
    }
  }

  public sealed class InventionProcedure : Inv.Mimic<Inv.BuildScript>
  {
    public InventionProcedure()
    {
      this.Base = new BuildScript("Invention Build");

      const string InvMountPath = @"C:\Projects\Invention\";
      const string InvManualPath = InvMountPath + @"InvManual\";
      const string InvDeploymentPath = @"C:\Deployment\Inv\";

      var InvMacServer = Base.SelectMacServer("CalMacPro", "callanh", "");

      var InvSolution = Base.SelectSolution(InvMountPath + "Inv.sln");

      var InvManualProcedure = Base.AddProcedure("Invention Manual");
      {
        var InvManualDeployment = InvDeploymentPath + "Invention.Manual";

        var InvManualTimestamp = Base.SelectTimestamp(InvManualPath + @"InvManual\Resources\Texts\Version.txt");
        var InvManualProject = Base.SelectProject(InvManualPath + @"InvManual\InvManual.csproj");

        var InvManualATarget = InvSolution.SelectTarget("Manual", "InvManualA");
        var InvManualAProject = Base.SelectProject(InvManualPath + @"InvManualA\InvManualA.csproj");
        var InvManualAManifest = InvManualAProject.SelectAndroidManifest();

        InvManualProcedure.AddTask("Android", T =>
        {
          var Configuration = BuildConfiguration.Release;
          var Platform = BuildPlatform.AnyCPU;

          T.CleanAndroidTarget(InvManualATarget, Configuration, Platform);
          T.ApplyTimestamp('a', InvManualTimestamp);
          T.GenerateResources(InvManualProject);
          T.IncrementAndroidVersion(InvManualAManifest);
          T.BuildAndroidTarget(InvManualATarget, Configuration, Platform);
          T.SignAndroidBundle(InvManualATarget, Configuration, Platform);
          T.DeployAndroidBundle(InvManualAProject, Configuration, Platform, InvManualDeployment, InvManualAManifest);
        });

        var InvManualITarget = InvSolution.SelectTarget("Manual", "InvManualI");
        var InvManualIProject = Base.SelectProject(InvManualPath + @"InvManualI\InvManualI.csproj");
        var InvManualIManifest = InvManualIProject.SelectiOSManifest();

        InvManualProcedure.AddTask("iOS", T =>
        {
          var Configuration = BuildConfiguration.Release;
          var Platform = BuildPlatform.ARM64;

          T.CleaniOSProject(InvManualIProject, Configuration, Platform, InvMacServer);
          T.ApplyTimestamp('i', InvManualTimestamp);
          T.GenerateResources(InvManualProject);
          T.IncrementiOSVersion(InvManualIManifest);
          T.PublishiOSProject(InvManualIProject, Configuration, Platform, InvMacServer);
          T.DeployiOSPackage(InvManualIProject, Configuration, Platform, InvManualDeployment, InvManualIManifest);
          T.DeployiOSdSYM(InvManualIProject, Configuration, Platform, InvManualDeployment, InvManualIManifest);
        });

        var InvManualUTarget = InvSolution.SelectTarget("Manual", "InvManualU");
        var InvManualUProject = Base.SelectProject(InvManualPath + @"InvManualU\InvManualU.csproj");
        var InvManualUManifest = InvManualUProject.SelectUwaManifest();

        InvManualProcedure.AddTask("Uwa", T =>
        {
          var Configuration = BuildConfiguration.Release;
          var PlatformArray = new[] { BuildPlatform.x86, BuildPlatform.x64, BuildPlatform.ARM };

          T.CleanUwaTarget(InvManualUTarget, Configuration, PlatformArray);
          T.ApplyTimestamp('u', InvManualTimestamp);
          T.GenerateResources(InvManualProject);
          T.IncrementUwaVersion(InvManualUManifest);
          T.BuildUwaTarget(InvManualUTarget, Configuration, PlatformArray);
          T.DeployUwaPackage(InvManualUProject, PlatformArray, InvManualDeployment, InvManualUManifest);
        });

        // TODO: Blazor manual?
      }

      var InvNugetProcedure = Base.AddProcedure("Invention Nuget");
      {
        var InvNugetVersion = Base.SelectNugetVersion(InvMountPath + "InvNugetVersion.txt");

        var InvPlayTarget = InvSolution.SelectTarget("Tool", "InvPlay");
        var InvPlayProject = Base.SelectProject(InvMountPath + @"InvPlay\InvPlay.csproj");

        var InvGenTarget = InvSolution.SelectTarget("Tool", "InvGen");
        var InvGenProject = Base.SelectProject(InvMountPath + @"InvGen\InvGen.csproj");

        var InvLibraryProject = Base.SelectProject(InvMountPath + @"InvLibrary\InvLibrary.csproj");
        var InvPlatformProject = Base.SelectProject(InvMountPath + @"InvPlatform\InvPlatform.csproj");
        var InvPlatformAProject = Base.SelectProject(InvMountPath + @"InvPlatformA\InvPlatformA.csproj");
        var InvPlatformIProject = Base.SelectProject(InvMountPath + @"InvPlatformI\InvPlatformI.csproj");
        var InvPlatformUProject = Base.SelectProject(InvMountPath + @"InvPlatformU\InvPlatformU.csproj");
        var InvPlatformSProject = Base.SelectProject(InvMountPath + @"InvPlatformS\InvPlatformS.csproj");
        var InvPlatformWProject = Base.SelectProject(InvMountPath + @"InvPlatformW\InvPlatformW.csproj");
        var InvPlatformBProject = Base.SelectProject(InvMountPath + @"InvPlatformB\InvPlatformB.csproj");

        var InvWindowsProject = Base.SelectProject(InvMountPath + @"InvWindows\InvWindows.csproj");
        var InvBuildProject = Base.SelectProject(InvMountPath + @"InvBuild\InvBuild.csproj");
        var InvRoslynProject = Base.SelectProject(InvMountPath + @"InvRoslyn\InvRoslyn.csproj");
        var InvOfficeProject = Base.SelectProject(InvMountPath + @"InvOffice\InvOffice.csproj");
        var InvMaterialProject = Base.SelectProject(InvMountPath + @"InvMaterial\InvMaterial.csproj");

        InvNugetProcedure.AddTask("Prep", T =>
        {
          var Configuration = BuildConfiguration.Release;
          var Platform = BuildPlatform.AnyCPU;

          T.IncrementNugetVersion(InvNugetVersion);
          T.WriteNugetAssemblyInfo(InvPlatformAProject, InvNugetVersion);
          T.WriteNugetAssemblyInfo(InvPlatformIProject, InvNugetVersion);
          T.WriteNugetAssemblyInfo(InvPlatformUProject, InvNugetVersion);
          T.WriteNugetAssemblyInfo(InvPlatformBProject, InvNugetVersion);

          T.NugetRestore(InvSolution);

          T.CleanTarget(InvGenTarget, Configuration, Platform);
          T.BuildTarget(InvGenTarget, Configuration, Platform);
          T.CleanTarget(InvPlayTarget, Configuration, Platform);
          T.BuildTarget(InvPlayTarget, Configuration, Platform);
        });

        InvNugetProcedure.AddTask("Pack", T =>
        {
          var Configuration = BuildConfiguration.Release;

          T.PackNugetProject(InvLibraryProject, InvDeploymentPath, InvNugetVersion, Configuration, new[] { BuildPlatform.AnyCPU });
          T.PackNugetProject(InvPlatformProject, InvDeploymentPath, InvNugetVersion, Configuration, new[] { BuildPlatform.AnyCPU });
          T.PackNugetProject(InvPlatformWProject, InvDeploymentPath, InvNugetVersion, Configuration, new[] { BuildPlatform.AnyCPU });
          T.PackNugetProject(InvPlatformAProject, InvDeploymentPath, InvNugetVersion, Configuration, new[] { BuildPlatform.AnyCPU });
          T.PackNugetProject(InvPlatformIProject, InvDeploymentPath, InvNugetVersion, Configuration, new[] { BuildPlatform.AnyCPU }, InvMacServer);
          T.PackNugetProject(InvPlatformUProject, InvDeploymentPath, InvNugetVersion, Configuration, new[] { BuildPlatform.x86 });
          T.PackNugetProject(InvPlatformBProject, InvDeploymentPath, InvNugetVersion, Configuration, new[] { BuildPlatform.AnyCPU });
          T.PackNugetProject(InvPlatformSProject, InvDeploymentPath, InvNugetVersion, Configuration, new[] { BuildPlatform.AnyCPU });
          T.PackNugetProject(InvOfficeProject, InvDeploymentPath, InvNugetVersion, Configuration, new[] { BuildPlatform.AnyCPU });
          T.PackNugetProject(InvMaterialProject, InvDeploymentPath, InvNugetVersion, Configuration, new[] { BuildPlatform.AnyCPU });
          T.PackNugetProject(InvWindowsProject, InvDeploymentPath, InvNugetVersion, Configuration, new[] { BuildPlatform.AnyCPU });
          T.PackNugetProject(InvBuildProject, InvDeploymentPath, InvNugetVersion, Configuration, new[] { BuildPlatform.AnyCPU });
        });

        InvNugetProcedure.AddTask("Push", T =>
        {
          T.PushNugetPackage(InvDeploymentPath, "Invention.Library", InvNugetVersion);
          T.PushNugetPackage(InvDeploymentPath, "Invention.Platform", InvNugetVersion);
          T.PushNugetPackage(InvDeploymentPath, "Invention.Platform.W", InvNugetVersion);
          T.PushNugetPackage(InvDeploymentPath, "Invention.Platform.A", InvNugetVersion);
          T.PushNugetPackage(InvDeploymentPath, "Invention.Platform.I", InvNugetVersion);
          T.PushNugetPackage(InvDeploymentPath, "Invention.Platform.U", InvNugetVersion);
          T.PushNugetPackage(InvDeploymentPath, "Inv.PlatformB", InvNugetVersion); // TODO: can't be named correctly, see the notes in InvPlatformB.csproj.
          T.PushNugetPackage(InvDeploymentPath, "Invention.Platform.S", InvNugetVersion);
          T.PushNugetPackage(InvDeploymentPath, "Invention.Office", InvNugetVersion);
          T.PushNugetPackage(InvDeploymentPath, "Invention.Material", InvNugetVersion);
          T.PushNugetPackage(InvDeploymentPath, "Invention.Windows", InvNugetVersion);
          T.PushNugetPackage(InvDeploymentPath, "Invention.Build", InvNugetVersion);
        });
      }

      var InvExtensionProcedure = Base.AddProcedure("Invention Extension");
      {
        var InvExtensionTarget = InvSolution.SelectTarget("Extension", "InvExtension");
        var InvExtensionProject = Base.SelectProject(InvMountPath + @"InvExtension\InvExtension.csproj");
        var InvExtensionManifest = InvExtensionProject.SelectVsixManifest();

        InvExtensionProcedure.AddTask("Vsix", T =>
        {
          var Configuration = BuildConfiguration.Release;
          var Platform = BuildPlatform.AnyCPU;

          T.CleanTarget(InvExtensionTarget, Configuration, Platform);
          T.IncrementVsixVersion(InvExtensionManifest);
          T.BuildTarget(InvExtensionTarget, Configuration, Platform);
          T.DeployVsixFile(InvMountPath + @"InvExtension\bin\Release\Invention.Extension.vsix", InvDeploymentPath + "Invention.Extension", InvExtensionManifest);
        });
      }
      // TODO: instructions to manually upload to VisualStudioGallery website?
    }
  }
}