﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  internal readonly struct ServerSurfaceTag
  {
    public ServerSurfaceTag(ushort IDValue)
    {
      ID = IDValue;
    }

    public readonly ushort ID;
    public bool IsZero => ID == 0;

    public override bool Equals(object obj) => base.Equals(obj);
    public override int GetHashCode() => ID.GetHashCode();
    public override string ToString() => ID.ToString();

    public static readonly ServerSurfaceTag Zero = new ServerSurfaceTag(0);
  }

  internal readonly struct ServerPanelTag
  {
    public ServerPanelTag(uint IDValue)
    {
      ID = IDValue;
    }

    public readonly uint ID;
    public bool IsZero => ID == 0;

    public override bool Equals(object obj) => base.Equals(obj);
    public override int GetHashCode() => ID.GetHashCode();
    public override string ToString() => ID.ToString();

    public static readonly ServerPanelTag Zero = new ServerPanelTag(0);
  }

  internal readonly struct ServerImageTag
  {
    public ServerImageTag(ushort IDValue)
    {
      ID = IDValue;
    }

    public readonly ushort ID;
    public bool IsZero => ID == 0;

    public override bool Equals(object obj) => base.Equals(obj);
    public override int GetHashCode() => ID.GetHashCode();
    public override string ToString() => ID.ToString();

    public static readonly ServerImageTag Zero = new ServerImageTag(0);
  }

  internal readonly struct ServerSoundTag
  {
    public ServerSoundTag(ushort IDValue)
    {
      ID = IDValue;
    }

    public readonly ushort ID;
    public bool IsZero => ID == 0;

    public override bool Equals(object obj) => base.Equals(obj);
    public override int GetHashCode() => ID.GetHashCode();
    public override string ToString() => ID.ToString();

    public static readonly ServerSoundTag Zero = new ServerSoundTag(0);
  }

  internal readonly struct ServerClipTag
  {
    public ServerClipTag(uint IDValue)
    {
      ID = IDValue;
    }

    public readonly uint ID;
    public bool IsZero => ID == 0;

    public override bool Equals(object obj) => base.Equals(obj);
    public override int GetHashCode() => ID.GetHashCode();
    public override string ToString() => ID.ToString();

    public static readonly ServerClipTag Zero = new ServerClipTag(0);
  }

  internal readonly struct ServerAnimationTag
  {
    public ServerAnimationTag(ushort IDValue)
    {
      ID = IDValue;
    }

    public readonly ushort ID;
    public bool IsZero => ID == 0;

    public override bool Equals(object obj) => base.Equals(obj);
    public override int GetHashCode() => ID.GetHashCode();
    public override string ToString() => ID.ToString();

    public static readonly ServerAnimationTag Zero = new ServerAnimationTag(0);
  }

  internal readonly struct ServerPickerTag
  {
    public ServerPickerTag(ushort IDValue)
    {
      ID = IDValue;
    }

    public readonly ushort ID;
    public bool IsZero => ID == 0;

    public override bool Equals(object obj) => base.Equals(obj);
    public override int GetHashCode() => ID.GetHashCode();
    public override string ToString() => ID.ToString();
  }

  internal sealed class ServerSurface
  {
    public ServerSurface(ServerSurfaceTag Tag)
    {
      this.Tag = Tag;
    }

    public ServerSurfaceTag Tag { get; private set; }
    public bool HasGestureBackward { get; set; }
    public bool HasGestureForward { get; set; }
  }

  internal sealed class ServerImage
  {
    public ServerImage(ServerEngine Engine, ServerImageTag Tag)
    {
      this.Engine = Engine;
      this.Tag = Tag;
    }

    public ServerEngine Engine { get; private set; }
    public ServerImageTag Tag { get; private set; }
  }

  internal sealed class ServerSound
  {
    public ServerSound(ServerEngine Engine, ServerSoundTag Tag)
    {
      this.Engine = Engine;
      this.Tag = Tag;
    }

    public ServerEngine Engine { get; private set; }
    public ServerSoundTag Tag { get; private set; }
  }

  internal sealed class ServerClip
  {
    public ServerClip(ServerEngine Engine, ServerClipTag Tag)
    {
      this.Engine = Engine;
      this.Tag = Tag;
    }

    public ServerEngine Engine { get; private set; }
    public ServerClipTag Tag { get; private set; }
  }

  internal sealed class ServerAnimation
  {
    public ServerAnimation(ServerEngine Engine, ServerAnimationTag Tag)
    {
      this.Engine = Engine;
      this.Tag = Tag;
    }

    public ServerEngine Engine { get; private set; }
    public ServerAnimationTag Tag { get; private set; }
  }

  internal sealed class ServerPicker
  {
    public ServerPicker(ServerPickerTag Tag)
    {
      this.Tag = Tag;
    }

    public ServerPickerTag Tag { get; private set; }
  }

  internal abstract class ServerPanel
  {
    public ServerPanel(ServerPanelTag Tag)
    {
      this.Tag = Tag;
      this.FixtureTag = ServerPanelTag.Zero;
    }

    public ServerPanelTag Tag { get; private set; }
    public bool HasAdjust { get; set; }
    public ServerPanelTag FixtureTag { get; set; }
  }

  internal sealed class ServerBrowser : ServerPanel
  {
    public ServerBrowser(ServerPanelTag Tag)
      : base(Tag)
    {
    }

    public string Html { get; set; }
    public Uri Uri { get; set; }
  }

  internal sealed class ServerButton : ServerPanel
  {
    public ServerButton(ServerPanelTag Tag)
      : base(Tag)
    {
      this.IsEnabled = true;
      this.IsFocusable = false;
    }

    public bool IsEnabled { get; set; }
    public bool IsFocusable { get; set; }
    public string Hint { get; set; }
    public bool HasPress { get; set; }
    public bool HasRelease { get; set; }
  }

  internal sealed class ServerBoard : ServerPanel
  {
    public ServerBoard(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal sealed class ServerDock : ServerPanel
  {
    public ServerDock(ServerPanelTag Tag)
      : base(Tag)
    {
    }

    public Inv.Orientation Orientation { get; set; }
  }

  internal sealed class ServerEdit : ServerPanel
  {
    public ServerEdit(ServerPanelTag Tag)
      : base(Tag)
    {
    }

    public bool HasChange { get; set; }
    public bool HasReturn { get; set; }
    public bool IsReadOnly { get; set; }
    public string Hint { get; set; }
    public string Text { get; set; }
  }

  internal sealed class ServerFlow : ServerPanel
  {
    public ServerFlow(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal sealed class ServerFrame : ServerPanel
  {
    public ServerFrame(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal sealed class ServerGraphic : ServerPanel
  {
    public ServerGraphic(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal sealed class ServerVideo : ServerPanel
  {
    public ServerVideo(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal sealed class ServerBlock : ServerPanel
  {
    public ServerBlock(ServerPanelTag Tag)
      : base(Tag)
    {
      this.LineWrapping = true; // true by default in Inv.Block.
    }

    public Justification? Justification { get; set; }
    public bool LineWrapping { get; set; }
  }

  internal struct ServerBlockSpan
  {
    public BlockStyle Style { get; set; }
    public string Text { get; set; }
    public ServerFont Font { get; set; }
    public Inv.Colour BackgroundColour { get; set; }
  }

  internal sealed class ServerLabel : ServerPanel
  {
    public ServerLabel(ServerPanelTag Tag)
      : base(Tag)
    {
      this.LineWrapping = true; // true by default in Inv.Label.
    }

    public string Text { get; set; }
    public Justification? Justification { get; set; }
    public bool LineWrapping { get; set; }
  }

  internal sealed class ServerMemo : ServerPanel
  {
    public ServerMemo(ServerPanelTag Tag)
      : base(Tag)
    {
    }

    public bool IsReadOnly { get; set; }
    public string Text { get; set; }
    public bool HasChange { get; set; }
  }

  internal sealed class ServerOverlay : ServerPanel
  {
    public ServerOverlay(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal enum RenderMessage
  {
    Invalid,
    Text,
    Image,
    StraightImage,
    OpacityImage,
    Arc,
    Line,
    Rectangle,
    StraightRectangle,
    Ellipse,
    Polygon,
    Mask,
    Unmask
  }

  internal sealed class ServerDrawMask : Inv.DrawMask
  {
    internal ServerDrawMask(Inv.Rect ViewportRect)
    {
      this.ViewportRect = ViewportRect;
      this.ElementList = new Inv.DistinctList<ServerDrawMaskElement>();
    }

    public Inv.Rect ViewportRect { get; }
    public IReadOnlyList<ServerDrawMaskElement> Elements => ElementList;

    void DrawMask.DrawImage(Inv.Rect Rect, Inv.Image Image, float Opacity, Mirror? Mirror)
    {
      ElementList.Add(new ServerDrawMaskElement(Rect, Image, Opacity, Mirror));
    }
    void DrawMask.DrawRectangle(Inv.Rect Rect, Inv.Colour FillColour, int StrokeThickness, Colour StrokeColour)
    {
      ElementList.Add(new ServerDrawMaskElement(Rect, FillColour, StrokeThickness, StrokeColour));
    }

    private readonly Inv.DistinctList<ServerDrawMaskElement> ElementList;
  }

  internal enum ServerDrawMaskType
  {
    Image,
    Rectangle
  }

  internal sealed class ServerDrawMaskElement
  {
    internal ServerDrawMaskElement(Inv.Rect Rect, Inv.Image Image, float Opacity, Inv.Mirror? Mirror)
    {
      this.Type = ServerDrawMaskType.Image;
      this.ImageRect = Rect;
      this.Image = Image;
      this.ImageOpacity = Opacity;
      this.ImageMirror = Mirror;
    }
    internal ServerDrawMaskElement(Inv.Rect Rect, Inv.Colour FillColour, int StrokeThickness, Colour StrokeColour)
    {
      this.Type = ServerDrawMaskType.Rectangle;
      this.RectangleRect = Rect;
      this.RectangleFillColour = FillColour;
      this.RectangleStrokeThickness = StrokeThickness;
      this.RectangleStrokeColour = StrokeColour;
    }

    public ServerDrawMaskType Type { get; }

    public Inv.Rect ImageRect { get; }
    public Inv.Image Image { get; }
    public float ImageOpacity { get; }
    public Inv.Mirror? ImageMirror { get; }

    public Inv.Rect RectangleRect { get; }
    public Inv.Colour RectangleFillColour { get; }
    public int RectangleStrokeThickness { get; }
    public Inv.Colour RectangleStrokeColour { get; }
  }

  internal sealed class ServerCanvas : ServerPanel, DrawContract
  {
    public ServerCanvas(ServerPanelTag Tag, Func<Inv.Image, ServerImageTag> TranslateImageTag)
      : base(Tag)
    {
      this.TranslateImageTag = TranslateImageTag;
      this.TransportSender = new Inv.TransportSender();
      this.CompactWriter = TransportSender.Writer;
      this.LastPacket = new Inv.TransportPacket(Array.Empty<byte>());
    }

    public bool HasMeasure { get; set; }

    public void Begin()
    {
      TransportSender.Reset();
    }
    public Inv.TransportPacket End()
    {
      var Result = TransportSender.ToPacket();

      // TODO: could we optimise this further by checking bytes as we go?
      if (Result.Buffer.Length == LastPacket.Buffer.Length && Result.Buffer.ShallowEqualTo(LastPacket.Buffer))
        return null;

      LastPacket = Result;

      return Result;
    }

    void DrawContract.Mask(Inv.Rect ViewportRect, Action<Inv.DrawMask> MaskAction)
    {
      var ServerDrawMask = new ServerDrawMask(ViewportRect);

      MaskAction?.Invoke(ServerDrawMask);

      CompactWriter.WriteByte((byte)RenderMessage.Mask);
      CompactWriter.WriteInt16((short)ViewportRect.Left);
      CompactWriter.WriteInt16((short)ViewportRect.Top);
      CompactWriter.WriteInt16((short)ViewportRect.Width);
      CompactWriter.WriteInt16((short)ViewportRect.Height);

      CompactWriter.WriteInt16((short)ServerDrawMask.Elements.Count);
      foreach (var Element in ServerDrawMask.Elements)
      {
        CompactWriter.WriteByte((byte)Element.Type);

        switch (Element.Type)
        {
          case ServerDrawMaskType.Image:
            CompactWriter.WriteInt16((short)Element.ImageRect.Left);
            CompactWriter.WriteInt16((short)Element.ImageRect.Top);
            CompactWriter.WriteInt16((short)Element.ImageRect.Width);
            CompactWriter.WriteInt16((short)Element.ImageRect.Height);
            CompactWriter.WriteImageTag(TranslateImageTag(Element.Image));
            CompactWriter.WriteFloat(Element.ImageOpacity);
            CompactWriter.WriteByte(Element.ImageMirror != null ? (byte)Element.ImageMirror.Value : (byte)255);
            break;

          case ServerDrawMaskType.Rectangle:
            CompactWriter.WriteInt16((short)Element.RectangleRect.Left);
            CompactWriter.WriteInt16((short)Element.RectangleRect.Top);
            CompactWriter.WriteInt16((short)Element.RectangleRect.Width);
            CompactWriter.WriteInt16((short)Element.RectangleRect.Height);
            CompactWriter.WriteColour(Element.RectangleFillColour);
            CompactWriter.WriteInt16((short)Element.RectangleStrokeThickness);
            CompactWriter.WriteColour(Element.RectangleStrokeColour);
            break;

          default:
            throw Inv.Support.EnumHelper.UnexpectedValueException(Element.Type);
        }
      }
    }
    void DrawContract.Unmask()
    {
      CompactWriter.WriteByte((byte)RenderMessage.Unmask);
    }
    void DrawContract.DrawText(string TextFragment, Inv.DrawFont TextFont, Inv.Point TextPoint, Inv.HorizontalPosition TextHorizontal, Inv.VerticalPosition TextVertical, int MaximumTextWidth, int MaximumTextHeight)
    {
      CompactWriter.WriteByte((byte)RenderMessage.Text);
      CompactWriter.WriteString(TextFragment);
      CompactWriter.WriteString(TextFont.Name);
      CompactWriter.WriteInt16((short)TextFont.Size);
      CompactWriter.WriteByte((byte)TextFont.Weight);
      CompactWriter.WriteColour(TextFont.Colour);
      CompactWriter.WriteInt16((short)TextPoint.X);
      CompactWriter.WriteInt16((short)TextPoint.Y);
      CompactWriter.WriteByte((byte)TextHorizontal);
      CompactWriter.WriteByte((byte)TextVertical);
      CompactWriter.WriteInt32(MaximumTextWidth);
      CompactWriter.WriteInt32(MaximumTextHeight);
    }
    void DrawContract.DrawLine(Inv.Colour LineStrokeColour, int LineStrokeThickness, Inv.LineJoin LineJoin, Inv.LineCap LineCap, Inv.Point LineSourcePoint, Point LineTargetPoint, params Point[] LineExtraPointArray)
    {
      CompactWriter.WriteByte((byte)RenderMessage.Line);
      CompactWriter.WriteInt16((short)LineStrokeThickness);
      if (LineStrokeThickness > 0)
      {
        CompactWriter.WriteColour(LineStrokeColour);
        CompactWriter.WriteByte((byte)LineJoin);
        CompactWriter.WriteByte((byte)LineCap);
      }
      CompactWriter.WriteInt16((short)LineSourcePoint.X);
      CompactWriter.WriteInt16((short)LineSourcePoint.Y);
      CompactWriter.WriteInt16((short)LineTargetPoint.X);
      CompactWriter.WriteInt16((short)LineTargetPoint.Y);
      CompactWriter.WriteInt16((short)LineExtraPointArray.Length);
      foreach (var LineExtraPoint in LineExtraPointArray)
      {
        CompactWriter.WriteInt16((short)LineExtraPoint.X);
        CompactWriter.WriteInt16((short)LineExtraPoint.Y);
      }
    }
    void DrawContract.DrawRectangle(Inv.Colour RectangleFillColour, Inv.Colour RectangleStrokeColour, int RectangleStrokeThickness, Inv.Rect RectangleRect)
    {
      if (RectangleRect.Width == RectangleRect.Height)
      {
        CompactWriter.WriteByte((byte)RenderMessage.StraightRectangle);
        CompactWriter.WriteColour(RectangleFillColour);
        CompactWriter.WriteInt16((short)RectangleStrokeThickness);
        if (RectangleStrokeThickness > 0)
          CompactWriter.WriteColour(RectangleStrokeColour);
        CompactWriter.WriteInt16((short)RectangleRect.Left);
        CompactWriter.WriteInt16((short)RectangleRect.Top);
        CompactWriter.WriteInt16((short)RectangleRect.Width);
      }
      else
      {
        CompactWriter.WriteByte((byte)RenderMessage.Rectangle);
        CompactWriter.WriteColour(RectangleFillColour);
        CompactWriter.WriteInt16((short)RectangleStrokeThickness);
        if (RectangleStrokeThickness > 0)
          CompactWriter.WriteColour(RectangleStrokeColour);
        CompactWriter.WriteInt16((short)RectangleRect.Left);
        CompactWriter.WriteInt16((short)RectangleRect.Top);
        CompactWriter.WriteInt16((short)RectangleRect.Width);
        CompactWriter.WriteInt16((short)RectangleRect.Height);
      }
    }
    void DrawContract.DrawArc(Inv.Colour ArcFillColour, Inv.Colour ArcStrokeColour, int ArcStrokeThickness, Inv.Point ArcCenter, Inv.Point ArcRadius, float ArcStartAngle, float ArcSweepAngle)
    {
      CompactWriter.WriteByte((byte)RenderMessage.Arc);
      CompactWriter.WriteColour(ArcFillColour);
      CompactWriter.WriteInt16((short)ArcStrokeThickness);
      if (ArcStrokeThickness > 0)
        CompactWriter.WriteColour(ArcStrokeColour);
      CompactWriter.WriteInt16((short)ArcCenter.X);
      CompactWriter.WriteInt16((short)ArcCenter.Y);
      CompactWriter.WriteInt16((short)ArcRadius.X);
      CompactWriter.WriteInt16((short)ArcRadius.Y);
      CompactWriter.WriteFloat(ArcStartAngle);
      CompactWriter.WriteFloat(ArcSweepAngle);
    }
    void DrawContract.DrawEllipse(Inv.Colour EllipseFillColour, Inv.Colour EllipseStrokeColour, int EllipseStrokeThickness, Inv.Point EllipseCenter, Inv.Point EllipseRadius)
    {
      CompactWriter.WriteByte((byte)RenderMessage.Ellipse);
      CompactWriter.WriteColour(EllipseFillColour);
      CompactWriter.WriteInt16((short)EllipseStrokeThickness);
      if (EllipseStrokeThickness > 0)
        CompactWriter.WriteColour(EllipseStrokeColour);
      CompactWriter.WriteInt16((short)EllipseCenter.X);
      CompactWriter.WriteInt16((short)EllipseCenter.Y);
      CompactWriter.WriteInt16((short)EllipseRadius.X);
      CompactWriter.WriteInt16((short)EllipseRadius.Y);
    }
    void DrawContract.DrawImage(Inv.Image ImageSource, Inv.Rect ImageRect, float ImageOpacity, Inv.Colour ImageTint, Inv.Mirror? ImageMirror, Inv.Rotation? ImageRotation)
    {
      if (ImageRect.Width == ImageRect.Height && ImageTint == null && ImageMirror == null && ImageRotation == null)
      {
        if (ImageOpacity == 1.0F)
        {
          CompactWriter.WriteByte((byte)RenderMessage.StraightImage);
          CompactWriter.WriteImageTag(TranslateImageTag(ImageSource));
          CompactWriter.WriteInt16((short)ImageRect.Left);
          CompactWriter.WriteInt16((short)ImageRect.Top);
          CompactWriter.WriteInt16((short)ImageRect.Width);
        }
        else
        {
          CompactWriter.WriteByte((byte)RenderMessage.OpacityImage);
          CompactWriter.WriteImageTag(TranslateImageTag(ImageSource));
          CompactWriter.WriteInt16((short)ImageRect.Left);
          CompactWriter.WriteInt16((short)ImageRect.Top);
          CompactWriter.WriteInt16((short)ImageRect.Width);
          CompactWriter.WriteFloat(ImageOpacity);
        }
      }
      else
      {
        CompactWriter.WriteByte((byte)RenderMessage.Image);
        CompactWriter.WriteImageTag(TranslateImageTag(ImageSource));
        CompactWriter.WriteInt16((short)ImageRect.Left);
        CompactWriter.WriteInt16((short)ImageRect.Top);
        CompactWriter.WriteInt16((short)ImageRect.Width);
        CompactWriter.WriteInt16((short)ImageRect.Height);
        CompactWriter.WriteFloat(ImageOpacity);
        CompactWriter.WriteColour(ImageTint);
        CompactWriter.WriteByte(ImageMirror != null ? (byte)ImageMirror.Value : (byte)255);
        CompactWriter.WriteByte(ImageRotation != null ? (byte)ImageRotation.Value.Pivot : (byte)255);
        if (ImageRotation != null)
        {
          CompactWriter.WriteFloat(ImageRotation.Value.Angle);

          if (ImageRotation.Value.Pivot == Pivot.Custom)
          {
            CompactWriter.WriteInt32(ImageRotation.Value.CustomPoint.Value.X);
            CompactWriter.WriteInt32(ImageRotation.Value.CustomPoint.Value.Y);
          }
        }
      }
    }
    void DrawContract.DrawPolygon(Colour PolygonFillColour, Colour PolygonStrokeColour, int PolygonStrokeThickness, LineJoin PolygonLineJoin, bool PolygonIsClosed, Point PolygonStartPoint, params Point[] PolygonPointArray)
    {
      CompactWriter.WriteByte((byte)RenderMessage.Polygon);
      CompactWriter.WriteColour(PolygonFillColour);
      CompactWriter.WriteInt16((short)PolygonStrokeThickness);
      if (PolygonStrokeThickness > 0)
      {
        CompactWriter.WriteColour(PolygonStrokeColour);
        CompactWriter.WriteByte((byte)PolygonLineJoin);
      }
      CompactWriter.WriteBoolean(PolygonIsClosed);
      CompactWriter.WriteInt16((short)PolygonStartPoint.X);
      CompactWriter.WriteInt16((short)PolygonStartPoint.Y);
      CompactWriter.WriteInt16((short)PolygonPointArray.Length);
      foreach (var PolygonPoint in PolygonPointArray)
      {
        CompactWriter.WriteInt16((short)PolygonPoint.X);
        CompactWriter.WriteInt16((short)PolygonPoint.Y);
      }
    }

    private readonly Inv.TransportSender TransportSender;
    private readonly Inv.CompactWriter CompactWriter;
    private readonly Func<Image, ServerImageTag> TranslateImageTag;
    private Inv.TransportPacket LastPacket;
  }

  internal sealed class ServerScroll : ServerPanel
  {
    public ServerScroll(ServerPanelTag Tag)
      : base(Tag)
    {
    }

    public Inv.Orientation Orientation { get; set; }
  }

  internal sealed class ServerShape : ServerPanel
  {
    public ServerShape(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal enum ServerShapeFigureType
  {
    Polygon,
    Ellipse,
    Line
  }

  internal sealed class ServerShapeFigure
  {
    public ServerShapeFigureType Type { get; set; }
    public Inv.Point[] PathArray { get; set; }
  }

  internal sealed class ServerStack : ServerPanel
  {
    public ServerStack(ServerPanelTag Tag)
      : base(Tag)
    {
    }

    public Inv.Orientation Orientation { get; set; }
  }

  internal sealed class ServerSwitch : ServerPanel
  {
    public ServerSwitch(ServerPanelTag Tag)
      : base(Tag)
    {
      this.IsEnabled = true;
    }

    public bool IsEnabled { get; set; }
    public bool IsOn { get; set; }
    public bool HasChange { get; set; }
  }

  internal sealed class ServerTable : ServerPanel
  {
    public ServerTable(ServerPanelTag Tag)
      : base(Tag)
    {
    }
  }

  internal sealed class ServerWrap : ServerPanel
  {
    public ServerWrap(ServerPanelTag Tag)
      : base(Tag)
    {
    }

    public Inv.Orientation Orientation { get; set; }
  }

  internal sealed class ServerTimer
  {
    internal ServerTimer()
    {
    }

    public event Action IntervalEvent;
    public bool IsEnabled => IsEnabledField;
    public TimeSpan IntervalTime
    {
      get => IntervalTimeField;
      set
      {
        if (IsEnabledField)
        {
          Stop();

          this.IntervalTimeField = value;

          Start();
        }
        else
        {
          this.IntervalTimeField = value;
        }
      }
    }

    public void Start()
    {
      if (!IsEnabledField)
      {
        this.IsEnabledField = true;

        this.Base = new System.Threading.Timer(Sender =>
        {
          IntervalEvent?.Invoke();
        }, state: null, TimeSpan.Zero, IntervalTimeField);
      }
    }
    public void Stop()
    {
      if (IsEnabledField)
      {
        this.IsEnabledField = false;

        if (Base != null)
        {
          Base.Dispose();
          this.Base = null;
        }
      }
    }

    private System.Threading.Timer Base;
    private TimeSpan IntervalTimeField;
    private bool IsEnabledField;
  }

  internal enum ServerFontStyle : byte
  {
    None = 0x00,
    SmallCaps = 0x01,
    Underline = 0x02,
    Strikethrough = 0x04,
    Italics = 0x08,
    NoSmallCaps = 0x10,
    NoUnderline = 0x20,
    NoStrikethrough = 0x40,
    NoItalics = 0x80,
  }

  internal struct ServerFont
  {
    public string Name;
    public int? Size;
    public Inv.Colour Colour;
    public Inv.FontWeight? Weight;
    public Inv.FontAxis? Axis;
    public bool? SmallCaps;
    public bool? Underline;
    public bool? Strikethrough;
    public bool? Italics;
  }

  internal struct ServerFocus
  {
    public bool HasGot;
    public bool HasLost;
  }

  internal struct ServerAnimationTarget
  {
    public ServerPanelTag PanelTag;
    public ServerAnimationTransform[] TransformArray;
  }

  internal struct ServerAnimationTransform
  {
    public AnimationType Type;
    // fade.
    public TimeSpan? FadeOffset;
    public TimeSpan FadeDuration;
    public float FadeFromOpacity;
    public float FadeToOpacity;
    // scale.
    public TimeSpan? ScaleOffset;
    public TimeSpan ScaleDuration;
    public float ScaleFromWidth;
    public float ScaleToWidth;
    public float ScaleFromHeight;
    public float ScaleToHeight;
    // rotate.
    public TimeSpan? RotateOffset;
    public TimeSpan RotateDuration;
    public float RotateFromAngle;
    public float RotateToAngle;
    // translate.
    public TimeSpan? TranslateOffset;
    public TimeSpan TranslateDuration;
    public int? TranslateFromX;
    public int? TranslateToX;
    public int? TranslateFromY;
    public int? TranslateToY;
  }

  internal struct ServerBoardPin
  {
    public Rect Rect;
    public ServerPanelTag PanelTag;
  }

  internal struct ServerTableAxis
  {
    public TableAxisLength LengthType;
    public int LengthValue;
    public ServerPanelTag PanelTag;
  }

  internal struct ServerTableCell
  {
    public int X;
    public int Y;
    public ServerPanelTag PanelTag;
  }

  internal struct ServerFlowSection
  {
    public int ItemCount;
    public ServerPanelTag HeaderPanelTag;
    public ServerPanelTag FooterPanelTag;
  }

  internal struct ServerEmailTo
  {
    public string Name;
    public string Address;
  }

  internal struct ServerEmailAttachment
  {
    public string Name;
    public byte[] Content;
  }
}