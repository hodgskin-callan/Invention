﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inv
{
  /// <summary>
  /// Thread-safe abstraction for sending and receiving packets on a single transport flow (usually a TCP/IP socket).
  /// Packets are sent and received in background tasks.
  /// </summary>
  public sealed class ServerQueue
  {
    public ServerQueue(Inv.TransportFlow Flow, string Name)
    {
      this.Flow = Flow;
      this.Name = Name;

      this.SendCriticalSection = new ExclusiveCriticalSection("TransportQueue-Send-" + Name);
      this.SendReadySignal = new AutoResetSignal(false, $"TransportQueue-Send-{Name}");
      this.SendQueue = new Queue<Inv.TransportPacket>();
      this.SendTask = Inv.TaskGovernor.NewActivity("TransportQueue-Send-" + Name, SendPackets);

      this.ReceiveCriticalSection = new ExclusiveCriticalSection("TransportQueue-Recv-" + Name);
      this.ReceiveReadySignal = new AutoResetSignal(false, $"TransportQueue-Recv-{Name}");
      this.ReceiveQueue = new Queue<Inv.TransportPacket>();
      this.ReceiveTask = Inv.TaskGovernor.NewActivity("TransportQueue-Recv-" + Name, ReceivePackets);
    }

    /// <summary>
    /// Tracks the total number of bytes sent.
    /// </summary>
    public ulong SentBytes { get; private set; }
    /// <summary>
    /// Tracks the total number of bytes received.
    /// </summary>
    public ulong ReceivedBytes { get; private set; }

    /// <summary>
    /// Start the background tasks for sending and receiving packets.
    /// </summary>
    public void Start()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        ReceiveTask.Start();
        SendTask.Start();
      }
    }
    /// <summary>
    /// Stop the background tasks for sending and receiving packets.
    /// </summary>
    public void Stop()
    {
      if (IsActive)
      {
        CancelTasks();

        ReceiveTask.Wait();
        SendTask.Wait();
      }
    }

    /// <summary>
    /// Add the packet to the send queue.
    /// </summary>
    /// <param name="Packet"></param>
    public void SendPacket(Inv.TransportPacket Packet)
    {
      using (SendCriticalSection.Lock())
      {
        SendQueue.Enqueue(Packet);

        SendReadySignal.Set();
      }
    }
    /// <summary>
    /// Waits until there is a packet to remove from the receive queue.
    /// </summary>
    /// <returns></returns>
    public Inv.TransportPacket ReceivePacket()
    {
      if (IsActive && ReceiveReadySignal.WaitOne())
      {
        if (!IsActive)
          return null;

        using (ReceiveCriticalSection.Lock())
        {
          var Result = ReceiveQueue.Dequeue();

          if (ReceiveQueue.Count > 0)
            ReceiveReadySignal.Set();

          return Result;
        }
      }
      else
      {
        return null;
      }
    }
    /// <summary>
    /// Immediately takes the next packet from the receive queue or return null if there are no available packets.
    /// </summary>
    /// <returns></returns>
    public Inv.TransportPacket TryReceivePacket()
    {
      return TryReceivePacket(TimeSpan.Zero);
    }
    /// <summary>
    /// Takes the next packet from the receive queue and will Wait up to the specified time span for one to be received.
    /// Returns null if there are no available packets within the specified time span.
    /// </summary>
    /// <returns></returns>
    public Inv.TransportPacket TryReceivePacket(TimeSpan TimeSpan)
    {
      if (IsActive && ReceiveReadySignal.WaitOne(TimeSpan))
      {
        if (!IsActive)
          return null;

        using (ReceiveCriticalSection.Lock())
        {
          var Result = ReceiveQueue.Dequeue();

          if (ReceiveQueue.Count > 0)
            ReceiveReadySignal.Set();

          return Result;
        }
      }
      else
      {
        return null;
      }
    }

    private void ReceivePackets()
    {
      while (IsActive)
      {
        var Packet = Flow.TryReceivePacket();

        if (Packet == null)
        {
          CancelTasks();
        }
        else
        {
          using (ReceiveCriticalSection.Lock())
          {
            ReceiveQueue.Enqueue(Packet);

            ReceiveReadySignal.Set();
          }

          ReceivedBytes += (uint)Packet.Buffer.Length;
        }
      }
    }
    private void SendPackets()
    {
      while (IsActive)
      {
        while (SendReadySignal.WaitOne() && IsActive)
        {
          Inv.TransportPacket Result;

          using (SendCriticalSection.Lock())
          {
            Result = SendQueue.Dequeue();

            if (SendQueue.Count > 0)
              SendReadySignal.Set();
          }

          if (!Flow.TrySendPacket(Result))
            IsActive = false;
          else
            SentBytes += (uint)Result.Buffer.Length;
        }
      }
    }
    private void CancelTasks()
    {
      this.IsActive = false;

      // wake up the tasks so they can finish.
      ReceiveReadySignal.Set();
      SendReadySignal.Set();
    }

    private readonly Inv.TransportFlow Flow;
    private readonly ExclusiveCriticalSection SendCriticalSection;
    private readonly AutoResetSignal SendReadySignal;
    private readonly Queue<Inv.TransportPacket> SendQueue;
    private readonly Inv.TaskActivity SendTask;
    private readonly ExclusiveCriticalSection ReceiveCriticalSection;
    private readonly AutoResetSignal ReceiveReadySignal;
    private readonly Queue<Inv.TransportPacket> ReceiveQueue;
    private readonly Inv.TaskActivity ReceiveTask;
    private readonly string Name;
    private bool IsActive;
  }

}
