﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Implementation for client-server remote applications.
  /// The server-side process is executing the application.
  /// The client is interacting with a remote projection of the application.
  /// The client interprets a continuous stream of delta updates to the user interface from the server.
  /// </summary>
  public static class ServerShell
  {
    static ServerShell()
    {
      SocketHost = "127.0.0.1";
      SocketPort = 3717;
      FrameDurationMilliseconds = 16;
    }

    public const long ProtocolVersion = 61; // Mask.DrawImage Mirror

    /// <summary>
    /// Name or IP address of the host.
    /// </summary>
    public static string SocketHost { get; set; }
    /// <summary>
    /// Port number of the listening host.
    /// </summary>
    public static int SocketPort { get; set; }
    /// <summary>
    /// Expected duration of each frame (eg. 60FPS ~16ms per frame).
    /// </summary>
    public static int FrameDurationMilliseconds { get; set; }

    /// <summary>
    /// Create a server engine to accept tenants (client connections).
    /// </summary>
    /// <param name="HostApplication"></param>
    /// <param name="InvAction"></param>
    /// <returns></returns>
    public static ServerEngine NewEngine(Inv.Application HostApplication, Action<Guid, Inv.Application> InvAction)
    {
      return new ServerEngine(HostApplication, InvAction);
    }
  }

  /// <summary>
  /// Manage the connected tenants.
  /// </summary>
  public sealed class ServerEngine
  {
    internal ServerEngine(Inv.Application HostApplication, Action<Guid, Inv.Application> InvAction)
    {
      this.HostApplication = HostApplication;
      this.TenantList = [];
      this.AcceptTenantList = [];
      this.RejectChannelList = [];

      this.WebServer = this.HostApplication.Web.NewServer(ServerShell.SocketHost, ServerShell.SocketPort);
      WebServer.AcceptEvent += (Channel) =>
      {
        Guid Identity;

        var TransportFlow = new Inv.TransportFlow(Channel.TransportStream);
        var TransportPacket = TransportFlow.TryReceivePacket();

        if (TransportPacket == null)
        {
          Channel.Drop();
        }
        else
        {
          using (var TransportReceiver = TransportPacket.ToReceiver())
          {
            var CompactReader = TransportReceiver.Reader;

            // identification message.
            var ClientMessage = CompactReader.ReadClientMessage();
            if (ClientMessage != ClientMessage.Identification)
            {
              Channel.Drop();
              return;
            }

            // 64bit protocol version number.
            var Version = CompactReader.ReadInt64();
            if (Version != ServerShell.ProtocolVersion)
            {
              Channel.Drop();
              return;
            }

            Identity = CompactReader.ReadGuid();
          }

          lock (AcceptTenantList)
          {
            AcceptTenantList.Add(() =>
            {
              var AcceptApplication = new Inv.Application();
              AcceptApplication.Dismiss();

              var Result = new ServerTenant(this, AcceptApplication, Channel, TransportFlow);

              InvAction(Identity, AcceptApplication);

              return Result;
            });
          }
        }
      };
      WebServer.RejectEvent += (Channel) =>
      {
        lock (RejectChannelList)
          RejectChannelList.Add(Channel);
      };

      this.SleepSignal = new Inv.ManualResetSignal(false, "Inv.ServerEngine-Sleep");
    }

    /// <summary>
    /// The base application for the host.
    /// </summary>
    public Inv.Application HostApplication { get; private set; }
    /// <summary>
    /// Handle to be notified when a tenant is accepted.
    /// </summary>
    public event Action<ServerTenant> AcceptEvent;
    /// <summary>
    /// Handle to be notified when a tenant is rejected.
    /// </summary>
    public event Action<ServerTenant> RejectEvent;
    /// <summary>
    /// Handle to run code each execution frame.
    /// </summary>
    public event Action ProcessEvent;

    /// <summary>
    /// Start the engine to listen for tenants.
    /// </summary>
    public void Start()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        this.LogWriter = null;
        //this.LogWriter = new StreamWriter(@"C:\Hole\InvServerEngine.log");
        this.ProcessStopwatch = new Stopwatch();

        WebServer.Connect();

        SleepSignal.Reset();

        this.Task = new System.Threading.Tasks.Task(RunThread);
        Task.Start();
      }
    }
    /// <summary>
    /// Stop the engine and disconnect all tenants.
    /// </summary>
    public void Stop()
    {
      if (IsActive)
      {
        this.IsActive = false;

        SleepSignal.Set();

        if (Task != null)
        {
          Task.Wait();
          this.Task = null;
        }

        WebServer.Disconnect();

        if (LogWriter != null)
        {
          LogWriter.Flush();
          LogWriter.Dispose();
        }
      }
    }
    /// <summary>
    /// Enumerate the tenants.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<ServerTenant> GetTenants()
    {
      return TenantList;
    }

    internal ServerImage NextImage()
    {
      return new ServerImage(this, new ServerImageTag(++LastImageID));
    }
    internal ServerSound NextSound()
    {
      return new ServerSound(this, new ServerSoundTag(++LastSoundID));
    }

    private void RunThread()
    {
      try
      {
        var FrameDuration = Inv.ServerShell.FrameDurationMilliseconds;

        while (IsActive)
        {
          ProcessStopwatch.Restart();

          Process();

          var FrameMS = (int)ProcessStopwatch.ElapsedMilliseconds;
          if (FrameMS < FrameDuration)
            SleepSignal.WaitOne(FrameDuration - FrameMS);
        }
      }
      finally
      {
        foreach (var Tenant in TenantList)
          Tenant.Stop();

        TenantList.Clear();
      }
    }
    private void Process()
    {
      // rejecting tenants.
      WebChannel[] RejectChannelArray;

      lock (RejectChannelList)
      {
        if (RejectChannelList.Count > 0)
        {
          RejectChannelArray = RejectChannelList.ToArray();
          RejectChannelList.Clear();
        }
        else
        {
          RejectChannelArray = null;
        }
      }

      if (RejectChannelArray != null)
      {
        foreach (var RejectChannel in RejectChannelList)
        {
          var RejectTenant = TenantList.Find(T => T.WebChannel == RejectChannel);

          if (RejectTenant != null)
          {
            TenantList.Remove(RejectTenant);
            RejectTenant.Stop();

            RejectEvent?.Invoke(RejectTenant);
          }
        }
      }

      // accepting tenants.
      Func<ServerTenant>[] AcceptTenantArray;
      lock (AcceptTenantList)
      {
        if (AcceptTenantList.Count > 0)
        {
          AcceptTenantArray = AcceptTenantList.ToArray();
          AcceptTenantList.Clear();
        }
        else
        {
          AcceptTenantArray = null;
        }
      }

      if (AcceptTenantArray != null)
      {
        foreach (var NewTenant in AcceptTenantArray)
        {
          var AcceptTenant = NewTenant();
          AcceptTenant.Start();

          TenantList.Add(AcceptTenant);

          AcceptEvent?.Invoke(AcceptTenant);
        }
      }

      // processing event.
      ProcessEvent?.Invoke();

      // processing tenants.
      foreach (var Tenant in TenantList)
      {
        Tenant.Process();

        if (Tenant.InvApplication.IsExit)
        {
          TenantList.Remove(Tenant);
          Tenant.Stop();
        }
      }
    }

    private readonly WebServer WebServer;
    private readonly Inv.DistinctList<ServerTenant> TenantList;
    private readonly Inv.DistinctList<Func<ServerTenant>> AcceptTenantList;
    private readonly Inv.DistinctList<WebChannel> RejectChannelList;
    private ushort LastImageID;
    private ushort LastSoundID;
    private System.Threading.Tasks.Task Task;
    private Stopwatch ProcessStopwatch;
    private System.IO.StreamWriter LogWriter;
    private bool IsActive;
    private readonly Inv.ManualResetSignal SleepSignal;
  }

  /// <summary>
  /// Represents a connected client and their application.
  /// </summary>
  public sealed class ServerTenant
  {
    internal ServerTenant(ServerEngine ServerEngine, Inv.Application InvApplication, WebChannel WebChannel, Inv.TransportFlow TransportFlow)
    {
      this.ServerEngine = ServerEngine;
      this.InvApplication = InvApplication;
      this.WebChannel = WebChannel;
      this.TransportFlow = TransportFlow;

      this.RouteArray = new Inv.EnumArray<Inv.ControlType, Func<Inv.Control, ServerPanel>>()
      {
        { Inv.ControlType.Block, TranslateBlock },
        { Inv.ControlType.Board, TranslateBoard },
        { Inv.ControlType.Browser, TranslateBrowser },
        { Inv.ControlType.Button, TranslateButton },
        { Inv.ControlType.Dock, TranslateDock },
        { Inv.ControlType.Edit, TranslateEdit },
        { Inv.ControlType.Flow, TranslateFlow },
        { Inv.ControlType.Frame, TranslateFrame },
        { Inv.ControlType.Graphic, TranslateGraphic },
        { Inv.ControlType.Label, TranslateLabel },
        { Inv.ControlType.Memo, TranslateMemo },
        { Inv.ControlType.Native, TranslateNative },
        { Inv.ControlType.Overlay, TranslateOverlay },
        { Inv.ControlType.Canvas, TranslateCanvas },
        { Inv.ControlType.Scroll, TranslateScroll },
        { Inv.ControlType.Shape, TranslateShape },
        { Inv.ControlType.Stack, TranslateStack },
        { Inv.ControlType.Switch, TranslateSwitch },
        { Inv.ControlType.Table, TranslateTable },
        { Inv.ControlType.Video, TranslateVideo },
        { Inv.ControlType.Wrap, TranslateWrap },
      };

      this.ServerQueue = new ServerQueue(TransportFlow, "Tenant");
      this.ServerSender = new ServerSender(this);
      this.ServerContract = ServerSender;

      this.SurfaceDictionary = [];
      this.PanelDictionary = [];
      this.ImageDictionary = [];
      this.SoundDictionary = [];
      this.AnimationDictionary = [];
      this.ClipDictionary = [];
      this.PickerDictionary = [];
      this.PostActionList = [];
      this.ReceiveArray = [];

      var OwnerType = GetType().GetReflectionInfo();
      var MethodInfoArray = OwnerType.GetReflectionMethods().Where(M => !M.IsPublic && !M.IsStatic).ToArray();

      foreach (var MethodInfo in MethodInfoArray)
      {
        var ParameterInfoArray = MethodInfo.GetParameters();

        if (MethodInfo.Name.StartsWith("Receive") && ParameterInfoArray.Length == 1 && ParameterInfoArray[0].ParameterType == typeof(Inv.CompactReader))
        {
          var Message = Inv.Support.EnumHelper.Parse<ClientMessage>(MethodInfo.Name.Substring("Receive".Length));

          ReceiveArray[Message] = (Action<Inv.CompactReader>)MethodInfo.CreateDelegate(typeof(Action<>).MakeGenericType(typeof(Inv.CompactReader)), this);
        }
      }

      // will be replaced from the remote user.
      InvApplication.SetPlatform(new ServerPlatform(this));
      InvApplication.Device.Target = DeviceTarget.WindowsDesktop; // TODO: unspecified?
      InvApplication.Device.Theme = DeviceTheme.Light;
      InvApplication.Device.Name = "";
      InvApplication.Device.Model = "";
      InvApplication.Device.Manufacturer = "";
      InvApplication.Device.System = "";
      InvApplication.Device.Keyboard = true;
      InvApplication.Device.Mouse = true;
      InvApplication.Device.Touch = true;
      InvApplication.Device.ProportionalFontName = "";
      InvApplication.Device.MonospacedFontName = "";
      InvApplication.Device.PixelDensity = 1.0F;
      InvApplication.Process.Id = 0;
      InvApplication.Window.Width = 1024;
      InvApplication.Window.Height = 768;
      InvApplication.Window.NativePanelType = null; // not supported.

      InvApplication.Graphics.CanvasDrawDelegate = (Canvas, DrawAction) => GraphicsCanvasDraw(TranslateCanvas(Canvas), DrawAction);

      this.CalendarTimeZoneName = TimeZoneInfo.Local.DisplayName;
    }

    /// <summary>
    /// The tenant's server-side application.
    /// </summary>
    public Application InvApplication { get; private set; }

    internal ServerEngine ServerEngine { get; private set; }
    internal WebChannel WebChannel { get; private set; }
    internal Inv.TransportFlow TransportFlow { get; private set; }
    internal ServerQueue ServerQueue { get; private set; }
    internal string CalendarTimeZoneName { get; private set; }
    internal bool PhoneIsSupported { get; private set; }
    internal bool LocationIsSupported { get; private set; }
    internal bool ClipboardIsTextSupported { get; private set; }
    internal bool ClipboardIsImageSupported { get; private set; }

    internal void Start()
    {
      InvApplication.Recruit();
      try
      {
        var TransportPacket = TransportFlow.ReceivePacket();

        using (var TransportReceiver = TransportPacket.ToReceiver())
        {
          var CompactReader = TransportReceiver.Reader;

          var ClientMessage = CompactReader.ReadClientMessage();
          if (ClientMessage != ClientMessage.StartApplication)
            throw new Exception("Invalid introductory message: " + ClientMessage);

          InvApplication.Version = CompactReader.ReadString();
          InvApplication.Device.Target = (DeviceTarget)CompactReader.ReadByte();
          InvApplication.Device.Name = CompactReader.ReadString();
          InvApplication.Device.Model = CompactReader.ReadString();
          InvApplication.Device.Manufacturer = CompactReader.ReadString();
          InvApplication.Device.System = CompactReader.ReadString();
          InvApplication.Device.Keyboard = CompactReader.ReadBoolean();
          InvApplication.Device.Mouse = CompactReader.ReadBoolean();
          InvApplication.Device.Touch = CompactReader.ReadBoolean();
          InvApplication.Device.ProportionalFontName = CompactReader.ReadString();
          InvApplication.Device.MonospacedFontName = CompactReader.ReadString();
          InvApplication.Device.PixelDensity = CompactReader.ReadFloat();
          InvApplication.Device.Theme = (DeviceTheme)CompactReader.ReadByte();

          InvApplication.Process.Id = CompactReader.ReadInt32();

          InvApplication.Window.Width = CompactReader.ReadInt32();
          InvApplication.Window.Height = CompactReader.ReadInt32();

          this.PhoneIsSupported = CompactReader.ReadBoolean();
          this.LocationIsSupported = CompactReader.ReadBoolean();
          this.ClipboardIsTextSupported = CompactReader.ReadBoolean();
          this.ClipboardIsImageSupported = CompactReader.ReadBoolean();
          InvApplication.Haptics.IsSupported = CompactReader.ReadBoolean();
          this.CalendarTimeZoneName = CompactReader.ReadString();
        }

        ServerQueue.Start();

        InvApplication.StartInvoke();

        ProcessChanges();

        ServerSender.Return(ServerMessage.ConfirmStartApplication, Writer =>
        {
          Writer.WriteString(InvApplication.Title);
        });
      }
      finally
      {
        InvApplication.Dismiss();
      }
    }
    internal void Stop()
    {
      InvApplication.Recruit();
      try
      {
        WebChannel.Drop();

        ServerQueue.Stop();

        InvApplication.StopInvoke();
      }
      finally
      {
        InvApplication.Dismiss();
      }
    }
    internal void PlaySound(Sound Sound, float Volume, float Rate, float Pan)
    {
      ServerContract.PlaySound(TranslateSoundTag(Sound), Volume, Rate, Pan);
    }
    internal void PlayClip(AudioClip Clip)
    {
      ServerContract.PlayClip(TranslateClipTag(Clip), TranslateSoundTag(Clip.Sound), Clip.Volume, Clip.Rate, Clip.Pan, Clip.Looped);
    }
    internal void PauseClip(AudioClip Clip)
    {
      var ServerClip = Clip.Node as ServerClip;
      if (ServerClip != null)
        ServerContract.PauseClip(ServerClip.Tag);
    }
    internal void ResumeClip(AudioClip Clip)
    {
      var ServerClip = Clip.Node as ServerClip;
      if (ServerClip != null)
        ServerContract.ResumeClip(ServerClip.Tag);
    }
    internal void StopClip(AudioClip Clip)
    {
      var ServerClip = Clip.Node as ServerClip;
      if (ServerClip != null)
        ServerContract.StopClip(ServerClip.Tag);
    }
    internal void ModulateClip(AudioClip Clip)
    {
      var ServerClip = Clip.Node as ServerClip;
      if (ServerClip != null)
        ServerContract.ModulateClip(ServerClip.Tag, Clip.Volume, Clip.Rate, Clip.Pan, Clip.Looped);
    }
    internal void WebLaunchUri(Uri Uri)
    {
      ServerContract.LaunchWebUri(Uri);
    }
    internal void WebInstallUri(Uri Uri)
    {
      ServerContract.InstallWebUri(Uri);
    }
    internal void MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      ServerContract.BrowseMarket(AppleiTunesID, GooglePlayID, WindowsStoreID);
    }
    internal string ClipboardGetText()
    {
      return ServerContract.ClipboardGetText();
    }
    internal void ClipboardSetText(string Text)
    {
      ServerContract.ClipboardSetText(Text);
    }
    internal Inv.Image ClipboardGetImage()
    {
      return ServerContract.ClipboardGetImage();
    }
    internal void ClipboardSetImage(Inv.Image Image)
    {
      ServerContract.ClipboardSetImage(Image);
    }
    internal void HapticFeedback(HapticFeedback Feedback)
    {
      ServerContract.HapticFeedback(Feedback);
    }
    internal void CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      var ServerPicker = (ServerPicker)CalendarPicker.Node;

      if (ServerPicker == null)
      {
        ServerPicker = new ServerPicker(new ServerPickerTag(++LastPickerID));

        PickerDictionary[ServerPicker.Tag] = CalendarPicker;

        CalendarPicker.Node = ServerPicker;
      }

      ServerContract.ShowCalendarPicker(ServerPicker.Tag, CalendarPicker.SetDate, CalendarPicker.SetTime, CalendarPicker.Value);
    }
    internal void DirectoryShowPicker(DirectoryFilePicker FilePicker)
    {
      var ServerPicker = (ServerPicker)FilePicker.Node;

      if (ServerPicker == null)
      {
        ServerPicker = new ServerPicker(new ServerPickerTag(++LastPickerID));

        PickerDictionary[ServerPicker.Tag] = FilePicker;

        FilePicker.Node = ServerPicker;
      }

      ServerContract.ShowDirectoryPicker(ServerPicker.Tag, FilePicker.Title, FilePicker.PickType);
    }
    internal bool EmailSendMessage(EmailMessage EmailMessage)
    {
      var ToArray = EmailMessage.GetTos().ToArray();
      var ServerToArray = new ServerEmailTo[ToArray.Length];
      var ToIndex = 0;
      foreach (var To in ToArray)
      {
        ServerToArray[ToIndex++] = new ServerEmailTo()
        {
          Name = To.Name,
          Address = To.Address
        };
      }

      var AttachmentArray = EmailMessage.GetAttachments().ToArray();
      var ServerAttachmentArray = new ServerEmailAttachment[AttachmentArray.Length];
      var AttachmentIndex = 0;
      foreach (var Attachment in AttachmentArray)
      {
        ServerAttachmentArray[AttachmentIndex++] = new ServerEmailAttachment()
        {
          Name = Attachment.Name,
          Content = Attachment.File.ReadAllBytes()
        };
      }

      return ServerContract.SendEmailMessage(EmailMessage.Subject, EmailMessage.Body, ServerToArray, ServerAttachmentArray);
    }
    internal void PhoneDial(string PhoneNumber)
    {
      ServerContract.DialPhone(PhoneNumber);
    }
    internal void PhoneSMS(string PhoneNumber)
    {
      ServerContract.SMSPhone(PhoneNumber);
    }
    internal void LocationLookup(LocationResult LocationLookup)
    {
      //ServerContract.LookupLocation(LocationLookup);
    }
    internal void WindowBrowse(File File)
    {
      // TODO: Tenant.WindowBrowse(File); // requires the file to be sent to the tenant first.
    }
    internal void WindowShare(File File)
    {
      // TODO: Tenant.WindowShare(File); // requires the file to be sent to the tenant first.
    }
    internal void Process()
    {
      InvApplication.Recruit();
      try
      {
        try
        {
          Action[] PostActionArray;
          lock (PostActionList)
          {
            if (PostActionList.Count > 0)
            {
              PostActionArray = PostActionList.ToArray();

              PostActionList.Clear();
            }
            else
            {
              PostActionArray = null;
            }
          }

          if (PostActionArray != null)
          {
            foreach (var PostAction in PostActionArray)
              PostAction();
          }

          // execute the received messages.
          ProcessMessages();

          // dispose any garbage.
          ProcessGarbage();

          // send queued messages to the client.
          ServerSender.Send();
        }
        catch (Exception Exception)
        {
          InvApplication.HandleExceptionInvoke(Exception);
        }

        InvApplication.Window.DisplayRate.Calculate();
      }
      finally
      {
        InvApplication.Dismiss();
      }
    }
    internal void ProcessMessages()
    {
      InvApplication.RequireThreadAffinity();

      var ReceivePacket = ServerQueue.TryReceivePacket();

      if (ReceivePacket == null)
      {
        ProcessChanges();
      }
      else
      {
        while (ReceivePacket != null)
        {
          using (var TransportReceiver = ReceivePacket.ToReceiver())
          {
            var CompactReader = TransportReceiver.Reader;

            var Message = CompactReader.ReadClientMessage();

            ExecuteMessage(Message, CompactReader);
          }

          ReceivePacket = ServerQueue.TryReceivePacket();
        }
      }
    }
    internal void ExecuteMessage(ClientMessage Message, CompactReader Reader)
    {
      InvApplication.RequireThreadAffinity();

      var ReceiveAction = ReceiveArray[Message];

      if (ReceiveAction == null)
        throw new Exception("ClientMessage not handled: " + Message);

      ReceiveAction(Reader);
    }
    internal void Post(Action Action)
    {
      lock (PostActionList)
        PostActionList.Add(Action);
    }
    internal void Call(Action Action)
    {
      throw new NotImplementedException();
    }
    internal IEnumerable<Inv.Image> GetImages()
    {
      foreach (var ImageReference in ImageDictionary.Values)
      {
        if (ImageReference.TryGetTarget(out Inv.Image Image))
          yield return Image;
      }
    }
    internal IEnumerable<Inv.Sound> GetSounds()
    {
      foreach (var SoundReference in SoundDictionary.Values)
      {
        if (SoundReference.TryGetTarget(out Inv.Sound Sound))
          yield return Sound;
      }
    }
    internal Inv.Dimension WindowGetDimension(Inv.Panel Panel)
    {
      return ServerContract.GetPanelDimension(TranslatePanelTag(Panel));
    }
    internal void StartAnimation(Inv.Animation InvAnimation)
    {
      var StartAnimationID = TranslateAnimationTag(InvAnimation);
      AnimationDictionary[StartAnimationID] = InvAnimation;

      var ServerTargetArray = new ServerAnimationTarget[InvAnimation.Targets.Count];

      var TargetIndex = 0;
      foreach (var Target in InvAnimation.Targets)
      {
        var ServerTransformArray = new ServerAnimationTransform[Target.Transforms.Count];

        var TransformIndex = 0;
        foreach (var Transform in Target.Transforms)
        {
          var ServerTransform = new ServerAnimationTransform()
          {
            Type = Transform.Type
          };

          switch (Transform.Type)
          {
            case AnimationType.Fade:
              var FadeOpacityTransform = (AnimationFadeTransform)Transform;
              ServerTransform.FadeOffset = FadeOpacityTransform.Offset;
              ServerTransform.FadeDuration = FadeOpacityTransform.Duration;
              ServerTransform.FadeFromOpacity = FadeOpacityTransform.FromOpacity;
              ServerTransform.FadeToOpacity = FadeOpacityTransform.ToOpacity;
              break;

            case AnimationType.Rotate:
              var RotateOpacityTransform = (AnimationRotateTransform)Transform;
              ServerTransform.RotateOffset = RotateOpacityTransform.Offset;
              ServerTransform.RotateDuration = RotateOpacityTransform.Duration;
              ServerTransform.RotateFromAngle = RotateOpacityTransform.FromAngle;
              ServerTransform.RotateToAngle = RotateOpacityTransform.ToAngle;
              break;

            case AnimationType.Scale:
              var ScaleSizeTransform = (AnimationScaleTransform)Transform;
              ServerTransform.ScaleOffset = ScaleSizeTransform.Offset;
              ServerTransform.ScaleDuration = ScaleSizeTransform.Duration;
              ServerTransform.ScaleFromWidth = ScaleSizeTransform.FromWidth;
              ServerTransform.ScaleToWidth = ScaleSizeTransform.ToWidth;
              ServerTransform.ScaleFromHeight = ScaleSizeTransform.FromHeight;
              ServerTransform.ScaleToHeight = ScaleSizeTransform.ToHeight;
              break;

            case AnimationType.Translate:
              var TranslateSizeTransform = (AnimationTranslateTransform)Transform;
              ServerTransform.TranslateOffset = TranslateSizeTransform.Offset;
              ServerTransform.TranslateDuration = TranslateSizeTransform.Duration;
              ServerTransform.TranslateFromX = TranslateSizeTransform.FromX;
              ServerTransform.TranslateToX = TranslateSizeTransform.ToX;
              ServerTransform.TranslateFromY = TranslateSizeTransform.FromY;
              ServerTransform.TranslateToY = TranslateSizeTransform.ToY;
              break;

            default:
              throw new Exception("AnimationType not handled: " + Transform.Type);
          }

          ServerTransformArray[TransformIndex++] = ServerTransform;
        }

        ServerTargetArray[TargetIndex++] = new ServerAnimationTarget()
        {
          PanelTag = TranslatePanelTag(Target.Panel),
          TransformArray = ServerTransformArray
        };
      }

      ServerContract.StartAnimation(StartAnimationID, ServerTargetArray);
    }
    internal void StopAnimation(Inv.Animation InvAnimation)
    {
      var AnimationTag = TranslateAnimationTag(InvAnimation);
      AnimationDictionary.Remove(AnimationTag);

      ServerContract.StopAnimation(AnimationTag);
    }
    internal void ShowPopup(Inv.Popup InvPopup)
    {
      throw new NotImplementedException();
    }
    internal void HidePopup(Inv.Popup InvPopup)
    {
      throw new NotImplementedException();
    }

    private void ProcessChanges()
    {
      if (InvApplication.IsExit)
      {
        ServerContract.ExitApplication();
      }
      else
      {
        var InvWindow = InvApplication.Window;
        var InvKeyboard = InvApplication.Keyboard;

        InvWindow.ProcessInvoke();

        if (InvWindow.ActiveTimerSet.Count > 0)
        {
          foreach (var InvTimer in InvWindow.ActiveTimerSet)
          {
            var ServerTimer = AccessTimer(InvTimer, S =>
            {
              var Result = new ServerTimer();
              Result.IntervalEvent += () =>
              {
                if (InvTimer.IsEnabled)
                  Post(() => S.IntervalInvoke());
              };
              return Result;
            });

            if (InvTimer.IsRestarting)
            {
              InvTimer.IsRestarting = false;
              ServerTimer.Stop();
            }

            if (ServerTimer.IntervalTime != InvTimer.IntervalTime)
              ServerTimer.IntervalTime = InvTimer.IntervalTime;

            if (InvTimer.IsEnabled && !ServerTimer.IsEnabled)
              ServerTimer.Start();
            else if (!InvTimer.IsEnabled && ServerTimer.IsEnabled)
              ServerTimer.Stop();
          }

          InvWindow.ActiveTimerSet.RemoveWhere(T => !T.IsEnabled);
        }

        var InvSurfaceActive = InvWindow.ActiveSurface;

        if (InvSurfaceActive != null)
        {
          var ServerSurface = AccessSurface(InvSurfaceActive);

          if (this.ActiveSurface != ServerSurface)
          {
            this.ActiveSurface = ServerSurface;

            InvSurfaceActive.ArrangeInvoke();
          }

          ProcessTransition(ServerSurface);

          InvSurfaceActive.ComposeInvoke();

          UpdateSurface(InvSurfaceActive, ServerSurface);
        }
        else
        {
          this.ActiveSurface = null;
        }

        InvWindow.ProcessChanges(P => TranslatePanel(P));

        if (InvKeyboard.Focus != null)
        {
          var FocusPanel = TranslatePanel(InvKeyboard.Focus.Control);

          InvKeyboard.Focus = null;

          ServerContract.SetKeyboardFocus(GetPanelTag(FocusPanel));
        }

        if (InvWindow.Render())
        {
          if (InvWindow.Background.Render())
            ServerContract.SetWindowBackground(InvWindow.Background.Colour);

          if (InvWindow.InputPrevented != WindowInputPrevented || InvWindow.DisablePanelList.Count > 0 || InvWindow.EnablePanelList.Count > 0)
          {
            this.WindowInputPrevented = InvWindow.InputPrevented;

            var DisablePanelArray = InvWindow.DisablePanelList.Select(P => GetPanelTag(TranslatePanel(P))).ToArray();
            var EnablePanelArray = InvWindow.EnablePanelList.Select(P => GetPanelTag(TranslatePanel(P))).ToArray();

            ServerContract.SetWindowInputPrevented(InvWindow.InputPrevented, DisablePanelArray, EnablePanelArray);

            InvWindow.DisablePanelList.Clear();
            InvWindow.EnablePanelList.Clear();
          }
        }
      }
    }
    private void ProcessTransition(ServerSurface ServerSurface)
    {
      var InvWindow = InvApplication.Window;

      var InvTransition = InvWindow.ActiveTransition;
      if (InvTransition == null)
      {
        Debug.Assert(ActiveSurface == null || InvWindow.ActiveSurface == null || InvWindow.ActiveSurface.Node == ActiveSurface);
      }
      else
      {
        if (InvWindow.FromSurface != null && InvWindow.FromSurface.Node is Inv.ServerSurface)
          UpdateSurface(InvWindow.FromSurface, (Inv.ServerSurface)InvWindow.FromSurface.Node);

        ServerContract.TransitionSurface(ServerSurface.Tag, InvTransition.Type, InvTransition.Duration);

        InvTransition.Complete(); // TODO: this needs to be a callback from the tenant when the transition actually finishes.

        InvWindow.ActiveTransition = null;
      }
    }
    private void ProcessGarbage()
    {
      GarbageDisposal(SurfaceDictionary, T => ServerContract.DisposeSurface(T));
      GarbageDisposal(PanelDictionary, T => ServerContract.DisposePanel(T));
      GarbageDisposal(ImageDictionary, T => ServerContract.DisposeImage(T));
      GarbageDisposal(SoundDictionary, T => ServerContract.DisposeSound(T));
    }
    private void UpdateSurface(Surface InvSurface, ServerSurface ServerSurface)
    {
      if (InvSurface.Render())
      {
        if (ServerSurface.HasGestureBackward != InvSurface.HasGestureBackward)
        {
          ServerSurface.HasGestureBackward = InvSurface.HasGestureBackward;
          ServerContract.SetSurfaceHasGestureBackward(ServerSurface.Tag, InvSurface.HasGestureBackward);
        }

        if (ServerSurface.HasGestureForward != InvSurface.HasGestureForward)
        {
          ServerSurface.HasGestureForward = InvSurface.HasGestureForward;
          ServerContract.SetSurfaceHasGestureForward(ServerSurface.Tag, InvSurface.HasGestureForward);
        }

        if (InvSurface.Background.Render())
          ServerContract.SetSurfaceBackground(ServerSurface.Tag, InvSurface.Background.Colour);

        var ContentPanel = TranslatePanel(InvSurface.Content);
        ServerContract.SetSurfaceContent(ServerSurface.Tag, GetPanelTag(ContentPanel));
      }
    }
    private ServerTimer AccessTimer(Inv.WindowTimer InvTimer, Func<Inv.WindowTimer, ServerTimer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (ServerTimer)InvTimer.Node;
      }
    }
    private ServerSurface AccessSurface(Inv.Surface InvSurface)
    {
      if (InvSurface.Node == null)
      {
        var Result = new ServerSurface(NextSurfaceTag());

        InvSurface.Node = Result;

        SurfaceDictionary[Result.Tag] = InvSurface.AsWeakReference();

        ServerContract.NewSurface(Result.Tag);

        return Result;
      }
      else
      {
        return (ServerSurface)InvSurface.Node;
      }
    }
    private ServerPanel TranslatePanel(Inv.Panel InvPanel)
    {
      var InvControl = InvPanel?.Control;

      if (InvControl == null)
        return null;
      else
        return RouteArray[InvControl.ControlType](InvControl);
    }
    private TElement AccessPanel<TControl, TElement>(TControl InvControl, Func<TControl, TElement> BuildFunction)
      where TControl : Inv.Control
      where TElement : ServerPanel
    {
      if (InvControl.Node == null)
      {
        var Result = BuildFunction(InvControl);

        InvControl.Node = Result;

        PanelDictionary[Result.Tag] = InvControl.AsWeakReference<Inv.Control>();

        return Result;
      }
      else
      {
        return (TElement)InvControl.Node;
      }
    }
    private ServerPanel TranslateBlock(Inv.Panel InvPanel)
    {
      var InvBlock = (Inv.Block)InvPanel;

      var ServerBlock = AccessPanel(InvBlock, P =>
      {
        var Result = new ServerBlock(NextPanelTag());

        ServerContract.NewBlock(Result.Tag);

        return Result;
      });

      if (InvBlock.Render())
      {
        TranslateLayout(InvBlock, ServerBlock);

        var InvTooltip = InvBlock.Tooltip;
        if (InvTooltip.Render())
        {
          //ServerContract.SetBlockTooltip(ServerBlock.Tag, TranslateTooltip(InvTooltip));
        }

        if (InvBlock.LineWrapping != ServerBlock.LineWrapping)
        {
          ServerBlock.LineWrapping = InvBlock.LineWrapping;
          ServerContract.SetBlockLineWrapping(ServerBlock.Tag, InvBlock.LineWrapping);
        }

        var Justification = InvBlock.Justify.Get();
        if (Justification != ServerBlock.Justification)
        {
          ServerBlock.Justification = Justification;
          ServerContract.SetBlockJustification(ServerBlock.Tag, Justification);
        }

        var InvFont = InvBlock.Font;
        if (InvFont.Render())
          ServerContract.SetBlockFont(ServerBlock.Tag, TranslateFont(InvFont));

        if (InvBlock.SpanCollection.Render())
        {
          ServerContract.SetBlockCollection(ServerBlock.Tag, InvBlock.SpanCollection.Select(S =>
          {
            S.Background.Render();
            S.Font.Render();

            return new ServerBlockSpan()
            {
              Style = S.Style,
              Text = S.Text,
              BackgroundColour = S.Background.Colour,
              Font = TranslateFont(S.Font),
            };
          }).ToArray());
        }
      }

      return ServerBlock;
    }
    private ServerPanel TranslateBrowser(Inv.Panel InvPanel)
    {
      var InvBrowser = (Inv.Browser)InvPanel;

      var ServerBrowser = AccessPanel(InvBrowser, P =>
      {
        var Result = new ServerBrowser(NextPanelTag());

        ServerContract.NewBrowser(Result.Tag);

        return Result;
      });

      if (InvBrowser.Render())
      {
        TranslateLayout(InvBrowser, ServerBrowser);

        if ((InvBrowser.Html != null && ServerBrowser.Html != InvBrowser.Html) || (InvBrowser.Uri != null && ServerBrowser.Uri != InvBrowser.Uri))
        {
          ServerBrowser.Html = InvBrowser.Html;
          ServerBrowser.Uri = InvBrowser.Uri;

          ServerContract.LoadBrowser(ServerBrowser.Tag, InvBrowser.Uri, InvBrowser.Html);
        }
      }

      return ServerBrowser;
    }
    private ServerPanel TranslateButton(Inv.Panel InvPanel)
    {
      var InvButton = (Inv.Button)InvPanel;

      var ServerButton = AccessPanel(InvButton, P =>
      {
        var Result = new ServerButton(NextPanelTag());

        ServerContract.NewButton(Result.Tag, InvButton.Style);

        return Result;
      });

      if (InvButton.Render())
      {
        TranslateLayout(InvButton, ServerButton);

        var InvTooltip = InvButton.Tooltip;
        if (InvTooltip.Render())
        {
          //ServerContract.SetButtonTooltip(ServerButton.Tag, TranslateTooltip(InvTooltip));
        }

        var InvFocus = InvButton.Focus;
        if (InvFocus.Render())
          ServerContract.SetButtonFocus(ServerButton.Tag, TranslateFocus(InvFocus));

        if (ServerButton.IsEnabled != InvButton.IsEnabled)
        {
          ServerButton.IsEnabled = InvButton.IsEnabled;
          ServerContract.SetButtonIsEnabled(ServerButton.Tag, InvButton.IsEnabled);
        }

        if (ServerButton.IsFocusable != InvButton.IsFocusable)
        {
          ServerButton.IsFocusable = InvButton.IsFocusable;
          ServerContract.SetButtonIsFocusable(ServerButton.Tag, InvButton.IsFocusable);
        }

        if (ServerButton.Hint != InvButton.Hint)
        {
          ServerButton.Hint = InvButton.Hint;
          ServerContract.SetButtonHint(ServerButton.Tag, InvButton.Hint);
        }

        var InvButtonHasPress = InvButton.HasPress();
        if (ServerButton.HasPress != InvButtonHasPress)
        {
          ServerButton.HasPress = InvButtonHasPress;
          ServerContract.SetButtonHasPress(ServerButton.Tag, InvButtonHasPress);
        }

        var InvButtonHasRelease = InvButton.HasRelease();
        if (ServerButton.HasRelease != InvButtonHasRelease)
        {
          ServerButton.HasRelease = InvButtonHasRelease;
          ServerContract.SetButtonHasRelease(ServerButton.Tag, InvButtonHasRelease);
        }

        if (InvButton.ContentSingleton.Render())
        {
          var ContentPanel = TranslatePanel(InvButton.ContentSingleton.Data);
          ServerContract.SetButtonContent(ServerButton.Tag, GetPanelTag(ContentPanel));
        }
      }

      return ServerButton;
    }
    private ServerPanel TranslateBoard(Inv.Panel InvPanel)
    {
      var InvBoard = (Inv.Board)InvPanel;

      var ServerBoard = AccessPanel(InvBoard, P =>
      {
        var Result = new ServerBoard(NextPanelTag());

        ServerContract.NewBoard(Result.Tag);

        return Result;
      });

      if (InvBoard.Render())
      {
        TranslateLayout(InvBoard, ServerBoard);

        if (InvBoard.PinCollection.Render())
        {
          var PinArray = new ServerBoardPin[InvBoard.PinCollection.Count];
          var PinIndex = 0;
          foreach (var InvPin in InvBoard.PinCollection)
          {
            PinArray[PinIndex++] = new ServerBoardPin()
            {
              Rect = InvPin.Rect,
              PanelTag = TranslatePanelTag(InvPin.Panel)
            };
          }

          ServerContract.SetBoardCollection(ServerBoard.Tag, PinArray);
        }
      }

      return ServerBoard;
    }
    private ServerPanel TranslateDock(Inv.Panel InvPanel)
    {
      var InvDock = (Inv.Dock)InvPanel;

      var ServerDock = AccessPanel(InvDock, P =>
      {
        var Result = new ServerDock(NextPanelTag());
        Result.Orientation = P.Orientation;

        ServerContract.NewDock(Result.Tag, P.Orientation);

        return Result;
      });

      if (InvDock.Render())
      {
        TranslateLayout(InvDock, ServerDock);

        if (InvDock.Orientation != ServerDock.Orientation)
        {
          ServerContract.SetDockOrientation(ServerDock.Tag, InvDock.Orientation);

          ServerDock.Orientation = InvDock.Orientation;
        }

        if (InvDock.CollectionRender())
        {
          var HeaderArray = new ServerPanelTag[InvDock.HeaderCollection.Count];
          var HeaderIndex = 0;
          foreach (var InvElement in InvDock.HeaderCollection)
            HeaderArray[HeaderIndex++] = TranslatePanelTag(InvElement);

          var ClientArray = new ServerPanelTag[InvDock.ClientCollection.Count];
          var ClientIndex = 0;
          foreach (var InvElement in InvDock.ClientCollection)
            ClientArray[ClientIndex++] = TranslatePanelTag(InvElement);

          var FooterArray = new ServerPanelTag[InvDock.FooterCollection.Count];
          var FooterIndex = 0;
          foreach (var InvElement in InvDock.FooterCollection)
            FooterArray[FooterIndex++] = TranslatePanelTag(InvElement);

          ServerContract.SetDockCollection(ServerDock.Tag, HeaderArray, ClientArray, FooterArray);
        }
      }

      return ServerDock;
    }
    private ServerPanel TranslateEdit(Inv.Panel InvPanel)
    {
      var InvEdit = (Inv.Edit)InvPanel;

      var ServerEdit = AccessPanel(InvEdit, P =>
      {
        var Result = new ServerEdit(NextPanelTag());

        ServerContract.NewEdit(Result.Tag, P.Input);

        return Result;
      });

      if (InvEdit.Render())
      {
        TranslateLayout(InvEdit, ServerEdit);

        if (ServerEdit.HasChange != InvEdit.HasChange)
        {
          ServerEdit.HasChange = InvEdit.HasChange;
          ServerContract.SetEditHasChange(ServerEdit.Tag, InvEdit.HasChange);
        }

        if (ServerEdit.HasReturn != InvEdit.HasReturn)
        {
          ServerEdit.HasReturn = InvEdit.HasReturn;
          ServerContract.SetEditHasReturn(ServerEdit.Tag, InvEdit.HasReturn);
        }

        if (InvEdit.IsReadOnly != ServerEdit.IsReadOnly)
        {
          ServerEdit.IsReadOnly = InvEdit.IsReadOnly;
          ServerContract.SetEditIsReadOnly(ServerEdit.Tag, InvEdit.IsReadOnly);
        }

        if (ServerEdit.Hint != InvEdit.Hint)
        {
          ServerEdit.Hint = InvEdit.Hint;
          ServerContract.SetEditHint(ServerEdit.Tag, InvEdit.Hint);
        }

        var InvFont = InvEdit.Font;
        if (InvFont.Render())
          ServerContract.SetEditFont(ServerEdit.Tag, TranslateFont(InvFont));

        var InvFocus = InvEdit.Focus;
        if (InvFocus.Render())
          ServerContract.SetEditFocus(ServerEdit.Tag, TranslateFocus(InvFocus));

        if (InvEdit.Text != ServerEdit.Text)
        {
          ServerEdit.Text = InvEdit.Text;
          ServerContract.SetEditText(ServerEdit.Tag, InvEdit.Text);
        }
      }

      return ServerEdit;
    }
    private ServerPanel TranslateFlow(Inv.Panel InvPanel)
    {
      var InvFlow = (Inv.Flow)InvPanel;

      var ServerFlow = AccessPanel(InvFlow, P =>
      {
        var Result = new ServerFlow(NextPanelTag());

        ServerContract.NewFlow(Result.Tag);

        return Result;
      });

      if (InvFlow.Render())
      {
        TranslateLayout(InvFlow, ServerFlow);

        var FixtureTag = GetPanelTag(TranslatePanel(InvFlow.Fixture));
        if (ServerFlow.FixtureTag.ID != FixtureTag.ID)
          ServerContract.SetFlowFixture(ServerFlow.Tag, FixtureTag);

        if (InvFlow.IsRefresh)
        {
          InvFlow.IsRefresh = false;

          // TODO: ServerContract.RefreshFlow for animation??
        }

        if (InvFlow.IsReload)
        {
          InvFlow.IsReload = false;

          var SectionArray = new ServerFlowSection[InvFlow.SectionCount];
          var SectionIndex = 0;
          foreach (var Section in InvFlow.Sections)
          {
            SectionArray[SectionIndex++] = new ServerFlowSection()
            {
              ItemCount = Section.ItemCount,
              HeaderPanelTag = GetPanelTag(TranslatePanel(Section.Header)),
              FooterPanelTag = GetPanelTag(TranslatePanel(Section.Footer))
            };
          }

          ServerContract.ReloadFlow(ServerFlow.Tag, SectionArray);
        }
      }

      return ServerFlow;
    }
    private ServerPanel TranslateFrame(Inv.Panel InvPanel)
    {
      var InvFrame = (Inv.Frame)InvPanel;

      var ServerFrame = AccessPanel(InvFrame, P =>
      {
        var Result = new ServerFrame(NextPanelTag());

        ServerContract.NewFrame(Result.Tag);

        return Result;
      });

      if (InvFrame.Render())
      {
        TranslateLayout(InvFrame, ServerFrame);

        if (InvFrame.ContentSingleton.Render())
        {
          var ContentPanel = TranslatePanel(InvFrame.ContentSingleton.Data);

          var InvTransition = InvFrame.ActiveTransition;

          if (InvTransition == null)
          {
            ServerContract.SetFrameContent(ServerFrame.Tag, GetPanelTag(ContentPanel));
          }
          else
          {
            if (InvFrame.FromPanel != null)
            {
              TranslatePanel(InvFrame.FromPanel);
              InvFrame.FromPanel = null;
            }

            ServerContract.SetFrameTransition(ServerFrame.Tag, GetPanelTag(ContentPanel), InvTransition.Type, InvTransition.Duration);

            InvFrame.ActiveTransition = null;
          }
        }
      }

      return ServerFrame;
    }
    private ServerPanel TranslateGraphic(Inv.Panel InvPanel)
    {
      var InvGraphic = (Inv.Graphic)InvPanel;

      var ServerGraphic = AccessPanel(InvGraphic, P =>
      {
        var Result = new ServerGraphic(NextPanelTag());

        ServerContract.NewGraphic(Result.Tag);

        return Result;
      });

      if (InvGraphic.Render())
      {
        TranslateLayout(InvGraphic, ServerGraphic);

        var InvTooltip = InvGraphic.Tooltip;
        if (InvTooltip.Render())
        {
          //ServerContract.SetGraphicTooltip(ServerGraphic.Tag, TranslateTooltip(InvTooltip));
        }

        if (InvGraphic.Fit.Render())
          ServerContract.SetGraphicFit(ServerGraphic.Tag, InvGraphic.Fit.Method);

        if (InvGraphic.ImageSingleton.Render())
          ServerContract.SetGraphicImage(ServerGraphic.Tag, TranslateImageTag(InvGraphic.ImageSingleton.Data));
      }

      return ServerGraphic;
    }
    private ServerPanel TranslateLabel(Inv.Panel InvPanel)
    {
      var InvLabel = (Inv.Label)InvPanel;

      var ServerLabel = AccessPanel(InvLabel, P =>
      {
        var Result = new ServerLabel(NextPanelTag());

        ServerContract.NewLabel(Result.Tag);

        return Result;
      });

      if (InvLabel.Render())
      {
        TranslateLayout(InvLabel, ServerLabel);

        var InvTooltip = InvLabel.Tooltip;
        if (InvTooltip.Render())
        {
          //ServerContract.SetLabelTooltip(ServerLabel.Tag, TranslateTooltip(InvTooltip));
        }

        if (InvLabel.LineWrapping != ServerLabel.LineWrapping)
        {
          ServerLabel.LineWrapping = InvLabel.LineWrapping;
          ServerContract.SetLabelLineWrapping(ServerLabel.Tag, InvLabel.LineWrapping);
        }

        var Justification = InvLabel.Justify.Get();
        if (Justification != ServerLabel.Justification)
        {
          ServerLabel.Justification = Justification;
          ServerContract.SetLabelJustification(ServerLabel.Tag, Justification);
        }

        var InvFont = InvLabel.Font;
        if (InvFont.Render())
          ServerContract.SetLabelFont(ServerLabel.Tag, TranslateFont(InvFont));

        if (InvLabel.Text != ServerLabel.Text)
        {
          ServerLabel.Text = InvLabel.Text;
          ServerContract.SetLabelText(ServerLabel.Tag, InvLabel.Text);
        }
      }

      return ServerLabel;
    }
    private ServerPanel TranslateMemo(Inv.Panel InvPanel)
    {
      var InvMemo = (Inv.Memo)InvPanel;

      var ServerMemo = AccessPanel(InvMemo, P =>
      {
        var Result = new ServerMemo(NextPanelTag());

        ServerContract.NewMemo(Result.Tag);

        return Result;
      });

      if (InvMemo.Render())
      {
        TranslateLayout(InvMemo, ServerMemo);

        if (ServerMemo.HasChange != InvMemo.HasChange)
        {
          ServerMemo.HasChange = InvMemo.HasChange;
          ServerContract.SetMemoHasChange(ServerMemo.Tag, InvMemo.HasChange);
        }

        if (InvMemo.IsReadOnly != ServerMemo.IsReadOnly)
        {
          ServerMemo.IsReadOnly = InvMemo.IsReadOnly;
          ServerContract.SetMemoIsReadOnly(ServerMemo.Tag, InvMemo.IsReadOnly);
        }

        var InvFont = InvMemo.Font;
        if (InvFont.Render())
          ServerContract.SetMemoFont(ServerMemo.Tag, TranslateFont(InvFont));

        var InvFocus = InvMemo.Focus;
        if (InvFocus.Render())
          ServerContract.SetMemoFocus(ServerMemo.Tag, TranslateFocus(InvFocus));

        if (InvMemo.Text != ServerMemo.Text)
        {
          ServerMemo.Text = InvMemo.Text;
          ServerContract.SetMemoText(ServerMemo.Tag, InvMemo.Text);
        }
      }

      return ServerMemo;
    }
    private ServerPanel TranslateNative(Inv.Panel InvPanel)
    {
      throw new Exception("Native panels are not supported in the Server platform.");
    }
    private ServerOverlay TranslateOverlay(Inv.Panel InvPanel)
    {
      var InvOverlay = (Inv.Overlay)InvPanel;

      var ServerOverlay = AccessPanel(InvOverlay, P =>
      {
        var Result = new ServerOverlay(NextPanelTag());

        ServerContract.NewOverlay(Result.Tag);

        return Result;
      });

      if (InvOverlay.Render())
      {
        TranslateLayout(InvOverlay, ServerOverlay);

        if (InvOverlay.PanelCollection.Render())
        {
          var PanelArray = new ServerPanelTag[InvOverlay.PanelCollection.Count];

          var PanelIndex = 0;
          foreach (var InvElement in InvOverlay.PanelCollection)
            PanelArray[PanelIndex++] = TranslatePanelTag(InvElement);

          ServerContract.SetOverlayCollection(ServerOverlay.Tag, PanelArray);
        }
      }

      return ServerOverlay;
    }
    private ServerCanvas TranslateCanvas(Inv.Panel InvPanel)
    {
      var InvCanvas = (Inv.Canvas)InvPanel;

      var ServerCanvas = AccessPanel(InvCanvas, P =>
      {
        var Result = new ServerCanvas(NextPanelTag(), TranslateImageTag);

        ServerContract.NewCanvas(Result.Tag);

        return Result;
      });

      if (InvCanvas.Render())
      {
        TranslateLayout(InvCanvas, ServerCanvas);

        if (ServerCanvas.HasMeasure != InvCanvas.HasMeasure)
        {
          ServerCanvas.HasMeasure = InvCanvas.HasMeasure;
          ServerContract.SetCanvasHasMeasure(ServerCanvas.Tag, InvCanvas.HasMeasure);
        }

        if (InvCanvas.IsInvalidated)
          GraphicsCanvasDraw(ServerCanvas, InvCanvas.DrawInvoke);
      }

      return ServerCanvas;
    }
    private ServerPanel TranslateScroll(Inv.Panel InvPanel)
    {
      var InvScroll = (Inv.Scroll)InvPanel;

      var ServerScroll = AccessPanel(InvScroll, P =>
      {
        var Result = new ServerScroll(NextPanelTag());
        Result.Orientation = P.Orientation;

        ServerContract.NewScroll(Result.Tag, P.Orientation);

        return Result;
      });

      if (InvScroll.Render())
      {
        TranslateLayout(InvScroll, ServerScroll);

        if (InvScroll.Orientation != ServerScroll.Orientation)
        {
          ServerContract.SetScrollOrientation(ServerScroll.Tag, InvScroll.Orientation);

          ServerScroll.Orientation = InvScroll.Orientation;
        }

        if (InvScroll.ContentSingleton.Render())
        {
          var ContentPanel = TranslatePanel(InvScroll.ContentSingleton.Data);
          ServerContract.SetScrollContent(ServerScroll.Tag, GetPanelTag(ContentPanel));
        }

        var Request = InvScroll.HandleRequest();
        if (Request != null)
          ServerContract.RequestScrollGoTo(ServerScroll.Tag, Request.Value);
      }

      return ServerScroll;
    }
    private ServerPanel TranslateShape(Inv.Panel InvPanel)
    {
      var InvShape = (Inv.Shape)InvPanel;

      var ServerShape = AccessPanel(InvShape, P =>
      {
        var Result = new ServerShape(NextPanelTag());

        ServerContract.NewShape(Result.Tag);

        return Result;
      });

      if (InvShape.Render())
      {
        TranslateLayout(InvShape, ServerShape);

        if (InvShape.Fit.Render())
          ServerContract.SetShapeFit(ServerShape.Tag, InvShape.Fit.Method);

        if (InvShape.Fill.Render())
          ServerContract.SetShapeFill(ServerShape.Tag, InvShape.Fill.Colour);

        if (InvShape.Stroke.Render())
          ServerContract.SetShapeStroke(ServerShape.Tag, InvShape.Stroke.Thickness, InvShape.Stroke.Colour, InvShape.Stroke.Cap, InvShape.Stroke.Join, InvShape.Stroke.DashPattern);

        if (InvShape.FigureCollection.Render())
        {
          ServerContract.SetShapeFigure(ServerShape.Tag, InvShape.FigureCollection.Select(F =>
          {
            var Result = new ServerShapeFigure();
            if (F.Polygon != null)
            {
              Result.Type = ServerShapeFigureType.Polygon;
              Result.PathArray = F.Polygon.PathArray.ToArray();
            }
            else if (F.Ellipse != null)
            {
              Result.Type = ServerShapeFigureType.Ellipse;
              Result.PathArray = [F.Ellipse.Center, F.Ellipse.Radius];
            }
            else if (F.Line != null)
            {
              Result.Type = ServerShapeFigureType.Line;
              Result.PathArray = [F.Line.Start, F.Line.End];
            }
            else
            {
              Result.Type = ServerShapeFigureType.Polygon;
              Result.PathArray = [];
            }
            return Result;
          }).ToArray());
        }
      }

      return ServerShape;
    }
    private ServerPanel TranslateStack(Inv.Panel InvPanel)
    {
      var InvStack = (Inv.Stack)InvPanel;

      var ServerStack = AccessPanel(InvStack, P =>
      {
        var Result = new ServerStack(NextPanelTag());
        Result.Orientation = P.Orientation;

        ServerContract.NewStack(Result.Tag, P.Orientation);

        return Result;
      });

      if (InvStack.Render())
      {
        TranslateLayout(InvStack, ServerStack);

        if (InvStack.Orientation != ServerStack.Orientation)
        {
          ServerContract.SetStackOrientation(ServerStack.Tag, InvStack.Orientation);

          ServerStack.Orientation = InvStack.Orientation;
        }

        if (InvStack.PanelCollection.Render())
        {
          var PanelArray = new ServerPanelTag[InvStack.PanelCollection.Count];
          var PanelIndex = 0;
          foreach (var InvElement in InvStack.PanelCollection)
            PanelArray[PanelIndex++] = TranslatePanelTag(InvElement);

          ServerContract.SetStackCollection(ServerStack.Tag, PanelArray);
        }
      }

      return ServerStack;
    }
    private ServerPanel TranslateSwitch(Inv.Panel InvPanel)
    {
      var InvSwitch = (Inv.Switch)InvPanel;

      var ServerSwitch = AccessPanel(InvSwitch, P =>
      {
        var Result = new ServerSwitch(NextPanelTag());

        ServerContract.NewSwitch(Result.Tag);

        return Result;
      });

      if (InvSwitch.Render())
      {
        TranslateLayout(InvSwitch, ServerSwitch);

        if (ServerSwitch.IsEnabled != InvSwitch.IsEnabled)
        {
          ServerSwitch.IsEnabled = InvSwitch.IsEnabled;
          ServerContract.SetSwitchIsEnabled(ServerSwitch.Tag, InvSwitch.IsEnabled);
        }

        if (ServerSwitch.IsOn != InvSwitch.IsOn)
        {
          ServerSwitch.IsOn = InvSwitch.IsOn;
          ServerContract.SetSwitchIsOn(ServerSwitch.Tag, InvSwitch.IsOn);
        }

        if (ServerSwitch.HasChange != InvSwitch.HasChange)
        {
          ServerSwitch.HasChange = InvSwitch.HasChange;
          ServerContract.SetSwitchHasChange(ServerSwitch.Tag, InvSwitch.HasChange);
        }
      }

      return ServerSwitch;
    }
    private ServerPanel TranslateTable(Inv.Panel InvPanel)
    {
      var InvTable = (Inv.Table)InvPanel;

      var ServerTable = AccessPanel(InvTable, P =>
      {
        var Result = new ServerTable(NextPanelTag());

        ServerContract.NewTable(Result.Tag);

        return Result;
      });

      if (InvTable.Render())
      {
        TranslateLayout(InvTable, ServerTable);

        if (InvTable.CollectionRender())
        {
          var RowArray = new ServerTableAxis[InvTable.RowCollection.Count];
          var RowIndex = 0;
          foreach (var Row in InvTable.RowCollection)
          {
            RowArray[RowIndex++] = new ServerTableAxis()
            {
              LengthType = Row.LengthType,
              LengthValue = Row.LengthValue,
              PanelTag = GetPanelTag(TranslatePanel(Row.Content))
            };
          }

          var ColumnArray = new ServerTableAxis[InvTable.ColumnCollection.Count];
          var ColumnIndex = 0;
          foreach (var Column in InvTable.ColumnCollection)
          {
            ColumnArray[ColumnIndex++] = new ServerTableAxis()
            {
              LengthType = Column.LengthType,
              LengthValue = Column.LengthValue,
              PanelTag = GetPanelTag(TranslatePanel(Column.Content))
            };
          }

          var CellArray = new ServerTableCell[InvTable.CellCollection.Count];
          var CellIndex = 0;
          foreach (var Cell in InvTable.CellCollection)
          {
            if (Cell.Content != null)
            {
              CellArray[CellIndex++] = new ServerTableCell()
              {
                X = Cell.X,
                Y = Cell.Y,
                PanelTag = TranslatePanelTag(Cell.Content)
              };
            }
          }

          ServerContract.SetTableCollection(ServerTable.Tag, RowArray, ColumnArray, CellArray);
        }
      }

      return ServerTable;
    }
    private ServerPanel TranslateVideo(Inv.Panel InvPanel)
    {
      var InvVideo = (Inv.Video)InvPanel;

      var ServerVideo = AccessPanel(InvVideo, P =>
      {
        var Result = new ServerVideo(NextPanelTag());

        ServerContract.NewVideo(Result.Tag);

        return Result;
      });

      if (InvVideo.Render())
      {
        TranslateLayout(InvVideo, ServerVideo);

        //if (InvVideo.AssetSingleton.Render())
        //  ServerContract.SetVideoAsset(ServerVideo.Tag, TranslateAssetTag(InvVideo.AssetSingleton.Data));
      }

      return ServerVideo;
    }
    private ServerPanel TranslateWrap(Inv.Panel InvPanel)
    {
      var InvWrap = (Inv.Wrap)InvPanel;

      var ServerWrap = AccessPanel(InvWrap, P =>
      {
        var Result = new ServerWrap(NextPanelTag());
        Result.Orientation = P.Orientation;

        ServerContract.NewWrap(Result.Tag, P.Orientation);

        return Result;
      });

      if (InvWrap.Render())
      {
        TranslateLayout(InvWrap, ServerWrap);

        if (InvWrap.Orientation != ServerWrap.Orientation)
        {
          ServerContract.SetWrapOrientation(ServerWrap.Tag, InvWrap.Orientation);

          ServerWrap.Orientation = InvWrap.Orientation;
        }

        if (InvWrap.PanelCollection.Render())
        {
          var PanelArray = new ServerPanelTag[InvWrap.PanelCollection.Count];
          var PanelIndex = 0;
          foreach (var InvElement in InvWrap.PanelCollection)
            PanelArray[PanelIndex++] = TranslatePanelTag(InvElement);

          ServerContract.SetWrapCollection(ServerWrap.Tag, PanelArray);
        }
      }

      return ServerWrap;
    }
    private void TranslateLayout(Inv.Control InvControl, ServerPanel ServerPanel)
    {
      var InvAlignment = InvControl.Alignment;
      if (InvAlignment.Render())
        ServerContract.SetPanelAlignment(ServerPanel.Tag, InvAlignment.Get());

      var InvBorder = InvControl.Border;
      if (InvBorder.Render())
        ServerContract.SetPanelBorder(ServerPanel.Tag, InvBorder.Left, InvBorder.Top, InvBorder.Right, InvBorder.Bottom, InvBorder.Colour);

      var InvCorner = InvControl.Corner;
      if (InvCorner.Render())
        ServerContract.SetPanelCorner(ServerPanel.Tag, InvCorner.TopLeft, InvCorner.TopRight, InvCorner.BottomRight, InvCorner.BottomLeft);

      var InvElevation = InvControl.Elevation;
      if (InvElevation.Render())
        ServerContract.SetPanelElevation(ServerPanel.Tag, InvElevation.Get());

      var InvMargin = InvControl.Margin;
      if (InvMargin.Render())
        ServerContract.SetPanelMargin(ServerPanel.Tag, InvMargin.Left, InvMargin.Top, InvMargin.Right, InvMargin.Bottom);

      var InvOpacity = InvControl.Opacity;
      if (InvOpacity.Render())
        ServerContract.SetPanelOpacity(ServerPanel.Tag, InvOpacity.Get());

      var InvPadding = InvControl.Padding;
      if (InvPadding.Render())
        ServerContract.SetPanelPadding(ServerPanel.Tag, InvPadding.Left, InvPadding.Top, InvPadding.Right, InvPadding.Bottom);

      var InvSize = InvControl.Size;
      if (InvSize.Render())
        ServerContract.SetPanelSize(ServerPanel.Tag, InvSize.Width, InvSize.Height, InvSize.MinimumWidth, InvSize.MinimumHeight, InvSize.MaximumWidth, InvSize.MaximumHeight);

      var InvVisibility = InvControl.Visibility;
      if (InvVisibility.Render())
        ServerContract.SetPanelVisibility(ServerPanel.Tag, InvVisibility.Get());

      var InvBackground = InvControl.Background;
      if (InvBackground.Render())
        ServerContract.SetPanelBackground(ServerPanel.Tag, InvBackground.Colour);

      if (ServerPanel.HasAdjust != InvControl.HasAdjust)
      {
        ServerPanel.HasAdjust = InvControl.HasAdjust;
        ServerContract.SetPanelHasAdjust(ServerPanel.Tag, InvControl.HasAdjust);
      }
    }
    private ServerPanelTag GetPanelTag(ServerPanel ServerPanel)
    {
      return ServerPanel != null ? ServerPanel.Tag : ServerPanelTag.Zero;
    }
    private ServerPanelTag TranslatePanelTag(Inv.Panel InvPanel)
    {
      var Result = TranslatePanel(InvPanel);

      return Result != null ? Result.Tag : ServerPanelTag.Zero;
    }
    private ServerImageTag TranslateImageTag(Inv.Image InvImage)
    {
      if (InvImage == null)
      {
        return ServerImageTag.Zero;
      }
      else
      {
        var ServerImage = InvImage.Node as ServerImage;

        if (ServerImage == null || ServerImage.Engine != ServerEngine)
        {
          ServerImage = ServerEngine.NextImage();
          InvImage.Node = ServerImage;
        }

        if (!ImageDictionary.ContainsKey(ServerImage.Tag))
        {
          ServerContract.NewImage(ServerImage.Tag, InvImage);

          ImageDictionary[ServerImage.Tag] = InvImage.AsWeakReference();
        }

        return ServerImage.Tag;
      }
    }
    private ServerSoundTag TranslateSoundTag(Inv.Sound InvSound)
    {
      if (InvSound == null)
      {
        return ServerSoundTag.Zero;
      }
      else
      {
        var ServerSound = InvSound.Node as ServerSound;

        if (ServerSound == null || ServerSound.Engine != ServerEngine)
        {
          ServerSound = ServerEngine.NextSound();
          InvSound.Node = ServerSound;
        }

        if (!SoundDictionary.ContainsKey(ServerSound.Tag))
        {
          ServerContract.NewSound(ServerSound.Tag, InvSound);

          SoundDictionary[ServerSound.Tag] = InvSound.AsWeakReference();
        }

        return ServerSound.Tag;
      }
    }
    private ServerClipTag TranslateClipTag(Inv.AudioClip InvClip)
    {
      if (InvClip == null)
      {
        return ServerClipTag.Zero;
      }
      else
      {
        var ServerClip = InvClip.Node as ServerClip;

        if (ServerClip == null || ServerClip.Engine != ServerEngine)
        {
          ServerClip = new ServerClip(ServerEngine, new ServerClipTag(++LastClipID));
          InvClip.Node = ServerClip;

          ClipDictionary[ServerClip.Tag] = InvClip;
        }

        return ServerClip.Tag;
      }
    }
    private ServerAnimationTag TranslateAnimationTag(Inv.Animation InvAnimation)
    {
      if (InvAnimation == null)
      {
        return ServerAnimationTag.Zero;
      }
      else
      {
        var ServerAnimation = (ServerAnimation)InvAnimation.Node;

        if (ServerAnimation == null || ServerAnimation.Engine != ServerEngine)
        {
          ServerAnimation = new ServerAnimation(ServerEngine, new ServerAnimationTag(++LastAnimationID));
          InvAnimation.Node = ServerAnimation;
        }

        return ServerAnimation.Tag;
      }
    }
    private ServerFont TranslateFont(Inv.Font InvFont)
    {
      return new ServerFont()
      {
        Name = InvFont.Name,
        Size = InvFont.Size,
        Colour = InvFont.Colour,
        Weight = InvFont.Weight,
        Axis = InvFont.Axis,
        SmallCaps = InvFont.IsSmallCaps,
        Underline = InvFont.IsUnderlined,
        Strikethrough = InvFont.IsStrikethrough,
        Italics = InvFont.IsItalics
      };
    }
    private ServerFocus TranslateFocus(Inv.Focus InvFocus)
    {
      return new ServerFocus()
      {
        HasGot = InvFocus.HasGot(),
        HasLost = InvFocus.HasLost()
      };
    }

    private ServerSurfaceTag NextSurfaceTag()
    {
      return new ServerSurfaceTag(++LastSurfaceID);
    }
    private ServerPanelTag NextPanelTag()
    {
      return new ServerPanelTag(++LastPanelID);
    }
    private Inv.Surface GetSurface(ServerSurfaceTag SurfaceTag)
    {
      var Reference = SurfaceDictionary.GetValueOrDefault(SurfaceTag);

      if (Reference == null)
        return null;

      if (!Reference.TryGetTarget(out Inv.Surface Result))
        return null;

      return Result;
    }
    private Inv.Control GetPanel(ServerPanelTag PanelTag)
    {
      var Reference = PanelDictionary.GetValueOrDefault(PanelTag);

      if (Reference == null)
        return null;

      if (!Reference.TryGetTarget(out var Result))
        return null;

      return Result;
    }
    private T GetPanel<T>(ServerPanelTag PanelTag)
      where T : Inv.Control
    {
      return (T)GetPanel(PanelTag);
    }
    private T RemovePicker<T>(ServerPickerTag PickerTag)
    {
      var Result = PickerDictionary.GetValueOrDefault(PickerTag);

      if (Result != null)
        PickerDictionary.Remove(PickerTag);

      return (T)Result;
    }
    private void GarbageDisposal<TKey, TValue>(Dictionary<TKey, WeakReference<TValue>> Dictionary, Action<TKey> DisposeAction)
      where TKey : struct
      where TValue : class
    {
      Inv.DistinctList<TKey> CompactList = null;

      foreach (var Entry in Dictionary)
      {
        if (!Entry.Value.TryGetTarget(out TValue Value))
        {
          if (CompactList == null)
            CompactList = [];

          CompactList.Add(Entry.Key);
        }
      }

      if (CompactList != null)
      {
        foreach (var Compact in CompactList)
        {
          Dictionary.Remove(Compact);

          DisposeAction(Compact);
        }
      }
    }
    private void GraphicsCanvasDraw(ServerCanvas ServerCanvas, Action<Inv.DrawContract> DrawAction)
    {
      ServerCanvas.Begin();

      DrawAction(ServerCanvas);

      var Result = ServerCanvas.End();
      if (Result != null) // has it changed since last time?
        ServerContract.DrawCanvas(ServerCanvas.Tag, Result);
    }
    private void ReceiveProcess(Action Action, ServerMessage Message, Action<CompactWriter> Writer = null)
    {
      Action();

      ProcessChanges();

      ServerSender.Return(Message, Writer);
    }

#pragma warning disable IDE0051 // Remove unused private members
    private void ReceiveSuspendApplication(CompactReader Reader)
    {
      ReceiveProcess(() =>
      {
        InvApplication.SuspendInvoke();
      }, ServerMessage.ConfirmSuspend);
    }
    private void ReceiveResumeApplication(CompactReader Reader)
    {
      ReceiveProcess(() =>
      {
        InvApplication.ResumeInvoke();
      }, ServerMessage.ConfirmResume);
    }
    private void ReceiveExitQueryApplication(CompactReader Reader)
    {
      var Result = false;

      ReceiveProcess(() =>
      {
        Result = InvApplication.ExitQueryInvoke();
      },
      ServerMessage.ConfirmExitQuery,
      Writer =>
      {
        Writer.WriteBoolean(Result);
      });
    }
    private void ReceiveHandleExceptionApplication(CompactReader Reader)
    {
      var ExceptionReport = Reader.ReadString();

      // TODO: reconstruct client exceptions?

      ReceiveProcess(() =>
      {
        InvApplication.HandleExceptionInvoke(new Exception(ExceptionReport));
      }, ServerMessage.ConfirmHandleException);
    }
    private void ReceiveKeyModifierKeyboard(CompactReader Reader)
    {
      var UpdateFlags = (KeyModifierFlags)Reader.ReadByte();
      var UpdateModifier = new KeyModifier(UpdateFlags);

      ReceiveProcess(() =>
      {
        InvApplication.Keyboard.CheckModifier(UpdateModifier);
      }, ServerMessage.ConfirmKeyModifierKeyboard);
    }
    private void ReceiveGestureBackwardSurface(CompactReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var Surface = GetSurface(SurfaceTag);

      ReceiveProcess(() =>
      {
        if (Surface != null)
          Surface.GestureBackwardInvoke();
      }, ServerMessage.ConfirmGestureBackwardSurface);
    }
    private void ReceiveGestureForwardSurface(CompactReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var Surface = GetSurface(SurfaceTag);

      ReceiveProcess(() =>
      {
        if (Surface != null)
          Surface.GestureForwardInvoke();
      }, ServerMessage.ConfirmGestureForwardSurface);
    }
    private void ReceiveKeystrokeKeyboardPress(CompactReader Reader)
    {
      var Key = (Inv.Key)Reader.ReadInt32();
      var Modifier = new Inv.KeyModifier((KeyModifierFlags)Reader.ReadByte());

      ReceiveProcess(() =>
      {
        InvApplication.Keyboard.KeyPress(new Keystroke(Key, Modifier));
      }, ServerMessage.ConfirmKeystrokeKeyboardPress);
    }
    private void ReceiveKeystrokeKeyboardRelease(CompactReader Reader)
    {
      var Key = (Inv.Key)Reader.ReadInt32();
      var Modifier = new Inv.KeyModifier((KeyModifierFlags)Reader.ReadByte());

      ReceiveProcess(() =>
      {
        InvApplication.Keyboard.KeyRelease(new Keystroke(Key, Modifier));
      }, ServerMessage.ConfirmKeystrokeKeyboardRelease);
    }
    private void ReceiveArrangeSurface(CompactReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var Surface = GetSurface(SurfaceTag);

      InvApplication.Window.Width = Reader.ReadInt32();
      InvApplication.Window.Height = Reader.ReadInt32();
      InvApplication.Device.PixelDensity = Reader.ReadFloat();

      ReceiveProcess(() =>
      {
        if (Surface != null)
          Surface.ArrangeInvoke();
      }, ServerMessage.ConfirmArrangeSurface);
    }
    private void ReceiveWindowActivate(CompactReader Reader)
    {
      ReceiveProcess(() =>
      {
        InvApplication.Window.ActivateInvoke();
      }, ServerMessage.ConfirmWindowActivate);
    }
    private void ReceiveWindowDeactivate(CompactReader Reader)
    {
      ReceiveProcess(() =>
      {
        InvApplication.Window.DeactivateInvoke();
      }, ServerMessage.ConfirmWindowDeactivate);
    }
    private void ReceiveAdjustPanel(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Panel = GetPanel(PanelTag);

      ReceiveProcess(() =>
      {
        if (Panel != null)
          Panel.AdjustInvoke();
      }, ServerMessage.ConfirmAdjustPanel);
    }
    private void ReceiveBrowserFetch(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Browser = GetPanel<Browser>(PanelTag);

      var Fetch = new BrowserFetch(Reader.ReadUri());

      ReceiveProcess(() =>
      {
        if (Browser != null)
          Browser.FetchInvoke(Fetch);
      }, ServerMessage.ConfirmBrowserFetch, Writer =>
      {
        Writer.WriteBoolean(Fetch.IsCancelled);
      });
    }
    private void ReceiveBrowserReady(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Browser = GetPanel<Browser>(PanelTag);

      var Ready = new BrowserReady(Reader.ReadUri());

      ReceiveProcess(() =>
      {
        if (Browser != null)
          Browser.ReadyInvoke(Ready);
      }, ServerMessage.ConfirmBrowserReady);
    }
    private void ReceiveButtonGotFocus(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Button = GetPanel<Button>(PanelTag);

      ReceiveProcess(() =>
      {
        if (Button != null)
          Button.Focus.GotInvoke();
      }, ServerMessage.ConfirmButtonGotFocus);
    }
    private void ReceiveButtonLostFocus(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Button = GetPanel<Button>(PanelTag);

      ReceiveProcess(() =>
      {
        if (Button != null)
          Button.Focus.LostInvoke();
      }, ServerMessage.ConfirmButtonLostFocus);
    }
    private void ReceiveButtonPress(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Button = GetPanel<Button>(PanelTag);

      ReceiveProcess(() =>
      {
        if (Button != null)
          Button.PressInvoke();
      }, ServerMessage.ConfirmButtonPress);
    }
    private void ReceiveButtonRelease(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Button = GetPanel<Button>(PanelTag);

      ReceiveProcess(() =>
      {
        if (Button != null)
          Button.ReleaseInvoke();
      }, ServerMessage.ConfirmButtonRelease);
    }
    private void ReceiveButtonSingleTap(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Button = GetPanel<Button>(PanelTag);

      ReceiveProcess(() =>
      {
        if (Button != null)
          Button.SingleTapInvoke();
      }, ServerMessage.ConfirmButtonSingleTap);
    }
    private void ReceiveButtonContextTap(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Button = GetPanel<Button>(PanelTag);

      ReceiveProcess(() =>
      {
        if (Button != null)
          Button.ContextTapInvoke();
      }, ServerMessage.ConfirmButtonContextTap);
    }
    private void ReceiveEditChange(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Text = Reader.ReadString();

      var Edit = GetPanel<Edit>(PanelTag);

      // the client already knows the text has been updated.
      // we need to fire the change event, but we don't need to call SetEditText back to the client.
      var ServerEdit = (ServerEdit)Edit?.Node;
      if (ServerEdit != null)
        ServerEdit.Text = Text;

      ReceiveProcess(() =>
      {
        if (Edit != null)
          Edit.ChangeText(Text);
      }, ServerMessage.ConfirmEditChange);
    }
    private void ReceiveEditReturn(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Edit = GetPanel<Edit>(PanelTag);

      ReceiveProcess(() =>
      {
        if (Edit != null)
          Edit.Return();
      }, ServerMessage.ConfirmEditReturn);
    }
    private void ReceiveEditGotFocus(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Edit = GetPanel<Edit>(PanelTag);

      ReceiveProcess(() =>
      {
        if (Edit != null)
          Edit.Focus.GotInvoke();
      }, ServerMessage.ConfirmEditGotFocus);
    }
    private void ReceiveEditLostFocus(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Edit = GetPanel<Edit>(PanelTag);

      ReceiveProcess(() =>
      {
        if (Edit != null)
          Edit.Focus.LostInvoke();
      }, ServerMessage.ConfirmEditLostFocus);
    }
    private void ReceiveMemoChange(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Text = Reader.ReadString();

      var Memo = GetPanel<Memo>(PanelTag);

      // the client already knows the text has been updated.
      // we need to fire the change event, but we don't need to call SetMemoText back to the client.
      var ServerMemo = (ServerMemo)Memo?.Node;
      if (ServerMemo != null)
        ServerMemo.Text = Text;

      ReceiveProcess(() =>
      {
        if (Memo != null)
          Memo.ChangeText(Text);
      }, ServerMessage.ConfirmMemoChange);
    }
    private void ReceiveMemoGotFocus(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Memo = GetPanel<Memo>(PanelTag);

      ReceiveProcess(() =>
      {
        if (Memo != null)
          Memo.Focus.GotInvoke();
      }, ServerMessage.ConfirmMemoGotFocus);
    }
    private void ReceiveMemoLostFocus(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Memo = GetPanel<Memo>(PanelTag);

      ReceiveProcess(() =>
      {
        if (Memo != null)
          Memo.Focus.LostInvoke();
      }, ServerMessage.ConfirmMemoLostFocus);
    }
    private void ReceiveCanvasPress(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var LeftMouseButton = Reader.ReadBoolean();
      var RightMouseButton = Reader.ReadBoolean();
      var PointX = Reader.ReadInt32();
      var PointY = Reader.ReadInt32();

      var Canvas = GetPanel<Canvas>(PanelTag);
      ReceiveProcess(() =>
      {
        Canvas?.PressInvoke(new Inv.CanvasCommand(new Inv.Point(PointX, PointY), LeftMouseButton, RightMouseButton));
      }, ServerMessage.ConfirmCanvasPress);
    }
    private void ReceiveCanvasRelease(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var LeftMouseButton = Reader.ReadBoolean();
      var RightMouseButton = Reader.ReadBoolean();
      var PointX = Reader.ReadInt32();
      var PointY = Reader.ReadInt32();

      var Canvas = GetPanel<Canvas>(PanelTag);
      ReceiveProcess(() =>
      {
        Canvas?.ReleaseInvoke(new Inv.CanvasCommand(new Inv.Point(PointX, PointY), LeftMouseButton, RightMouseButton));
      }, ServerMessage.ConfirmCanvasRelease);
    }
    private void ReceiveCanvasMove(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var LeftMouseButton = Reader.ReadBoolean();
      var RightMouseButton = Reader.ReadBoolean();
      var PointX = Reader.ReadInt32();
      var PointY = Reader.ReadInt32();

      var Canvas = GetPanel<Canvas>(PanelTag);
      ReceiveProcess(() =>
      {
        Canvas?.MoveInvoke(new Inv.CanvasCommand(new Inv.Point(PointX, PointY), LeftMouseButton, RightMouseButton));
      }, ServerMessage.ConfirmCanvasMove);
    }
    private void ReceiveCanvasSingleTap(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var PointX = Reader.ReadInt32();
      var PointY = Reader.ReadInt32();

      var Canvas = GetPanel<Canvas>(PanelTag);
      ReceiveProcess(() =>
      {
        Canvas?.SingleTapInvoke(new Inv.Point(PointX, PointY));
      }, ServerMessage.ConfirmCanvasSingleTap);
    }
    private void ReceiveCanvasDoubleTap(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var PointX = Reader.ReadInt32();
      var PointY = Reader.ReadInt32();

      var Canvas = GetPanel<Canvas>(PanelTag);

      ReceiveProcess(() =>
      {
        Canvas?.DoubleTapInvoke(new Inv.Point(PointX, PointY));
      }, ServerMessage.ConfirmCanvasDoubleTap);
    }
    private void ReceiveCanvasContextTap(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var PointX = Reader.ReadInt32();
      var PointY = Reader.ReadInt32();

      var Canvas = GetPanel<Canvas>(PanelTag);
      ReceiveProcess(() =>
      {
        Canvas?.ContextTapInvoke(new Inv.Point(PointX, PointY));
      }, ServerMessage.ConfirmCanvasContextTap);
    }
    private void ReceiveCanvasZoom(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var PointX = Reader.ReadInt32();
      var PointY = Reader.ReadInt32();
      var Delta = Reader.ReadInt32();

      var Canvas = GetPanel<Canvas>(PanelTag);
      ReceiveProcess(() =>
      {
        Canvas?.ZoomInvoke(new Inv.Zoom(new Inv.Point(PointX, PointY), Delta));
      }, ServerMessage.ConfirmCanvasZoom);
    }
    private void ReceiveCanvasMeasure(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Constraint = Reader.ReadDimension();

      var Measure = new Inv.CanvasMeasure(Constraint);

      var Canvas = GetPanel<Canvas>(PanelTag);
      ReceiveProcess(() =>
      {
        Canvas?.MeasureInvoke(Measure);
      }, ServerMessage.ConfirmCanvasMeasure, Writer =>
      {
        Writer.WriteBoolean(Measure.Dimension == null);
        if (Measure.Dimension != null)
          Writer.WriteDimension(Measure.Dimension.Value);
      });
    }
    private void ReceiveSwitchChange(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var IsOn = Reader.ReadBoolean();

      var Switch = GetPanel<Switch>(PanelTag);

      // the client already knows the text has been updated.
      // we need to fire the change event, but we don't need to call SetSwitchIsOn back to the client.
      var ServerSwitch = (ServerSwitch)Switch?.Node;
      if (ServerSwitch != null)
        ServerSwitch.IsOn = IsOn;

      ReceiveProcess(() =>
      {
        Switch?.UpdateChecked(IsOn);
      }, ServerMessage.ConfirmSwitchChange);
    }
    private void ReceiveCompleteAnimation(CompactReader Reader)
    {
      var AnimationTag = Reader.ReadAnimationTag();

      var Animation = AnimationDictionary.GetValueOrDefault(AnimationTag);
      if (Animation != null)
        AnimationDictionary.Remove(AnimationTag);

      ReceiveProcess(() =>
      {
        Animation?.Complete();
      }, ServerMessage.ConfirmCompleteAnimation);
    }
    private void ReceiveItemQueryFlow(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var SectionIndex = Reader.ReadInt32();
      var Start = Reader.ReadInt32();
      var Count = Reader.ReadInt32();

      var Flow = GetPanel<Flow>(PanelTag);

      var Section = Flow?.Sections[SectionIndex];

      var ResultArray = new ServerPanelTag[Count];

      ReceiveProcess(() =>
      {
        for (var Index = 0; Index < Count; Index++)
          ResultArray[Index] = TranslatePanelTag(Section?.ItemInvoke(Start + Index));
      },
      ServerMessage.ConfirmItemQueryFlow,
      Writer =>
      {
        foreach (var Result in ResultArray)
          Writer.WritePanelTag(Result);
      });
    }
    private void ReceiveSelectCalendarPicker(CompactReader Reader)
    {
      var PickerTag = Reader.ReadPickerTag();
      var Value = Reader.ReadDateTime();

      var CalendarPicker = RemovePicker<CalendarPicker>(PickerTag);

      ReceiveProcess(() =>
      {
        CalendarPicker.Value = Value;
        CalendarPicker.SelectInvoke();
      }, ServerMessage.ConfirmSelectCalendarPicker);
    }
    private void ReceiveCancelCalendarPicker(CompactReader Reader)
    {
      var PickerTag = Reader.ReadPickerTag();
      var Value = Reader.ReadDateTime();

      var CalendarPicker = RemovePicker<CalendarPicker>(PickerTag);

      ReceiveProcess(() =>
      {
        CalendarPicker.Value = Value;
        CalendarPicker.CancelInvoke();
      }, ServerMessage.ConfirmCancelCalendarPicker);
    }
    private void ReceiveSelectDirectoryPicker(CompactReader Reader)
    {
      var PickerTag = Reader.ReadPickerTag();
      var Name = Reader.ReadString();
      var Value = Reader.ReadBinary();

      var DirectoryPicker = RemovePicker<DirectoryFilePicker>(PickerTag);

      ReceiveProcess(() =>
      {
        if (DirectoryPicker != null)
          DirectoryPicker.SelectInvoke(new Inv.Pick(Name, () => new System.IO.MemoryStream(Value.GetBuffer())));
      }, ServerMessage.ConfirmSelectDirectoryPicker);
    }
    private void ReceiveCancelDirectoryPicker(CompactReader Reader)
    {
      var PickerTag = Reader.ReadPickerTag();

      var DirectoryPicker = RemovePicker<DirectoryFilePicker>(PickerTag);

      ReceiveProcess(() =>
      {
        if (DirectoryPicker != null)
          DirectoryPicker.CancelInvoke();
      }, ServerMessage.ConfirmCancelDirectoryPicker);
    }
#pragma warning restore IDE0051 // Remove unused private members

    private readonly ServerSender ServerSender;
    private readonly ServerContract ServerContract;
    private ServerSurface ActiveSurface;
    private ushort LastSurfaceID;
    private uint LastPanelID;
    private ushort LastAnimationID;
    private ushort LastPickerID;
    private uint LastClipID;
    private bool WindowInputPrevented;
    private readonly Inv.EnumArray<ClientMessage, Action<Inv.CompactReader>> ReceiveArray;
    private readonly Inv.EnumArray<Inv.ControlType, Func<Inv.Control, ServerPanel>> RouteArray;
    private readonly Dictionary<ServerSurfaceTag, WeakReference<Inv.Surface>> SurfaceDictionary;
    private readonly Dictionary<ServerPanelTag, WeakReference<Inv.Control>> PanelDictionary;
    private readonly Dictionary<ServerImageTag, WeakReference<Inv.Image>> ImageDictionary;
    private readonly Dictionary<ServerSoundTag, WeakReference<Inv.Sound>> SoundDictionary;
    private readonly Dictionary<ServerClipTag, Inv.AudioClip> ClipDictionary;
    private readonly Dictionary<ServerPickerTag, object> PickerDictionary;
    private readonly Dictionary<ServerAnimationTag, Inv.Animation> AnimationDictionary;
    private readonly List<Action> PostActionList;
  }

  /// <summary>
  /// The tenant's client-side application.
  /// </summary>
  public sealed class ServerApplication : Inv.Mimic<Inv.Application>, ServerContract
  {
    /// <summary>
    /// Create a new server application.
    /// </summary>
    /// <param name="Base"></param>
    /// <param name="Identity"></param>
    public ServerApplication(Inv.Application Base, Guid Identity)
    {
      this.Base = Base;
      this.Identity = Identity;
      this.SurfaceDictionary = [];
      this.PanelDictionary = [];
      this.ImageDictionary = [];
      this.SoundDictionary = [];
      this.AnimationDictionary = [];
      this.ClipDictionary = [];
      this.CanvasDrawPacketDictionary = [];

      Base.StartEvent += () => Start();
      Base.StopEvent += () => Stop();
    }

    /// <summary>
    /// Unique identity of this client.
    /// </summary>
    public Guid Identity { get; private set; }
    /// <summary>
    /// Ask if the application is started.
    /// </summary>
    public bool IsActive { get; private set; }
    /// <summary>
    /// Handle to be notified when the application exits.
    /// </summary>
    public event Action<bool> ExitEvent;

    /// <summary>
    /// Start the application and connect to the server.
    /// </summary>
    public void Start()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        this.WebClient = Base.Web.NewClient(Inv.ServerShell.SocketHost, Inv.ServerShell.SocketPort);
        WebClient.Connect();

        this.TransportSender = new Inv.TransportSender();
        this.ServerQueue = new ServerQueue(new Inv.TransportFlow(WebClient.TransportStream), "Application");

        var ServerContract = (ServerContract)this;
#if DEBUG
        //ServerContract = new ServerLog(ServerContract); // NOTE: may slow down debugging/execution considerably.
#endif
        this.ServerReceiver = new ServerReceiver(ServerContract);

        ServerQueue.Start();

        SendMessage(ClientMessage.Identification, Writer =>
        {
          Writer.WriteInt64(ServerShell.ProtocolVersion);
          Writer.WriteGuid(Identity);
        });

        // send introductory message.
        SendAndReceive(ClientMessage.StartApplication, Writer =>
        {
          Writer.WriteString(Base.Version);
          Writer.WriteByte((byte)Base.Device.Target);
          Writer.WriteString(Base.Device.Name);
          Writer.WriteString(Base.Device.Model);
          Writer.WriteString(Base.Device.Manufacturer);
          Writer.WriteString(Base.Device.System);
          Writer.WriteBoolean(Base.Device.Keyboard);
          Writer.WriteBoolean(Base.Device.Mouse);
          Writer.WriteBoolean(Base.Device.Touch);
          Writer.WriteString(Base.Device.ProportionalFontName);
          Writer.WriteString(Base.Device.MonospacedFontName);
          Writer.WriteFloat(Base.Device.PixelDensity);
          Writer.WriteByte((byte)Base.Device.Theme);

          Writer.WriteInt32(Base.Process.Id);

          Writer.WriteInt32(Base.Window.Width);
          Writer.WriteInt32(Base.Window.Height);

          Writer.WriteBoolean(Base.Phone.IsSupported);
          Writer.WriteBoolean(Base.Location.IsSupported);
          Writer.WriteBoolean(Base.Clipboard.IsTextSupported);
          Writer.WriteBoolean(Base.Clipboard.IsImageSupported);
          Writer.WriteBoolean(Base.Haptics.IsSupported);
          Writer.WriteString(Base.Calendar.GetTimeZoneName());
        }, ServerMessage.ConfirmStartApplication, Reader =>
        {
          Base.Title = Reader.ReadString();
        });

        Base.SuspendEvent += Suspend;
        Base.ResumeEvent += Resume;
        Base.HandleExceptionEvent += HandleException;
        Base.ExitQuery += ExitQuery;
        Base.Window.ProcessEvent += WindowProcess;
        Base.Window.ActivateEvent += WindowActivate;
        Base.Window.DeactivateEvent += WindowDeactivate;
        Base.Keyboard.KeyModifierEvent += KeyModifier;
        Base.Keyboard.KeyPressEvent += KeyPress;
        Base.Keyboard.KeyReleaseEvent += KeyRelease;

        this.AttachmentFolder = Base.Directory.Root.NewFolder("Attachments");
      }
    }
    /// <summary>
    /// Stop the application and disconnect from the server.
    /// </summary>
    public void Stop()
    {
      if (IsActive)
      {
        this.IsActive = false;

        Base.SuspendEvent -= Suspend;
        Base.ResumeEvent -= Resume;
        Base.HandleExceptionEvent -= HandleException;
        Base.ExitQuery -= ExitQuery;
        Base.Window.ProcessEvent -= WindowProcess;
        Base.Window.ActivateEvent -= WindowActivate;
        Base.Window.DeactivateEvent -= WindowDeactivate;
        Base.Keyboard.KeyModifierEvent -= KeyModifier;
        Base.Keyboard.KeyPressEvent -= KeyPress;
        Base.Keyboard.KeyReleaseEvent -= KeyRelease;

        // TODO: Flush pending messages to the server.

        WebClient.Disconnect();

        if (ServerQueue != null)
        {
          ServerQueue.Stop();
          this.ServerQueue = null;
        }

        // ensure that we stop any active clips.
        foreach (var ClipEntry in ClipDictionary)
          ClipEntry.Value.Stop();
      }
    }

    void ServerContract.ExitApplication()
    {
      ExitInvoke(WasDisconnected: false);
    }
    void ServerContract.SetWindowBackground(Colour Colour)
    {
      Base.Window.Background.Colour = Colour;
    }
    void ServerContract.SetKeyboardFocus(ServerPanelTag FocusTag)
    {
      Base.Keyboard.SetFocus(GetPanel(FocusTag));
    }
    void ServerContract.NewSurface(ServerSurfaceTag SurfaceTag)
    {
      var Surface = Base.Window.NewSurface();
      Surface.ArrangeEvent += () =>
      {
        SendAndReceive(ClientMessage.ArrangeSurface, Writer =>
        {
          Writer.WriteSurfaceTag(SurfaceTag);
          Writer.WriteInt32(Base.Window.Width);
          Writer.WriteInt32(Base.Window.Height);
          Writer.WriteFloat(Base.Device.PixelDensity);
        }, ServerMessage.ConfirmArrangeSurface);
      };

      SurfaceDictionary[SurfaceTag] = Surface;
    }
    void ServerContract.SetSurfaceHasGestureBackward(ServerSurfaceTag SurfaceTag, bool HasGestureBackward)
    {
      var Surface = GetSurface(SurfaceTag);

      if (HasGestureBackward)
        Surface.SetGestureBackward(() => SendAndReceive(ClientMessage.GestureBackwardSurface, Writer => Writer.WriteSurfaceTag(SurfaceTag), ServerMessage.ConfirmGestureBackwardSurface));
      else
        Surface.SetGestureBackward(null);
    }
    void ServerContract.SetSurfaceHasGestureForward(ServerSurfaceTag SurfaceTag, bool HasGestureForward)
    {
      var Surface = GetSurface(SurfaceTag);

      if (HasGestureForward)
        Surface.SetGestureForward(() => SendAndReceive(ClientMessage.GestureForwardSurface, Writer => Writer.WriteSurfaceTag(SurfaceTag), ServerMessage.ConfirmGestureForwardSurface));
      else
        Surface.SetGestureForward(null);
    }
    void ServerContract.SetSurfaceBackground(ServerSurfaceTag SurfaceTag, Colour Colour)
    {
      var Surface = GetSurface(SurfaceTag);
      Surface.Background.Colour = Colour;
    }
    void ServerContract.TransitionSurface(ServerSurfaceTag SurfaceTag, TransitionType TransitionType, TimeSpan TransitionDuration)
    {
      var Surface = GetSurface(SurfaceTag);

      var InvTransition = Base.Window.Transition(Surface);
      InvTransition.Duration = TransitionDuration;
      InvTransition.SetType(TransitionType);
    }
    void ServerContract.SetSurfaceContent(ServerSurfaceTag SurfaceTag, ServerPanelTag ContentTag)
    {
      var Surface = GetSurface(SurfaceTag);
      Surface.Content = UsePanel(ContentTag);
    }
    void ServerContract.SetWindowInputPrevented(bool InputPrevented, ServerPanelTag[] DisablePanelArray, ServerPanelTag[] EnablePanelArray)
    {
      if (InputPrevented)
        Base.Window.PreventInput();
      else
        Base.Window.AllowInput();

      foreach (var DisablePanel in DisablePanelArray)
        Base.Window.PreventInput(GetPanel(DisablePanel));

      foreach (var EnablePanel in EnablePanelArray)
        Base.Window.AllowInput(GetPanel(EnablePanel));
    }
    void ServerContract.NewBrowser(ServerPanelTag PanelTag)
    {
      var Browser = Inv.Browser.New();
      PanelDictionary[PanelTag] = Browser;

      Browser.FetchEvent += (Fetch) =>
      {
        SendAndReceive(ClientMessage.BrowserFetch, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteUri(Fetch.Uri);
        }, ServerMessage.ConfirmBrowserFetch, Reader =>
        {
          if (Reader.ReadBoolean())
            Fetch.Cancel();
        });
      };
      Browser.ReadyEvent += (Ready) =>
      {
        SendAndReceive(ClientMessage.BrowserReady, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteUri(Ready.Uri);
        }, ServerMessage.ConfirmBrowserReady);
      };
    }
    void ServerContract.LoadBrowser(ServerPanelTag PanelTag, Uri Uri, string Html)
    {
      var Browser = GetPanel<Browser>(PanelTag);

      if (Uri != null)
        Browser.LoadUri(Uri);
      else if (Html != null)
        Browser.LoadHtml(Html);
    }
    void ServerContract.NewButton(ServerPanelTag PanelTag, Inv.ButtonStyle Style)
    {
      var Button = Inv.Button.New(Style);
      PanelDictionary[PanelTag] = Button;

      Button.SingleTapEvent += () =>
      {
        SendAndReceive(ClientMessage.ButtonSingleTap, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
        }, ServerMessage.ConfirmButtonSingleTap);
      };
      Button.ContextTapEvent += () =>
      {
        SendAndReceive(ClientMessage.ButtonContextTap, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
        }, ServerMessage.ConfirmButtonContextTap);
      };
    }
    void ServerContract.SetButtonFocus(ServerPanelTag PanelTag, ServerFocus Focus)
    {
      var Button = GetPanel<Button>(PanelTag);
      ProcessFocus(PanelTag, Button.Focus, Focus, ClientMessage.ButtonGotFocus, ServerMessage.ConfirmButtonGotFocus, ClientMessage.ButtonLostFocus, ServerMessage.ConfirmButtonLostFocus);
    }
    void ServerContract.SetButtonContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      var Button = GetPanel<Button>(PanelTag);
      Button.Content = UsePanel(ContentTag);
    }
    void ServerContract.SetButtonIsEnabled(ServerPanelTag PanelTag, bool IsEnabled)
    {
      var Button = GetPanel<Button>(PanelTag);
      Button.IsEnabled = IsEnabled;
    }
    void ServerContract.SetButtonIsFocusable(ServerPanelTag PanelTag, bool IsFocusable)
    {
      var Button = GetPanel<Button>(PanelTag);
      Button.IsFocusable = IsFocusable;
    }
    void ServerContract.SetButtonHint(ServerPanelTag PanelTag, string Hint)
    {
      var Button = GetPanel<Button>(PanelTag);
      Button.Hint = Hint;
    }
    void ServerContract.SetButtonHasPress(ServerPanelTag PanelTag, bool HasPress)
    {
      var Button = GetPanel<Button>(PanelTag);

      if (HasPress)
        Button.SetPress(() => SendAndReceive(ClientMessage.ButtonPress, Writer => Writer.WritePanelTag(PanelTag), ServerMessage.ConfirmButtonPress));
      else
        Button.SetPress(null);
    }
    void ServerContract.SetButtonHasRelease(ServerPanelTag PanelTag, bool HasRelease)
    {
      var Button = GetPanel<Button>(PanelTag);

      if (HasRelease)
        Button.SetRelease(() => SendAndReceive(ClientMessage.ButtonRelease, Writer => Writer.WritePanelTag(PanelTag), ServerMessage.ConfirmButtonRelease));
      else
        Button.SetRelease(null);
    }
    void ServerContract.NewBlock(ServerPanelTag PanelTag)
    {
      var Block = Inv.Block.New();
      PanelDictionary[PanelTag] = Block;
    }
    void ServerContract.SetBlockCollection(ServerPanelTag PanelTag, ServerBlockSpan[] SpanArray)
    {
      var Block = GetPanel<Block>(PanelTag);

      Block.RemoveSpans();
      foreach (var Span in SpanArray)
      {
        var BlockSpan = Block.AddSpan(Span.Style, Span.Text);

        BlockSpan.Background.Colour = Span.BackgroundColour;

        ProcessFont(BlockSpan.Font, Span.Font);
      }
    }
    void ServerContract.SetBlockLineWrapping(ServerPanelTag PanelTag, bool LineWrapping)
    {
      var Block = GetPanel<Block>(PanelTag);
      Block.LineWrapping = LineWrapping;
    }
    void ServerContract.SetBlockJustification(ServerPanelTag PanelTag, Justification Justification)
    {
      var Block = GetPanel<Block>(PanelTag);
      Block.Justify.Set(Justification);
    }
    void ServerContract.SetBlockFont(ServerPanelTag PanelTag, ServerFont Font)
    {
      var Block = GetPanel<Block>(PanelTag);
      ProcessFont(Block.Font, Font);
    }
    void ServerContract.NewBoard(ServerPanelTag PanelTag)
    {
      var Board = Inv.Board.New();
      PanelDictionary[PanelTag] = Board;
    }
    void ServerContract.SetBoardCollection(ServerPanelTag PanelTag, ServerBoardPin[] PinArray)
    {
      var Board = GetPanel<Board>(PanelTag);

      Board.RemovePins();
      foreach (var Pink in PinArray)
        Board.AddPin(UsePanel(Pink.PanelTag), Pink.Rect);
    }
    void ServerContract.NewDock(ServerPanelTag PanelTag, Orientation Orientation)
    {
      var Dock = Inv.Dock.New(Orientation);
      PanelDictionary[PanelTag] = Dock;
    }
    void ServerContract.SetDockOrientation(ServerPanelTag PanelTag, Orientation Orientation)
    {
      var Dock = GetPanel<Dock>(PanelTag);
      Dock.SetOrientation(Orientation);
    }
    void ServerContract.SetDockCollection(ServerPanelTag PanelTag, ServerPanelTag[] HeaderArray, ServerPanelTag[] ClientArray, ServerPanelTag[] FooterArray)
    {
      var Dock = GetPanel<Dock>(PanelTag);

      Dock.ComposePanels(UsePanelArray(HeaderArray), UsePanelArray(ClientArray), UsePanelArray(FooterArray));
    }
    void ServerContract.NewEdit(ServerPanelTag PanelTag, EditInput Input)
    {
      var Edit = Inv.Edit.New(Input);
      PanelDictionary[PanelTag] = Edit;
    }
    void ServerContract.SetEditIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly)
    {
      var Edit = GetPanel<Edit>(PanelTag);
      Edit.IsReadOnly = ReadOnly;
    }
    void ServerContract.SetEditHint(ServerPanelTag PanelTag, string Hint)
    {
      var Button = GetPanel<Edit>(PanelTag);
      Button.Hint = Hint;
    }
    void ServerContract.SetEditHasChange(ServerPanelTag PanelTag, bool HasChange)
    {
      var Edit = GetPanel<Edit>(PanelTag);

      if (HasChange)
        Edit.SetChange(() => SendAndReceive(ClientMessage.EditChange, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteString(Edit.Text);
        }, ServerMessage.ConfirmEditChange));
      else
        Edit.SetChange(null);
    }
    void ServerContract.SetEditHasReturn(ServerPanelTag PanelTag, bool HasReturn)
    {
      var Edit = GetPanel<Edit>(PanelTag);

      if (HasReturn)
        Edit.SetReturn(() => SendAndReceive(ClientMessage.EditReturn, Writer => Writer.WritePanelTag(PanelTag), ServerMessage.ConfirmEditReturn));
      else
        Edit.SetReturn(null);
    }
    void ServerContract.SetEditFont(ServerPanelTag PanelTag, ServerFont Font)
    {
      var Edit = GetPanel<Edit>(PanelTag);
      ProcessFont(Edit.Font, Font);
    }
    void ServerContract.SetEditFocus(ServerPanelTag PanelTag, ServerFocus Focus)
    {
      var Edit = GetPanel<Edit>(PanelTag);
      ProcessFocus(PanelTag, Edit.Focus, Focus, ClientMessage.EditGotFocus, ServerMessage.ConfirmEditGotFocus, ClientMessage.EditLostFocus, ServerMessage.ConfirmEditLostFocus);
    }
    void ServerContract.SetEditText(ServerPanelTag PanelTag, string Text)
    {
      var Edit = GetPanel<Edit>(PanelTag);
      Edit.UpdateText(Text);
    }
    void ServerContract.NewFlow(ServerPanelTag PanelTag)
    {
      var Flow = Inv.Flow.New();
      PanelDictionary[PanelTag] = Flow;
    }
    void ServerContract.SetFlowFixture(ServerPanelTag PanelTag, ServerPanelTag FixtureTag)
    {
      var Flow = GetPanel<Flow>(PanelTag);
      if (Flow == null)
        return;

      Flow.SetFixture(UsePanel(FixtureTag));
    }
    void ServerContract.ReloadFlow(ServerPanelTag PanelTag, ServerFlowSection[] SectionArray)
    {
      var Flow = GetPanel<Flow>(PanelTag);
      if (Flow == null)
        return;

      Flow.RemoveSections();

      var Index = 0;
      foreach (var Section in SectionArray)
      {
        var SectionIndex = Index;

        var FlowSection = Flow.AddBatchedSection<ServerPanelTag>();
        FlowSection.RequestEvent += (Start, Count, Token, Callback) =>
        {
          // NOTE: this is expected to be in the UI thread.
          SendAndReceive(ClientMessage.ItemQueryFlow, Writer =>
          {
            Writer.WritePanelTag(PanelTag);
            Writer.WriteInt32(SectionIndex);
            Writer.WriteInt32(Start);
            Writer.WriteInt32(Count);
          }, ServerMessage.ConfirmItemQueryFlow, Reader =>
          {
            var ResultArray = new ServerPanelTag[Count];
            for (var ResultIndex = 0; ResultIndex < Count; ResultIndex++)
              ResultArray[ResultIndex] = Reader.ReadPanelTag();

            Callback(ResultArray);
          });
        };
        FlowSection.SetItemCount(Section.ItemCount);
        FlowSection.SetHeader(UsePanel(Section.HeaderPanelTag));
        FlowSection.SetFooter(UsePanel(Section.FooterPanelTag));
        FlowSection.ItemQuery += (ItemTag) => UsePanel(ItemTag);

        Index++;
      }

      Flow.Reload();
    }
    void ServerContract.NewFrame(ServerPanelTag PanelTag)
    {
      var Frame = Inv.Frame.New();
      PanelDictionary[PanelTag] = Frame;
    }
    void ServerContract.SetFrameContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      var Frame = GetPanel<Frame>(PanelTag);
      Frame.Content = UsePanel(ContentTag);
    }
    void ServerContract.SetFrameTransition(ServerPanelTag PanelTag, ServerPanelTag ContentTag, TransitionType TransitionType, TimeSpan TransitionDuration)
    {
      var Frame = GetPanel<Frame>(PanelTag);
      var Transition = Frame.Transition(UsePanel(ContentTag));
      Transition.Duration = TransitionDuration;
      Transition.SetType(TransitionType);
    }
    void ServerContract.NewGraphic(ServerPanelTag PanelTag)
    {
      var Graphic = Inv.Graphic.New();
      PanelDictionary[PanelTag] = Graphic;
    }
    void ServerContract.SetGraphicFit(ServerPanelTag PanelTag, FitMethod FitMethod)
    {
      var Graphic = GetPanel<Graphic>(PanelTag);
      if (Graphic != null)
        Graphic.Fit.Set(FitMethod);
    }
    void ServerContract.SetGraphicImage(ServerPanelTag PanelTag, ServerImageTag ImageTag)
    {
      var Graphic = GetPanel<Graphic>(PanelTag);
      if (Graphic != null)
        Graphic.Image = LookupImage(ImageTag);
    }
    void ServerContract.NewVideo(ServerPanelTag PanelTag)
    {
      var Video = Inv.Video.New();
      PanelDictionary[PanelTag] = Video;
    }
    void ServerContract.NewLabel(ServerPanelTag PanelTag)
    {
      var Label = Inv.Label.New();
      if (Label != null)
        PanelDictionary[PanelTag] = Label;
    }
    void ServerContract.SetLabelLineWrapping(ServerPanelTag PanelTag, bool LineWrapping)
    {
      var Label = GetPanel<Label>(PanelTag);
      if (Label != null)
        Label.LineWrapping = LineWrapping;
    }
    void ServerContract.SetLabelJustification(ServerPanelTag PanelTag, Justification Justification)
    {
      var Label = GetPanel<Label>(PanelTag);
      if (Label != null)
        Label.Justify.Set(Justification);
    }
    void ServerContract.SetLabelFont(ServerPanelTag PanelTag, ServerFont Font)
    {
      var Label = GetPanel<Label>(PanelTag);
      if (Label != null)
        ProcessFont(Label.Font, Font);
    }
    void ServerContract.SetLabelText(ServerPanelTag PanelTag, string Text)
    {
      var Label = GetPanel<Label>(PanelTag);
      if (Label != null)
        Label.Text = Text;
    }
    void ServerContract.NewMemo(ServerPanelTag PanelTag)
    {
      var Memo = Inv.Memo.New();
      PanelDictionary[PanelTag] = Memo;
    }
    void ServerContract.SetMemoIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly)
    {
      var Memo = GetPanel<Memo>(PanelTag);
      Memo.IsReadOnly = ReadOnly;
    }
    void ServerContract.SetMemoHasChange(ServerPanelTag PanelTag, bool HasChange)
    {
      var Memo = GetPanel<Memo>(PanelTag);

      if (HasChange)
        Memo.SetChange(() => SendAndReceive(ClientMessage.MemoChange, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteString(Memo.Text);
        }, ServerMessage.ConfirmMemoChange));
      else
        Memo.SetChange(null);
    }
    void ServerContract.SetMemoFont(ServerPanelTag PanelTag, ServerFont Font)
    {
      var Memo = GetPanel<Memo>(PanelTag);
      ProcessFont(Memo.Font, Font);
    }
    void ServerContract.SetMemoFocus(ServerPanelTag PanelTag, ServerFocus Focus)
    {
      var Memo = GetPanel<Memo>(PanelTag);
      ProcessFocus(PanelTag, Memo.Focus, Focus, ClientMessage.MemoGotFocus, ServerMessage.ConfirmMemoGotFocus, ClientMessage.MemoLostFocus, ServerMessage.ConfirmMemoLostFocus);
    }
    void ServerContract.SetMemoText(ServerPanelTag PanelTag, string Text)
    {
      var Memo = GetPanel<Memo>(PanelTag);
      Memo.UpdateText(Text);
    }
    void ServerContract.NewOverlay(ServerPanelTag PanelTag)
    {
      var Overlay = Inv.Overlay.New();
      PanelDictionary[PanelTag] = Overlay;
    }
    void ServerContract.SetOverlayCollection(ServerPanelTag PanelTag, ServerPanelTag[] PanelArray)
    {
      var Overlay = GetPanel<Overlay>(PanelTag);

      Overlay.ComposePanels(UsePanelArray(PanelArray));
    }
    void ServerContract.NewCanvas(ServerPanelTag PanelTag)
    {
      var Canvas = Inv.Canvas.New();
      Canvas.DrawEvent += (Context) =>
      {
        var Packet = CanvasDrawPacketDictionary.GetValueOrDefault(Canvas);

        if (Packet != null)
        {
          using (var TransportReceiver = Packet.ToReceiver())
          {
            var CompactReader = TransportReceiver.Reader;

            while (!TransportReceiver.EndOfPacket)
            {
              var RenderElement = (RenderMessage)CompactReader.ReadByte();

              switch (RenderElement)
              {
                case Inv.RenderMessage.Text:
                  var Text = CompactReader.ReadString();
                  var TextFontName = CompactReader.ReadString();
                  var TextFontSize = CompactReader.ReadInt16();
                  var TextFontWeight = (Inv.FontWeight)CompactReader.ReadByte();
                  var TextFontColour = CompactReader.ReadColour();
                  var TextX = CompactReader.ReadInt16();
                  var TextY = CompactReader.ReadInt16();
                  var TextHorizontal = (Inv.HorizontalPosition)CompactReader.ReadByte();
                  var TextVertical = (Inv.VerticalPosition)CompactReader.ReadByte();
                  var MaximumTextWidth = CompactReader.ReadInt32();
                  var MaximumTextHeight = CompactReader.ReadInt32();
                  Context.DrawText(Text, Inv.DrawFont.New(TextFontName, TextFontSize, TextFontWeight, TextFontColour), new Inv.Point(TextX, TextY), TextHorizontal, TextVertical, MaximumTextWidth, MaximumTextHeight);
                  break;

                case Inv.RenderMessage.Rectangle:
                  var RectangleFillColour = CompactReader.ReadColour();
                  var RectangleStrokeThickness = CompactReader.ReadInt16();
                  var RectangleStrokeColour = RectangleStrokeThickness > 0 ? CompactReader.ReadColour() : null;
                  var RectangleLeft = CompactReader.ReadInt16();
                  var RectangleTop = CompactReader.ReadInt16();
                  var RectangleWidth = CompactReader.ReadInt16();
                  var RectangleHeight = CompactReader.ReadInt16();
                  Context.DrawRectangle(RectangleFillColour, RectangleStrokeColour, RectangleStrokeThickness, new Inv.Rect(RectangleLeft, RectangleTop, RectangleWidth, RectangleHeight));
                  break;

                case Inv.RenderMessage.StraightRectangle:
                  var StraightRectangleFillColour = CompactReader.ReadColour();
                  var StraightRectangleStrokeThickness = CompactReader.ReadInt16();
                  var StraightRectangleStrokeColour = StraightRectangleStrokeThickness > 0 ? CompactReader.ReadColour() : null;
                  var StraightRectangleLeft = CompactReader.ReadInt16();
                  var StraightRectangleTop = CompactReader.ReadInt16();
                  var StraightRectangleSize = CompactReader.ReadInt16();
                  Context.DrawRectangle(StraightRectangleFillColour, StraightRectangleStrokeColour, StraightRectangleStrokeThickness, new Inv.Rect(StraightRectangleLeft, StraightRectangleTop, StraightRectangleSize, StraightRectangleSize));
                  break;

                case Inv.RenderMessage.Ellipse:
                  var EllipseFillColour = CompactReader.ReadColour();
                  var EllipseStrokeThickness = CompactReader.ReadInt16();
                  var EllipseStrokeColour = EllipseStrokeThickness > 0 ? CompactReader.ReadColour() : null;
                  var EllipseCenterX = CompactReader.ReadInt16();
                  var EllipseCenterY = CompactReader.ReadInt16();
                  var EllipseRadiusX = CompactReader.ReadInt16();
                  var EllipseRadiusY = CompactReader.ReadInt16();
                  Context.DrawEllipse(EllipseFillColour, EllipseStrokeColour, EllipseStrokeThickness, new Inv.Point(EllipseCenterX, EllipseCenterY), new Inv.Point(EllipseRadiusX, EllipseRadiusY));
                  break;

                case Inv.RenderMessage.Image:
                  var ImageTag = CompactReader.ReadImageTag();
                  var ImageLeft = CompactReader.ReadInt16();
                  var ImageTop = CompactReader.ReadInt16();
                  var ImageWidth = CompactReader.ReadInt16();
                  var ImageHeight = CompactReader.ReadInt16();
                  var ImageOpacity = CompactReader.ReadFloat();
                  var ImageTint = CompactReader.ReadColour();
                  var ImageMirror = CompactReader.ReadByte();
                  var ImageRotationPivot = CompactReader.ReadByte();

                  var ImageRotation = ImageRotationPivot == 255 ? (Inv.Rotation?)null :
                    new Inv.Rotation(CompactReader.ReadFloat(), (Inv.Pivot)ImageRotationPivot, ImageRotationPivot == (byte)Inv.Pivot.Custom ? new Inv.Point(CompactReader.ReadInt32(), CompactReader.ReadInt32()) : (Inv.Point?)null);

                  Context.DrawImage(LookupImage(ImageTag), new Inv.Rect(ImageLeft, ImageTop, ImageWidth, ImageHeight), ImageOpacity, ImageTint, ImageMirror == 255 ? (Inv.Mirror?)null : (Inv.Mirror)ImageMirror, ImageRotation);
                  break;

                case Inv.RenderMessage.StraightImage:
                  var StraightImageTag = CompactReader.ReadImageTag();
                  var StraightImageLeft = CompactReader.ReadInt16();
                  var StraightImageTop = CompactReader.ReadInt16();
                  var StraightImageSize = CompactReader.ReadInt16();
                  Context.DrawImage(LookupImage(StraightImageTag), new Inv.Rect(StraightImageLeft, StraightImageTop, StraightImageSize, StraightImageSize), 1.0F, null, null, null);
                  break;

                case Inv.RenderMessage.OpacityImage:
                  var OpacityImageTag = CompactReader.ReadImageTag();
                  var OpacityImageLeft = CompactReader.ReadInt16();
                  var OpacityImageTop = CompactReader.ReadInt16();
                  var OpacityImageSize = CompactReader.ReadInt16();
                  var OpacityImageOpacity = CompactReader.ReadFloat();
                  Context.DrawImage(LookupImage(OpacityImageTag), new Inv.Rect(OpacityImageLeft, OpacityImageTop, OpacityImageSize, OpacityImageSize), OpacityImageOpacity, null, null, null);
                  break;

                case Inv.RenderMessage.Line:
                  var LineStrokeThickness = CompactReader.ReadInt16();
                  var LineStrokeColour = LineStrokeThickness > 0 ? CompactReader.ReadColour() : null;
                  var LineJoin = LineStrokeThickness > 0 ? (Inv.LineJoin)CompactReader.ReadByte() : Inv.LineJoin.Miter;
                  var LineCap = LineStrokeThickness > 0 ? (Inv.LineCap)CompactReader.ReadByte() : Inv.LineCap.Butt;

                  var LineSourcePointX = CompactReader.ReadInt16();
                  var LineSourcePointY = CompactReader.ReadInt16();
                  var LineTargetPointX = CompactReader.ReadInt16();
                  var LineTargetPointY = CompactReader.ReadInt16();

                  var LineExtraPointLength = CompactReader.ReadInt16();
                  var LineExtraPointArray = new Inv.Point[LineExtraPointLength];
                  for (var Index = 0; Index < LineExtraPointLength; Index++)
                  {
                    var LineExtraPointX = CompactReader.ReadInt16();
                    var LineExtraPointY = CompactReader.ReadInt16();
                    LineExtraPointArray[Index] = new Inv.Point(LineExtraPointX, LineExtraPointY);
                  }
                  Context.DrawLine(LineStrokeColour, LineStrokeThickness, LineJoin, LineCap, new Inv.Point(LineSourcePointX, LineSourcePointY), new Inv.Point(LineTargetPointX, LineTargetPointY), LineExtraPointArray);
                  break;

                case Inv.RenderMessage.Arc:
                  var ArcFillColour = CompactReader.ReadColour();
                  var ArcStrokeThickness = CompactReader.ReadInt16();
                  var ArcStrokeColour = ArcStrokeThickness > 0 ? CompactReader.ReadColour() : null;
                  var ArcCenterX = CompactReader.ReadInt16();
                  var ArcCenterY = CompactReader.ReadInt16();
                  var ArcRadiusX = CompactReader.ReadInt16();
                  var ArcRadiusY = CompactReader.ReadInt16();
                  var ArcStartAngle = CompactReader.ReadFloat();
                  var ArcSweepAngle = CompactReader.ReadFloat();
                  Context.DrawArc(ArcFillColour, ArcStrokeColour, ArcStrokeThickness, new Inv.Point(ArcCenterX, ArcCenterY), new Inv.Point(ArcRadiusX, ArcRadiusY), ArcStartAngle, ArcSweepAngle);
                  break;

                case Inv.RenderMessage.Polygon:
                  var PolygonFillColour = CompactReader.ReadColour();
                  var PolygonStrokeThickness = CompactReader.ReadInt16();
                  var PolygonStrokeColour = PolygonStrokeThickness > 0 ? CompactReader.ReadColour() : null;
                  var PolygonLineJoin = PolygonStrokeThickness > 0 ? (Inv.LineJoin)CompactReader.ReadByte() : Inv.LineJoin.Miter;
                  var PolygonIsClosed = CompactReader.ReadBoolean();
                  var PolygonStartX = CompactReader.ReadInt16();
                  var PolygonStartY = CompactReader.ReadInt16();

                  var PolygonPointLength = CompactReader.ReadInt16();
                  var PolygonPointArray = new Inv.Point[PolygonPointLength];
                  for (var Index = 0; Index < PolygonPointLength; Index++)
                  {
                    var PolygonPointX = CompactReader.ReadInt16();
                    var PolygonPointY = CompactReader.ReadInt16();
                    PolygonPointArray[Index] = new Inv.Point(PolygonPointX, PolygonPointY);
                  }

                  Context.DrawPolygon(PolygonFillColour, PolygonStrokeColour, PolygonStrokeThickness, PolygonLineJoin, PolygonIsClosed, new Inv.Point(PolygonStartX, PolygonStartY), PolygonPointArray);
                  break;

                case RenderMessage.Mask:
                  var ViewportLeft = CompactReader.ReadInt16();
                  var ViewportTop = CompactReader.ReadInt16();
                  var ViewportWidth = CompactReader.ReadInt16();
                  var ViewportHeight = CompactReader.ReadInt16();

                  var MaskElementCount = CompactReader.ReadInt16();

                  Context.Mask(new Inv.Rect(ViewportLeft, ViewportTop, ViewportWidth, ViewportHeight), DM =>
                  {
                    for (var MaskElementIndex = 0; MaskElementIndex < MaskElementCount; MaskElementIndex++)
                    {
                      var MaskDrawType = (Inv.ServerDrawMaskType)CompactReader.ReadByte();

                      switch (MaskDrawType)
                      {
                        case ServerDrawMaskType.Image:
                          var ImageLeft = CompactReader.ReadInt16();
                          var ImageTop = CompactReader.ReadInt16();
                          var ImageWidth = CompactReader.ReadInt16();
                          var ImageHeight = CompactReader.ReadInt16();
                          var ImageTag = CompactReader.ReadImageTag();
                          var ImageOpacity = CompactReader.ReadFloat();
                          var ImageMirror = CompactReader.ReadByte();

                          DM.DrawImage(new Inv.Rect(ImageLeft, ImageTop, ImageWidth, ImageHeight), LookupImage(ImageTag), ImageOpacity, ImageMirror == 255 ? (Inv.Mirror?)null : (Inv.Mirror)ImageMirror);
                          break;

                        case ServerDrawMaskType.Rectangle:
                          var RectangleLeft = CompactReader.ReadInt16();
                          var RectangleTop = CompactReader.ReadInt16();
                          var RectangleWidth = CompactReader.ReadInt16();
                          var RectangleHeight = CompactReader.ReadInt16();
                          var RectangleFillColour = CompactReader.ReadColour();
                          var RectangleStrokeThickness = CompactReader.ReadInt16();
                          var RectangleStrokeColour = CompactReader.ReadColour();

                          DM.DrawRectangle(new Inv.Rect(RectangleLeft, RectangleTop, RectangleWidth, RectangleHeight), RectangleFillColour, RectangleStrokeThickness, RectangleStrokeColour);
                          break;

                        default:
                          throw Inv.Support.EnumHelper.UnexpectedValueException(MaskDrawType);
                      }
                    }
                  });
                  break;

                case RenderMessage.Unmask:
                  Context.Unmask();
                  break;

                default:
                  throw new Exception("RenderElement not handled: " + RenderElement);
              }
            }
          }
        }
      };
      Canvas.SingleTapEvent += (Point) =>
      {
        SendAndReceive(ClientMessage.CanvasSingleTap, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteInt32(Point.X);
          Writer.WriteInt32(Point.Y);
        }, ServerMessage.ConfirmCanvasSingleTap);
      };
      Canvas.DoubleTapEvent += (Point) =>
      {
        SendAndReceive(ClientMessage.CanvasDoubleTap, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteInt32(Point.X);
          Writer.WriteInt32(Point.Y);
        }, ServerMessage.ConfirmCanvasDoubleTap);
      };
      Canvas.ContextTapEvent += (Point) =>
      {
        SendAndReceive(ClientMessage.CanvasContextTap, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteInt32(Point.X);
          Writer.WriteInt32(Point.Y);
        }, ServerMessage.ConfirmCanvasContextTap);
      };
      Canvas.PressEvent += (Command) =>
      {
        SendAndReceive(ClientMessage.CanvasPress, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteBoolean(Command.LeftMouseButton);
          Writer.WriteBoolean(Command.RightMouseButton);
          Writer.WriteInt32(Command.Point.X);
          Writer.WriteInt32(Command.Point.Y);
        }, ServerMessage.ConfirmCanvasPress);
      };
      Canvas.ReleaseEvent += (Command) =>
      {
        SendAndReceive(ClientMessage.CanvasRelease, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteBoolean(Command.LeftMouseButton);
          Writer.WriteBoolean(Command.RightMouseButton);
          Writer.WriteInt32(Command.Point.X);
          Writer.WriteInt32(Command.Point.Y);
        }, ServerMessage.ConfirmCanvasRelease);
      };
      Canvas.MoveEvent += (Command) =>
      {
        SendAndReceive(ClientMessage.CanvasMove, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteBoolean(Command.LeftMouseButton);
          Writer.WriteBoolean(Command.RightMouseButton);
          Writer.WriteInt32(Command.Point.X);
          Writer.WriteInt32(Command.Point.Y);
        }, ServerMessage.ConfirmCanvasMove);
      };
      Canvas.ZoomEvent += (Zoom) =>
      {
        SendAndReceive(ClientMessage.CanvasZoom, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteInt32(Zoom.Point.X);
          Writer.WriteInt32(Zoom.Point.Y);
          Writer.WriteInt32(Zoom.Delta);
        }, ServerMessage.ConfirmCanvasZoom);
      };

      PanelDictionary[PanelTag] = Canvas;
    }
    void ServerContract.DrawCanvas(ServerPanelTag PanelTag, Inv.TransportPacket Packet)
    {
      var Canvas = GetPanel<Canvas>(PanelTag);

      if (Canvas != null)
      {
        CanvasDrawPacketDictionary[Canvas] = Packet;

        Canvas.InvalidateDraw();
      }
    }
    void ServerContract.SetCanvasHasMeasure(ServerPanelTag PanelTag, bool HasMeasure)
    {
      var Canvas = GetPanel<Canvas>(PanelTag);

      if (HasMeasure)
        Canvas.SetMeasure(Measure => SendAndReceive(ClientMessage.CanvasMeasure, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteDimension(Measure.Constraint);
        }, ServerMessage.ConfirmCanvasMeasure, Reader =>
        {
          if (!Reader.ReadBoolean())
            Measure.Set(Reader.ReadDimension());
        }));
      else
        Canvas.SetMeasure(null);
    }
    void ServerContract.NewScroll(ServerPanelTag PanelTag, Orientation Orientation)
    {
      var Scroll = Inv.Scroll.New(Orientation);
      PanelDictionary[PanelTag] = Scroll;
    }
    void ServerContract.SetScrollOrientation(ServerPanelTag PanelTag, Orientation Orientation)
    {
      var Scroll = GetPanel<Scroll>(PanelTag);
      Scroll.SetOrientation(Orientation);
    }
    void ServerContract.SetScrollContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      var Scroll = GetPanel<Scroll>(PanelTag);
      Scroll.Content = UsePanel(ContentTag);
    }
    void ServerContract.RequestScrollGoTo(ServerPanelTag PanelTag, ScrollRequest ScrollRequest)
    {
      var Scroll = GetPanel<Scroll>(PanelTag);
      Scroll.GoTo(ScrollRequest);
    }
    void ServerContract.NewShape(ServerPanelTag PanelTag)
    {
      var Shape = Inv.Shape.New();
      PanelDictionary[PanelTag] = Shape;
    }
    void ServerContract.SetShapeFit(ServerPanelTag PanelTag, FitMethod FitMethod)
    {
      var Shape = GetPanel<Shape>(PanelTag);
      Shape.Fit.Set(FitMethod);
    }
    void ServerContract.SetShapeFill(ServerPanelTag PanelTag, Inv.Colour Colour)
    {
      var Shape = GetPanel<Shape>(PanelTag);
      Shape.Fill.Colour = Colour;
    }
    void ServerContract.SetShapeStroke(ServerPanelTag PanelTag, int Thickness, Inv.Colour Colour, Inv.LineCap LineCap, Inv.LineJoin LineJoin, int[] DashPattern)
    {
      var Shape = GetPanel<Shape>(PanelTag);
      Shape.Stroke.Set(Thickness);
      Shape.Stroke.Colour = Colour;
      Shape.Stroke.Cap = LineCap;
      Shape.Stroke.Join = LineJoin;
      Shape.Stroke.Dash(DashPattern);
    }
    void ServerContract.SetShapeFigure(ServerPanelTag PanelTag, ServerShapeFigure[] FigureArray)
    {
      var Shape = GetPanel<Shape>(PanelTag);

      Shape.RemoveFigures();

      foreach (var Figure in FigureArray)
      {
        switch (Figure.Type)
        {
          case ServerShapeFigureType.Polygon:
            Shape.Polygon(Figure.PathArray);
            break;

          case ServerShapeFigureType.Ellipse:
            if (Figure.PathArray.Length == 2)
              Shape.Ellipse(Figure.PathArray[0], Figure.PathArray[1]);
            else
              Shape.Polygon(Figure.PathArray);
            break;

          case ServerShapeFigureType.Line:
            if (Figure.PathArray.Length == 2)
              Shape.Line(Figure.PathArray[0], Figure.PathArray[1]);
            else
              Shape.Polygon(Figure.PathArray);
            break;

          default:
            throw new Exception("ServerShapeFigureType not handled: " + Figure.Type);
        }
      }
    }
    void ServerContract.NewStack(ServerPanelTag PanelTag, Orientation Orientation)
    {
      var Stack = Inv.Stack.New(Orientation);
      PanelDictionary[PanelTag] = Stack;
    }
    void ServerContract.SetStackOrientation(ServerPanelTag PanelTag, Orientation Orientation)
    {
      var Stack = GetPanel<Stack>(PanelTag);
      Stack.SetOrientation(Orientation);
    }
    void ServerContract.SetStackCollection(ServerPanelTag PanelTag, ServerPanelTag[] PanelArray)
    {
      var Stack = GetPanel<Stack>(PanelTag);

      Stack.ComposePanels(UsePanelArray(PanelArray));
    }
    void ServerContract.NewSwitch(ServerPanelTag PanelTag)
    {
      var Switch = Inv.Switch.New();
      PanelDictionary[PanelTag] = Switch;
    }
    void ServerContract.SetSwitchHasChange(ServerPanelTag PanelTag, bool HasChange)
    {
      var Switch = GetPanel<Switch>(PanelTag);

      if (HasChange)
      {
        Switch.SetChange(() => SendAndReceive(ClientMessage.SwitchChange, Writer =>
        {
          Writer.WritePanelTag(PanelTag);
          Writer.WriteBoolean(Switch.IsOn);
        }, ServerMessage.ConfirmSwitchChange));
      }
      else
      {
        Switch.SetChange(null);
      }
    }
    void ServerContract.SetSwitchIsEnabled(ServerPanelTag PanelTag, bool IsEnabled)
    {
      var Switch = GetPanel<Switch>(PanelTag);
      Switch.IsEnabled = IsEnabled;
    }
    void ServerContract.SetSwitchIsOn(ServerPanelTag PanelTag, bool IsOn)
    {
      var Switch = GetPanel<Switch>(PanelTag);
      Switch.IsOn = IsOn;
    }
    void ServerContract.NewTable(ServerPanelTag PanelTag)
    {
      var Table = Inv.Table.New();
      PanelDictionary[PanelTag] = Table;
    }
    void ServerContract.SetTableCollection(ServerPanelTag PanelTag, ServerTableAxis[] RowArray, ServerTableAxis[] ColumnArray, ServerTableCell[] CellArray)
    {
      var Table = GetPanel<Table>(PanelTag);

      Table.RemovePanels();

      foreach (var Row in RowArray)
      {
        var TableRow = Table.AddRow();
        TableRow.Set(Row.LengthType, Row.LengthValue);
        TableRow.Content = UsePanel(Row.PanelTag);
      }

      foreach (var Column in ColumnArray)
      {
        var TableColumn = Table.AddColumn();
        TableColumn.Set(Column.LengthType, Column.LengthValue);
        TableColumn.Content = UsePanel(Column.PanelTag);
      }

      foreach (var Cell in CellArray)
        Table.GetCell(Cell.X, Cell.Y).Content = UsePanel(Cell.PanelTag);
    }
    void ServerContract.NewWrap(ServerPanelTag PanelTag, Orientation Orientation)
    {
      var Wrap = Inv.Wrap.New(Orientation);
      PanelDictionary[PanelTag] = Wrap;
    }
    void ServerContract.SetWrapOrientation(ServerPanelTag PanelTag, Orientation Orientation)
    {
      var Wrap = GetPanel<Wrap>(PanelTag);
      Wrap.SetOrientation(Orientation);
    }
    void ServerContract.SetWrapCollection(ServerPanelTag PanelTag, ServerPanelTag[] PanelArray)
    {
      var Wrap = GetPanel<Wrap>(PanelTag);
      Wrap.ComposePanels(UsePanelArray(PanelArray));
    }
    void ServerContract.SetPanelAlignment(ServerPanelTag PanelTag, Inv.Placement Placement)
    {
      var Panel = GetPanel(PanelTag);
      if (Panel != null)
        Panel.Alignment.Set(Placement);
    }
    void ServerContract.SetPanelBorder(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom, Colour Colour)
    {
      var Panel = GetPanel(PanelTag);
      if (Panel != null)
        Panel.Border.Set(Left, Top, Right, Bottom).In(Colour);
    }
    void ServerContract.SetPanelCorner(ServerPanelTag PanelTag, int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      var Panel = GetPanel(PanelTag);
      if (Panel != null)
        Panel.Corner.Set(TopLeft, TopRight, BottomRight, BottomLeft);
    }
    void ServerContract.SetPanelElevation(ServerPanelTag PanelTag, int Depth)
    {
      var Panel = GetPanel(PanelTag);
      if (Panel != null)
        Panel.Elevation.Set(Depth);
    }
    void ServerContract.SetPanelMargin(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom)
    {
      var Panel = GetPanel(PanelTag);
      if (Panel != null)
        Panel.Margin.Set(Left, Top, Right, Bottom);
    }
    void ServerContract.SetPanelOpacity(ServerPanelTag PanelTag, float Opacity)
    {
      var Panel = GetPanel(PanelTag);
      if (Panel != null)
        Panel.Opacity.Set(Opacity);
    }
    void ServerContract.SetPanelPadding(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom)
    {
      var Panel = GetPanel(PanelTag);
      if (Panel != null)
        Panel.Padding.Set(Left, Top, Right, Bottom);
    }
    void ServerContract.SetPanelSize(ServerPanelTag PanelTag, int? Width, int? Height, int? MinimumWidth, int? MinimumHeight, int? MaximumWidth, int? MaximumHeight)
    {
      var Panel = GetPanel(PanelTag);
      if (Panel != null)
        Panel.Size.Set(Width, Height, MinimumWidth, MinimumHeight, MaximumWidth, MaximumHeight);
    }
    void ServerContract.SetPanelVisibility(ServerPanelTag PanelTag, bool Visibility)
    {
      var Panel = GetPanel(PanelTag);
      if (Panel != null)
        Panel.Visibility.Set(Visibility);
    }
    void ServerContract.SetPanelBackground(ServerPanelTag PanelTag, Colour BackgroundColour)
    {
      var Panel = GetPanel(PanelTag);
      if (Panel != null)
        Panel.Background.Colour = BackgroundColour;
    }
    void ServerContract.SetPanelHasAdjust(ServerPanelTag PanelTag, bool HasAdjust)
    {
      var Panel = GetPanel(PanelTag);

      if (HasAdjust)
        Panel.SetAdjust(() => SendAndReceive(ClientMessage.AdjustPanel, Writer => Writer.WritePanelTag(PanelTag), ServerMessage.ConfirmAdjustPanel));
      else
        Panel.SetAdjust(null);
    }
    Inv.Dimension ServerContract.GetPanelDimension(ServerPanelTag PanelTag)
    {
      var Panel = GetPanel(PanelTag);

      var Result = Panel?.GetDimension() ?? Inv.Dimension.Zero;

      SendMessage(ClientMessage.ConfirmGetPanelDimension, Writer =>
      {
        Writer.WriteInt32(Result.Width);
        Writer.WriteInt32(Result.Height);
      });

      return Result;
    }
    void ServerContract.NewImage(ServerImageTag ImageTag, Inv.Image ImageSource)
    {
      ImageDictionary[ImageTag] = ImageSource;
    }
    void ServerContract.NewSound(ServerSoundTag SoundTag, Inv.Sound SoundSource)
    {
      SoundDictionary[SoundTag] = SoundSource;
    }
    void ServerContract.PlaySound(ServerSoundTag SoundTag, float SoundVolume, float SoundRate, float SoundPan)
    {
      Base.Audio.Play(LookupSound(SoundTag), SoundVolume, SoundRate, SoundPan);
    }
    void ServerContract.PlayClip(ServerClipTag ClipTag, ServerSoundTag SoundTag, float SoundVolume, float SoundRate, float SoundPan, bool SoundLoop)
    {
      var Clip = Base.Audio.NewClip(LookupSound(SoundTag), SoundVolume, SoundRate, SoundPan, SoundLoop);

      ClipDictionary[ClipTag] = Clip;

      Clip.Play();
    }
    void ServerContract.PauseClip(ServerClipTag ClipTag)
    {
      var Clip = ClipDictionary.GetValueOrDefault(ClipTag);
      if (Clip != null)
        Clip.Pause();
    }
    void ServerContract.ResumeClip(ServerClipTag ClipTag)
    {
      var Clip = ClipDictionary.GetValueOrDefault(ClipTag);
      if (Clip != null)
        Clip.Resume();
    }
    void ServerContract.StopClip(ServerClipTag ClipTag)
    {
      var Clip = ClipDictionary.GetValueOrDefault(ClipTag);

      if (Clip != null)
      {
        ClipDictionary.Remove(ClipTag);

        Clip.Stop();
      }
    }
    void ServerContract.ModulateClip(ServerClipTag ClipTag, float SoundVolume, float SoundRate, float SoundPan, bool SoundLoop)
    {
      var Clip = ClipDictionary.GetValueOrDefault(ClipTag);

      if (Clip != null)
        Clip.Set(SoundVolume, SoundRate, SoundPan, SoundLoop);
    }
    void ServerContract.StartAnimation(ServerAnimationTag AnimationTag, ServerAnimationTarget[] TargetArray)
    {
      var Animation = Inv.Animation.New();
      Animation.CompleteEvent += () =>
      {
        SendAndReceive(ClientMessage.CompleteAnimation, Writer =>
        {
          Writer.WriteAnimationTag(AnimationTag);
        }, ServerMessage.ConfirmCompleteAnimation);
      };

      foreach (var Target in TargetArray)
      {
        var Panel = GetPanel(Target.PanelTag);

        var AnimationTarget = Animation.AddTarget(Panel);

        foreach (var Transform in Target.TransformArray)
        {
          switch (Transform.Type)
          {
            case AnimationType.Fade:
              AnimationTarget.FadeOpacity(Transform.FadeFromOpacity, Transform.FadeToOpacity, Transform.FadeDuration, Transform.FadeOffset);
              break;

            case AnimationType.Rotate:
              AnimationTarget.RotateAngle(Transform.RotateFromAngle, Transform.RotateToAngle, Transform.RotateDuration, Transform.RotateOffset);
              break;

            case AnimationType.Scale:
              AnimationTarget.ScaleSize(Transform.ScaleFromWidth, Transform.ScaleToWidth, Transform.ScaleFromHeight, Transform.ScaleToHeight, Transform.ScaleDuration, Transform.ScaleOffset);
              break;

            case AnimationType.Translate:
              AnimationTarget.TranslatePosition(Transform.TranslateFromX, Transform.TranslateToX, Transform.TranslateFromY, Transform.TranslateToY, Transform.TranslateDuration, Transform.TranslateOffset);
              break;

            default:
              throw new Exception("AnimationType not handled: " + Transform.Type);
          }
        }
      }
      AnimationDictionary[AnimationTag] = Animation;

      Animation.Start();
    }
    void ServerContract.StopAnimation(ServerAnimationTag AnimationTag)
    {
      var Animation = AnimationDictionary.GetValueOrDefault(AnimationTag);

      if (Animation != null)
      {
        AnimationDictionary.Remove(AnimationTag);

        Animation.Stop();
      }
    }
    void ServerContract.BrowseMarket(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      Base.Market.Browse(AppleiTunesID, GooglePlayID, WindowsStoreID);
    }
    void ServerContract.LaunchWebUri(Uri Uri)
    {
      Base.Web.Launch(Uri);
    }
    void ServerContract.InstallWebUri(Uri Uri)
    {
      Base.Web.Install(Uri);
    }
    void ServerContract.ShowCalendarPicker(ServerPickerTag PickerTag, bool SetDate, bool SetTime, DateTime Value)
    {
      var CalendarPicker = Base.Calendar.NewPicker(SetDate, SetTime);
      CalendarPicker.Value = Value;
      CalendarPicker.SelectEvent += () =>
      {
        SendAndReceive(ClientMessage.SelectCalendarPicker, Writer =>
        {
          Writer.WritePickerTag(PickerTag);
          Writer.WriteDateTime(CalendarPicker.Value);
        }, ServerMessage.ConfirmSelectCalendarPicker);
      };
      CalendarPicker.CancelEvent += () =>
      {
        SendAndReceive(ClientMessage.CancelCalendarPicker, Writer =>
        {
          Writer.WritePickerTag(PickerTag);
          Writer.WriteDateTime(CalendarPicker.Value);
        }, ServerMessage.ConfirmCancelCalendarPicker);
      };
      CalendarPicker.Show();
    }
    void ServerContract.ShowDirectoryPicker(ServerPickerTag PickerTag, string Title, PickType FileType)
    {
      var FilePicker = Base.Directory.NewFilePicker(FileType);
      FilePicker.SelectEvent += (Pick) =>
      {
        SendAndReceive(ClientMessage.SelectDirectoryPicker, Writer =>
        {
          Writer.WritePickerTag(PickerTag);
          Writer.WriteString(Pick.Name);
          Writer.WriteBinary(Pick.ReadBinary());
        }, ServerMessage.ConfirmSelectDirectoryPicker);
      };
      FilePicker.CancelEvent += () =>
      {
        SendAndReceive(ClientMessage.CancelDirectoryPicker, Writer =>
        {
          Writer.WritePickerTag(PickerTag);
        }, ServerMessage.ConfirmCancelDirectoryPicker);
      };
      FilePicker.Show();
    }
    bool ServerContract.SendEmailMessage(string Subject, string Body, ServerEmailTo[] ToArray, ServerEmailAttachment[] AttachmentArray)
    {
      var EmailMessage = Base.Email.NewMessage();
      EmailMessage.Subject = Subject;
      EmailMessage.Body = Body;

      foreach (var To in ToArray)
        EmailMessage.To(To.Name, To.Address);

      foreach (var Attachment in AttachmentArray)
      {
        var File = AttachmentFolder.NewFile(Attachment.Name);
        File.WriteAllBytes(Attachment.Content);

        EmailMessage.Attach(Attachment.Name, File);
      }

      var Result = EmailMessage.Send();

      SendMessage(ClientMessage.ConfirmSendEmailMessage, Writer =>
      {
        Writer.WriteBoolean(Result);
      });

      return Result;
    }
    void ServerContract.DialPhone(string PhoneNumber)
    {
      Base.Phone.Dial(PhoneNumber);

      SendMessage(ClientMessage.ConfirmDialPhone);
    }
    void ServerContract.SMSPhone(string PhoneNumber)
    {
      Base.Phone.SMS(PhoneNumber);

      SendMessage(ClientMessage.ConfirmSMSPhone);
    }
    string ServerContract.ClipboardGetText()
    {
      var Result = Base.Clipboard.Text;

      SendMessage(ClientMessage.ConfirmClipboardGetText, Writer => Writer.WriteString(Result));

      return Result;
    }
    void ServerContract.ClipboardSetText(string Text)
    {
      Base.Clipboard.Text = Text;

      SendMessage(ClientMessage.ConfirmClipboardSetText);
    }
    Inv.Image ServerContract.ClipboardGetImage()
    {
      var Result = Base.Clipboard.Image;

      SendMessage(ClientMessage.ConfirmClipboardGetImage, Writer => Writer.WriteImage(Result));

      return Result;
    }
    void ServerContract.ClipboardSetImage(Inv.Image Image)
    {
      Base.Clipboard.Image = Image;

      SendMessage(ClientMessage.ConfirmClipboardSetImage);
    }
    void ServerContract.HapticFeedback(HapticFeedback Feedback)
    {
      Base.Haptics.Feedback(Feedback);

      SendMessage(ClientMessage.ConfirmHapticFeedback);
    }
    void ServerContract.DisposeSurface(ServerSurfaceTag SurfaceTag)
    {
      SurfaceDictionary.Remove(SurfaceTag);
    }
    void ServerContract.DisposePanel(ServerPanelTag PanelTag)
    {
      PanelDictionary.Remove(PanelTag);
    }
    void ServerContract.DisposeImage(ServerImageTag ImageTag)
    {
      ImageDictionary.Remove(ImageTag);
    }
    void ServerContract.DisposeSound(ServerSoundTag SoundTag)
    {
      SoundDictionary.Remove(SoundTag);
    }
    private void ExitInvoke(bool WasDisconnected)
    {
      if (ExitEvent != null)
        ExitEvent(WasDisconnected);
      else
        Base.Exit();
    }
    private void KeyModifier()
    {
      SendAndReceive(ClientMessage.KeyModifierKeyboard, Writer =>
      {
        Writer.WriteByte((byte)Base.Keyboard.KeyModifier.GetFlags());
      }, ServerMessage.ConfirmKeyModifierKeyboard);
    }
    private void KeyPress(Inv.Keystroke Keystroke)
    {
      SendAndReceive(ClientMessage.KeystrokeKeyboardPress, Writer =>
      {
        Writer.WriteInt32((int)Keystroke.Key);
        Writer.WriteByte((byte)Keystroke.Modifier.GetFlags());
      }, ServerMessage.ConfirmKeystrokeKeyboardPress);
    }
    private void KeyRelease(Inv.Keystroke Keystroke)
    {
      SendAndReceive(ClientMessage.KeystrokeKeyboardRelease, Writer =>
      {
        Writer.WriteInt32((int)Keystroke.Key);
        Writer.WriteByte((byte)Keystroke.Modifier.GetFlags());
      }, ServerMessage.ConfirmKeystrokeKeyboardRelease);
    }
    private bool ExitQuery()
    {
      // synchronously receive the response (true or false).
      var Result = false;

      SendAndReceive(ClientMessage.ExitQueryApplication, null, ServerMessage.ConfirmExitQuery, Reader =>
      {
        Result = Reader.ReadBoolean();
      });

      return Result;
    }
    private void HandleException(Exception Exception)
    {
      SendAndReceive(ClientMessage.HandleExceptionApplication, Writer =>
      {
        Writer.WriteString(Exception.AsReport());
      }, ServerMessage.ConfirmHandleException);
    }
    private void Resume()
    {
      SendAndReceive(ClientMessage.ResumeApplication, ServerMessage.ConfirmResume);
    }
    private void Suspend()
    {
      SendAndReceive(ClientMessage.SuspendApplication, ServerMessage.ConfirmSuspend);
    }
    private void SendAndReceive(ClientMessage ClientMessage, ServerMessage ServerMessage)
    {
      SendAndReceive(ClientMessage, null, ServerMessage, null);
    }
    private void SendAndReceive(ClientMessage ClientMessage, Action<CompactWriter> SendAction, ServerMessage ServerMessage, Action<CompactReader> ReceiveAction = null)
    {
      WindowProcess();

      SendMessage(ClientMessage, SendAction);
      ReceiveMessage(ServerMessage, ReceiveAction);
    }
    private void SendMessage(ClientMessage Message, Action<CompactWriter> Action = null)
    {
      TransportSender.Writer.WriteMessage(Message);

      Action?.Invoke(TransportSender.Writer);

      if (IsActive)
        ServerQueue.SendPacket(TransportSender.ToPacket());

      TransportSender.Reset();
    }
    private void ReceiveMessage(ServerMessage Message, Action<CompactReader> Action = null)
    {
      Base.RequireThreadAffinity();

      while (IsActive)
      {
        var Packet = ServerQueue.ReceivePacket();

        if (Packet == null)
        {
          // disconnected.
          // TODO: reconnect or display message on exit?

          ExitInvoke(true);
          return;
        }

        using (var TransportReceiver = Packet.ToReceiver())
        {
          var CompactReader = TransportReceiver.Reader;

          var NextMessage = CompactReader.ReadServerMessage();

          while (IsActive && NextMessage != Message)
          {
            ServerReceiver.ReceiveOne(NextMessage, CompactReader);

            if (TransportReceiver.EndOfPacket)
              break;

            NextMessage = CompactReader.ReadServerMessage();
          }

          if (IsActive && NextMessage == Message)
          {
            Action?.Invoke(CompactReader);

#if DEBUG
            // internal fault, should only found during development.
            if (!TransportReceiver.EndOfPacket)
              throw new Exception("The entire packet must be processed: " + NextMessage);
#endif

            break;
          }
        }
      }
    }
    private Surface GetSurface(ServerSurfaceTag SurfaceTag)
    {
      if (SurfaceTag.IsZero)
        return null;

      var Result = SurfaceDictionary.GetValueOrDefault(SurfaceTag);

      Debug.Assert(Result != null, "Surface not found for tag: " + SurfaceTag);

      return Result;
    }
    private Inv.Control GetPanel(ServerPanelTag PanelTag)
    {
      if (PanelTag.IsZero)
        return null;

      var Result = PanelDictionary.GetValueOrDefault(PanelTag);

      Debug.Assert(Result != null, "Panel not found for tag: " + PanelTag);

      return Result;
    }
    private Inv.Panel UsePanel(ServerPanelTag PanelTag)
    {
      var Result = GetPanel(PanelTag);

      if (Result != null && Result.Parent != null)
        Result.Unparent();

      return Result;
    }
    private Inv.Panel[] UsePanelArray(ServerPanelTag[] PanelTagArray)
    {
      var Result = new Inv.Panel[PanelTagArray.Length];

      var UseLength = 0;
      foreach (var PanelTag in PanelTagArray)
      {
        var Panel = UsePanel(PanelTag);

        if (Panel != null)
          Result[UseLength++] = Panel;
      }

      if (UseLength < Result.Length)
        Array.Resize(ref Result, UseLength);

      return Result;
    }
    private T GetPanel<T>(ServerPanelTag PanelTag)
      where T : Inv.Control
    {
      var Result = PanelDictionary.GetValueOrDefault(PanelTag);

      if (Result == null)
      {
        Debug.Fail("Unable to locate panel with tag: " + PanelTag);

        return null;
      }
      else
      {
        return (T)Result;
      }
    }
    private void WindowActivate()
    {
      Base.RequireThreadAffinity();

      SendAndReceive(ClientMessage.WindowActivate, ServerMessage.ConfirmWindowActivate);
    }
    private void WindowDeactivate()
    {
      Base.RequireThreadAffinity();

      SendAndReceive(ClientMessage.WindowDeactivate, ServerMessage.ConfirmWindowDeactivate);
    }
    private void WindowProcess()
    {
      Base.RequireThreadAffinity();

      if (IsActive)
      {
        var Packet = ServerQueue.TryReceivePacket();
        while (Packet != null)
        {
          ServerReceiver.ReceiveAll(Packet);

          if (IsActive)
            Packet = ServerQueue.TryReceivePacket();
          else
            Packet = null;
        }
      }
    }
    private Inv.Sound LookupSound(ServerSoundTag SoundTag)
    {
      return SoundTag.IsZero ? null : SoundDictionary.GetValueOrDefault(SoundTag);
    }
    private Inv.Image LookupImage(ServerImageTag ImageTag)
    {
      return ImageTag.IsZero ? null : ImageDictionary.GetValueOrDefault(ImageTag);
    }
    private void ProcessFont(Inv.Font InvFont, ServerFont ServerFont)
    {
      InvFont.Name = ServerFont.Name;
      InvFont.Size = ServerFont.Size;
      InvFont.Colour = ServerFont.Colour;
      InvFont.Weight = ServerFont.Weight;
      InvFont.Axis = ServerFont.Axis;
      InvFont.IsSmallCaps = ServerFont.SmallCaps;
      InvFont.IsUnderlined = ServerFont.Underline;
      InvFont.IsStrikethrough = ServerFont.Strikethrough;
      InvFont.IsItalics = ServerFont.Italics;
    }
    private void ProcessFocus(ServerPanelTag PanelTag, Inv.Focus InvFocus, ServerFocus ServerFocus, ClientMessage GotRequestMessage, ServerMessage GotResponseMessage, ClientMessage LostRequestMessage, ServerMessage LostResponseMessage)
    {
      if (ServerFocus.HasGot)
        InvFocus.SetGot(() => SendAndReceive(GotRequestMessage, Writer => Writer.WritePanelTag(PanelTag), GotResponseMessage));
      else
        InvFocus.SetGot(null);

      if (ServerFocus.HasLost)
        InvFocus.SetLost(() => SendAndReceive(LostRequestMessage, Writer => Writer.WritePanelTag(PanelTag), LostResponseMessage));
      else
        InvFocus.SetLost(null);
    }

    private WebClient WebClient;
    private ServerReceiver ServerReceiver;
    private ServerQueue ServerQueue;
    private Inv.TransportSender TransportSender;
    private Inv.Folder AttachmentFolder;
    private readonly Dictionary<ServerSurfaceTag, Inv.Surface> SurfaceDictionary;
    private readonly Dictionary<ServerPanelTag, Inv.Control> PanelDictionary;
    private readonly Dictionary<ServerImageTag, Inv.Image> ImageDictionary;
    private readonly Dictionary<ServerSoundTag, Inv.Sound> SoundDictionary;
    private readonly Dictionary<ServerClipTag, Inv.AudioClip> ClipDictionary;
    private readonly Dictionary<ServerAnimationTag, Inv.Animation> AnimationDictionary;
    private readonly Dictionary<Canvas, Inv.TransportPacket> CanvasDrawPacketDictionary;
  }

  internal sealed class ServerPlatform : Inv.Platform
  {
    public ServerPlatform(ServerTenant Tenant)
    {
      this.Tenant = Tenant;
      this.HostPlatform = Tenant.ServerEngine.HostApplication.Platform;
    }

    int Platform.ThreadAffinity()
    {
      return HostPlatform.ThreadAffinity();
    }
    string Platform.CalendarTimeZoneName()
    {
      return Tenant.CalendarTimeZoneName;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      Tenant.CalendarShowPicker(CalendarPicker);
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      return Tenant.EmailSendMessage(EmailMessage);
    }
    bool Platform.PhoneIsSupported
    {
      get => Tenant.PhoneIsSupported;
    }
    void Platform.PhoneDial(string PhoneNumber)
    {
      Tenant.PhoneDial(PhoneNumber);
    }
    void Platform.PhoneSMS(string PhoneNumber)
    {
      Tenant.PhoneSMS(PhoneNumber);
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      return HostPlatform.DirectoryGetLengthFile(File);
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      return HostPlatform.DirectoryGetLastWriteTimeUtcFile(File);
    }
    long Platform.DirectoryGetLengthAsset(Asset Asset)
    {
      return HostPlatform.DirectoryGetLengthAsset(Asset);
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcAsset(Asset Asset)
    {
      return HostPlatform.DirectoryGetLastWriteTimeUtcAsset(Asset);
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      HostPlatform.DirectorySetLastWriteTimeUtcFile(File, Timestamp);
    }
    System.IO.Stream Platform.DirectoryCreateFile(File File)
    {
      return HostPlatform.DirectoryCreateFile(File);
    }
    System.IO.Stream Platform.DirectoryAppendFile(File File)
    {
      return HostPlatform.DirectoryAppendFile(File);
    }
    System.IO.Stream Platform.DirectoryOpenFile(File File)
    {
      return HostPlatform.DirectoryOpenFile(File);
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      return HostPlatform.DirectoryExistsFile(File);
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      HostPlatform.DirectoryDeleteFile(File);
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      HostPlatform.DirectoryCopyFile(SourceFile, TargetFile);
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      HostPlatform.DirectoryMoveFile(SourceFile, TargetFile);
    }
    void Platform.DirectoryReplaceFile(File SourceFile, File TargetFile)
    {
      HostPlatform.DirectoryReplaceFile(SourceFile, TargetFile);
    }
    IEnumerable<Folder> Platform.DirectoryGetFolderSubfolders(Folder Folder, string FolderMask)
    {
      return HostPlatform.DirectoryGetFolderSubfolders(Folder, FolderMask);
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      return HostPlatform.DirectoryGetFolderFiles(Folder, FileMask);
    }
    IEnumerable<Asset> Platform.DirectoryGetAssets(string AssetMask)
    {
      return HostPlatform.DirectoryGetAssets(AssetMask);
    }
    System.IO.Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      return HostPlatform.DirectoryOpenAsset(Asset);
    }
    bool Platform.DirectoryExistsAsset(Asset Asset)
    {
      return HostPlatform.DirectoryExistsAsset(Asset);
    }
    void Platform.DirectoryShowFilePicker(DirectoryFilePicker FilePicker)
    {
      Tenant.DirectoryShowPicker(FilePicker);
    }
    string Platform.DirectoryGetFolderPath(Inv.Folder Folder)
    {
      return HostPlatform.DirectoryGetFolderPath(Folder);
    }
    string Platform.DirectoryGetFilePath(Inv.File File)
    {
      return HostPlatform.DirectoryGetFilePath(File);
    }
    bool Platform.LocationIsSupported
    {
      get => Tenant.LocationIsSupported;
    }
    void Platform.LocationShowMap(string Location)
    {
    }
    void Platform.LocationLookup(LocationResult LocationLookup)
    {
      Tenant.LocationLookup(LocationLookup);
    }
    void Platform.AudioPlaySound(Sound Sound, float Volume, float Rate, float Pan)
    {
      Tenant.PlaySound(Sound, Volume, Rate, Pan);
    }
    TimeSpan Platform.AudioGetSoundLength(Inv.Sound Sound)
    {
      // NOTE: don't need to ask the tenant to determine the sound length, we can do it server side.
      return HostPlatform.AudioGetSoundLength(Sound);
    }
    void Platform.AudioReclaim(IReadOnlyList<Inv.Sound> SoundList)
    {
      HostPlatform.AudioReclaim(SoundList);
    }
    void Platform.AudioPlayClip(AudioClip Clip)
    {
      Tenant.PlayClip(Clip);
    }
    void Platform.AudioPauseClip(AudioClip Clip)
    {
      Tenant.PauseClip(Clip);
    }
    void Platform.AudioResumeClip(AudioClip Clip)
    {
      Tenant.ResumeClip(Clip);
    }
    void Platform.AudioStopClip(AudioClip Clip)
    {
      Tenant.StopClip(Clip);
    }
    void Platform.AudioModulateClip(AudioClip Clip)
    {
      Tenant.ModulateClip(Clip);
    }
    void Platform.AudioStartSpeechRecognition(Inv.SpeechRecognition SpeechRecognition)
    {
      // TODO: tenant speechrecognition.
    }
    void Platform.AudioStopSpeechRecognition(Inv.SpeechRecognition SpeechRecognition)
    {
      // TODO: tenant speechrecognition.
    }
    bool Platform.AudioSpeechRecognitionIsSupported
    {
      get => HostPlatform.AudioSpeechRecognitionIsSupported; // TODO: tenant speechrecognition.
    }
    void Platform.WindowBrowse(File File)
    {
      Tenant.WindowBrowse(File);
    }
    void Platform.WindowShare(File File)
    {
      Tenant.WindowShare(File);
    }
    void Platform.WindowPost(Action Action)
    {
      Tenant.Post(Action);
    }
    void Platform.WindowCall(Action Action)
    {
      Tenant.Call(Action);
    }
    Inv.Dimension Platform.WindowGetDimension(Inv.Panel Panel)
    {
      return Tenant.WindowGetDimension(Panel);
    }
    void Platform.WindowStartAnimation(Inv.Animation Animation)
    {
      Tenant.StartAnimation(Animation);
    }
    void Platform.WindowStopAnimation(Inv.Animation Animation)
    {
      Tenant.StopAnimation(Animation);
    }
    void Platform.WindowShowPopup(Inv.Popup Popup)
    {
      Tenant.ShowPopup(Popup);
    }
    void Platform.WindowHidePopup(Inv.Popup Popup)
    {
      Tenant.HidePopup(Popup);
    }
    long Platform.ProcessMemoryUsedBytes()
    {
      return HostPlatform.ProcessMemoryUsedBytes();
    }
    string Platform.ProcessAncillaryInformation()
    {
      return HostPlatform.ProcessAncillaryInformation();
    }
    void Platform.WebClientConnect(WebClient WebClient)
    {
      HostPlatform.WebClientConnect(WebClient);
    }
    void Platform.WebClientDisconnect(WebClient WebClient)
    {
      HostPlatform.WebClientDisconnect(WebClient);
    }
    void Platform.WebServerConnect(WebServer WebServer)
    {
      HostPlatform.WebServerConnect(WebServer);
    }
    void Platform.WebServerDisconnect(WebServer WebServer)
    {
      HostPlatform.WebServerDisconnect(WebServer);
    }
    void Platform.WebBroadcastConnect(WebBroadcast WebBroadcast)
    {
      HostPlatform.WebBroadcastConnect(WebBroadcast);
    }
    void Platform.WebBroadcastDisconnect(WebBroadcast WebBroadcast)
    {
      HostPlatform.WebBroadcastDisconnect(WebBroadcast);
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      Tenant.WebLaunchUri(Uri);
    }
    void Platform.WebInstallUri(Uri Uri)
    {
      Tenant.WebInstallUri(Uri);
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      Tenant.MarketBrowse(AppleiTunesID, GooglePlayID, WindowsStoreID);
    }
    void Platform.VaultLoadSecret(Secret Secret)
    {
      HostPlatform.VaultLoadSecret(Secret);
    }
    void Platform.VaultSaveSecret(Secret Secret)
    {
      HostPlatform.VaultSaveSecret(Secret);
    }
    void Platform.VaultDeleteSecret(Secret Secret)
    {
      HostPlatform.VaultDeleteSecret(Secret);
    }
    bool Platform.ClipboardIsTextSupported
    {
      get => Tenant.ClipboardIsTextSupported;
    }
    string Platform.ClipboardGetText()
    {
      return Tenant.ClipboardGetText();
    }
    void Platform.ClipboardSetText(string Text)
    {
      Tenant.ClipboardSetText(Text);
    }
    bool Platform.ClipboardIsImageSupported
    {
      get => Tenant.ClipboardIsImageSupported;
    }
    Inv.Image Platform.ClipboardGetImage()
    {
      return Tenant.ClipboardGetImage();
    }
    void Platform.ClipboardSetImage(Inv.Image Image)
    {
      Tenant.ClipboardSetImage(Image);
    }
    void Platform.HapticFeedback(HapticFeedback Feedback)
    {
      Tenant.HapticFeedback(Feedback);
    }
    Inv.Dimension Platform.GraphicsGetDimension(Inv.Image Image)
    {
      return HostPlatform.GraphicsGetDimension(Image);
    }
    Inv.Image Platform.GraphicsGrayscale(Inv.Image Image)
    {
      return HostPlatform.GraphicsGrayscale(Image);
    }
    Inv.Image Platform.GraphicsTint(Inv.Image Image, Inv.Colour Colour)
    {
      return HostPlatform.GraphicsTint(Image, Colour);
    }
    Inv.Image Platform.GraphicsResize(Inv.Image Image, Inv.Dimension Dimension)
    {
      return HostPlatform.GraphicsResize(Image, Dimension);
    }
    Inv.Pixels Platform.GraphicsReadPixels(Inv.Image Image)
    {
      return HostPlatform.GraphicsReadPixels(Image);
    }
    Inv.Image Platform.GraphicsWritePixels(Inv.Pixels Pixels)
    {
      return HostPlatform.GraphicsWritePixels(Pixels);
    }
    void Platform.GraphicsRender(Inv.Panel Panel, Action<Inv.Image> ReturnAction)
    {
      HostPlatform.GraphicsRender(Panel, ReturnAction);
    }
    void Platform.GraphicsReclaim(IReadOnlyList<Inv.Image> ImageList)
    {
      HostPlatform.GraphicsReclaim(ImageList);
    }
    Inv.Dimension Platform.CanvasCalculateText(string TextFragment, Inv.DrawFont TextFont, int MaximumTextWidth, int MaximumTextHeight)
    {
      return HostPlatform.CanvasCalculateText(TextFragment, TextFont, MaximumTextWidth, MaximumTextHeight);
    }

    private readonly ServerTenant Tenant;
    private readonly Platform HostPlatform;
  }

  internal interface ServerContract
  {
    void ExitApplication();
    void SetWindowBackground(Colour Colour);
    void SetKeyboardFocus(ServerPanelTag FocusTag);
    void SetWindowInputPrevented(bool InputPrevented, ServerPanelTag[] DisablePanelArray, ServerPanelTag[] EnablePanelArray);
    void NewSurface(ServerSurfaceTag SurfaceTag);
    void SetSurfaceHasGestureBackward(ServerSurfaceTag SurfaceTag, bool HasGestureBackward);
    void SetSurfaceHasGestureForward(ServerSurfaceTag SurfaceTag, bool HasGestureForward);
    void DisposeSurface(ServerSurfaceTag SurfaceTag);
    void DisposePanel(ServerPanelTag PanelTag);
    void SetSurfaceBackground(ServerSurfaceTag SurfaceTag, Colour Colour);
    void SetSurfaceContent(ServerSurfaceTag SurfaceTag, ServerPanelTag ContentTag);
    void TransitionSurface(ServerSurfaceTag SurfaceTag, TransitionType TransitionType, TimeSpan TransitionDuration);
    void NewBrowser(ServerPanelTag PanelTag);
    void LoadBrowser(ServerPanelTag PanelTag, Uri Uri, string Html);
    void NewButton(ServerPanelTag PanelTag, Inv.ButtonStyle Style);
    void SetButtonFocus(ServerPanelTag PanelTag, ServerFocus Focus);
    void SetButtonContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag);
    void SetButtonIsEnabled(ServerPanelTag PanelTag, bool IsEnabled);
    void SetButtonIsFocusable(ServerPanelTag PanelTag, bool IsFocusable);
    void SetButtonHint(ServerPanelTag PanelTag, string Hint);
    void SetButtonHasPress(ServerPanelTag PanelTag, bool HasPress);
    void SetButtonHasRelease(ServerPanelTag PanelTag, bool HasRelease);
    void NewBlock(ServerPanelTag PanelTag);
    void SetBlockCollection(ServerPanelTag PanelTag, ServerBlockSpan[] SpanArray);
    void SetBlockLineWrapping(ServerPanelTag PanelTag, bool LineWrapping);
    void SetBlockJustification(ServerPanelTag PanelTag, Justification Justification);
    void SetBlockFont(ServerPanelTag PanelTag, ServerFont Font);
    void NewBoard(ServerPanelTag PanelTag);
    void SetBoardCollection(ServerPanelTag PanelTag, ServerBoardPin[] PinArray);
    void NewDock(ServerPanelTag PanelTag, Orientation Orientation);
    void SetDockOrientation(ServerPanelTag PanelTag, Orientation Orientation);
    void SetDockCollection(ServerPanelTag PanelTag, ServerPanelTag[] HeaderArray, ServerPanelTag[] ClientArray, ServerPanelTag[] FooterArray);
    void NewEdit(ServerPanelTag PanelTag, EditInput Input);
    void SetEditFont(ServerPanelTag PanelTag, ServerFont Font);
    void SetEditFocus(ServerPanelTag PanelTag, ServerFocus Focus);
    void SetEditText(ServerPanelTag PanelTag, string Text);
    void SetEditHint(ServerPanelTag PanelTag, string Hint);
    void SetEditIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly);
    void SetEditHasChange(ServerPanelTag PanelTag, bool HasChange);
    void SetEditHasReturn(ServerPanelTag PanelTag, bool HasReturn);
    void NewFlow(ServerPanelTag PanelTag);
    void SetFlowFixture(ServerPanelTag PanelTag, ServerPanelTag FixtureTag);
    void ReloadFlow(ServerPanelTag PanelTag, ServerFlowSection[] SectionArray);
    void NewFrame(ServerPanelTag PanelTag);
    void SetFrameContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag);
    void SetFrameTransition(ServerPanelTag PanelTag, ServerPanelTag ContentTag, TransitionType TransitionType, TimeSpan TransitionDuration);
    void NewGraphic(ServerPanelTag PanelTag);
    void SetGraphicFit(ServerPanelTag PanelTag, FitMethod FitMethod);
    void SetGraphicImage(ServerPanelTag PanelTag, ServerImageTag ImageTag);
    void NewVideo(ServerPanelTag PanelTag);
    void NewLabel(ServerPanelTag PanelTag);
    void SetLabelLineWrapping(ServerPanelTag PanelTag, bool LineWrapping);
    void SetLabelJustification(ServerPanelTag PanelTag, Justification Justification);
    void SetLabelFont(ServerPanelTag PanelTag, ServerFont Font);
    void SetLabelText(ServerPanelTag PanelTag, string Text);
    void NewMemo(ServerPanelTag PanelTag);
    void SetMemoFont(ServerPanelTag PanelTag, ServerFont Font);
    void SetMemoFocus(ServerPanelTag PanelTag, ServerFocus Focus);
    void SetMemoText(ServerPanelTag PanelTag, string Text);
    void SetMemoIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly);
    void SetMemoHasChange(ServerPanelTag PanelTag, bool HasChange);
    void NewOverlay(ServerPanelTag PanelTag);
    void SetOverlayCollection(ServerPanelTag PanelTag, ServerPanelTag[] PanelArray);
    void NewCanvas(ServerPanelTag PanelTag);
    void DrawCanvas(ServerPanelTag PanelTag, Inv.TransportPacket Packet);
    void SetCanvasHasMeasure(ServerPanelTag PanelTag, bool HasMeasure);
    void NewScroll(ServerPanelTag PanelTag, Orientation Orientation);
    void SetScrollOrientation(ServerPanelTag PanelTag, Orientation Orientation);
    void SetScrollContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag);
    void RequestScrollGoTo(ServerPanelTag PanelTag, ScrollRequest ScrollRequest);
    void NewShape(ServerPanelTag PanelTag);
    void SetShapeFit(ServerPanelTag PanelTag, Inv.FitMethod FitMethod);
    void SetShapeFill(ServerPanelTag PanelTag, Inv.Colour Colour);
    void SetShapeStroke(ServerPanelTag PanelTag, int Thickness, Inv.Colour Colour, Inv.LineCap LineCap, Inv.LineJoin LineJoin, int[] DashPattern);
    void SetShapeFigure(ServerPanelTag PanelTag, ServerShapeFigure[] FigureArray);
    void NewStack(ServerPanelTag PanelTag, Orientation Orientation);
    void SetStackOrientation(ServerPanelTag PanelTag, Orientation Orientation);
    void SetStackCollection(ServerPanelTag PanelTag, ServerPanelTag[] PanelArray);
    void NewSwitch(ServerPanelTag PanelTag);
    void SetSwitchIsEnabled(ServerPanelTag PanelTag, bool IsEnabled);
    void SetSwitchIsOn(ServerPanelTag PanelTag, bool IsOn);
    void SetSwitchHasChange(ServerPanelTag PanelTag, bool HasChange);
    void NewTable(ServerPanelTag PanelTag);
    void SetTableCollection(ServerPanelTag PanelTag, ServerTableAxis[] RowArray, ServerTableAxis[] ColumnArray, ServerTableCell[] CellArray);
    void NewWrap(ServerPanelTag PanelTag, Orientation Orientation);
    void SetWrapOrientation(ServerPanelTag PanelTag, Orientation Orientation);
    void SetWrapCollection(ServerPanelTag PanelTag, ServerPanelTag[] PanelArray);
    void SetPanelAlignment(ServerPanelTag PanelTag, Inv.Placement Placement);
    void SetPanelBorder(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom, Colour Colour);
    void SetPanelCorner(ServerPanelTag PanelTag, int TopLeft, int TopRight, int BottomRight, int BottomLeft);
    void SetPanelElevation(ServerPanelTag PanelTag, int Depth);
    void SetPanelMargin(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom);
    void SetPanelOpacity(ServerPanelTag PanelTag, float Opacity);
    void SetPanelPadding(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom);
    void SetPanelSize(ServerPanelTag PanelTag, int? Width, int? Height, int? MinimumWidth, int? MinimumHeight, int? MaximumWidth, int? MaximumHeight);
    void SetPanelVisibility(ServerPanelTag PanelTag, bool Visibility);
    void SetPanelBackground(ServerPanelTag PanelTag, Colour BackgroundColour);
    void SetPanelHasAdjust(ServerPanelTag PanelTag, bool HasAdjust);
    Dimension GetPanelDimension(ServerPanelTag PanelTag);
    void NewImage(ServerImageTag ImageTag, Inv.Image Image);
    void DisposeImage(ServerImageTag ImageTag);
    void NewSound(ServerSoundTag SoundTag, Inv.Sound Sound);
    void DisposeSound(ServerSoundTag SoundTag);
    void PlaySound(ServerSoundTag SoundTag, float SoundVolume, float SoundRate, float SoundPan);
    void PlayClip(ServerClipTag ClipTag, ServerSoundTag SoundTag, float SoundVolume, float SoundRate, float SoundPan, bool SoundLoop);
    void ModulateClip(ServerClipTag ClipTag, float SoundVolume, float SoundRate, float SoundPan, bool SoundLoop);
    void PauseClip(ServerClipTag ClipTag);
    void ResumeClip(ServerClipTag ClipTag);
    void StopClip(ServerClipTag ClipTag);
    void StartAnimation(ServerAnimationTag AnimationTag, ServerAnimationTarget[] TargetArray);
    void StopAnimation(ServerAnimationTag AnimationTag);
    void BrowseMarket(string AppleiTunesID, string GooglePlayID, string WindowsStoreID);
    void LaunchWebUri(Uri Uri);
    void InstallWebUri(Uri Uri);
    void ShowCalendarPicker(ServerPickerTag PickerTag, bool SetDate, bool SetTime, DateTime Value);
    void ShowDirectoryPicker(ServerPickerTag PickerTag, string Title, PickType FileType);
    bool SendEmailMessage(string Subject, string Body, ServerEmailTo[] ToArray, ServerEmailAttachment[] AttachmentArray);
    void DialPhone(string PhoneNumber);
    void SMSPhone(string PhoneNumber);
    string ClipboardGetText();
    void ClipboardSetText(string Text);
    Inv.Image ClipboardGetImage();
    void ClipboardSetImage(Inv.Image Image);
    void HapticFeedback(HapticFeedback Feedback);
  }

  internal enum ServerMessage
  {
    Invalid,
    ConfirmStartApplication,
    ConfirmSuspend,
    ConfirmResume,
    ConfirmHandleException,
    ConfirmExitQuery,
    ConfirmKeyModifierKeyboard,
    ConfirmArrangeSurface,
    ConfirmGestureBackwardSurface,
    ConfirmGestureForwardSurface,
    ConfirmKeystrokeKeyboardPress,
    ConfirmKeystrokeKeyboardRelease,
    ConfirmAdjustPanel,
    ConfirmBrowserFetch,
    ConfirmBrowserReady,
    ConfirmButtonGotFocus,
    ConfirmButtonLostFocus,
    ConfirmButtonPress,
    ConfirmButtonRelease,
    ConfirmButtonSingleTap,
    ConfirmButtonContextTap,
    ConfirmEditChange,
    ConfirmEditReturn,
    ConfirmEditGotFocus,
    ConfirmEditLostFocus,
    ConfirmItemQueryFlow,
    ConfirmMemoGotFocus,
    ConfirmMemoLostFocus,
    ConfirmMemoChange,
    ConfirmCanvasSingleTap,
    ConfirmCanvasDoubleTap,
    ConfirmCanvasContextTap,
    ConfirmCanvasPress,
    ConfirmCanvasRelease,
    ConfirmCanvasMove,
    ConfirmCanvasZoom,
    ConfirmCanvasMeasure,
    ConfirmSwitchChange,
    ConfirmCompleteAnimation,
    ConfirmSelectCalendarPicker,
    ConfirmCancelCalendarPicker,
    ConfirmSelectDirectoryPicker,
    ConfirmCancelDirectoryPicker,
    ConfirmWindowActivate,
    ConfirmWindowDeactivate,
    ExitApplication,
    SetWindowBackground,
    SetKeyboardFocus,
    SetWindowInputPrevented,
    NewSurface,
    SetSurfaceHasGestureBackward,
    SetSurfaceHasGestureForward,
    SetSurfaceBackground,
    SetSurfaceContent,
    TransitionSurface,
    NewBrowser,
    LoadBrowser,
    NewButton,
    SetButtonFocus,
    SetButtonContent,
    SetButtonIsEnabled,
    SetButtonIsFocusable,
    SetButtonHint,
    SetButtonHasPress,
    SetButtonHasRelease,
    NewBoard,
    SetBoardCollection,
    NewBlock,
    SetBlockCollection,
    SetBlockJustification,
    SetBlockLineWrapping,
    SetBlockFont,
    NewHorizontalDock,
    NewVerticalDock,
    NewEdit,
    SetEditText,
    SetEditHint,
    SetEditIsReadOnly,
    SetEditHasChange,
    SetEditHasReturn,
    SetEditFont,
    SetEditFocus,
    NewFlow,
    ReloadFlow,
    SetFlowFixture,
    NewFrame,
    SetFrameContent,
    SetFrameTransition,
    NewGraphic,
    SetGraphicFit,
    SetGraphicImage,
    NewVideo,
    SetDockOrientation,
    SetDockCollection,
    NewLabel,
    SetLabelText,
    SetLabelJustification,
    SetLabelLineWrapping,
    SetLabelFont,
    NewMemo,
    SetMemoText,
    SetMemoIsReadOnly,
    SetMemoHasChange,
    SetMemoFont,
    SetMemoFocus,
    NewOverlay,
    SetOverlayCollection,
    NewCanvas,
    DrawCanvas,
    SetCanvasHasMeasure,
    NewHorizontalScroll,
    NewVerticalScroll,
    SetScrollOrientation,
    SetScrollContent,
    RequestScrollGoTo,
    NewShape,
    SetShapeFit,
    SetShapeFill,
    SetShapeStroke,
    SetShapeFigure,
    NewHorizontalStack,
    NewVerticalStack,
    SetStackOrientation,
    SetStackCollection,
    NewSwitch,
    SetSwitchHasChange,
    SetSwitchIsEnabled,
    SetSwitchIsOn,
    NewTable,
    SetTableCollection,
    NewHorizontalWrap,
    NewVerticalWrap,
    SetWrapOrientation,
    SetWrapCollection,
    SetPanelAlignment,
    SetPanelBackground,
    SetPanelSize,
    SetPanelMargin,
    SetPanelPadding,
    SetPanelBorder,
    SetPanelOpacity,
    SetPanelElevation,
    SetPanelCorner,
    SetPanelVisibility,
    SetPanelHasAdjust,
    GetPanelDimension,
    NewImage,
    NewSound,
    PlaySound,
    PlayClip,
    PauseClip,
    ResumeClip,
    StopClip,
    ModulateClip,
    StartAnimation,
    StopAnimation,
    BrowseMarket,
    LaunchWebUri,
    InstallWebUri,
    ShowCalendarPicker,
    ShowDirectoryPicker,
    SendEmailMessage,
    DialPhone,
    SMSPhone,
    ClipboardGetText,
    ClipboardSetText,
    ClipboardGetImage,
    ClipboardSetImage,
    HapticFeedback,
    DisposeSurface,
    DisposePanel,
    DisposeImage,
    DisposeSound
  }

  internal enum ClientMessage
  {
    Invalid, // do not move this value.
    Identification, // do not move this value.
    ConfirmSendEmailMessage,
    ConfirmDialPhone,
    ConfirmSMSPhone,
    ConfirmGetPanelDimension,
    ConfirmClipboardGetText,
    ConfirmClipboardSetText,
    ConfirmClipboardGetImage,
    ConfirmClipboardSetImage,
    ConfirmHapticFeedback,
    StartApplication,
    SuspendApplication,
    ResumeApplication,
    HandleExceptionApplication,
    ExitQueryApplication,
    KeyModifierKeyboard,
    ArrangeSurface,
    GestureBackwardSurface,
    GestureForwardSurface,
    KeystrokeKeyboardPress,
    KeystrokeKeyboardRelease,
    AdjustPanel,
    BrowserFetch,
    BrowserReady,
    ButtonGotFocus,
    ButtonLostFocus,
    ButtonPress,
    ButtonRelease,
    ButtonSingleTap,
    ButtonContextTap,
    EditChange,
    EditReturn,
    EditGotFocus,
    EditLostFocus,
    ItemQueryFlow,
    MemoChange,
    MemoGotFocus,
    MemoLostFocus,
    CanvasSingleTap,
    CanvasDoubleTap,
    CanvasContextTap,
    CanvasPress,
    CanvasRelease,
    CanvasMove,
    CanvasZoom,
    CanvasMeasure,
    SwitchChange,
    CompleteAnimation,
    SelectCalendarPicker,
    CancelCalendarPicker,
    SelectDirectoryPicker,
    CancelDirectoryPicker,
    WindowActivate,
    WindowDeactivate
  }

  internal static class ServerFoundation
  {
    public static void WriteSurfaceTag(this CompactWriter Writer, ServerSurfaceTag SurfaceTag)
    {
      Writer.WriteUInt16(SurfaceTag.ID);
    }
    public static ServerSurfaceTag ReadSurfaceTag(this CompactReader Reader)
    {
      return new ServerSurfaceTag(Reader.ReadUInt16());
    }
    public static ServerPanelTag ReadPanelTag(this CompactReader Reader)
    {
      return new ServerPanelTag(Reader.ReadUInt32());
    }
    public static void WritePanelTag(this CompactWriter Writer, ServerPanelTag PanelTag)
    {
      Writer.WriteUInt32(PanelTag.ID);
    }
    public static void WriteMessage(this CompactWriter Writer, ServerMessage Message)
    {
      Writer.WriteByte((byte)Message);
    }
    public static ServerMessage ReadServerMessage(this CompactReader Reader)
    {
      return (ServerMessage)Reader.ReadByte();
    }
    public static void WriteMessage(this CompactWriter Writer, ClientMessage Message)
    {
      Writer.WriteByte((byte)Message);
    }
    public static ClientMessage ReadClientMessage(this CompactReader Reader)
    {
      return (ClientMessage)Reader.ReadByte();
    }
    public static void WriteAnimationTag(this CompactWriter Writer, ServerAnimationTag AnimationTag)
    {
      Writer.WriteUInt16(AnimationTag.ID);
    }
    public static ServerAnimationTag ReadAnimationTag(this CompactReader Reader)
    {
      return new ServerAnimationTag(Reader.ReadUInt16());
    }
    public static void WriteImageTag(this CompactWriter Writer, ServerImageTag ImageTag)
    {
      Writer.WriteUInt16(ImageTag.ID);
    }
    public static ServerImageTag ReadImageTag(this CompactReader Reader)
    {
      return new ServerImageTag(Reader.ReadUInt16());
    }
    public static void WriteSoundTag(this CompactWriter Writer, ServerSoundTag SoundTag)
    {
      Writer.WriteUInt16(SoundTag.ID);
    }
    public static ServerSoundTag ReadSoundTag(this CompactReader Reader)
    {
      return new ServerSoundTag(Reader.ReadUInt16());
    }
    public static void WriteClipTag(this CompactWriter Writer, ServerClipTag ClipTag)
    {
      Writer.WriteUInt32(ClipTag.ID);
    }
    public static ServerClipTag ReadClipTag(this CompactReader Reader)
    {
      return new ServerClipTag(Reader.ReadUInt32());
    }
    public static void WritePickerTag(this CompactWriter Writer, ServerPickerTag PickerTag)
    {
      Writer.WriteUInt16(PickerTag.ID);
    }
    public static ServerPickerTag ReadPickerTag(this CompactReader Reader)
    {
      return new ServerPickerTag(Reader.ReadUInt16());
    }
    public static WeakReference<T> AsWeakReference<T>(this T Target)
      where T : class
    {
      return new WeakReference<T>(Target);
    }
    public static Inv.Dimension ReadDimension(this CompactReader Reader)
    {
      return new Inv.Dimension(Reader.ReadInt32(), Reader.ReadInt32());
    }
    public static void WriteDimension(this CompactWriter Writer, Inv.Dimension Dimension)
    {
      Writer.WriteInt32(Dimension.Width);
      Writer.WriteInt32(Dimension.Height);
    }
  }

  internal sealed class ServerSender : ServerContract
  {
    public ServerSender(ServerTenant Tenant)
    {
      this.ServerTenant = Tenant;
      this.TransportSender = new Inv.TransportSender();
      this.CompactWriter = TransportSender.Writer;
    }

    public void Return(ServerMessage Message, Action<CompactWriter> Action = null)
    {
      CompactWriter.WriteMessage(Message);

      Action?.Invoke(CompactWriter);

      Send();
    }
    public void Send()
    {
      if (TransportSender.HasData)
      {
        ServerTenant.ServerQueue.SendPacket(TransportSender.ToPacket());
        TransportSender.Reset();
      }
    }

    void ServerContract.ExitApplication()
    {
      CompactWriter.WriteMessage(ServerMessage.ExitApplication);
    }
    void ServerContract.SetWindowBackground(Colour Colour)
    {
      CompactWriter.WriteMessage(ServerMessage.SetWindowBackground);
      CompactWriter.WriteColour(Colour);
    }
    void ServerContract.SetKeyboardFocus(ServerPanelTag FocusTag)
    {
      CompactWriter.WriteMessage(ServerMessage.SetKeyboardFocus);
      CompactWriter.WritePanelTag(FocusTag);
    }
    void ServerContract.SetWindowInputPrevented(bool InputPrevented, ServerPanelTag[] DisablePanelArray, ServerPanelTag[] EnablePanelArray)
    {
      CompactWriter.WriteMessage(ServerMessage.SetWindowInputPrevented);
      CompactWriter.WriteBoolean(InputPrevented);
      CompactWriter.WriteInt32(DisablePanelArray.Length);
      foreach (var DisablePanel in DisablePanelArray)
        CompactWriter.WritePanelTag(DisablePanel);
      CompactWriter.WriteInt32(EnablePanelArray.Length);
      foreach (var EnablePanel in EnablePanelArray)
        CompactWriter.WritePanelTag(EnablePanel);
    }
    void ServerContract.NewSurface(ServerSurfaceTag SurfaceTag)
    {
      CompactWriter.WriteMessage(ServerMessage.NewSurface);
      CompactWriter.WriteSurfaceTag(SurfaceTag);
    }
    void ServerContract.SetSurfaceHasGestureBackward(ServerSurfaceTag SurfaceTag, bool HasGestureBackward)
    {
      CompactWriter.WriteMessage(ServerMessage.SetSurfaceHasGestureBackward);
      CompactWriter.WriteSurfaceTag(SurfaceTag);
      CompactWriter.WriteBoolean(HasGestureBackward);
    }
    void ServerContract.SetSurfaceHasGestureForward(ServerSurfaceTag SurfaceTag, bool HasGestureForward)
    {
      CompactWriter.WriteMessage(ServerMessage.SetSurfaceHasGestureForward);
      CompactWriter.WriteSurfaceTag(SurfaceTag);
      CompactWriter.WriteBoolean(HasGestureForward);
    }
    void ServerContract.SetSurfaceBackground(ServerSurfaceTag SurfaceTag, Colour Colour)
    {
      CompactWriter.WriteMessage(ServerMessage.SetSurfaceBackground);
      CompactWriter.WriteSurfaceTag(SurfaceTag);
      CompactWriter.WriteColour(Colour);
    }
    void ServerContract.SetSurfaceContent(ServerSurfaceTag SurfaceTag, ServerPanelTag ContentTag)
    {
      CompactWriter.WriteMessage(ServerMessage.SetSurfaceContent);
      CompactWriter.WriteSurfaceTag(SurfaceTag);
      CompactWriter.WritePanelTag(ContentTag);
    }
    void ServerContract.TransitionSurface(ServerSurfaceTag SurfaceTag, TransitionType TransitionType, TimeSpan TransitionDuration)
    {
      CompactWriter.WriteMessage(ServerMessage.TransitionSurface);
      CompactWriter.WriteSurfaceTag(SurfaceTag);
      CompactWriter.WriteByte((byte)TransitionType);
      CompactWriter.WriteTimeSpan(TransitionDuration);
    }
    void ServerContract.NewBrowser(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.NewBrowser);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.LoadBrowser(ServerPanelTag PanelTag, Uri Uri, string Html)
    {
      CompactWriter.WriteMessage(ServerMessage.LoadBrowser);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteUri(Uri);
      CompactWriter.WriteString(Html);
    }
    void ServerContract.NewButton(ServerPanelTag PanelTag, Inv.ButtonStyle Style)
    {
      CompactWriter.WriteMessage(ServerMessage.NewButton);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteByte((byte)Style);
    }
    void ServerContract.SetButtonFocus(ServerPanelTag PanelTag, ServerFocus Focus)
    {
      CompactWriter.WriteMessage(ServerMessage.SetButtonFocus);
      CompactWriter.WritePanelTag(PanelTag);
      WriteFocus(Focus);
    }
    void ServerContract.SetButtonContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      CompactWriter.WriteMessage(ServerMessage.SetButtonContent);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WritePanelTag(ContentTag);
    }
    void ServerContract.SetButtonIsEnabled(ServerPanelTag PanelTag, bool IsEnabled)
    {
      CompactWriter.WriteMessage(ServerMessage.SetButtonIsEnabled);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(IsEnabled);
    }
    void ServerContract.SetButtonIsFocusable(ServerPanelTag PanelTag, bool IsFocusable)
    {
      CompactWriter.WriteMessage(ServerMessage.SetButtonIsFocusable);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(IsFocusable);
    }
    void ServerContract.SetButtonHint(ServerPanelTag PanelTag, string Hint)
    {
      CompactWriter.WriteMessage(ServerMessage.SetButtonHint);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteString(Hint);
    }
    void ServerContract.SetButtonHasPress(ServerPanelTag PanelTag, bool HasPress)
    {
      CompactWriter.WriteMessage(ServerMessage.SetButtonHasPress);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(HasPress);
    }
    void ServerContract.SetButtonHasRelease(ServerPanelTag PanelTag, bool HasRelease)
    {
      CompactWriter.WriteMessage(ServerMessage.SetButtonHasRelease);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(HasRelease);
    }
    void ServerContract.NewBlock(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.NewBlock);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.SetBlockCollection(ServerPanelTag PanelTag, ServerBlockSpan[] SpanArray)
    {
      CompactWriter.WriteMessage(ServerMessage.SetBlockCollection);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteInt32(SpanArray.Length);
      foreach (var Span in SpanArray)
      {
        CompactWriter.WriteByte((byte)Span.Style);
        CompactWriter.WriteString(Span.Text);
        CompactWriter.WriteColour(Span.BackgroundColour);
        WriteFont(Span.Font);
      }
    }
    void ServerContract.SetBlockLineWrapping(ServerPanelTag PanelTag, bool LineWrapping)
    {
      CompactWriter.WriteMessage(ServerMessage.SetBlockLineWrapping);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(LineWrapping);
    }
    void ServerContract.SetBlockJustification(ServerPanelTag PanelTag, Justification Justification)
    {
      CompactWriter.WriteMessage(ServerMessage.SetBlockJustification);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteByte((byte)Justification);
    }
    void ServerContract.SetBlockFont(ServerPanelTag PanelTag, ServerFont Font)
    {
      CompactWriter.WriteMessage(ServerMessage.SetBlockFont);
      CompactWriter.WritePanelTag(PanelTag);
      WriteFont(Font);
    }
    void ServerContract.NewBoard(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.NewBoard);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.SetBoardCollection(ServerPanelTag PanelTag, ServerBoardPin[] PinArray)
    {
      CompactWriter.WriteMessage(ServerMessage.SetBoardCollection);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteInt32(PinArray.Length);
      foreach (var Pin in PinArray)
      {
        CompactWriter.WritePanelTag(Pin.PanelTag);
        CompactWriter.WriteInt32(Pin.Rect.Left);
        CompactWriter.WriteInt32(Pin.Rect.Top);
        CompactWriter.WriteInt32(Pin.Rect.Width);
        CompactWriter.WriteInt32(Pin.Rect.Height);
      }
    }
    void ServerContract.NewDock(ServerPanelTag PanelTag, Orientation Orientation)
    {
      CompactWriter.WriteMessage(Orientation == Orientation.Horizontal ? ServerMessage.NewHorizontalDock : ServerMessage.NewVerticalDock);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.NewEdit(ServerPanelTag PanelTag, EditInput Input)
    {
      CompactWriter.WriteMessage(ServerMessage.NewEdit);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteByte((byte)Input);
    }
    void ServerContract.SetEditHint(ServerPanelTag PanelTag, string Hint)
    {
      CompactWriter.WriteMessage(ServerMessage.SetEditHint);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteString(Hint);
    }
    void ServerContract.SetEditIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly)
    {
      CompactWriter.WriteMessage(ServerMessage.SetEditIsReadOnly);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(ReadOnly);
    }
    void ServerContract.SetEditHasChange(ServerPanelTag PanelTag, bool HasChange)
    {
      CompactWriter.WriteMessage(ServerMessage.SetEditHasChange);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(HasChange);
    }
    void ServerContract.SetEditHasReturn(ServerPanelTag PanelTag, bool HasReturn)
    {
      CompactWriter.WriteMessage(ServerMessage.SetEditHasReturn);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(HasReturn);
    }
    void ServerContract.SetEditText(ServerPanelTag PanelTag, string Text)
    {
      CompactWriter.WriteMessage(ServerMessage.SetEditText);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteString(Text);
    }
    void ServerContract.SetEditFont(ServerPanelTag PanelTag, ServerFont Font)
    {
      CompactWriter.WriteMessage(ServerMessage.SetEditFont);
      CompactWriter.WritePanelTag(PanelTag);
      WriteFont(Font);
    }
    void ServerContract.SetEditFocus(ServerPanelTag PanelTag, ServerFocus Focus)
    {
      CompactWriter.WriteMessage(ServerMessage.SetEditFocus);
      CompactWriter.WritePanelTag(PanelTag);
      WriteFocus(Focus);
    }
    void ServerContract.NewFlow(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.NewFlow);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.SetFlowFixture(ServerPanelTag PanelTag, ServerPanelTag FixtureTag)
    {
      CompactWriter.WriteMessage(ServerMessage.SetFlowFixture);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WritePanelTag(FixtureTag);
    }
    void ServerContract.ReloadFlow(ServerPanelTag PanelTag, ServerFlowSection[] SectionArray)
    {
      CompactWriter.WriteMessage(ServerMessage.ReloadFlow);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteInt32(SectionArray.Length);
      foreach (var Section in SectionArray)
      {
        CompactWriter.WriteInt32(Section.ItemCount);
        CompactWriter.WritePanelTag(Section.HeaderPanelTag);
        CompactWriter.WritePanelTag(Section.FooterPanelTag);
      }
    }
    void ServerContract.NewFrame(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.NewFrame);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.NewGraphic(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.NewGraphic);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.SetGraphicFit(ServerPanelTag PanelTag, FitMethod FitMethod)
    {
      CompactWriter.WriteMessage(ServerMessage.SetGraphicFit);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteByte((byte)FitMethod);
    }
    void ServerContract.SetGraphicImage(ServerPanelTag PanelTag, ServerImageTag ImageTag)
    {
      CompactWriter.WriteMessage(ServerMessage.SetGraphicImage);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteImageTag(ImageTag);
    }
    void ServerContract.NewVideo(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.NewVideo);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.NewLabel(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.NewLabel);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.SetLabelText(ServerPanelTag PanelTag, string Text)
    {
      CompactWriter.WriteMessage(ServerMessage.SetLabelText);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteString(Text);
    }
    void ServerContract.SetLabelLineWrapping(ServerPanelTag PanelTag, bool LineWrapping)
    {
      CompactWriter.WriteMessage(ServerMessage.SetLabelLineWrapping);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(LineWrapping);
    }
    void ServerContract.SetLabelJustification(ServerPanelTag PanelTag, Justification Justification)
    {
      CompactWriter.WriteMessage(ServerMessage.SetLabelJustification);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteByte((byte)Justification);
    }
    void ServerContract.SetLabelFont(ServerPanelTag PanelTag, ServerFont Font)
    {
      CompactWriter.WriteMessage(ServerMessage.SetLabelFont);
      CompactWriter.WritePanelTag(PanelTag);
      WriteFont(Font);
    }
    void ServerContract.NewMemo(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.NewMemo);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.SetMemoIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly)
    {
      CompactWriter.WriteMessage(ServerMessage.SetMemoIsReadOnly);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(ReadOnly);
    }
    void ServerContract.SetMemoHasChange(ServerPanelTag PanelTag, bool HasChange)
    {
      CompactWriter.WriteMessage(ServerMessage.SetMemoHasChange);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(HasChange);
    }
    void ServerContract.SetMemoText(ServerPanelTag PanelTag, string Text)
    {
      CompactWriter.WriteMessage(ServerMessage.SetMemoText);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteString(Text);
    }
    void ServerContract.SetMemoFont(ServerPanelTag PanelTag, ServerFont Font)
    {
      CompactWriter.WriteMessage(ServerMessage.SetMemoFont);
      CompactWriter.WritePanelTag(PanelTag);
      WriteFont(Font);
    }
    void ServerContract.SetMemoFocus(ServerPanelTag PanelTag, ServerFocus Focus)
    {
      CompactWriter.WriteMessage(ServerMessage.SetMemoFocus);
      CompactWriter.WritePanelTag(PanelTag);
      WriteFocus(Focus);
    }
    void ServerContract.NewOverlay(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.NewOverlay);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.NewCanvas(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.NewCanvas);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.DrawCanvas(ServerPanelTag PanelTag, Inv.TransportPacket Packet)
    {
      CompactWriter.WriteMessage(ServerMessage.DrawCanvas);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteByteArray(Packet.Buffer);
    }
    void ServerContract.SetCanvasHasMeasure(ServerPanelTag PanelTag, bool HasMeasure)
    {
      CompactWriter.WriteMessage(ServerMessage.SetCanvasHasMeasure);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(HasMeasure);
    }
    void ServerContract.NewScroll(ServerPanelTag PanelTag, Orientation Orientation)
    {
      CompactWriter.WriteMessage(Orientation == Orientation.Horizontal ? ServerMessage.NewHorizontalScroll : ServerMessage.NewVerticalScroll);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.NewShape(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.NewShape);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.SetShapeFit(ServerPanelTag PanelTag, Inv.FitMethod Fit)
    {
      CompactWriter.WriteMessage(ServerMessage.SetShapeFit);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteByte((byte)Fit);
    }
    void ServerContract.SetShapeFill(ServerPanelTag PanelTag, Inv.Colour Colour)
    {
      CompactWriter.WriteMessage(ServerMessage.SetShapeFill);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteColour(Colour);
    }
    void ServerContract.SetShapeStroke(ServerPanelTag PanelTag, int Thickness, Inv.Colour Colour, Inv.LineCap LineCap, Inv.LineJoin LineJoin, int[] DashPattern)
    {
      CompactWriter.WriteMessage(ServerMessage.SetShapeStroke);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteInt32(Thickness);
      CompactWriter.WriteColour(Colour);
      CompactWriter.WriteByte((byte)LineCap);
      CompactWriter.WriteByte((byte)LineJoin);
      CompactWriter.WriteInt32Array(DashPattern);
    }
    void ServerContract.SetShapeFigure(ServerPanelTag PanelTag, ServerShapeFigure[] FigureArray)
    {
      CompactWriter.WriteMessage(ServerMessage.SetShapeFigure);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteInt32(FigureArray.Length);

      foreach (var Figure in FigureArray)
      {
        CompactWriter.WriteByte((byte)Figure.Type);
        CompactWriter.WriteInt32(Figure.PathArray.Length);

        foreach (var Path in Figure.PathArray)
        {
          CompactWriter.WriteInt32(Path.X);
          CompactWriter.WriteInt32(Path.Y);
        }
      }
    }
    void ServerContract.NewStack(ServerPanelTag PanelTag, Orientation Orientation)
    {
      CompactWriter.WriteMessage(Orientation == Orientation.Horizontal ? ServerMessage.NewHorizontalStack : ServerMessage.NewVerticalStack);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.NewSwitch(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.NewSwitch);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.SetSwitchHasChange(ServerPanelTag PanelTag, bool HasChange)
    {
      CompactWriter.WriteMessage(ServerMessage.SetSwitchHasChange);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(HasChange);
    }
    void ServerContract.SetSwitchIsEnabled(ServerPanelTag PanelTag, bool Enabled)
    {
      CompactWriter.WriteMessage(ServerMessage.SetSwitchIsEnabled);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(Enabled);
    }
    void ServerContract.SetSwitchIsOn(ServerPanelTag PanelTag, bool On)
    {
      CompactWriter.WriteMessage(ServerMessage.SetSwitchIsOn);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(On);
    }
    void ServerContract.NewTable(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.NewTable);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.NewWrap(ServerPanelTag PanelTag, Orientation Orientation)
    {
      CompactWriter.WriteMessage(Orientation == Orientation.Horizontal ? ServerMessage.NewHorizontalWrap : ServerMessage.NewVerticalWrap);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.SetTableCollection(ServerPanelTag PanelTag, ServerTableAxis[] RowArray, ServerTableAxis[] ColumnArray, ServerTableCell[] CellArray)
    {
      CompactWriter.WriteMessage(ServerMessage.SetTableCollection);
      CompactWriter.WritePanelTag(PanelTag);

      CompactWriter.WriteInt32(RowArray.Length);
      foreach (var Row in RowArray)
      {
        CompactWriter.WriteByte((byte)Row.LengthType);
        CompactWriter.WriteInt32(Row.LengthValue);
        CompactWriter.WritePanelTag(Row.PanelTag);
      }

      CompactWriter.WriteInt32(ColumnArray.Length);
      foreach (var Column in ColumnArray)
      {
        CompactWriter.WriteByte((byte)Column.LengthType);
        CompactWriter.WriteInt32(Column.LengthValue);
        CompactWriter.WritePanelTag(Column.PanelTag);
      }

      CompactWriter.WriteInt32(CellArray.Length);
      foreach (var Cell in CellArray)
      {
        CompactWriter.WriteInt32(Cell.X);
        CompactWriter.WriteInt32(Cell.Y);
        CompactWriter.WritePanelTag(Cell.PanelTag);
      }
    }
    void ServerContract.SetPanelAlignment(ServerPanelTag PanelTag, Inv.Placement Placement)
    {
      CompactWriter.WriteMessage(ServerMessage.SetPanelAlignment);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteByte((byte)Placement);
    }
    void ServerContract.SetPanelBorder(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom, Colour Colour)
    {
      CompactWriter.WriteMessage(ServerMessage.SetPanelBorder);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteInt32(Left);
      CompactWriter.WriteInt32(Top);
      CompactWriter.WriteInt32(Right);
      CompactWriter.WriteInt32(Bottom);
      CompactWriter.WriteColour(Colour);
    }
    void ServerContract.SetPanelCorner(ServerPanelTag PanelTag, int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      CompactWriter.WriteMessage(ServerMessage.SetPanelCorner);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteInt32(TopLeft);
      CompactWriter.WriteInt32(TopRight);
      CompactWriter.WriteInt32(BottomRight);
      CompactWriter.WriteInt32(BottomLeft);
    }
    void ServerContract.SetPanelElevation(ServerPanelTag PanelTag, int Depth)
    {
      CompactWriter.WriteMessage(ServerMessage.SetPanelElevation);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteInt32(Depth);
    }
    void ServerContract.SetPanelMargin(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom)
    {
      CompactWriter.WriteMessage(ServerMessage.SetPanelMargin);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteInt32(Left);
      CompactWriter.WriteInt32(Top);
      CompactWriter.WriteInt32(Right);
      CompactWriter.WriteInt32(Bottom);
    }
    void ServerContract.SetPanelOpacity(ServerPanelTag PanelTag, float Opacity)
    {
      CompactWriter.WriteMessage(ServerMessage.SetPanelOpacity);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteFloat(Opacity);
    }
    void ServerContract.SetPanelPadding(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom)
    {
      CompactWriter.WriteMessage(ServerMessage.SetPanelPadding);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteInt32(Left);
      CompactWriter.WriteInt32(Top);
      CompactWriter.WriteInt32(Right);
      CompactWriter.WriteInt32(Bottom);
    }
    void ServerContract.SetPanelSize(ServerPanelTag PanelTag, int? Width, int? Height, int? MinimumWidth, int? MinimumHeight, int? MaximumWidth, int? MaximumHeight)
    {
      CompactWriter.WriteMessage(ServerMessage.SetPanelSize);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteInt32(Width ?? -1);
      CompactWriter.WriteInt32(Height ?? -1);
      CompactWriter.WriteInt32(MinimumWidth ?? -1);
      CompactWriter.WriteInt32(MinimumHeight ?? -1);
      CompactWriter.WriteInt32(MaximumWidth ?? -1);
      CompactWriter.WriteInt32(MaximumHeight ?? -1);
    }
    void ServerContract.SetPanelVisibility(ServerPanelTag PanelTag, bool Visibility)
    {
      CompactWriter.WriteMessage(ServerMessage.SetPanelVisibility);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(Visibility);
    }
    void ServerContract.SetPanelBackground(ServerPanelTag PanelTag, Colour BackgroundColour)
    {
      CompactWriter.WriteMessage(ServerMessage.SetPanelBackground);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteColour(BackgroundColour);
    }
    void ServerContract.SetPanelHasAdjust(ServerPanelTag PanelTag, bool HasAdjust)
    {
      CompactWriter.WriteMessage(ServerMessage.SetPanelHasAdjust);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteBoolean(HasAdjust);
    }
    Inv.Dimension ServerContract.GetPanelDimension(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.GetPanelDimension);
      CompactWriter.WritePanelTag(PanelTag);

      Send();

      var Width = 0;
      var Height = 0;

      ReceiveMessage(ClientMessage.ConfirmGetPanelDimension, Reader =>
      {
        Width = Reader.ReadInt32();
        Height = Reader.ReadInt32();
      });

      return new Dimension(Width, Height);
    }
    void ServerContract.SetDockOrientation(ServerPanelTag PanelTag, Orientation Orientation)
    {
      CompactWriter.WriteMessage(ServerMessage.SetDockOrientation);
      CompactWriter.WritePanelTag(PanelTag);

      CompactWriter.WriteByte((byte)Orientation);
    }
    void ServerContract.SetDockCollection(ServerPanelTag PanelTag, ServerPanelTag[] HeaderArray, ServerPanelTag[] ClientArray, ServerPanelTag[] FooterArray)
    {
      CompactWriter.WriteMessage(ServerMessage.SetDockCollection);
      CompactWriter.WritePanelTag(PanelTag);

      CompactWriter.WriteInt32(HeaderArray.Length);
      foreach (var Panel in HeaderArray)
        CompactWriter.WritePanelTag(Panel);

      CompactWriter.WriteInt32(ClientArray.Length);
      foreach (var Panel in ClientArray)
        CompactWriter.WritePanelTag(Panel);

      CompactWriter.WriteInt32(FooterArray.Length);
      foreach (var Panel in FooterArray)
        CompactWriter.WritePanelTag(Panel);
    }
    void ServerContract.SetFrameContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      CompactWriter.WriteMessage(ServerMessage.SetFrameContent);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WritePanelTag(ContentTag);
    }
    void ServerContract.SetFrameTransition(ServerPanelTag PanelTag, ServerPanelTag ContentTag, TransitionType TransitionType, TimeSpan TransitionDuration)
    {
      CompactWriter.WriteMessage(ServerMessage.SetFrameTransition);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WritePanelTag(ContentTag);
      CompactWriter.WriteByte((byte)TransitionType);
      CompactWriter.WriteTimeSpan(TransitionDuration);
    }
    void ServerContract.SetOverlayCollection(ServerPanelTag PanelTag, ServerPanelTag[] PanelArray)
    {
      CompactWriter.WriteMessage(ServerMessage.SetOverlayCollection);
      CompactWriter.WritePanelTag(PanelTag);

      CompactWriter.WriteInt32(PanelArray.Length);
      foreach (var Panel in PanelArray)
        CompactWriter.WritePanelTag(Panel);
    }
    void ServerContract.SetScrollOrientation(ServerPanelTag PanelTag, Orientation Orientation)
    {
      CompactWriter.WriteMessage(ServerMessage.SetScrollOrientation);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteByte((byte)Orientation);
    }
    void ServerContract.SetScrollContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      CompactWriter.WriteMessage(ServerMessage.SetScrollContent);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WritePanelTag(ContentTag);
    }
    void ServerContract.RequestScrollGoTo(ServerPanelTag PanelTag, ScrollRequest ScrollRequest)
    {
      CompactWriter.WriteMessage(ServerMessage.RequestScrollGoTo);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteByte((byte)ScrollRequest);
    }
    void ServerContract.SetStackOrientation(ServerPanelTag PanelTag, Orientation Orientation)
    {
      CompactWriter.WriteMessage(ServerMessage.SetStackOrientation);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteByte((byte)Orientation);
    }
    void ServerContract.SetStackCollection(ServerPanelTag PanelTag, ServerPanelTag[] PanelArray)
    {
      CompactWriter.WriteMessage(ServerMessage.SetStackCollection);
      CompactWriter.WritePanelTag(PanelTag);

      CompactWriter.WriteInt32(PanelArray.Length);
      foreach (var Panel in PanelArray)
        CompactWriter.WritePanelTag(Panel);
    }
    void ServerContract.SetWrapOrientation(ServerPanelTag PanelTag, Orientation Orientation)
    {
      CompactWriter.WriteMessage(ServerMessage.SetWrapOrientation);
      CompactWriter.WritePanelTag(PanelTag);
      CompactWriter.WriteByte((byte)Orientation);
    }
    void ServerContract.SetWrapCollection(ServerPanelTag PanelTag, ServerPanelTag[] PanelArray)
    {
      CompactWriter.WriteMessage(ServerMessage.SetWrapCollection);
      CompactWriter.WritePanelTag(PanelTag);

      CompactWriter.WriteInt32(PanelArray.Length);
      foreach (var Panel in PanelArray)
        CompactWriter.WritePanelTag(Panel);
    }
    void ServerContract.NewImage(ServerImageTag ImageTag, Inv.Image ImageSource)
    {
      CompactWriter.WriteMessage(ServerMessage.NewImage);
      CompactWriter.WriteImageTag(ImageTag);
      CompactWriter.WriteImage(ImageSource);
    }
    void ServerContract.NewSound(ServerSoundTag SoundTag, Inv.Sound SoundSource)
    {
      CompactWriter.WriteMessage(ServerMessage.NewSound);
      CompactWriter.WriteSoundTag(SoundTag);
      CompactWriter.WriteSound(SoundSource);
    }
    void ServerContract.PlaySound(ServerSoundTag SoundTag, float SoundVolume, float SoundRate, float SoundPan)
    {
      CompactWriter.WriteMessage(ServerMessage.PlaySound);
      CompactWriter.WriteSoundTag(SoundTag);
      CompactWriter.WriteFloat(SoundVolume);
      CompactWriter.WriteFloat(SoundRate);
      CompactWriter.WriteFloat(SoundPan);
    }
    void ServerContract.PlayClip(ServerClipTag ClipTag, ServerSoundTag SoundTag, float SoundVolume, float SoundRate, float SoundPan, bool SoundLoop)
    {
      CompactWriter.WriteMessage(ServerMessage.PlayClip);
      CompactWriter.WriteClipTag(ClipTag);
      CompactWriter.WriteSoundTag(SoundTag);
      CompactWriter.WriteFloat(SoundVolume);
      CompactWriter.WriteFloat(SoundRate);
      CompactWriter.WriteFloat(SoundPan);
      CompactWriter.WriteBoolean(SoundLoop);
    }
    void ServerContract.PauseClip(ServerClipTag ClipTag)
    {
      CompactWriter.WriteMessage(ServerMessage.PauseClip);
      CompactWriter.WriteClipTag(ClipTag);
    }
    void ServerContract.ResumeClip(ServerClipTag ClipTag)
    {
      CompactWriter.WriteMessage(ServerMessage.ResumeClip);
      CompactWriter.WriteClipTag(ClipTag);
    }
    void ServerContract.StopClip(ServerClipTag ClipTag)
    {
      CompactWriter.WriteMessage(ServerMessage.StopClip);
      CompactWriter.WriteClipTag(ClipTag);
    }
    void ServerContract.ModulateClip(ServerClipTag ClipTag, float SoundVolume, float SoundRate, float SoundPan, bool SoundLoop)
    {
      CompactWriter.WriteMessage(ServerMessage.ModulateClip);
      CompactWriter.WriteClipTag(ClipTag);
      CompactWriter.WriteFloat(SoundVolume);
      CompactWriter.WriteFloat(SoundRate);
      CompactWriter.WriteFloat(SoundPan);
      CompactWriter.WriteBoolean(SoundLoop);
    }
    void ServerContract.StartAnimation(ServerAnimationTag AnimationTag, ServerAnimationTarget[] TargetArray)
    {
      CompactWriter.WriteMessage(ServerMessage.StartAnimation);
      CompactWriter.WriteAnimationTag(AnimationTag);
      CompactWriter.WriteInt32(TargetArray.Length);
      foreach (var Target in TargetArray)
      {
        CompactWriter.WritePanelTag(Target.PanelTag);
        CompactWriter.WriteInt32(Target.TransformArray.Length);
        foreach (var Transform in Target.TransformArray)
        {
          CompactWriter.WriteByte((byte)Transform.Type);

          switch (Transform.Type)
          {
            case AnimationType.Fade:
              CompactWriter.WriteTimeSpan(Transform.FadeOffset ?? TimeSpan.Zero);
              CompactWriter.WriteTimeSpan(Transform.FadeDuration);
              CompactWriter.WriteFloat(Transform.FadeFromOpacity);
              CompactWriter.WriteFloat(Transform.FadeToOpacity);
              break;

            case AnimationType.Rotate:
              CompactWriter.WriteTimeSpan(Transform.RotateOffset ?? TimeSpan.Zero);
              CompactWriter.WriteTimeSpan(Transform.RotateDuration);
              CompactWriter.WriteFloat(Transform.RotateFromAngle);
              CompactWriter.WriteFloat(Transform.RotateToAngle);
              break;

            case AnimationType.Scale:
              CompactWriter.WriteTimeSpan(Transform.ScaleOffset ?? TimeSpan.Zero);
              CompactWriter.WriteTimeSpan(Transform.ScaleDuration);
              CompactWriter.WriteFloat(Transform.ScaleFromWidth);
              CompactWriter.WriteFloat(Transform.ScaleToWidth);
              CompactWriter.WriteFloat(Transform.ScaleFromHeight);
              CompactWriter.WriteFloat(Transform.ScaleToHeight);
              break;

            case AnimationType.Translate:
              CompactWriter.WriteTimeSpan(Transform.TranslateOffset ?? TimeSpan.Zero);
              CompactWriter.WriteTimeSpan(Transform.TranslateDuration);
              CompactWriter.WriteInt32Nullable(Transform.TranslateFromX);
              CompactWriter.WriteInt32Nullable(Transform.TranslateToX);
              CompactWriter.WriteInt32Nullable(Transform.TranslateFromY);
              CompactWriter.WriteInt32Nullable(Transform.TranslateToY);
              break;

            default:
              throw new Exception("AnimationType not handled: " + Transform.Type);
          }
        }
      }
    }
    void ServerContract.StopAnimation(ServerAnimationTag AnimationTag)
    {
      CompactWriter.WriteMessage(ServerMessage.StopAnimation);
      CompactWriter.WriteAnimationTag(AnimationTag);
    }
    void ServerContract.BrowseMarket(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      CompactWriter.WriteMessage(ServerMessage.BrowseMarket);
      CompactWriter.WriteString(AppleiTunesID);
      CompactWriter.WriteString(GooglePlayID);
      CompactWriter.WriteString(WindowsStoreID);
    }
    void ServerContract.LaunchWebUri(Uri Uri)
    {
      CompactWriter.WriteMessage(ServerMessage.LaunchWebUri);
      CompactWriter.WriteUri(Uri);
    }
    void ServerContract.InstallWebUri(Uri Uri)
    {
      CompactWriter.WriteMessage(ServerMessage.InstallWebUri);
      CompactWriter.WriteUri(Uri);
    }
    void ServerContract.ShowCalendarPicker(ServerPickerTag PickerTag, bool SetDate, bool SetTime, DateTime Value)
    {
      CompactWriter.WriteMessage(ServerMessage.ShowCalendarPicker);
      CompactWriter.WritePickerTag(PickerTag);
      CompactWriter.WriteBoolean(SetDate);
      CompactWriter.WriteBoolean(SetTime);
      CompactWriter.WriteDateTime(Value);
    }
    void ServerContract.ShowDirectoryPicker(ServerPickerTag PickerTag, string Title, PickType FileType)
    {
      CompactWriter.WriteMessage(ServerMessage.ShowDirectoryPicker);
      CompactWriter.WritePickerTag(PickerTag);
      CompactWriter.WriteString(Title);
      CompactWriter.WriteByte((byte)FileType);
    }
    bool ServerContract.SendEmailMessage(string Subject, string Body, ServerEmailTo[] ToArray, ServerEmailAttachment[] AttachmentArray)
    {
      ServerTenant.ProcessMessages();

      CompactWriter.WriteMessage(ServerMessage.SendEmailMessage);
      CompactWriter.WriteString(Subject);
      CompactWriter.WriteString(Body);

      CompactWriter.WriteInt32(ToArray.Length);
      foreach (var To in ToArray)
      {
        CompactWriter.WriteString(To.Name);
        CompactWriter.WriteString(To.Address);
      }

      CompactWriter.WriteInt32(AttachmentArray.Length);
      foreach (var Attachment in AttachmentArray)
      {
        CompactWriter.WriteString(Attachment.Name);
        CompactWriter.WriteByteArray(Attachment.Content);
      }

      Send();

      var Result = false;

      ReceiveMessage(ClientMessage.ConfirmSendEmailMessage, Reader =>
      {
        Result = Reader.ReadBoolean();
      });

      return Result;
    }
    void ServerContract.DialPhone(string PhoneNumber)
    {
      ServerTenant.ProcessMessages();

      CompactWriter.WriteMessage(ServerMessage.DialPhone);
      CompactWriter.WriteString(PhoneNumber);

      Send();

      ReceiveMessage(ClientMessage.ConfirmDialPhone);
    }
    void ServerContract.SMSPhone(string PhoneNumber)
    {
      ServerTenant.ProcessMessages();

      CompactWriter.WriteMessage(ServerMessage.SMSPhone);
      CompactWriter.WriteString(PhoneNumber);

      Send();

      ReceiveMessage(ClientMessage.ConfirmSMSPhone);
    }
    string ServerContract.ClipboardGetText()
    {
      ServerTenant.ProcessMessages();

      CompactWriter.WriteMessage(ServerMessage.ClipboardGetText);

      Send();

      string Result = null;

      ReceiveMessage(ClientMessage.ConfirmClipboardGetText, Reader =>
      {
        Result = Reader.ReadString();
      });

      return Result;
    }
    void ServerContract.ClipboardSetText(string Text)
    {
      ServerTenant.ProcessMessages();

      CompactWriter.WriteMessage(ServerMessage.ClipboardSetText);
      CompactWriter.WriteString(Text);

      Send();

      ReceiveMessage(ClientMessage.ConfirmClipboardSetText);
    }
    Inv.Image ServerContract.ClipboardGetImage()
    {
      ServerTenant.ProcessMessages();

      CompactWriter.WriteMessage(ServerMessage.ClipboardGetImage);

      Send();

      Inv.Image Result = null;

      ReceiveMessage(ClientMessage.ConfirmClipboardGetImage, Reader =>
      {
        Result = Reader.ReadImage();
      });

      return Result;
    }
    void ServerContract.ClipboardSetImage(Inv.Image Image)
    {
      ServerTenant.ProcessMessages();

      CompactWriter.WriteMessage(ServerMessage.ClipboardSetImage);
      CompactWriter.WriteImage(Image);

      Send();

      ReceiveMessage(ClientMessage.ConfirmClipboardSetImage);
    }
    void ServerContract.HapticFeedback(HapticFeedback Feedback)
    {
      ServerTenant.ProcessMessages();

      CompactWriter.WriteMessage(ServerMessage.HapticFeedback);
      CompactWriter.WriteByte((byte)Feedback);

      Send();

      ReceiveMessage(ClientMessage.ConfirmHapticFeedback);
    }

    void ServerContract.DisposeSurface(ServerSurfaceTag SurfaceTag)
    {
      CompactWriter.WriteMessage(ServerMessage.DisposeSurface);
      CompactWriter.WriteSurfaceTag(SurfaceTag);
    }
    void ServerContract.DisposePanel(ServerPanelTag PanelTag)
    {
      CompactWriter.WriteMessage(ServerMessage.DisposePanel);
      CompactWriter.WritePanelTag(PanelTag);
    }
    void ServerContract.DisposeImage(ServerImageTag ImageTag)
    {
      CompactWriter.WriteMessage(ServerMessage.DisposeImage);
      CompactWriter.WriteImageTag(ImageTag);
    }
    void ServerContract.DisposeSound(ServerSoundTag SoundTag)
    {
      CompactWriter.WriteMessage(ServerMessage.DisposeSound);
      CompactWriter.WriteSoundTag(SoundTag);
    }

    private void WriteFont(ServerFont Font)
    {
      CompactWriter.WriteString(Font.Name);
      CompactWriter.WriteInt32(Font.Size ?? 0);
      CompactWriter.WriteColour(Font.Colour);
      CompactWriter.WriteByte(Font.Weight == null ? (byte)255 : (byte)Font.Weight.Value);
      CompactWriter.WriteByte(Font.Axis == null ? (byte)255 : (byte)Font.Axis.Value);

      var FontStyle = ServerFontStyle.None;
      if (Font.SmallCaps != null)
        FontStyle |= Font.SmallCaps.Value ? ServerFontStyle.SmallCaps : ServerFontStyle.NoSmallCaps;

      if (Font.Underline != null)
        FontStyle |= Font.Underline.Value ? ServerFontStyle.Underline : ServerFontStyle.NoUnderline;

      if (Font.Strikethrough != null)
        FontStyle |= Font.Strikethrough.Value ? ServerFontStyle.Strikethrough : ServerFontStyle.NoStrikethrough;

      if (Font.Italics != null)
        FontStyle |= Font.Italics.Value ? ServerFontStyle.Italics : ServerFontStyle.NoItalics;

      CompactWriter.WriteByte((byte)FontStyle);
    }
    private void WriteFocus(ServerFocus Focus)
    {
      CompactWriter.WriteBoolean(Focus.HasGot);
      CompactWriter.WriteBoolean(Focus.HasLost);
    }
    private void ReceiveMessage(ClientMessage Message, Action<CompactReader> Action = null)
    {
      do
      {
        var Packet = ServerTenant.ServerQueue.ReceivePacket();
        if (Packet == null)
          break;

        using (var TransportReceiver = Packet.ToReceiver())
        {
          var Reader = TransportReceiver.Reader;

          var NextMessage = Reader.ReadClientMessage();

          while (NextMessage != Message)
          {
            ServerTenant.ExecuteMessage(NextMessage, Reader);

            if (TransportReceiver.EndOfPacket)
              break;

            NextMessage = Reader.ReadClientMessage();
          }

          if (NextMessage == Message)
          {
            Action?.Invoke(Reader);

#if DEBUG
            if (!TransportReceiver.EndOfPacket)
              throw new Exception("End of packet was not found: " + NextMessage);
#endif

            break;
          }
        }
      }
      while (true);
    }

    private readonly ServerTenant ServerTenant;
    private readonly Inv.TransportSender TransportSender;
    private readonly Inv.CompactWriter CompactWriter;
  }

  internal sealed class ServerReceiver
  {
    public ServerReceiver(ServerContract Contract)
    {
      this.Contract = Contract;
      this.MessageArray = [];

      var OwnerType = GetType().GetReflectionInfo();
      var MethodInfoArray = OwnerType.GetReflectionMethods().Where(M => !M.IsPublic && !M.IsStatic).ToArray();

      foreach (var MethodInfo in MethodInfoArray)
      {
        if (MethodInfo.ReturnType != typeof(void))
          continue;

        var ParameterInfoArray = MethodInfo.GetParameters();

        if (ParameterInfoArray.Length == 1 && ParameterInfoArray[0].ParameterType == typeof(CompactReader))
        {
          var Message = Inv.Support.EnumHelper.Parse<ServerMessage>(MethodInfo.Name);

          MessageArray[Message] = (Action<CompactReader>)MethodInfo.CreateDelegate(typeof(Action<>).MakeGenericType(typeof(CompactReader)), this);
        }
      }

#if DEBUG
      var UnhandledMessageSet = new HashSet<ServerMessage>();

      foreach (var Message in Inv.Support.EnumHelper.GetEnumerable<ServerMessage>().Except(ServerMessage.Invalid))
      {
        if (!Message.ToString().StartsWith("Confirm") && MessageArray[Message] == null)
          UnhandledMessageSet.Add(Message);
      }

      if (UnhandledMessageSet.Count > 0)
        throw new Exception("Unhandled server messages:" + Environment.NewLine + Environment.NewLine + UnhandledMessageSet.Select(M => M.ToString()).AsLineSeparatedText());
#endif
    }

    public void ReceiveAll(Inv.TransportPacket Packet)
    {
      using (var TransportReceiver = Packet.ToReceiver())
      {
        var CompactReader = TransportReceiver.Reader;

        while (!TransportReceiver.EndOfPacket)
        {
          var Message = CompactReader.ReadServerMessage();
          ReceiveOne(Message, CompactReader);
        }
      }
    }
    public void ReceiveOne(ServerMessage Message, Inv.CompactReader Reader)
    {
      if ((int)Message < 0 || (int)Message >= MessageArray.Length)
        throw new Exception("Server message out of bounds: " + Message);

      var ReceiveAction = MessageArray[Message];

      if (ReceiveAction == null)
        throw new Exception("Server message not handled: " + Message);

      ReceiveAction(Reader);
    }

    private ServerFont ReadFont(CompactReader Reader)
    {
      var Name = Reader.ReadString();
      var Size = Reader.ReadInt32();
      var Colour = Reader.ReadColour();
      var Weight = Reader.ReadByte();
      var Axis = Reader.ReadByte();
      var FontStyle = (ServerFontStyle)Reader.ReadByte();

      return new ServerFont()
      {
        Name = Name,
        Size = Size > 0 ? Size : (int?)null,
        Colour = Colour,
        Weight = Weight == 255 ? (Inv.FontWeight?)null : (Inv.FontWeight)Weight,
        Axis = Axis == 255 ? (Inv.FontAxis?)null : (Inv.FontAxis)Axis,
        SmallCaps = (FontStyle & ServerFontStyle.SmallCaps) > 0 ? true : (FontStyle & ServerFontStyle.NoSmallCaps) > 0 ? false : (bool?)null,
        Underline = (FontStyle & ServerFontStyle.Underline) > 0 ? true : (FontStyle & ServerFontStyle.NoUnderline) > 0 ? false : (bool?)null,
        Strikethrough = (FontStyle & ServerFontStyle.Strikethrough) > 0 ? true : (FontStyle & ServerFontStyle.NoStrikethrough) > 0 ? false : (bool?)null,
        Italics = (FontStyle & ServerFontStyle.Italics) > 0 ? true : (FontStyle & ServerFontStyle.NoItalics) > 0 ? false : (bool?)null,
      };
    }
    private ServerFocus ReadFocus(CompactReader Reader)
    {
      var HasGot = Reader.ReadBoolean();
      var HasLost = Reader.ReadBoolean();

      return new ServerFocus()
      {
        HasGot = HasGot,
        HasLost = HasLost
      };
    }

#pragma warning disable IDE0051 // Remove unused private members
    private void ExitApplication(CompactReader Reader)
    {
      Contract.ExitApplication();
    }
    private void SetWindowBackground(CompactReader Reader)
    {
      var Colour = Reader.ReadColour();
      Contract.SetWindowBackground(Colour);
    }
    private void SetKeyboardFocus(CompactReader Reader)
    {
      var FocusTag = Reader.ReadPanelTag();
      Contract.SetKeyboardFocus(FocusTag);
    }
    private void SetWindowInputPrevented(CompactReader Reader)
    {
      var InputPrevented = Reader.ReadBoolean();

      var DisableCount = Reader.ReadInt32();
      var DisableArray = new ServerPanelTag[DisableCount];
      for (var i = 0; i < DisableCount; i++)
        DisableArray[i] = Reader.ReadPanelTag();

      var EnableCount = Reader.ReadInt32();
      var EnableArray = new ServerPanelTag[EnableCount];
      for (var i = 0; i < EnableCount; i++)
        EnableArray[i] = Reader.ReadPanelTag();

      Contract.SetWindowInputPrevented(InputPrevented, DisableArray, EnableArray);
    }
    private void NewSurface(CompactReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      Contract.NewSurface(SurfaceTag);
    }
    private void SetSurfaceHasGestureBackward(CompactReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var HasGestureBackward = Reader.ReadBoolean();
      Contract.SetSurfaceHasGestureBackward(SurfaceTag, HasGestureBackward);
    }
    private void SetSurfaceHasGestureForward(CompactReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var HasGestureForward = Reader.ReadBoolean();
      Contract.SetSurfaceHasGestureForward(SurfaceTag, HasGestureForward);
    }
    private void SetSurfaceBackground(CompactReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var Colour = Reader.ReadColour();
      Contract.SetSurfaceBackground(SurfaceTag, Colour);
    }
    private void SetSurfaceContent(CompactReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var ContentTag = Reader.ReadPanelTag();
      Contract.SetSurfaceContent(SurfaceTag, ContentTag);
    }
    private void TransitionSurface(CompactReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();
      var TransitionType = (TransitionType)Reader.ReadByte();
      var TransitionDuration = Reader.ReadTimeSpan();

      Contract.TransitionSurface(SurfaceTag, TransitionType, TransitionDuration);
    }
    private void NewBrowser(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewBrowser(PanelTag);
    }
    private void LoadBrowser(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Uri = Reader.ReadUri();
      var Html = Reader.ReadString();
      Contract.LoadBrowser(PanelTag, Uri, Html);
    }
    private void NewButton(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var ButtonStyle = (Inv.ButtonStyle)Reader.ReadByte();
      Contract.NewButton(PanelTag, ButtonStyle);
    }
    private void SetButtonFocus(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Focus = ReadFocus(Reader);
      Contract.SetButtonFocus(PanelTag, Focus);
    }
    private void SetButtonContent(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var ContentTag = Reader.ReadPanelTag();
      Contract.SetButtonContent(PanelTag, ContentTag);
    }
    private void SetButtonIsEnabled(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var IsEnabled = Reader.ReadBoolean();
      Contract.SetButtonIsEnabled(PanelTag, IsEnabled);
    }
    private void SetButtonIsFocusable(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var IsFocusable = Reader.ReadBoolean();
      Contract.SetButtonIsFocusable(PanelTag, IsFocusable);
    }
    private void SetButtonHint(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Hint = Reader.ReadString();
      Contract.SetButtonHint(PanelTag, Hint);
    }
    private void SetButtonHasPress(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var HasPress = Reader.ReadBoolean();
      Contract.SetButtonHasPress(PanelTag, HasPress);
    }
    private void SetButtonHasRelease(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var HasRelease = Reader.ReadBoolean();
      Contract.SetButtonHasRelease(PanelTag, HasRelease);
    }
    private void NewBlock(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewBlock(PanelTag);
    }
    private void SetBlockCollection(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Count = Reader.ReadInt32();
      var SpanArray = new ServerBlockSpan[Count];
      for (var i = 0; i < Count; i++)
      {
        var Style = (BlockStyle)Reader.ReadByte();
        var Text = Reader.ReadString();
        var BackgroundColour = Reader.ReadColour();
        var Font = ReadFont(Reader);

        SpanArray[i] = new ServerBlockSpan()
        {
          Style = Style,
          Text = Text,
          BackgroundColour = BackgroundColour,
          Font = Font
        };
      }

      Contract.SetBlockCollection(PanelTag, SpanArray);
    }
    private void SetBlockLineWrapping(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var LineWrapping = Reader.ReadBoolean();
      Contract.SetBlockLineWrapping(PanelTag, LineWrapping);
    }
    private void SetBlockJustification(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Justification = Reader.ReadByte();

      // NOTE: the 255 comparison is backwards compatibility from when it was a nullable field.
      Contract.SetBlockJustification(PanelTag, Justification != 255 ? (Inv.Justification)Justification : Inv.Justification.Left);
    }
    private void SetBlockFont(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Font = ReadFont(Reader);
      Contract.SetBlockFont(PanelTag, Font);
    }
    private void NewBoard(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewBoard(PanelTag);
    }
    private void SetBoardCollection(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Count = Reader.ReadInt32();
      var PinArray = new ServerBoardPin[Count];
      for (var i = 0; i < Count; i++)
      {
        var PinTag = Reader.ReadPanelTag();
        var PanelLeft = Reader.ReadInt32();
        var PanelTop = Reader.ReadInt32();
        var PanelWidth = Reader.ReadInt32();
        var PanelHeight = Reader.ReadInt32();

        PinArray[i] = new ServerBoardPin()
        {
          PanelTag = PinTag,
          Rect = new Inv.Rect(PanelLeft, PanelTop, PanelWidth, PanelHeight)
        };
      }

      Contract.SetBoardCollection(PanelTag, PinArray);
    }
    private void NewHorizontalDock(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewDock(PanelTag, Orientation.Horizontal);
    }
    private void NewVerticalDock(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewDock(PanelTag, Orientation.Vertical);
    }
    private void SetDockOrientation(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Orientation = (Orientation)Reader.ReadByte();

      Contract.SetDockOrientation(PanelTag, Orientation);
    }
    private void SetDockCollection(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var HeaderCount = Reader.ReadInt32();
      var HeaderArray = new ServerPanelTag[HeaderCount];
      for (var i = 0; i < HeaderCount; i++)
        HeaderArray[i] = Reader.ReadPanelTag();

      var ClientCount = Reader.ReadInt32();
      var ClientArray = new ServerPanelTag[ClientCount];
      for (var i = 0; i < ClientCount; i++)
        ClientArray[i] = Reader.ReadPanelTag();

      var FooterCount = Reader.ReadInt32();
      var FooterArray = new ServerPanelTag[FooterCount];
      for (var i = 0; i < FooterCount; i++)
        FooterArray[i] = Reader.ReadPanelTag();

      Contract.SetDockCollection(PanelTag, HeaderArray, ClientArray, FooterArray);
    }
    private void NewEdit(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Input = (EditInput)Reader.ReadByte();
      Contract.NewEdit(PanelTag, Input);
    }
    private void SetEditText(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Text = Reader.ReadString();
      Contract.SetEditText(PanelTag, Text);
    }
    private void SetEditHint(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Hint = Reader.ReadString();
      Contract.SetEditHint(PanelTag, Hint);
    }
    private void SetEditIsReadOnly(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var ReadOnly = Reader.ReadBoolean();
      Contract.SetEditIsReadOnly(PanelTag, ReadOnly);
    }
    private void SetEditHasChange(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var HasChange = Reader.ReadBoolean();
      Contract.SetEditHasChange(PanelTag, HasChange);
    }
    private void SetEditHasReturn(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var HasReturn = Reader.ReadBoolean();
      Contract.SetEditHasReturn(PanelTag, HasReturn);
    }
    private void SetEditFont(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Font = ReadFont(Reader);
      Contract.SetEditFont(PanelTag, Font);
    }
    private void SetEditFocus(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Focus = ReadFocus(Reader);
      Contract.SetEditFocus(PanelTag, Focus);
    }
    private void NewFlow(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewFlow(PanelTag);
    }
    private void SetFlowFixture(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var FixtureTag = Reader.ReadPanelTag();

      Contract.SetFlowFixture(PanelTag, FixtureTag);
    }
    private void ReloadFlow(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var SectionCount = Reader.ReadInt32();

      var SectionArray = new ServerFlowSection[SectionCount];
      for (var i = 0; i < SectionCount; i++)
      {
        var ItemCount = Reader.ReadInt32();
        var HeaderPanelTag = Reader.ReadPanelTag();
        var FooterPanelTag = Reader.ReadPanelTag();

        SectionArray[i] = new ServerFlowSection()
        {
          ItemCount = ItemCount,
          HeaderPanelTag = HeaderPanelTag,
          FooterPanelTag = FooterPanelTag
        };
      }

      Contract.ReloadFlow(PanelTag, SectionArray);
    }
    private void NewFrame(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewFrame(PanelTag);
    }
    private void NewGraphic(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewGraphic(PanelTag);
    }
    private void SetGraphicFit(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var FitMethod = (FitMethod)Reader.ReadByte();
      Contract.SetGraphicFit(PanelTag, FitMethod);
    }
    private void SetGraphicImage(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var ImageTag = Reader.ReadImageTag();
      Contract.SetGraphicImage(PanelTag, ImageTag);
    }
    private void NewVideo(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewVideo(PanelTag);
    }
    private void SetFrameContent(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var ContentTag = Reader.ReadPanelTag();
      Contract.SetFrameContent(PanelTag, ContentTag);
    }
    private void SetFrameTransition(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var ContentTag = Reader.ReadPanelTag();
      var TransitionType = (TransitionType)Reader.ReadByte();
      var TransitionDuration = Reader.ReadTimeSpan();
      Contract.SetFrameTransition(PanelTag, ContentTag, TransitionType, TransitionDuration);
    }
    private void NewLabel(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewLabel(PanelTag);
    }
    private void SetLabelText(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Text = Reader.ReadString();
      Contract.SetLabelText(PanelTag, Text);
    }
    private void SetLabelLineWrapping(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var LineWrapping = Reader.ReadBoolean();
      Contract.SetLabelLineWrapping(PanelTag, LineWrapping);
    }
    private void SetLabelJustification(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Justification = Reader.ReadByte();

      // NOTE: the 255 comparison is backwards compatibility from when it was a nullable field.
      Contract.SetLabelJustification(PanelTag, Justification != 255 ? (Inv.Justification)Justification : Inv.Justification.Left);
    }
    private void SetLabelFont(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Font = ReadFont(Reader);
      Contract.SetLabelFont(PanelTag, Font);
    }
    private void NewMemo(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewMemo(PanelTag);
    }
    private void SetMemoText(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Text = Reader.ReadString();
      Contract.SetMemoText(PanelTag, Text);
    }
    private void SetMemoIsReadOnly(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var ReadOnly = Reader.ReadBoolean();
      Contract.SetMemoIsReadOnly(PanelTag, ReadOnly);
    }
    private void SetMemoHasChange(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var HasChange = Reader.ReadBoolean();
      Contract.SetMemoHasChange(PanelTag, HasChange);
    }
    private void SetMemoFont(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Font = ReadFont(Reader);
      Contract.SetMemoFont(PanelTag, Font);
    }
    private void SetMemoFocus(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Focus = ReadFocus(Reader);
      Contract.SetMemoFocus(PanelTag, Focus);
    }
    private void NewOverlay(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewOverlay(PanelTag);
    }
    private void SetOverlayCollection(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Count = Reader.ReadInt32();
      var PanelArray = new ServerPanelTag[Count];
      for (var i = 0; i < Count; i++)
        PanelArray[i] = Reader.ReadPanelTag();

      Contract.SetOverlayCollection(PanelTag, PanelArray);
    }
    private void NewCanvas(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewCanvas(PanelTag);
    }
    private void DrawCanvas(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Packet = new Inv.TransportPacket(Reader.ReadByteArray());

      Contract.DrawCanvas(PanelTag, Packet);
    }
    private void SetCanvasHasMeasure(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var HasMeasure = Reader.ReadBoolean();
      Contract.SetCanvasHasMeasure(PanelTag, HasMeasure);
    }
    private void NewHorizontalScroll(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewScroll(PanelTag, Orientation.Horizontal);
    }
    private void NewVerticalScroll(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewScroll(PanelTag, Orientation.Vertical);
    }
    private void SetScrollOrientation(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Orientation = (Orientation)Reader.ReadByte();

      Contract.SetScrollOrientation(PanelTag, Orientation);
    }
    private void SetScrollContent(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var ContentTag = Reader.ReadPanelTag();
      Contract.SetScrollContent(PanelTag, ContentTag);
    }
    private void RequestScrollGoTo(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var ScrollRequest = (ScrollRequest)Reader.ReadByte();
      Contract.RequestScrollGoTo(PanelTag, ScrollRequest);
    }
    private void NewShape(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewShape(PanelTag);
    }
    private void SetShapeFit(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Fit = (Inv.FitMethod)Reader.ReadByte();
      Contract.SetShapeFit(PanelTag, Fit);
    }
    private void SetShapeFill(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Colour = Reader.ReadColour();
      Contract.SetShapeFill(PanelTag, Colour);
    }
    private void SetShapeStroke(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Thickness = Reader.ReadInt32();
      var Colour = Reader.ReadColour();
      var LineCap = (LineCap)Reader.ReadByte();
      var LineJoin = (LineJoin)Reader.ReadByte();
      var DashPattern = Reader.ReadInt32Array();
      Contract.SetShapeStroke(PanelTag, Thickness, Colour, LineCap, LineJoin, DashPattern);
    }
    private void SetShapeFigure(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var FigureArray = new ServerShapeFigure[Reader.ReadInt32()];

      for (var FigureIndex = 0; FigureIndex < FigureArray.Length; FigureIndex++)
      {
        var Figure = new ServerShapeFigure();
        Figure.Type = (ServerShapeFigureType)Reader.ReadByte();
        Figure.PathArray = new Inv.Point[Reader.ReadInt32()];

        for (var PathIndex = 0; PathIndex < Figure.PathArray.Length; PathIndex++)
          Figure.PathArray[PathIndex] = new Inv.Point(Reader.ReadInt32(), Reader.ReadInt32());

        FigureArray[FigureIndex] = Figure;
      }

      Contract.SetShapeFigure(PanelTag, FigureArray);
    }
    private void NewHorizontalStack(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewStack(PanelTag, Orientation.Horizontal);
    }
    private void NewVerticalStack(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewStack(PanelTag, Orientation.Vertical);
    }
    private void SetStackOrientation(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Orientation = (Orientation)Reader.ReadByte();

      Contract.SetStackOrientation(PanelTag, Orientation);
    }
    private void SetStackCollection(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Count = Reader.ReadInt32();
      var PanelArray = new ServerPanelTag[Count];
      for (var i = 0; i < Count; i++)
        PanelArray[i] = Reader.ReadPanelTag();

      Contract.SetStackCollection(PanelTag, PanelArray);
    }
    private void NewSwitch(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewSwitch(PanelTag);
    }
    private void SetSwitchHasChange(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var HasChange = Reader.ReadBoolean();
      Contract.SetSwitchHasChange(PanelTag, HasChange);
    }
    private void SetSwitchIsEnabled(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var IsEnabled = Reader.ReadBoolean();
      Contract.SetSwitchIsEnabled(PanelTag, IsEnabled);
    }
    private void SetSwitchIsOn(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var IsOn = Reader.ReadBoolean();
      Contract.SetSwitchIsOn(PanelTag, IsOn);
    }
    private void NewTable(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewTable(PanelTag);
    }
    private void SetTableCollection(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var RowCount = Reader.ReadInt32();
      var RowArray = new ServerTableAxis[RowCount];
      for (var i = 0; i < RowCount; i++)
      {
        var RowLengthType = (TableAxisLength)Reader.ReadByte();
        var RowLengthValue = Reader.ReadInt32();
        var RowPanelTag = Reader.ReadPanelTag();

        RowArray[i] = new ServerTableAxis()
        {
          PanelTag = RowPanelTag,
          LengthType = RowLengthType,
          LengthValue = RowLengthValue
        };
      }

      var ColumnCount = Reader.ReadInt32();
      var ColumnArray = new ServerTableAxis[ColumnCount];
      for (var i = 0; i < ColumnCount; i++)
      {
        var ColumnLengthType = (TableAxisLength)Reader.ReadByte();
        var ColumnLengthValue = Reader.ReadInt32();
        var ColumnPanelTag = Reader.ReadPanelTag();

        ColumnArray[i] = new ServerTableAxis()
        {
          PanelTag = ColumnPanelTag,
          LengthType = ColumnLengthType,
          LengthValue = ColumnLengthValue
        };
      }

      var CellCount = Reader.ReadInt32();
      var CellArray = new ServerTableCell[CellCount];
      for (var i = 0; i < CellCount; i++)
      {
        var CellX = Reader.ReadInt32();
        var CellY = Reader.ReadInt32();
        var CellPanelTag = Reader.ReadPanelTag();

        CellArray[i] = new ServerTableCell()
        {
          X = CellX,
          Y = CellY,
          PanelTag = CellPanelTag,
        };
      }

      Contract.SetTableCollection(PanelTag, RowArray, ColumnArray, CellArray);
    }
    private void NewHorizontalWrap(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewWrap(PanelTag, Orientation.Horizontal);
    }
    private void NewVerticalWrap(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      Contract.NewWrap(PanelTag, Orientation.Vertical);
    }
    private void SetWrapOrientation(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Orientation = (Orientation)Reader.ReadByte();

      Contract.SetWrapOrientation(PanelTag, Orientation);
    }
    private void SetWrapCollection(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      var Count = Reader.ReadInt32();
      var PanelArray = new ServerPanelTag[Count];
      for (var i = 0; i < Count; i++)
        PanelArray[i] = Reader.ReadPanelTag();

      Contract.SetWrapCollection(PanelTag, PanelArray);
    }
    private void SetPanelBackground(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Colour = Reader.ReadColour();
      Contract.SetPanelBackground(PanelTag, Colour);
    }
    private void SetPanelHasAdjust(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var HasAdjust = Reader.ReadBoolean();
      Contract.SetPanelHasAdjust(PanelTag, HasAdjust);
    }
    private void GetPanelDimension(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      /* var Result = */
      Contract.GetPanelDimension(PanelTag);
      // NOTE: we can ignore the above return value because the contract method call takes care of the writing of the result back.
    }
    private void SetPanelAlignment(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Placement = (Placement)Reader.ReadByte();
      Contract.SetPanelAlignment(PanelTag, Placement);
    }
    private void SetPanelCorner(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var TopLeft = Reader.ReadInt32();
      var TopRight = Reader.ReadInt32();
      var BottomRight = Reader.ReadInt32();
      var BottomLeft = Reader.ReadInt32();
      Contract.SetPanelCorner(PanelTag, TopLeft, TopRight, BottomRight, BottomLeft);
    }
    private void SetPanelVisibility(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Visibility = Reader.ReadBoolean();
      Contract.SetPanelVisibility(PanelTag, Visibility);
    }
    private void SetPanelMargin(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Left = Reader.ReadInt32();
      var Top = Reader.ReadInt32();
      var Right = Reader.ReadInt32();
      var Bottom = Reader.ReadInt32();
      Contract.SetPanelMargin(PanelTag, Left, Top, Right, Bottom);
    }
    private void SetPanelPadding(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Left = Reader.ReadInt32();
      var Top = Reader.ReadInt32();
      var Right = Reader.ReadInt32();
      var Bottom = Reader.ReadInt32();
      Contract.SetPanelPadding(PanelTag, Left, Top, Right, Bottom);
    }
    private void SetPanelBorder(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Left = Reader.ReadInt32();
      var Top = Reader.ReadInt32();
      var Right = Reader.ReadInt32();
      var Bottom = Reader.ReadInt32();
      var Colour = Reader.ReadColour();
      Contract.SetPanelBorder(PanelTag, Left, Top, Right, Bottom, Colour);
    }
    private void SetPanelElevation(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Depth = Reader.ReadInt32();
      Contract.SetPanelElevation(PanelTag, Depth);
    }
    private void SetPanelOpacity(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Opacity = Reader.ReadFloat();
      Contract.SetPanelOpacity(PanelTag, Opacity);
    }
    private void SetPanelSize(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();
      var Width = Reader.ReadInt32();
      var Height = Reader.ReadInt32();
      var MinimumWidth = Reader.ReadInt32();
      var MinimumHeight = Reader.ReadInt32();
      var MaximumWidth = Reader.ReadInt32();
      var MaximumHeight = Reader.ReadInt32();
      Contract.SetPanelSize(PanelTag,
        Width >= 0 ? Width : (int?)null,
        Height >= 0 ? Height : (int?)null,
        MinimumWidth >= 0 ? MinimumWidth : (int?)null,
        MinimumHeight >= 0 ? MinimumHeight : (int?)null,
        MaximumWidth >= 0 ? MaximumWidth : (int?)null,
        MaximumHeight >= 0 ? MaximumHeight : (int?)null);
    }
    private void NewImage(CompactReader Reader)
    {
      var ImageTag = Reader.ReadImageTag();
      var ImageSource = Reader.ReadImage();
      Contract.NewImage(ImageTag, ImageSource);
    }
    private void NewSound(CompactReader Reader)
    {
      var SoundTag = Reader.ReadSoundTag();
      var SoundSource = Reader.ReadSound();
      Contract.NewSound(SoundTag, SoundSource);
    }
    private void PlaySound(CompactReader Reader)
    {
      var SoundTag = Reader.ReadSoundTag();
      var SoundVolume = Reader.ReadFloat();
      var SoundRate = Reader.ReadFloat();
      var SoundPan = Reader.ReadFloat();
      Contract.PlaySound(SoundTag, SoundVolume, SoundRate, SoundPan);
    }
    private void PlayClip(CompactReader Reader)
    {
      var ClipTag = Reader.ReadClipTag();
      var SoundTag = Reader.ReadSoundTag();
      var Volume = Reader.ReadFloat();
      var Rate = Reader.ReadFloat();
      var Pan = Reader.ReadFloat();
      var Loop = Reader.ReadBoolean();

      Contract.PlayClip(ClipTag, SoundTag, Volume, Rate, Pan, Loop);
    }
    private void PauseClip(CompactReader Reader)
    {
      var ClipTag = Reader.ReadClipTag();
      Contract.PauseClip(ClipTag);
    }
    private void ResumeClip(CompactReader Reader)
    {
      var ClipTag = Reader.ReadClipTag();
      Contract.ResumeClip(ClipTag);
    }
    private void StopClip(CompactReader Reader)
    {
      var ClipTag = Reader.ReadClipTag();
      Contract.StopClip(ClipTag);
    }
    private void ModulateClip(CompactReader Reader)
    {
      var ClipTag = Reader.ReadClipTag();
      var Volume = Reader.ReadFloat();
      var Rate = Reader.ReadFloat();
      var Pan = Reader.ReadFloat();
      var Loop = Reader.ReadBoolean();

      Contract.ModulateClip(ClipTag, Volume, Rate, Pan, Loop);
    }
    private void StartAnimation(CompactReader Reader)
    {
      var AnimationTag = Reader.ReadAnimationTag();
      var TargetLength = Reader.ReadInt32();
      var TargetArray = new ServerAnimationTarget[TargetLength];
      for (var TargetIndex = 0; TargetIndex < TargetLength; TargetIndex++)
      {
        var Target = new ServerAnimationTarget();
        Target.PanelTag = Reader.ReadPanelTag();

        var TransformLength = Reader.ReadInt32();
        var TransformArray = new ServerAnimationTransform[TransformLength];
        Target.TransformArray = TransformArray;

        for (var TransformIndex = 0; TransformIndex < TransformLength; TransformIndex++)
        {
          var Transform = new ServerAnimationTransform();
          Transform.Type = (AnimationType)Reader.ReadByte();

          switch (Transform.Type)
          {
            case AnimationType.Fade:
              Transform.FadeOffset = Reader.ReadTimeSpan();
              if (Transform.FadeOffset == TimeSpan.Zero)
                Transform.FadeOffset = null;
              Transform.FadeDuration = Reader.ReadTimeSpan();
              Transform.FadeFromOpacity = Reader.ReadFloat();
              Transform.FadeToOpacity = Reader.ReadFloat();
              break;

            case AnimationType.Rotate:
              Transform.RotateOffset = Reader.ReadTimeSpan();
              if (Transform.RotateOffset == TimeSpan.Zero)
                Transform.RotateOffset = null;
              Transform.RotateDuration = Reader.ReadTimeSpan();
              Transform.RotateFromAngle = Reader.ReadFloat();
              Transform.RotateToAngle = Reader.ReadFloat();
              break;

            case AnimationType.Scale:
              Transform.ScaleOffset = Reader.ReadTimeSpan();
              if (Transform.ScaleOffset == TimeSpan.Zero)
                Transform.ScaleOffset = null;
              Transform.ScaleDuration = Reader.ReadTimeSpan();
              Transform.ScaleFromWidth = Reader.ReadFloat();
              Transform.ScaleToWidth = Reader.ReadFloat();
              Transform.ScaleFromHeight = Reader.ReadFloat();
              Transform.ScaleToHeight = Reader.ReadFloat();
              break;

            case AnimationType.Translate:
              Transform.TranslateOffset = Reader.ReadTimeSpan();
              if (Transform.TranslateOffset == TimeSpan.Zero)
                Transform.TranslateOffset = null;
              Transform.TranslateDuration = Reader.ReadTimeSpan();
              Transform.TranslateFromX = Reader.ReadInt32Nullable();
              Transform.TranslateToX = Reader.ReadInt32Nullable();
              Transform.TranslateFromY = Reader.ReadInt32Nullable();
              Transform.TranslateToY = Reader.ReadInt32Nullable();
              break;

            default:
              throw new Exception("AnimationType not handled: " + Transform.Type);
          }

          TransformArray[TransformIndex] = Transform;
        }

        TargetArray[TargetIndex] = Target;
      }

      Contract.StartAnimation(AnimationTag, TargetArray);
    }
    private void StopAnimation(CompactReader Reader)
    {
      var AnimationTag = Reader.ReadAnimationTag();
      Contract.StopAnimation(AnimationTag);
    }
    private void BrowseMarket(CompactReader Reader)
    {
      var AppleiTunesID = Reader.ReadString();
      var GooglePlayID = Reader.ReadString();
      var WindowsStoreID = Reader.ReadString();

      Contract.BrowseMarket(AppleiTunesID, GooglePlayID, WindowsStoreID);
    }
    private void LaunchWebUri(CompactReader Reader)
    {
      var Uri = Reader.ReadUri();

      Contract.LaunchWebUri(Uri);
    }
    private void InstallWebUri(CompactReader Reader)
    {
      var Uri = Reader.ReadUri();

      Contract.InstallWebUri(Uri);
    }
    private void ShowCalendarPicker(CompactReader Reader)
    {
      var PickerTag = Reader.ReadPickerTag();
      var SetDate = Reader.ReadBoolean();
      var SetTime = Reader.ReadBoolean();
      var Value = Reader.ReadDateTime();

      Contract.ShowCalendarPicker(PickerTag, SetDate, SetTime, Value);
    }
    private void ShowDirectoryPicker(CompactReader Reader)
    {
      var PickerTag = Reader.ReadPickerTag();
      var Title = Reader.ReadString();
      var FileType = (PickType)Reader.ReadByte();

      Contract.ShowDirectoryPicker(PickerTag, Title, FileType);
    }
    private void SendEmailMessage(CompactReader Reader)
    {
      var Subject = Reader.ReadString();
      var Body = Reader.ReadString();

      var ToCount = Reader.ReadInt32();
      var ToArray = new ServerEmailTo[ToCount];
      for (var i = 0; i < ToCount; i++)
      {
        var Name = Reader.ReadString();
        var Address = Reader.ReadString();

        ToArray[i] = new ServerEmailTo()
        {
          Name = Name,
          Address = Address
        };
      }

      var AttachmentCount = Reader.ReadInt32();
      var AttachmentArray = new ServerEmailAttachment[AttachmentCount];
      for (var i = 0; i < AttachmentCount; i++)
      {
        var Name = Reader.ReadString();
        var Content = Reader.ReadByteArray();

        AttachmentArray[i] = new ServerEmailAttachment()
        {
          Name = Name,
          Content = Content
        };
      }

      /*var Result =*/
      Contract.SendEmailMessage(Subject, Body, ToArray, AttachmentArray);
      // NOTE: we can ignore the above return value because the contract method call takes care of the writing of the result back.
    }
    private void DialPhone(CompactReader Reader)
    {
      var PhoneNumber = Reader.ReadString();

      Contract.DialPhone(PhoneNumber);
    }
    private void SMSPhone(CompactReader Reader)
    {
      var PhoneNumber = Reader.ReadString();

      Contract.SMSPhone(PhoneNumber);
    }
    private void ClipboardGetText(CompactReader Reader)
    {
      /*var Result = */
      Contract.ClipboardGetText();
      // NOTE: we can ignore the above return value because the contract method call takes care of the writing of the result back.
    }
    private void ClipboardSetText(CompactReader Reader)
    {
      var Text = Reader.ReadString();

      Contract.ClipboardSetText(Text);
    }
    private void ClipboardGetImage(CompactReader Reader)
    {
      /*var Result = */
      Contract.ClipboardGetImage();
      // NOTE: we can ignore the above return value because the contract method call takes care of the writing of the result back.
    }
    private void ClipboardSetImage(CompactReader Reader)
    {
      var Image = Reader.ReadImage();

      Contract.ClipboardSetImage(Image);
    }
    private void HapticFeedback(CompactReader Reader)
    {
      var Feedback = (HapticFeedback)Reader.ReadByte();

      Contract.HapticFeedback(Feedback);
    }
    private void DisposeSurface(CompactReader Reader)
    {
      var SurfaceTag = Reader.ReadSurfaceTag();

      Contract.DisposeSurface(SurfaceTag);
    }
    private void DisposePanel(CompactReader Reader)
    {
      var PanelTag = Reader.ReadPanelTag();

      Contract.DisposePanel(PanelTag);
    }
    private void DisposeImage(CompactReader Reader)
    {
      var ImageTag = Reader.ReadImageTag();

      Contract.DisposeImage(ImageTag);
    }
    private void DisposeSound(CompactReader Reader)
    {
      var SoundTag = Reader.ReadSoundTag();

      Contract.DisposeSound(SoundTag);
    }
#pragma warning restore IDE0051 // Remove unused private members

    private readonly Inv.EnumArray<ServerMessage, Action<CompactReader>> MessageArray;
    private readonly ServerContract Contract;
  }

  internal sealed class ServerLog : ServerContract
  {
    public ServerLog(ServerContract Adapter)
    {
      this.Adapter = Adapter;
    }

    void ServerContract.SetWindowBackground(Colour Colour)
    {
      WriteLine("Window.Background = {0};", FormatColour(Colour));

      Adapter.SetWindowBackground(Colour);
    }
    void ServerContract.SetKeyboardFocus(ServerPanelTag FocusTag)
    {
      WriteLine($"Keyboard.SetFocus({FormatPanelTag(FocusTag)});");

      Adapter.SetKeyboardFocus(FocusTag);
    }
    void ServerContract.SetWindowInputPrevented(bool InputPrevented, ServerPanelTag[] DisablePanelArray, ServerPanelTag[] EnablePanelArray)
    {
      WriteLine($"Window.InputPrevented = {FormatBoolean(InputPrevented)};");

      foreach (var DisablePanel in DisablePanelArray)
        WriteLine($"Window.PreventInput({FormatPanelTag(DisablePanel)};");

      foreach (var EnablePanel in EnablePanelArray)
        WriteLine($"Window.AllowInput({FormatPanelTag(EnablePanel)};");

      Adapter.SetWindowInputPrevented(InputPrevented, DisablePanelArray, EnablePanelArray);
    }
    void ServerContract.NewSurface(ServerSurfaceTag SurfaceTag)
    {
      WriteLine("var S{0} = Application.Window.NewSurface();", SurfaceTag);

      Adapter.NewSurface(SurfaceTag);
    }
    void ServerContract.SetSurfaceHasGestureBackward(ServerSurfaceTag SurfaceTag, bool HasGestureBackward)
    {
      WriteLine("S{0}.HasGestureBackward = {1};", SurfaceTag, FormatBoolean(HasGestureBackward));

      Adapter.SetSurfaceHasGestureBackward(SurfaceTag, HasGestureBackward);
    }
    void ServerContract.SetSurfaceHasGestureForward(ServerSurfaceTag SurfaceTag, bool HasGestureForward)
    {
      WriteLine("S{0}.HasGestureForward = {1};", SurfaceTag, FormatBoolean(HasGestureForward));

      Adapter.SetSurfaceHasGestureForward(SurfaceTag, HasGestureForward);
    }
    void ServerContract.SetSurfaceContent(ServerSurfaceTag SurfaceTag, ServerPanelTag ContentTag)
    {
      WriteLine("S{0}.Content = {1};", SurfaceTag, FormatPanelTag(ContentTag));

      Adapter.SetSurfaceContent(SurfaceTag, ContentTag);
    }
    void ServerContract.NewFrame(ServerPanelTag PanelTag)
    {
      WriteLine($"var P{PanelTag} = Inv.Frame.New();");

      Adapter.NewFrame(PanelTag);
    }
    void ServerContract.NewDock(ServerPanelTag PanelTag, Orientation Orientation)
    {
      WriteLine("var P{0} = Inv.Dock.New{1}();", PanelTag, Orientation);

      Adapter.NewDock(PanelTag, Orientation);
    }
    void ServerContract.SetPanelAlignment(ServerPanelTag PanelTag, Inv.Placement Placement)
    {
      WriteLine("P{0}.Alignment.{1}();", PanelTag, Placement);

      Adapter.SetPanelAlignment(PanelTag, Placement);
    }
    void ServerContract.SetPanelBorder(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom, Colour Colour)
    {
      if (Left != 0 || Top != 0 || Right != 0 || Bottom != 0)
        WriteLine("P{0}.Border.Set({1});", PanelTag, FormatEdge(Left, Top, Right, Bottom));

      if (Colour != null)
        WriteLine("P{0}.Border.Colour = {1};", PanelTag, FormatColour(Colour));

      Adapter.SetPanelBorder(PanelTag, Left, Top, Right, Bottom, Colour);
    }
    void ServerContract.SetPanelCorner(ServerPanelTag PanelTag, int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      WriteLine("P{0}.Corner.Set({1});", PanelTag, FormatCorner(TopLeft, TopRight, BottomRight, BottomLeft));

      Adapter.SetPanelCorner(PanelTag, TopLeft, TopRight, BottomRight, BottomLeft);
    }
    void ServerContract.SetPanelElevation(ServerPanelTag PanelTag, int Depth)
    {
      WriteLine("P{0}.Elevation.Set({1});", PanelTag, Depth);

      Adapter.SetPanelElevation(PanelTag, Depth);
    }
    void ServerContract.SetPanelMargin(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom)
    {
      WriteLine("P{0}.Margin.Set({1});", PanelTag, FormatEdge(Left, Top, Right, Bottom));

      Adapter.SetPanelMargin(PanelTag, Left, Top, Right, Bottom);
    }
    void ServerContract.SetPanelOpacity(ServerPanelTag PanelTag, float Opacity)
    {
      WriteLine("P{0}.Opacity.Set({1});", PanelTag, Opacity);

      Adapter.SetPanelOpacity(PanelTag, Opacity);
    }
    void ServerContract.SetPanelPadding(ServerPanelTag PanelTag, int Left, int Top, int Right, int Bottom)
    {
      WriteLine("P{0}.Padding.Set({1});", PanelTag, FormatEdge(Left, Top, Right, Bottom));

      Adapter.SetPanelPadding(PanelTag, Left, Top, Right, Bottom);
    }
    void ServerContract.SetPanelSize(ServerPanelTag PanelTag, int? Width, int? Height, int? MinimumWidth, int? MaximumWidth, int? MinimumHeight, int? MaximumHeight)
    {
      if (Width != null || Height != null)
      {
        if (Width == null)
          WriteLine("P{0}.Size.SetHeight({1});", PanelTag, Height);
        else if (Height == null)
          WriteLine("P{0}.Size.SetWidth({1});", PanelTag, Width);
        else
          WriteLine("P{0}.Size.Set({1}, {2});", PanelTag, Width, Height);
      }

      if (MinimumWidth != null || MinimumHeight != null)
      {
        if (MinimumWidth == null)
          WriteLine("P{0}.Size.SetMinimumHeight({1});", PanelTag, MinimumHeight);
        else if (MinimumHeight == null)
          WriteLine("P{0}.Size.SetMinimumWidth({1});", PanelTag, MinimumWidth);
        else
          WriteLine("P{0}.Size.SetMinimum({1}, {2});", PanelTag, MinimumWidth, MinimumHeight);
      }

      if (MaximumWidth != null || MaximumHeight != null)
      {
        if (MaximumWidth == null)
          WriteLine("P{0}.Size.SetMaximumHeight({1});", PanelTag, MaximumHeight);
        else if (MaximumHeight == null)
          WriteLine("P{0}.Size.SetMaximumWidth({1});", PanelTag, MaximumWidth);
        else
          WriteLine("P{0}.Size.SetMaximum({1}, {2});", PanelTag, MaximumWidth, MaximumHeight);
      }

      Adapter.SetPanelSize(PanelTag, Width, Height, MinimumWidth, MaximumWidth, MinimumHeight, MaximumHeight);
    }
    void ServerContract.SetPanelVisibility(ServerPanelTag PanelTag, bool Visibility)
    {
      WriteLine("P{0}.Visibility.{1}();", PanelTag, Visibility ? "Show" : "Collapse");

      Adapter.SetPanelVisibility(PanelTag, Visibility);
    }
    void ServerContract.SetPanelBackground(ServerPanelTag PanelTag, Colour BackgroundColour)
    {
      WriteLine("P{0}.Background.Colour = {1};", PanelTag, FormatColour(BackgroundColour));

      Adapter.SetPanelBackground(PanelTag, BackgroundColour);
    }
    void ServerContract.SetPanelHasAdjust(ServerPanelTag PanelTag, bool HasAdjust)
    {
      WriteLine("P{0}.HasAdjust = {1};", PanelTag, FormatBoolean(HasAdjust));

      Adapter.SetPanelHasAdjust(PanelTag, HasAdjust);
    }
    Inv.Dimension ServerContract.GetPanelDimension(ServerPanelTag PanelTag)
    {
      WriteLine("P{0}.GetDimension();", PanelTag);

      return Adapter.GetPanelDimension(PanelTag);
    }
    void ServerContract.SetButtonFocus(ServerPanelTag PanelTag, ServerFocus Focus)
    {
      WriteFocus(PanelTag, Focus);

      Adapter.SetButtonFocus(PanelTag, Focus);
    }
    void ServerContract.SetButtonContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      WriteLine("P{0}.Content = {1};", PanelTag, FormatPanelTag(ContentTag));

      Adapter.SetButtonContent(PanelTag, ContentTag);
    }
    void ServerContract.SetButtonIsEnabled(ServerPanelTag PanelTag, bool IsEnabled)
    {
      WriteLine("P{0}.IsEnabled = {1};", PanelTag, FormatBoolean(IsEnabled));

      Adapter.SetButtonIsEnabled(PanelTag, IsEnabled);
    }
    void ServerContract.SetButtonIsFocusable(ServerPanelTag PanelTag, bool IsFocusable)
    {
      WriteLine("P{0}.IsFocusable = {1};", PanelTag, FormatBoolean(IsFocusable));

      Adapter.SetButtonIsFocusable(PanelTag, IsFocusable);
    }
    void ServerContract.SetButtonHint(ServerPanelTag PanelTag, string Hint)
    {
      WriteLine("P{0}.Hint = {1};", PanelTag, FormatString(Hint));

      Adapter.SetButtonHint(PanelTag, Hint);
    }
    void ServerContract.SetButtonHasPress(ServerPanelTag PanelTag, bool HasPress)
    {
      WriteLine("P{0}.HasPress = {1};", PanelTag, FormatBoolean(HasPress));

      Adapter.SetButtonHasPress(PanelTag, HasPress);
    }
    void ServerContract.SetButtonHasRelease(ServerPanelTag PanelTag, bool HasRelease)
    {
      WriteLine("P{0}.HasRelease = {1};", PanelTag, FormatBoolean(HasRelease));

      Adapter.SetButtonHasRelease(PanelTag, HasRelease);
    }
    void ServerContract.SetDockOrientation(ServerPanelTag PanelTag, Orientation Orientation)
    {
      WriteLine("P{0}.Orientation = {1};", PanelTag, Orientation);

      Adapter.SetDockOrientation(PanelTag, Orientation);
    }
    void ServerContract.SetDockCollection(ServerPanelTag PanelTag, ServerPanelTag[] HeaderArray, ServerPanelTag[] ClientArray, ServerPanelTag[] FooterArray)
    {
      WriteLine("P{0}.ComposePanels(new Inv.Panel[]{{{1}}}, new Inv.Panel[]{{{2}}}, new Inv.Panel[]{{{3}}});", PanelTag, FormatPanelTagArray(HeaderArray), FormatPanelTagArray(ClientArray), FormatPanelTagArray(FooterArray));

      Adapter.SetDockCollection(PanelTag, HeaderArray, ClientArray, FooterArray);
    }
    void ServerContract.SetFrameContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      WriteLine("P{0}.Content = {1};", PanelTag, FormatPanelTag(ContentTag));

      Adapter.SetFrameContent(PanelTag, ContentTag);
    }
    void ServerContract.SetFrameTransition(ServerPanelTag PanelTag, ServerPanelTag ContentTag, TransitionType TransitionType, TimeSpan TransitionDuration)
    {
      WriteLine("P{0}.Transition({1}).{2}({3});", PanelTag, FormatPanelTag(ContentTag), TransitionType, TransitionDuration);

      Adapter.SetFrameTransition(PanelTag, ContentTag, TransitionType, TransitionDuration);
    }
    void ServerContract.SetOverlayCollection(ServerPanelTag PanelTag, ServerPanelTag[] PanelArray)
    {
      WriteLine("P{0}.ComposePanels({1});", PanelTag, FormatPanelTagArray(PanelArray));

      Adapter.SetOverlayCollection(PanelTag, PanelArray);
    }
    void ServerContract.ExitApplication()
    {
      WriteLine("Application.Exit();");

      Adapter.ExitApplication();
    }
    void ServerContract.SetSurfaceBackground(ServerSurfaceTag SurfaceTag, Colour Colour)
    {
      WriteLine("S{0}.Background = {1};", SurfaceTag, FormatColour(Colour));

      Adapter.SetSurfaceBackground(SurfaceTag, Colour);
    }
    void ServerContract.TransitionSurface(ServerSurfaceTag SurfaceTag, TransitionType TransitionType, TimeSpan TransitionDuration)
    {
      WriteLine("Application.Window.Transition(S{0}).{1}({2});", SurfaceTag, TransitionType, TransitionDuration);

      Adapter.TransitionSurface(SurfaceTag, TransitionType, TransitionDuration);
    }
    void ServerContract.NewBlock(ServerPanelTag PanelTag)
    {
      WriteLine($"var P{PanelTag} = Inv.Block.New();");

      Adapter.NewBlock(PanelTag);
    }
    void ServerContract.SetBlockCollection(ServerPanelTag PanelTag, ServerBlockSpan[] SpanArray)
    {
      WriteLine("P{0}.Collection({1});", PanelTag, FormatBlockSpanArray(SpanArray));

      Adapter.SetBlockCollection(PanelTag, SpanArray);
    }
    void ServerContract.SetBlockFont(ServerPanelTag PanelTag, ServerFont Font)
    {
      WriteFont(PanelTag, Font);

      Adapter.SetBlockFont(PanelTag, Font);
    }
    void ServerContract.SetBlockLineWrapping(ServerPanelTag PanelTag, bool LineWrapping)
    {
      WriteLine("P{0}.LineWrapping = {1};", PanelTag, FormatBoolean(LineWrapping));

      Adapter.SetBlockLineWrapping(PanelTag, LineWrapping);
    }
    void ServerContract.SetBlockJustification(ServerPanelTag PanelTag, Justification Justification)
    {
      WriteLine("P{0}.Justify.{1}();", PanelTag, Justification.ToString());

      Adapter.SetBlockJustification(PanelTag, Justification);
    }
    void ServerContract.NewBrowser(ServerPanelTag PanelTag)
    {
      WriteLine($"var P{PanelTag} = Inv.Browser.New();");

      Adapter.NewBrowser(PanelTag);
    }
    void ServerContract.LoadBrowser(ServerPanelTag PanelTag, Uri Uri, string Html)
    {
      WriteLine("P{0}.Load{1}({2});", PanelTag, Uri != null ? "Uri" : "Html", Uri != null ? Uri.OriginalString : Html);

      Adapter.LoadBrowser(PanelTag, Uri, Html);
    }
    void ServerContract.NewButton(ServerPanelTag PanelTag, Inv.ButtonStyle Style)
    {
      WriteLine($"var P{PanelTag} = Inv.Button.New{Style}();");

      Adapter.NewButton(PanelTag, Style);
    }
    void ServerContract.NewBoard(ServerPanelTag PanelTag)
    {
      WriteLine($"var P{PanelTag} = Inv.Board.New();");

      Adapter.NewBoard(PanelTag);
    }
    void ServerContract.SetBoardCollection(ServerPanelTag PanelTag, ServerBoardPin[] PinArray)
    {
      WriteLine("P{0}.Collection({1});", PanelTag, FormatBoardPinArray(PinArray));

      Adapter.SetBoardCollection(PanelTag, PinArray);
    }
    void ServerContract.NewEdit(ServerPanelTag PanelTag, EditInput Input)
    {
      WriteLine($"var P{PanelTag} = Inv.Edit.New{Input}();");

      Adapter.NewEdit(PanelTag, Input);
    }
    void ServerContract.SetEditFont(ServerPanelTag PanelTag, ServerFont Font)
    {
      WriteFont(PanelTag, Font);

      Adapter.SetEditFont(PanelTag, Font);
    }
    void ServerContract.SetEditFocus(ServerPanelTag PanelTag, ServerFocus Focus)
    {
      WriteFocus(PanelTag, Focus);

      Adapter.SetEditFocus(PanelTag, Focus);
    }
    void ServerContract.SetEditHint(ServerPanelTag PanelTag, string Hint)
    {
      WriteLine("P{0}.Hint = {1};", PanelTag, FormatString(Hint));

      Adapter.SetEditHint(PanelTag, Hint);
    }
    void ServerContract.SetEditText(ServerPanelTag PanelTag, string Text)
    {
      WriteLine("P{0}.Text = {1};", PanelTag, FormatString(Text));

      Adapter.SetEditText(PanelTag, Text);
    }
    void ServerContract.SetEditIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly)
    {
      WriteLine("P{0}.IsReadOnly = {1};", PanelTag, FormatBoolean(ReadOnly));

      Adapter.SetEditIsReadOnly(PanelTag, ReadOnly);
    }
    void ServerContract.SetEditHasChange(ServerPanelTag PanelTag, bool HasChange)
    {
      WriteLine("P{0}.HasChange = {1};", PanelTag, FormatBoolean(HasChange));

      Adapter.SetEditHasChange(PanelTag, HasChange);
    }
    void ServerContract.SetEditHasReturn(ServerPanelTag PanelTag, bool HasReturn)
    {
      WriteLine("P{0}.HasReturn = {1};", PanelTag, FormatBoolean(HasReturn));

      Adapter.SetEditHasReturn(PanelTag, HasReturn);
    }
    void ServerContract.NewFlow(ServerPanelTag PanelTag)
    {
      WriteLine($"var P{PanelTag} = Inv.Flow.New();");

      Adapter.NewFlow(PanelTag);
    }
    void ServerContract.SetFlowFixture(ServerPanelTag PanelTag, ServerPanelTag FixtureTag)
    {
      WriteLine($"{FormatPanelTag(PanelTag)}.SetFixture({FormatPanelTag(FixtureTag)});");

      Adapter.SetFlowFixture(PanelTag, FixtureTag);
    }
    void ServerContract.ReloadFlow(ServerPanelTag PanelTag, ServerFlowSection[] SectionArray)
    {
      WriteLine("P{0}.Reload()", PanelTag); // TODO: sections.

      Adapter.ReloadFlow(PanelTag, SectionArray);
    }
    void ServerContract.NewGraphic(ServerPanelTag PanelTag)
    {
      WriteLine($"var P{PanelTag} = Inv.Graphic.New();");

      Adapter.NewGraphic(PanelTag);
    }
    void ServerContract.SetGraphicFit(ServerPanelTag PanelTag, FitMethod FitMethod)
    {
      WriteLine($"P{PanelTag}.Image = IMG{FitMethod};");

      Adapter.SetGraphicFit(PanelTag, FitMethod);
    }
    void ServerContract.SetGraphicImage(ServerPanelTag PanelTag, ServerImageTag ImageTag)
    {
      WriteLine("P{0}.Image = IMG{1};", PanelTag, ImageTag);

      Adapter.SetGraphicImage(PanelTag, ImageTag);
    }
    void ServerContract.NewVideo(ServerPanelTag PanelTag)
    {
      WriteLine($"var P{PanelTag} = Inv.Video.New();");

      Adapter.NewVideo(PanelTag);
    }
    void ServerContract.NewLabel(ServerPanelTag PanelTag)
    {
      WriteLine($"var P{PanelTag} = Inv.Label.New();");

      Adapter.NewLabel(PanelTag);
    }
    void ServerContract.SetLabelFont(ServerPanelTag PanelTag, ServerFont Font)
    {
      WriteFont(PanelTag, Font);

      Adapter.SetLabelFont(PanelTag, Font);
    }
    void ServerContract.SetLabelLineWrapping(ServerPanelTag PanelTag, bool LineWrapping)
    {
      WriteLine("P{0}.LineWrapping = {1};", PanelTag, FormatBoolean(LineWrapping));

      Adapter.SetLabelLineWrapping(PanelTag, LineWrapping);
    }
    void ServerContract.SetLabelJustification(ServerPanelTag PanelTag, Justification Justification)
    {
      WriteLine("P{0}.Justify.{1}();", PanelTag, Justification.ToString());

      Adapter.SetLabelJustification(PanelTag, Justification);
    }
    void ServerContract.SetLabelText(ServerPanelTag PanelTag, string Text)
    {
      WriteLine("P{0}.Text = {1};", PanelTag, FormatString(Text));

      Adapter.SetLabelText(PanelTag, Text);
    }
    void ServerContract.NewMemo(ServerPanelTag PanelTag)
    {
      WriteLine($"var P{PanelTag} = Inv.Memo.New();");

      Adapter.NewMemo(PanelTag);
    }
    void ServerContract.SetMemoFont(ServerPanelTag PanelTag, ServerFont Font)
    {
      WriteFont(PanelTag, Font);

      Adapter.SetMemoFont(PanelTag, Font);
    }
    void ServerContract.SetMemoFocus(ServerPanelTag PanelTag, ServerFocus Focus)
    {
      WriteFocus(PanelTag, Focus);

      Adapter.SetMemoFocus(PanelTag, Focus);
    }
    void ServerContract.SetMemoText(ServerPanelTag PanelTag, string Text)
    {
      WriteLine("P{0}.Text = {1};", PanelTag, FormatString(Text));

      Adapter.SetMemoText(PanelTag, Text);
    }
    void ServerContract.SetMemoIsReadOnly(ServerPanelTag PanelTag, bool ReadOnly)
    {
      WriteLine("P{0}.IsReadOnly = {1};", PanelTag, FormatBoolean(ReadOnly));

      Adapter.SetMemoIsReadOnly(PanelTag, ReadOnly);
    }
    void ServerContract.SetMemoHasChange(ServerPanelTag PanelTag, bool HasChange)
    {
      WriteLine("P{0}.HasChange = {1};", PanelTag, FormatBoolean(HasChange));

      Adapter.SetMemoHasChange(PanelTag, HasChange);
    }
    void ServerContract.NewOverlay(ServerPanelTag PanelTag)
    {
      WriteLine($"var P{PanelTag} = Inv.Overay.New();");

      Adapter.NewOverlay(PanelTag);
    }
    void ServerContract.NewCanvas(ServerPanelTag PanelTag)
    {
      WriteLine($"var P{PanelTag} = Inv.Canvas.New();");

      Adapter.NewCanvas(PanelTag);
    }
    void ServerContract.DrawCanvas(ServerPanelTag PanelTag, Inv.TransportPacket Packet)
    {
      WriteLine("P{0}.Draw({1});", PanelTag, Packet.Buffer.Length);

      Adapter.DrawCanvas(PanelTag, Packet);
    }
    void ServerContract.SetCanvasHasMeasure(ServerPanelTag PanelTag, bool HasMeasure)
    {
      WriteLine("P{0}.HasMeasure = {1};", PanelTag, FormatBoolean(HasMeasure));

      Adapter.SetCanvasHasMeasure(PanelTag, HasMeasure);
    }
    void ServerContract.NewScroll(ServerPanelTag PanelTag, Orientation Orientation)
    {
      WriteLine($"var P{PanelTag} = Inv.Scroll.New{Orientation}();");

      Adapter.NewScroll(PanelTag, Orientation);
    }
    void ServerContract.NewShape(ServerPanelTag PanelTag)
    {
      WriteLine($"var P{PanelTag} = Inv.Shape.New();");

      Adapter.NewShape(PanelTag);
    }
    void ServerContract.SetShapeFit(ServerPanelTag PanelTag, Inv.FitMethod Fit)
    {
      WriteLine($"P{PanelTag}.Fit = {Fit};");

      Adapter.SetShapeFit(PanelTag, Fit);
    }
    void ServerContract.SetShapeFill(ServerPanelTag PanelTag, Inv.Colour Colour)
    {
      WriteLine($"P{PanelTag}.Fill.Colour = {Colour};");

      Adapter.SetShapeFill(PanelTag, Colour);
    }
    void ServerContract.SetShapeStroke(ServerPanelTag PanelTag, int Thickness, Inv.Colour Colour, Inv.LineCap LineCap, Inv.LineJoin LineJoin, int[] DashPattern)
    {
      WriteLine($"P{PanelTag}.Stroke.Set({Thickness}).{LineCap}().{LineJoin}().Dash({DashPattern}).Colour = {Colour};");

      Adapter.SetShapeStroke(PanelTag, Thickness, Colour, LineCap, LineJoin, DashPattern);
    }
    void ServerContract.SetShapeFigure(ServerPanelTag PanelTag, ServerShapeFigure[] FigureArray)
    {
      WriteLine($"P{PanelTag}.Figure();"); // TODO: FigureArray.

      Adapter.SetShapeFigure(PanelTag, FigureArray);
    }
    void ServerContract.NewStack(ServerPanelTag PanelTag, Orientation Orientation)
    {
      WriteLine($"var P{PanelTag} = Inv.Stack.New{Orientation}();");

      Adapter.NewStack(PanelTag, Orientation);
    }
    void ServerContract.SetStackOrientation(ServerPanelTag PanelTag, Orientation Orientation)
    {
      WriteLine($"P{PanelTag}.Orientation = {Orientation};");

      Adapter.SetStackOrientation(PanelTag, Orientation);
    }
    void ServerContract.SetStackCollection(ServerPanelTag PanelTag, ServerPanelTag[] PanelArray)
    {
      WriteLine("P{0}.ComposePanels({1});", PanelTag, FormatPanelTagArray(PanelArray));

      Adapter.SetStackCollection(PanelTag, PanelArray);
    }
    void ServerContract.SetScrollOrientation(ServerPanelTag PanelTag, Orientation Orientation)
    {
      WriteLine("P{0}.Orientation = {1};", PanelTag, Orientation);

      Adapter.SetScrollOrientation(PanelTag, Orientation);
    }
    void ServerContract.SetScrollContent(ServerPanelTag PanelTag, ServerPanelTag ContentTag)
    {
      WriteLine("P{0}.Content = {1};", PanelTag, FormatPanelTag(ContentTag));

      Adapter.SetScrollContent(PanelTag, ContentTag);
    }
    void ServerContract.RequestScrollGoTo(ServerPanelTag PanelTag, ScrollRequest ScrollRequest)
    {
      WriteLine("P{0}.GoTo{1}();", PanelTag, ScrollRequest);

      Adapter.RequestScrollGoTo(PanelTag, ScrollRequest);
    }
    void ServerContract.NewSwitch(ServerPanelTag PanelTag)
    {
      WriteLine($"var P{PanelTag} = Inv.Switch.New();");

      Adapter.NewSwitch(PanelTag);
    }
    void ServerContract.SetSwitchHasChange(ServerPanelTag PanelTag, bool HasChange)
    {
      WriteLine("P{0}.HasChange = {1};", PanelTag, FormatBoolean(HasChange));

      Adapter.SetSwitchHasChange(PanelTag, HasChange);
    }
    void ServerContract.SetSwitchIsEnabled(ServerPanelTag PanelTag, bool IsEnabled)
    {
      WriteLine("P{0}.IsEnabled = {1};", PanelTag, FormatBoolean(IsEnabled));

      Adapter.SetSwitchIsEnabled(PanelTag, IsEnabled);
    }
    void ServerContract.SetSwitchIsOn(ServerPanelTag PanelTag, bool IsOn)
    {
      WriteLine("P{0}.IsOn = {1};", PanelTag, FormatBoolean(IsOn));

      Adapter.SetSwitchIsOn(PanelTag, IsOn);
    }
    void ServerContract.NewTable(ServerPanelTag PanelTag)
    {
      WriteLine($"var P{PanelTag} = Inv.Table.New();");

      Adapter.NewTable(PanelTag);
    }
    void ServerContract.SetTableCollection(ServerPanelTag PanelTag, ServerTableAxis[] RowArray, ServerTableAxis[] ColumnArray, ServerTableCell[] CellArray)
    {
      WriteLine("P{0}.Collection({0}, {1}, {2});", PanelTag, RowArray.Length, ColumnArray.Length, CellArray.Length);

      // TODO: proper logging explanation.

      Adapter.SetTableCollection(PanelTag, RowArray, ColumnArray, CellArray);
    }
    void ServerContract.NewWrap(ServerPanelTag PanelTag, Orientation Orientation)
    {
      WriteLine($"var P{PanelTag} = Inv.Wrap.New{Orientation}();");

      Adapter.NewWrap(PanelTag, Orientation);
    }
    void ServerContract.SetWrapOrientation(ServerPanelTag PanelTag, Orientation Orientation)
    {
      WriteLine("P{0}.Orientation = {1};", PanelTag, Orientation);

      Adapter.SetWrapOrientation(PanelTag, Orientation);
    }
    void ServerContract.SetWrapCollection(ServerPanelTag PanelTag, ServerPanelTag[] PanelArray)
    {
      WriteLine("P{0}.ComposePanels({1});", PanelTag, FormatPanelTagArray(PanelArray));

      Adapter.SetWrapCollection(PanelTag, PanelArray);
    }
    void ServerContract.NewImage(ServerImageTag ImageTag, Image Image)
    {
      WriteLine("var IMG{0} = NewImage();", ImageTag);

      Adapter.NewImage(ImageTag, Image);
    }
    void ServerContract.NewSound(ServerSoundTag SoundTag, Sound Sound)
    {
      WriteLine("var SFX{0} = NewSound();", SoundTag);

      Adapter.NewSound(SoundTag, Sound);
    }
    void ServerContract.PlaySound(ServerSoundTag SoundTag, float SoundVolume, float SoundRate, float SoundPan)
    {
      WriteLine($"SFX{SoundTag}.Play({SoundVolume}, {SoundRate}, {SoundPan});");

      Adapter.PlaySound(SoundTag, SoundVolume, SoundRate, SoundPan);
    }
    void ServerContract.StartAnimation(ServerAnimationTag AnimationTag, ServerAnimationTarget[] TargetArray)
    {
      WriteLine("var A{0} = Inv.Animation.New({1});", AnimationTag, FormatAnimationTarget(TargetArray));

      Adapter.StartAnimation(AnimationTag, TargetArray);
    }
    void ServerContract.StopAnimation(ServerAnimationTag AnimationTag)
    {
      WriteLine("A{0}.Stop();", AnimationTag);

      Adapter.StopAnimation(AnimationTag);
    }
    void ServerContract.PlayClip(ServerClipTag ClipTag, ServerSoundTag SoundTag, float SoundVolume, float SoundRate, float SoundPan, bool SoundLoop)
    {
      WriteLine("var Clip{0} = Application.Audio.NewClip({1}, {2:F2}, {2:F2}, {3:F2}, {4}).Play();", ClipTag, SoundTag, SoundVolume, SoundRate, SoundPan, FormatBoolean(SoundLoop));

      Adapter.PlayClip(ClipTag, SoundTag, SoundVolume, SoundRate, SoundPan, SoundLoop);
    }
    void ServerContract.PauseClip(ServerClipTag ClipTag)
    {
      WriteLine("Clip{0}.Pause();", ClipTag);

      Adapter.PauseClip(ClipTag);
    }
    void ServerContract.ResumeClip(ServerClipTag ClipTag)
    {
      WriteLine("Clip{0}.Resume();", ClipTag);

      Adapter.ResumeClip(ClipTag);
    }
    void ServerContract.StopClip(ServerClipTag ClipTag)
    {
      WriteLine($"Clip{ClipTag}.Stop();");

      Adapter.StopClip(ClipTag);
    }
    void ServerContract.ModulateClip(ServerClipTag ClipTag, float SoundVolume, float SoundRate, float SoundPan, bool SoundLoop)
    {
      WriteLine($"Clip{ClipTag}.Set({SoundVolume}, {SoundRate}, {SoundPan}, {FormatBoolean(SoundLoop)});");

      Adapter.ModulateClip(ClipTag, SoundVolume, SoundRate, SoundPan, SoundLoop);
    }
    void ServerContract.BrowseMarket(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      WriteLine("Application.Market.Browse({0}, {1}, {2});", FormatString(AppleiTunesID), FormatString(GooglePlayID), FormatString(WindowsStoreID));

      Adapter.BrowseMarket(AppleiTunesID, GooglePlayID, WindowsStoreID);
    }
    void ServerContract.LaunchWebUri(Uri Uri)
    {
      WriteLine("Application.Web.Launch({0});", FormatString(Uri.AbsoluteUri));

      Adapter.LaunchWebUri(Uri);
    }
    void ServerContract.InstallWebUri(Uri Uri)
    {
      WriteLine("Application.Web.Install({0});", FormatString(Uri.AbsoluteUri));

      Adapter.InstallWebUri(Uri);
    }
    void ServerContract.ShowCalendarPicker(ServerPickerTag PickerTag, bool SetDate, bool SetTime, DateTime Value)
    {
      WriteLine("Application.Calendar.Show{0}{1}Picker({2}, {3});", SetDate ? "Date" : "", SetTime ? "Time" : "", PickerTag, Value);

      Adapter.ShowCalendarPicker(PickerTag, SetDate, SetTime, Value);
    }
    void ServerContract.ShowDirectoryPicker(ServerPickerTag PickerTag, string Title, PickType FileType)
    {
      WriteLine("Application.Directory.Show{0}FilePicker({1}, {2});", FileType, PickerTag, Title.ConvertToCSharpString());

      Adapter.ShowDirectoryPicker(PickerTag, Title, FileType);
    }
    bool ServerContract.SendEmailMessage(string Subject, string Body, ServerEmailTo[] ToArray, ServerEmailAttachment[] AttachmentArray)
    {
      // TODO: NewMessage syntax.

      WriteLine("Application.Email.SendMessage({0}, {1}, {2}, {3});", Subject, Body, "{ " + ToArray.Select(T => T.Name + "[" + T.Address + "]").AsSeparatedText(",") + " }", "{ " + AttachmentArray.Select(A => A.Name + "[" + Inv.DataSize.FromBytes(A.Content.Length).ToBriefString() + "]").AsSeparatedText(",") + " }");

      return Adapter.SendEmailMessage(Subject, Body, ToArray, AttachmentArray);
    }
    void ServerContract.DialPhone(string PhoneNumber)
    {
      WriteLine("Application.Phone.Dial({0});", PhoneNumber);

      Adapter.DialPhone(PhoneNumber);
    }
    void ServerContract.SMSPhone(string PhoneNumber)
    {
      WriteLine("Application.Phone.SMS({0});", PhoneNumber);

      Adapter.SMSPhone(PhoneNumber);
    }
    string ServerContract.ClipboardGetText()
    {
      var Result = Adapter.ClipboardGetText();

      WriteLine($"Application.Clipboard.GetText({FormatString(Result)});");

      return Result;
    }
    void ServerContract.ClipboardSetText(string Text)
    {
      WriteLine($"Application.Clipboard.SetText({FormatString(Text)};");

      Adapter.ClipboardSetText(Text);
    }
    Inv.Image ServerContract.ClipboardGetImage()
    {
      var Result = Adapter.ClipboardGetImage();

      WriteLine($"Application.Clipboard.GetImage({FormatImage(Result)});");

      return Result;
    }
    void ServerContract.ClipboardSetImage(Inv.Image Image)
    {
      WriteLine($"Application.Clipboard.SetImage({FormatImage(Image)};");

      Adapter.ClipboardSetImage(Image);
    }
    void ServerContract.HapticFeedback(HapticFeedback Feedback)
    {
      WriteLine($"Application.Haptics.Feedback({Feedback};");

      Adapter.HapticFeedback(Feedback);
    }
    void ServerContract.DisposeSurface(ServerSurfaceTag SurfaceTag)
    {
      WriteLine("S{0}.Dispose();", SurfaceTag);
    }
    void ServerContract.DisposePanel(ServerPanelTag PanelTag)
    {
      WriteLine("P{0}.Dispose();", PanelTag);
    }
    void ServerContract.DisposeImage(ServerImageTag ImageTag)
    {
      WriteLine("IMG{0}.Dispose();", ImageTag);
    }
    void ServerContract.DisposeSound(ServerSoundTag SoundTag)
    {
      WriteLine("SFX{0}.Dispose();", SoundTag);
    }

    private void WriteFont(ServerPanelTag PanelTag, ServerFont Font)
    {
      if (Font.Name != null)
        WriteLine($"P{PanelTag}.Font.Name = {FormatString(Font.Name)};");

      if (Font.Size != null)
        WriteLine($"P{PanelTag}.Font.Size = {Font.Size.Value};");

      if (Font.Colour != null)
        WriteLine($"P{PanelTag}.Font.Colour = {FormatColour(Font.Colour)};");

      if (Font.Weight != null)
        WriteLine($"P{PanelTag}.Font.{Font.Weight.Value}();");

      if (Font.Axis != null)
        WriteLine($"P{PanelTag}.Font.{Font.Axis.Value}();");

      if (Font.Italics != null)
        WriteLine($"P{PanelTag}.Font.IsItalics = {FormatBoolean(Font.Italics.Value)};");

      if (Font.SmallCaps != null)
        WriteLine($"P{PanelTag}.Font.IsSmallCaps = {FormatBoolean(Font.SmallCaps.Value)};");

      if (Font.Underline != null)
        WriteLine($"P{PanelTag}.Font.IsUnderlined = {FormatBoolean(Font.Underline.Value)};");

      if (Font.Strikethrough != null)
        WriteLine($"P{PanelTag}.Font.IsStrikethrough = {FormatBoolean(Font.Strikethrough.Value)};");
    }
    private void WriteFocus(ServerPanelTag PanelTag, ServerFocus Focus)
    {
      WriteLine($"P{PanelTag}.Focus.HasGot = {FormatBoolean(Focus.HasGot)};");
      WriteLine($"P{PanelTag}.Focus.HasLost = {FormatBoolean(Focus.HasLost)};");
    }
    private void WriteLine(string Format, params object[] FieldArray)
    {
      Debug.WriteLine(Format, FieldArray);
    }
    private string FormatImage(Inv.Image Value)
    {
      if (Value == null)
        return "null";
      else
        return Value.ToString();
    }
    private string FormatString(string Value)
    {
      if (Value == null)
        return "null";
      else
        return Value.ConvertToCSharpString();
    }
    private string FormatBoolean(bool Value)
    {
      return Value.ConvertToCSharpKeyword();
    }
    private string FormatAnimationTarget(ServerAnimationTarget[] TargetArray)
    {
      // TODO: all fields.

      return TargetArray.Select(T => T.PanelTag + ":" + T.TransformArray.Select(C => C.Type.ToString()).AsSeparatedText(",")).AsSeparatedText(" | ");
    }
    private string FormatPanelTag(ServerPanelTag PanelTag)
    {
      return PanelTag.IsZero ? "null" : "P" + PanelTag;
    }
    private string FormatPanelTagArray(ServerPanelTag[] PanelTagArray)
    {
      return PanelTagArray.Select(P => FormatPanelTag(P)).AsSeparatedText(",");
    }
    private string FormatBoardPinArray(ServerBoardPin[] PinArray)
    {
      return PinArray.Select(P => P.Rect.ToString() + ":" + FormatPanelTag(P.PanelTag)).AsSeparatedText(",");
    }
    private string FormatBlockSpanArray(ServerBlockSpan[] PinArray)
    {
      return string.Concat(PinArray.Select(P => P.Text ?? Environment.NewLine));
    }
    private string FormatColour(Colour Colour)
    {
      return Colour != null ? "Inv.Colour." + Colour.ToString() : "null";
    }
    private string FormatEdge(int Left, int Top, int Right, int Bottom)
    {
      if (Left == Top && Top == Right && Right == Bottom)
        return Left.ToString();
      else
        return string.Format("{0}, {1}, {2}, {3}", Left, Top, Right, Bottom);
    }
    private string FormatCorner(int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      if (TopLeft == TopRight && TopRight == BottomRight && BottomRight == BottomLeft)
        return TopLeft.ToString();
      else
        return string.Format("{0}, {1}, {2}, {3}", TopLeft, TopRight, BottomRight, BottomLeft);
    }

    private readonly ServerContract Adapter;
  }
}